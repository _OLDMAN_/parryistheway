// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "To7/Toon Shader_Branch_GardientAlpha"
{
	Properties
	{
		[HideInInspector] _AlphaCutoff("Alpha Cutoff ", Range(0, 1)) = 0.5
		[ASEBegin][Header(Vertex Offset)]_WidthOffset("Width Offset", Range( 0 , 1)) = 0
		_CSRange("CSRange", Float) = 0
		_CSSoft("CSSoft", Float) = 1
		_GlowColorOffset("Glow Color Offset", Float) = 0
		_Clip("Clip", Float) = 0.11
		[HDR]_GrowColor("Grow Color", Color) = (1,1,1,0)
		[Header(Albedo)]_AlbedoMap("Albedo Map(主貼圖)", 2D) = "white" {}
		[HDR]_AlbedoEmissionColor("Albedo/Emission  Color(主貼圖/高光顏色)", Color) = (0.3301887,0.3301887,0.3301887,0)
		[Header(Normal)][Normal]_NormalMap("Normal Map(法線貼圖)", 2D) = "bump" {}
		_NormalStrength("Normal Strength(法線強度)", Range( 0 , 5)) = 0
		[Header(Emission)]_EmissionTexture("Emission Texture", 2D) = "white" {}
		[HDR]_EmissionColor("Emission Color", Color) = (0,0,0,1)
		_EmissionStrength("Emission Strength", Float) = 1
		[Header(Light)]_DirLightIntensity("DirLight Intensity(日光強度)", Float) = 1
		_DirLightNormalScale("DirLightNormalScale(N.L強度)", Float) = 0.6
		_DiffuseLightStrength("DiffuseLight Strength(環境光強度(疊加)", Float) = 0
		_IndirectDiffuseLightNormal("Indirect Diffuse Light Normal(漫射光法線貼圖)", 2D) = "bump" {}
		[Header(Shadow Type)]_ShadowRemapMap("Shadow Remap Map", 2D) = "white" {}
		[KeywordEnum(UseShaderGardientColor,UseShadowRemapMap)] _ShadowRemapType("Shadow Remap Type(陰影重映射方式)(左為背光右為迎光)", Float) = 1
		_RampAddincludePower("Ramp Add(include Power)", Float) = 0
		_RampAddwithoutPower("Ramp Add(without Power)", Float) = 0
		_RampPower("Ramp Power", Range( 0 , 3)) = 1
		[Header(Rim)]_RimOffset("Rim Offset(泛光量值)", Float) = 0
		[HDR]_RimColor("Rim Color(泛光顏色)", Color) = (0,0,0,0)
		_RimSmoothStepMinMax("Rim SmoothStepValue and Smoothness(泛光平滑閾值)", Vector) = (0,1,0,0)
		[Header(Specular)]_SpecularMap("Specular Map(高光貼圖)", 2D) = "white" {}
		_SpecularPow("Specular Pow(高光量值)", Float) = 4
		[HDR]_SpecularColor("Specular Color(高光顏色)", Color) = (1,1,1,0)
		_SpecularSmoothStepMinMax("Specular SmoothStep Value & Smoothness(高光值與平滑度)", Vector) = (0,0.01,0,0)
		_SpecularIntensity("Specular Intensity(高光強度)", Range( 0 , 1)) = 0
		[Header(SRP Addition Lighting Setting)]_SpecularSRPLightSampler("Specular SRPLight Sampler(高光燈光採樣次數)", Float) = 5
		_SpecularSRPLightSmoothstep("Specular SRPLight Smoothstep(高光燈光平滑閾值)", Vector) = (0,1,0,0)
		_SpecularDirLightSRPLightIntensity("Specular DirLight/SRPLight Intensity(高光受光影響程度 主光源/SRP光源))", Vector) = (1,1,0,0)
		[Header(Light Attenuation)]_LightAttValue("Light Att Value(光衰減程度)", Float) = -0.3
		_LightAttSoft("Light Att Soft(光衰減平滑度)", Float) = 0.46
		[Header(Celluloid Edge Noise Texture)]_RampCelluloidNoiseTexture("Ramp Celluloid Noise Texture(賽璐璐陰影邊緣噪波貼圖)", 2D) = "white" {}
		_RampCelluloidNoiseValue("Ramp Celluloid Noise Value(賽璐璐陰影強度)", Float) = 0
		_RampCelluloidNoiseSoftness("Ramp Celluloid Noise Softness(賽璐璐陰影平滑度)", Float) = 1
		[Header(SSAO)]_AOMix("AO Mix", Range( 0 , 1)) = 0
		[Toggle(_USESSAO_ON)] _UseSSAO("Use SSAO", Float) = 0
		_AOSmoothness("AO Smoothness", Range( 0 , 1)) = 1
		_AOThresold("AO Thresold", Range( 0 , 1)) = 0
		[HDR]_AOColour("AO Colour", Color) = (0,0,0,0)
		[HDR]_BassColor("BassColor", Color) = (1,1,1,1)
		[HDR]_Color0("Color 0", Color) = (1,1,1,0)
		[Header(Height Fog Setting)]_FogHeightEnd("Fog Height End", Float) = 4
		_FogHeightStart("Fog Height Start", Float) = 0
		_HeightFogWorldPosYClipAdd("HeightFog WorldPos Y Clip Add", Float) = 0
		_NoiseFogClip("NoiseFog Clip", Float) = 0.76
		_NoiseFogSoft("NoiseFog Soft", Float) = 0.4
		[HDR]_HeightFogColor("HeightFogColor", Color) = (0,0,0,0)
		[Toggle(_USEHEIGHTFOG_ON)] _UseHeightFog("Use HeightFog ?", Float) = 0
		[Header(Fog Noise Setting)][Enum(Off,0,On,1)]_UseFogNoise("Use Fog Noise", Float) = 0
		_NoiseSpeedWorldPos("Noise Speed(World Pos)", Vector) = (0,0,0,0)
		_NoiseScale("Noise Scale", Float) = 1
		_NoiseDensityUseAdd("Noise Density(Use Add)", Float) = 0
		_NoiseStrengthUsePower("Noise Strength(Use Power)", Float) = 0
		_FogNoiseEdgeValue("Fog Noise Edge Value", Float) = 0
		_FogNoiseEdgeSmoothness("Fog Noise Edge Smoothness", Float) = 0
		_WorldNoiseYMaskClip("世界霧Y軸噪聲衰減極值", Float) = -0.59
		[ASEEnd]_WorldNoiseYMaskStrength("世界霧Y軸噪聲衰減強度", Range( 0 , 1000)) = -0.59
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

		//_TessPhongStrength( "Tess Phong Strength", Range( 0, 1 ) ) = 0.5
		_TessValue( "Max Tessellation", Range( 1, 32 ) ) = 16
		//_TessMin( "Tess Min Distance", Float ) = 10
		//_TessMax( "Tess Max Distance", Float ) = 25
		//_TessEdgeLength ( "Tess Edge length", Range( 2, 50 ) ) = 16
		//_TessMaxDisp( "Tess Max Displacement", Float ) = 25
	}

	SubShader
	{
		LOD 0

		
		Tags { "RenderPipeline"="UniversalPipeline" "RenderType"="Opaque" "Queue"="Geometry" }
		
		Cull Off
		AlphaToMask Off
		HLSLINCLUDE
		#pragma target 5.0

		#ifndef ASE_TESS_FUNCS
		#define ASE_TESS_FUNCS
		float4 FixedTess( float tessValue )
		{
			return tessValue;
		}
		
		float CalcDistanceTessFactor (float4 vertex, float minDist, float maxDist, float tess, float4x4 o2w, float3 cameraPos )
		{
			float3 wpos = mul(o2w,vertex).xyz;
			float dist = distance (wpos, cameraPos);
			float f = clamp(1.0 - (dist - minDist) / (maxDist - minDist), 0.01, 1.0) * tess;
			return f;
		}

		float4 CalcTriEdgeTessFactors (float3 triVertexFactors)
		{
			float4 tess;
			tess.x = 0.5 * (triVertexFactors.y + triVertexFactors.z);
			tess.y = 0.5 * (triVertexFactors.x + triVertexFactors.z);
			tess.z = 0.5 * (triVertexFactors.x + triVertexFactors.y);
			tess.w = (triVertexFactors.x + triVertexFactors.y + triVertexFactors.z) / 3.0f;
			return tess;
		}

		float CalcEdgeTessFactor (float3 wpos0, float3 wpos1, float edgeLen, float3 cameraPos, float4 scParams )
		{
			float dist = distance (0.5 * (wpos0+wpos1), cameraPos);
			float len = distance(wpos0, wpos1);
			float f = max(len * scParams.y / (edgeLen * dist), 1.0);
			return f;
		}

		float DistanceFromPlane (float3 pos, float4 plane)
		{
			float d = dot (float4(pos,1.0f), plane);
			return d;
		}

		bool WorldViewFrustumCull (float3 wpos0, float3 wpos1, float3 wpos2, float cullEps, float4 planes[6] )
		{
			float4 planeTest;
			planeTest.x = (( DistanceFromPlane(wpos0, planes[0]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[0]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[0]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.y = (( DistanceFromPlane(wpos0, planes[1]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[1]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[1]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.z = (( DistanceFromPlane(wpos0, planes[2]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[2]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[2]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.w = (( DistanceFromPlane(wpos0, planes[3]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[3]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[3]) > -cullEps) ? 1.0f : 0.0f );
			return !all (planeTest);
		}

		float4 DistanceBasedTess( float4 v0, float4 v1, float4 v2, float tess, float minDist, float maxDist, float4x4 o2w, float3 cameraPos )
		{
			float3 f;
			f.x = CalcDistanceTessFactor (v0,minDist,maxDist,tess,o2w,cameraPos);
			f.y = CalcDistanceTessFactor (v1,minDist,maxDist,tess,o2w,cameraPos);
			f.z = CalcDistanceTessFactor (v2,minDist,maxDist,tess,o2w,cameraPos);

			return CalcTriEdgeTessFactors (f);
		}

		float4 EdgeLengthBasedTess( float4 v0, float4 v1, float4 v2, float edgeLength, float4x4 o2w, float3 cameraPos, float4 scParams )
		{
			float3 pos0 = mul(o2w,v0).xyz;
			float3 pos1 = mul(o2w,v1).xyz;
			float3 pos2 = mul(o2w,v2).xyz;
			float4 tess;
			tess.x = CalcEdgeTessFactor (pos1, pos2, edgeLength, cameraPos, scParams);
			tess.y = CalcEdgeTessFactor (pos2, pos0, edgeLength, cameraPos, scParams);
			tess.z = CalcEdgeTessFactor (pos0, pos1, edgeLength, cameraPos, scParams);
			tess.w = (tess.x + tess.y + tess.z) / 3.0f;
			return tess;
		}

		float4 EdgeLengthBasedTessCull( float4 v0, float4 v1, float4 v2, float edgeLength, float maxDisplacement, float4x4 o2w, float3 cameraPos, float4 scParams, float4 planes[6] )
		{
			float3 pos0 = mul(o2w,v0).xyz;
			float3 pos1 = mul(o2w,v1).xyz;
			float3 pos2 = mul(o2w,v2).xyz;
			float4 tess;

			if (WorldViewFrustumCull(pos0, pos1, pos2, maxDisplacement, planes))
			{
				tess = 0.0f;
			}
			else
			{
				tess.x = CalcEdgeTessFactor (pos1, pos2, edgeLength, cameraPos, scParams);
				tess.y = CalcEdgeTessFactor (pos2, pos0, edgeLength, cameraPos, scParams);
				tess.z = CalcEdgeTessFactor (pos0, pos1, edgeLength, cameraPos, scParams);
				tess.w = (tess.x + tess.y + tess.z) / 3.0f;
			}
			return tess;
		}
		#endif //ASE_TESS_FUNCS

		ENDHLSL

		
		Pass
		{
			
			Name "Forward"
			Tags { "LightMode"="UniversalForward" }
			
			Blend One Zero, One Zero
			ZWrite On
			ZTest LEqual
			Offset 0 , 0
			ColorMask RGBA
			

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#define TESSELLATION_ON 1
			#pragma require tessellation tessHW
			#pragma hull HullFunction
			#pragma domain DomainFunction
			#define ASE_FIXED_TESSELLATION
			#define _ALPHATEST_ON 1
			#define ASE_SRP_VERSION 100302

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/UnityInstancing.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

			#if ASE_SRP_VERSION <= 70108
			#define REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR
			#endif

			#include "Packages/com.unity.shadergraph/ShaderGraphLibrary/Functions.hlsl"
			#define ASE_NEEDS_VERT_NORMAL
			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#define ASE_NEEDS_FRAG_SHADOWCOORDS
			#pragma shader_feature_local _SHADOWREMAPTYPE_USESHADERGARDIENTCOLOR _SHADOWREMAPTYPE_USESHADOWREMAPMAP
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma shader_feature_local _USESSAO_ON
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma shader_feature_local _USEHEIGHTFOG_ON
			#pragma __SCREEN_SPACE_OCCLUSION
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_tangent : TANGENT;
				float4 texcoord1 : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				#ifdef ASE_FOG
				float fogFactor : TEXCOORD2;
				#endif
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_texcoord4 : TEXCOORD4;
				float4 ase_texcoord5 : TEXCOORD5;
				float4 ase_texcoord6 : TEXCOORD6;
				float4 lightmapUVOrVertexSH : TEXCOORD7;
				float4 ase_texcoord8 : TEXCOORD8;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _AlbedoEmissionColor;
			float4 _GrowColor;
			float4 _HeightFogColor;
			float4 _EmissionColor;
			float4 _EmissionTexture_ST;
			float4 _NormalMap_ST;
			float4 _SpecularMap_ST;
			float4 _SpecularColor;
			float4 _Color0;
			float4 _AOColour;
			float4 _AlbedoMap_ST;
			float4 _BassColor;
			float4 _RimColor;
			float4 _RampCelluloidNoiseTexture_ST;
			float4 _IndirectDiffuseLightNormal_ST;
			float3 _NoiseSpeedWorldPos;
			float2 _SpecularSmoothStepMinMax;
			float2 _SpecularDirLightSRPLightIntensity;
			float2 _SpecularSRPLightSmoothstep;
			float2 _RimSmoothStepMinMax;
			float _FogNoiseEdgeSmoothness;
			float _FogNoiseEdgeValue;
			float _WorldNoiseYMaskStrength;
			float _NoiseDensityUseAdd;
			float _WorldNoiseYMaskClip;
			float _NoiseFogSoft;
			float _NoiseStrengthUsePower;
			float _UseFogNoise;
			float _SpecularPow;
			float _SpecularIntensity;
			float _EmissionStrength;
			float _NoiseScale;
			float _NoiseFogClip;
			float _CSRange;
			float _FogHeightStart;
			float _CSSoft;
			float _WidthOffset;
			float _RampAddincludePower;
			float _RampPower;
			float _NormalStrength;
			float _DirLightNormalScale;
			float _LightAttValue;
			float _LightAttSoft;
			float _DirLightIntensity;
			float _HeightFogWorldPosYClipAdd;
			float _RampAddwithoutPower;
			float _RampCelluloidNoiseSoftness;
			float _SpecularSRPLightSampler;
			float _AOThresold;
			float _AOSmoothness;
			float _AOMix;
			float _DiffuseLightStrength;
			float _GlowColorOffset;
			float _RimOffset;
			float _FogHeightEnd;
			float _RampCelluloidNoiseValue;
			float _Clip;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			sampler2D _ShadowRemapMap;
			sampler2D _NormalMap;
			sampler2D _RampCelluloidNoiseTexture;
			sampler2D _AlbedoMap;
			sampler2D _IndirectDiffuseLightNormal;
			sampler2D _SpecularMap;
			sampler2D _EmissionTexture;


			
			float4 SampleGradient( Gradient gradient, float time )
			{
				float3 color = gradient.colors[0].rgb;
				UNITY_UNROLL
				for (int c = 1; c < 8; c++)
				{
				float colorPos = saturate((time - gradient.colors[c-1].w) / ( 0.00001 + (gradient.colors[c].w - gradient.colors[c-1].w)) * step(c, gradient.colorsLength-1));
				color = lerp(color, gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), gradient.type));
				}
				#ifndef UNITY_COLORSPACE_GAMMA
				color = SRGBToLinear(color);
				#endif
				float alpha = gradient.alphas[0].x;
				UNITY_UNROLL
				for (int a = 1; a < 8; a++)
				{
				float alphaPos = saturate((time - gradient.alphas[a-1].y) / ( 0.00001 + (gradient.alphas[a].y - gradient.alphas[a-1].y)) * step(a, gradient.alphasLength-1));
				alpha = lerp(alpha, gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), gradient.type));
				}
				return float4(color, alpha);
			}
			
			float3 AdditionalLightsLambert( float3 WorldPosition, float3 WorldNormal )
			{
				float3 Color = 0;
				#ifdef _ADDITIONAL_LIGHTS
				int numLights = GetAdditionalLightsCount();
				for(int i = 0; i<numLights;i++)
				{
					Light light = GetAdditionalLight(i, WorldPosition);
					half3 AttLightColor = light.color *(light.distanceAttenuation * light.shadowAttenuation);
					Color +=LightingLambert(AttLightColor, light.direction, WorldNormal);
					
				}
				#endif
				return Color;
			}
			
			float3 ASEIndirectDiffuse( float2 uvStaticLightmap, float3 normalWS )
			{
			#ifdef LIGHTMAP_ON
				return SampleLightmap( uvStaticLightmap, normalWS );
			#else
				return SampleSH(normalWS);
			#endif
			}
			
			half SampleAO17_g13( half2 In0 )
			{
				return SampleAmbientOcclusion(In0);
			}
			
			float3 mod3D289( float3 x ) { return x - floor( x / 289.0 ) * 289.0; }
			float4 mod3D289( float4 x ) { return x - floor( x / 289.0 ) * 289.0; }
			float4 permute( float4 x ) { return mod3D289( ( x * 34.0 + 1.0 ) * x ); }
			float4 taylorInvSqrt( float4 r ) { return 1.79284291400159 - r * 0.85373472095314; }
			float snoise( float3 v )
			{
				const float2 C = float2( 1.0 / 6.0, 1.0 / 3.0 );
				float3 i = floor( v + dot( v, C.yyy ) );
				float3 x0 = v - i + dot( i, C.xxx );
				float3 g = step( x0.yzx, x0.xyz );
				float3 l = 1.0 - g;
				float3 i1 = min( g.xyz, l.zxy );
				float3 i2 = max( g.xyz, l.zxy );
				float3 x1 = x0 - i1 + C.xxx;
				float3 x2 = x0 - i2 + C.yyy;
				float3 x3 = x0 - 0.5;
				i = mod3D289( i);
				float4 p = permute( permute( permute( i.z + float4( 0.0, i1.z, i2.z, 1.0 ) ) + i.y + float4( 0.0, i1.y, i2.y, 1.0 ) ) + i.x + float4( 0.0, i1.x, i2.x, 1.0 ) );
				float4 j = p - 49.0 * floor( p / 49.0 );  // mod(p,7*7)
				float4 x_ = floor( j / 7.0 );
				float4 y_ = floor( j - 7.0 * x_ );  // mod(j,N)
				float4 x = ( x_ * 2.0 + 0.5 ) / 7.0 - 1.0;
				float4 y = ( y_ * 2.0 + 0.5 ) / 7.0 - 1.0;
				float4 h = 1.0 - abs( x ) - abs( y );
				float4 b0 = float4( x.xy, y.xy );
				float4 b1 = float4( x.zw, y.zw );
				float4 s0 = floor( b0 ) * 2.0 + 1.0;
				float4 s1 = floor( b1 ) * 2.0 + 1.0;
				float4 sh = -step( h, 0.0 );
				float4 a0 = b0.xzyw + s0.xzyw * sh.xxyy;
				float4 a1 = b1.xzyw + s1.xzyw * sh.zzww;
				float3 g0 = float3( a0.xy, h.x );
				float3 g1 = float3( a0.zw, h.y );
				float3 g2 = float3( a1.xy, h.z );
				float3 g3 = float3( a1.zw, h.w );
				float4 norm = taylorInvSqrt( float4( dot( g0, g0 ), dot( g1, g1 ), dot( g2, g2 ), dot( g3, g3 ) ) );
				g0 *= norm.x;
				g1 *= norm.y;
				g2 *= norm.z;
				g3 *= norm.w;
				float4 m = max( 0.6 - float4( dot( x0, x0 ), dot( x1, x1 ), dot( x2, x2 ), dot( x3, x3 ) ), 0.0 );
				m = m* m;
				m = m* m;
				float4 px = float4( dot( x0, g0 ), dot( x1, g1 ), dot( x2, g2 ), dot( x3, g3 ) );
				return 42.0 * dot( m, px);
			}
			
			
			VertexOutput VertexFunction ( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float temp_output_431_0 = (1.05 + (_WidthOffset - 0.0) * (-0.3 - 1.05) / (1.0 - 0.0));
				float2 temp_cast_0 = (temp_output_431_0).xx;
				float2 texCoord403 = v.ase_texcoord.xy * float2( 1,1 ) + temp_cast_0;
				float smoothstepResult408 = smoothstep( _CSRange , ( _CSRange + _CSSoft ) , texCoord403.y);
				
				float3 ase_worldTangent = TransformObjectToWorldDir(v.ase_tangent.xyz);
				o.ase_texcoord4.xyz = ase_worldTangent;
				float3 ase_worldNormal = TransformObjectToWorldNormal(v.ase_normal);
				o.ase_texcoord5.xyz = ase_worldNormal;
				float ase_vertexTangentSign = v.ase_tangent.w * unity_WorldTransformParams.w;
				float3 ase_worldBitangent = cross( ase_worldNormal, ase_worldTangent ) * ase_vertexTangentSign;
				o.ase_texcoord6.xyz = ase_worldBitangent;
				OUTPUT_LIGHTMAP_UV( v.texcoord1, unity_LightmapST, o.lightmapUVOrVertexSH.xy );
				OUTPUT_SH( ase_worldNormal, o.lightmapUVOrVertexSH.xyz );
				
				o.ase_texcoord3.xy = v.ase_texcoord.xy;
				o.ase_texcoord8 = v.vertex;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord3.zw = 0;
				o.ase_texcoord4.w = 0;
				o.ase_texcoord5.w = 0;
				o.ase_texcoord6.w = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = ( saturate( smoothstepResult408 ) * ( 1.0 - texCoord403.y ) * v.ase_normal );
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif
				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );
				float4 positionCS = TransformWorldToHClip( positionWS );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				VertexPositionInputs vertexInput = (VertexPositionInputs)0;
				vertexInput.positionWS = positionWS;
				vertexInput.positionCS = positionCS;
				o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				#ifdef ASE_FOG
				o.fogFactor = ComputeFogFactor( positionCS.z );
				#endif
				o.clipPos = positionCS;
				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_tangent : TANGENT;
				float4 texcoord1 : TEXCOORD1;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord = v.ase_texcoord;
				o.ase_tangent = v.ase_tangent;
				o.texcoord1 = v.texcoord1;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				o.ase_tangent = patch[0].ase_tangent * bary.x + patch[1].ase_tangent * bary.y + patch[2].ase_tangent * bary.z;
				o.texcoord1 = patch[0].texcoord1 * bary.x + patch[1].texcoord1 * bary.y + patch[2].texcoord1 * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag ( VertexOutput IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif
				float saferPower192 = max( _RampAddincludePower , 0.0001 );
				float2 uv_NormalMap = IN.ase_texcoord3.xy * _NormalMap_ST.xy + _NormalMap_ST.zw;
				float3 unpack42 = UnpackNormalScale( tex2D( _NormalMap, uv_NormalMap ), _NormalStrength );
				unpack42.z = lerp( 1, unpack42.z, saturate(_NormalStrength) );
				float3 Normal43 = unpack42;
				float3 ase_worldTangent = IN.ase_texcoord4.xyz;
				float3 ase_worldNormal = IN.ase_texcoord5.xyz;
				float3 ase_worldBitangent = IN.ase_texcoord6.xyz;
				float3 tanToWorld0 = float3( ase_worldTangent.x, ase_worldBitangent.x, ase_worldNormal.x );
				float3 tanToWorld1 = float3( ase_worldTangent.y, ase_worldBitangent.y, ase_worldNormal.y );
				float3 tanToWorld2 = float3( ase_worldTangent.z, ase_worldBitangent.z, ase_worldNormal.z );
				float3 tanNormal2 = Normal43;
				float3 worldNormal2 = float3(dot(tanToWorld0,tanNormal2), dot(tanToWorld1,tanNormal2), dot(tanToWorld2,tanNormal2));
				float dotResult3 = dot( worldNormal2 , SafeNormalize(_MainLightPosition.xyz) );
				float ase_lightAtten = 0;
				Light ase_lightAtten_mainLight = GetMainLight( ShadowCoords );
				ase_lightAtten = ase_lightAtten_mainLight.distanceAttenuation * ase_lightAtten_mainLight.shadowAttenuation;
				float smoothstepResult158 = smoothstep( _LightAttValue , ( _LightAttValue + _LightAttSoft ) , ase_lightAtten);
				float3 temp_output_15_0 = ( smoothstepResult158 * ( _MainLightColor.rgb * _MainLightColor.a * _DirLightIntensity ) );
				float3 break16 = temp_output_15_0;
				float2 uv_RampCelluloidNoiseTexture = IN.ase_texcoord3.xy * _RampCelluloidNoiseTexture_ST.xy + _RampCelluloidNoiseTexture_ST.zw;
				float smoothstepResult166 = smoothstep( _RampCelluloidNoiseValue , _RampCelluloidNoiseSoftness , tex2D( _RampCelluloidNoiseTexture, uv_RampCelluloidNoiseTexture ).r);
				float temp_output_165_0 = ( ( pow( saferPower192 , _RampPower ) + ( (dotResult3*_DirLightNormalScale + 0.5) * ( max( max( break16.x , break16.y ) , break16.z ) + 0.0 ) ) + _RampAddwithoutPower ) * smoothstepResult166 );
				float2 temp_cast_0 = (temp_output_165_0).xx;
				Gradient gradient6 = NewGradient( 0, 8, 2, float4( 0.490566, 0.490566, 0.490566, 0 ), float4( 0.4901961, 0.4901961, 0.4901961, 0.2399939 ), float4( 0.6643071, 0.6643071, 0.6643071, 0.2599985 ), float4( 0.6627451, 0.6627451, 0.6627451, 0.4899977 ), float4( 0.8228067, 0.8228067, 0.8228067, 0.5100023 ), float4( 0.8235294, 0.8235294, 0.8235294, 0.7400015 ), float4( 1, 1, 1, 0.7600061 ), float4( 1, 1, 1, 1 ), float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
				float2 temp_cast_1 = (temp_output_165_0).xx;
				#if defined(_SHADOWREMAPTYPE_USESHADERGARDIENTCOLOR)
				float4 staticSwitch123 = SampleGradient( gradient6, temp_output_165_0 );
				#elif defined(_SHADOWREMAPTYPE_USESHADOWREMAPMAP)
				float4 staticSwitch123 = tex2D( _ShadowRemapMap, temp_cast_0 );
				#else
				float4 staticSwitch123 = tex2D( _ShadowRemapMap, temp_cast_0 );
				#endif
				float4 RampFinal360 = staticSwitch123;
				float3 temp_cast_2 = (_SpecularSRPLightSmoothstep.x).xxx;
				float3 temp_cast_3 = (( _SpecularSRPLightSmoothstep.x + _SpecularSRPLightSmoothstep.y )).xxx;
				float3 WorldPosition5_g12 = WorldPosition;
				float3 tanNormal12_g12 = Normal43;
				float3 worldNormal12_g12 = float3(dot(tanToWorld0,tanNormal12_g12), dot(tanToWorld1,tanNormal12_g12), dot(tanToWorld2,tanNormal12_g12));
				float3 WorldNormal5_g12 = worldNormal12_g12;
				float3 localAdditionalLightsLambert5_g12 = AdditionalLightsLambert( WorldPosition5_g12 , WorldNormal5_g12 );
				float3 SRPAdditionLight203 = localAdditionalLightsLambert5_g12;
				float3 smoothstepResult114 = smoothstep( temp_cast_2 , temp_cast_3 , ( ( SRPAdditionLight203 * _SpecularSRPLightSampler ) / _SpecularSRPLightSampler ));
				float3 SRPLightFinal357 = smoothstepResult114;
				float3 bakedGI13_g13 = ASEIndirectDiffuse( IN.lightmapUVOrVertexSH.xy, ase_worldNormal);
				float4 unityObjectToClipPos2_g13 = TransformWorldToHClip(TransformObjectToWorld(IN.ase_texcoord8.xyz));
				float4 computeScreenPos3_g13 = ComputeScreenPos( unityObjectToClipPos2_g13 );
				half2 In017_g13 = ( computeScreenPos3_g13 / computeScreenPos3_g13.w ).xy;
				half localSampleAO17_g13 = SampleAO17_g13( In017_g13 );
				float SF_SSAO18_g13 = localSampleAO17_g13;
				float smoothstepResult8_g13 = smoothstep( _AOThresold , ( _AOThresold + _AOSmoothness ) , SF_SSAO18_g13);
				float4 lerpResult12_g13 = lerp( ( float4( bakedGI13_g13 , 0.0 ) * _AOColour ) , _Color0 , smoothstepResult8_g13);
				float4 lerpResult20_g13 = lerp( _BassColor , ( _BassColor * lerpResult12_g13 ) , _AOMix);
				#ifdef _USESSAO_ON
				float4 staticSwitch23_g13 = lerpResult20_g13;
				#else
				float4 staticSwitch23_g13 = _BassColor;
				#endif
				float2 uv_AlbedoMap = IN.ase_texcoord3.xy * _AlbedoMap_ST.xy + _AlbedoMap_ST.zw;
				float4 tex2DNode8 = tex2D( _AlbedoMap, uv_AlbedoMap );
				float4 temp_output_11_0 = ( _AlbedoEmissionColor * tex2DNode8 );
				float2 uv_IndirectDiffuseLightNormal = IN.ase_texcoord3.xy * _IndirectDiffuseLightNormal_ST.xy + _IndirectDiffuseLightNormal_ST.zw;
				float3 tanNormal24 = UnpackNormalScale( tex2D( _IndirectDiffuseLightNormal, uv_IndirectDiffuseLightNormal ), 1.0f );
				float3 bakedGI24 = ASEIndirectDiffuse( IN.lightmapUVOrVertexSH.xy, float3(dot(tanToWorld0,tanNormal24), dot(tanToWorld1,tanNormal24), dot(tanToWorld2,tanNormal24)));
				float3 LightAtt209 = temp_output_15_0;
				float4 temp_output_26_0 = ( ( temp_output_11_0 * float4( ( bakedGI24 + _DiffuseLightStrength ) , 0.0 ) ) + ( ( temp_output_11_0 * float4( LightAtt209 , 0.0 ) ) * staticSwitch123 ) + ( temp_output_11_0 * float4( SRPAdditionLight203 , 0.0 ) ) );
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - WorldPosition );
				ase_worldViewDir = SafeNormalize( ase_worldViewDir );
				float3 normalizedWorldNormal = normalize( ase_worldNormal );
				float dotResult75 = dot( ase_worldViewDir , normalizedWorldNormal );
				float smoothstepResult81 = smoothstep( _RimSmoothStepMinMax.x , ( _RimSmoothStepMinMax.x + _RimSmoothStepMinMax.y ) , saturate( ( 1.0 - ( dotResult75 + _RimOffset ) ) ));
				float4 Rim201 = ( _RimColor * smoothstepResult81 * float4( LightAtt209 , 0.0 ) );
				float4 temp_output_84_0 = ( temp_output_26_0 + Rim201 );
				float temp_output_13_0_g11 = _FogHeightEnd;
				float3 temp_cast_14 = (WorldPosition.y).xxx;
				float clampResult11_g11 = clamp( ( ( temp_output_13_0_g11 - distance( temp_cast_14 , float3( 0,0,0 ) ) ) / ( temp_output_13_0_g11 - _FogHeightStart ) ) , 0.0 , 1.0 );
				float temp_output_310_0 = step( WorldPosition.y , _HeightFogWorldPosYClipAdd );
				float temp_output_311_0 = ( ( 1.0 - ( 1.0 - clampResult11_g11 ) ) * temp_output_310_0 );
				float smoothstepResult298 = smoothstep( _NoiseFogClip , ( _NoiseFogClip + _NoiseFogSoft ) , temp_output_311_0);
				float temp_output_300_0 = ( temp_output_311_0 + smoothstepResult298 );
				float simplePerlin3D313 = snoise( ( ( _TimeParameters.x * _NoiseSpeedWorldPos ) + WorldPosition )*_NoiseScale );
				simplePerlin3D313 = simplePerlin3D313*0.5 + 0.5;
				float smoothstepResult352 = smoothstep( _FogNoiseEdgeValue , ( _FogNoiseEdgeValue + _FogNoiseEdgeSmoothness ) , saturate( ( simplePerlin3D313 + _NoiseDensityUseAdd ) ));
				float HeightFogWorldPosYClipStep348 = temp_output_310_0;
				float saferPower317 = max( ( smoothstepResult352 * saturate( pow( ( ( HeightFogWorldPosYClipStep348 + _WorldNoiseYMaskClip + WorldPosition.y ) * HeightFogWorldPosYClipStep348 ) , _WorldNoiseYMaskStrength ) ) ) , 0.0001 );
				float FogNoise350 = pow( saferPower317 , _NoiseStrengthUsePower );
				float lerpResult347 = lerp( temp_output_300_0 , ( temp_output_300_0 * FogNoise350 ) , _UseFogNoise);
				float HeightFog301 = lerpResult347;
				float4 lerpResult302 = lerp( temp_output_84_0 , _HeightFogColor , HeightFog301);
				#ifdef _USEHEIGHTFOG_ON
				float4 staticSwitch305 = lerpResult302;
				#else
				float4 staticSwitch305 = temp_output_84_0;
				#endif
				float3 tanNormal90 = Normal43;
				float3 worldNormal90 = float3(dot(tanToWorld0,tanNormal90), dot(tanToWorld1,tanNormal90), dot(tanToWorld2,tanNormal90));
				float dotResult92 = dot( ( ase_worldViewDir + _MainLightPosition.xyz ) , worldNormal90 );
				float smoothstepResult95 = smoothstep( _SpecularSmoothStepMinMax.x , _SpecularSmoothStepMinMax.y , pow( dotResult92 , _SpecularPow ));
				float2 uv_SpecularMap = IN.ase_texcoord3.xy * _SpecularMap_ST.xy + _SpecularMap_ST.zw;
				float4 Specular101 = ( float4( ( ( LightAtt209 * _SpecularDirLightSRPLightIntensity.x ) + ( smoothstepResult114 * _SpecularDirLightSRPLightIntensity.y ) ) , 0.0 ) * ( smoothstepResult95 * _SpecularIntensity ) * _SpecularColor * tex2D( _SpecularMap, uv_SpecularMap ) );
				float2 uv_EmissionTexture = IN.ase_texcoord3.xy * _EmissionTexture_ST.xy + _EmissionTexture_ST.zw;
				float4 temp_output_135_0 = ( ( staticSwitch305 + Specular101 ) + ( tex2D( _EmissionTexture, uv_EmissionTexture ) * _EmissionColor * _EmissionStrength ) );
				float temp_output_431_0 = (1.05 + (_WidthOffset - 0.0) * (-0.3 - 1.05) / (1.0 - 0.0));
				float2 temp_cast_16 = (( temp_output_431_0 + _GlowColorOffset )).xx;
				float2 texCoord424 = IN.ase_texcoord3.xy * float2( 1,1 ) + temp_cast_16;
				float4 lerpResult420 = lerp( ( ( RampFinal360 * float4( SRPLightFinal357 , 0.0 ) ) + ( staticSwitch23_g13 * temp_output_135_0 ) ) , _GrowColor , saturate( texCoord424.y ));
				
				float2 temp_cast_19 = (temp_output_431_0).xx;
				float2 texCoord403 = IN.ase_texcoord3.xy * float2( 1,1 ) + temp_cast_19;
				
				float3 BakedAlbedo = lerpResult420.rgb;
				float3 BakedEmission = 0;
				float3 Color = lerpResult420.rgb;
				float Alpha = 1;
				float AlphaClipThreshold = ( _Clip + texCoord403.y );
				float AlphaClipThresholdShadow = 0.5;

				#ifdef _ALPHATEST_ON
					clip( Alpha - AlphaClipThreshold );
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif

				#ifdef ASE_FOG
					Color = MixFog( Color, IN.fogFactor );
				#endif

				return half4( Color, Alpha );
			}

			ENDHLSL
		}

		
		Pass
		{
			
			Name "ShadowCaster"
			Tags { "LightMode"="ShadowCaster" }

			ZWrite On
			ZTest LEqual
			AlphaToMask Off

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#define TESSELLATION_ON 1
			#pragma require tessellation tessHW
			#pragma hull HullFunction
			#pragma domain DomainFunction
			#define ASE_FIXED_TESSELLATION
			#define _ALPHATEST_ON 1
			#define ASE_SRP_VERSION 100302

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

			#define ASE_NEEDS_VERT_NORMAL
			#pragma __SCREEN_SPACE_OCCLUSION
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				float4 ase_texcoord2 : TEXCOORD2;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _AlbedoEmissionColor;
			float4 _GrowColor;
			float4 _HeightFogColor;
			float4 _EmissionColor;
			float4 _EmissionTexture_ST;
			float4 _NormalMap_ST;
			float4 _SpecularMap_ST;
			float4 _SpecularColor;
			float4 _Color0;
			float4 _AOColour;
			float4 _AlbedoMap_ST;
			float4 _BassColor;
			float4 _RimColor;
			float4 _RampCelluloidNoiseTexture_ST;
			float4 _IndirectDiffuseLightNormal_ST;
			float3 _NoiseSpeedWorldPos;
			float2 _SpecularSmoothStepMinMax;
			float2 _SpecularDirLightSRPLightIntensity;
			float2 _SpecularSRPLightSmoothstep;
			float2 _RimSmoothStepMinMax;
			float _FogNoiseEdgeSmoothness;
			float _FogNoiseEdgeValue;
			float _WorldNoiseYMaskStrength;
			float _NoiseDensityUseAdd;
			float _WorldNoiseYMaskClip;
			float _NoiseFogSoft;
			float _NoiseStrengthUsePower;
			float _UseFogNoise;
			float _SpecularPow;
			float _SpecularIntensity;
			float _EmissionStrength;
			float _NoiseScale;
			float _NoiseFogClip;
			float _CSRange;
			float _FogHeightStart;
			float _CSSoft;
			float _WidthOffset;
			float _RampAddincludePower;
			float _RampPower;
			float _NormalStrength;
			float _DirLightNormalScale;
			float _LightAttValue;
			float _LightAttSoft;
			float _DirLightIntensity;
			float _HeightFogWorldPosYClipAdd;
			float _RampAddwithoutPower;
			float _RampCelluloidNoiseSoftness;
			float _SpecularSRPLightSampler;
			float _AOThresold;
			float _AOSmoothness;
			float _AOMix;
			float _DiffuseLightStrength;
			float _GlowColorOffset;
			float _RimOffset;
			float _FogHeightEnd;
			float _RampCelluloidNoiseValue;
			float _Clip;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			

			
			float3 _LightDirection;

			VertexOutput VertexFunction( VertexInput v )
			{
				VertexOutput o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );

				float temp_output_431_0 = (1.05 + (_WidthOffset - 0.0) * (-0.3 - 1.05) / (1.0 - 0.0));
				float2 temp_cast_0 = (temp_output_431_0).xx;
				float2 texCoord403 = v.ase_texcoord.xy * float2( 1,1 ) + temp_cast_0;
				float smoothstepResult408 = smoothstep( _CSRange , ( _CSRange + _CSSoft ) , texCoord403.y);
				
				o.ase_texcoord2.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord2.zw = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = ( saturate( smoothstepResult408 ) * ( 1.0 - texCoord403.y ) * v.ase_normal );
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif

				float3 normalWS = TransformObjectToWorldDir( v.ase_normal );

				float4 clipPos = TransformWorldToHClip( ApplyShadowBias( positionWS, normalWS, _LightDirection ) );

				#if UNITY_REVERSED_Z
					clipPos.z = min(clipPos.z, clipPos.w * UNITY_NEAR_CLIP_VALUE);
				#else
					clipPos.z = max(clipPos.z, clipPos.w * UNITY_NEAR_CLIP_VALUE);
				#endif

				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = clipPos;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				o.clipPos = clipPos;

				return o;
			}
			
			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord = v.ase_texcoord;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float temp_output_431_0 = (1.05 + (_WidthOffset - 0.0) * (-0.3 - 1.05) / (1.0 - 0.0));
				float2 temp_cast_0 = (temp_output_431_0).xx;
				float2 texCoord403 = IN.ase_texcoord2.xy * float2( 1,1 ) + temp_cast_0;
				
				float Alpha = 1;
				float AlphaClipThreshold = ( _Clip + texCoord403.y );
				float AlphaClipThresholdShadow = 0.5;

				#ifdef _ALPHATEST_ON
					#ifdef _ALPHATEST_SHADOW_ON
						clip(Alpha - AlphaClipThresholdShadow);
					#else
						clip(Alpha - AlphaClipThreshold);
					#endif
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif
				return 0;
			}

			ENDHLSL
		}

		
		Pass
		{
			
			Name "DepthOnly"
			Tags { "LightMode"="DepthOnly" }

			ZWrite On
			ColorMask 0
			AlphaToMask Off

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#define TESSELLATION_ON 1
			#pragma require tessellation tessHW
			#pragma hull HullFunction
			#pragma domain DomainFunction
			#define ASE_FIXED_TESSELLATION
			#define _ALPHATEST_ON 1
			#define ASE_SRP_VERSION 100302

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

			#define ASE_NEEDS_VERT_NORMAL
			#pragma __SCREEN_SPACE_OCCLUSION
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				float4 ase_texcoord2 : TEXCOORD2;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _AlbedoEmissionColor;
			float4 _GrowColor;
			float4 _HeightFogColor;
			float4 _EmissionColor;
			float4 _EmissionTexture_ST;
			float4 _NormalMap_ST;
			float4 _SpecularMap_ST;
			float4 _SpecularColor;
			float4 _Color0;
			float4 _AOColour;
			float4 _AlbedoMap_ST;
			float4 _BassColor;
			float4 _RimColor;
			float4 _RampCelluloidNoiseTexture_ST;
			float4 _IndirectDiffuseLightNormal_ST;
			float3 _NoiseSpeedWorldPos;
			float2 _SpecularSmoothStepMinMax;
			float2 _SpecularDirLightSRPLightIntensity;
			float2 _SpecularSRPLightSmoothstep;
			float2 _RimSmoothStepMinMax;
			float _FogNoiseEdgeSmoothness;
			float _FogNoiseEdgeValue;
			float _WorldNoiseYMaskStrength;
			float _NoiseDensityUseAdd;
			float _WorldNoiseYMaskClip;
			float _NoiseFogSoft;
			float _NoiseStrengthUsePower;
			float _UseFogNoise;
			float _SpecularPow;
			float _SpecularIntensity;
			float _EmissionStrength;
			float _NoiseScale;
			float _NoiseFogClip;
			float _CSRange;
			float _FogHeightStart;
			float _CSSoft;
			float _WidthOffset;
			float _RampAddincludePower;
			float _RampPower;
			float _NormalStrength;
			float _DirLightNormalScale;
			float _LightAttValue;
			float _LightAttSoft;
			float _DirLightIntensity;
			float _HeightFogWorldPosYClipAdd;
			float _RampAddwithoutPower;
			float _RampCelluloidNoiseSoftness;
			float _SpecularSRPLightSampler;
			float _AOThresold;
			float _AOSmoothness;
			float _AOMix;
			float _DiffuseLightStrength;
			float _GlowColorOffset;
			float _RimOffset;
			float _FogHeightEnd;
			float _RampCelluloidNoiseValue;
			float _Clip;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			

			
			VertexOutput VertexFunction( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float temp_output_431_0 = (1.05 + (_WidthOffset - 0.0) * (-0.3 - 1.05) / (1.0 - 0.0));
				float2 temp_cast_0 = (temp_output_431_0).xx;
				float2 texCoord403 = v.ase_texcoord.xy * float2( 1,1 ) + temp_cast_0;
				float smoothstepResult408 = smoothstep( _CSRange , ( _CSRange + _CSSoft ) , texCoord403.y);
				
				o.ase_texcoord2.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord2.zw = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = ( saturate( smoothstepResult408 ) * ( 1.0 - texCoord403.y ) * v.ase_normal );
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif

				o.clipPos = TransformWorldToHClip( positionWS );
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = clipPos;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord = v.ase_texcoord;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float temp_output_431_0 = (1.05 + (_WidthOffset - 0.0) * (-0.3 - 1.05) / (1.0 - 0.0));
				float2 temp_cast_0 = (temp_output_431_0).xx;
				float2 texCoord403 = IN.ase_texcoord2.xy * float2( 1,1 ) + temp_cast_0;
				
				float Alpha = 1;
				float AlphaClipThreshold = ( _Clip + texCoord403.y );

				#ifdef _ALPHATEST_ON
					clip(Alpha - AlphaClipThreshold);
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif
				return 0;
			}
			ENDHLSL
		}

		
		Pass
		{
			
			Name "Meta"
			Tags { "LightMode"="Meta" }

			Cull Off

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#define TESSELLATION_ON 1
			#pragma require tessellation tessHW
			#pragma hull HullFunction
			#pragma domain DomainFunction
			#define ASE_FIXED_TESSELLATION
			#define _ALPHATEST_ON 1
			#define ASE_SRP_VERSION 100302

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/MetaInput.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

			#include "Packages/com.unity.shadergraph/ShaderGraphLibrary/Functions.hlsl"
			#define ASE_NEEDS_VERT_NORMAL
			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#define ASE_NEEDS_FRAG_SHADOWCOORDS
			#define ASE_NEEDS_VERT_TEXTURE_COORDINATES1
			#pragma shader_feature_local _SHADOWREMAPTYPE_USESHADERGARDIENTCOLOR _SHADOWREMAPTYPE_USESHADOWREMAPMAP
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma shader_feature_local _USESSAO_ON
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma shader_feature_local _USEHEIGHTFOG_ON
			#pragma __SCREEN_SPACE_OCCLUSION
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 texcoord1 : TEXCOORD1;
				float4 texcoord2 : TEXCOORD2;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_tangent : TANGENT;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				float4 ase_texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_texcoord4 : TEXCOORD4;
				float4 ase_texcoord5 : TEXCOORD5;
				float4 lightmapUVOrVertexSH : TEXCOORD6;
				float4 ase_texcoord7 : TEXCOORD7;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _AlbedoEmissionColor;
			float4 _GrowColor;
			float4 _HeightFogColor;
			float4 _EmissionColor;
			float4 _EmissionTexture_ST;
			float4 _NormalMap_ST;
			float4 _SpecularMap_ST;
			float4 _SpecularColor;
			float4 _Color0;
			float4 _AOColour;
			float4 _AlbedoMap_ST;
			float4 _BassColor;
			float4 _RimColor;
			float4 _RampCelluloidNoiseTexture_ST;
			float4 _IndirectDiffuseLightNormal_ST;
			float3 _NoiseSpeedWorldPos;
			float2 _SpecularSmoothStepMinMax;
			float2 _SpecularDirLightSRPLightIntensity;
			float2 _SpecularSRPLightSmoothstep;
			float2 _RimSmoothStepMinMax;
			float _FogNoiseEdgeSmoothness;
			float _FogNoiseEdgeValue;
			float _WorldNoiseYMaskStrength;
			float _NoiseDensityUseAdd;
			float _WorldNoiseYMaskClip;
			float _NoiseFogSoft;
			float _NoiseStrengthUsePower;
			float _UseFogNoise;
			float _SpecularPow;
			float _SpecularIntensity;
			float _EmissionStrength;
			float _NoiseScale;
			float _NoiseFogClip;
			float _CSRange;
			float _FogHeightStart;
			float _CSSoft;
			float _WidthOffset;
			float _RampAddincludePower;
			float _RampPower;
			float _NormalStrength;
			float _DirLightNormalScale;
			float _LightAttValue;
			float _LightAttSoft;
			float _DirLightIntensity;
			float _HeightFogWorldPosYClipAdd;
			float _RampAddwithoutPower;
			float _RampCelluloidNoiseSoftness;
			float _SpecularSRPLightSampler;
			float _AOThresold;
			float _AOSmoothness;
			float _AOMix;
			float _DiffuseLightStrength;
			float _GlowColorOffset;
			float _RimOffset;
			float _FogHeightEnd;
			float _RampCelluloidNoiseValue;
			float _Clip;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			sampler2D _ShadowRemapMap;
			sampler2D _NormalMap;
			sampler2D _RampCelluloidNoiseTexture;
			sampler2D _AlbedoMap;
			sampler2D _IndirectDiffuseLightNormal;
			sampler2D _SpecularMap;
			sampler2D _EmissionTexture;


			
			float4 SampleGradient( Gradient gradient, float time )
			{
				float3 color = gradient.colors[0].rgb;
				UNITY_UNROLL
				for (int c = 1; c < 8; c++)
				{
				float colorPos = saturate((time - gradient.colors[c-1].w) / ( 0.00001 + (gradient.colors[c].w - gradient.colors[c-1].w)) * step(c, gradient.colorsLength-1));
				color = lerp(color, gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), gradient.type));
				}
				#ifndef UNITY_COLORSPACE_GAMMA
				color = SRGBToLinear(color);
				#endif
				float alpha = gradient.alphas[0].x;
				UNITY_UNROLL
				for (int a = 1; a < 8; a++)
				{
				float alphaPos = saturate((time - gradient.alphas[a-1].y) / ( 0.00001 + (gradient.alphas[a].y - gradient.alphas[a-1].y)) * step(a, gradient.alphasLength-1));
				alpha = lerp(alpha, gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), gradient.type));
				}
				return float4(color, alpha);
			}
			
			float3 AdditionalLightsLambert( float3 WorldPosition, float3 WorldNormal )
			{
				float3 Color = 0;
				#ifdef _ADDITIONAL_LIGHTS
				int numLights = GetAdditionalLightsCount();
				for(int i = 0; i<numLights;i++)
				{
					Light light = GetAdditionalLight(i, WorldPosition);
					half3 AttLightColor = light.color *(light.distanceAttenuation * light.shadowAttenuation);
					Color +=LightingLambert(AttLightColor, light.direction, WorldNormal);
					
				}
				#endif
				return Color;
			}
			
			float3 ASEIndirectDiffuse( float2 uvStaticLightmap, float3 normalWS )
			{
			#ifdef LIGHTMAP_ON
				return SampleLightmap( uvStaticLightmap, normalWS );
			#else
				return SampleSH(normalWS);
			#endif
			}
			
			half SampleAO17_g13( half2 In0 )
			{
				return SampleAmbientOcclusion(In0);
			}
			
			float3 mod3D289( float3 x ) { return x - floor( x / 289.0 ) * 289.0; }
			float4 mod3D289( float4 x ) { return x - floor( x / 289.0 ) * 289.0; }
			float4 permute( float4 x ) { return mod3D289( ( x * 34.0 + 1.0 ) * x ); }
			float4 taylorInvSqrt( float4 r ) { return 1.79284291400159 - r * 0.85373472095314; }
			float snoise( float3 v )
			{
				const float2 C = float2( 1.0 / 6.0, 1.0 / 3.0 );
				float3 i = floor( v + dot( v, C.yyy ) );
				float3 x0 = v - i + dot( i, C.xxx );
				float3 g = step( x0.yzx, x0.xyz );
				float3 l = 1.0 - g;
				float3 i1 = min( g.xyz, l.zxy );
				float3 i2 = max( g.xyz, l.zxy );
				float3 x1 = x0 - i1 + C.xxx;
				float3 x2 = x0 - i2 + C.yyy;
				float3 x3 = x0 - 0.5;
				i = mod3D289( i);
				float4 p = permute( permute( permute( i.z + float4( 0.0, i1.z, i2.z, 1.0 ) ) + i.y + float4( 0.0, i1.y, i2.y, 1.0 ) ) + i.x + float4( 0.0, i1.x, i2.x, 1.0 ) );
				float4 j = p - 49.0 * floor( p / 49.0 );  // mod(p,7*7)
				float4 x_ = floor( j / 7.0 );
				float4 y_ = floor( j - 7.0 * x_ );  // mod(j,N)
				float4 x = ( x_ * 2.0 + 0.5 ) / 7.0 - 1.0;
				float4 y = ( y_ * 2.0 + 0.5 ) / 7.0 - 1.0;
				float4 h = 1.0 - abs( x ) - abs( y );
				float4 b0 = float4( x.xy, y.xy );
				float4 b1 = float4( x.zw, y.zw );
				float4 s0 = floor( b0 ) * 2.0 + 1.0;
				float4 s1 = floor( b1 ) * 2.0 + 1.0;
				float4 sh = -step( h, 0.0 );
				float4 a0 = b0.xzyw + s0.xzyw * sh.xxyy;
				float4 a1 = b1.xzyw + s1.xzyw * sh.zzww;
				float3 g0 = float3( a0.xy, h.x );
				float3 g1 = float3( a0.zw, h.y );
				float3 g2 = float3( a1.xy, h.z );
				float3 g3 = float3( a1.zw, h.w );
				float4 norm = taylorInvSqrt( float4( dot( g0, g0 ), dot( g1, g1 ), dot( g2, g2 ), dot( g3, g3 ) ) );
				g0 *= norm.x;
				g1 *= norm.y;
				g2 *= norm.z;
				g3 *= norm.w;
				float4 m = max( 0.6 - float4( dot( x0, x0 ), dot( x1, x1 ), dot( x2, x2 ), dot( x3, x3 ) ), 0.0 );
				m = m* m;
				m = m* m;
				float4 px = float4( dot( x0, g0 ), dot( x1, g1 ), dot( x2, g2 ), dot( x3, g3 ) );
				return 42.0 * dot( m, px);
			}
			

			VertexOutput VertexFunction( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float temp_output_431_0 = (1.05 + (_WidthOffset - 0.0) * (-0.3 - 1.05) / (1.0 - 0.0));
				float2 temp_cast_0 = (temp_output_431_0).xx;
				float2 texCoord403 = v.ase_texcoord.xy * float2( 1,1 ) + temp_cast_0;
				float smoothstepResult408 = smoothstep( _CSRange , ( _CSRange + _CSSoft ) , texCoord403.y);
				
				float3 ase_worldTangent = TransformObjectToWorldDir(v.ase_tangent.xyz);
				o.ase_texcoord3.xyz = ase_worldTangent;
				float3 ase_worldNormal = TransformObjectToWorldNormal(v.ase_normal);
				o.ase_texcoord4.xyz = ase_worldNormal;
				float ase_vertexTangentSign = v.ase_tangent.w * unity_WorldTransformParams.w;
				float3 ase_worldBitangent = cross( ase_worldNormal, ase_worldTangent ) * ase_vertexTangentSign;
				o.ase_texcoord5.xyz = ase_worldBitangent;
				OUTPUT_LIGHTMAP_UV( v.texcoord1, unity_LightmapST, o.lightmapUVOrVertexSH.xy );
				OUTPUT_SH( ase_worldNormal, o.lightmapUVOrVertexSH.xyz );
				
				o.ase_texcoord2.xy = v.ase_texcoord.xy;
				o.ase_texcoord7 = v.vertex;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord2.zw = 0;
				o.ase_texcoord3.w = 0;
				o.ase_texcoord4.w = 0;
				o.ase_texcoord5.w = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = ( saturate( smoothstepResult408 ) * ( 1.0 - texCoord403.y ) * v.ase_normal );
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif

				o.clipPos = MetaVertexPosition( v.vertex, v.texcoord1.xy, v.texcoord1.xy, unity_LightmapST, unity_DynamicLightmapST );
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = o.clipPos;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_tangent : TANGENT;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord = v.ase_texcoord;
				o.ase_tangent = v.ase_tangent;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				o.ase_tangent = patch[0].ase_tangent * bary.x + patch[1].ase_tangent * bary.y + patch[2].ase_tangent * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float saferPower192 = max( _RampAddincludePower , 0.0001 );
				float2 uv_NormalMap = IN.ase_texcoord2.xy * _NormalMap_ST.xy + _NormalMap_ST.zw;
				float3 unpack42 = UnpackNormalScale( tex2D( _NormalMap, uv_NormalMap ), _NormalStrength );
				unpack42.z = lerp( 1, unpack42.z, saturate(_NormalStrength) );
				float3 Normal43 = unpack42;
				float3 ase_worldTangent = IN.ase_texcoord3.xyz;
				float3 ase_worldNormal = IN.ase_texcoord4.xyz;
				float3 ase_worldBitangent = IN.ase_texcoord5.xyz;
				float3 tanToWorld0 = float3( ase_worldTangent.x, ase_worldBitangent.x, ase_worldNormal.x );
				float3 tanToWorld1 = float3( ase_worldTangent.y, ase_worldBitangent.y, ase_worldNormal.y );
				float3 tanToWorld2 = float3( ase_worldTangent.z, ase_worldBitangent.z, ase_worldNormal.z );
				float3 tanNormal2 = Normal43;
				float3 worldNormal2 = float3(dot(tanToWorld0,tanNormal2), dot(tanToWorld1,tanNormal2), dot(tanToWorld2,tanNormal2));
				float dotResult3 = dot( worldNormal2 , SafeNormalize(_MainLightPosition.xyz) );
				float ase_lightAtten = 0;
				Light ase_lightAtten_mainLight = GetMainLight( ShadowCoords );
				ase_lightAtten = ase_lightAtten_mainLight.distanceAttenuation * ase_lightAtten_mainLight.shadowAttenuation;
				float smoothstepResult158 = smoothstep( _LightAttValue , ( _LightAttValue + _LightAttSoft ) , ase_lightAtten);
				float3 temp_output_15_0 = ( smoothstepResult158 * ( _MainLightColor.rgb * _MainLightColor.a * _DirLightIntensity ) );
				float3 break16 = temp_output_15_0;
				float2 uv_RampCelluloidNoiseTexture = IN.ase_texcoord2.xy * _RampCelluloidNoiseTexture_ST.xy + _RampCelluloidNoiseTexture_ST.zw;
				float smoothstepResult166 = smoothstep( _RampCelluloidNoiseValue , _RampCelluloidNoiseSoftness , tex2D( _RampCelluloidNoiseTexture, uv_RampCelluloidNoiseTexture ).r);
				float temp_output_165_0 = ( ( pow( saferPower192 , _RampPower ) + ( (dotResult3*_DirLightNormalScale + 0.5) * ( max( max( break16.x , break16.y ) , break16.z ) + 0.0 ) ) + _RampAddwithoutPower ) * smoothstepResult166 );
				float2 temp_cast_0 = (temp_output_165_0).xx;
				Gradient gradient6 = NewGradient( 0, 8, 2, float4( 0.490566, 0.490566, 0.490566, 0 ), float4( 0.4901961, 0.4901961, 0.4901961, 0.2399939 ), float4( 0.6643071, 0.6643071, 0.6643071, 0.2599985 ), float4( 0.6627451, 0.6627451, 0.6627451, 0.4899977 ), float4( 0.8228067, 0.8228067, 0.8228067, 0.5100023 ), float4( 0.8235294, 0.8235294, 0.8235294, 0.7400015 ), float4( 1, 1, 1, 0.7600061 ), float4( 1, 1, 1, 1 ), float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
				float2 temp_cast_1 = (temp_output_165_0).xx;
				#if defined(_SHADOWREMAPTYPE_USESHADERGARDIENTCOLOR)
				float4 staticSwitch123 = SampleGradient( gradient6, temp_output_165_0 );
				#elif defined(_SHADOWREMAPTYPE_USESHADOWREMAPMAP)
				float4 staticSwitch123 = tex2D( _ShadowRemapMap, temp_cast_0 );
				#else
				float4 staticSwitch123 = tex2D( _ShadowRemapMap, temp_cast_0 );
				#endif
				float4 RampFinal360 = staticSwitch123;
				float3 temp_cast_2 = (_SpecularSRPLightSmoothstep.x).xxx;
				float3 temp_cast_3 = (( _SpecularSRPLightSmoothstep.x + _SpecularSRPLightSmoothstep.y )).xxx;
				float3 WorldPosition5_g12 = WorldPosition;
				float3 tanNormal12_g12 = Normal43;
				float3 worldNormal12_g12 = float3(dot(tanToWorld0,tanNormal12_g12), dot(tanToWorld1,tanNormal12_g12), dot(tanToWorld2,tanNormal12_g12));
				float3 WorldNormal5_g12 = worldNormal12_g12;
				float3 localAdditionalLightsLambert5_g12 = AdditionalLightsLambert( WorldPosition5_g12 , WorldNormal5_g12 );
				float3 SRPAdditionLight203 = localAdditionalLightsLambert5_g12;
				float3 smoothstepResult114 = smoothstep( temp_cast_2 , temp_cast_3 , ( ( SRPAdditionLight203 * _SpecularSRPLightSampler ) / _SpecularSRPLightSampler ));
				float3 SRPLightFinal357 = smoothstepResult114;
				float3 bakedGI13_g13 = ASEIndirectDiffuse( IN.lightmapUVOrVertexSH.xy, ase_worldNormal);
				float4 unityObjectToClipPos2_g13 = TransformWorldToHClip(TransformObjectToWorld(IN.ase_texcoord7.xyz));
				float4 computeScreenPos3_g13 = ComputeScreenPos( unityObjectToClipPos2_g13 );
				half2 In017_g13 = ( computeScreenPos3_g13 / computeScreenPos3_g13.w ).xy;
				half localSampleAO17_g13 = SampleAO17_g13( In017_g13 );
				float SF_SSAO18_g13 = localSampleAO17_g13;
				float smoothstepResult8_g13 = smoothstep( _AOThresold , ( _AOThresold + _AOSmoothness ) , SF_SSAO18_g13);
				float4 lerpResult12_g13 = lerp( ( float4( bakedGI13_g13 , 0.0 ) * _AOColour ) , _Color0 , smoothstepResult8_g13);
				float4 lerpResult20_g13 = lerp( _BassColor , ( _BassColor * lerpResult12_g13 ) , _AOMix);
				#ifdef _USESSAO_ON
				float4 staticSwitch23_g13 = lerpResult20_g13;
				#else
				float4 staticSwitch23_g13 = _BassColor;
				#endif
				float2 uv_AlbedoMap = IN.ase_texcoord2.xy * _AlbedoMap_ST.xy + _AlbedoMap_ST.zw;
				float4 tex2DNode8 = tex2D( _AlbedoMap, uv_AlbedoMap );
				float4 temp_output_11_0 = ( _AlbedoEmissionColor * tex2DNode8 );
				float2 uv_IndirectDiffuseLightNormal = IN.ase_texcoord2.xy * _IndirectDiffuseLightNormal_ST.xy + _IndirectDiffuseLightNormal_ST.zw;
				float3 tanNormal24 = UnpackNormalScale( tex2D( _IndirectDiffuseLightNormal, uv_IndirectDiffuseLightNormal ), 1.0f );
				float3 bakedGI24 = ASEIndirectDiffuse( IN.lightmapUVOrVertexSH.xy, float3(dot(tanToWorld0,tanNormal24), dot(tanToWorld1,tanNormal24), dot(tanToWorld2,tanNormal24)));
				float3 LightAtt209 = temp_output_15_0;
				float4 temp_output_26_0 = ( ( temp_output_11_0 * float4( ( bakedGI24 + _DiffuseLightStrength ) , 0.0 ) ) + ( ( temp_output_11_0 * float4( LightAtt209 , 0.0 ) ) * staticSwitch123 ) + ( temp_output_11_0 * float4( SRPAdditionLight203 , 0.0 ) ) );
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - WorldPosition );
				ase_worldViewDir = SafeNormalize( ase_worldViewDir );
				float3 normalizedWorldNormal = normalize( ase_worldNormal );
				float dotResult75 = dot( ase_worldViewDir , normalizedWorldNormal );
				float smoothstepResult81 = smoothstep( _RimSmoothStepMinMax.x , ( _RimSmoothStepMinMax.x + _RimSmoothStepMinMax.y ) , saturate( ( 1.0 - ( dotResult75 + _RimOffset ) ) ));
				float4 Rim201 = ( _RimColor * smoothstepResult81 * float4( LightAtt209 , 0.0 ) );
				float4 temp_output_84_0 = ( temp_output_26_0 + Rim201 );
				float temp_output_13_0_g11 = _FogHeightEnd;
				float3 temp_cast_14 = (WorldPosition.y).xxx;
				float clampResult11_g11 = clamp( ( ( temp_output_13_0_g11 - distance( temp_cast_14 , float3( 0,0,0 ) ) ) / ( temp_output_13_0_g11 - _FogHeightStart ) ) , 0.0 , 1.0 );
				float temp_output_310_0 = step( WorldPosition.y , _HeightFogWorldPosYClipAdd );
				float temp_output_311_0 = ( ( 1.0 - ( 1.0 - clampResult11_g11 ) ) * temp_output_310_0 );
				float smoothstepResult298 = smoothstep( _NoiseFogClip , ( _NoiseFogClip + _NoiseFogSoft ) , temp_output_311_0);
				float temp_output_300_0 = ( temp_output_311_0 + smoothstepResult298 );
				float simplePerlin3D313 = snoise( ( ( _TimeParameters.x * _NoiseSpeedWorldPos ) + WorldPosition )*_NoiseScale );
				simplePerlin3D313 = simplePerlin3D313*0.5 + 0.5;
				float smoothstepResult352 = smoothstep( _FogNoiseEdgeValue , ( _FogNoiseEdgeValue + _FogNoiseEdgeSmoothness ) , saturate( ( simplePerlin3D313 + _NoiseDensityUseAdd ) ));
				float HeightFogWorldPosYClipStep348 = temp_output_310_0;
				float saferPower317 = max( ( smoothstepResult352 * saturate( pow( ( ( HeightFogWorldPosYClipStep348 + _WorldNoiseYMaskClip + WorldPosition.y ) * HeightFogWorldPosYClipStep348 ) , _WorldNoiseYMaskStrength ) ) ) , 0.0001 );
				float FogNoise350 = pow( saferPower317 , _NoiseStrengthUsePower );
				float lerpResult347 = lerp( temp_output_300_0 , ( temp_output_300_0 * FogNoise350 ) , _UseFogNoise);
				float HeightFog301 = lerpResult347;
				float4 lerpResult302 = lerp( temp_output_84_0 , _HeightFogColor , HeightFog301);
				#ifdef _USEHEIGHTFOG_ON
				float4 staticSwitch305 = lerpResult302;
				#else
				float4 staticSwitch305 = temp_output_84_0;
				#endif
				float3 tanNormal90 = Normal43;
				float3 worldNormal90 = float3(dot(tanToWorld0,tanNormal90), dot(tanToWorld1,tanNormal90), dot(tanToWorld2,tanNormal90));
				float dotResult92 = dot( ( ase_worldViewDir + _MainLightPosition.xyz ) , worldNormal90 );
				float smoothstepResult95 = smoothstep( _SpecularSmoothStepMinMax.x , _SpecularSmoothStepMinMax.y , pow( dotResult92 , _SpecularPow ));
				float2 uv_SpecularMap = IN.ase_texcoord2.xy * _SpecularMap_ST.xy + _SpecularMap_ST.zw;
				float4 Specular101 = ( float4( ( ( LightAtt209 * _SpecularDirLightSRPLightIntensity.x ) + ( smoothstepResult114 * _SpecularDirLightSRPLightIntensity.y ) ) , 0.0 ) * ( smoothstepResult95 * _SpecularIntensity ) * _SpecularColor * tex2D( _SpecularMap, uv_SpecularMap ) );
				float2 uv_EmissionTexture = IN.ase_texcoord2.xy * _EmissionTexture_ST.xy + _EmissionTexture_ST.zw;
				float4 temp_output_135_0 = ( ( staticSwitch305 + Specular101 ) + ( tex2D( _EmissionTexture, uv_EmissionTexture ) * _EmissionColor * _EmissionStrength ) );
				float temp_output_431_0 = (1.05 + (_WidthOffset - 0.0) * (-0.3 - 1.05) / (1.0 - 0.0));
				float2 temp_cast_16 = (( temp_output_431_0 + _GlowColorOffset )).xx;
				float2 texCoord424 = IN.ase_texcoord2.xy * float2( 1,1 ) + temp_cast_16;
				float4 lerpResult420 = lerp( ( ( RampFinal360 * float4( SRPLightFinal357 , 0.0 ) ) + ( staticSwitch23_g13 * temp_output_135_0 ) ) , _GrowColor , saturate( texCoord424.y ));
				
				float2 temp_cast_18 = (temp_output_431_0).xx;
				float2 texCoord403 = IN.ase_texcoord2.xy * float2( 1,1 ) + temp_cast_18;
				
				float3 BakedAlbedo = lerpResult420.rgb;
				float3 BakedEmission = 0;
				float Alpha = 1;
				float AlphaClipThreshold = ( _Clip + texCoord403.y );

				#ifdef _ALPHATEST_ON
					clip(Alpha - AlphaClipThreshold);
				#endif

				MetaInput metaInput = (MetaInput)0;
				metaInput.Albedo = BakedAlbedo;
				metaInput.Emission = BakedEmission;
				
				return MetaFragment(metaInput);
			}
			ENDHLSL
		}
		
	}
	CustomEditor "UnityEditor.ShaderGraph.PBRMasterGUI"
	Fallback "Hidden/InternalErrorShader"
	
}
/*ASEBEGIN
Version=18900
0;0;1536;812.6;-3035.774;1826.78;1;True;False
Node;AmplifyShaderEditor.CommentaryNode;273;1882.907,1766.129;Inherit;False;2089.905;846.2246;Comment;13;300;298;296;294;293;290;286;283;274;307;308;310;311;FogHeight;0.3622641,0.7043549,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;46;-3761.005,-154.5279;Inherit;False;1169.146;309.3865;Comment;3;43;42;44;Normal;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;48;-2055.019,621.7488;Inherit;False;506.8718;338.4612;Comment;5;14;15;124;125;158;Light Color & Attenuation;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;312;2769.344,2589.78;Inherit;False;Property;_HeightFogWorldPosYClipAdd;HeightFog WorldPos Y Clip Add;73;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;327;1897.712,3115.82;Inherit;False;1897.938;1035.197;Comment;25;350;349;317;319;341;340;343;336;342;339;337;344;313;316;334;326;314;325;335;281;324;352;354;353;355;Fog 3D Noise Setting;1,0.390566,0.390566,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;159;-2620.927,632.905;Inherit;False;Property;_LightAttValue;Light Att Value(光衰減程度);36;1;[Header];Create;False;1;Light Attenuation;0;0;False;0;False;-0.3;-0.3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;160;-2618.927,713.905;Inherit;False;Property;_LightAttSoft;Light Att Soft(光衰減平滑度);37;0;Create;False;0;0;0;False;0;False;0.46;0.46;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;274;1937.824,2108.304;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleAddOpNode;161;-2361.237,716.5262;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;281;1956.384,3384.579;Inherit;False;Property;_NoiseSpeedWorldPos;Noise Speed(World Pos);79;0;Create;True;1;Fog Noise Setting;0;0;False;0;False;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.StepOpNode;310;3162.62,2495.563;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;324;2042.406,3296.4;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-3711.005,-58.56604;Inherit;False;Property;_NormalStrength;Normal Strength(法線強度);9;0;Create;False;0;0;0;True;0;False;0;3;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.LightColorNode;14;-2162.017,749.4439;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;125;-2186.869,876.6174;Inherit;False;Property;_DirLightIntensity;DirLight Intensity(日光強度);13;1;[Header];Create;False;1;Light;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LightAttenuation;12;-2286.485,621.0951;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;124;-1926.444,773.9294;Inherit;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;42;-3375.967,-104.528;Inherit;True;Property;_NormalMap;Normal Map(法線貼圖);8;2;[Header];[Normal];Create;False;1;Normal;0;0;True;0;False;-1;None;7fc2b053fad84b84a9dbd7b061610e0c;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;325;2329.406,3365.4;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldPosInputsNode;314;1947.712,3553.143;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RegisterLocalVarNode;348;3424.917,2644.231;Inherit;False;HeightFogWorldPosYClipStep;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;158;-1987.824,638.7889;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;349;2148.064,3683.687;Inherit;False;348;HeightFogWorldPosYClipStep;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;15;-1783.546,706.8102;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;47;-1735.329,-142.6868;Inherit;False;1070.12;398.215;Comment;6;45;1;2;3;5;131;Normal Light(N.L);1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;326;2518.406,3528.4;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;43;-2816.659,-103.5414;Inherit;True;Normal;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;52;-1489.389,930.8528;Inherit;False;741.3994;303.9028;Color Remap模塊中自有陰影重映射的Gardient,因此若不先漸變為灰度圖,重映射會不正確;3;17;18;16;轉換為灰度圖;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;316;2359.251,3189.995;Inherit;False;Property;_NoiseScale;Noise Scale;80;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;335;2194.677,3875.565;Inherit;False;Property;_WorldNoiseYMaskClip;世界霧Y軸噪聲衰減極值;85;0;Create;False;0;0;0;False;0;False;-0.59;-0.59;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;313;2654.633,3165.82;Inherit;False;Simplex3D;True;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;334;2474.769,3775.977;Inherit;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;344;2609.273,3270.742;Inherit;False;Property;_NoiseDensityUseAdd;Noise Density(Use Add);81;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;45;-1685.329,-92.68679;Inherit;False;43;Normal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.BreakToComponentsNode;16;-1441.063,980.0394;Inherit;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.CommentaryNode;102;-1652.828,1817.899;Inherit;False;1581.267;646.9999;;12;82;76;74;75;80;81;78;73;77;83;85;189;Rim ;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;17;-1233.066,980.0394;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;1;-1418.154,74.92816;Inherit;False;True;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;339;2706.771,3775.947;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;354;2703.085,3455.214;Inherit;False;Property;_FogNoiseEdgeSmoothness;Fog Noise Edge Smoothness;84;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;2;-1386.231,-86.19307;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;337;2466.055,4011.257;Inherit;False;Property;_WorldNoiseYMaskStrength;世界霧Y軸噪聲衰減強度;86;0;Create;False;0;0;0;False;0;False;-0.59;-0.59;0;1000;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;342;2840.42,3171.456;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;353;2742.012,3377.286;Inherit;False;Property;_FogNoiseEdgeValue;Fog Noise Edge Value;83;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;343;2975.447,3169.345;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;73;-1580.863,1964.308;Inherit;False;World;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.CommentaryNode;208;-3786.049,304.2767;Inherit;False;740.6223;260.14;Comment;2;65;203;SRP Light;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;131;-1191.847,-105.1862;Inherit;False;Property;_DirLightNormalScale;DirLightNormalScale(N.L強度);14;0;Create;False;0;0;0;True;0;False;0.6;0.6;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;355;2984.031,3409.928;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;286;2385.42,2521.612;Inherit;False;Property;_FogHeightStart;Fog Height Start;72;0;Create;True;1;HeightFog Setting;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;283;2215.236,2205.902;Inherit;False;Property;_FogHeightEnd;Fog Height End;71;1;[Header];Create;True;1;Height Fog Setting;0;0;False;0;False;4;4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;336;2918.709,3775.978;Inherit;True;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;74;-1602.828,2137.635;Inherit;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMaxOpNode;18;-983.3943,981.3561;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;3;-1154.231,-17.19307;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;213;-1440.361,367.9636;Inherit;False;835.4715;413.2869;Comment;5;172;166;169;163;170;Celluloid Texture;1,1,1,1;0;0
Node;AmplifyShaderEditor.SmoothstepOpNode;352;3138.053,3169.657;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;340;3167.483,3776.317;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;193;-846.1345,-215.3708;Inherit;False;Property;_RampPower;Ramp Power;21;0;Create;True;0;0;0;False;0;False;1;1.59;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;191;-815.1287,-296.2067;Inherit;False;Property;_RampAddincludePower;Ramp Add(include Power);19;0;Create;True;0;0;0;False;0;False;0;0.62;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;50;-644.6549,-143.2718;Inherit;False;906.9587;494.713;;6;13;7;122;165;190;198;Shadow Remap;0.6320754,0.6320754,0.6320754,1;0;0
Node;AmplifyShaderEditor.FunctionNode;65;-3508.587,452.7984;Inherit;False;SRP Additional Light;-1;;12;6c86746ad131a0a408ca599df5f40861;3,6,1,9,0,23,0;5;2;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;15;FLOAT3;0,0,0;False;14;FLOAT3;1,1,1;False;18;FLOAT;0.5;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;77;-1362.214,2335.58;Inherit;False;Property;_RimOffset;Rim Offset(泛光量值);25;1;[Header];Create;False;1;Rim;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;75;-1363.693,2050.069;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;307;2719.741,2211.42;Inherit;False;Linear Fog;-1;;11;e327318b664bb6b4ca7fb9865de9053d;1,14,1;5;13;FLOAT;700;False;15;FLOAT;0;False;16;FLOAT3;0,0,0;False;17;FLOAT3;0,0,0;False;18;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;199;-1347.544,-2076.739;Inherit;False;1707.054;658.7182;;2;197;6;Ramp Gradient;1,0.9619094,0,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;71;-685.5591,978.8214;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;5;-932.609,-17.07917;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0.5;False;2;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;172;-1390.361,442.9;Inherit;False;0;163;2;3;2;SAMPLER2D;;False;0;FLOAT2;2,2;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;113;-2043.139,2645.438;Inherit;False;927.4;304.3999;Comment;7;108;110;111;114;115;206;356;Specular SRPLight 光源漸變採樣計算;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;170;-1367.16,665.8505;Inherit;False;Property;_RampCelluloidNoiseSoftness;Ramp Celluloid Noise Softness(賽璐璐陰影平滑度);40;0;Create;False;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;192;-538.2935,-223.1223;Inherit;False;True;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;76;-1095.102,2049.899;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;290;3105.59,2191.123;Inherit;False;Property;_NoiseFogSoft;NoiseFog Soft;75;0;Create;True;0;0;0;False;0;False;0.4;0.4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;169;-1368.353,597.6141;Inherit;False;Property;_RampCelluloidNoiseValue;Ramp Celluloid Noise Value(賽璐璐陰影強度);39;0;Create;False;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;296;3143.149,2278.689;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;103;-1931.334,3052.232;Inherit;False;2880.684;912.0061;Comment;17;116;101;93;97;95;90;98;96;87;99;92;89;88;91;100;94;106;Specular;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;196;-627.5509,214.0154;Inherit;False;Property;_RampAddwithoutPower;Ramp Add(without Power);20;0;Create;True;0;0;0;False;0;False;0;-0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;319;3150.634,3587.434;Inherit;False;Property;_NoiseStrengthUsePower;Noise Strength(Use Power);82;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;341;3328.842,3166.96;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;203;-3287.027,354.2767;Inherit;False;SRPAdditionLight;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GradientNode;6;-767.6221,-1781.876;Inherit;False;0;8;2;0.490566,0.490566,0.490566,0;0.4901961,0.4901961,0.4901961,0.2399939;0.6643071,0.6643071,0.6643071,0.2599985;0.6627451,0.6627451,0.6627451,0.4899977;0.8228067,0.8228067,0.8228067,0.5100023;0.8235294,0.8235294,0.8235294,0.7400015;1,1,1,0.7600061;1,1,1,1;1,0;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-667.6294,-14.88554;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;163;-1102.991,417.9636;Inherit;True;Property;_RampCelluloidNoiseTexture;Ramp Celluloid Noise Texture(賽璐璐陰影邊緣噪波貼圖);38;1;[Header];Create;False;1;Celluloid Edge Noise Texture;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;293;3096.849,2115.37;Inherit;False;Property;_NoiseFogClip;NoiseFog Clip;74;0;Create;True;1;Noise Fog Setting;0;0;False;0;False;0.76;0.76;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;317;3460.773,3166.761;Inherit;False;True;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;190;-464.1931,-38.94222;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;91;-1881.334,3484.766;Inherit;False;43;Normal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.Vector2Node;85;-912.0933,2298.396;Inherit;False;Property;_RimSmoothStepMinMax;Rim SmoothStepValue and Smoothness(泛光平滑閾值);27;0;Create;False;0;0;0;True;0;False;0,1;0,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleAddOpNode;294;3293.516,2170.728;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceLightPos;88;-1859.821,3281.149;Inherit;False;0;3;FLOAT4;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.GetLocalVarNode;206;-1946.751,2690.272;Inherit;False;203;SRPAdditionLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;311;3343.414,2274.445;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;78;-853.4456,2049.662;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;49;-725.8527,-998.4443;Inherit;False;546.8332;461.799;Comment;3;8;10;11;Albedo;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;111;-2002.139,2822.438;Inherit;False;Property;_SpecularSRPLightSampler;Specular SRPLight Sampler(高光燈光採樣次數);33;1;[Header];Create;False;1;SRP Addition Lighting Setting;0;0;False;0;False;5;5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;87;-1802.549,3102.232;Inherit;False;World;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RegisterLocalVarNode;197;-496.319,-1786.893;Inherit;False;RampGradient;-1;True;1;0;OBJECT;;False;1;OBJECT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;166;-793.6898,447.1251;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;173;-1077.302,-388.5433;Inherit;True;Property;_IndirectDiffuseLightNormal;Indirect Diffuse Light Normal(漫射光法線貼圖);16;0;Create;False;0;0;0;False;0;False;-1;None;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SmoothstepOpNode;298;3521.73,2097.046;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;10;-670.3519,-943.9666;Inherit;False;Property;_AlbedoEmissionColor;Albedo/Emission  Color(主貼圖/高光顏色);7;2;[HDR];[Header];Create;False;0;0;0;False;0;False;0.3301887,0.3301887,0.3301887,0;2.828427,2.828427,2.828427,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;198;-655.8887,-103.998;Inherit;False;197;RampGradient;1;0;OBJECT;;False;1;OBJECT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;209;-1731.342,1003.499;Inherit;False;LightAtt;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;165;-343.5769,-38.36261;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.31;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;51;-562.3288,-424.9936;Inherit;False;691.1977;124;間接漫射光從Unity的全局照明系統獲取漫射的環境光。這相當於說它檢索周圍的光探測器的信息。;1;24;漫射光照;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;108;-1698.721,2695.526;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldNormalVector;90;-1629.821,3489.149;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.Vector2Node;115;-1691.883,2794.597;Inherit;False;Property;_SpecularSRPLightSmoothstep;Specular SRPLight Smoothstep(高光燈光平滑閾值);34;0;Create;False;0;0;0;False;0;False;0,1;0,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SamplerNode;8;-675.8527,-766.6453;Inherit;True;Property;_AlbedoMap;Albedo Map(主貼圖);6;1;[Header];Create;False;1;Albedo;0;0;True;0;False;-1;None;a229177d1f5a60b4db38a46dcd5502cc;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;89;-1571.821,3207.149;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;80;-655.7444,2051.484;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;350;3624.943,3161.86;Inherit;False;FogNoise;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;189;-597.5825,2325.11;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;94;-1345.334,3456.766;Inherit;False;Property;_SpecularPow;Specular Pow(高光量值);29;0;Create;False;0;0;0;False;0;False;4;4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.IndirectDiffuseLighting;24;-529.329,-380.9936;Inherit;False;Tangent;1;0;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;300;3849.478,1921.38;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GradientSampleNode;7;-182.8084,-100.8572;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;356;-1382.958,2823.208;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;134;-553.49,-292.229;Inherit;False;Property;_DiffuseLightStrength;DiffuseLight Strength(環境光強度(疊加);15;0;Create;False;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;351;4019.522,2061.669;Inherit;False;350;FogNoise;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;122;-171.1407,112.4082;Inherit;True;Property;_ShadowRemapMap;Shadow Remap Map;17;1;[Header];Create;True;1;Shadow Type;0;0;False;0;False;-1;None;5c17639b73e66a64eb1bf87957b3160a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;212;-650.5209,2463.829;Inherit;False;209;LightAtt;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SmoothstepOpNode;81;-492.8645,2052.062;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;210;26.27039,-289.6051;Inherit;False;209;LightAtt;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;121;-919.228,2558.679;Inherit;False;596.8665;382.1125;Comment;3;119;117;120;日光/燈光強度調整;1,1,1,1;0;0
Node;AmplifyShaderEditor.DotProductOpNode;92;-1344.334,3336.766;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;83;-466.9252,1867.899;Inherit;False;Property;_RimColor;Rim Color(泛光顏色);26;1;[HDR];Create;False;0;0;0;False;0;False;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleDivideOpNode;110;-1441.109,2695.438;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;11;-341.4195,-809.9456;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;93;-1146.334,3338.766;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;123;250.7033,15.29237;Inherit;True;Property;_ShadowRemapType;Shadow Remap Type(陰影重映射方式)(左為背光右為迎光);18;0;Create;False;0;0;0;True;0;False;0;1;1;True;;KeywordEnum;2;UseShaderGardientColor;UseShadowRemapMap;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;19;286.3075,-335.1839;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;82;-233.9601,2030.378;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.Vector2Node;96;-1341.25,3537.111;Inherit;False;Property;_SpecularSmoothStepMinMax;Specular SmoothStep Value & Smoothness(高光值與平滑度);31;0;Create;False;0;0;0;False;0;False;0,0.01;0,0.01;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleAddOpNode;133;-257.3873,-370.4496;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;204;-463.7957,-520.6062;Inherit;False;203;SRPAdditionLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;315;4245.834,1954.886;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;346;4490.048,1831.716;Inherit;False;Property;_UseFogNoise;Use Fog Noise;78;2;[Header];[Enum];Create;True;1;Fog Noise Setting;2;Off;0;On;1;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;120;-869.228,2608.679;Inherit;False;Property;_SpecularDirLightSRPLightIntensity;Specular DirLight/SRPLight Intensity(高光受光影響程度 主光源/SRP光源));35;0;Create;False;0;0;0;False;0;False;1,1;1,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SmoothstepOpNode;114;-1252.023,2695.08;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;1,1,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;119;-600.7057,2805.991;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SmoothstepOpNode;95;-928.2501,3339.111;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;117;-484.7615,2614.423;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;72;-142.9164,-538.8558;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;347;4756.258,1917.221;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;51.40301,-802.3359;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;98;-924.3385,3468.558;Inherit;False;Property;_SpecularIntensity;Specular Intensity(高光強度);32;0;Create;False;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;201;-1.40047,2027.621;Inherit;False;Rim;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;667.6438,-159.0945;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;116;-628.0491,3730.579;Inherit;True;Property;_SpecularMap;Specular Map(高光貼圖);28;1;[Header];Create;False;1;Specular;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;26;800.676,-394.5883;Inherit;True;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;301;4996.028,1912.021;Inherit;False;HeightFog;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;107;-197.0791,2785.457;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;106;-567.5364,3555.574;Inherit;False;Property;_SpecularColor;Specular Color(高光顏色);30;1;[HDR];Create;False;0;0;0;True;0;False;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;202;1074.665,-217.0325;Inherit;False;201;Rim;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;97;-540.3385,3339.558;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;99;260.2788,3319.518;Inherit;True;4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;304;1069.214,-799.8181;Inherit;False;301;HeightFog;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;303;1067.724,-628.0547;Inherit;False;Property;_HeightFogColor;HeightFogColor;76;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;84;1227.921,-394.2037;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;302;1364.6,-644.3997;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;101;589.3277,3315.691;Inherit;False;Specular;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;414;3318.729,-1489.921;Inherit;False;Property;_WidthOffset;Width Offset;0;1;[Header];Create;True;1;Vertex Offset;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;140;1687.493,449.0935;Inherit;False;Property;_EmissionStrength;Emission Strength;12;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;139;1588.464,46.05324;Inherit;True;Property;_EmissionTexture;Emission Texture;10;1;[Header];Create;True;1;Emission;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;418;3687.729,-1783.225;Inherit;False;1539.734;678.2131;Comment;18;400;401;402;403;404;405;407;408;412;413;415;410;419;423;424;425;430;431;Vertex Offset;1,1,1,1;0;0
Node;AmplifyShaderEditor.ColorNode;136;1675.038,275.4757;Inherit;False;Property;_EmissionColor;Emission Color;11;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,1;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;305;1641.19,-625.0854;Inherit;False;Property;_UseHeightFog;Use HeightFog ?;77;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;105;1670.549,-140.4306;Inherit;False;101;Specular;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;357;-1046.8,2907.391;Inherit;False;SRPLightFinal;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;402;4077.404,-1685.666;Inherit;False;Property;_CSRange;CSRange;1;0;Create;True;1;Vertex Offset;0;0;False;0;False;0;1.03;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;138;1958.569,221.3665;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.TFHCRemapNode;431;3672.774,-1484.78;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;1.05;False;4;FLOAT;-0.3;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;104;1888.794,-269.9369;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;360;726.4897,195.2701;Inherit;False;RampFinal;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;401;4076.404,-1616.666;Inherit;False;Property;_CSSoft;CSSoft;2;0;Create;True;0;0;0;False;0;False;1;0.03;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;400;3704.618,-1243.527;Inherit;False;Property;_GlowColorOffset;Glow Color Offset;3;0;Create;True;0;0;0;False;0;False;0;-0.79;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;404;4221.403,-1656.666;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;135;2129.364,-269.2554;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;309;4261.665,-801.1899;Inherit;False;360;RampFinal;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;271;3988.562,-442.9611;Inherit;False;SSAO_F;63;;13;146f1dca6fd359a4eba9e48143697ea7;0;0;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;358;4239.044,-653.2905;Inherit;False;357;SRPLightFinal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;403;3993.591,-1530.912;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;425;3874.64,-1380.735;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;408;4414.255,-1703.022;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;270;4315.063,-442.961;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;40;107.4315,462.4314;Inherit;False;1417.653;1005.605;Comment;6;29;39;36;30;28;53;Outline;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;361;4504.667,-670.5522;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;424;4008.611,-1376.832;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NormalVertexDataNode;407;4627.777,-1330.011;Inherit;True;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;70;-1487.409,1328.731;Inherit;False;743.0731;304.7168;Comment;3;67;68;69;轉換為灰度圖;1,1,1,1;0;0
Node;AmplifyShaderEditor.ColorNode;422;4828.074,-759.8188;Inherit;False;Property;_GrowColor;Grow Color;5;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,0;1.112188,0.2999498,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;430;4751.762,-1576.365;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;359;4681.508,-540.5632;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;426;5008.794,-942.1887;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;53;184.4391,1041.779;Inherit;False;629.5074;380.8989;;4;37;31;35;32;頂點位置(物件空間轉裁減空間);1,1,1,1;0;0
Node;AmplifyShaderEditor.OneMinusNode;410;4844.293,-1490.717;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;390;3693.043,548.0008;Inherit;False;1565.582;646.4756;Comment;18;370;371;372;373;374;375;376;377;378;379;380;381;382;383;384;385;386;387;PlayerMask;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;412;4822.517,-1733.225;Inherit;False;Property;_Clip;Clip;4;0;Create;True;0;0;0;False;0;False;0.11;-0.05;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;230;557.0043,-859.4298;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;237;4009.592,-127.6014;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;205;-1763.531,1373.2;Inherit;False;203;SRPAdditionLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PowerNode;376;4470.412,713.7913;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;222;3095.285,-671.8954;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.DitheringNode;234;3291.031,-133.9313;Inherit;False;0;True;4;0;FLOAT;0;False;1;SAMPLER2D;;False;2;FLOAT4;0,0,0,0;False;3;SAMPLERSTATE;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;373;4759.105,885.6867;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;392;4111.592,39.71965;Inherit;False;Property;_PlayerPositionMaskAlpha;PlayerPositionMask Alpha;52;1;[Header];Create;True;1;PlayerPosition Alpha;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;236;2983.754,-229.7363;Inherit;False;Property;_Alpha;Alpha;51;1;[Header];Create;True;1;Alpha Setting;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;259;3660.73,17.61337;Inherit;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;221;3561.303,-824.9984;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;380;4394.441,603.6762;Inherit;False;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;385;4313.142,798.7014;Inherit;False;Property;_Power;Power;55;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;268;2209.646,200.2153;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;228;799.1799,-1033.167;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DistanceOpNode;381;4171.709,715.0043;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;420;5220.676,-715.5579;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.NormalizeNode;382;4400.341,859.1763;Inherit;False;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;30;472.5356,865.4287;Inherit;False;Property;_OutlineWidth;Outline Width(外描邊寬度);22;1;[Header];Create;False;1;Outline;0;0;False;0;False;0.1;0.1;0;0.1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;186;1914.779,-979.6716;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;217;3239.755,-829.9717;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;1;False;4;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;378;4578.411,905.7639;Inherit;False;Property;_BlackClipValue;Black Clip Value;56;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;176;1207.77,-928.9268;Inherit;False;Property;_ClipNoiseScale;ClipNoiseScale(死亡溶解噪點紋理大小);47;0;Create;False;0;0;0;False;0;False;10;10;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;245;3166.48,271.041;Inherit;False;True;2;0;FLOAT;0;False;1;FLOAT;-1.46;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;39;1110.67,509.4182;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;218;3065.057,-1176.426;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;5000;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;69;-979.7379,1382.048;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalVertexDataNode;28;571.3978,713.4045;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;219;3347.101,-1172.889;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;229;1069.004,-1109.43;Inherit;False;Property;_DissolveMode;Dissolve Mode;50;0;Create;True;0;0;0;False;0;False;0;0;0;True;;KeywordEnum;4;UV;WorldSpace;ScreenSpace;ViewSpace;Create;True;True;9;1;FLOAT4;0,0,0,0;False;0;FLOAT4;0,0,0,0;False;2;FLOAT4;0,0,0,0;False;3;FLOAT4;0,0,0,0;False;4;FLOAT4;0,0,0,0;False;5;FLOAT4;0,0,0,0;False;6;FLOAT4;0,0,0,0;False;7;FLOAT4;0,0,0,0;False;8;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;413;5077.352,-1514.482;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;214;2740.056,-836.1557;Inherit;False;Property;_GenerateDither;GenerateDither;42;0;Create;True;0;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TransformPositionNode;387;3947.088,1007.393;Inherit;False;Object;World;False;Fast;True;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ScreenPosInputsNode;235;3075.315,-72.24324;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldPosInputsNode;226;355.0914,-1011.601;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;233;790.5745,-683.6543;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;397;2921.872,265.3072;Inherit;False;Property;_OnlyUseXZAxis;Only Use XZ Axis;62;0;Create;True;0;0;0;False;0;False;0;1;1;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;395;4530.171,5.643555;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;386;3925.735,598.0008;Inherit;False;Property;_PlayerMaskPosition;PlayerMaskPosition;54;0;Create;False;0;0;0;False;0;False;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DistanceOpNode;243;2795.062,428.4039;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;405;4376.789,-1351.178;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;306;1384.467,-752.3165;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;181;2041.184,-1094.568;Inherit;False;Property;_ClipEdgeColor;ClipEdgeColor(死亡溶解顏色);43;1;[HDR];Create;False;0;0;0;False;0;False;7.464264,1.717473,0,0;7.464264,1.717473,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;36;538.6442,529.1975;Inherit;False;Property;_OutlineColor;Outline Color(外描邊顏色);24;1;[HDR];Create;False;0;0;0;False;0;False;0.1981132,0.1981132,0.1981132,0;0.1981132,0.1981132,0.1981132,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;255;4156.695,-190.0572;Inherit;False;Property;_UseCameraDepthClip;Use Camera Depth Clip ?;61;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceCameraPos;242;2353.786,486.8223;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.NoiseGeneratorNode;177;1516.677,-1152.396;Inherit;True;Simple;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;17.21;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;188;325.3442,-1193.885;Inherit;False;Property;_ClipNoiseTiling;ClipNoiseTiling(死亡溶解噪點紋理拉伸);46;0;Create;False;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;263;2632.417,403.5661;Inherit;False;FLOAT2;0;2;2;3;1;0;FLOAT3;0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SmoothstepOpNode;187;2273.031,-1341.66;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0.2;False;2;FLOAT;0.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;371;5069.825,601.6898;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;175;817.5867,-1212.357;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;179;1744.385,-1144.662;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;32;213.6345,1098.211;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;377;4534.241,982.0574;Inherit;False;Property;_BlackClipSmoothness;Black Clip Smoothness;57;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;184;1701.198,-973.901;Inherit;False;Property;_ClipEdgeSoftness;ClipEdgeSoftness(死亡溶解邊緣顏色平滑度);45;0;Create;False;0;0;0;False;0;False;0.01;0.01;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;35;311.7556,1286.288;Inherit;False;Property;_OutlineMax;Outline Max外描邊最大閾值(Accroding to Camera Distance)(根據照相機遠近) ;23;0;Create;False;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;393;4203.153,197.2127;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;215;3049.871,-830.4197;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;247;2910.05,571.1335;Inherit;False;Property;_CamPosDither;CamPos Dither;58;1;[Header];Create;True;1;Camera Dither Setting;0;0;False;0;False;-6;7.08;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;423;4232.202,-1425.613;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceCameraPos;370;3857.015,773.2062;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SaturateNode;372;4305.709,715.0043;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;185;2535.896,-1120.15;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;225;2668.604,-376.3749;Inherit;False;Property;_Sprint;_Sprint(衝刺);49;0;Create;False;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;384;4181.014,604.2062;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldSpaceLightPos;272;3744.491,-1062.506;Inherit;False;0;3;FLOAT4;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.DotProductOpNode;374;4613.497,602.7387;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;419;4976.816,-1236.776;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;37;636.017,1187.584;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;375;3743.043,1013.876;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BreakToComponentsNode;67;-1437.409,1380.731;Inherit;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleAddOpNode;180;1871.077,-1335.514;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;220;3525.141,-1175.459;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;68;-1229.409,1380.731;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TransformPositionNode;227;551.0247,-1016.486;Inherit;False;World;Object;False;Fast;True;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;182;1733.446,-1057.547;Inherit;False;Property;_ClipEdge;ClipEdge(死亡溶解邊緣顏色範圍);44;0;Create;False;0;0;0;False;0;False;0.99;0.99;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;251;3850.566,-324.3606;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;231;790.0043,-859.4298;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;379;4176.84,859.4762;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldPosInputsNode;241;2240.486,31.25961;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DitheringNode;394;4418.869,135.5246;Inherit;False;0;True;4;0;FLOAT;0;False;1;SAMPLER2D;;False;2;FLOAT4;0,0,0,0;False;3;SAMPLERSTATE;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;254;2697.782,-20.8131;Inherit;False;Property;_CamSub;CamSub;59;0;Create;True;0;0;0;False;0;False;0.5;-6;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;415;5003.433,-1693.895;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;262;3872.531,38.09085;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;388;5124.366,-226.7689;Inherit;False;Property;_PlayerMaskOpen;PlayerMask Open;53;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightAttenuation;100;-532.2645,3234.371;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;216;2870.203,-1176.045;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;383;4855.809,603.2572;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;-1;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;389;4989.384,-67.64137;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;232;581.5745,-683.6543;Inherit;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.DistanceOpNode;396;2642.605,147.69;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;183;2779.869,-673.2944;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;791.7612,785.7435;Inherit;True;4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;224;2698.877,-542.126;Inherit;False;Property;_SprintColor;Sprint Color;48;1;[HDR];Create;True;0;0;0;False;0;False;2.297397,0.9640058,0.2072162,0;2.297397,0.9640058,0.2072162,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SwizzleNode;264;2630.135,480.2819;Inherit;False;FLOAT2;0;2;2;3;1;0;FLOAT3;0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;178;1431.006,-1342.442;Inherit;False;Property;_DeadClip;_DeadClip(死亡溶解控制);41;1;[Header];Create;False;1;Dead Dissolve Setting;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;261;3256.077,158.3258;Inherit;False;Property;_NoiseAlpha;Noise Alpha;60;0;Create;True;0;0;0;False;0;False;0;-76;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;308;3720.241,2484.497;Inherit;False;FogTest;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.UnityObjToClipPosHlpNode;31;391.2534,1094.421;Inherit;False;1;0;FLOAT3;0,0,0;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;64;1360.861,-0.1071291;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;Meta;0;4;Meta;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;LightMode=Meta;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;60;4165.43,442.0439;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;ExtraPrePass;0;0;Outline;5;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;True;1;1;False;-1;0;False;-1;1;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;True;1;False;-1;False;True;True;True;True;True;0;False;-1;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;0;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;63;1360.861,-0.1071291;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;DepthOnly;0;3;DepthOnly;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;False;False;True;False;False;False;False;0;False;-1;False;False;False;False;False;False;False;False;False;True;1;False;-1;False;False;True;1;LightMode=DepthOnly;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;61;5555.419,-537.8066;Float;False;True;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;To7/Toon Shader_Branch_GardientAlpha;2992e84f91cbeb14eab234972e07ea9d;True;Forward;0;1;Forward;8;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;True;True;2;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;7;0;False;True;1;1;False;-1;0;False;-1;1;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;True;True;True;True;0;False;-1;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;1;LightMode=UniversalForward;False;2;Include;;False;;Native;Pragma;__SCREEN_SPACE_OCCLUSION;False;;Custom;Hidden/InternalErrorShader;0;0;Standard;22;Surface;0;  Blend;0;Two Sided;0;Cast Shadows;1;  Use Shadow Threshold;0;Receive Shadows;1;GPU Instancing;1;LOD CrossFade;1;Built-in Fog;1;DOTS Instancing;0;Meta Pass;1;Extra Pre Pass;0;Tessellation;1;  Phong;0;  Strength;0.5,False,-1;  Type;0;  Tess;24,False,-1;  Min;10,False,-1;  Max;25,False,-1;  Edge Length;16,False,-1;  Max Displacement;25,False,-1;Vertex Position,InvertActionOnDeselection;1;0;5;False;True;True;True;True;False;;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;62;1360.861,-0.1071291;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;ShadowCaster;0;2;ShadowCaster;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;False;-1;True;3;False;-1;False;True;1;LightMode=ShadowCaster;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
WireConnection;161;0;159;0
WireConnection;161;1;160;0
WireConnection;310;0;274;2
WireConnection;310;1;312;0
WireConnection;124;0;14;1
WireConnection;124;1;14;2
WireConnection;124;2;125;0
WireConnection;42;5;44;0
WireConnection;325;0;324;0
WireConnection;325;1;281;0
WireConnection;348;0;310;0
WireConnection;158;0;12;0
WireConnection;158;1;159;0
WireConnection;158;2;161;0
WireConnection;15;0;158;0
WireConnection;15;1;124;0
WireConnection;326;0;325;0
WireConnection;326;1;314;0
WireConnection;43;0;42;0
WireConnection;313;0;326;0
WireConnection;313;1;316;0
WireConnection;334;0;349;0
WireConnection;334;1;335;0
WireConnection;334;2;314;2
WireConnection;16;0;15;0
WireConnection;17;0;16;0
WireConnection;17;1;16;1
WireConnection;339;0;334;0
WireConnection;339;1;349;0
WireConnection;2;0;45;0
WireConnection;342;0;313;0
WireConnection;342;1;344;0
WireConnection;343;0;342;0
WireConnection;355;0;353;0
WireConnection;355;1;354;0
WireConnection;336;0;339;0
WireConnection;336;1;337;0
WireConnection;18;0;17;0
WireConnection;18;1;16;2
WireConnection;3;0;2;0
WireConnection;3;1;1;0
WireConnection;352;0;343;0
WireConnection;352;1;353;0
WireConnection;352;2;355;0
WireConnection;340;0;336;0
WireConnection;65;2;43;0
WireConnection;75;0;73;0
WireConnection;75;1;74;0
WireConnection;307;13;283;0
WireConnection;307;15;286;0
WireConnection;307;16;274;2
WireConnection;71;0;18;0
WireConnection;5;0;3;0
WireConnection;5;1;131;0
WireConnection;192;0;191;0
WireConnection;192;1;193;0
WireConnection;76;0;75;0
WireConnection;76;1;77;0
WireConnection;296;0;307;0
WireConnection;341;0;352;0
WireConnection;341;1;340;0
WireConnection;203;0;65;0
WireConnection;13;0;5;0
WireConnection;13;1;71;0
WireConnection;163;1;172;0
WireConnection;317;0;341;0
WireConnection;317;1;319;0
WireConnection;190;0;192;0
WireConnection;190;1;13;0
WireConnection;190;2;196;0
WireConnection;294;0;293;0
WireConnection;294;1;290;0
WireConnection;311;0;296;0
WireConnection;311;1;310;0
WireConnection;78;0;76;0
WireConnection;197;0;6;0
WireConnection;166;0;163;1
WireConnection;166;1;169;0
WireConnection;166;2;170;0
WireConnection;298;0;311;0
WireConnection;298;1;293;0
WireConnection;298;2;294;0
WireConnection;209;0;15;0
WireConnection;165;0;190;0
WireConnection;165;1;166;0
WireConnection;108;0;206;0
WireConnection;108;1;111;0
WireConnection;90;0;91;0
WireConnection;89;0;87;0
WireConnection;89;1;88;1
WireConnection;80;0;78;0
WireConnection;350;0;317;0
WireConnection;189;0;85;1
WireConnection;189;1;85;2
WireConnection;24;0;173;0
WireConnection;300;0;311;0
WireConnection;300;1;298;0
WireConnection;7;0;198;0
WireConnection;7;1;165;0
WireConnection;356;0;115;1
WireConnection;356;1;115;2
WireConnection;122;1;165;0
WireConnection;81;0;80;0
WireConnection;81;1;85;1
WireConnection;81;2;189;0
WireConnection;92;0;89;0
WireConnection;92;1;90;0
WireConnection;110;0;108;0
WireConnection;110;1;111;0
WireConnection;11;0;10;0
WireConnection;11;1;8;0
WireConnection;93;0;92;0
WireConnection;93;1;94;0
WireConnection;123;1;7;0
WireConnection;123;0;122;0
WireConnection;19;0;11;0
WireConnection;19;1;210;0
WireConnection;82;0;83;0
WireConnection;82;1;81;0
WireConnection;82;2;212;0
WireConnection;133;0;24;0
WireConnection;133;1;134;0
WireConnection;315;0;300;0
WireConnection;315;1;351;0
WireConnection;114;0;110;0
WireConnection;114;1;115;1
WireConnection;114;2;356;0
WireConnection;119;0;114;0
WireConnection;119;1;120;2
WireConnection;95;0;93;0
WireConnection;95;1;96;1
WireConnection;95;2;96;2
WireConnection;117;0;212;0
WireConnection;117;1;120;1
WireConnection;72;0;11;0
WireConnection;72;1;204;0
WireConnection;347;0;300;0
WireConnection;347;1;315;0
WireConnection;347;2;346;0
WireConnection;25;0;11;0
WireConnection;25;1;133;0
WireConnection;201;0;82;0
WireConnection;9;0;19;0
WireConnection;9;1;123;0
WireConnection;26;0;25;0
WireConnection;26;1;9;0
WireConnection;26;2;72;0
WireConnection;301;0;347;0
WireConnection;107;0;117;0
WireConnection;107;1;119;0
WireConnection;97;0;95;0
WireConnection;97;1;98;0
WireConnection;99;0;107;0
WireConnection;99;1;97;0
WireConnection;99;2;106;0
WireConnection;99;3;116;0
WireConnection;84;0;26;0
WireConnection;84;1;202;0
WireConnection;302;0;84;0
WireConnection;302;1;303;0
WireConnection;302;2;304;0
WireConnection;101;0;99;0
WireConnection;305;1;84;0
WireConnection;305;0;302;0
WireConnection;357;0;114;0
WireConnection;138;0;139;0
WireConnection;138;1;136;0
WireConnection;138;2;140;0
WireConnection;431;0;414;0
WireConnection;104;0;305;0
WireConnection;104;1;105;0
WireConnection;360;0;123;0
WireConnection;404;0;402;0
WireConnection;404;1;401;0
WireConnection;135;0;104;0
WireConnection;135;1;138;0
WireConnection;403;1;431;0
WireConnection;425;0;431;0
WireConnection;425;1;400;0
WireConnection;408;0;403;2
WireConnection;408;1;402;0
WireConnection;408;2;404;0
WireConnection;270;0;271;0
WireConnection;270;1;135;0
WireConnection;361;0;309;0
WireConnection;361;1;358;0
WireConnection;424;1;425;0
WireConnection;430;0;408;0
WireConnection;359;0;361;0
WireConnection;359;1;270;0
WireConnection;426;0;424;2
WireConnection;410;0;403;2
WireConnection;237;0;234;0
WireConnection;237;1;262;0
WireConnection;376;0;381;0
WireConnection;376;1;385;0
WireConnection;222;0;183;0
WireConnection;222;1;224;0
WireConnection;222;2;225;0
WireConnection;234;0;236;0
WireConnection;234;2;235;0
WireConnection;373;0;378;0
WireConnection;373;1;377;0
WireConnection;259;0;218;0
WireConnection;259;1;245;0
WireConnection;259;2;261;0
WireConnection;221;0;220;0
WireConnection;221;1;180;0
WireConnection;380;0;384;0
WireConnection;228;0;188;0
WireConnection;228;1;226;0
WireConnection;381;0;386;0
WireConnection;381;1;387;0
WireConnection;420;0;359;0
WireConnection;420;1;422;0
WireConnection;420;2;426;0
WireConnection;382;0;379;0
WireConnection;186;0;182;0
WireConnection;186;1;184;0
WireConnection;217;0;215;0
WireConnection;245;0;397;0
WireConnection;245;1;247;0
WireConnection;39;0;26;0
WireConnection;39;1;36;0
WireConnection;218;0;216;0
WireConnection;69;0;68;0
WireConnection;69;1;67;2
WireConnection;219;0;218;0
WireConnection;219;1;217;0
WireConnection;229;1;175;0
WireConnection;229;0;228;0
WireConnection;229;2;231;0
WireConnection;229;3;233;0
WireConnection;413;0;430;0
WireConnection;413;1;410;0
WireConnection;413;2;407;0
WireConnection;387;0;375;0
WireConnection;233;0;188;0
WireConnection;233;1;232;0
WireConnection;397;1;396;0
WireConnection;397;0;243;0
WireConnection;395;0;218;0
WireConnection;395;1;392;0
WireConnection;243;0;263;0
WireConnection;243;1;264;0
WireConnection;405;0;423;0
WireConnection;405;1;400;0
WireConnection;306;0;304;0
WireConnection;306;1;303;0
WireConnection;255;1;234;0
WireConnection;255;0;237;0
WireConnection;177;0;229;0
WireConnection;177;1;176;0
WireConnection;263;0;241;0
WireConnection;187;0;180;0
WireConnection;187;1;182;0
WireConnection;187;2;186;0
WireConnection;371;0;376;0
WireConnection;371;1;378;0
WireConnection;371;2;373;0
WireConnection;175;0;188;0
WireConnection;179;0;177;0
WireConnection;215;0;214;0
WireConnection;423;0;424;2
WireConnection;372;0;381;0
WireConnection;185;0;187;0
WireConnection;185;1;181;0
WireConnection;384;0;386;0
WireConnection;384;1;370;0
WireConnection;374;0;380;0
WireConnection;374;1;382;0
WireConnection;419;0;408;0
WireConnection;419;1;423;0
WireConnection;37;0;31;4
WireConnection;37;1;35;0
WireConnection;67;0;205;0
WireConnection;180;0;178;0
WireConnection;180;1;179;0
WireConnection;220;0;219;0
WireConnection;68;0;67;0
WireConnection;68;1;67;1
WireConnection;227;0;226;0
WireConnection;251;0;135;0
WireConnection;231;0;188;0
WireConnection;231;1;230;0
WireConnection;379;0;387;0
WireConnection;379;1;370;0
WireConnection;394;0;392;0
WireConnection;394;2;393;0
WireConnection;415;0;412;0
WireConnection;415;1;403;2
WireConnection;262;0;259;0
WireConnection;388;1;255;0
WireConnection;388;0;389;0
WireConnection;383;0;376;0
WireConnection;389;0;255;0
WireConnection;389;1;395;0
WireConnection;389;2;371;0
WireConnection;396;0;241;0
WireConnection;396;1;242;0
WireConnection;183;0;185;0
WireConnection;183;1;135;0
WireConnection;29;0;28;0
WireConnection;29;1;30;0
WireConnection;29;2;8;4
WireConnection;29;3;37;0
WireConnection;264;0;242;0
WireConnection;308;0;311;0
WireConnection;31;0;32;0
WireConnection;61;0;420;0
WireConnection;61;2;420;0
WireConnection;61;4;415;0
WireConnection;61;5;413;0
ASEEND*/
//CHKSM=970AA0AAB9CC1EFF14397E82AE45B2BD9DF46C63