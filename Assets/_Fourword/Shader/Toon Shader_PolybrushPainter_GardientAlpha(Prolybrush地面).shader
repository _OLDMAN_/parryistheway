// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "To7/Toon Shader_PolybrushPainter_GardientAlpha"
{
	Properties
	{
		[HideInInspector] _AlphaCutoff("Alpha Cutoff ", Range(0, 1)) = 0.5
		[ASEBegin][Header(Albedo)]_AlbedoTexture1("Albedo Map 1(主貼圖)", 2D) = "white" {}
		[HDR]_AlbedoTexture1Color("AlbedoTexture1Color", Color) = (1,1,1,1)
		[Header(Albedo)]_AlbedoTexture2("Albedo Map 2(筆刷1)", 2D) = "white" {}
		[HDR]_AlbedoTexture2Color("AlbedoTexture2Color", Color) = (1,1,1,1)
		[Header(Albedo)]_AlbedoTexture3("Albedo Map 3(筆刷2)", 2D) = "white" {}
		[HDR]_AlbedoTexture3Color("AlbedoTexture3Color", Color) = (1,1,1,1)
		[Header(Albedo)]_AlbedoTexture4("Albedo Map 4(筆刷3)", 2D) = "white" {}
		[HDR]_AlbedoTexture4Color("AlbedoTexture4Color", Color) = (1,1,1,1)
		[HDR]_AlbedoEmissionColor("Albedo/Emission  Color(主貼圖/高光顏色)", Color) = (0.3301887,0.3301887,0.3301887,0)
		[Header(Normal)][Normal]_NormalMap1("Bump", 2D) = "bump" {}
		[Header(Normal)][Normal]_NormalMap("Normal Map(法線貼圖)", 2D) = "bump" {}
		_NormalStrength("Normal Strength(法線強度)", Range( 0 , 2)) = 0
		[Header(Emission)]_EmissionTexture("Emission Texture", 2D) = "white" {}
		[HDR]_EmissionColor("Emission Color", Color) = (0,0,0,1)
		_EmissionStrength("Emission Strength", Float) = 1
		[Header(Light)]_DirLightIntensity("DirLight Intensity(日光強度)", Float) = 1
		_DirLightNormalScale("DirLightNormalScale(N.L強度)", Float) = 0.6
		_DiffuseLightStrength("DiffuseLight Strength(環境光強度(疊加)", Float) = 0
		_IndirectDiffuseLightNormal("Indirect Diffuse Light Normal(漫射光法線貼圖)", 2D) = "bump" {}
		[Header(Shadow Type)]_ShadowRemapMap("Shadow Remap Map", 2D) = "white" {}
		[KeywordEnum(UseShaderGardientColor,UseShadowRemapMap)] _ShadowRemapType("Shadow Remap Type(陰影重映射方式)(左為背光右為迎光)", Float) = 1
		_RampAddincludePower("Ramp Add(include Power)", Float) = 0
		_RampAddwithoutPower("Ramp Add(without Power)", Float) = 0
		_RampPower("Ramp Power", Range( 0 , 3)) = 1
		[Header(Rim)]_RimOffset("Rim Offset(泛光量值)", Float) = 0
		[HDR]_RimColor("Rim Color(泛光顏色)", Color) = (0,0,0,0)
		_RimSmoothStepMinMax("Rim SmoothStepValue and Smoothness(泛光平滑閾值)", Vector) = (0,1,0,0)
		[Header(Specular)]_SpecularMap("Specular Map(高光貼圖)", 2D) = "white" {}
		_SpecularPow("Specular Pow(高光量值)", Float) = 4
		[HDR]_SpecularColor("Specular Color(高光顏色)", Color) = (1,1,1,0)
		_SpecularSmoothStepMinMax("Specular SmoothStep Value & Smoothness(高光值與平滑度)", Vector) = (0,0.01,0,0)
		_SpecularIntensity("Specular Intensity(高光強度)", Range( 0 , 1)) = 0
		_SpecularSRPLightSampler("Specular SRPLight Sampler(高光燈光採樣次數)", Float) = 5
		_SpecularSRPLightSmoothstep("Specular SRPLight Smoothstep(高光燈光平滑閾值)", Vector) = (0,1,0,0)
		_SpecularDirLightSRPLightIntensity("Specular DirLight/SRPLight Intensity(高光受光影響程度 主光源/SRP光源))", Vector) = (1,1,0,0)
		[Header(Light Attenuation)]_LightAttValue("Light Att Value(光衰減程度)", Float) = -0.3
		_LightAttSoft("Light Att Soft(光衰減平滑度)", Float) = 0.46
		[Header(Celluloid Edge Noise Texture)]_RampCelluloidNoiseTexture("Ramp Celluloid Noise Texture(賽璐璐陰影邊緣噪波貼圖)", 2D) = "white" {}
		_RampCelluloidNoiseValue("Ramp Celluloid Noise Value(賽璐璐陰影強度)", Float) = 0
		_RampCelluloidNoiseSoftness("Ramp Celluloid Noise Softness(賽璐璐陰影平滑度)", Float) = 1
		_Alpha("Alpha", Range( 0 , 1)) = 1
		[Toggle(_SHADOWUSEDITHER_ON)] _ShadowUseDither("Shadow Use Dither(陰影隨溶解消失)", Float) = 0
		[HDR][Header(Fog)]_FogColor("FogColor", Color) = (1,1,1,0)
		_FogStart("FogStart", Float) = 0
		[Header(Linear Fog Setting)]_FogEnd("FogEnd", Float) = 500
		[Header(SSAO)]_AOMix("AO Mix", Range( 0 , 1)) = 0
		[Toggle(_USESSAO_ON)] _UseSSAO("Use SSAO", Float) = 0
		_AOSmoothness("AO Smoothness", Range( 0 , 1)) = 1
		_AOThresold("AO Thresold", Range( 0 , 1)) = 0
		[HDR]_AOColour("AO Colour", Color) = (0,0,0,0)
		[HDR]_BassColor("BassColor", Color) = (1,1,1,1)
		[ASEEnd][HDR]_Color0("Color 0", Color) = (1,1,1,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

		//_TessPhongStrength( "Tess Phong Strength", Range( 0, 1 ) ) = 0.5
		//_TessValue( "Tess Max Tessellation", Range( 1, 32 ) ) = 16
		//_TessMin( "Tess Min Distance", Float ) = 10
		//_TessMax( "Tess Max Distance", Float ) = 25
		//_TessEdgeLength ( "Tess Edge length", Range( 2, 50 ) ) = 16
		//_TessMaxDisp( "Tess Max Displacement", Float ) = 25
	}

	SubShader
	{
		LOD 0

		
		Tags { "RenderPipeline"="UniversalPipeline" "RenderType"="Transparent" "Queue"="AlphaTest" }
		
		Cull Off
		AlphaToMask Off
		HLSLINCLUDE
		#pragma target 5.0

		#ifndef ASE_TESS_FUNCS
		#define ASE_TESS_FUNCS
		float4 FixedTess( float tessValue )
		{
			return tessValue;
		}
		
		float CalcDistanceTessFactor (float4 vertex, float minDist, float maxDist, float tess, float4x4 o2w, float3 cameraPos )
		{
			float3 wpos = mul(o2w,vertex).xyz;
			float dist = distance (wpos, cameraPos);
			float f = clamp(1.0 - (dist - minDist) / (maxDist - minDist), 0.01, 1.0) * tess;
			return f;
		}

		float4 CalcTriEdgeTessFactors (float3 triVertexFactors)
		{
			float4 tess;
			tess.x = 0.5 * (triVertexFactors.y + triVertexFactors.z);
			tess.y = 0.5 * (triVertexFactors.x + triVertexFactors.z);
			tess.z = 0.5 * (triVertexFactors.x + triVertexFactors.y);
			tess.w = (triVertexFactors.x + triVertexFactors.y + triVertexFactors.z) / 3.0f;
			return tess;
		}

		float CalcEdgeTessFactor (float3 wpos0, float3 wpos1, float edgeLen, float3 cameraPos, float4 scParams )
		{
			float dist = distance (0.5 * (wpos0+wpos1), cameraPos);
			float len = distance(wpos0, wpos1);
			float f = max(len * scParams.y / (edgeLen * dist), 1.0);
			return f;
		}

		float DistanceFromPlane (float3 pos, float4 plane)
		{
			float d = dot (float4(pos,1.0f), plane);
			return d;
		}

		bool WorldViewFrustumCull (float3 wpos0, float3 wpos1, float3 wpos2, float cullEps, float4 planes[6] )
		{
			float4 planeTest;
			planeTest.x = (( DistanceFromPlane(wpos0, planes[0]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[0]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[0]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.y = (( DistanceFromPlane(wpos0, planes[1]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[1]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[1]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.z = (( DistanceFromPlane(wpos0, planes[2]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[2]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[2]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.w = (( DistanceFromPlane(wpos0, planes[3]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[3]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[3]) > -cullEps) ? 1.0f : 0.0f );
			return !all (planeTest);
		}

		float4 DistanceBasedTess( float4 v0, float4 v1, float4 v2, float tess, float minDist, float maxDist, float4x4 o2w, float3 cameraPos )
		{
			float3 f;
			f.x = CalcDistanceTessFactor (v0,minDist,maxDist,tess,o2w,cameraPos);
			f.y = CalcDistanceTessFactor (v1,minDist,maxDist,tess,o2w,cameraPos);
			f.z = CalcDistanceTessFactor (v2,minDist,maxDist,tess,o2w,cameraPos);

			return CalcTriEdgeTessFactors (f);
		}

		float4 EdgeLengthBasedTess( float4 v0, float4 v1, float4 v2, float edgeLength, float4x4 o2w, float3 cameraPos, float4 scParams )
		{
			float3 pos0 = mul(o2w,v0).xyz;
			float3 pos1 = mul(o2w,v1).xyz;
			float3 pos2 = mul(o2w,v2).xyz;
			float4 tess;
			tess.x = CalcEdgeTessFactor (pos1, pos2, edgeLength, cameraPos, scParams);
			tess.y = CalcEdgeTessFactor (pos2, pos0, edgeLength, cameraPos, scParams);
			tess.z = CalcEdgeTessFactor (pos0, pos1, edgeLength, cameraPos, scParams);
			tess.w = (tess.x + tess.y + tess.z) / 3.0f;
			return tess;
		}

		float4 EdgeLengthBasedTessCull( float4 v0, float4 v1, float4 v2, float edgeLength, float maxDisplacement, float4x4 o2w, float3 cameraPos, float4 scParams, float4 planes[6] )
		{
			float3 pos0 = mul(o2w,v0).xyz;
			float3 pos1 = mul(o2w,v1).xyz;
			float3 pos2 = mul(o2w,v2).xyz;
			float4 tess;

			if (WorldViewFrustumCull(pos0, pos1, pos2, maxDisplacement, planes))
			{
				tess = 0.0f;
			}
			else
			{
				tess.x = CalcEdgeTessFactor (pos1, pos2, edgeLength, cameraPos, scParams);
				tess.y = CalcEdgeTessFactor (pos2, pos0, edgeLength, cameraPos, scParams);
				tess.z = CalcEdgeTessFactor (pos0, pos1, edgeLength, cameraPos, scParams);
				tess.w = (tess.x + tess.y + tess.z) / 3.0f;
			}
			return tess;
		}
		#endif //ASE_TESS_FUNCS

		ENDHLSL

		
		Pass
		{
			
			Name "Forward"
			Tags { "LightMode"="UniversalForward" }
			
			Blend One Zero, One Zero
			ZWrite On
			ZTest LEqual
			Offset 0 , 0
			ColorMask RGBA
			

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#define _ALPHATEST_ON 1
			#define ASE_SRP_VERSION 100302

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/UnityInstancing.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

			#if ASE_SRP_VERSION <= 70108
			#define REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR
			#endif

			#include "Packages/com.unity.shadergraph/ShaderGraphLibrary/Functions.hlsl"
			#define ASE_NEEDS_VERT_NORMAL
			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#define ASE_NEEDS_FRAG_SHADOWCOORDS
			#define ASE_NEEDS_FRAG_COLOR
			#pragma shader_feature_local _SHADOWREMAPTYPE_USESHADERGARDIENTCOLOR _SHADOWREMAPTYPE_USESHADOWREMAPMAP
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma shader_feature_local _USESSAO_ON
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma shader_feature_local _SHADOWUSEDITHER_ON
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_color : COLOR;
				float4 ase_tangent : TANGENT;
				float4 texcoord1 : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				#ifdef ASE_FOG
				float fogFactor : TEXCOORD2;
				#endif
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_color : COLOR;
				float4 ase_texcoord4 : TEXCOORD4;
				float4 ase_texcoord5 : TEXCOORD5;
				float4 ase_texcoord6 : TEXCOORD6;
				float4 lightmapUVOrVertexSH : TEXCOORD7;
				float4 ase_texcoord8 : TEXCOORD8;
				float4 ase_texcoord9 : TEXCOORD9;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _AlbedoTexture1Color;
			float4 _AlbedoTexture4_ST;
			float4 _AlbedoTexture3Color;
			float4 _AlbedoTexture3_ST;
			float4 _AlbedoTexture2Color;
			float4 _AlbedoTexture2_ST;
			float4 _RimColor;
			float4 _AlbedoTexture1_ST;
			float4 _AlbedoEmissionColor;
			float4 _Color0;
			float4 _AOColour;
			float4 _AlbedoTexture4Color;
			float4 _BassColor;
			float4 _SpecularMap_ST;
			float4 _NormalMap_ST;
			float4 _FogColor;
			float4 _NormalMap1_ST;
			float4 _EmissionColor;
			float4 _EmissionTexture_ST;
			float4 _RampCelluloidNoiseTexture_ST;
			float4 _IndirectDiffuseLightNormal_ST;
			float4 _SpecularColor;
			float2 _SpecularDirLightSRPLightIntensity;
			float2 _SpecularSmoothStepMinMax;
			float2 _RimSmoothStepMinMax;
			float2 _SpecularSRPLightSmoothstep;
			float _RimOffset;
			float _EmissionStrength;
			float _SpecularIntensity;
			float _SpecularPow;
			float _FogEnd;
			float _RampAddincludePower;
			float _FogStart;
			float _AOMix;
			float _AOSmoothness;
			float _AOThresold;
			float _SpecularSRPLightSampler;
			float _RampCelluloidNoiseSoftness;
			float _RampCelluloidNoiseValue;
			float _RampAddwithoutPower;
			float _DirLightIntensity;
			float _LightAttSoft;
			float _LightAttValue;
			float _DirLightNormalScale;
			float _NormalStrength;
			float _RampPower;
			float _DiffuseLightStrength;
			float _Alpha;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			sampler2D _ShadowRemapMap;
			sampler2D _NormalMap;
			sampler2D _NormalMap1;
			sampler2D _RampCelluloidNoiseTexture;
			sampler2D _AlbedoTexture1;
			sampler2D _AlbedoTexture2;
			sampler2D _AlbedoTexture3;
			sampler2D _AlbedoTexture4;
			sampler2D _IndirectDiffuseLightNormal;
			sampler2D _SpecularMap;
			sampler2D _EmissionTexture;


			
			float4 SampleGradient( Gradient gradient, float time )
			{
				float3 color = gradient.colors[0].rgb;
				UNITY_UNROLL
				for (int c = 1; c < 8; c++)
				{
				float colorPos = saturate((time - gradient.colors[c-1].w) / ( 0.00001 + (gradient.colors[c].w - gradient.colors[c-1].w)) * step(c, gradient.colorsLength-1));
				color = lerp(color, gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), gradient.type));
				}
				#ifndef UNITY_COLORSPACE_GAMMA
				color = SRGBToLinear(color);
				#endif
				float alpha = gradient.alphas[0].x;
				UNITY_UNROLL
				for (int a = 1; a < 8; a++)
				{
				float alphaPos = saturate((time - gradient.alphas[a-1].y) / ( 0.00001 + (gradient.alphas[a].y - gradient.alphas[a-1].y)) * step(a, gradient.alphasLength-1));
				alpha = lerp(alpha, gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), gradient.type));
				}
				return float4(color, alpha);
			}
			
			float3 AdditionalLightsLambert( float3 WorldPosition, float3 WorldNormal )
			{
				float3 Color = 0;
				#ifdef _ADDITIONAL_LIGHTS
				int numLights = GetAdditionalLightsCount();
				for(int i = 0; i<numLights;i++)
				{
					Light light = GetAdditionalLight(i, WorldPosition);
					half3 AttLightColor = light.color *(light.distanceAttenuation * light.shadowAttenuation);
					Color +=LightingLambert(AttLightColor, light.direction, WorldNormal);
					
				}
				#endif
				return Color;
			}
			
			float3 ASEIndirectDiffuse( float2 uvStaticLightmap, float3 normalWS )
			{
			#ifdef LIGHTMAP_ON
				return SampleLightmap( uvStaticLightmap, normalWS );
			#else
				return SampleSH(normalWS);
			#endif
			}
			
			half SampleAO17_g6( half2 In0 )
			{
				return SampleAmbientOcclusion(In0);
			}
			
			inline float Dither4x4Bayer( int x, int y )
			{
				const float dither[ 16 ] = {
			 1,  9,  3, 11,
			13,  5, 15,  7,
			 4, 12,  2, 10,
			16,  8, 14,  6 };
				int r = y * 4 + x;
				return dither[r] / 16; // same # of instructions as pre-dividing due to compiler magic
			}
			
			
			VertexOutput VertexFunction ( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float3 ase_worldTangent = TransformObjectToWorldDir(v.ase_tangent.xyz);
				o.ase_texcoord4.xyz = ase_worldTangent;
				float3 ase_worldNormal = TransformObjectToWorldNormal(v.ase_normal);
				o.ase_texcoord5.xyz = ase_worldNormal;
				float ase_vertexTangentSign = v.ase_tangent.w * unity_WorldTransformParams.w;
				float3 ase_worldBitangent = cross( ase_worldNormal, ase_worldTangent ) * ase_vertexTangentSign;
				o.ase_texcoord6.xyz = ase_worldBitangent;
				OUTPUT_LIGHTMAP_UV( v.texcoord1, unity_LightmapST, o.lightmapUVOrVertexSH.xy );
				OUTPUT_SH( ase_worldNormal, o.lightmapUVOrVertexSH.xyz );
				
				float4 ase_clipPos = TransformObjectToHClip((v.vertex).xyz);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord9 = screenPos;
				
				o.ase_texcoord3.xy = v.ase_texcoord.xy;
				o.ase_color = v.ase_color;
				o.ase_texcoord8 = v.vertex;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord3.zw = 0;
				o.ase_texcoord4.w = 0;
				o.ase_texcoord5.w = 0;
				o.ase_texcoord6.w = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = defaultVertexValue;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif
				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );
				float4 positionCS = TransformWorldToHClip( positionWS );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				VertexPositionInputs vertexInput = (VertexPositionInputs)0;
				vertexInput.positionWS = positionWS;
				vertexInput.positionCS = positionCS;
				o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				#ifdef ASE_FOG
				o.fogFactor = ComputeFogFactor( positionCS.z );
				#endif
				o.clipPos = positionCS;
				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_color : COLOR;
				float4 ase_tangent : TANGENT;
				float4 texcoord1 : TEXCOORD1;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord = v.ase_texcoord;
				o.ase_color = v.ase_color;
				o.ase_tangent = v.ase_tangent;
				o.texcoord1 = v.texcoord1;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				o.ase_color = patch[0].ase_color * bary.x + patch[1].ase_color * bary.y + patch[2].ase_color * bary.z;
				o.ase_tangent = patch[0].ase_tangent * bary.x + patch[1].ase_tangent * bary.y + patch[2].ase_tangent * bary.z;
				o.texcoord1 = patch[0].texcoord1 * bary.x + patch[1].texcoord1 * bary.y + patch[2].texcoord1 * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag ( VertexOutput IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif
				float saferPower192 = max( _RampAddincludePower , 0.0001 );
				float2 uv_NormalMap = IN.ase_texcoord3.xy * _NormalMap_ST.xy + _NormalMap_ST.zw;
				float3 unpack42 = UnpackNormalScale( tex2D( _NormalMap, uv_NormalMap ), _NormalStrength );
				unpack42.z = lerp( 1, unpack42.z, saturate(_NormalStrength) );
				float2 uv_NormalMap1 = IN.ase_texcoord3.xy * _NormalMap1_ST.xy + _NormalMap1_ST.zw;
				float3 lerpResult263 = lerp( unpack42 , UnpackNormalScale( tex2D( _NormalMap1, uv_NormalMap1 ), 1.0f ) , IN.ase_color.r);
				float3 Normal43 = lerpResult263;
				float3 ase_worldTangent = IN.ase_texcoord4.xyz;
				float3 ase_worldNormal = IN.ase_texcoord5.xyz;
				float3 ase_worldBitangent = IN.ase_texcoord6.xyz;
				float3 tanToWorld0 = float3( ase_worldTangent.x, ase_worldBitangent.x, ase_worldNormal.x );
				float3 tanToWorld1 = float3( ase_worldTangent.y, ase_worldBitangent.y, ase_worldNormal.y );
				float3 tanToWorld2 = float3( ase_worldTangent.z, ase_worldBitangent.z, ase_worldNormal.z );
				float3 tanNormal2 = Normal43;
				float3 worldNormal2 = float3(dot(tanToWorld0,tanNormal2), dot(tanToWorld1,tanNormal2), dot(tanToWorld2,tanNormal2));
				float dotResult3 = dot( worldNormal2 , SafeNormalize(_MainLightPosition.xyz) );
				float ase_lightAtten = 0;
				Light ase_lightAtten_mainLight = GetMainLight( ShadowCoords );
				ase_lightAtten = ase_lightAtten_mainLight.distanceAttenuation * ase_lightAtten_mainLight.shadowAttenuation;
				float smoothstepResult158 = smoothstep( _LightAttValue , ( _LightAttValue + _LightAttSoft ) , ase_lightAtten);
				float3 temp_output_15_0 = ( smoothstepResult158 * ( _MainLightColor.rgb * _MainLightColor.a * _DirLightIntensity ) );
				float3 break16 = temp_output_15_0;
				float2 uv_RampCelluloidNoiseTexture = IN.ase_texcoord3.xy * _RampCelluloidNoiseTexture_ST.xy + _RampCelluloidNoiseTexture_ST.zw;
				float smoothstepResult166 = smoothstep( _RampCelluloidNoiseValue , _RampCelluloidNoiseSoftness , tex2D( _RampCelluloidNoiseTexture, uv_RampCelluloidNoiseTexture ).r);
				float temp_output_165_0 = ( ( pow( saferPower192 , _RampPower ) + ( (dotResult3*_DirLightNormalScale + 0.5) * ( max( max( break16.x , break16.y ) , break16.z ) + 0.0 ) ) + _RampAddwithoutPower ) * smoothstepResult166 );
				float2 temp_cast_0 = (temp_output_165_0).xx;
				Gradient gradient6 = NewGradient( 0, 3, 2, float4( 0.9150943, 0.4437344, 0.4437344, 0.2515908 ), float4( 0.9811321, 0.6723437, 0.4665005, 0.6992905 ), float4( 0.9853496, 1, 0.6264151, 1 ), 0, 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
				float2 temp_cast_1 = (temp_output_165_0).xx;
				#if defined(_SHADOWREMAPTYPE_USESHADERGARDIENTCOLOR)
				float4 staticSwitch123 = SampleGradient( gradient6, temp_output_165_0 );
				#elif defined(_SHADOWREMAPTYPE_USESHADOWREMAPMAP)
				float4 staticSwitch123 = tex2D( _ShadowRemapMap, temp_cast_0 );
				#else
				float4 staticSwitch123 = tex2D( _ShadowRemapMap, temp_cast_0 );
				#endif
				float4 RampFinal283 = staticSwitch123;
				float3 temp_cast_2 = (_SpecularSRPLightSmoothstep.x).xxx;
				float3 temp_cast_3 = (( _SpecularSRPLightSmoothstep.x + _SpecularSRPLightSmoothstep.y )).xxx;
				float3 WorldPosition5_g5 = WorldPosition;
				float3 tanNormal12_g5 = Normal43;
				float3 worldNormal12_g5 = float3(dot(tanToWorld0,tanNormal12_g5), dot(tanToWorld1,tanNormal12_g5), dot(tanToWorld2,tanNormal12_g5));
				float3 WorldNormal5_g5 = worldNormal12_g5;
				float3 localAdditionalLightsLambert5_g5 = AdditionalLightsLambert( WorldPosition5_g5 , WorldNormal5_g5 );
				float3 SRPAdditionLight203 = localAdditionalLightsLambert5_g5;
				float3 smoothstepResult114 = smoothstep( temp_cast_2 , temp_cast_3 , ( floor( ( SRPAdditionLight203 * _SpecularSRPLightSampler ) ) / _SpecularSRPLightSampler ));
				float3 SRPLightFinal281 = smoothstepResult114;
				float3 bakedGI13_g6 = ASEIndirectDiffuse( IN.lightmapUVOrVertexSH.xy, ase_worldNormal);
				float4 unityObjectToClipPos2_g6 = TransformWorldToHClip(TransformObjectToWorld(IN.ase_texcoord8.xyz));
				float4 computeScreenPos3_g6 = ComputeScreenPos( unityObjectToClipPos2_g6 );
				half2 In017_g6 = ( computeScreenPos3_g6 / computeScreenPos3_g6.w ).xy;
				half localSampleAO17_g6 = SampleAO17_g6( In017_g6 );
				float SF_SSAO18_g6 = localSampleAO17_g6;
				float smoothstepResult8_g6 = smoothstep( _AOThresold , ( _AOThresold + _AOSmoothness ) , SF_SSAO18_g6);
				float4 lerpResult12_g6 = lerp( ( float4( bakedGI13_g6 , 0.0 ) * _AOColour ) , _Color0 , smoothstepResult8_g6);
				float4 lerpResult20_g6 = lerp( _BassColor , ( _BassColor * lerpResult12_g6 ) , _AOMix);
				#ifdef _USESSAO_ON
				float4 staticSwitch23_g6 = lerpResult20_g6;
				#else
				float4 staticSwitch23_g6 = _BassColor;
				#endif
				float2 uv_AlbedoTexture1 = IN.ase_texcoord3.xy * _AlbedoTexture1_ST.xy + _AlbedoTexture1_ST.zw;
				float2 uv_AlbedoTexture2 = IN.ase_texcoord3.xy * _AlbedoTexture2_ST.xy + _AlbedoTexture2_ST.zw;
				float4 lerpResult248 = lerp( ( tex2D( _AlbedoTexture1, uv_AlbedoTexture1 ) * _AlbedoTexture1Color ) , ( tex2D( _AlbedoTexture2, uv_AlbedoTexture2 ) * _AlbedoTexture2Color ) , IN.ase_color.g);
				float2 uv_AlbedoTexture3 = IN.ase_texcoord3.xy * _AlbedoTexture3_ST.xy + _AlbedoTexture3_ST.zw;
				float4 lerpResult249 = lerp( lerpResult248 , ( tex2D( _AlbedoTexture3, uv_AlbedoTexture3 ) * _AlbedoTexture3Color ) , IN.ase_color.b);
				float2 uv_AlbedoTexture4 = IN.ase_texcoord3.xy * _AlbedoTexture4_ST.xy + _AlbedoTexture4_ST.zw;
				float4 lerpResult250 = lerp( lerpResult249 , ( tex2D( _AlbedoTexture4, uv_AlbedoTexture4 ) * _AlbedoTexture4Color ) , IN.ase_color.a);
				float4 Albedo242 = lerpResult250;
				float4 temp_output_11_0 = ( _AlbedoEmissionColor * Albedo242 );
				float2 uv_IndirectDiffuseLightNormal = IN.ase_texcoord3.xy * _IndirectDiffuseLightNormal_ST.xy + _IndirectDiffuseLightNormal_ST.zw;
				float3 tanNormal24 = UnpackNormalScale( tex2D( _IndirectDiffuseLightNormal, uv_IndirectDiffuseLightNormal ), 1.0f );
				float3 bakedGI24 = ASEIndirectDiffuse( IN.lightmapUVOrVertexSH.xy, float3(dot(tanToWorld0,tanNormal24), dot(tanToWorld1,tanNormal24), dot(tanToWorld2,tanNormal24)));
				float3 LightAtt209 = temp_output_15_0;
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - WorldPosition );
				ase_worldViewDir = SafeNormalize( ase_worldViewDir );
				float3 normalizedWorldNormal = normalize( ase_worldNormal );
				float dotResult75 = dot( ase_worldViewDir , normalizedWorldNormal );
				float smoothstepResult81 = smoothstep( _RimSmoothStepMinMax.x , ( _RimSmoothStepMinMax.x + _RimSmoothStepMinMax.y ) , saturate( ( 1.0 - ( dotResult75 + _RimOffset ) ) ));
				float4 Rim201 = ( _RimColor * smoothstepResult81 * float4( LightAtt209 , 0.0 ) );
				float3 tanNormal90 = Normal43;
				float3 worldNormal90 = float3(dot(tanToWorld0,tanNormal90), dot(tanToWorld1,tanNormal90), dot(tanToWorld2,tanNormal90));
				float dotResult92 = dot( ( ase_worldViewDir + _MainLightPosition.xyz ) , worldNormal90 );
				float smoothstepResult95 = smoothstep( _SpecularSmoothStepMinMax.x , _SpecularSmoothStepMinMax.y , pow( dotResult92 , _SpecularPow ));
				float2 uv_SpecularMap = IN.ase_texcoord3.xy * _SpecularMap_ST.xy + _SpecularMap_ST.zw;
				float4 Specular101 = ( float4( ( ( LightAtt209 * _SpecularDirLightSRPLightIntensity.x ) + ( smoothstepResult114 * _SpecularDirLightSRPLightIntensity.y ) ) , 0.0 ) * ( smoothstepResult95 * _SpecularIntensity ) * _SpecularColor * tex2D( _SpecularMap, uv_SpecularMap ) );
				float2 uv_EmissionTexture = IN.ase_texcoord3.xy * _EmissionTexture_ST.xy + _EmissionTexture_ST.zw;
				float4 temp_output_135_0 = ( ( ( ( ( temp_output_11_0 * float4( ( bakedGI24 + _DiffuseLightStrength ) , 0.0 ) ) + ( ( temp_output_11_0 * float4( LightAtt209 , 0.0 ) ) * staticSwitch123 ) + ( temp_output_11_0 * float4( SRPAdditionLight203 , 0.0 ) ) ) + Rim201 ) + Specular101 ) + ( tex2D( _EmissionTexture, uv_EmissionTexture ) * _EmissionColor * _EmissionStrength ) );
				float temp_output_13_0_g4 = _FogEnd;
				float clampResult11_g4 = clamp( ( ( temp_output_13_0_g4 - distance( WorldPosition , _WorldSpaceCameraPos ) ) / ( temp_output_13_0_g4 - _FogStart ) ) , 0.0 , 1.0 );
				float LinearFog270 = ( 1.0 - clampResult11_g4 );
				float clampResult273 = clamp( LinearFog270 , 0.0 , 1.0 );
				float4 lerpResult274 = lerp( temp_output_135_0 , _FogColor , clampResult273);
				float4 temp_output_285_0 = ( ( RampFinal283 * float4( SRPLightFinal281 , 0.0 ) ) + ( staticSwitch23_g6 * lerpResult274 ) );
				
				float4 screenPos = IN.ase_texcoord9;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float4 ditherCustomScreenPos234 = ase_screenPosNorm;
				float2 clipScreen234 = ditherCustomScreenPos234.xy * _ScreenParams.xy;
				float dither234 = Dither4x4Bayer( fmod(clipScreen234.x, 4), fmod(clipScreen234.y, 4) );
				dither234 = step( dither234, _Alpha );
				
				#ifdef _SHADOWUSEDITHER_ON
				float staticSwitch239 = 0.001;
				#else
				float staticSwitch239 = 0.0;
				#endif
				
				float3 BakedAlbedo = temp_output_285_0.rgb;
				float3 BakedEmission = 0;
				float3 Color = temp_output_285_0.rgb;
				float Alpha = ( _Alpha * dither234 );
				float AlphaClipThreshold = staticSwitch239;
				float AlphaClipThresholdShadow = 0.5;

				#ifdef _ALPHATEST_ON
					clip( Alpha - AlphaClipThreshold );
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif

				#ifdef ASE_FOG
					Color = MixFog( Color, IN.fogFactor );
				#endif

				return half4( Color, Alpha );
			}

			ENDHLSL
		}

		
		Pass
		{
			
			Name "ShadowCaster"
			Tags { "LightMode"="ShadowCaster" }

			ZWrite On
			ZTest LEqual
			AlphaToMask Off

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#define _ALPHATEST_ON 1
			#define ASE_SRP_VERSION 100302

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

			#pragma shader_feature_local _SHADOWUSEDITHER_ON
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				float4 ase_texcoord2 : TEXCOORD2;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _AlbedoTexture1Color;
			float4 _AlbedoTexture4_ST;
			float4 _AlbedoTexture3Color;
			float4 _AlbedoTexture3_ST;
			float4 _AlbedoTexture2Color;
			float4 _AlbedoTexture2_ST;
			float4 _RimColor;
			float4 _AlbedoTexture1_ST;
			float4 _AlbedoEmissionColor;
			float4 _Color0;
			float4 _AOColour;
			float4 _AlbedoTexture4Color;
			float4 _BassColor;
			float4 _SpecularMap_ST;
			float4 _NormalMap_ST;
			float4 _FogColor;
			float4 _NormalMap1_ST;
			float4 _EmissionColor;
			float4 _EmissionTexture_ST;
			float4 _RampCelluloidNoiseTexture_ST;
			float4 _IndirectDiffuseLightNormal_ST;
			float4 _SpecularColor;
			float2 _SpecularDirLightSRPLightIntensity;
			float2 _SpecularSmoothStepMinMax;
			float2 _RimSmoothStepMinMax;
			float2 _SpecularSRPLightSmoothstep;
			float _RimOffset;
			float _EmissionStrength;
			float _SpecularIntensity;
			float _SpecularPow;
			float _FogEnd;
			float _RampAddincludePower;
			float _FogStart;
			float _AOMix;
			float _AOSmoothness;
			float _AOThresold;
			float _SpecularSRPLightSampler;
			float _RampCelluloidNoiseSoftness;
			float _RampCelluloidNoiseValue;
			float _RampAddwithoutPower;
			float _DirLightIntensity;
			float _LightAttSoft;
			float _LightAttValue;
			float _DirLightNormalScale;
			float _NormalStrength;
			float _RampPower;
			float _DiffuseLightStrength;
			float _Alpha;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			

			inline float Dither4x4Bayer( int x, int y )
			{
				const float dither[ 16 ] = {
			 1,  9,  3, 11,
			13,  5, 15,  7,
			 4, 12,  2, 10,
			16,  8, 14,  6 };
				int r = y * 4 + x;
				return dither[r] / 16; // same # of instructions as pre-dividing due to compiler magic
			}
			

			float3 _LightDirection;

			VertexOutput VertexFunction( VertexInput v )
			{
				VertexOutput o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );

				float4 ase_clipPos = TransformObjectToHClip((v.vertex).xyz);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord2 = screenPos;
				
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = defaultVertexValue;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif

				float3 normalWS = TransformObjectToWorldDir( v.ase_normal );

				float4 clipPos = TransformWorldToHClip( ApplyShadowBias( positionWS, normalWS, _LightDirection ) );

				#if UNITY_REVERSED_Z
					clipPos.z = min(clipPos.z, clipPos.w * UNITY_NEAR_CLIP_VALUE);
				#else
					clipPos.z = max(clipPos.z, clipPos.w * UNITY_NEAR_CLIP_VALUE);
				#endif

				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = clipPos;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				o.clipPos = clipPos;

				return o;
			}
			
			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float4 screenPos = IN.ase_texcoord2;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float4 ditherCustomScreenPos234 = ase_screenPosNorm;
				float2 clipScreen234 = ditherCustomScreenPos234.xy * _ScreenParams.xy;
				float dither234 = Dither4x4Bayer( fmod(clipScreen234.x, 4), fmod(clipScreen234.y, 4) );
				dither234 = step( dither234, _Alpha );
				
				#ifdef _SHADOWUSEDITHER_ON
				float staticSwitch239 = 0.001;
				#else
				float staticSwitch239 = 0.0;
				#endif
				
				float Alpha = ( _Alpha * dither234 );
				float AlphaClipThreshold = staticSwitch239;
				float AlphaClipThresholdShadow = 0.5;

				#ifdef _ALPHATEST_ON
					#ifdef _ALPHATEST_SHADOW_ON
						clip(Alpha - AlphaClipThresholdShadow);
					#else
						clip(Alpha - AlphaClipThreshold);
					#endif
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif
				return 0;
			}

			ENDHLSL
		}

		
		Pass
		{
			
			Name "DepthOnly"
			Tags { "LightMode"="DepthOnly" }

			ZWrite On
			ColorMask 0
			AlphaToMask Off

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#define _ALPHATEST_ON 1
			#define ASE_SRP_VERSION 100302

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

			#pragma shader_feature_local _SHADOWUSEDITHER_ON
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				float4 ase_texcoord2 : TEXCOORD2;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _AlbedoTexture1Color;
			float4 _AlbedoTexture4_ST;
			float4 _AlbedoTexture3Color;
			float4 _AlbedoTexture3_ST;
			float4 _AlbedoTexture2Color;
			float4 _AlbedoTexture2_ST;
			float4 _RimColor;
			float4 _AlbedoTexture1_ST;
			float4 _AlbedoEmissionColor;
			float4 _Color0;
			float4 _AOColour;
			float4 _AlbedoTexture4Color;
			float4 _BassColor;
			float4 _SpecularMap_ST;
			float4 _NormalMap_ST;
			float4 _FogColor;
			float4 _NormalMap1_ST;
			float4 _EmissionColor;
			float4 _EmissionTexture_ST;
			float4 _RampCelluloidNoiseTexture_ST;
			float4 _IndirectDiffuseLightNormal_ST;
			float4 _SpecularColor;
			float2 _SpecularDirLightSRPLightIntensity;
			float2 _SpecularSmoothStepMinMax;
			float2 _RimSmoothStepMinMax;
			float2 _SpecularSRPLightSmoothstep;
			float _RimOffset;
			float _EmissionStrength;
			float _SpecularIntensity;
			float _SpecularPow;
			float _FogEnd;
			float _RampAddincludePower;
			float _FogStart;
			float _AOMix;
			float _AOSmoothness;
			float _AOThresold;
			float _SpecularSRPLightSampler;
			float _RampCelluloidNoiseSoftness;
			float _RampCelluloidNoiseValue;
			float _RampAddwithoutPower;
			float _DirLightIntensity;
			float _LightAttSoft;
			float _LightAttValue;
			float _DirLightNormalScale;
			float _NormalStrength;
			float _RampPower;
			float _DiffuseLightStrength;
			float _Alpha;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			

			inline float Dither4x4Bayer( int x, int y )
			{
				const float dither[ 16 ] = {
			 1,  9,  3, 11,
			13,  5, 15,  7,
			 4, 12,  2, 10,
			16,  8, 14,  6 };
				int r = y * 4 + x;
				return dither[r] / 16; // same # of instructions as pre-dividing due to compiler magic
			}
			

			VertexOutput VertexFunction( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float4 ase_clipPos = TransformObjectToHClip((v.vertex).xyz);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord2 = screenPos;
				
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = defaultVertexValue;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif

				o.clipPos = TransformWorldToHClip( positionWS );
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = clipPos;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float4 screenPos = IN.ase_texcoord2;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float4 ditherCustomScreenPos234 = ase_screenPosNorm;
				float2 clipScreen234 = ditherCustomScreenPos234.xy * _ScreenParams.xy;
				float dither234 = Dither4x4Bayer( fmod(clipScreen234.x, 4), fmod(clipScreen234.y, 4) );
				dither234 = step( dither234, _Alpha );
				
				#ifdef _SHADOWUSEDITHER_ON
				float staticSwitch239 = 0.001;
				#else
				float staticSwitch239 = 0.0;
				#endif
				
				float Alpha = ( _Alpha * dither234 );
				float AlphaClipThreshold = staticSwitch239;

				#ifdef _ALPHATEST_ON
					clip(Alpha - AlphaClipThreshold);
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif
				return 0;
			}
			ENDHLSL
		}

		
		Pass
		{
			
			Name "Meta"
			Tags { "LightMode"="Meta" }

			Cull Off

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#define _ALPHATEST_ON 1
			#define ASE_SRP_VERSION 100302

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/MetaInput.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

			#include "Packages/com.unity.shadergraph/ShaderGraphLibrary/Functions.hlsl"
			#define ASE_NEEDS_VERT_NORMAL
			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#define ASE_NEEDS_FRAG_SHADOWCOORDS
			#define ASE_NEEDS_VERT_TEXTURE_COORDINATES1
			#define ASE_NEEDS_FRAG_COLOR
			#pragma shader_feature_local _SHADOWREMAPTYPE_USESHADERGARDIENTCOLOR _SHADOWREMAPTYPE_USESHADOWREMAPMAP
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma shader_feature_local _USESSAO_ON
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma shader_feature_local _SHADOWUSEDITHER_ON
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 texcoord1 : TEXCOORD1;
				float4 texcoord2 : TEXCOORD2;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_color : COLOR;
				float4 ase_tangent : TANGENT;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				float4 ase_texcoord2 : TEXCOORD2;
				float4 ase_color : COLOR;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_texcoord4 : TEXCOORD4;
				float4 ase_texcoord5 : TEXCOORD5;
				float4 lightmapUVOrVertexSH : TEXCOORD6;
				float4 ase_texcoord7 : TEXCOORD7;
				float4 ase_texcoord8 : TEXCOORD8;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _AlbedoTexture1Color;
			float4 _AlbedoTexture4_ST;
			float4 _AlbedoTexture3Color;
			float4 _AlbedoTexture3_ST;
			float4 _AlbedoTexture2Color;
			float4 _AlbedoTexture2_ST;
			float4 _RimColor;
			float4 _AlbedoTexture1_ST;
			float4 _AlbedoEmissionColor;
			float4 _Color0;
			float4 _AOColour;
			float4 _AlbedoTexture4Color;
			float4 _BassColor;
			float4 _SpecularMap_ST;
			float4 _NormalMap_ST;
			float4 _FogColor;
			float4 _NormalMap1_ST;
			float4 _EmissionColor;
			float4 _EmissionTexture_ST;
			float4 _RampCelluloidNoiseTexture_ST;
			float4 _IndirectDiffuseLightNormal_ST;
			float4 _SpecularColor;
			float2 _SpecularDirLightSRPLightIntensity;
			float2 _SpecularSmoothStepMinMax;
			float2 _RimSmoothStepMinMax;
			float2 _SpecularSRPLightSmoothstep;
			float _RimOffset;
			float _EmissionStrength;
			float _SpecularIntensity;
			float _SpecularPow;
			float _FogEnd;
			float _RampAddincludePower;
			float _FogStart;
			float _AOMix;
			float _AOSmoothness;
			float _AOThresold;
			float _SpecularSRPLightSampler;
			float _RampCelluloidNoiseSoftness;
			float _RampCelluloidNoiseValue;
			float _RampAddwithoutPower;
			float _DirLightIntensity;
			float _LightAttSoft;
			float _LightAttValue;
			float _DirLightNormalScale;
			float _NormalStrength;
			float _RampPower;
			float _DiffuseLightStrength;
			float _Alpha;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			sampler2D _ShadowRemapMap;
			sampler2D _NormalMap;
			sampler2D _NormalMap1;
			sampler2D _RampCelluloidNoiseTexture;
			sampler2D _AlbedoTexture1;
			sampler2D _AlbedoTexture2;
			sampler2D _AlbedoTexture3;
			sampler2D _AlbedoTexture4;
			sampler2D _IndirectDiffuseLightNormal;
			sampler2D _SpecularMap;
			sampler2D _EmissionTexture;


			
			float4 SampleGradient( Gradient gradient, float time )
			{
				float3 color = gradient.colors[0].rgb;
				UNITY_UNROLL
				for (int c = 1; c < 8; c++)
				{
				float colorPos = saturate((time - gradient.colors[c-1].w) / ( 0.00001 + (gradient.colors[c].w - gradient.colors[c-1].w)) * step(c, gradient.colorsLength-1));
				color = lerp(color, gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), gradient.type));
				}
				#ifndef UNITY_COLORSPACE_GAMMA
				color = SRGBToLinear(color);
				#endif
				float alpha = gradient.alphas[0].x;
				UNITY_UNROLL
				for (int a = 1; a < 8; a++)
				{
				float alphaPos = saturate((time - gradient.alphas[a-1].y) / ( 0.00001 + (gradient.alphas[a].y - gradient.alphas[a-1].y)) * step(a, gradient.alphasLength-1));
				alpha = lerp(alpha, gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), gradient.type));
				}
				return float4(color, alpha);
			}
			
			float3 AdditionalLightsLambert( float3 WorldPosition, float3 WorldNormal )
			{
				float3 Color = 0;
				#ifdef _ADDITIONAL_LIGHTS
				int numLights = GetAdditionalLightsCount();
				for(int i = 0; i<numLights;i++)
				{
					Light light = GetAdditionalLight(i, WorldPosition);
					half3 AttLightColor = light.color *(light.distanceAttenuation * light.shadowAttenuation);
					Color +=LightingLambert(AttLightColor, light.direction, WorldNormal);
					
				}
				#endif
				return Color;
			}
			
			float3 ASEIndirectDiffuse( float2 uvStaticLightmap, float3 normalWS )
			{
			#ifdef LIGHTMAP_ON
				return SampleLightmap( uvStaticLightmap, normalWS );
			#else
				return SampleSH(normalWS);
			#endif
			}
			
			half SampleAO17_g6( half2 In0 )
			{
				return SampleAmbientOcclusion(In0);
			}
			
			inline float Dither4x4Bayer( int x, int y )
			{
				const float dither[ 16 ] = {
			 1,  9,  3, 11,
			13,  5, 15,  7,
			 4, 12,  2, 10,
			16,  8, 14,  6 };
				int r = y * 4 + x;
				return dither[r] / 16; // same # of instructions as pre-dividing due to compiler magic
			}
			

			VertexOutput VertexFunction( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float3 ase_worldTangent = TransformObjectToWorldDir(v.ase_tangent.xyz);
				o.ase_texcoord3.xyz = ase_worldTangent;
				float3 ase_worldNormal = TransformObjectToWorldNormal(v.ase_normal);
				o.ase_texcoord4.xyz = ase_worldNormal;
				float ase_vertexTangentSign = v.ase_tangent.w * unity_WorldTransformParams.w;
				float3 ase_worldBitangent = cross( ase_worldNormal, ase_worldTangent ) * ase_vertexTangentSign;
				o.ase_texcoord5.xyz = ase_worldBitangent;
				OUTPUT_LIGHTMAP_UV( v.texcoord1, unity_LightmapST, o.lightmapUVOrVertexSH.xy );
				OUTPUT_SH( ase_worldNormal, o.lightmapUVOrVertexSH.xyz );
				
				float4 ase_clipPos = TransformObjectToHClip((v.vertex).xyz);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord8 = screenPos;
				
				o.ase_texcoord2.xy = v.ase_texcoord.xy;
				o.ase_color = v.ase_color;
				o.ase_texcoord7 = v.vertex;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord2.zw = 0;
				o.ase_texcoord3.w = 0;
				o.ase_texcoord4.w = 0;
				o.ase_texcoord5.w = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = defaultVertexValue;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif

				o.clipPos = MetaVertexPosition( v.vertex, v.texcoord1.xy, v.texcoord1.xy, unity_LightmapST, unity_DynamicLightmapST );
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = o.clipPos;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_color : COLOR;
				float4 ase_tangent : TANGENT;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord = v.ase_texcoord;
				o.ase_color = v.ase_color;
				o.ase_tangent = v.ase_tangent;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				o.ase_color = patch[0].ase_color * bary.x + patch[1].ase_color * bary.y + patch[2].ase_color * bary.z;
				o.ase_tangent = patch[0].ase_tangent * bary.x + patch[1].ase_tangent * bary.y + patch[2].ase_tangent * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float saferPower192 = max( _RampAddincludePower , 0.0001 );
				float2 uv_NormalMap = IN.ase_texcoord2.xy * _NormalMap_ST.xy + _NormalMap_ST.zw;
				float3 unpack42 = UnpackNormalScale( tex2D( _NormalMap, uv_NormalMap ), _NormalStrength );
				unpack42.z = lerp( 1, unpack42.z, saturate(_NormalStrength) );
				float2 uv_NormalMap1 = IN.ase_texcoord2.xy * _NormalMap1_ST.xy + _NormalMap1_ST.zw;
				float3 lerpResult263 = lerp( unpack42 , UnpackNormalScale( tex2D( _NormalMap1, uv_NormalMap1 ), 1.0f ) , IN.ase_color.r);
				float3 Normal43 = lerpResult263;
				float3 ase_worldTangent = IN.ase_texcoord3.xyz;
				float3 ase_worldNormal = IN.ase_texcoord4.xyz;
				float3 ase_worldBitangent = IN.ase_texcoord5.xyz;
				float3 tanToWorld0 = float3( ase_worldTangent.x, ase_worldBitangent.x, ase_worldNormal.x );
				float3 tanToWorld1 = float3( ase_worldTangent.y, ase_worldBitangent.y, ase_worldNormal.y );
				float3 tanToWorld2 = float3( ase_worldTangent.z, ase_worldBitangent.z, ase_worldNormal.z );
				float3 tanNormal2 = Normal43;
				float3 worldNormal2 = float3(dot(tanToWorld0,tanNormal2), dot(tanToWorld1,tanNormal2), dot(tanToWorld2,tanNormal2));
				float dotResult3 = dot( worldNormal2 , SafeNormalize(_MainLightPosition.xyz) );
				float ase_lightAtten = 0;
				Light ase_lightAtten_mainLight = GetMainLight( ShadowCoords );
				ase_lightAtten = ase_lightAtten_mainLight.distanceAttenuation * ase_lightAtten_mainLight.shadowAttenuation;
				float smoothstepResult158 = smoothstep( _LightAttValue , ( _LightAttValue + _LightAttSoft ) , ase_lightAtten);
				float3 temp_output_15_0 = ( smoothstepResult158 * ( _MainLightColor.rgb * _MainLightColor.a * _DirLightIntensity ) );
				float3 break16 = temp_output_15_0;
				float2 uv_RampCelluloidNoiseTexture = IN.ase_texcoord2.xy * _RampCelluloidNoiseTexture_ST.xy + _RampCelluloidNoiseTexture_ST.zw;
				float smoothstepResult166 = smoothstep( _RampCelluloidNoiseValue , _RampCelluloidNoiseSoftness , tex2D( _RampCelluloidNoiseTexture, uv_RampCelluloidNoiseTexture ).r);
				float temp_output_165_0 = ( ( pow( saferPower192 , _RampPower ) + ( (dotResult3*_DirLightNormalScale + 0.5) * ( max( max( break16.x , break16.y ) , break16.z ) + 0.0 ) ) + _RampAddwithoutPower ) * smoothstepResult166 );
				float2 temp_cast_0 = (temp_output_165_0).xx;
				Gradient gradient6 = NewGradient( 0, 3, 2, float4( 0.9150943, 0.4437344, 0.4437344, 0.2515908 ), float4( 0.9811321, 0.6723437, 0.4665005, 0.6992905 ), float4( 0.9853496, 1, 0.6264151, 1 ), 0, 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
				float2 temp_cast_1 = (temp_output_165_0).xx;
				#if defined(_SHADOWREMAPTYPE_USESHADERGARDIENTCOLOR)
				float4 staticSwitch123 = SampleGradient( gradient6, temp_output_165_0 );
				#elif defined(_SHADOWREMAPTYPE_USESHADOWREMAPMAP)
				float4 staticSwitch123 = tex2D( _ShadowRemapMap, temp_cast_0 );
				#else
				float4 staticSwitch123 = tex2D( _ShadowRemapMap, temp_cast_0 );
				#endif
				float4 RampFinal283 = staticSwitch123;
				float3 temp_cast_2 = (_SpecularSRPLightSmoothstep.x).xxx;
				float3 temp_cast_3 = (( _SpecularSRPLightSmoothstep.x + _SpecularSRPLightSmoothstep.y )).xxx;
				float3 WorldPosition5_g5 = WorldPosition;
				float3 tanNormal12_g5 = Normal43;
				float3 worldNormal12_g5 = float3(dot(tanToWorld0,tanNormal12_g5), dot(tanToWorld1,tanNormal12_g5), dot(tanToWorld2,tanNormal12_g5));
				float3 WorldNormal5_g5 = worldNormal12_g5;
				float3 localAdditionalLightsLambert5_g5 = AdditionalLightsLambert( WorldPosition5_g5 , WorldNormal5_g5 );
				float3 SRPAdditionLight203 = localAdditionalLightsLambert5_g5;
				float3 smoothstepResult114 = smoothstep( temp_cast_2 , temp_cast_3 , ( floor( ( SRPAdditionLight203 * _SpecularSRPLightSampler ) ) / _SpecularSRPLightSampler ));
				float3 SRPLightFinal281 = smoothstepResult114;
				float3 bakedGI13_g6 = ASEIndirectDiffuse( IN.lightmapUVOrVertexSH.xy, ase_worldNormal);
				float4 unityObjectToClipPos2_g6 = TransformWorldToHClip(TransformObjectToWorld(IN.ase_texcoord7.xyz));
				float4 computeScreenPos3_g6 = ComputeScreenPos( unityObjectToClipPos2_g6 );
				half2 In017_g6 = ( computeScreenPos3_g6 / computeScreenPos3_g6.w ).xy;
				half localSampleAO17_g6 = SampleAO17_g6( In017_g6 );
				float SF_SSAO18_g6 = localSampleAO17_g6;
				float smoothstepResult8_g6 = smoothstep( _AOThresold , ( _AOThresold + _AOSmoothness ) , SF_SSAO18_g6);
				float4 lerpResult12_g6 = lerp( ( float4( bakedGI13_g6 , 0.0 ) * _AOColour ) , _Color0 , smoothstepResult8_g6);
				float4 lerpResult20_g6 = lerp( _BassColor , ( _BassColor * lerpResult12_g6 ) , _AOMix);
				#ifdef _USESSAO_ON
				float4 staticSwitch23_g6 = lerpResult20_g6;
				#else
				float4 staticSwitch23_g6 = _BassColor;
				#endif
				float2 uv_AlbedoTexture1 = IN.ase_texcoord2.xy * _AlbedoTexture1_ST.xy + _AlbedoTexture1_ST.zw;
				float2 uv_AlbedoTexture2 = IN.ase_texcoord2.xy * _AlbedoTexture2_ST.xy + _AlbedoTexture2_ST.zw;
				float4 lerpResult248 = lerp( ( tex2D( _AlbedoTexture1, uv_AlbedoTexture1 ) * _AlbedoTexture1Color ) , ( tex2D( _AlbedoTexture2, uv_AlbedoTexture2 ) * _AlbedoTexture2Color ) , IN.ase_color.g);
				float2 uv_AlbedoTexture3 = IN.ase_texcoord2.xy * _AlbedoTexture3_ST.xy + _AlbedoTexture3_ST.zw;
				float4 lerpResult249 = lerp( lerpResult248 , ( tex2D( _AlbedoTexture3, uv_AlbedoTexture3 ) * _AlbedoTexture3Color ) , IN.ase_color.b);
				float2 uv_AlbedoTexture4 = IN.ase_texcoord2.xy * _AlbedoTexture4_ST.xy + _AlbedoTexture4_ST.zw;
				float4 lerpResult250 = lerp( lerpResult249 , ( tex2D( _AlbedoTexture4, uv_AlbedoTexture4 ) * _AlbedoTexture4Color ) , IN.ase_color.a);
				float4 Albedo242 = lerpResult250;
				float4 temp_output_11_0 = ( _AlbedoEmissionColor * Albedo242 );
				float2 uv_IndirectDiffuseLightNormal = IN.ase_texcoord2.xy * _IndirectDiffuseLightNormal_ST.xy + _IndirectDiffuseLightNormal_ST.zw;
				float3 tanNormal24 = UnpackNormalScale( tex2D( _IndirectDiffuseLightNormal, uv_IndirectDiffuseLightNormal ), 1.0f );
				float3 bakedGI24 = ASEIndirectDiffuse( IN.lightmapUVOrVertexSH.xy, float3(dot(tanToWorld0,tanNormal24), dot(tanToWorld1,tanNormal24), dot(tanToWorld2,tanNormal24)));
				float3 LightAtt209 = temp_output_15_0;
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - WorldPosition );
				ase_worldViewDir = SafeNormalize( ase_worldViewDir );
				float3 normalizedWorldNormal = normalize( ase_worldNormal );
				float dotResult75 = dot( ase_worldViewDir , normalizedWorldNormal );
				float smoothstepResult81 = smoothstep( _RimSmoothStepMinMax.x , ( _RimSmoothStepMinMax.x + _RimSmoothStepMinMax.y ) , saturate( ( 1.0 - ( dotResult75 + _RimOffset ) ) ));
				float4 Rim201 = ( _RimColor * smoothstepResult81 * float4( LightAtt209 , 0.0 ) );
				float3 tanNormal90 = Normal43;
				float3 worldNormal90 = float3(dot(tanToWorld0,tanNormal90), dot(tanToWorld1,tanNormal90), dot(tanToWorld2,tanNormal90));
				float dotResult92 = dot( ( ase_worldViewDir + _MainLightPosition.xyz ) , worldNormal90 );
				float smoothstepResult95 = smoothstep( _SpecularSmoothStepMinMax.x , _SpecularSmoothStepMinMax.y , pow( dotResult92 , _SpecularPow ));
				float2 uv_SpecularMap = IN.ase_texcoord2.xy * _SpecularMap_ST.xy + _SpecularMap_ST.zw;
				float4 Specular101 = ( float4( ( ( LightAtt209 * _SpecularDirLightSRPLightIntensity.x ) + ( smoothstepResult114 * _SpecularDirLightSRPLightIntensity.y ) ) , 0.0 ) * ( smoothstepResult95 * _SpecularIntensity ) * _SpecularColor * tex2D( _SpecularMap, uv_SpecularMap ) );
				float2 uv_EmissionTexture = IN.ase_texcoord2.xy * _EmissionTexture_ST.xy + _EmissionTexture_ST.zw;
				float4 temp_output_135_0 = ( ( ( ( ( temp_output_11_0 * float4( ( bakedGI24 + _DiffuseLightStrength ) , 0.0 ) ) + ( ( temp_output_11_0 * float4( LightAtt209 , 0.0 ) ) * staticSwitch123 ) + ( temp_output_11_0 * float4( SRPAdditionLight203 , 0.0 ) ) ) + Rim201 ) + Specular101 ) + ( tex2D( _EmissionTexture, uv_EmissionTexture ) * _EmissionColor * _EmissionStrength ) );
				float temp_output_13_0_g4 = _FogEnd;
				float clampResult11_g4 = clamp( ( ( temp_output_13_0_g4 - distance( WorldPosition , _WorldSpaceCameraPos ) ) / ( temp_output_13_0_g4 - _FogStart ) ) , 0.0 , 1.0 );
				float LinearFog270 = ( 1.0 - clampResult11_g4 );
				float clampResult273 = clamp( LinearFog270 , 0.0 , 1.0 );
				float4 lerpResult274 = lerp( temp_output_135_0 , _FogColor , clampResult273);
				float4 temp_output_285_0 = ( ( RampFinal283 * float4( SRPLightFinal281 , 0.0 ) ) + ( staticSwitch23_g6 * lerpResult274 ) );
				
				float4 screenPos = IN.ase_texcoord8;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float4 ditherCustomScreenPos234 = ase_screenPosNorm;
				float2 clipScreen234 = ditherCustomScreenPos234.xy * _ScreenParams.xy;
				float dither234 = Dither4x4Bayer( fmod(clipScreen234.x, 4), fmod(clipScreen234.y, 4) );
				dither234 = step( dither234, _Alpha );
				
				#ifdef _SHADOWUSEDITHER_ON
				float staticSwitch239 = 0.001;
				#else
				float staticSwitch239 = 0.0;
				#endif
				
				float3 BakedAlbedo = temp_output_285_0.rgb;
				float3 BakedEmission = 0;
				float Alpha = ( _Alpha * dither234 );
				float AlphaClipThreshold = staticSwitch239;

				#ifdef _ALPHATEST_ON
					clip(Alpha - AlphaClipThreshold);
				#endif

				MetaInput metaInput = (MetaInput)0;
				metaInput.Albedo = BakedAlbedo;
				metaInput.Emission = BakedEmission;
				
				return MetaFragment(metaInput);
			}
			ENDHLSL
		}
		
	}
	CustomEditor "UnityEditor.ShaderGraph.PBRMasterGUI"
	Fallback "wiiu"
	
}
/*ASEBEGIN
Version=18900
0;73.6;892.6;719;-4008.619;686.6235;1;True;False
Node;AmplifyShaderEditor.CommentaryNode;46;-3761.005,-154.5279;Inherit;False;1169.146;309.3865;Comment;2;42;44;Normal;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;252;-4068.143,-1522.308;Inherit;False;2087.428;1225.619;Comment;16;251;260;259;258;257;250;248;249;253;254;255;256;8;247;246;245;Painter Albedo;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-3711.005,-58.56604;Inherit;False;Property;_NormalStrength;Normal Strength(法線強度);12;0;Create;False;0;0;0;True;0;False;0;0;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;262;-3382.156,140.0307;Inherit;True;Property;_NormalMap1;Bump;10;2;[Header];[Normal];Create;False;1;Normal;0;0;True;0;False;-1;None;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;160;-2618.927,713.905;Inherit;False;Property;_LightAttSoft;Light Att Soft(光衰減平滑度);37;0;Create;False;0;0;0;False;0;False;0.46;0.36;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;159;-2620.927,632.905;Inherit;False;Property;_LightAttValue;Light Att Value(光衰減程度);36;1;[Header];Create;False;1;Light Attenuation;0;0;False;0;False;-0.3;-0.25;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;48;-2055.019,621.7488;Inherit;False;506.8718;338.4612;Comment;5;14;15;124;125;158;Light Color & Attenuation;1,1,1,1;0;0
Node;AmplifyShaderEditor.VertexColorNode;251;-3428.515,-660.9487;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;42;-3375.967,-104.528;Inherit;True;Property;_NormalMap;Normal Map(法線貼圖);11;2;[Header];[Normal];Create;False;1;Normal;0;0;True;0;False;-1;None;23aaeedc485ed4640bd00f040cb36cf8;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LightColorNode;14;-2162.017,749.4439;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.LerpOp;263;-2986.008,67.20064;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LightAttenuation;12;-2274.485,638.0951;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;161;-2361.237,716.5262;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;125;-2186.869,876.6174;Inherit;False;Property;_DirLightIntensity;DirLight Intensity(日光強度);16;1;[Header];Create;False;1;Light;0;0;False;0;False;1;1.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;158;-1987.824,638.7889;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;43;-2463.452,-104.9831;Inherit;True;Normal;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;124;-1926.444,773.9294;Inherit;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;208;-3769.389,616.646;Inherit;False;740.6223;260.14;Comment;3;65;112;203;SRP Light;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;47;-1735.329,-142.6868;Inherit;False;1070.12;398.215;Comment;6;45;1;2;3;5;131;Normal Light(N.L);1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;15;-1783.546,706.8102;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;112;-3719.389,761.386;Inherit;False;43;Normal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;52;-1489.389,930.8528;Inherit;False;741.3994;303.9028;Color Remap模塊中自有陰影重映射的Gardient,因此若不先漸變為灰度圖,重映射會不正確;3;17;18;16;轉換為灰度圖;1,1,1,1;0;0
Node;AmplifyShaderEditor.ColorNode;254;-4002.05,-1150.793;Inherit;False;Property;_AlbedoTexture2Color;AlbedoTexture2Color;3;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,1;0.4716981,0.4716981,0.4716981,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;245;-3563.472,-1264.393;Inherit;True;Property;_AlbedoTexture2;Albedo Map 2(筆刷1);2;1;[Header];Create;False;1;Albedo;0;0;True;0;False;-1;None;07b09773e1dbb184094e88981ee0ed01;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;253;-3998.526,-1321.643;Inherit;False;Property;_AlbedoTexture1Color;AlbedoTexture1Color;1;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,1;0.7490196,0.7490196,0.7490196,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;8;-3568.109,-1472.308;Inherit;True;Property;_AlbedoTexture1;Albedo Map 1(主貼圖);0;1;[Header];Create;False;1;Albedo;0;0;True;0;False;-1;None;c25e2f002e0060b439f5386e5909d521;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;45;-1685.329,-92.68679;Inherit;False;43;Normal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.BreakToComponentsNode;16;-1441.063,980.0394;Inherit;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.FunctionNode;65;-3491.927,765.1676;Inherit;False;SRP Additional Light;-1;;5;6c86746ad131a0a408ca599df5f40861;3,6,1,9,0,23,0;5;2;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;15;FLOAT3;0,0,0;False;14;FLOAT3;1,1,1;False;18;FLOAT;0.5;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;1;-1418.154,74.92816;Inherit;False;True;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;257;-3244.092,-1337.432;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;113;-2445.705,2480.957;Inherit;False;927.4;304.3999;Comment;8;109;108;110;111;114;115;206;280;Specular SRPLight 光源漸變採樣計算;1,1,1,1;0;0
Node;AmplifyShaderEditor.ColorNode;255;-4004.661,-979.9951;Inherit;False;Property;_AlbedoTexture3Color;AlbedoTexture3Color;5;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,1;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;246;-3563.472,-1059.071;Inherit;True;Property;_AlbedoTexture3;Albedo Map 3(筆刷2);4;1;[Header];Create;False;1;Albedo;0;0;True;0;False;-1;None;6e792b75d2f482146be53e346ab6612b;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;258;-3242.135,-1177.899;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;203;-3270.367,666.646;Inherit;False;SRPAdditionLight;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;102;-1652.828,1817.899;Inherit;False;1581.267;646.9999;;12;82;76;74;75;80;81;78;73;77;83;85;189;Rim ;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldNormalVector;2;-1386.231,-86.19307;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMaxOpNode;17;-1233.066,980.0394;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;206;-2349.317,2525.791;Inherit;False;203;SRPAdditionLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;131;-1191.847,-105.1862;Inherit;False;Property;_DirLightNormalScale;DirLightNormalScale(N.L強度);17;0;Create;False;0;0;0;True;0;False;0.6;0.6;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;213;-1440.361,367.9636;Inherit;False;835.4715;413.2869;Comment;5;172;166;169;163;170;Celluloid Texture;1,1,1,1;0;0
Node;AmplifyShaderEditor.DotProductOpNode;3;-1154.231,-17.19307;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;18;-983.3943,981.3561;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;256;-4008.057,-809.5366;Inherit;False;Property;_AlbedoTexture4Color;AlbedoTexture4Color;7;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,1;2.168827,1.607182,1.419968,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;73;-1580.863,1964.308;Inherit;False;World;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldNormalVector;74;-1602.828,2137.635;Inherit;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;111;-2395.705,2669.957;Inherit;False;Property;_SpecularSRPLightSampler;Specular SRPLight Sampler(高光燈光採樣次數);33;0;Create;False;0;0;0;True;0;False;5;5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;103;-1931.334,3052.232;Inherit;False;2880.684;912.0061;Comment;17;116;101;93;97;95;90;98;96;87;99;92;89;88;91;100;94;106;Specular;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;259;-3243.448,-1048.033;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;247;-3561.941,-856.8148;Inherit;True;Property;_AlbedoTexture4;Albedo Map 4(筆刷3);6;1;[Header];Create;False;1;Albedo;0;0;True;0;False;-1;None;ba24f8805297f854da3bbbc9732d9c4e;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;248;-2720.963,-1283.152;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;172;-1390.361,442.9;Inherit;False;0;163;2;3;2;SAMPLER2D;;False;0;FLOAT2;2,2;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;260;-3243.448,-852.0662;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;50;-644.6549,-143.2718;Inherit;False;906.9587;494.713;;6;13;7;122;165;190;198;Shadow Remap;0.6320754,0.6320754,0.6320754,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;193;-846.1345,-215.3708;Inherit;False;Property;_RampPower;Ramp Power;24;0;Create;True;0;0;0;False;0;False;1;0.76;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;199;-1347.544,-2076.739;Inherit;False;1707.054;658.7182;;2;197;6;Ramp Gradient;1,0.9619094,0,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;191;-815.1287,-296.2067;Inherit;False;Property;_RampAddincludePower;Ramp Add(include Power);22;0;Create;True;0;0;0;False;0;False;0;-0.19;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;87;-1802.549,3102.232;Inherit;False;World;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceLightPos;88;-1859.821,3281.149;Inherit;False;0;3;FLOAT4;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;77;-1362.214,2335.58;Inherit;False;Property;_RimOffset;Rim Offset(泛光量值);25;1;[Header];Create;False;1;Rim;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;5;-932.609,-17.07917;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0.5;False;2;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;91;-1881.334,3484.766;Inherit;False;43;Normal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;108;-2101.288,2531.045;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DotProductOpNode;75;-1363.693,2050.069;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;249;-2530.593,-1077.553;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;71;-685.5591,978.8214;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;76;-1095.102,2049.899;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;192;-538.2935,-223.1223;Inherit;False;True;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GradientNode;6;-767.6221,-1781.876;Inherit;False;0;3;2;0.9150943,0.4437344,0.4437344,0.2515908;0.9811321,0.6723437,0.4665005,0.6992905;0.9853496,1,0.6264151,1;1,0;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.FloorOpNode;109;-1965.575,2530.957;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;250;-2331.988,-875.2549;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.WorldNormalVector;90;-1629.821,3489.149;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.Vector2Node;115;-2108.45,2668.116;Inherit;False;Property;_SpecularSRPLightSmoothstep;Specular SRPLight Smoothstep(高光燈光平滑閾值);34;0;Create;False;0;0;0;False;0;False;0,1;-0.5,-114;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleAddOpNode;89;-1571.821,3207.149;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;170;-1367.16,665.8505;Inherit;False;Property;_RampCelluloidNoiseSoftness;Ramp Celluloid Noise Softness(賽璐璐陰影平滑度);40;0;Create;False;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-667.6294,-14.88554;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;196;-627.5509,214.0154;Inherit;False;Property;_RampAddwithoutPower;Ramp Add(without Power);23;0;Create;True;0;0;0;False;0;False;0;-0.67;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;163;-1102.991,417.9636;Inherit;True;Property;_RampCelluloidNoiseTexture;Ramp Celluloid Noise Texture(賽璐璐陰影邊緣噪波貼圖);38;1;[Header];Create;False;1;Celluloid Edge Noise Texture;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;169;-1368.353,597.6141;Inherit;False;Property;_RampCelluloidNoiseValue;Ramp Celluloid Noise Value(賽璐璐陰影強度);39;0;Create;False;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;94;-1345.334,3456.766;Inherit;False;Property;_SpecularPow;Specular Pow(高光量值);29;0;Create;False;0;0;0;False;0;False;4;4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;78;-853.4456,2049.662;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;92;-1344.334,3336.766;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;121;-919.228,2558.679;Inherit;False;596.8665;382.1125;Comment;3;119;117;120;日光/燈光強度調整;1,1,1,1;0;0
Node;AmplifyShaderEditor.SmoothstepOpNode;166;-793.6898,447.1251;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;280;-1806.082,2695.571;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;110;-1843.675,2530.957;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;190;-464.1931,-38.94222;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;49;-725.8527,-998.4443;Inherit;False;546.8332;461.799;Comment;3;10;11;243;Albedo;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;197;-496.319,-1786.893;Inherit;False;RampGradient;-1;True;1;0;OBJECT;;False;1;OBJECT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;242;-2137.36,-880.9568;Inherit;False;Albedo;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;209;-1731.342,1003.499;Inherit;False;LightAtt;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.Vector2Node;85;-912.0933,2298.396;Inherit;False;Property;_RimSmoothStepMinMax;Rim SmoothStepValue and Smoothness(泛光平滑閾值);27;0;Create;False;0;0;0;True;0;False;0,1;0,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.GetLocalVarNode;243;-551.9526,-699.7589;Inherit;False;242;Albedo;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;165;-343.5769,-38.36261;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.31;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;120;-869.228,2608.679;Inherit;False;Property;_SpecularDirLightSRPLightIntensity;Specular DirLight/SRPLight Intensity(高光受光影響程度 主光源/SRP光源));35;0;Create;False;0;0;0;True;0;False;1,1;1,1.87;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.Vector2Node;96;-1341.25,3537.111;Inherit;False;Property;_SpecularSmoothStepMinMax;Specular SmoothStep Value & Smoothness(高光值與平滑度);31;0;Create;False;0;0;0;False;0;False;0,0.01;0,0.01;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.GetLocalVarNode;198;-655.8887,-103.998;Inherit;False;197;RampGradient;1;0;OBJECT;;False;1;OBJECT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;189;-597.5825,2325.11;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;114;-1717.589,2531.599;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;1,1,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;212;-650.5209,2463.829;Inherit;False;209;LightAtt;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;51;-562.3288,-424.9936;Inherit;False;602.1977;121;間接漫射光從Unity的全局照明系統獲取漫射的環境光。這相當於說它檢索周圍的光探測器的信息。;1;24;漫射光照;1,1,1,1;0;0
Node;AmplifyShaderEditor.PowerNode;93;-1146.334,3338.766;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;10;-670.3519,-943.9666;Inherit;False;Property;_AlbedoEmissionColor;Albedo/Emission  Color(主貼圖/高光顏色);8;2;[HDR];[Header];Create;False;0;0;0;False;0;False;0.3301887,0.3301887,0.3301887,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;173;-1077.302,-388.5433;Inherit;True;Property;_IndirectDiffuseLightNormal;Indirect Diffuse Light Normal(漫射光法線貼圖);19;0;Create;False;0;0;0;False;0;False;-1;None;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;80;-655.7444,2051.484;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;210;26.27039,-289.6051;Inherit;False;209;LightAtt;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;117;-484.7615,2614.423;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;11;-341.4195,-809.9456;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SmoothstepOpNode;81;-492.8645,2052.062;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;134;-553.49,-292.229;Inherit;False;Property;_DiffuseLightStrength;DiffuseLight Strength(環境光強度(疊加);18;0;Create;False;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;83;-466.9252,1867.899;Inherit;False;Property;_RimColor;Rim Color(泛光顏色);26;1;[HDR];Create;False;0;0;0;False;0;False;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GradientSampleNode;7;-182.8084,-100.8572;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;122;-171.1407,112.4082;Inherit;True;Property;_ShadowRemapMap;Shadow Remap Map;20;1;[Header];Create;True;1;Shadow Type;0;0;False;0;False;-1;None;5c17639b73e66a64eb1bf87957b3160a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.IndirectDiffuseLighting;24;-529.329,-380.9936;Inherit;False;Tangent;1;0;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SmoothstepOpNode;95;-928.2501,3339.111;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;98;-924.3385,3468.558;Inherit;False;Property;_SpecularIntensity;Specular Intensity(高光強度);32;0;Create;False;0;0;0;True;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;119;-600.7057,2805.991;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;123;220.2502,-106.5203;Inherit;True;Property;_ShadowRemapType;Shadow Remap Type(陰影重映射方式)(左為背光右為迎光);21;0;Create;False;0;0;0;True;0;False;0;1;1;True;;KeywordEnum;2;UseShaderGardientColor;UseShadowRemapMap;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;264;2754.46,262.0988;Inherit;False;1184.134;600.4499;Comment;7;271;269;268;267;266;265;60;FogDistance;0.3622641,0.7043549,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;204;-463.7957,-520.6062;Inherit;False;203;SRPAdditionLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;19;286.3075,-335.1839;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;116;-628.0491,3730.579;Inherit;True;Property;_SpecularMap;Specular Map(高光貼圖);28;1;[Header];Create;False;1;Specular;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;133;-257.3873,-370.4496;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;82;-233.9601,2030.378;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;97;-540.3385,3339.558;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;106;-567.5364,3555.574;Inherit;False;Property;_SpecularColor;Specular Color(高光顏色);30;1;[HDR];Create;False;0;0;0;True;0;False;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;107;-197.0791,2785.457;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldSpaceCameraPos;265;2804.46,497.5743;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;51.40301,-802.3359;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;72;-142.9164,-538.8558;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;268;3355.765,382.2103;Inherit;False;Property;_FogEnd;FogEnd;55;1;[Header];Create;True;1;Linear Fog Setting;0;0;False;0;False;500;73.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;267;2875.377,312.0988;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;266;3353.672,664.5833;Inherit;False;Property;_FogStart;FogStart;54;0;Create;True;0;0;0;False;0;False;0;13;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;667.6438,-159.0945;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;99;-252.3093,3316.284;Inherit;True;4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;201;-1.40047,2027.621;Inherit;False;Rim;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;26;800.676,-394.5883;Inherit;True;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;101;240.0562,3309.223;Inherit;False;Specular;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;269;3576.595,488.4162;Inherit;False;Linear Fog;-1;;4;e327318b664bb6b4ca7fb9865de9053d;1,14,0;5;13;FLOAT;700;False;15;FLOAT;0;False;16;FLOAT3;0,0,0;False;17;FLOAT3;0,0,0;False;18;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;202;1079.665,-273.0325;Inherit;False;201;Rim;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;139;1588.464,46.05324;Inherit;True;Property;_EmissionTexture;Emission Texture;13;1;[Header];Create;True;1;Emission;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;136;1675.038,275.4757;Inherit;False;Property;_EmissionColor;Emission Color;14;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,1;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;140;1687.493,449.0935;Inherit;False;Property;_EmissionStrength;Emission Strength;15;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;270;3986.95,483.0223;Inherit;False;LinearFog;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;84;1280.921,-392.2037;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;105;1322.589,-147.2235;Inherit;False;101;Specular;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;104;1683.779,-266.7266;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;138;1958.569,221.3665;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;272;4017.006,60.10787;Inherit;False;270;LinearFog;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;135;2127.759,-261.2296;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ClampOpNode;273;4205.343,65.68939;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;281;-1489.082,2685.571;Inherit;False;SRPLightFinal;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;283;747.2128,134.0783;Inherit;False;RampFinal;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;275;4106.313,-156.8893;Inherit;False;Property;_FogColor;FogColor;53;2;[HDR];[Header];Create;True;1;Fog;0;0;False;0;False;1,1,1,0;0.254717,0.1184674,0.07305089,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;274;4548.53,-265.062;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;287;4695.937,-506.6966;Inherit;False;281;SRPLightFinal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.FunctionNode;278;4564.365,-408.5534;Inherit;False;SSAO_F;56;;6;146f1dca6fd359a4eba9e48143697ea7;0;0;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;286;4701.231,-653.0207;Inherit;False;283;RampFinal;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;235;3075.315,-72.24324;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;236;2983.754,-229.7363;Inherit;False;Property;_Alpha;Alpha;51;0;Create;True;0;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;238;3295.952,-544.7941;Inherit;False;Constant;_Clip;Clip(為0時陰影永存);46;0;Create;False;0;0;0;False;0;False;0.001;0.001;0;0.001;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;70;-1487.409,1328.731;Inherit;False;743.0731;304.7168;Comment;3;67;68;69;轉換為灰度圖;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;240;3419.446,-367.3064;Inherit;False;Constant;_Float0;Float 0;47;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DitheringNode;234;3291.031,-133.9313;Inherit;False;0;True;4;0;FLOAT;0;False;1;SAMPLER2D;;False;2;FLOAT4;0,0,0,0;False;3;SAMPLERSTATE;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;277;4784.226,-319.009;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;284;4944.233,-522.383;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;239;3591.406,-414.6838;Inherit;False;Property;_ShadowUseDither;Shadow Use Dither(陰影隨溶解消失);52;0;Create;False;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;217;3239.755,-829.9717;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;1;False;4;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;218;3065.057,-1176.426;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;5000;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;177;1516.677,-1152.396;Inherit;True;Simple;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;17.21;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;216;2870.203,-1176.045;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TransformPositionNode;227;551.0247,-1016.486;Inherit;False;World;Object;False;Fast;True;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleSubtractOpNode;219;3347.101,-1172.889;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;220;3525.141,-1175.459;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightAttenuation;100;-532.2645,3234.371;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;225;2622.054,-305.748;Inherit;False;Property;_Sprint;_Sprint(衝刺);49;0;Create;False;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;229;1069.004,-1109.43;Inherit;False;Property;_DissolveMode;Dissolve Mode;50;0;Create;True;0;0;0;False;0;False;0;0;0;True;;KeywordEnum;4;UV;WorldSpace;ScreenSpace;ViewSpace;Create;True;True;9;1;FLOAT4;0,0,0,0;False;0;FLOAT4;0,0,0,0;False;2;FLOAT4;0,0,0,0;False;3;FLOAT4;0,0,0,0;False;4;FLOAT4;0,0,0,0;False;5;FLOAT4;0,0,0,0;False;6;FLOAT4;0,0,0,0;False;7;FLOAT4;0,0,0,0;False;8;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;231;790.0043,-859.4298;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;188;325.3442,-1193.885;Inherit;False;Property;_ClipNoiseTiling;ClipNoiseTiling(死亡溶解噪點紋理拉伸);46;0;Create;False;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;68;-1229.409,1380.731;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;67;-1437.409,1380.731;Inherit;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.GetLocalVarNode;205;-1763.531,1373.2;Inherit;False;203;SRPAdditionLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;228;799.1799,-1033.167;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldPosInputsNode;226;355.0914,-1011.601;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;232;581.5745,-683.6543;Inherit;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;178;1431.006,-1342.442;Inherit;False;Property;_DeadClip;_DeadClip(死亡溶解控制);41;1;[Header];Create;False;1;Dead Dissolve Setting;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;180;1871.077,-1335.514;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;69;-979.7379,1382.048;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;215;3049.871,-830.4197;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;186;1914.779,-979.6716;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;214;2740.056,-836.1557;Inherit;False;Property;_GenerateDither;GenerateDither;42;0;Create;True;0;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;261;-3306.837,368.6276;Inherit;False;1;-1;4;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;183;2779.869,-673.2944;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;237;3679.06,-229.2217;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;271;2818.165,676.9494;Inherit;False;Property;_CircleFogStartPosition1;CircleFog Start Position;9;0;Create;True;0;0;0;False;0;False;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;184;1701.198,-973.901;Inherit;False;Property;_ClipEdgeSoftness;ClipEdgeSoftness(死亡溶解邊緣顏色平滑度);45;0;Create;False;0;0;0;False;0;False;0.01;0.01;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;233;790.5745,-683.6543;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;222;3095.285,-671.8954;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;181;2041.184,-1094.568;Inherit;False;Property;_ClipEdgeColor;ClipEdgeColor(死亡溶解顏色);43;1;[HDR];Create;False;0;0;0;False;0;False;7.464264,1.717473,0,0;7.464264,1.717473,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;175;817.5867,-1212.357;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;185;2535.896,-1120.15;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;182;1733.446,-1057.547;Inherit;False;Property;_ClipEdge;ClipEdge(死亡溶解邊緣顏色範圍);44;0;Create;False;0;0;0;False;0;False;0.99;0.99;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;230;557.0043,-859.4298;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;179;1744.385,-1144.662;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;176;1207.77,-928.9268;Inherit;False;Property;_ClipNoiseScale;ClipNoiseScale(死亡溶解噪點紋理大小);47;0;Create;False;0;0;0;False;0;False;10;10;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;285;5121.074,-392.394;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;224;2698.877,-542.126;Inherit;False;Property;_SprintColor;Sprint Color;48;1;[HDR];Create;True;0;0;0;False;0;False;2.297397,0.9640058,0.2072162,0;2.297397,0.9640058,0.2072162,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;221;3561.303,-824.9984;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;187;2273.031,-1341.66;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0.2;False;2;FLOAT;0.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;61;5272.41,-246.0892;Float;False;True;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;To7/Toon Shader_PolybrushPainter_GardientAlpha;2992e84f91cbeb14eab234972e07ea9d;True;Forward;0;1;Forward;8;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;2;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Transparent=RenderType;Queue=AlphaTest=Queue=0;True;7;0;False;True;1;1;False;-1;0;False;-1;1;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;True;True;True;True;0;False;-1;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;1;LightMode=UniversalForward;False;0;wiiu;0;0;Standard;22;Surface;0;  Blend;0;Two Sided;1;Cast Shadows;1;  Use Shadow Threshold;0;Receive Shadows;1;GPU Instancing;1;LOD CrossFade;0;Built-in Fog;0;DOTS Instancing;0;Meta Pass;1;Extra Pre Pass;0;Tessellation;0;  Phong;1;  Strength;0.5,False,-1;  Type;0;  Tess;16,False,-1;  Min;10,False,-1;  Max;25,False,-1;  Edge Length;16,False,-1;  Max Displacement;25,False,-1;Vertex Position,InvertActionOnDeselection;1;0;5;False;True;True;True;True;False;;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;62;1360.861,-0.1071291;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;ShadowCaster;0;2;ShadowCaster;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;False;-1;True;3;False;-1;False;True;1;LightMode=ShadowCaster;False;0;wiiu;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;64;1360.861,-0.1071291;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;Meta;0;4;Meta;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;LightMode=Meta;False;0;wiiu;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;60;3903.117,606.4792;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;ExtraPrePass;0;0;Outline;5;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;True;1;1;False;-1;0;False;-1;0;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;True;True;True;True;0;False;-1;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;0;False;0;wiiu;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;63;1360.861,-0.1071291;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;DepthOnly;0;3;DepthOnly;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;False;False;True;False;False;False;False;0;False;-1;False;False;False;False;False;False;False;False;False;True;1;False;-1;False;False;True;1;LightMode=DepthOnly;False;0;wiiu;0;0;Standard;0;False;0
WireConnection;42;5;44;0
WireConnection;263;0;42;0
WireConnection;263;1;262;0
WireConnection;263;2;251;1
WireConnection;161;0;159;0
WireConnection;161;1;160;0
WireConnection;158;0;12;0
WireConnection;158;1;159;0
WireConnection;158;2;161;0
WireConnection;43;0;263;0
WireConnection;124;0;14;1
WireConnection;124;1;14;2
WireConnection;124;2;125;0
WireConnection;15;0;158;0
WireConnection;15;1;124;0
WireConnection;16;0;15;0
WireConnection;65;2;112;0
WireConnection;257;0;8;0
WireConnection;257;1;253;0
WireConnection;258;0;245;0
WireConnection;258;1;254;0
WireConnection;203;0;65;0
WireConnection;2;0;45;0
WireConnection;17;0;16;0
WireConnection;17;1;16;1
WireConnection;3;0;2;0
WireConnection;3;1;1;0
WireConnection;18;0;17;0
WireConnection;18;1;16;2
WireConnection;259;0;246;0
WireConnection;259;1;255;0
WireConnection;248;0;257;0
WireConnection;248;1;258;0
WireConnection;248;2;251;2
WireConnection;260;0;247;0
WireConnection;260;1;256;0
WireConnection;5;0;3;0
WireConnection;5;1;131;0
WireConnection;108;0;206;0
WireConnection;108;1;111;0
WireConnection;75;0;73;0
WireConnection;75;1;74;0
WireConnection;249;0;248;0
WireConnection;249;1;259;0
WireConnection;249;2;251;3
WireConnection;71;0;18;0
WireConnection;76;0;75;0
WireConnection;76;1;77;0
WireConnection;192;0;191;0
WireConnection;192;1;193;0
WireConnection;109;0;108;0
WireConnection;250;0;249;0
WireConnection;250;1;260;0
WireConnection;250;2;251;4
WireConnection;90;0;91;0
WireConnection;89;0;87;0
WireConnection;89;1;88;1
WireConnection;13;0;5;0
WireConnection;13;1;71;0
WireConnection;163;1;172;0
WireConnection;78;0;76;0
WireConnection;92;0;89;0
WireConnection;92;1;90;0
WireConnection;166;0;163;1
WireConnection;166;1;169;0
WireConnection;166;2;170;0
WireConnection;280;0;115;1
WireConnection;280;1;115;2
WireConnection;110;0;109;0
WireConnection;110;1;111;0
WireConnection;190;0;192;0
WireConnection;190;1;13;0
WireConnection;190;2;196;0
WireConnection;197;0;6;0
WireConnection;242;0;250;0
WireConnection;209;0;15;0
WireConnection;165;0;190;0
WireConnection;165;1;166;0
WireConnection;189;0;85;1
WireConnection;189;1;85;2
WireConnection;114;0;110;0
WireConnection;114;1;115;1
WireConnection;114;2;280;0
WireConnection;93;0;92;0
WireConnection;93;1;94;0
WireConnection;80;0;78;0
WireConnection;117;0;212;0
WireConnection;117;1;120;1
WireConnection;11;0;10;0
WireConnection;11;1;243;0
WireConnection;81;0;80;0
WireConnection;81;1;85;1
WireConnection;81;2;189;0
WireConnection;7;0;198;0
WireConnection;7;1;165;0
WireConnection;122;1;165;0
WireConnection;24;0;173;0
WireConnection;95;0;93;0
WireConnection;95;1;96;1
WireConnection;95;2;96;2
WireConnection;119;0;114;0
WireConnection;119;1;120;2
WireConnection;123;1;7;0
WireConnection;123;0;122;0
WireConnection;19;0;11;0
WireConnection;19;1;210;0
WireConnection;133;0;24;0
WireConnection;133;1;134;0
WireConnection;82;0;83;0
WireConnection;82;1;81;0
WireConnection;82;2;212;0
WireConnection;97;0;95;0
WireConnection;97;1;98;0
WireConnection;107;0;117;0
WireConnection;107;1;119;0
WireConnection;25;0;11;0
WireConnection;25;1;133;0
WireConnection;72;0;11;0
WireConnection;72;1;204;0
WireConnection;9;0;19;0
WireConnection;9;1;123;0
WireConnection;99;0;107;0
WireConnection;99;1;97;0
WireConnection;99;2;106;0
WireConnection;99;3;116;0
WireConnection;201;0;82;0
WireConnection;26;0;25;0
WireConnection;26;1;9;0
WireConnection;26;2;72;0
WireConnection;101;0;99;0
WireConnection;269;13;268;0
WireConnection;269;15;266;0
WireConnection;269;16;267;0
WireConnection;269;17;265;0
WireConnection;270;0;269;0
WireConnection;84;0;26;0
WireConnection;84;1;202;0
WireConnection;104;0;84;0
WireConnection;104;1;105;0
WireConnection;138;0;139;0
WireConnection;138;1;136;0
WireConnection;138;2;140;0
WireConnection;135;0;104;0
WireConnection;135;1;138;0
WireConnection;273;0;272;0
WireConnection;281;0;114;0
WireConnection;283;0;123;0
WireConnection;274;0;135;0
WireConnection;274;1;275;0
WireConnection;274;2;273;0
WireConnection;234;0;236;0
WireConnection;234;2;235;0
WireConnection;277;0;278;0
WireConnection;277;1;274;0
WireConnection;284;0;286;0
WireConnection;284;1;287;0
WireConnection;239;1;240;0
WireConnection;239;0;238;0
WireConnection;217;0;215;0
WireConnection;218;0;216;0
WireConnection;177;0;229;0
WireConnection;177;1;176;0
WireConnection;227;0;226;0
WireConnection;219;0;218;0
WireConnection;219;1;217;0
WireConnection;220;0;219;0
WireConnection;229;1;175;0
WireConnection;229;0;228;0
WireConnection;229;2;231;0
WireConnection;229;3;233;0
WireConnection;231;0;188;0
WireConnection;231;1;230;0
WireConnection;68;0;67;0
WireConnection;68;1;67;1
WireConnection;67;0;205;0
WireConnection;228;0;188;0
WireConnection;228;1;226;0
WireConnection;180;0;178;0
WireConnection;180;1;179;0
WireConnection;69;0;68;0
WireConnection;69;1;67;2
WireConnection;215;0;214;0
WireConnection;186;0;182;0
WireConnection;186;1;184;0
WireConnection;183;0;185;0
WireConnection;183;1;135;0
WireConnection;237;0;236;0
WireConnection;237;1;234;0
WireConnection;233;0;188;0
WireConnection;233;1;232;0
WireConnection;222;0;183;0
WireConnection;222;1;224;0
WireConnection;222;2;225;0
WireConnection;175;0;188;0
WireConnection;185;0;187;0
WireConnection;185;1;181;0
WireConnection;179;0;177;0
WireConnection;285;0;284;0
WireConnection;285;1;277;0
WireConnection;221;0;220;0
WireConnection;221;1;180;0
WireConnection;187;0;180;0
WireConnection;187;1;182;0
WireConnection;187;2;186;0
WireConnection;61;0;285;0
WireConnection;61;2;285;0
WireConnection;61;3;237;0
WireConnection;61;4;239;0
ASEEND*/
//CHKSM=ABDB211E7278B03E2AF81CC4F40CD4BF5D91E28B