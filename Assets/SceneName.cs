using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class SceneName : MonoBehaviour
{
    public RectTransform bgRect;
    public RectTransform mainTitle;
    public RectTransform secondTitle;

    public float bgDuringSec = 0.5f;
    public float mainDuringSec = 0.5f;
    public float secondDuringSec = 0.5f;
    public float waitTime = 1;

    private Vector2 bgOriSize;
    private Vector2 mainTitleOriPos;
    private Vector2 secondTitleOriPos;

    public RectTransform leftPoint;
    public RectTransform rightPoint;

    private void Start()
    {
        bgOriSize = bgRect.sizeDelta;
        mainTitleOriPos = mainTitle.anchoredPosition;
        secondTitleOriPos = secondTitle.anchoredPosition;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
            MoveIn();
    }

    public void MoveIn()
    {
        bgRect.DOSizeDelta(bgOriSize, bgDuringSec).SetEase(Ease.OutBack).From(new Vector2(bgOriSize.x, 0));
        mainTitle.DOAnchorPosX(mainTitleOriPos.x, mainDuringSec).From(new Vector2(leftPoint.anchoredPosition.x, mainTitleOriPos.y)).SetEase(Ease.OutCubic);
        secondTitle.DOAnchorPosX(secondTitleOriPos.x, secondDuringSec).From(new Vector2(rightPoint.anchoredPosition.x, secondTitleOriPos.y)).SetEase(Ease.OutCubic);
        bgRect.gameObject.SetActive(true);
        Invoke("MoveOut", waitTime + bgDuringSec);
    }

    public void MoveOut()
    {
        bgRect.DOSizeDelta(new Vector2(bgOriSize.x, 0), bgDuringSec).SetEase(Ease.OutBack);
        mainTitle.DOAnchorPosX(rightPoint.anchoredPosition.x, mainDuringSec).SetEase(Ease.OutCubic);
        secondTitle.DOAnchorPosX(leftPoint.anchoredPosition.x, secondDuringSec).SetEase(Ease.OutCubic).OnComplete(() =>{
            bgRect.gameObject.SetActive(false);
        });
    }
}
