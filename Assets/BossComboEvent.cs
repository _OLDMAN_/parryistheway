using UnityEngine;

public class BossComboEvent : MonoBehaviour
{
    public BossManagerF bossManager;
    private Animator animator;
    public RuntimeAnimatorController P1animator;
    public int P1comboTotal;
    public RuntimeAnimatorController P2animator;
    public int P2comboTotal;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }
    public void ChangeAnimatorController(BossStatus stage)
    {
        if(stage == BossStatus.FirstStage)
            animator.runtimeAnimatorController = P1animator;
        else
            animator.runtimeAnimatorController = P2animator;
    }
    public void PlayRandCombo()
    {
        int r;
        if (bossManager.bossStatus.stage == BossStatus.FirstStage)
        {
            r = Random.Range(1, P1comboTotal + 1);
        }
        else
        {
            r = Random.Range(1, P2comboTotal + 1);
        }
        animator.SetInteger("ComboIndex", r);
    }

    public void MeleeShort_L()
    {
        bossManager.hand_L.HitTargetPoint(true);
    }
    public void MeleeShort_R()
    {
        bossManager.hand_R.HitTargetPoint(true);
    }
    public void MeleeLong_L()
    {
        bossManager.hand_L.HitTargetPoint(false);
    }
    public void MeleeLong_R()
    {
        bossManager.hand_R.HitTargetPoint(false);
    }
    public void MeleeLongWithCircle_L()
    {
        bossManager.hand_L.HitTargetPointCircle();
    }
    public void MeleeLongWithCircle_R()
    {
        bossManager.hand_R.HitTargetPointCircle();
    }

    public void ShootShort()
    {
        bossManager.shoot.Shoot(BossShootType.ShortShoot);
    }
    public void ShootLong()
    {
        bossManager.shoot.Shoot(BossShootType.LongShoot);
    }
    public void ContinuousShootShort()
    {
        bossManager.shoot.Shoot(BossShootType.ContinuousShootShort);
    }
    public void ContinuousShootLong()
    {
        bossManager.shoot.Shoot(BossShootType.ContinuousShootLong);
    }
    public void Strafing()
    {
        bossManager.shoot.Shoot(BossShootType.Strafing);
    }

    public void Wave_L()
    {
        bossManager.ctrl.SpawnWave_L();
    }
    public void Wave_R()
    {
        bossManager.ctrl.SpawnWave_R();
    }

    public void Relax(bool autoPlay = true)
    {
        float recoverTime = bossManager.bossBreak.recoverTime;
        //bossManager.ctrl.Relax(recoverTime);
        bossManager.hand_L.Relax(recoverTime);
        bossManager.hand_R.Relax(recoverTime);
        if (autoPlay == true)
            Invoke("PlayRandCombo", recoverTime);
    }
    public void ComboEnd()
    {
        //讓手回到BOSS旁
        animator.SetInteger("ComboIndex", 0);
        Relax();
        animator.Play("Empty");
    }

    public void ResetEmpty()
    {
        CancelInvoke();
        animator.SetInteger("ComboIndex", 0);
        animator.Play("Empty");
    }

    public void Empty()
    {
        animator.Play("Empty");
    }
}
