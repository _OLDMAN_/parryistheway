using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleDestory : MonoBehaviour
{
    private float maxLifetime;
    
    public void AllParticleDestory(GameObject FatherParticleSystem)
    {
        //獲取所有子孫物件的Transform組件返回給Component數組
        ParticleSystem[] childrenParticle = FatherParticleSystem.GetComponentsInChildren<ParticleSystem>(); 

        //遍歷所有子物件的Transform組件
        foreach(ParticleSystem child in childrenParticle)
        {   
            //遍歷所有子物件的粒子系統startlifetime
            //新增一個maxLifetime做比較 每次比較大於maxLifetime時 將該值賦於給maxLifetime
            if(maxLifetime < child.main.startLifetimeMultiplier)
            {
                maxLifetime = child.main.startLifetimeMultiplier;
            }
        }
        //檢查粒子有無開關looping
        ParticleSystem ps = FatherParticleSystem.GetComponent<ParticleSystem>();
        var main = ps.main;
        if (main.loop == true)
        {
            //粒子系統取消發射 包含子孫粒子系統
            ps.Stop(true , ParticleSystemStopBehavior.StopEmitting);
        }

        //協程初始化(若協程從別的類調用 則此腳本不會啟動Start()方法 故協程不會初始化 得要自己手動)
        //ParticleDestory fatherParticleDestory = FatherParticleSystem.GetComponent<ParticleDestory>();
        //調用協程函數
        StartCoroutine(LastDestory(FatherParticleSystem));

    }
    IEnumerator LastDestory(GameObject FatherParticleSystem)
    {
        //在maxLifetime秒過後 並在該秒後一禎執行完畢後執行下列代碼
        yield return new WaitForSecondsRealtime(maxLifetime);
        //刪掉父物件
        Destroy(FatherParticleSystem);
    }
}
