using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class VerticesGet : MonoBehaviour
{
    public GameObject Object;
    public Mesh capMesh;
    [Range(0, 360)]
    public int angle;
    // Start is called before the first frame update
    void Start()
    {
        //capMesh = GetComponent<MeshFilter>().mesh;
    }

    // Update is called once per frame
    void Update()
    {
        
        meshCount();
    }
    void meshCount()
    {
        transform.position = Object.transform.position;
        Color[] meshColor = new Color[capMesh.vertexCount];
        Vector3[] xx = capMesh.vertices;

        for (int i = 0 ; i < capMesh.vertexCount ; i ++)
        {
            meshColor[i] = Color.white;

            float a = Vector3.Angle(Object.transform.forward, capMesh.vertices[i]);

            if(a > angle / 2)
            meshColor[i] = new Color(1 , 1 , 1 , 0);

            Physics.Raycast(transform.position, capMesh.vertices[i] , out RaycastHit hit, capMesh.vertices[i].magnitude);
            if(hit.collider != null)  //! 撞到了
            {
                meshColor[i] = new Color(1 , 1 , 1 , 0);
            }
        }
        capMesh.colors = meshColor;
    }


}
