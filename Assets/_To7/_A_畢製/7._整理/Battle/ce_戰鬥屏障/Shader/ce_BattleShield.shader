// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ce_BattleShield"
{
	Properties
	{
		[HideInInspector] _EmissionColor("Emission Color", Color) = (1,1,1,1)
		[HideInInspector] _AlphaCutoff("Alpha Cutoff ", Range(0, 1)) = 0.5
		[ASEBegin][Header(Block Texture)]_TextureSample2("Block Texture", 2D) = "white" {}
		_iconTiling("Block Texture ST", Vector) = (1,1,0,0)
		[Header(Player Setting)]_PlayerPosition("PlayerPosition", Vector) = (0,0,0,0)
		[HDR]_iconcolor("Block Color", Color) = (1,1,1,1)
		_DistanceEdgeSmoothness("Distance Edge Smoothness", Float) = 1
		_DistanceValue("Distance Value", Float) = 0
		[Header(All Wind Mask Ctrl)]_DepthAlpha("Depth Alpha", Float) = 0
		[Header(Wind Setting)][Header(Wind Texture 1)]_WindNoiseTexture("Wind Texture 1", 2D) = "white" {}
		[KeywordEnum(UseWind,UseAurora)] _ShieldMode("Shield Mode", Float) = 0
		_WindNoiseST("Wind Noise 1 ST", Vector) = (1,1,0,0)
		[HDR]_Color0("Wind Texture 1 Color", Color) = (0,0,0,0)
		[Header(Wind Texture 2)]_TextureSample1("Wind Texture 2", 2D) = "white" {}
		_WindTexture("Wind Texture 2 ST", Vector) = (1,1,0,-0.2)
		[HDR]_WindColor("Wind TExture 2 Color", Color) = (1,1,1,1)
		_WindFieldMaskTexture("Wind Field Mask Texture", 2D) = "white" {}
		_MaskScale("Wind Field Mask Scale", Range( 0 , 1)) = 0
		_MaskSize("Wind Field Texture Size", Float) = 0
		[HDR][Header(Aurora Setting)]_AyroraColor("Ayrora Color", Color) = (1,1,1,1)
		[HDR]_AyroraButtomColor("Ayrora Buttom Color", Color) = (0,0,0,0)
		_AyroraAlpha("Ayrora Alpha", Float) = 0
		_ColorGradientValue("Color Gradient Value", Float) = 0
		_ColorGradientSmoothness("Color Gradient Smoothness", Float) = 0
		_AuroraMaskStrength("Aurora Mask Strength", Float) = 1
		_AuroraTexture1("Aurora Texture 1", 2D) = "white" {}
		_AuroraTexture1ST("Aurora Texture 1  ST", Vector) = (1,1,0,0)
		_AuroraTexture2("Aurora Texture 2", 2D) = "white" {}
		_AuroraTexture2ST("Aurora Texture 2 ST", Vector) = (1,1,0,0)
		[Toggle(_USEBLOCK_ON)] _UseBlock("Use Block", Float) = 0
		_AuroraNoise1ST("Aurora Noise 1 ST", Vector) = (1,1,0,0)
		_AuroraNoise1NoiseScale("Aurora Noise 1 Noise Scale", Float) = 4.93
		_AuroraNoise2ST("Aurora Noise 2 ST", Vector) = (1,1,0,0)
		[ASEEnd]_AuroraNoise2NoiseScale("Aurora Noise 2 Noise Scale", Float) = 5.98

		//_TessPhongStrength( "Tess Phong Strength", Range( 0, 1 ) ) = 0.5
		//_TessValue( "Tess Max Tessellation", Range( 1, 32 ) ) = 16
		//_TessMin( "Tess Min Distance", Float ) = 10
		//_TessMax( "Tess Max Distance", Float ) = 25
		//_TessEdgeLength ( "Tess Edge length", Range( 2, 50 ) ) = 16
		//_TessMaxDisp( "Tess Max Displacement", Float ) = 25
	}

	SubShader
	{
		LOD 0

		
		Tags { "RenderPipeline"="UniversalPipeline" "RenderType"="Transparent" "Queue"="Transparent" }
		
		Cull Off
		AlphaToMask Off
		HLSLINCLUDE
		#pragma target 2.0

		#ifndef ASE_TESS_FUNCS
		#define ASE_TESS_FUNCS
		float4 FixedTess( float tessValue )
		{
			return tessValue;
		}
		
		float CalcDistanceTessFactor (float4 vertex, float minDist, float maxDist, float tess, float4x4 o2w, float3 cameraPos )
		{
			float3 wpos = mul(o2w,vertex).xyz;
			float dist = distance (wpos, cameraPos);
			float f = clamp(1.0 - (dist - minDist) / (maxDist - minDist), 0.01, 1.0) * tess;
			return f;
		}

		float4 CalcTriEdgeTessFactors (float3 triVertexFactors)
		{
			float4 tess;
			tess.x = 0.5 * (triVertexFactors.y + triVertexFactors.z);
			tess.y = 0.5 * (triVertexFactors.x + triVertexFactors.z);
			tess.z = 0.5 * (triVertexFactors.x + triVertexFactors.y);
			tess.w = (triVertexFactors.x + triVertexFactors.y + triVertexFactors.z) / 3.0f;
			return tess;
		}

		float CalcEdgeTessFactor (float3 wpos0, float3 wpos1, float edgeLen, float3 cameraPos, float4 scParams )
		{
			float dist = distance (0.5 * (wpos0+wpos1), cameraPos);
			float len = distance(wpos0, wpos1);
			float f = max(len * scParams.y / (edgeLen * dist), 1.0);
			return f;
		}

		float DistanceFromPlane (float3 pos, float4 plane)
		{
			float d = dot (float4(pos,1.0f), plane);
			return d;
		}

		bool WorldViewFrustumCull (float3 wpos0, float3 wpos1, float3 wpos2, float cullEps, float4 planes[6] )
		{
			float4 planeTest;
			planeTest.x = (( DistanceFromPlane(wpos0, planes[0]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[0]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[0]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.y = (( DistanceFromPlane(wpos0, planes[1]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[1]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[1]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.z = (( DistanceFromPlane(wpos0, planes[2]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[2]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[2]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.w = (( DistanceFromPlane(wpos0, planes[3]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[3]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[3]) > -cullEps) ? 1.0f : 0.0f );
			return !all (planeTest);
		}

		float4 DistanceBasedTess( float4 v0, float4 v1, float4 v2, float tess, float minDist, float maxDist, float4x4 o2w, float3 cameraPos )
		{
			float3 f;
			f.x = CalcDistanceTessFactor (v0,minDist,maxDist,tess,o2w,cameraPos);
			f.y = CalcDistanceTessFactor (v1,minDist,maxDist,tess,o2w,cameraPos);
			f.z = CalcDistanceTessFactor (v2,minDist,maxDist,tess,o2w,cameraPos);

			return CalcTriEdgeTessFactors (f);
		}

		float4 EdgeLengthBasedTess( float4 v0, float4 v1, float4 v2, float edgeLength, float4x4 o2w, float3 cameraPos, float4 scParams )
		{
			float3 pos0 = mul(o2w,v0).xyz;
			float3 pos1 = mul(o2w,v1).xyz;
			float3 pos2 = mul(o2w,v2).xyz;
			float4 tess;
			tess.x = CalcEdgeTessFactor (pos1, pos2, edgeLength, cameraPos, scParams);
			tess.y = CalcEdgeTessFactor (pos2, pos0, edgeLength, cameraPos, scParams);
			tess.z = CalcEdgeTessFactor (pos0, pos1, edgeLength, cameraPos, scParams);
			tess.w = (tess.x + tess.y + tess.z) / 3.0f;
			return tess;
		}

		float4 EdgeLengthBasedTessCull( float4 v0, float4 v1, float4 v2, float edgeLength, float maxDisplacement, float4x4 o2w, float3 cameraPos, float4 scParams, float4 planes[6] )
		{
			float3 pos0 = mul(o2w,v0).xyz;
			float3 pos1 = mul(o2w,v1).xyz;
			float3 pos2 = mul(o2w,v2).xyz;
			float4 tess;

			if (WorldViewFrustumCull(pos0, pos1, pos2, maxDisplacement, planes))
			{
				tess = 0.0f;
			}
			else
			{
				tess.x = CalcEdgeTessFactor (pos1, pos2, edgeLength, cameraPos, scParams);
				tess.y = CalcEdgeTessFactor (pos2, pos0, edgeLength, cameraPos, scParams);
				tess.z = CalcEdgeTessFactor (pos0, pos1, edgeLength, cameraPos, scParams);
				tess.w = (tess.x + tess.y + tess.z) / 3.0f;
			}
			return tess;
		}
		#endif //ASE_TESS_FUNCS

		ENDHLSL

		
		Pass
		{
			
			Name "Forward"
			Tags { "LightMode"="UniversalForward" }
			
			Blend SrcAlpha OneMinusSrcAlpha, OneMinusDstColor One
			ZWrite Off
			ZTest LEqual
			Offset 0 , 0
			ColorMask RGBA
			

			HLSLPROGRAM
			#define _RECEIVE_SHADOWS_OFF 1
			#pragma multi_compile_instancing
			#define ASE_SRP_VERSION 100302
			#define REQUIRE_DEPTH_TEXTURE 1

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/UnityInstancing.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

			#if ASE_SRP_VERSION <= 70108
			#define REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR
			#endif

			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#pragma shader_feature_local _SHIELDMODE_USEWIND _SHIELDMODE_USEAURORA
			#pragma shader_feature_local _USEBLOCK_ON


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				#ifdef ASE_FOG
				float fogFactor : TEXCOORD2;
				#endif
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_texcoord4 : TEXCOORD4;
				float4 ase_texcoord5 : TEXCOORD5;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _WindNoiseST;
			float4 _AyroraButtomColor;
			float4 _AyroraColor;
			float4 _AuroraTexture2ST;
			float4 _AuroraTexture1ST;
			float4 _AuroraNoise2ST;
			float4 _iconcolor;
			float4 _AuroraNoise1ST;
			float4 _WindColor;
			float4 _WindTexture;
			float4 _Color0;
			float4 _iconTiling;
			float3 _PlayerPosition;
			float _DistanceEdgeSmoothness;
			float _AuroraNoise2NoiseScale;
			float _AyroraAlpha;
			float _AuroraNoise1NoiseScale;
			float _DistanceValue;
			float _ColorGradientValue;
			float _ColorGradientSmoothness;
			float _DepthAlpha;
			float _MaskSize;
			float _MaskScale;
			float _AuroraMaskStrength;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			sampler2D _WindNoiseTexture;
			sampler2D _TextureSample1;
			sampler2D _TextureSample2;
			sampler2D _AuroraTexture1;
			sampler2D _AuroraTexture2;
			uniform float4 _CameraDepthTexture_TexelSize;
			sampler2D _WindFieldMaskTexture;


			//https://www.shadertoy.com/view/XdXGW8
			float2 GradientNoiseDir( float2 x )
			{
				const float2 k = float2( 0.3183099, 0.3678794 );
				x = x * k + k.yx;
				return -1.0 + 2.0 * frac( 16.0 * k * frac( x.x * x.y * ( x.x + x.y ) ) );
			}
			
			float GradientNoise( float2 UV, float Scale )
			{
				float2 p = UV * Scale;
				float2 i = floor( p );
				float2 f = frac( p );
				float2 u = f * f * ( 3.0 - 2.0 * f );
				return lerp( lerp( dot( GradientNoiseDir( i + float2( 0.0, 0.0 ) ), f - float2( 0.0, 0.0 ) ),
						dot( GradientNoiseDir( i + float2( 1.0, 0.0 ) ), f - float2( 1.0, 0.0 ) ), u.x ),
						lerp( dot( GradientNoiseDir( i + float2( 0.0, 1.0 ) ), f - float2( 0.0, 1.0 ) ),
						dot( GradientNoiseDir( i + float2( 1.0, 1.0 ) ), f - float2( 1.0, 1.0 ) ), u.x ), u.y );
			}
			
			float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }
			float snoise( float2 v )
			{
				const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
				float2 i = floor( v + dot( v, C.yy ) );
				float2 x0 = v - i + dot( i, C.xx );
				float2 i1;
				i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
				float4 x12 = x0.xyxy + C.xxzz;
				x12.xy -= i1;
				i = mod2D289( i );
				float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
				float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
				m = m * m;
				m = m * m;
				float3 x = 2.0 * frac( p * C.www ) - 1.0;
				float3 h = abs( x ) - 0.5;
				float3 ox = floor( x + 0.5 );
				float3 a0 = x - ox;
				m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
				float3 g;
				g.x = a0.x * x0.x + h.x * x0.y;
				g.yz = a0.yz * x12.xz + h.yz * x12.yw;
				return 130.0 * dot( m, g );
			}
			
			
			VertexOutput VertexFunction ( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float4 ase_clipPos = TransformObjectToHClip((v.vertex).xyz);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord5 = screenPos;
				
				o.ase_texcoord3.xy = v.ase_texcoord.xy;
				o.ase_texcoord4 = v.vertex;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord3.zw = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = defaultVertexValue;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif
				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );
				float4 positionCS = TransformWorldToHClip( positionWS );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				VertexPositionInputs vertexInput = (VertexPositionInputs)0;
				vertexInput.positionWS = positionWS;
				vertexInput.positionCS = positionCS;
				o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				#ifdef ASE_FOG
				o.fogFactor = ComputeFogFactor( positionCS.z );
				#endif
				o.clipPos = positionCS;
				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord = v.ase_texcoord;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag ( VertexOutput IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif
				float2 appendResult56 = (float2(_WindNoiseST.x , _WindNoiseST.y));
				float2 appendResult57 = (float2(_WindNoiseST.z , _WindNoiseST.w));
				float2 texCoord60 = IN.ase_texcoord3.xy * appendResult56 + ( _TimeParameters.x * appendResult57 );
				float4 tex2DNode5 = tex2D( _WindNoiseTexture, texCoord60 );
				float2 appendResult22 = (float2(_WindTexture.x , _WindTexture.y));
				float2 appendResult23 = (float2(( _WindTexture.z * _TimeParameters.x ) , ( _WindTexture.w * _TimeParameters.x )));
				float2 texCoord18 = IN.ase_texcoord3.xy * appendResult22 + appendResult23;
				float4 tex2DNode16 = tex2D( _TextureSample1, texCoord18 );
				float4 temp_cast_0 = (0.0).xxxx;
				float smoothstepResult50 = smoothstep( _DistanceValue , ( _DistanceValue + _DistanceEdgeSmoothness ) , distance( _PlayerPosition , WorldPosition ));
				float2 appendResult39 = (float2(_iconTiling.x , _iconTiling.y));
				float2 appendResult40 = (float2(_iconTiling.z , _iconTiling.w));
				float2 texCoord36 = IN.ase_texcoord3.xy * appendResult39 + appendResult40;
				float temp_output_41_0 = ( ( 1.0 - smoothstepResult50 ) * tex2D( _TextureSample2, texCoord36 ).r * _iconcolor.a );
				#ifdef _USEBLOCK_ON
				float4 staticSwitch150 = ( temp_output_41_0 * _iconcolor );
				#else
				float4 staticSwitch150 = temp_cast_0;
				#endif
				float4 BlockTextureColorFinish137 = staticSwitch150;
				float2 appendResult118 = (float2(_AuroraNoise2ST.z , _AuroraNoise2ST.w));
				float2 appendResult116 = (float2(_AuroraNoise2ST.x , _AuroraNoise2ST.y));
				float2 texCoord117 = IN.ase_texcoord3.xy * appendResult116 + float2( 0,0 );
				float2 panner114 = ( 1.0 * _Time.y * appendResult118 + texCoord117);
				float gradientNoise119 = GradientNoise(panner114,_AuroraNoise2NoiseScale);
				gradientNoise119 = gradientNoise119*0.5 + 0.5;
				float2 appendResult113 = (float2(_AuroraNoise1ST.z , _AuroraNoise1ST.w));
				float2 appendResult111 = (float2(_AuroraNoise1ST.x , _AuroraNoise1ST.y));
				float2 texCoord112 = IN.ase_texcoord3.xy * appendResult111 + float2( 0,0 );
				float2 panner109 = ( 1.0 * _Time.y * appendResult113 + texCoord112);
				float simplePerlin2D108 = snoise( panner109*_AuroraNoise1NoiseScale );
				simplePerlin2D108 = simplePerlin2D108*0.5 + 0.5;
				float2 appendResult96 = (float2(_AuroraTexture1ST.z , _AuroraTexture1ST.w));
				float2 appendResult95 = (float2(_AuroraTexture1ST.x , _AuroraTexture1ST.y));
				float2 texCoord92 = IN.ase_texcoord3.xy * appendResult95 + float2( 0,0 );
				float2 panner93 = ( 1.0 * _Time.y * appendResult96 + texCoord92);
				float2 appendResult129 = (float2(_AuroraTexture2ST.z , _AuroraTexture2ST.w));
				float2 appendResult128 = (float2(_AuroraTexture2ST.x , _AuroraTexture2ST.y));
				float2 texCoord130 = IN.ase_texcoord3.xy * appendResult128 + float2( 0,0 );
				float2 panner131 = ( 1.0 * _Time.y * appendResult129 + texCoord130);
				float4 temp_output_124_0 = ( ( ( gradientNoise119 + simplePerlin2D108 ) / 1.0 ) + ( tex2D( _AuroraTexture1, panner93 ) * tex2D( _AuroraTexture2, panner131 ) ) );
				float smoothstepResult166 = smoothstep( _ColorGradientValue , ( _ColorGradientValue + _ColorGradientSmoothness ) , IN.ase_texcoord4.xyz.z);
				float4 lerpResult159 = lerp( _AyroraColor , _AyroraButtomColor , smoothstepResult166);
				#if defined(_SHIELDMODE_USEWIND)
				float4 staticSwitch141 = ( ( tex2DNode5 * _Color0 ) + ( ( tex2DNode16 * _WindColor ) + staticSwitch150 ) );
				#elif defined(_SHIELDMODE_USEAURORA)
				float4 staticSwitch141 = ( BlockTextureColorFinish137 + ( temp_output_124_0 * lerpResult159 ) );
				#else
				float4 staticSwitch141 = ( ( tex2DNode5 * _Color0 ) + ( ( tex2DNode16 * _WindColor ) + staticSwitch150 ) );
				#endif
				
				#ifdef _USEBLOCK_ON
				float staticSwitch149 = temp_output_41_0;
				#else
				float staticSwitch149 = 0.0;
				#endif
				float4 screenPos = IN.ase_texcoord5;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float screenDepth62 = LinearEyeDepth(SHADERGRAPH_SAMPLE_SCENE_DEPTH( ase_screenPosNorm.xy ),_ZBufferParams);
				float distanceDepth62 = abs( ( screenDepth62 - LinearEyeDepth( ase_screenPosNorm.z,_ZBufferParams ) ) / ( _DepthAlpha ) );
				float temp_output_64_0 = saturate( distanceDepth62 );
				float2 texCoord69 = IN.ase_texcoord3.xy * float2( 1,1 ) + float2( 0,0 );
				float2 panner80 = ( 1.0 * _Time.y * float2( -0.09,0 ) + texCoord69);
				float gradientNoise78 = GradientNoise(panner80,3.74);
				gradientNoise78 = gradientNoise78*0.5 + 0.5;
				float4 tex2DNode73 = tex2D( _WindFieldMaskTexture, ( texCoord69 + ( ( texCoord69 - float2( 0.5,0.5 ) ) * _MaskSize ) ) );
				float saferPower77 = max( ( tex2DNode73.g * tex2DNode73.b ) , 0.0001 );
				float lerpResult75 = lerp( ( gradientNoise78 * tex2DNode73.r ) , pow( saferPower77 , 0.29 ) , _MaskScale);
				float Mask152 = lerpResult75;
				float BlockTextureAlphaFinish138 = staticSwitch149;
				float4 break126 = temp_output_124_0;
				float2 texCoord101 = IN.ase_texcoord3.xy * float2( 1,1 ) + float2( 0,0 );
				float DepthAlpha135 = temp_output_64_0;
				#if defined(_SHIELDMODE_USEWIND)
				float staticSwitch142 = ( staticSwitch149 + ( temp_output_64_0 * ( ( _Color0.a * ( tex2DNode5.r * tex2DNode5.a ) ) + ( tex2DNode16.a * _WindColor.a ) ) * Mask152 ) );
				#elif defined(_SHIELDMODE_USEAURORA)
				float staticSwitch142 = ( ( ( BlockTextureAlphaFinish138 * 0.5 ) + ( break126.r * break126.a * _AyroraAlpha * saturate( ( texCoord101.y + _AuroraMaskStrength ) ) * DepthAlpha135 ) ) * Mask152 );
				#else
				float staticSwitch142 = ( staticSwitch149 + ( temp_output_64_0 * ( ( _Color0.a * ( tex2DNode5.r * tex2DNode5.a ) ) + ( tex2DNode16.a * _WindColor.a ) ) * Mask152 ) );
				#endif
				
				float3 BakedAlbedo = 0;
				float3 BakedEmission = 0;
				float3 Color = staticSwitch141.rgb;
				float Alpha = staticSwitch142;
				float AlphaClipThreshold = 0.5;
				float AlphaClipThresholdShadow = 0.5;

				#ifdef _ALPHATEST_ON
					clip( Alpha - AlphaClipThreshold );
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif

				#ifdef ASE_FOG
					Color = MixFog( Color, IN.fogFactor );
				#endif

				return half4( Color, Alpha );
			}

			ENDHLSL
		}

		
		Pass
		{
			
			Name "DepthOnly"
			Tags { "LightMode"="DepthOnly" }

			ZWrite On
			ColorMask 0
			AlphaToMask Off

			HLSLPROGRAM
			#define _RECEIVE_SHADOWS_OFF 1
			#pragma multi_compile_instancing
			#define ASE_SRP_VERSION 100302
			#define REQUIRE_DEPTH_TEXTURE 1

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#pragma shader_feature_local _SHIELDMODE_USEWIND _SHIELDMODE_USEAURORA
			#pragma shader_feature_local _USEBLOCK_ON


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				float4 ase_texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _WindNoiseST;
			float4 _AyroraButtomColor;
			float4 _AyroraColor;
			float4 _AuroraTexture2ST;
			float4 _AuroraTexture1ST;
			float4 _AuroraNoise2ST;
			float4 _iconcolor;
			float4 _AuroraNoise1ST;
			float4 _WindColor;
			float4 _WindTexture;
			float4 _Color0;
			float4 _iconTiling;
			float3 _PlayerPosition;
			float _DistanceEdgeSmoothness;
			float _AuroraNoise2NoiseScale;
			float _AyroraAlpha;
			float _AuroraNoise1NoiseScale;
			float _DistanceValue;
			float _ColorGradientValue;
			float _ColorGradientSmoothness;
			float _DepthAlpha;
			float _MaskSize;
			float _MaskScale;
			float _AuroraMaskStrength;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			sampler2D _TextureSample2;
			uniform float4 _CameraDepthTexture_TexelSize;
			sampler2D _WindNoiseTexture;
			sampler2D _TextureSample1;
			sampler2D _WindFieldMaskTexture;
			sampler2D _AuroraTexture1;
			sampler2D _AuroraTexture2;


			//https://www.shadertoy.com/view/XdXGW8
			float2 GradientNoiseDir( float2 x )
			{
				const float2 k = float2( 0.3183099, 0.3678794 );
				x = x * k + k.yx;
				return -1.0 + 2.0 * frac( 16.0 * k * frac( x.x * x.y * ( x.x + x.y ) ) );
			}
			
			float GradientNoise( float2 UV, float Scale )
			{
				float2 p = UV * Scale;
				float2 i = floor( p );
				float2 f = frac( p );
				float2 u = f * f * ( 3.0 - 2.0 * f );
				return lerp( lerp( dot( GradientNoiseDir( i + float2( 0.0, 0.0 ) ), f - float2( 0.0, 0.0 ) ),
						dot( GradientNoiseDir( i + float2( 1.0, 0.0 ) ), f - float2( 1.0, 0.0 ) ), u.x ),
						lerp( dot( GradientNoiseDir( i + float2( 0.0, 1.0 ) ), f - float2( 0.0, 1.0 ) ),
						dot( GradientNoiseDir( i + float2( 1.0, 1.0 ) ), f - float2( 1.0, 1.0 ) ), u.x ), u.y );
			}
			
			float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }
			float snoise( float2 v )
			{
				const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
				float2 i = floor( v + dot( v, C.yy ) );
				float2 x0 = v - i + dot( i, C.xx );
				float2 i1;
				i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
				float4 x12 = x0.xyxy + C.xxzz;
				x12.xy -= i1;
				i = mod2D289( i );
				float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
				float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
				m = m * m;
				m = m * m;
				float3 x = 2.0 * frac( p * C.www ) - 1.0;
				float3 h = abs( x ) - 0.5;
				float3 ox = floor( x + 0.5 );
				float3 a0 = x - ox;
				m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
				float3 g;
				g.x = a0.x * x0.x + h.x * x0.y;
				g.yz = a0.yz * x12.xz + h.yz * x12.yw;
				return 130.0 * dot( m, g );
			}
			

			VertexOutput VertexFunction( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float4 ase_clipPos = TransformObjectToHClip((v.vertex).xyz);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord3 = screenPos;
				
				o.ase_texcoord2.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord2.zw = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = defaultVertexValue;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif

				o.clipPos = TransformWorldToHClip( positionWS );
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = clipPos;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord = v.ase_texcoord;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float smoothstepResult50 = smoothstep( _DistanceValue , ( _DistanceValue + _DistanceEdgeSmoothness ) , distance( _PlayerPosition , WorldPosition ));
				float2 appendResult39 = (float2(_iconTiling.x , _iconTiling.y));
				float2 appendResult40 = (float2(_iconTiling.z , _iconTiling.w));
				float2 texCoord36 = IN.ase_texcoord2.xy * appendResult39 + appendResult40;
				float temp_output_41_0 = ( ( 1.0 - smoothstepResult50 ) * tex2D( _TextureSample2, texCoord36 ).r * _iconcolor.a );
				#ifdef _USEBLOCK_ON
				float staticSwitch149 = temp_output_41_0;
				#else
				float staticSwitch149 = 0.0;
				#endif
				float4 screenPos = IN.ase_texcoord3;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float screenDepth62 = LinearEyeDepth(SHADERGRAPH_SAMPLE_SCENE_DEPTH( ase_screenPosNorm.xy ),_ZBufferParams);
				float distanceDepth62 = abs( ( screenDepth62 - LinearEyeDepth( ase_screenPosNorm.z,_ZBufferParams ) ) / ( _DepthAlpha ) );
				float temp_output_64_0 = saturate( distanceDepth62 );
				float2 appendResult56 = (float2(_WindNoiseST.x , _WindNoiseST.y));
				float2 appendResult57 = (float2(_WindNoiseST.z , _WindNoiseST.w));
				float2 texCoord60 = IN.ase_texcoord2.xy * appendResult56 + ( _TimeParameters.x * appendResult57 );
				float4 tex2DNode5 = tex2D( _WindNoiseTexture, texCoord60 );
				float2 appendResult22 = (float2(_WindTexture.x , _WindTexture.y));
				float2 appendResult23 = (float2(( _WindTexture.z * _TimeParameters.x ) , ( _WindTexture.w * _TimeParameters.x )));
				float2 texCoord18 = IN.ase_texcoord2.xy * appendResult22 + appendResult23;
				float4 tex2DNode16 = tex2D( _TextureSample1, texCoord18 );
				float2 texCoord69 = IN.ase_texcoord2.xy * float2( 1,1 ) + float2( 0,0 );
				float2 panner80 = ( 1.0 * _Time.y * float2( -0.09,0 ) + texCoord69);
				float gradientNoise78 = GradientNoise(panner80,3.74);
				gradientNoise78 = gradientNoise78*0.5 + 0.5;
				float4 tex2DNode73 = tex2D( _WindFieldMaskTexture, ( texCoord69 + ( ( texCoord69 - float2( 0.5,0.5 ) ) * _MaskSize ) ) );
				float saferPower77 = max( ( tex2DNode73.g * tex2DNode73.b ) , 0.0001 );
				float lerpResult75 = lerp( ( gradientNoise78 * tex2DNode73.r ) , pow( saferPower77 , 0.29 ) , _MaskScale);
				float Mask152 = lerpResult75;
				float BlockTextureAlphaFinish138 = staticSwitch149;
				float2 appendResult118 = (float2(_AuroraNoise2ST.z , _AuroraNoise2ST.w));
				float2 appendResult116 = (float2(_AuroraNoise2ST.x , _AuroraNoise2ST.y));
				float2 texCoord117 = IN.ase_texcoord2.xy * appendResult116 + float2( 0,0 );
				float2 panner114 = ( 1.0 * _Time.y * appendResult118 + texCoord117);
				float gradientNoise119 = GradientNoise(panner114,_AuroraNoise2NoiseScale);
				gradientNoise119 = gradientNoise119*0.5 + 0.5;
				float2 appendResult113 = (float2(_AuroraNoise1ST.z , _AuroraNoise1ST.w));
				float2 appendResult111 = (float2(_AuroraNoise1ST.x , _AuroraNoise1ST.y));
				float2 texCoord112 = IN.ase_texcoord2.xy * appendResult111 + float2( 0,0 );
				float2 panner109 = ( 1.0 * _Time.y * appendResult113 + texCoord112);
				float simplePerlin2D108 = snoise( panner109*_AuroraNoise1NoiseScale );
				simplePerlin2D108 = simplePerlin2D108*0.5 + 0.5;
				float2 appendResult96 = (float2(_AuroraTexture1ST.z , _AuroraTexture1ST.w));
				float2 appendResult95 = (float2(_AuroraTexture1ST.x , _AuroraTexture1ST.y));
				float2 texCoord92 = IN.ase_texcoord2.xy * appendResult95 + float2( 0,0 );
				float2 panner93 = ( 1.0 * _Time.y * appendResult96 + texCoord92);
				float2 appendResult129 = (float2(_AuroraTexture2ST.z , _AuroraTexture2ST.w));
				float2 appendResult128 = (float2(_AuroraTexture2ST.x , _AuroraTexture2ST.y));
				float2 texCoord130 = IN.ase_texcoord2.xy * appendResult128 + float2( 0,0 );
				float2 panner131 = ( 1.0 * _Time.y * appendResult129 + texCoord130);
				float4 temp_output_124_0 = ( ( ( gradientNoise119 + simplePerlin2D108 ) / 1.0 ) + ( tex2D( _AuroraTexture1, panner93 ) * tex2D( _AuroraTexture2, panner131 ) ) );
				float4 break126 = temp_output_124_0;
				float2 texCoord101 = IN.ase_texcoord2.xy * float2( 1,1 ) + float2( 0,0 );
				float DepthAlpha135 = temp_output_64_0;
				#if defined(_SHIELDMODE_USEWIND)
				float staticSwitch142 = ( staticSwitch149 + ( temp_output_64_0 * ( ( _Color0.a * ( tex2DNode5.r * tex2DNode5.a ) ) + ( tex2DNode16.a * _WindColor.a ) ) * Mask152 ) );
				#elif defined(_SHIELDMODE_USEAURORA)
				float staticSwitch142 = ( ( ( BlockTextureAlphaFinish138 * 0.5 ) + ( break126.r * break126.a * _AyroraAlpha * saturate( ( texCoord101.y + _AuroraMaskStrength ) ) * DepthAlpha135 ) ) * Mask152 );
				#else
				float staticSwitch142 = ( staticSwitch149 + ( temp_output_64_0 * ( ( _Color0.a * ( tex2DNode5.r * tex2DNode5.a ) ) + ( tex2DNode16.a * _WindColor.a ) ) * Mask152 ) );
				#endif
				
				float Alpha = staticSwitch142;
				float AlphaClipThreshold = 0.5;

				#ifdef _ALPHATEST_ON
					clip(Alpha - AlphaClipThreshold);
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif
				return 0;
			}
			ENDHLSL
		}

	
	}
	CustomEditor "UnityEditor.ShaderGraph.PBRMasterGUI"
	Fallback "Hidden/InternalErrorShader"
	
}
/*ASEBEGIN
Version=18900
0;73.6;814.2;719;1984.216;-3072.229;1;False;False
Node;AmplifyShaderEditor.Vector4Node;115;-2244.515,2143.103;Inherit;False;Property;_AuroraNoise2ST;Aurora Noise 2 ST;30;0;Create;True;0;0;0;False;0;False;1,1,0,0;1,0.5,-0.01,0.1;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector4Node;110;-2249.029,2402.986;Inherit;False;Property;_AuroraNoise1ST;Aurora Noise 1 ST;28;0;Create;True;0;0;0;False;0;False;1,1,0,0;-0.56,0.2,0.01,0.025;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;69;-1322.413,-597.587;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector4Node;38;-2538.364,1389.207;Inherit;False;Property;_iconTiling;Block Texture ST;1;0;Create;False;0;0;0;False;0;False;1,1,0,0;60,60,0,0.27;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector4Node;94;-2249.909,2652.653;Inherit;False;Property;_AuroraTexture1ST;Aurora Texture 1  ST;25;0;Create;True;0;0;0;False;0;False;1,1,0,0;5,3,0.005,0.05;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;116;-1936.15,2164.555;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;53;-1864.858,887.749;Inherit;False;Property;_DistanceValue;Distance Value;6;0;Create;True;0;0;0;False;0;False;0;-1.92;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;81;-1119.451,-457.4091;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;111;-1940.664,2422.725;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;83;-1080.451,-338.4091;Inherit;False;Property;_MaskSize;Wind Field Texture Size;17;0;Create;False;0;0;0;False;0;False;0;0.06;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;52;-1943.858,970.7491;Inherit;False;Property;_DistanceEdgeSmoothness;Distance Edge Smoothness;5;0;Create;True;0;0;0;False;0;False;1;7.21;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;33;-2043.465,1210.703;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.Vector3Node;31;-2044.827,1053.516;Inherit;False;Property;_PlayerPosition;PlayerPosition;3;1;[Header];Create;True;1;Player Setting;0;0;False;0;False;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.Vector4Node;127;-2255.808,2899.256;Inherit;False;Property;_AuroraTexture2ST;Aurora Texture 2 ST;27;0;Create;True;0;0;0;False;0;False;1,1,0,0;-7.3,0.43,-0.005,0.01;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;39;-2256.364,1369.207;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;128;-1947.448,2918.995;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;95;-1941.546,2672.392;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector4Node;58;-1768.207,-248.9057;Inherit;False;Property;_WindNoiseST;Wind Noise 1 ST;10;0;Create;False;0;0;0;False;0;False;1,1,0,0;0.39,1,0.24,0.19;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DistanceOpNode;30;-1812.198,1059.993;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;117;-1809.369,2150.913;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;40;-2250.364,1462.207;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;112;-1813.883,2409.083;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;82;-965.4509,-457.4091;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;51;-1636.858,931.749;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;113;-1810.622,2530.339;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;118;-1806.108,2272.169;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleTimeNode;17;-1998.27,703.7687;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;36;-2078.567,1390.571;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;57;-1482.207,-109.9057;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SmoothstepOpNode;50;-1453.169,1058.861;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector4Node;21;-1955.335,426.0571;Inherit;False;Property;_WindTexture;Wind Texture 2 ST;13;0;Create;False;0;0;0;False;0;False;1,1,0,-0.2;0.5,0.02,0.05,0.08;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;129;-1817.406,3026.609;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;114;-1555.348,2151.954;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleTimeNode;55;-1740.216,-24.25075;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;96;-1811.504,2780.006;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;109;-1559.862,2410.124;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;122;-1631.343,2545.724;Inherit;False;Property;_AuroraNoise1NoiseScale;Aurora Noise 1 Noise Scale;29;0;Create;True;0;0;0;False;0;False;4.93;5.66;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;120;-1634.109,2290.485;Inherit;False;Property;_AuroraNoise2NoiseScale;Aurora Noise 2 Noise Scale;31;0;Create;True;0;0;0;False;0;False;5.98;9.49;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;92;-1814.765,2658.75;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;130;-1820.667,2905.353;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;84;-810.4509,-599.4091;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;56;-1479.207,-210.9057;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;19;-1749.67,670.7083;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;108;-1339.6,2410.565;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;66;-871.0038,290.3298;Inherit;False;Property;_DepthAlpha;Depth Alpha;7;1;[Header];Create;True;1;All Wind Mask Ctrl;0;0;False;0;False;0;-10.63;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;59;-1342.207,-23.90575;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;73;-690.209,-625.4155;Inherit;True;Property;_WindFieldMaskTexture;Wind Field Mask Texture;15;0;Create;True;0;0;0;False;0;False;-1;54523dc94927c1447bdf7f52919211ce;54523dc94927c1447bdf7f52919211ce;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;-1756.467,573.4671;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;46;-1297.245,1351.965;Inherit;False;Property;_iconcolor;Block Color;4;1;[HDR];Create;False;0;0;0;False;0;False;1,1,1,1;0,0.7490196,0.1921569,0.3921569;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;93;-1560.744,2659.791;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;80;-591.2985,-859.9242;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-0.09,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;35;-1703.692,1327.639;Inherit;True;Property;_TextureSample2;Block Texture;0;1;[Header];Create;False;1;Block Texture;0;0;False;0;False;-1;c5550bf23f38be3409ea7a98a531c49a;636b3c0ca1acc524788d031120b878dc;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;54;-1276.078,967.1272;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;119;-1335.086,2152.395;Inherit;True;Gradient;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;131;-1566.646,2906.394;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;23;-1561.966,596.6243;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;151;-831.4194,1110.493;Inherit;False;Constant;_Float0;Float 0;30;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;22;-1553.69,455.7578;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;86;-1384.134,2633.863;Inherit;True;Property;_AuroraTexture1;Aurora Texture 1;24;0;Create;True;0;0;0;False;0;False;-1;c22f5c857f7f1324cb74a0ab4aa33fda;36ada77200269134f905e49618155867;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;60;-1195.927,-134.4022;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NoiseGeneratorNode;78;-352.3788,-863.1691;Inherit;True;Gradient;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;3.74;False;1;FLOAT;0
Node;AmplifyShaderEditor.DepthFade;62;-625.064,272.5133;Inherit;False;True;False;True;2;1;FLOAT3;0,0,0;False;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;41;-988.3477,1057.858;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;132;-1393.735,2879.588;Inherit;True;Property;_AuroraTexture2;Aurora Texture 2;26;0;Create;True;0;0;0;False;0;False;-1;c22f5c857f7f1324cb74a0ab4aa33fda;8fe36ee15de3bf747932bbb8c9fdeb03;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;123;-1059.28,2296.754;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;74;-353.5407,-574.7186;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;64;-327.3967,441.0556;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;18;-1322.302,634.0705;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;133;-1076.54,2789.144;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;105;-1578.649,3732.293;Inherit;False;Property;_AuroraMaskStrength;Aurora Mask Strength;23;0;Create;True;0;0;0;False;0;False;1;-0.2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;101;-1574.236,3614.722;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;76;-136.4539,-317.3176;Inherit;False;Property;_MaskScale;Wind Field Mask Scale;16;0;Create;False;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;79;-23.83259,-784.772;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;149;-658.736,1029.596;Inherit;False;Property;_UseBlock;Use Block;2;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;5;-913.4957,-10.07577;Inherit;True;Property;_WindNoiseTexture;Wind Texture 1;8;1;[Header];Create;False;2;Wind Setting;Wind Texture 1;0;0;False;0;False;-1;None;1028a9ab963ee014eb604a527e73c29b;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PowerNode;77;-56.12608,-556.2024;Inherit;True;True;2;0;FLOAT;0;False;1;FLOAT;0.29;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;125;-927.3385,2296.116;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;13;-823.5234,-224.6638;Inherit;False;Property;_Color0;Wind Texture 1 Color;11;1;[HDR];Create;False;0;0;0;False;0;False;0,0,0,0;1,1,1,0.3960784;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;138;-241.3492,1266.175;Inherit;False;BlockTextureAlphaFinish;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;15;-571.9579,72.84297;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;135;-102.8303,523.2238;Inherit;False;DepthAlpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;16;-1037.303,608.0676;Inherit;True;Property;_TextureSample1;Wind Texture 2;12;1;[Header];Create;False;1;Wind Texture 2;0;0;False;0;False;-1;8521bcdbe9f01b24b877983f3bf15d90;8521bcdbe9f01b24b877983f3bf15d90;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;29;-958.0564,835.1732;Inherit;False;Property;_WindColor;Wind TExture 2 Color;14;1;[HDR];Create;False;0;0;0;False;0;False;1,1,1,1;0,1,0.6666667,0.1215686;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;106;-1330.37,3661.376;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;124;-1059.203,2644.725;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;75;247.4923,-604.0461;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0.21;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;136;-1100.773,3505.443;Inherit;False;135;DepthAlpha;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;28;-683.1665,715.1614;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;152;577.1459,-570.105;Inherit;False;Mask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;148;-740.8552,3106.499;Inherit;False;Constant;_AuroraAlphaDark;Aurora Alpha Dark;27;0;Create;True;0;0;0;False;0;False;0.5;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;14;-411.5763,51.91388;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;157;-1033.216,3299.193;Inherit;False;Property;_AyroraAlpha;Ayrora Alpha;20;0;Create;True;0;0;0;False;0;False;0;0.78;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;140;-811.2731,2809.478;Inherit;False;138;BlockTextureAlphaFinish;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;126;-949.5771,2643.463;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SaturateNode;107;-1199.37,3673.376;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;25;-47.9524,283.7309;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;153;-59.65734,419.5996;Inherit;False;152;Mask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;147;-553.6262,2817.362;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;100;-797.6161,2914.109;Inherit;False;5;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;98;-383.4178,2895.467;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;63;157.4676,369.8638;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;154;-411.0613,3012.143;Inherit;False;152;Mask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;155;-98.9668,2944.828;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;67;404.9988,525.1163;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;97;-776.8636,2665.552;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;168;-1646.216,3525.229;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;161;-1572.857,3440.195;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;163;-1877.857,3432.195;Inherit;False;Property;_ColorGradientValue;Color Gradient Value;21;0;Create;True;0;0;0;False;0;False;0;-0.01;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;165;-1905.857,3659.195;Inherit;False;Property;_ColorGradientSmoothness;Color Gradient Smoothness;22;0;Create;True;0;0;0;False;0;False;0;0.05;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;158;-1842.516,3289.594;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;142;1717.059,1060.997;Inherit;False;Property;_ShieldMode;Shield Mode;25;0;Create;True;0;0;0;False;0;False;0;0;1;True;;KeywordEnum;2;UseWind;UseAurora;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;-215.2067,-8.412579;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SmoothstepOpNode;166;-1424.216,3466.229;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;137;-241.4238,1147.697;Inherit;False;BlockTextureColorFinish;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;99;-549.7361,2706.905;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;139;-896.1243,2410.545;Inherit;False;137;BlockTextureColorFinish;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;61;-985.084,1170.95;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;34;-473.2086,610.6647;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;160;-1609.117,3257.59;Inherit;False;Property;_AyroraButtomColor;Ayrora Buttom Color;19;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,0;24.25146,9.380002,2.608175,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;87;-1595.723,3082.665;Inherit;False;Property;_AyroraColor;Ayrora Color;18;2;[HDR];[Header];Create;True;1;Aurora Setting;0;0;False;0;False;1,1,1,1;0,4,3.168628,0.6392157;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;150;-657.9263,1144.058;Inherit;False;Property;_UseBlock;Use Block;28;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;141;1721.47,942.5603;Inherit;False;Property;_ShieldMode;Shield Mode;9;0;Create;True;0;0;0;False;0;False;0;0;1;True;;KeywordEnum;2;UseWind;UseAurora;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;27;-682.9706,610.502;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;26;-16.68517,82.38593;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;159;-1238.617,3085.991;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;0,0;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;ExtraPrePass;0;0;ExtraPrePass;5;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;True;1;1;False;-1;0;False;-1;0;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;True;True;True;True;0;False;-1;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;0;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;4;0,0;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;Meta;0;4;Meta;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;LightMode=Meta;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;2;0,0;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;ShadowCaster;0;2;ShadowCaster;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;False;-1;True;3;False;-1;False;True;1;LightMode=ShadowCaster;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;3;0,0;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;DepthOnly;0;3;DepthOnly;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;False;False;True;False;False;False;False;0;False;-1;False;False;False;False;False;False;False;False;False;True;1;False;-1;False;False;True;1;LightMode=DepthOnly;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;1;2072.746,966.6497;Float;False;True;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;ce_BattleShield;2992e84f91cbeb14eab234972e07ea9d;True;Forward;0;1;Forward;8;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;2;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Transparent=RenderType;Queue=Transparent=Queue=0;True;0;0;False;True;2;5;False;-1;10;False;-1;5;4;False;-1;1;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;True;True;True;True;0;False;-1;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;2;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;1;LightMode=UniversalForward;False;0;Hidden/InternalErrorShader;0;0;Standard;22;Surface;1;  Blend;0;Two Sided;0;Cast Shadows;0;  Use Shadow Threshold;0;Receive Shadows;0;GPU Instancing;1;LOD CrossFade;0;Built-in Fog;0;DOTS Instancing;0;Meta Pass;0;Extra Pre Pass;0;Tessellation;0;  Phong;0;  Strength;0.5,False,-1;  Type;0;  Tess;16,False,-1;  Min;10,False,-1;  Max;25,False,-1;  Edge Length;16,False,-1;  Max Displacement;25,False,-1;Vertex Position,InvertActionOnDeselection;1;0;5;False;True;False;True;False;False;;False;0
WireConnection;116;0;115;1
WireConnection;116;1;115;2
WireConnection;81;0;69;0
WireConnection;111;0;110;1
WireConnection;111;1;110;2
WireConnection;39;0;38;1
WireConnection;39;1;38;2
WireConnection;128;0;127;1
WireConnection;128;1;127;2
WireConnection;95;0;94;1
WireConnection;95;1;94;2
WireConnection;30;0;31;0
WireConnection;30;1;33;0
WireConnection;117;0;116;0
WireConnection;40;0;38;3
WireConnection;40;1;38;4
WireConnection;112;0;111;0
WireConnection;82;0;81;0
WireConnection;82;1;83;0
WireConnection;51;0;53;0
WireConnection;51;1;52;0
WireConnection;113;0;110;3
WireConnection;113;1;110;4
WireConnection;118;0;115;3
WireConnection;118;1;115;4
WireConnection;36;0;39;0
WireConnection;36;1;40;0
WireConnection;57;0;58;3
WireConnection;57;1;58;4
WireConnection;50;0;30;0
WireConnection;50;1;53;0
WireConnection;50;2;51;0
WireConnection;129;0;127;3
WireConnection;129;1;127;4
WireConnection;114;0;117;0
WireConnection;114;2;118;0
WireConnection;96;0;94;3
WireConnection;96;1;94;4
WireConnection;109;0;112;0
WireConnection;109;2;113;0
WireConnection;92;0;95;0
WireConnection;130;0;128;0
WireConnection;84;0;69;0
WireConnection;84;1;82;0
WireConnection;56;0;58;1
WireConnection;56;1;58;2
WireConnection;19;0;21;4
WireConnection;19;1;17;0
WireConnection;108;0;109;0
WireConnection;108;1;122;0
WireConnection;59;0;55;0
WireConnection;59;1;57;0
WireConnection;73;1;84;0
WireConnection;24;0;21;3
WireConnection;24;1;17;0
WireConnection;93;0;92;0
WireConnection;93;2;96;0
WireConnection;80;0;69;0
WireConnection;35;1;36;0
WireConnection;54;0;50;0
WireConnection;119;0;114;0
WireConnection;119;1;120;0
WireConnection;131;0;130;0
WireConnection;131;2;129;0
WireConnection;23;0;24;0
WireConnection;23;1;19;0
WireConnection;22;0;21;1
WireConnection;22;1;21;2
WireConnection;86;1;93;0
WireConnection;60;0;56;0
WireConnection;60;1;59;0
WireConnection;78;0;80;0
WireConnection;62;0;66;0
WireConnection;41;0;54;0
WireConnection;41;1;35;1
WireConnection;41;2;46;4
WireConnection;132;1;131;0
WireConnection;123;0;119;0
WireConnection;123;1;108;0
WireConnection;74;0;73;2
WireConnection;74;1;73;3
WireConnection;64;0;62;0
WireConnection;18;0;22;0
WireConnection;18;1;23;0
WireConnection;133;0;86;0
WireConnection;133;1;132;0
WireConnection;79;0;78;0
WireConnection;79;1;73;1
WireConnection;149;1;151;0
WireConnection;149;0;41;0
WireConnection;5;1;60;0
WireConnection;77;0;74;0
WireConnection;125;0;123;0
WireConnection;138;0;149;0
WireConnection;15;0;5;1
WireConnection;15;1;5;4
WireConnection;135;0;64;0
WireConnection;16;1;18;0
WireConnection;106;0;101;2
WireConnection;106;1;105;0
WireConnection;124;0;125;0
WireConnection;124;1;133;0
WireConnection;75;0;79;0
WireConnection;75;1;77;0
WireConnection;75;2;76;0
WireConnection;28;0;16;4
WireConnection;28;1;29;4
WireConnection;152;0;75;0
WireConnection;14;0;13;4
WireConnection;14;1;15;0
WireConnection;126;0;124;0
WireConnection;107;0;106;0
WireConnection;25;0;14;0
WireConnection;25;1;28;0
WireConnection;147;0;140;0
WireConnection;147;1;148;0
WireConnection;100;0;126;0
WireConnection;100;1;126;3
WireConnection;100;2;157;0
WireConnection;100;3;107;0
WireConnection;100;4;136;0
WireConnection;98;0;147;0
WireConnection;98;1;100;0
WireConnection;63;0;64;0
WireConnection;63;1;25;0
WireConnection;63;2;153;0
WireConnection;155;0;98;0
WireConnection;155;1;154;0
WireConnection;67;0;149;0
WireConnection;67;1;63;0
WireConnection;97;0;124;0
WireConnection;97;1;159;0
WireConnection;168;0;163;0
WireConnection;168;1;165;0
WireConnection;161;0;158;2
WireConnection;142;1;67;0
WireConnection;142;0;155;0
WireConnection;10;0;5;0
WireConnection;10;1;13;0
WireConnection;166;0;158;3
WireConnection;166;1;163;0
WireConnection;166;2;168;0
WireConnection;137;0;150;0
WireConnection;99;0;139;0
WireConnection;99;1;97;0
WireConnection;61;0;41;0
WireConnection;61;1;46;0
WireConnection;34;0;27;0
WireConnection;34;1;150;0
WireConnection;150;1;151;0
WireConnection;150;0;61;0
WireConnection;141;1;26;0
WireConnection;141;0;99;0
WireConnection;27;0;16;0
WireConnection;27;1;29;0
WireConnection;26;0;10;0
WireConnection;26;1;34;0
WireConnection;159;0;87;0
WireConnection;159;1;160;0
WireConnection;159;2;166;0
WireConnection;1;2;141;0
WireConnection;1;3;142;0
ASEEND*/
//CHKSM=552A24EF74416AFFB9F77ED00024E121C459481D