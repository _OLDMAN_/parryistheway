using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vfx_BattleShield : MonoBehaviour
{
    Material thisMaterial;
    [Header("玩家位置")]
    public Transform playerPosition;
    [Header("vfx_紅色屏障_01")]
    public GameObject DoNotEnterParticle;

    // Start is called before the first frame update
    void Start()
    {
        thisMaterial = GetComponent<MeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        //獲取玩家位置並賦值給材質球
        thisMaterial.SetVector("_PlayerPosition" , new Vector4(playerPosition.position.x , playerPosition.position.y , playerPosition.position.z , 0));
    }
    void OnCollisionEnter(Collision collisionInfo)
    {
        //Debug.Log("撞到了");
        //碰撞點偵測
        ContactPoint contact = collisionInfo.contacts[0];
        Vector3 playerParticleInstantPoint = contact.point;

        //生成及預備摧毀
        GameObject WantToDestoryParticle = Instantiate(DoNotEnterParticle , playerParticleInstantPoint , transform.rotation * Quaternion.Euler(90 , 0 , 0));
        Destroy(WantToDestoryParticle , 1.25f);
    }
    
}
