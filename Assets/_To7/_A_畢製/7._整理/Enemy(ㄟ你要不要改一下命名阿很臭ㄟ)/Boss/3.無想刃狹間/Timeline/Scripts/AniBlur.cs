using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class AniBlur : MonoBehaviour
{
    // Start is called before the first frame update
    public Material aniBlurMaterial;
    public float fDensity;
    public float centerPointX;
    public float centerPointY;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        aniBlurMaterial.SetFloat("_fDensity" , fDensity);
        aniBlurMaterial.SetFloat("_fX" , centerPointX);
        aniBlurMaterial.SetFloat("_fY" , centerPointY);
    }
}
