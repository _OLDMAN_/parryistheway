using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
/*created by Jason 20180814
 * 浮生若梦出品
 * 作者QQ:541211225
 * v0.1
*/
public class PlaySpeed : MonoBehaviour {

	//public float playSpeed = 1.0f;
	public float currentTimeScale = 1f;
	public float anotherTimeScale = 0.1f;
	public float needSec = 0.5f;
	public AnimationCurve curve;

	void OnEnable()
	{
		ComeBack();
	}
	
	private void ComeBack()
    {
		Time.timeScale = currentTimeScale;
        float temp = currentTimeScale;
        DOTween.To(() => temp, x => temp = x, anotherTimeScale, needSec).SetEase(curve).SetUpdate(true).OnUpdate(() => {
            Time.timeScale = temp;
        });
    }

}
