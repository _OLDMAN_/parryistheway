// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "To7/Toon Shader_Gradution"
{
	Properties
	{
		[HideInInspector] _AlphaCutoff("Alpha Cutoff ", Range(0, 1)) = 0.5
		[ASEBegin][Header(Albedo)]_AlbedoMap("Albedo Map(主貼圖)", 2D) = "white" {}
		[HDR]_AlbedoEmissionColor("Albedo/Emission  Color(主貼圖/高光顏色)", Color) = (1,1,1,0)
		[Header(Normal)][Normal]_NormalMap("Normal Map(法線貼圖)", 2D) = "bump" {}
		_NormalStrength("Normal Strength(法線強度)", Range( 0 , 2)) = 0
		[Header(Emission)]_EmissionTexture("Emission Texture", 2D) = "white" {}
		[HDR]_EmissionColor("Emission Color", Color) = (0,0,0,1)
		_EmissionStrength("Emission Strength", Float) = 1
		[Header(Light)]_DirLightIntensity("DirLight Intensity(日光強度)", Float) = 1
		_DirLightNormalScale("DirLightNormalScale(N.L強度)", Float) = 0.6
		_DiffuseLightStrength("DiffuseLight Strength(環境光強度(疊加)", Float) = 0
		_IndirectDiffuseLightNormal("Indirect Diffuse Light Normal(漫射光法線貼圖)", 2D) = "bump" {}
		[Header(Shadow Type)]_ShadowRemapMap("Shadow Remap Map", 2D) = "white" {}
		[KeywordEnum(UseShaderGardientColor,UseShadowRemapMap)] _ShadowRemapType("Shadow Remap Type(陰影重映射方式)(左為背光右為迎光)", Float) = 1
		_RampAddincludePower("Ramp Add(include Power)", Float) = 0
		_RampAddwithoutPower("Ramp Add(without Power)", Float) = 0
		_RampPower("Ramp Power", Range( 0 , 3)) = 1
		[Header(Outline)]_OutlineWidth("Outline Width(外描邊寬度)", Range( 0 , 0.1)) = 0.1
		_OutlineMax("Outline Max外描邊最大閾值(Accroding to Camera Distance)(根據照相機遠近) ", Float) = 0
		[HDR]_OutlineColor("Outline Color(外描邊顏色)", Color) = (0.1981132,0.1981132,0.1981132,0)
		[Header(Rim)]_RimOffset("Rim Offset(泛光量值)", Float) = 0
		[HDR]_RimColor("Rim Color(泛光顏色)", Color) = (0,0,0,0)
		_RimSmoothStepMinMax("Rim SmoothStepValue and Smoothness(泛光平滑閾值)", Vector) = (0,1,0,0)
		[Header(Specular)]_SpecularMap("Specular Map(高光貼圖)", 2D) = "white" {}
		_SpecularPow("Specular Pow(高光量值)", Float) = 4
		[HDR]_SpecularColor("Specular Color(高光顏色)", Color) = (1,1,1,0)
		_SpecularSmoothStepMinMax("Specular SmoothStep Value & Smoothness(高光值與平滑度)", Vector) = (0,0.01,0,0)
		_SpecularIntensity("Specular Intensity(高光強度)", Range( 0 , 1)) = 0
		_SpecularSRPLightSampler("Specular SRPLight Sampler(高光燈光採樣次數)", Float) = 5
		_SpecularDirLightSRPLightIntensity("Specular DirLight/SRPLight Intensity(高光受光影響程度 主光源/SRP光源))", Vector) = (1,1,0,0)
		[Header(Light Attenuation)]_LightAttValue("Light Att Value(光衰減程度)", Float) = -0.3
		_LightAttSoft("Light Att Soft(光衰減平滑度)", Float) = 0.46
		[Header(Celluloid Edge Noise Texture)]_RampCelluloidNoiseTexture("Ramp Celluloid Noise Texture(賽璐璐陰影邊緣噪波貼圖)", 2D) = "white" {}
		_RampCelluloidNoiseValue("Ramp Celluloid Noise Value(賽璐璐陰影強度)", Float) = 0
		_RampCelluloidNoiseSoftness("Ramp Celluloid Noise Softness(賽璐璐陰影平滑度)", Float) = 1
		[Header(Dead Dissolve Setting)]_DeadClip("_DeadClip(死亡溶解控制)", Range( 0 , 1)) = 0
		_GenerateDither("GenerateDither", Range( 0 , 1)) = 1
		[HDR]_ClipEdgeColor("ClipEdgeColor(死亡溶解顏色)", Color) = (7.464264,1.717473,0,0)
		_ClipEdge("ClipEdge(死亡溶解邊緣顏色範圍)", Float) = 0.99
		_ClipEdgeSoftness("ClipEdgeSoftness(死亡溶解邊緣顏色平滑度)", Float) = 0.01
		_ClipNoiseTiling("ClipNoiseTiling(死亡溶解噪點紋理拉伸)", Float) = 1
		_ClipNoiseScale("ClipNoiseScale(死亡溶解噪點紋理大小)", Float) = 10
		[HDR]_SprintColor("Sprint Color", Color) = (2.297397,0.9640058,0.2072162,0)
		_Sprint("_Sprint(衝刺)", Range( 0 , 1)) = 0
		[ASEEnd][KeywordEnum(UV,WorldSpace,ScreenSpace,ViewSpace)] _DissolveMode("Dissolve Mode", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

		//_TessPhongStrength( "Tess Phong Strength", Range( 0, 1 ) ) = 0.5
		//_TessValue( "Tess Max Tessellation", Range( 1, 32 ) ) = 16
		//_TessMin( "Tess Min Distance", Float ) = 10
		//_TessMax( "Tess Max Distance", Float ) = 25
		//_TessEdgeLength ( "Tess Edge length", Range( 2, 50 ) ) = 16
		//_TessMaxDisp( "Tess Max Displacement", Float ) = 25
	}

	SubShader
	{
		LOD 0

		
		Tags { "RenderPipeline"="UniversalPipeline" "RenderType"="Opaque" "Queue"="Geometry" }
		
		Cull Off
		AlphaToMask Off
		HLSLINCLUDE
		#pragma target 5.0

		#ifndef ASE_TESS_FUNCS
		#define ASE_TESS_FUNCS
		float4 FixedTess( float tessValue )
		{
			return tessValue;
		}
		
		float CalcDistanceTessFactor (float4 vertex, float minDist, float maxDist, float tess, float4x4 o2w, float3 cameraPos )
		{
			float3 wpos = mul(o2w,vertex).xyz;
			float dist = distance (wpos, cameraPos);
			float f = clamp(1.0 - (dist - minDist) / (maxDist - minDist), 0.01, 1.0) * tess;
			return f;
		}

		float4 CalcTriEdgeTessFactors (float3 triVertexFactors)
		{
			float4 tess;
			tess.x = 0.5 * (triVertexFactors.y + triVertexFactors.z);
			tess.y = 0.5 * (triVertexFactors.x + triVertexFactors.z);
			tess.z = 0.5 * (triVertexFactors.x + triVertexFactors.y);
			tess.w = (triVertexFactors.x + triVertexFactors.y + triVertexFactors.z) / 3.0f;
			return tess;
		}

		float CalcEdgeTessFactor (float3 wpos0, float3 wpos1, float edgeLen, float3 cameraPos, float4 scParams )
		{
			float dist = distance (0.5 * (wpos0+wpos1), cameraPos);
			float len = distance(wpos0, wpos1);
			float f = max(len * scParams.y / (edgeLen * dist), 1.0);
			return f;
		}

		float DistanceFromPlane (float3 pos, float4 plane)
		{
			float d = dot (float4(pos,1.0f), plane);
			return d;
		}

		bool WorldViewFrustumCull (float3 wpos0, float3 wpos1, float3 wpos2, float cullEps, float4 planes[6] )
		{
			float4 planeTest;
			planeTest.x = (( DistanceFromPlane(wpos0, planes[0]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[0]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[0]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.y = (( DistanceFromPlane(wpos0, planes[1]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[1]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[1]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.z = (( DistanceFromPlane(wpos0, planes[2]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[2]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[2]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.w = (( DistanceFromPlane(wpos0, planes[3]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[3]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[3]) > -cullEps) ? 1.0f : 0.0f );
			return !all (planeTest);
		}

		float4 DistanceBasedTess( float4 v0, float4 v1, float4 v2, float tess, float minDist, float maxDist, float4x4 o2w, float3 cameraPos )
		{
			float3 f;
			f.x = CalcDistanceTessFactor (v0,minDist,maxDist,tess,o2w,cameraPos);
			f.y = CalcDistanceTessFactor (v1,minDist,maxDist,tess,o2w,cameraPos);
			f.z = CalcDistanceTessFactor (v2,minDist,maxDist,tess,o2w,cameraPos);

			return CalcTriEdgeTessFactors (f);
		}

		float4 EdgeLengthBasedTess( float4 v0, float4 v1, float4 v2, float edgeLength, float4x4 o2w, float3 cameraPos, float4 scParams )
		{
			float3 pos0 = mul(o2w,v0).xyz;
			float3 pos1 = mul(o2w,v1).xyz;
			float3 pos2 = mul(o2w,v2).xyz;
			float4 tess;
			tess.x = CalcEdgeTessFactor (pos1, pos2, edgeLength, cameraPos, scParams);
			tess.y = CalcEdgeTessFactor (pos2, pos0, edgeLength, cameraPos, scParams);
			tess.z = CalcEdgeTessFactor (pos0, pos1, edgeLength, cameraPos, scParams);
			tess.w = (tess.x + tess.y + tess.z) / 3.0f;
			return tess;
		}

		float4 EdgeLengthBasedTessCull( float4 v0, float4 v1, float4 v2, float edgeLength, float maxDisplacement, float4x4 o2w, float3 cameraPos, float4 scParams, float4 planes[6] )
		{
			float3 pos0 = mul(o2w,v0).xyz;
			float3 pos1 = mul(o2w,v1).xyz;
			float3 pos2 = mul(o2w,v2).xyz;
			float4 tess;

			if (WorldViewFrustumCull(pos0, pos1, pos2, maxDisplacement, planes))
			{
				tess = 0.0f;
			}
			else
			{
				tess.x = CalcEdgeTessFactor (pos1, pos2, edgeLength, cameraPos, scParams);
				tess.y = CalcEdgeTessFactor (pos2, pos0, edgeLength, cameraPos, scParams);
				tess.z = CalcEdgeTessFactor (pos0, pos1, edgeLength, cameraPos, scParams);
				tess.w = (tess.x + tess.y + tess.z) / 3.0f;
			}
			return tess;
		}
		#endif //ASE_TESS_FUNCS

		ENDHLSL

		
		Pass
		{
			Name "Outline"
			
			
			Blend One Zero, One Zero
			Cull Front
			ZWrite On
			ZTest LEqual
			Offset 0 , 0
			ColorMask RGBA
			

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#define _ALPHATEST_ON 1
			#define ASE_SRP_VERSION 100302

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/UnityInstancing.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

			#if ASE_SRP_VERSION <= 70108
			#define REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR
			#endif

			#include "Packages/com.unity.shadergraph/ShaderGraphLibrary/Functions.hlsl"
			#define ASE_NEEDS_VERT_NORMAL
			#define ASE_NEEDS_VERT_POSITION
			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#define ASE_NEEDS_FRAG_SHADOWCOORDS
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma shader_feature_local _SHADOWREMAPTYPE_USESHADERGARDIENTCOLOR _SHADOWREMAPTYPE_USESHADOWREMAPMAP
			#pragma shader_feature_local _DISSOLVEMODE_UV _DISSOLVEMODE_WORLDSPACE _DISSOLVEMODE_SCREENSPACE _DISSOLVEMODE_VIEWSPACE
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
				float4 ase_tangent : TANGENT;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				#ifdef ASE_FOG
				float fogFactor : TEXCOORD2;
				#endif
				float4 ase_texcoord3 : TEXCOORD3;
				float4 lightmapUVOrVertexSH : TEXCOORD4;
				float4 ase_texcoord5 : TEXCOORD5;
				float4 ase_texcoord6 : TEXCOORD6;
				float4 ase_texcoord7 : TEXCOORD7;
				float4 ase_texcoord8 : TEXCOORD8;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _SprintColor;
			float4 _AlbedoMap_ST;
			float4 _EmissionColor;
			float4 _AlbedoEmissionColor;
			float4 _IndirectDiffuseLightNormal_ST;
			float4 _EmissionTexture_ST;
			float4 _SpecularMap_ST;
			float4 _SpecularColor;
			float4 _OutlineColor;
			float4 _RampCelluloidNoiseTexture_ST;
			float4 _ClipEdgeColor;
			float4 _NormalMap_ST;
			float4 _RimColor;
			float2 _SpecularSmoothStepMinMax;
			float2 _SpecularDirLightSRPLightIntensity;
			float2 _RimSmoothStepMinMax;
			float _SpecularSRPLightSampler;
			float _ClipEdgeSoftness;
			float _SpecularPow;
			float _SpecularIntensity;
			float _EmissionStrength;
			float _RimOffset;
			float _ClipEdge;
			float _GenerateDither;
			float _ClipNoiseTiling;
			float _OutlineWidth;
			float _OutlineMax;
			float _DiffuseLightStrength;
			float _LightAttValue;
			float _LightAttSoft;
			float _DirLightIntensity;
			float _ClipNoiseScale;
			float _RampAddincludePower;
			float _NormalStrength;
			float _DirLightNormalScale;
			float _RampAddwithoutPower;
			float _RampCelluloidNoiseValue;
			float _RampCelluloidNoiseSoftness;
			float _DeadClip;
			float _RampPower;
			float _Sprint;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			sampler2D _AlbedoMap;
			sampler2D _IndirectDiffuseLightNormal;
			sampler2D _ShadowRemapMap;
			sampler2D _NormalMap;
			sampler2D _RampCelluloidNoiseTexture;


			float3 ASEIndirectDiffuse( float2 uvStaticLightmap, float3 normalWS )
			{
			#ifdef LIGHTMAP_ON
				return SampleLightmap( uvStaticLightmap, normalWS );
			#else
				return SampleSH(normalWS);
			#endif
			}
			
			float3 AdditionalLightsLambert( float3 WorldPosition, float3 WorldNormal )
			{
				float3 Color = 0;
				#ifdef _ADDITIONAL_LIGHTS
				int numLights = GetAdditionalLightsCount();
				for(int i = 0; i<numLights;i++)
				{
					Light light = GetAdditionalLight(i, WorldPosition);
					half3 AttLightColor = light.color *(light.distanceAttenuation * light.shadowAttenuation);
					Color +=LightingLambert(AttLightColor, light.direction, WorldNormal);
					
				}
				#endif
				return Color;
			}
			
			
			float4 SampleGradient( Gradient gradient, float time )
			{
				float3 color = gradient.colors[0].rgb;
				UNITY_UNROLL
				for (int c = 1; c < 8; c++)
				{
				float colorPos = saturate((time - gradient.colors[c-1].w) / ( 0.00001 + (gradient.colors[c].w - gradient.colors[c-1].w)) * step(c, gradient.colorsLength-1));
				color = lerp(color, gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), gradient.type));
				}
				#ifndef UNITY_COLORSPACE_GAMMA
				color = SRGBToLinear(color);
				#endif
				float alpha = gradient.alphas[0].x;
				UNITY_UNROLL
				for (int a = 1; a < 8; a++)
				{
				float alphaPos = saturate((time - gradient.alphas[a-1].y) / ( 0.00001 + (gradient.alphas[a].y - gradient.alphas[a-1].y)) * step(a, gradient.alphasLength-1));
				alpha = lerp(alpha, gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), gradient.type));
				}
				return float4(color, alpha);
			}
			
			float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }
			float snoise( float2 v )
			{
				const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
				float2 i = floor( v + dot( v, C.yy ) );
				float2 x0 = v - i + dot( i, C.xx );
				float2 i1;
				i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
				float4 x12 = x0.xyxy + C.xxzz;
				x12.xy -= i1;
				i = mod2D289( i );
				float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
				float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
				m = m * m;
				m = m * m;
				float3 x = 2.0 * frac( p * C.www ) - 1.0;
				float3 h = abs( x ) - 0.5;
				float3 ox = floor( x + 0.5 );
				float3 a0 = x - ox;
				m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
				float3 g;
				g.x = a0.x * x0.x + h.x * x0.y;
				g.yz = a0.yz * x12.xz + h.yz * x12.yw;
				return 130.0 * dot( m, g );
			}
			
			inline float noise_randomValue (float2 uv) { return frac(sin(dot(uv, float2(12.9898, 78.233)))*43758.5453); }
			inline float noise_interpolate (float a, float b, float t) { return (1.0-t)*a + (t*b); }
			inline float valueNoise (float2 uv)
			{
				float2 i = floor(uv);
				float2 f = frac( uv );
				f = f* f * (3.0 - 2.0 * f);
				uv = abs( frac(uv) - 0.5);
				float2 c0 = i + float2( 0.0, 0.0 );
				float2 c1 = i + float2( 1.0, 0.0 );
				float2 c2 = i + float2( 0.0, 1.0 );
				float2 c3 = i + float2( 1.0, 1.0 );
				float r0 = noise_randomValue( c0 );
				float r1 = noise_randomValue( c1 );
				float r2 = noise_randomValue( c2 );
				float r3 = noise_randomValue( c3 );
				float bottomOfGrid = noise_interpolate( r0, r1, f.x );
				float topOfGrid = noise_interpolate( r2, r3, f.x );
				float t = noise_interpolate( bottomOfGrid, topOfGrid, f.y );
				return t;
			}
			
			float SimpleNoise(float2 UV)
			{
				float t = 0.0;
				float freq = pow( 2.0, float( 0 ) );
				float amp = pow( 0.5, float( 3 - 0 ) );
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(1));
				amp = pow(0.5, float(3-1));
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(2));
				amp = pow(0.5, float(3-2));
				t += valueNoise( UV/freq )*amp;
				return t;
			}
			

			VertexOutput VertexFunction( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float2 uv_AlbedoMap = v.ase_texcoord.xy * _AlbedoMap_ST.xy + _AlbedoMap_ST.zw;
				float4 tex2DNode8 = tex2Dlod( _AlbedoMap, float4( uv_AlbedoMap, 0, 0.0) );
				float4 unityObjectToClipPos31 = TransformWorldToHClip(TransformObjectToWorld(v.vertex.xyz));
				
				float3 ase_worldNormal = TransformObjectToWorldNormal(v.ase_normal);
				OUTPUT_LIGHTMAP_UV( v.texcoord1, unity_LightmapST, o.lightmapUVOrVertexSH.xy );
				OUTPUT_SH( ase_worldNormal, o.lightmapUVOrVertexSH.xyz );
				float3 ase_worldTangent = TransformObjectToWorldDir(v.ase_tangent.xyz);
				o.ase_texcoord5.xyz = ase_worldTangent;
				o.ase_texcoord6.xyz = ase_worldNormal;
				float ase_vertexTangentSign = v.ase_tangent.w * unity_WorldTransformParams.w;
				float3 ase_worldBitangent = cross( ase_worldNormal, ase_worldTangent ) * ase_vertexTangentSign;
				o.ase_texcoord7.xyz = ase_worldBitangent;
				
				float4 ase_clipPos = TransformObjectToHClip((v.vertex).xyz);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord8 = screenPos;
				
				o.ase_texcoord3.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord3.zw = 0;
				o.ase_texcoord5.w = 0;
				o.ase_texcoord6.w = 0;
				o.ase_texcoord7.w = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = ( v.ase_normal * _OutlineWidth * tex2DNode8.a * min( unityObjectToClipPos31.w , _OutlineMax ) );
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif
				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );
				float4 positionCS = TransformWorldToHClip( positionWS );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				VertexPositionInputs vertexInput = (VertexPositionInputs)0;
				vertexInput.positionWS = positionWS;
				vertexInput.positionCS = positionCS;
				o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				#ifdef ASE_FOG
				o.fogFactor = ComputeFogFactor( positionCS.z );
				#endif
				o.clipPos = positionCS;
				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
				float4 ase_tangent : TANGENT;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord = v.ase_texcoord;
				o.texcoord1 = v.texcoord1;
				o.ase_tangent = v.ase_tangent;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				o.texcoord1 = patch[0].texcoord1 * bary.x + patch[1].texcoord1 * bary.y + patch[2].texcoord1 * bary.z;
				o.ase_tangent = patch[0].ase_tangent * bary.x + patch[1].ase_tangent * bary.y + patch[2].ase_tangent * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag ( VertexOutput IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif
				float2 uv_AlbedoMap = IN.ase_texcoord3.xy * _AlbedoMap_ST.xy + _AlbedoMap_ST.zw;
				float4 tex2DNode8 = tex2D( _AlbedoMap, uv_AlbedoMap );
				float4 temp_output_11_0 = ( _AlbedoEmissionColor * tex2DNode8 );
				float2 uv_IndirectDiffuseLightNormal = IN.ase_texcoord3.xy * _IndirectDiffuseLightNormal_ST.xy + _IndirectDiffuseLightNormal_ST.zw;
				float3 ase_worldTangent = IN.ase_texcoord5.xyz;
				float3 ase_worldNormal = IN.ase_texcoord6.xyz;
				float3 ase_worldBitangent = IN.ase_texcoord7.xyz;
				float3 tanToWorld0 = float3( ase_worldTangent.x, ase_worldBitangent.x, ase_worldNormal.x );
				float3 tanToWorld1 = float3( ase_worldTangent.y, ase_worldBitangent.y, ase_worldNormal.y );
				float3 tanToWorld2 = float3( ase_worldTangent.z, ase_worldBitangent.z, ase_worldNormal.z );
				float3 tanNormal24 = UnpackNormalScale( tex2D( _IndirectDiffuseLightNormal, uv_IndirectDiffuseLightNormal ), 1.0f );
				float3 bakedGI24 = ASEIndirectDiffuse( IN.lightmapUVOrVertexSH.xy, float3(dot(tanToWorld0,tanNormal24), dot(tanToWorld1,tanNormal24), dot(tanToWorld2,tanNormal24)));
				float ase_lightAtten = 0;
				Light ase_lightAtten_mainLight = GetMainLight( ShadowCoords );
				ase_lightAtten = ase_lightAtten_mainLight.distanceAttenuation * ase_lightAtten_mainLight.shadowAttenuation;
				float smoothstepResult158 = smoothstep( _LightAttValue , ( _LightAttValue + _LightAttSoft ) , ase_lightAtten);
				float3 temp_output_15_0 = ( smoothstepResult158 * ( _MainLightColor.rgb * _MainLightColor.a * _DirLightIntensity ) );
				float3 LightAtt209 = temp_output_15_0;
				float saferPower192 = max( _RampAddincludePower , 0.0001 );
				float2 uv_NormalMap = IN.ase_texcoord3.xy * _NormalMap_ST.xy + _NormalMap_ST.zw;
				float3 unpack42 = UnpackNormalScale( tex2D( _NormalMap, uv_NormalMap ), _NormalStrength );
				unpack42.z = lerp( 1, unpack42.z, saturate(_NormalStrength) );
				float3 Normal43 = unpack42;
				float3 tanNormal2 = Normal43;
				float3 worldNormal2 = float3(dot(tanToWorld0,tanNormal2), dot(tanToWorld1,tanNormal2), dot(tanToWorld2,tanNormal2));
				float dotResult3 = dot( worldNormal2 , SafeNormalize(_MainLightPosition.xyz) );
				float3 break16 = temp_output_15_0;
				float3 WorldPosition5_g1 = WorldPosition;
				float3 tanNormal12_g1 = Normal43;
				float3 worldNormal12_g1 = float3(dot(tanToWorld0,tanNormal12_g1), dot(tanToWorld1,tanNormal12_g1), dot(tanToWorld2,tanNormal12_g1));
				float3 WorldNormal5_g1 = worldNormal12_g1;
				float3 localAdditionalLightsLambert5_g1 = AdditionalLightsLambert( WorldPosition5_g1 , WorldNormal5_g1 );
				float3 SRPAdditionLight203 = localAdditionalLightsLambert5_g1;
				float3 break67 = SRPAdditionLight203;
				float2 uv_RampCelluloidNoiseTexture = IN.ase_texcoord3.xy * _RampCelluloidNoiseTexture_ST.xy + _RampCelluloidNoiseTexture_ST.zw;
				float smoothstepResult166 = smoothstep( _RampCelluloidNoiseValue , _RampCelluloidNoiseSoftness , tex2D( _RampCelluloidNoiseTexture, uv_RampCelluloidNoiseTexture ).r);
				float temp_output_165_0 = ( ( pow( saferPower192 , _RampPower ) + ( (dotResult3*_DirLightNormalScale + 0.5) * ( max( max( break16.x , break16.y ) , break16.z ) + max( max( break67.x , break67.y ) , break67.z ) ) ) + _RampAddwithoutPower ) * smoothstepResult166 );
				float2 temp_cast_2 = (temp_output_165_0).xx;
				Gradient gradient6 = NewGradient( 0, 3, 2, float4( 0.9150943, 0.4437344, 0.4437344, 0.2515908 ), float4( 0.9811321, 0.6723437, 0.4665005, 0.6992905 ), float4( 0.9853496, 1, 0.6264151, 1 ), 0, 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
				float2 temp_cast_3 = (temp_output_165_0).xx;
				#if defined(_SHADOWREMAPTYPE_USESHADERGARDIENTCOLOR)
				float4 staticSwitch123 = SampleGradient( gradient6, temp_output_165_0 );
				#elif defined(_SHADOWREMAPTYPE_USESHADOWREMAPMAP)
				float4 staticSwitch123 = tex2D( _ShadowRemapMap, temp_cast_2 );
				#else
				float4 staticSwitch123 = tex2D( _ShadowRemapMap, temp_cast_2 );
				#endif
				float4 temp_output_26_0 = ( ( temp_output_11_0 * float4( ( bakedGI24 + _DiffuseLightStrength ) , 0.0 ) ) + ( ( temp_output_11_0 * float4( LightAtt209 , 0.0 ) ) * staticSwitch123 ) + ( temp_output_11_0 * float4( SRPAdditionLight203 , 0.0 ) ) );
				
				float4 screenPos = IN.ase_texcoord8;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float simplePerlin2D218 = snoise( ase_screenPosNorm.xy*5000.0 );
				simplePerlin2D218 = simplePerlin2D218*0.5 + 0.5;
				float2 temp_cast_7 = (_ClipNoiseTiling).xx;
				float2 texCoord175 = IN.ase_texcoord3.xy * temp_cast_7 + float2( 0,0 );
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - WorldPosition );
				ase_worldViewDir = normalize(ase_worldViewDir);
				#if defined(_DISSOLVEMODE_UV)
				float4 staticSwitch229 = float4( texCoord175, 0.0 , 0.0 );
				#elif defined(_DISSOLVEMODE_WORLDSPACE)
				float4 staticSwitch229 = float4( ( _ClipNoiseTiling * WorldPosition ) , 0.0 );
				#elif defined(_DISSOLVEMODE_SCREENSPACE)
				float4 staticSwitch229 = ( _ClipNoiseTiling * ase_screenPosNorm );
				#elif defined(_DISSOLVEMODE_VIEWSPACE)
				float4 staticSwitch229 = float4( ( _ClipNoiseTiling * ase_worldViewDir ) , 0.0 );
				#else
				float4 staticSwitch229 = float4( texCoord175, 0.0 , 0.0 );
				#endif
				float simpleNoise177 = SimpleNoise( staticSwitch229.xy*_ClipNoiseScale );
				float temp_output_180_0 = ( _DeadClip + saturate( simpleNoise177 ) );
				float temp_output_221_0 = ( saturate( ( simplePerlin2D218 - (1.0 + (( 1.0 - _GenerateDither ) - 0.0) * (-1.0 - 1.0) / (1.0 - 0.0)) ) ) + temp_output_180_0 );
				
				float3 Color = ( temp_output_26_0 * _OutlineColor ).rgb;
				float Alpha = 1;
				float AlphaClipThreshold = temp_output_221_0;

				#ifdef _ALPHATEST_ON
					clip( Alpha - AlphaClipThreshold );
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif

				#ifdef ASE_FOG
					Color = MixFog( Color, IN.fogFactor );
				#endif

				return half4( Color, Alpha );
			}

			ENDHLSL
		}

		
		Pass
		{
			
			Name "Forward"
			Tags { "LightMode"="UniversalForward" }
			
			Blend One Zero, One Zero
			ZWrite On
			ZTest LEqual
			Offset 0 , 0
			ColorMask RGBA
			

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#define _ALPHATEST_ON 1
			#define ASE_SRP_VERSION 100302

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/UnityInstancing.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

			#if ASE_SRP_VERSION <= 70108
			#define REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR
			#endif

			#include "Packages/com.unity.shadergraph/ShaderGraphLibrary/Functions.hlsl"
			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#define ASE_NEEDS_VERT_NORMAL
			#define ASE_NEEDS_FRAG_SHADOWCOORDS
			#pragma shader_feature_local _DISSOLVEMODE_UV _DISSOLVEMODE_WORLDSPACE _DISSOLVEMODE_SCREENSPACE _DISSOLVEMODE_VIEWSPACE
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma shader_feature_local _SHADOWREMAPTYPE_USESHADERGARDIENTCOLOR _SHADOWREMAPTYPE_USESHADOWREMAPMAP
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
				float4 ase_tangent : TANGENT;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				#ifdef ASE_FOG
				float fogFactor : TEXCOORD2;
				#endif
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_texcoord4 : TEXCOORD4;
				float4 lightmapUVOrVertexSH : TEXCOORD5;
				float4 ase_texcoord6 : TEXCOORD6;
				float4 ase_texcoord7 : TEXCOORD7;
				float4 ase_texcoord8 : TEXCOORD8;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _SprintColor;
			float4 _AlbedoMap_ST;
			float4 _EmissionColor;
			float4 _AlbedoEmissionColor;
			float4 _IndirectDiffuseLightNormal_ST;
			float4 _EmissionTexture_ST;
			float4 _SpecularMap_ST;
			float4 _SpecularColor;
			float4 _OutlineColor;
			float4 _RampCelluloidNoiseTexture_ST;
			float4 _ClipEdgeColor;
			float4 _NormalMap_ST;
			float4 _RimColor;
			float2 _SpecularSmoothStepMinMax;
			float2 _SpecularDirLightSRPLightIntensity;
			float2 _RimSmoothStepMinMax;
			float _SpecularSRPLightSampler;
			float _ClipEdgeSoftness;
			float _SpecularPow;
			float _SpecularIntensity;
			float _EmissionStrength;
			float _RimOffset;
			float _ClipEdge;
			float _GenerateDither;
			float _ClipNoiseTiling;
			float _OutlineWidth;
			float _OutlineMax;
			float _DiffuseLightStrength;
			float _LightAttValue;
			float _LightAttSoft;
			float _DirLightIntensity;
			float _ClipNoiseScale;
			float _RampAddincludePower;
			float _NormalStrength;
			float _DirLightNormalScale;
			float _RampAddwithoutPower;
			float _RampCelluloidNoiseValue;
			float _RampCelluloidNoiseSoftness;
			float _DeadClip;
			float _RampPower;
			float _Sprint;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			sampler2D _AlbedoMap;
			sampler2D _IndirectDiffuseLightNormal;
			sampler2D _ShadowRemapMap;
			sampler2D _NormalMap;
			sampler2D _RampCelluloidNoiseTexture;
			sampler2D _SpecularMap;
			sampler2D _EmissionTexture;


			inline float noise_randomValue (float2 uv) { return frac(sin(dot(uv, float2(12.9898, 78.233)))*43758.5453); }
			inline float noise_interpolate (float a, float b, float t) { return (1.0-t)*a + (t*b); }
			inline float valueNoise (float2 uv)
			{
				float2 i = floor(uv);
				float2 f = frac( uv );
				f = f* f * (3.0 - 2.0 * f);
				uv = abs( frac(uv) - 0.5);
				float2 c0 = i + float2( 0.0, 0.0 );
				float2 c1 = i + float2( 1.0, 0.0 );
				float2 c2 = i + float2( 0.0, 1.0 );
				float2 c3 = i + float2( 1.0, 1.0 );
				float r0 = noise_randomValue( c0 );
				float r1 = noise_randomValue( c1 );
				float r2 = noise_randomValue( c2 );
				float r3 = noise_randomValue( c3 );
				float bottomOfGrid = noise_interpolate( r0, r1, f.x );
				float topOfGrid = noise_interpolate( r2, r3, f.x );
				float t = noise_interpolate( bottomOfGrid, topOfGrid, f.y );
				return t;
			}
			
			float SimpleNoise(float2 UV)
			{
				float t = 0.0;
				float freq = pow( 2.0, float( 0 ) );
				float amp = pow( 0.5, float( 3 - 0 ) );
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(1));
				amp = pow(0.5, float(3-1));
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(2));
				amp = pow(0.5, float(3-2));
				t += valueNoise( UV/freq )*amp;
				return t;
			}
			
			float3 ASEIndirectDiffuse( float2 uvStaticLightmap, float3 normalWS )
			{
			#ifdef LIGHTMAP_ON
				return SampleLightmap( uvStaticLightmap, normalWS );
			#else
				return SampleSH(normalWS);
			#endif
			}
			
			float3 AdditionalLightsLambert( float3 WorldPosition, float3 WorldNormal )
			{
				float3 Color = 0;
				#ifdef _ADDITIONAL_LIGHTS
				int numLights = GetAdditionalLightsCount();
				for(int i = 0; i<numLights;i++)
				{
					Light light = GetAdditionalLight(i, WorldPosition);
					half3 AttLightColor = light.color *(light.distanceAttenuation * light.shadowAttenuation);
					Color +=LightingLambert(AttLightColor, light.direction, WorldNormal);
					
				}
				#endif
				return Color;
			}
			
			
			float4 SampleGradient( Gradient gradient, float time )
			{
				float3 color = gradient.colors[0].rgb;
				UNITY_UNROLL
				for (int c = 1; c < 8; c++)
				{
				float colorPos = saturate((time - gradient.colors[c-1].w) / ( 0.00001 + (gradient.colors[c].w - gradient.colors[c-1].w)) * step(c, gradient.colorsLength-1));
				color = lerp(color, gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), gradient.type));
				}
				#ifndef UNITY_COLORSPACE_GAMMA
				color = SRGBToLinear(color);
				#endif
				float alpha = gradient.alphas[0].x;
				UNITY_UNROLL
				for (int a = 1; a < 8; a++)
				{
				float alphaPos = saturate((time - gradient.alphas[a-1].y) / ( 0.00001 + (gradient.alphas[a].y - gradient.alphas[a-1].y)) * step(a, gradient.alphasLength-1));
				alpha = lerp(alpha, gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), gradient.type));
				}
				return float4(color, alpha);
			}
			
			float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }
			float snoise( float2 v )
			{
				const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
				float2 i = floor( v + dot( v, C.yy ) );
				float2 x0 = v - i + dot( i, C.xx );
				float2 i1;
				i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
				float4 x12 = x0.xyxy + C.xxzz;
				x12.xy -= i1;
				i = mod2D289( i );
				float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
				float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
				m = m * m;
				m = m * m;
				float3 x = 2.0 * frac( p * C.www ) - 1.0;
				float3 h = abs( x ) - 0.5;
				float3 ox = floor( x + 0.5 );
				float3 a0 = x - ox;
				m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
				float3 g;
				g.x = a0.x * x0.x + h.x * x0.y;
				g.yz = a0.yz * x12.xz + h.yz * x12.yw;
				return 130.0 * dot( m, g );
			}
			
			
			VertexOutput VertexFunction ( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float4 ase_clipPos = TransformObjectToHClip((v.vertex).xyz);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord4 = screenPos;
				float3 ase_worldNormal = TransformObjectToWorldNormal(v.ase_normal);
				OUTPUT_LIGHTMAP_UV( v.texcoord1, unity_LightmapST, o.lightmapUVOrVertexSH.xy );
				OUTPUT_SH( ase_worldNormal, o.lightmapUVOrVertexSH.xyz );
				float3 ase_worldTangent = TransformObjectToWorldDir(v.ase_tangent.xyz);
				o.ase_texcoord6.xyz = ase_worldTangent;
				o.ase_texcoord7.xyz = ase_worldNormal;
				float ase_vertexTangentSign = v.ase_tangent.w * unity_WorldTransformParams.w;
				float3 ase_worldBitangent = cross( ase_worldNormal, ase_worldTangent ) * ase_vertexTangentSign;
				o.ase_texcoord8.xyz = ase_worldBitangent;
				
				o.ase_texcoord3.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord3.zw = 0;
				o.ase_texcoord6.w = 0;
				o.ase_texcoord7.w = 0;
				o.ase_texcoord8.w = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = defaultVertexValue;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif
				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );
				float4 positionCS = TransformWorldToHClip( positionWS );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				VertexPositionInputs vertexInput = (VertexPositionInputs)0;
				vertexInput.positionWS = positionWS;
				vertexInput.positionCS = positionCS;
				o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				#ifdef ASE_FOG
				o.fogFactor = ComputeFogFactor( positionCS.z );
				#endif
				o.clipPos = positionCS;
				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				float4 texcoord1 : TEXCOORD1;
				float4 ase_tangent : TANGENT;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord = v.ase_texcoord;
				o.texcoord1 = v.texcoord1;
				o.ase_tangent = v.ase_tangent;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				o.texcoord1 = patch[0].texcoord1 * bary.x + patch[1].texcoord1 * bary.y + patch[2].texcoord1 * bary.z;
				o.ase_tangent = patch[0].ase_tangent * bary.x + patch[1].ase_tangent * bary.y + patch[2].ase_tangent * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag ( VertexOutput IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif
				float2 temp_cast_0 = (_ClipNoiseTiling).xx;
				float2 texCoord175 = IN.ase_texcoord3.xy * temp_cast_0 + float2( 0,0 );
				float4 screenPos = IN.ase_texcoord4;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - WorldPosition );
				ase_worldViewDir = normalize(ase_worldViewDir);
				#if defined(_DISSOLVEMODE_UV)
				float4 staticSwitch229 = float4( texCoord175, 0.0 , 0.0 );
				#elif defined(_DISSOLVEMODE_WORLDSPACE)
				float4 staticSwitch229 = float4( ( _ClipNoiseTiling * WorldPosition ) , 0.0 );
				#elif defined(_DISSOLVEMODE_SCREENSPACE)
				float4 staticSwitch229 = ( _ClipNoiseTiling * ase_screenPosNorm );
				#elif defined(_DISSOLVEMODE_VIEWSPACE)
				float4 staticSwitch229 = float4( ( _ClipNoiseTiling * ase_worldViewDir ) , 0.0 );
				#else
				float4 staticSwitch229 = float4( texCoord175, 0.0 , 0.0 );
				#endif
				float simpleNoise177 = SimpleNoise( staticSwitch229.xy*_ClipNoiseScale );
				float temp_output_180_0 = ( _DeadClip + saturate( simpleNoise177 ) );
				float smoothstepResult187 = smoothstep( _ClipEdge , ( _ClipEdge + _ClipEdgeSoftness ) , temp_output_180_0);
				float2 uv_AlbedoMap = IN.ase_texcoord3.xy * _AlbedoMap_ST.xy + _AlbedoMap_ST.zw;
				float4 tex2DNode8 = tex2D( _AlbedoMap, uv_AlbedoMap );
				float4 temp_output_11_0 = ( _AlbedoEmissionColor * tex2DNode8 );
				float2 uv_IndirectDiffuseLightNormal = IN.ase_texcoord3.xy * _IndirectDiffuseLightNormal_ST.xy + _IndirectDiffuseLightNormal_ST.zw;
				float3 ase_worldTangent = IN.ase_texcoord6.xyz;
				float3 ase_worldNormal = IN.ase_texcoord7.xyz;
				float3 ase_worldBitangent = IN.ase_texcoord8.xyz;
				float3 tanToWorld0 = float3( ase_worldTangent.x, ase_worldBitangent.x, ase_worldNormal.x );
				float3 tanToWorld1 = float3( ase_worldTangent.y, ase_worldBitangent.y, ase_worldNormal.y );
				float3 tanToWorld2 = float3( ase_worldTangent.z, ase_worldBitangent.z, ase_worldNormal.z );
				float3 tanNormal24 = UnpackNormalScale( tex2D( _IndirectDiffuseLightNormal, uv_IndirectDiffuseLightNormal ), 1.0f );
				float3 bakedGI24 = ASEIndirectDiffuse( IN.lightmapUVOrVertexSH.xy, float3(dot(tanToWorld0,tanNormal24), dot(tanToWorld1,tanNormal24), dot(tanToWorld2,tanNormal24)));
				float ase_lightAtten = 0;
				Light ase_lightAtten_mainLight = GetMainLight( ShadowCoords );
				ase_lightAtten = ase_lightAtten_mainLight.distanceAttenuation * ase_lightAtten_mainLight.shadowAttenuation;
				float smoothstepResult158 = smoothstep( _LightAttValue , ( _LightAttValue + _LightAttSoft ) , ase_lightAtten);
				float3 temp_output_15_0 = ( smoothstepResult158 * ( _MainLightColor.rgb * _MainLightColor.a * _DirLightIntensity ) );
				float3 LightAtt209 = temp_output_15_0;
				float saferPower192 = max( _RampAddincludePower , 0.0001 );
				float2 uv_NormalMap = IN.ase_texcoord3.xy * _NormalMap_ST.xy + _NormalMap_ST.zw;
				float3 unpack42 = UnpackNormalScale( tex2D( _NormalMap, uv_NormalMap ), _NormalStrength );
				unpack42.z = lerp( 1, unpack42.z, saturate(_NormalStrength) );
				float3 Normal43 = unpack42;
				float3 tanNormal2 = Normal43;
				float3 worldNormal2 = float3(dot(tanToWorld0,tanNormal2), dot(tanToWorld1,tanNormal2), dot(tanToWorld2,tanNormal2));
				float dotResult3 = dot( worldNormal2 , SafeNormalize(_MainLightPosition.xyz) );
				float3 break16 = temp_output_15_0;
				float3 WorldPosition5_g1 = WorldPosition;
				float3 tanNormal12_g1 = Normal43;
				float3 worldNormal12_g1 = float3(dot(tanToWorld0,tanNormal12_g1), dot(tanToWorld1,tanNormal12_g1), dot(tanToWorld2,tanNormal12_g1));
				float3 WorldNormal5_g1 = worldNormal12_g1;
				float3 localAdditionalLightsLambert5_g1 = AdditionalLightsLambert( WorldPosition5_g1 , WorldNormal5_g1 );
				float3 SRPAdditionLight203 = localAdditionalLightsLambert5_g1;
				float3 break67 = SRPAdditionLight203;
				float2 uv_RampCelluloidNoiseTexture = IN.ase_texcoord3.xy * _RampCelluloidNoiseTexture_ST.xy + _RampCelluloidNoiseTexture_ST.zw;
				float smoothstepResult166 = smoothstep( _RampCelluloidNoiseValue , _RampCelluloidNoiseSoftness , tex2D( _RampCelluloidNoiseTexture, uv_RampCelluloidNoiseTexture ).r);
				float temp_output_165_0 = ( ( pow( saferPower192 , _RampPower ) + ( (dotResult3*_DirLightNormalScale + 0.5) * ( max( max( break16.x , break16.y ) , break16.z ) + max( max( break67.x , break67.y ) , break67.z ) ) ) + _RampAddwithoutPower ) * smoothstepResult166 );
				float2 temp_cast_8 = (temp_output_165_0).xx;
				Gradient gradient6 = NewGradient( 0, 3, 2, float4( 0.9150943, 0.4437344, 0.4437344, 0.2515908 ), float4( 0.9811321, 0.6723437, 0.4665005, 0.6992905 ), float4( 0.9853496, 1, 0.6264151, 1 ), 0, 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
				float2 temp_cast_9 = (temp_output_165_0).xx;
				#if defined(_SHADOWREMAPTYPE_USESHADERGARDIENTCOLOR)
				float4 staticSwitch123 = SampleGradient( gradient6, temp_output_165_0 );
				#elif defined(_SHADOWREMAPTYPE_USESHADOWREMAPMAP)
				float4 staticSwitch123 = tex2D( _ShadowRemapMap, temp_cast_8 );
				#else
				float4 staticSwitch123 = tex2D( _ShadowRemapMap, temp_cast_8 );
				#endif
				float4 temp_output_26_0 = ( ( temp_output_11_0 * float4( ( bakedGI24 + _DiffuseLightStrength ) , 0.0 ) ) + ( ( temp_output_11_0 * float4( LightAtt209 , 0.0 ) ) * staticSwitch123 ) + ( temp_output_11_0 * float4( SRPAdditionLight203 , 0.0 ) ) );
				ase_worldViewDir = SafeNormalize( ase_worldViewDir );
				float3 normalizedWorldNormal = normalize( ase_worldNormal );
				float dotResult75 = dot( ase_worldViewDir , normalizedWorldNormal );
				float smoothstepResult81 = smoothstep( _RimSmoothStepMinMax.x , ( _RimSmoothStepMinMax.x + _RimSmoothStepMinMax.y ) , saturate( ( 1.0 - ( dotResult75 + _RimOffset ) ) ));
				float4 Rim201 = ( _RimColor * smoothstepResult81 * float4( LightAtt209 , 0.0 ) );
				float3 tanNormal90 = Normal43;
				float3 worldNormal90 = float3(dot(tanToWorld0,tanNormal90), dot(tanToWorld1,tanNormal90), dot(tanToWorld2,tanNormal90));
				float dotResult92 = dot( ( ase_worldViewDir + _MainLightPosition.xyz ) , worldNormal90 );
				float smoothstepResult95 = smoothstep( _SpecularSmoothStepMinMax.x , _SpecularSmoothStepMinMax.y , pow( dotResult92 , _SpecularPow ));
				float2 uv_SpecularMap = IN.ase_texcoord3.xy * _SpecularMap_ST.xy + _SpecularMap_ST.zw;
				float4 Specular101 = ( float4( ( ( LightAtt209 * _SpecularDirLightSRPLightIntensity.x ) + ( SRPAdditionLight203 * _SpecularDirLightSRPLightIntensity.y ) ) , 0.0 ) * ( smoothstepResult95 * _SpecularIntensity ) * _SpecularColor * tex2D( _SpecularMap, uv_SpecularMap ) );
				float2 uv_EmissionTexture = IN.ase_texcoord3.xy * _EmissionTexture_ST.xy + _EmissionTexture_ST.zw;
				float4 lerpResult222 = lerp( ( ( smoothstepResult187 * _ClipEdgeColor ) + ( ( ( temp_output_26_0 + Rim201 ) + Specular101 ) + ( tex2D( _EmissionTexture, uv_EmissionTexture ) * _EmissionColor * _EmissionStrength ) ) ) , _SprintColor , _Sprint);
				
				float simplePerlin2D218 = snoise( ase_screenPosNorm.xy*5000.0 );
				simplePerlin2D218 = simplePerlin2D218*0.5 + 0.5;
				float temp_output_221_0 = ( saturate( ( simplePerlin2D218 - (1.0 + (( 1.0 - _GenerateDither ) - 0.0) * (-1.0 - 1.0) / (1.0 - 0.0)) ) ) + temp_output_180_0 );
				
				float3 BakedAlbedo = 0;
				float3 BakedEmission = 0;
				float3 Color = lerpResult222.rgb;
				float Alpha = 1;
				float AlphaClipThreshold = temp_output_221_0;
				float AlphaClipThresholdShadow = 0.5;

				#ifdef _ALPHATEST_ON
					clip( Alpha - AlphaClipThreshold );
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif

				#ifdef ASE_FOG
					Color = MixFog( Color, IN.fogFactor );
				#endif

				return half4( Color, Alpha );
			}

			ENDHLSL
		}

		
		Pass
		{
			
			Name "ShadowCaster"
			Tags { "LightMode"="ShadowCaster" }

			ZWrite On
			ZTest LEqual
			AlphaToMask Off

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#define _ALPHATEST_ON 1
			#define ASE_SRP_VERSION 100302

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#pragma shader_feature_local _DISSOLVEMODE_UV _DISSOLVEMODE_WORLDSPACE _DISSOLVEMODE_SCREENSPACE _DISSOLVEMODE_VIEWSPACE
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				float4 ase_texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _SprintColor;
			float4 _AlbedoMap_ST;
			float4 _EmissionColor;
			float4 _AlbedoEmissionColor;
			float4 _IndirectDiffuseLightNormal_ST;
			float4 _EmissionTexture_ST;
			float4 _SpecularMap_ST;
			float4 _SpecularColor;
			float4 _OutlineColor;
			float4 _RampCelluloidNoiseTexture_ST;
			float4 _ClipEdgeColor;
			float4 _NormalMap_ST;
			float4 _RimColor;
			float2 _SpecularSmoothStepMinMax;
			float2 _SpecularDirLightSRPLightIntensity;
			float2 _RimSmoothStepMinMax;
			float _SpecularSRPLightSampler;
			float _ClipEdgeSoftness;
			float _SpecularPow;
			float _SpecularIntensity;
			float _EmissionStrength;
			float _RimOffset;
			float _ClipEdge;
			float _GenerateDither;
			float _ClipNoiseTiling;
			float _OutlineWidth;
			float _OutlineMax;
			float _DiffuseLightStrength;
			float _LightAttValue;
			float _LightAttSoft;
			float _DirLightIntensity;
			float _ClipNoiseScale;
			float _RampAddincludePower;
			float _NormalStrength;
			float _DirLightNormalScale;
			float _RampAddwithoutPower;
			float _RampCelluloidNoiseValue;
			float _RampCelluloidNoiseSoftness;
			float _DeadClip;
			float _RampPower;
			float _Sprint;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			

			float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }
			float snoise( float2 v )
			{
				const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
				float2 i = floor( v + dot( v, C.yy ) );
				float2 x0 = v - i + dot( i, C.xx );
				float2 i1;
				i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
				float4 x12 = x0.xyxy + C.xxzz;
				x12.xy -= i1;
				i = mod2D289( i );
				float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
				float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
				m = m * m;
				m = m * m;
				float3 x = 2.0 * frac( p * C.www ) - 1.0;
				float3 h = abs( x ) - 0.5;
				float3 ox = floor( x + 0.5 );
				float3 a0 = x - ox;
				m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
				float3 g;
				g.x = a0.x * x0.x + h.x * x0.y;
				g.yz = a0.yz * x12.xz + h.yz * x12.yw;
				return 130.0 * dot( m, g );
			}
			
			inline float noise_randomValue (float2 uv) { return frac(sin(dot(uv, float2(12.9898, 78.233)))*43758.5453); }
			inline float noise_interpolate (float a, float b, float t) { return (1.0-t)*a + (t*b); }
			inline float valueNoise (float2 uv)
			{
				float2 i = floor(uv);
				float2 f = frac( uv );
				f = f* f * (3.0 - 2.0 * f);
				uv = abs( frac(uv) - 0.5);
				float2 c0 = i + float2( 0.0, 0.0 );
				float2 c1 = i + float2( 1.0, 0.0 );
				float2 c2 = i + float2( 0.0, 1.0 );
				float2 c3 = i + float2( 1.0, 1.0 );
				float r0 = noise_randomValue( c0 );
				float r1 = noise_randomValue( c1 );
				float r2 = noise_randomValue( c2 );
				float r3 = noise_randomValue( c3 );
				float bottomOfGrid = noise_interpolate( r0, r1, f.x );
				float topOfGrid = noise_interpolate( r2, r3, f.x );
				float t = noise_interpolate( bottomOfGrid, topOfGrid, f.y );
				return t;
			}
			
			float SimpleNoise(float2 UV)
			{
				float t = 0.0;
				float freq = pow( 2.0, float( 0 ) );
				float amp = pow( 0.5, float( 3 - 0 ) );
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(1));
				amp = pow(0.5, float(3-1));
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(2));
				amp = pow(0.5, float(3-2));
				t += valueNoise( UV/freq )*amp;
				return t;
			}
			

			float3 _LightDirection;

			VertexOutput VertexFunction( VertexInput v )
			{
				VertexOutput o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );

				float4 ase_clipPos = TransformObjectToHClip((v.vertex).xyz);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord2 = screenPos;
				
				o.ase_texcoord3.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord3.zw = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = defaultVertexValue;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif

				float3 normalWS = TransformObjectToWorldDir( v.ase_normal );

				float4 clipPos = TransformWorldToHClip( ApplyShadowBias( positionWS, normalWS, _LightDirection ) );

				#if UNITY_REVERSED_Z
					clipPos.z = min(clipPos.z, clipPos.w * UNITY_NEAR_CLIP_VALUE);
				#else
					clipPos.z = max(clipPos.z, clipPos.w * UNITY_NEAR_CLIP_VALUE);
				#endif

				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = clipPos;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				o.clipPos = clipPos;

				return o;
			}
			
			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord = v.ase_texcoord;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float4 screenPos = IN.ase_texcoord2;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float simplePerlin2D218 = snoise( ase_screenPosNorm.xy*5000.0 );
				simplePerlin2D218 = simplePerlin2D218*0.5 + 0.5;
				float2 temp_cast_1 = (_ClipNoiseTiling).xx;
				float2 texCoord175 = IN.ase_texcoord3.xy * temp_cast_1 + float2( 0,0 );
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - WorldPosition );
				ase_worldViewDir = normalize(ase_worldViewDir);
				#if defined(_DISSOLVEMODE_UV)
				float4 staticSwitch229 = float4( texCoord175, 0.0 , 0.0 );
				#elif defined(_DISSOLVEMODE_WORLDSPACE)
				float4 staticSwitch229 = float4( ( _ClipNoiseTiling * WorldPosition ) , 0.0 );
				#elif defined(_DISSOLVEMODE_SCREENSPACE)
				float4 staticSwitch229 = ( _ClipNoiseTiling * ase_screenPosNorm );
				#elif defined(_DISSOLVEMODE_VIEWSPACE)
				float4 staticSwitch229 = float4( ( _ClipNoiseTiling * ase_worldViewDir ) , 0.0 );
				#else
				float4 staticSwitch229 = float4( texCoord175, 0.0 , 0.0 );
				#endif
				float simpleNoise177 = SimpleNoise( staticSwitch229.xy*_ClipNoiseScale );
				float temp_output_180_0 = ( _DeadClip + saturate( simpleNoise177 ) );
				float temp_output_221_0 = ( saturate( ( simplePerlin2D218 - (1.0 + (( 1.0 - _GenerateDither ) - 0.0) * (-1.0 - 1.0) / (1.0 - 0.0)) ) ) + temp_output_180_0 );
				
				float Alpha = 1;
				float AlphaClipThreshold = temp_output_221_0;
				float AlphaClipThresholdShadow = 0.5;

				#ifdef _ALPHATEST_ON
					#ifdef _ALPHATEST_SHADOW_ON
						clip(Alpha - AlphaClipThresholdShadow);
					#else
						clip(Alpha - AlphaClipThreshold);
					#endif
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif
				return 0;
			}

			ENDHLSL
		}

		
		Pass
		{
			
			Name "DepthOnly"
			Tags { "LightMode"="DepthOnly" }

			ZWrite On
			ColorMask 0
			AlphaToMask Off

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#define _ALPHATEST_ON 1
			#define ASE_SRP_VERSION 100302

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#pragma shader_feature_local _DISSOLVEMODE_UV _DISSOLVEMODE_WORLDSPACE _DISSOLVEMODE_SCREENSPACE _DISSOLVEMODE_VIEWSPACE
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				float4 ase_texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _SprintColor;
			float4 _AlbedoMap_ST;
			float4 _EmissionColor;
			float4 _AlbedoEmissionColor;
			float4 _IndirectDiffuseLightNormal_ST;
			float4 _EmissionTexture_ST;
			float4 _SpecularMap_ST;
			float4 _SpecularColor;
			float4 _OutlineColor;
			float4 _RampCelluloidNoiseTexture_ST;
			float4 _ClipEdgeColor;
			float4 _NormalMap_ST;
			float4 _RimColor;
			float2 _SpecularSmoothStepMinMax;
			float2 _SpecularDirLightSRPLightIntensity;
			float2 _RimSmoothStepMinMax;
			float _SpecularSRPLightSampler;
			float _ClipEdgeSoftness;
			float _SpecularPow;
			float _SpecularIntensity;
			float _EmissionStrength;
			float _RimOffset;
			float _ClipEdge;
			float _GenerateDither;
			float _ClipNoiseTiling;
			float _OutlineWidth;
			float _OutlineMax;
			float _DiffuseLightStrength;
			float _LightAttValue;
			float _LightAttSoft;
			float _DirLightIntensity;
			float _ClipNoiseScale;
			float _RampAddincludePower;
			float _NormalStrength;
			float _DirLightNormalScale;
			float _RampAddwithoutPower;
			float _RampCelluloidNoiseValue;
			float _RampCelluloidNoiseSoftness;
			float _DeadClip;
			float _RampPower;
			float _Sprint;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			

			float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }
			float snoise( float2 v )
			{
				const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
				float2 i = floor( v + dot( v, C.yy ) );
				float2 x0 = v - i + dot( i, C.xx );
				float2 i1;
				i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
				float4 x12 = x0.xyxy + C.xxzz;
				x12.xy -= i1;
				i = mod2D289( i );
				float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
				float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
				m = m * m;
				m = m * m;
				float3 x = 2.0 * frac( p * C.www ) - 1.0;
				float3 h = abs( x ) - 0.5;
				float3 ox = floor( x + 0.5 );
				float3 a0 = x - ox;
				m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
				float3 g;
				g.x = a0.x * x0.x + h.x * x0.y;
				g.yz = a0.yz * x12.xz + h.yz * x12.yw;
				return 130.0 * dot( m, g );
			}
			
			inline float noise_randomValue (float2 uv) { return frac(sin(dot(uv, float2(12.9898, 78.233)))*43758.5453); }
			inline float noise_interpolate (float a, float b, float t) { return (1.0-t)*a + (t*b); }
			inline float valueNoise (float2 uv)
			{
				float2 i = floor(uv);
				float2 f = frac( uv );
				f = f* f * (3.0 - 2.0 * f);
				uv = abs( frac(uv) - 0.5);
				float2 c0 = i + float2( 0.0, 0.0 );
				float2 c1 = i + float2( 1.0, 0.0 );
				float2 c2 = i + float2( 0.0, 1.0 );
				float2 c3 = i + float2( 1.0, 1.0 );
				float r0 = noise_randomValue( c0 );
				float r1 = noise_randomValue( c1 );
				float r2 = noise_randomValue( c2 );
				float r3 = noise_randomValue( c3 );
				float bottomOfGrid = noise_interpolate( r0, r1, f.x );
				float topOfGrid = noise_interpolate( r2, r3, f.x );
				float t = noise_interpolate( bottomOfGrid, topOfGrid, f.y );
				return t;
			}
			
			float SimpleNoise(float2 UV)
			{
				float t = 0.0;
				float freq = pow( 2.0, float( 0 ) );
				float amp = pow( 0.5, float( 3 - 0 ) );
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(1));
				amp = pow(0.5, float(3-1));
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(2));
				amp = pow(0.5, float(3-2));
				t += valueNoise( UV/freq )*amp;
				return t;
			}
			

			VertexOutput VertexFunction( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float4 ase_clipPos = TransformObjectToHClip((v.vertex).xyz);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord2 = screenPos;
				
				o.ase_texcoord3.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord3.zw = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = defaultVertexValue;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif

				o.clipPos = TransformWorldToHClip( positionWS );
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = clipPos;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord = v.ase_texcoord;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float4 screenPos = IN.ase_texcoord2;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float simplePerlin2D218 = snoise( ase_screenPosNorm.xy*5000.0 );
				simplePerlin2D218 = simplePerlin2D218*0.5 + 0.5;
				float2 temp_cast_1 = (_ClipNoiseTiling).xx;
				float2 texCoord175 = IN.ase_texcoord3.xy * temp_cast_1 + float2( 0,0 );
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - WorldPosition );
				ase_worldViewDir = normalize(ase_worldViewDir);
				#if defined(_DISSOLVEMODE_UV)
				float4 staticSwitch229 = float4( texCoord175, 0.0 , 0.0 );
				#elif defined(_DISSOLVEMODE_WORLDSPACE)
				float4 staticSwitch229 = float4( ( _ClipNoiseTiling * WorldPosition ) , 0.0 );
				#elif defined(_DISSOLVEMODE_SCREENSPACE)
				float4 staticSwitch229 = ( _ClipNoiseTiling * ase_screenPosNorm );
				#elif defined(_DISSOLVEMODE_VIEWSPACE)
				float4 staticSwitch229 = float4( ( _ClipNoiseTiling * ase_worldViewDir ) , 0.0 );
				#else
				float4 staticSwitch229 = float4( texCoord175, 0.0 , 0.0 );
				#endif
				float simpleNoise177 = SimpleNoise( staticSwitch229.xy*_ClipNoiseScale );
				float temp_output_180_0 = ( _DeadClip + saturate( simpleNoise177 ) );
				float temp_output_221_0 = ( saturate( ( simplePerlin2D218 - (1.0 + (( 1.0 - _GenerateDither ) - 0.0) * (-1.0 - 1.0) / (1.0 - 0.0)) ) ) + temp_output_180_0 );
				
				float Alpha = 1;
				float AlphaClipThreshold = temp_output_221_0;

				#ifdef _ALPHATEST_ON
					clip(Alpha - AlphaClipThreshold);
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif
				return 0;
			}
			ENDHLSL
		}

		
		Pass
		{
			
			Name "Meta"
			Tags { "LightMode"="Meta" }

			Cull Off

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#define _ALPHATEST_ON 1
			#define ASE_SRP_VERSION 100302

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/MetaInput.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#pragma shader_feature_local _DISSOLVEMODE_UV _DISSOLVEMODE_WORLDSPACE _DISSOLVEMODE_SCREENSPACE _DISSOLVEMODE_VIEWSPACE
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 texcoord1 : TEXCOORD1;
				float4 texcoord2 : TEXCOORD2;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				float4 ase_texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _SprintColor;
			float4 _AlbedoMap_ST;
			float4 _EmissionColor;
			float4 _AlbedoEmissionColor;
			float4 _IndirectDiffuseLightNormal_ST;
			float4 _EmissionTexture_ST;
			float4 _SpecularMap_ST;
			float4 _SpecularColor;
			float4 _OutlineColor;
			float4 _RampCelluloidNoiseTexture_ST;
			float4 _ClipEdgeColor;
			float4 _NormalMap_ST;
			float4 _RimColor;
			float2 _SpecularSmoothStepMinMax;
			float2 _SpecularDirLightSRPLightIntensity;
			float2 _RimSmoothStepMinMax;
			float _SpecularSRPLightSampler;
			float _ClipEdgeSoftness;
			float _SpecularPow;
			float _SpecularIntensity;
			float _EmissionStrength;
			float _RimOffset;
			float _ClipEdge;
			float _GenerateDither;
			float _ClipNoiseTiling;
			float _OutlineWidth;
			float _OutlineMax;
			float _DiffuseLightStrength;
			float _LightAttValue;
			float _LightAttSoft;
			float _DirLightIntensity;
			float _ClipNoiseScale;
			float _RampAddincludePower;
			float _NormalStrength;
			float _DirLightNormalScale;
			float _RampAddwithoutPower;
			float _RampCelluloidNoiseValue;
			float _RampCelluloidNoiseSoftness;
			float _DeadClip;
			float _RampPower;
			float _Sprint;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			

			float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }
			float snoise( float2 v )
			{
				const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
				float2 i = floor( v + dot( v, C.yy ) );
				float2 x0 = v - i + dot( i, C.xx );
				float2 i1;
				i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
				float4 x12 = x0.xyxy + C.xxzz;
				x12.xy -= i1;
				i = mod2D289( i );
				float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
				float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
				m = m * m;
				m = m * m;
				float3 x = 2.0 * frac( p * C.www ) - 1.0;
				float3 h = abs( x ) - 0.5;
				float3 ox = floor( x + 0.5 );
				float3 a0 = x - ox;
				m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
				float3 g;
				g.x = a0.x * x0.x + h.x * x0.y;
				g.yz = a0.yz * x12.xz + h.yz * x12.yw;
				return 130.0 * dot( m, g );
			}
			
			inline float noise_randomValue (float2 uv) { return frac(sin(dot(uv, float2(12.9898, 78.233)))*43758.5453); }
			inline float noise_interpolate (float a, float b, float t) { return (1.0-t)*a + (t*b); }
			inline float valueNoise (float2 uv)
			{
				float2 i = floor(uv);
				float2 f = frac( uv );
				f = f* f * (3.0 - 2.0 * f);
				uv = abs( frac(uv) - 0.5);
				float2 c0 = i + float2( 0.0, 0.0 );
				float2 c1 = i + float2( 1.0, 0.0 );
				float2 c2 = i + float2( 0.0, 1.0 );
				float2 c3 = i + float2( 1.0, 1.0 );
				float r0 = noise_randomValue( c0 );
				float r1 = noise_randomValue( c1 );
				float r2 = noise_randomValue( c2 );
				float r3 = noise_randomValue( c3 );
				float bottomOfGrid = noise_interpolate( r0, r1, f.x );
				float topOfGrid = noise_interpolate( r2, r3, f.x );
				float t = noise_interpolate( bottomOfGrid, topOfGrid, f.y );
				return t;
			}
			
			float SimpleNoise(float2 UV)
			{
				float t = 0.0;
				float freq = pow( 2.0, float( 0 ) );
				float amp = pow( 0.5, float( 3 - 0 ) );
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(1));
				amp = pow(0.5, float(3-1));
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(2));
				amp = pow(0.5, float(3-2));
				t += valueNoise( UV/freq )*amp;
				return t;
			}
			

			VertexOutput VertexFunction( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float4 ase_clipPos = TransformObjectToHClip((v.vertex).xyz);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord2 = screenPos;
				
				o.ase_texcoord3.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord3.zw = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = defaultVertexValue;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif

				o.clipPos = MetaVertexPosition( v.vertex, v.texcoord1.xy, v.texcoord1.xy, unity_LightmapST, unity_DynamicLightmapST );
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = o.clipPos;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord = v.ase_texcoord;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float4 screenPos = IN.ase_texcoord2;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float simplePerlin2D218 = snoise( ase_screenPosNorm.xy*5000.0 );
				simplePerlin2D218 = simplePerlin2D218*0.5 + 0.5;
				float2 temp_cast_1 = (_ClipNoiseTiling).xx;
				float2 texCoord175 = IN.ase_texcoord3.xy * temp_cast_1 + float2( 0,0 );
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - WorldPosition );
				ase_worldViewDir = normalize(ase_worldViewDir);
				#if defined(_DISSOLVEMODE_UV)
				float4 staticSwitch229 = float4( texCoord175, 0.0 , 0.0 );
				#elif defined(_DISSOLVEMODE_WORLDSPACE)
				float4 staticSwitch229 = float4( ( _ClipNoiseTiling * WorldPosition ) , 0.0 );
				#elif defined(_DISSOLVEMODE_SCREENSPACE)
				float4 staticSwitch229 = ( _ClipNoiseTiling * ase_screenPosNorm );
				#elif defined(_DISSOLVEMODE_VIEWSPACE)
				float4 staticSwitch229 = float4( ( _ClipNoiseTiling * ase_worldViewDir ) , 0.0 );
				#else
				float4 staticSwitch229 = float4( texCoord175, 0.0 , 0.0 );
				#endif
				float simpleNoise177 = SimpleNoise( staticSwitch229.xy*_ClipNoiseScale );
				float temp_output_180_0 = ( _DeadClip + saturate( simpleNoise177 ) );
				float temp_output_221_0 = ( saturate( ( simplePerlin2D218 - (1.0 + (( 1.0 - _GenerateDither ) - 0.0) * (-1.0 - 1.0) / (1.0 - 0.0)) ) ) + temp_output_180_0 );
				
				float3 BakedAlbedo = 0;
				float3 BakedEmission = 0;
				float Alpha = 1;
				float AlphaClipThreshold = temp_output_221_0;

				#ifdef _ALPHATEST_ON
					clip(Alpha - AlphaClipThreshold);
				#endif

				MetaInput metaInput = (MetaInput)0;
				metaInput.Albedo = BakedAlbedo;
				metaInput.Emission = BakedEmission;
				
				return MetaFragment(metaInput);
			}
			ENDHLSL
		}
		
	}
	CustomEditor "UnityEditor.ShaderGraph.PBRMasterGUI"
	Fallback "Hidden/InternalErrorShader"
	
}
/*ASEBEGIN
Version=18900
0;0;1536;812.6;1468.685;1083.816;1;True;False
Node;AmplifyShaderEditor.ScreenPosInputsNode;230;557.0043,-859.4298;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;232;581.5745,-683.6543;Inherit;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldPosInputsNode;226;355.0914,-1011.601;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;188;325.3442,-1193.885;Inherit;False;Property;_ClipNoiseTiling;ClipNoiseTiling(死亡溶解噪點紋理拉伸);40;0;Create;False;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;214;2740.056,-836.1557;Inherit;False;Property;_GenerateDither;GenerateDither;36;0;Create;True;0;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;228;799.1799,-1033.167;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;175;817.5867,-1212.357;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;231;790.0043,-859.4298;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;233;790.5745,-683.6543;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.OneMinusNode;215;3049.871,-830.4197;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;176;1311.643,-964.6332;Inherit;False;Property;_ClipNoiseScale;ClipNoiseScale(死亡溶解噪點紋理大小);41;0;Create;False;0;0;0;False;0;False;10;10;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;216;2870.203,-1176.045;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;229;1069.004,-1109.43;Inherit;False;Property;_DissolveMode;Dissolve Mode;44;0;Create;True;0;0;0;False;0;False;0;0;0;True;;KeywordEnum;4;UV;WorldSpace;ScreenSpace;ViewSpace;Create;True;True;9;1;FLOAT4;0,0,0,0;False;0;FLOAT4;0,0,0,0;False;2;FLOAT4;0,0,0,0;False;3;FLOAT4;0,0,0,0;False;4;FLOAT4;0,0,0,0;False;5;FLOAT4;0,0,0,0;False;6;FLOAT4;0,0,0,0;False;7;FLOAT4;0,0,0,0;False;8;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TFHCRemapNode;217;3239.755,-829.9717;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;1;False;4;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;177;1516.677,-1152.396;Inherit;True;Simple;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;17.21;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;218;3065.057,-1176.426;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;5000;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;219;3347.101,-1172.889;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;178;1431.006,-1342.442;Inherit;False;Property;_DeadClip;_DeadClip(死亡溶解控制);35;1;[Header];Create;False;1;Dead Dissolve Setting;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;179;1744.385,-1144.662;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;40;107.4315,462.4314;Inherit;False;1417.653;1005.605;Comment;6;29;39;36;30;28;53;Outline;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;121;-919.228,2558.679;Inherit;False;596.8665;382.1125;Comment;4;119;117;120;207;日光/燈光強度調整;1,1,1,1;0;0
Node;AmplifyShaderEditor.SaturateNode;220;3525.141,-1175.459;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;49;-725.8527,-998.4443;Inherit;False;546.8332;461.799;Comment;3;8;10;11;Albedo;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;208;-3786.049,304.2767;Inherit;False;740.6223;260.14;Comment;3;65;112;203;SRP Light;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;50;-644.6549,-143.2718;Inherit;False;906.9587;494.713;;6;13;7;122;165;190;198;Shadow Remap;0.6320754,0.6320754,0.6320754,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;180;1871.077,-1335.514;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;47;-1735.329,-142.6868;Inherit;False;1070.12;398.215;Comment;6;45;1;2;3;5;131;Normal Light(N.L);1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;113;-2445.705,2480.957;Inherit;False;927.4;304.3999;Comment;7;109;108;110;111;114;115;206;Specular SRPLight 光源漸變採樣計算;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;52;-1489.389,930.8528;Inherit;False;741.3994;303.9028;Color Remap模塊中自有陰影重映射的Gardient,因此若不先漸變為灰度圖,重映射會不正確;3;17;18;16;轉換為灰度圖;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;199;-1347.544,-2076.739;Inherit;False;1707.054;658.7182;;2;197;6;Ramp Gradient;1,0.9619094,0,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;70;-1487.409,1328.731;Inherit;False;743.0731;304.7168;Comment;3;67;68;69;轉換為灰度圖;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;53;184.4391,1041.779;Inherit;False;629.5074;380.8989;;4;37;31;35;32;頂點位置(物件空間轉裁減空間);1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;103;-1931.334,3052.232;Inherit;False;2880.684;912.0061;Comment;17;116;101;93;97;95;90;98;96;87;99;92;89;88;91;100;94;106;Specular;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;213;-1440.361,367.9636;Inherit;False;835.4715;413.2869;Comment;5;172;166;169;163;170;Celluloid Texture;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;51;-562.3288,-424.9936;Inherit;False;602.1977;121;間接漫射光從Unity的全局照明系統獲取漫射的環境光。這相當於說它檢索周圍的光探測器的信息。;1;24;漫射光照;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;48;-2055.019,621.7488;Inherit;False;506.8718;338.4612;Comment;5;14;15;124;125;158;Light Color & Attenuation;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;102;-1652.828,1817.899;Inherit;False;1581.267;646.9999;;12;82;76;74;75;80;81;78;73;77;83;85;189;Rim ;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;46;-3761.005,-154.5279;Inherit;False;1169.146;309.3865;Comment;3;43;42;44;Normal;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;43;-2816.659,-103.5414;Inherit;True;Normal;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;51.40301,-802.3359;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;71;-685.5591,978.8214;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;9;667.6438,-159.0945;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;35;311.7556,1286.288;Inherit;False;Property;_OutlineMax;Outline Max外描邊最大閾值(Accroding to Camera Distance)(根據照相機遠近) ;17;0;Create;False;0;0;0;False;0;False;0;0.03;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;30;472.5356,865.4287;Inherit;False;Property;_OutlineWidth;Outline Width(外描邊寬度);16;1;[Header];Create;False;1;Outline;0;0;False;0;False;0.1;0.1;0;0.1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;190;-464.1931,-38.94222;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;191;-815.1287,-296.2067;Inherit;False;Property;_RampAddincludePower;Ramp Add(include Power);13;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;92;-1344.334,3336.766;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;161;-2361.237,716.5262;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;2;-1386.231,-86.19307;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;160;-2618.927,713.905;Inherit;False;Property;_LightAttSoft;Light Att Soft(光衰減平滑度);31;0;Create;False;0;0;0;False;0;False;0.46;0.46;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;119;-600.7057,2805.991;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;18;-983.3943,981.3561;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;99;-252.3093,3316.284;Inherit;True;4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SmoothstepOpNode;81;-492.8645,2052.062;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;85;-912.0933,2298.396;Inherit;False;Property;_RimSmoothStepMinMax;Rim SmoothStepValue and Smoothness(泛光平滑閾值);21;0;Create;False;0;0;0;True;0;False;0,1;0,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;72;-142.9164,-538.8558;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;203;-3287.027,354.2767;Inherit;False;SRPAdditionLight;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;165;-343.5769,-38.36261;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.31;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;105;1322.589,-147.2235;Inherit;False;101;Specular;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;159;-2620.927,632.905;Inherit;False;Property;_LightAttValue;Light Att Value(光衰減程度);30;1;[Header];Create;False;1;Light Attenuation;0;0;False;0;False;-0.3;-0.3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;221;3561.303,-824.9984;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;16;-1441.063,980.0394;Inherit;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RangedFloatNode;134;-553.49,-292.229;Inherit;False;Property;_DiffuseLightStrength;DiffuseLight Strength(環境光強度(疊加);9;0;Create;False;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;110;-1843.675,2530.957;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;111;-2395.705,2669.957;Inherit;False;Property;_SpecularSRPLightSampler;Specular SRPLight Sampler(高光燈光採樣次數);27;0;Create;False;0;0;0;True;0;False;5;5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;123;250.7033,15.29237;Inherit;True;Property;_ShadowRemapType;Shadow Remap Type(陰影重映射方式)(左為背光右為迎光);12;0;Create;False;0;0;0;True;0;False;0;1;1;True;;KeywordEnum;2;UseShaderGardientColor;UseShadowRemapMap;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;107;-197.0791,2785.457;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;173;-1077.302,-388.5433;Inherit;True;Property;_IndirectDiffuseLightNormal;Indirect Diffuse Light Normal(漫射光法線貼圖);10;0;Create;False;0;0;0;False;0;False;-1;None;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DotProductOpNode;75;-1363.693,2050.069;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;3;-1154.231,-17.19307;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;163;-1102.991,417.9636;Inherit;True;Property;_RampCelluloidNoiseTexture;Ramp Celluloid Noise Texture(賽璐璐陰影邊緣噪波貼圖);32;1;[Header];Create;False;1;Celluloid Edge Noise Texture;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;73;-1580.863,1964.308;Inherit;False;World;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;184;1701.198,-973.901;Inherit;False;Property;_ClipEdgeSoftness;ClipEdgeSoftness(死亡溶解邊緣顏色平滑度);39;0;Create;False;0;0;0;False;0;False;0.01;0.01;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;94;-1345.334,3456.766;Inherit;False;Property;_SpecularPow;Specular Pow(高光量值);23;0;Create;False;0;0;0;False;0;False;4;4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;202;1079.665,-273.0325;Inherit;False;201;Rim;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;204;-463.7957,-520.6062;Inherit;False;203;SRPAdditionLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.Vector2Node;96;-1341.25,3537.111;Inherit;False;Property;_SpecularSmoothStepMinMax;Specular SmoothStep Value & Smoothness(高光值與平滑度);25;0;Create;False;0;0;0;False;0;False;0,0.01;0,0.01;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.GetLocalVarNode;112;-3736.049,449.0168;Inherit;False;43;Normal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;91;-1881.334,3484.766;Inherit;False;43;Normal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;196;-627.5509,214.0154;Inherit;False;Property;_RampAddwithoutPower;Ramp Add(without Power);14;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-667.6294,-14.88554;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;125;-2186.869,876.6174;Inherit;False;Property;_DirLightIntensity;DirLight Intensity(日光強度);7;1;[Header];Create;False;1;Light;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;224;2698.877,-542.126;Inherit;False;Property;_SprintColor;Sprint Color;42;1;[HDR];Create;True;0;0;0;False;0;False;2.297397,0.9640058,0.2072162,0;2.297397,0.9640058,0.2072162,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;210;26.27039,-289.6051;Inherit;False;209;LightAtt;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;206;-2349.317,2525.791;Inherit;False;203;SRPAdditionLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;11;-341.4195,-809.9456;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;139;1588.464,46.05324;Inherit;True;Property;_EmissionTexture;Emission Texture;4;1;[Header];Create;True;1;Emission;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;201;-1.40047,2027.621;Inherit;False;Rim;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LightAttenuation;100;-532.2645,3234.371;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;182;1733.446,-1057.547;Inherit;False;Property;_ClipEdge;ClipEdge(死亡溶解邊緣顏色範圍);38;0;Create;False;0;0;0;False;0;False;0.99;0.99;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;89;-1571.821,3207.149;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;205;-1763.531,1373.2;Inherit;False;203;SRPAdditionLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;116;-628.0491,3730.579;Inherit;True;Property;_SpecularMap;Specular Map(高光貼圖);22;1;[Header];Create;False;1;Specular;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;136;1675.038,275.4757;Inherit;False;Property;_EmissionColor;Emission Color;5;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,1;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;77;-1362.214,2335.58;Inherit;False;Property;_RimOffset;Rim Offset(泛光量值);19;1;[Header];Create;False;1;Rim;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;225;2640.877,-333.126;Inherit;False;Property;_Sprint;_Sprint(衝刺);43;0;Create;False;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.UnityObjToClipPosHlpNode;31;391.2534,1094.421;Inherit;False;1;0;FLOAT3;0,0,0;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;39;1110.67,509.4182;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;140;1687.493,449.0935;Inherit;False;Property;_EmissionStrength;Emission Strength;6;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;17;-1233.066,980.0394;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;198;-655.8887,-103.998;Inherit;False;197;RampGradient;1;0;OBJECT;;False;1;OBJECT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;185;2535.896,-1120.15;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;65;-3508.587,452.7984;Inherit;False;SRP Additional Light;-1;;1;6c86746ad131a0a408ca599df5f40861;3,6,1,9,0,23,0;5;2;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;15;FLOAT3;0,0,0;False;14;FLOAT3;1,1,1;False;18;FLOAT;0.5;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;98;-924.3385,3468.558;Inherit;False;Property;_SpecularIntensity;Specular Intensity(高光強度);26;0;Create;False;0;0;0;True;0;False;0;0.1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;124;-1926.444,773.9294;Inherit;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GradientNode;6;-767.6221,-1781.876;Inherit;False;0;3;2;0.9150943,0.4437344,0.4437344,0.2515908;0.9811321,0.6723437,0.4665005,0.6992905;0.9853496,1,0.6264151,1;1,0;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;138;1958.569,221.3665;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;76;-1095.102,2049.899;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;158;-1987.824,638.7889;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;74;-1602.828,2137.635;Inherit;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.NormalVertexDataNode;28;571.3978,713.4045;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SmoothstepOpNode;187;2273.031,-1341.66;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0.2;False;2;FLOAT;0.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;68;-1229.409,1380.731;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-3711.005,-58.56604;Inherit;False;Property;_NormalStrength;Normal Strength(法線強度);3;0;Create;False;0;0;0;True;0;False;0;0;0;2;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;5;-932.609,-17.07917;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0.5;False;2;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;104;1683.779,-266.7266;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;192;-538.2935,-223.1223;Inherit;False;True;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;114;-1717.589,2531.599;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;1,1,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;186;1914.779,-979.6716;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.TransformPositionNode;227;551.0247,-1016.486;Inherit;False;World;Object;False;Fast;True;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;108;-2101.288,2531.045;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;84;1280.921,-392.2037;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;135;2127.759,-261.2296;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;69;-979.7379,1382.048;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;80;-655.7444,2051.484;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;212;-650.5209,2463.829;Inherit;False;209;LightAtt;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;193;-846.1345,-215.3708;Inherit;False;Property;_RampPower;Ramp Power;15;0;Create;True;0;0;0;False;0;False;1;1;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;97;-540.3385,3339.558;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;183;2779.869,-673.2944;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FloorOpNode;109;-1965.575,2530.957;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;791.7612,785.7435;Inherit;True;4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;122;-171.1407,112.4082;Inherit;True;Property;_ShadowRemapMap;Shadow Remap Map;11;1;[Header];Create;True;1;Shadow Type;0;0;False;0;False;-1;None;8b9086a9b225a584fb43685652719251;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;117;-484.7615,2614.423;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;133;-257.3873,-370.4496;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;222;3095.285,-671.8954;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.IndirectDiffuseLighting;24;-529.329,-380.9936;Inherit;False;Tangent;1;0;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;36;538.6442,529.1975;Inherit;False;Property;_OutlineColor;Outline Color(外描邊顏色);18;1;[HDR];Create;False;0;0;0;False;0;False;0.1981132,0.1981132,0.1981132,0;0.1981132,0.1981132,0.1981132,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PowerNode;93;-1146.334,3338.766;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;170;-1367.16,665.8505;Inherit;False;Property;_RampCelluloidNoiseSoftness;Ramp Celluloid Noise Softness(賽璐璐陰影平滑度);34;0;Create;False;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;37;636.017,1187.584;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;101;240.0562,3309.223;Inherit;False;Specular;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;189;-597.5825,2325.11;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;197;-496.319,-1786.893;Inherit;False;RampGradient;-1;True;1;0;OBJECT;;False;1;OBJECT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;95;-928.2501,3339.111;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;209;-1731.342,1003.499;Inherit;False;LightAtt;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;82;-233.9601,2030.378;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;87;-1802.549,3102.232;Inherit;False;World;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.ColorNode;106;-567.5364,3555.574;Inherit;False;Property;_SpecularColor;Specular Color(高光顏色);24;1;[HDR];Create;False;0;0;0;True;0;False;1,1,1,0;0.2641509,0.08323245,0.08323245,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LightAttenuation;12;-2274.485,638.0951;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.GradientSampleNode;7;-182.8084,-100.8572;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldNormalVector;90;-1629.821,3489.149;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.Vector2Node;120;-869.228,2608.679;Inherit;False;Property;_SpecularDirLightSRPLightIntensity;Specular DirLight/SRPLight Intensity(高光受光影響程度 主光源/SRP光源));29;0;Create;False;0;0;0;True;0;False;1,1;1,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.TextureCoordinatesNode;172;-1390.361,442.9;Inherit;False;0;163;2;3;2;SAMPLER2D;;False;0;FLOAT2;2,2;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;45;-1685.329,-92.68679;Inherit;False;43;Normal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;8;-675.8527,-766.6453;Inherit;True;Property;_AlbedoMap;Albedo Map(主貼圖);0;1;[Header];Create;False;1;Albedo;0;0;True;0;False;-1;None;89625aefe9c5bf54dbb496c61535befc;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;42;-3375.967,-104.528;Inherit;True;Property;_NormalMap;Normal Map(法線貼圖);2;2;[Header];[Normal];Create;False;1;Normal;0;0;True;0;False;-1;None;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;207;-841.3389,2800.3;Inherit;False;203;SRPAdditionLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;181;2041.184,-1094.568;Inherit;False;Property;_ClipEdgeColor;ClipEdgeColor(死亡溶解顏色);37;1;[HDR];Create;False;0;0;0;False;0;False;7.464264,1.717473,0,0;7.464264,1.717473,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BreakToComponentsNode;67;-1437.409,1380.731;Inherit;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.LightColorNode;14;-2162.017,749.4439;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;1;-1418.154,74.92816;Inherit;False;True;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.Vector2Node;115;-2017.45,2638.116;Inherit;False;Property;_SpecularSRPLightSmoothstep;Specular SRPLight Smoothstep(高光燈光平滑閾值);28;0;Create;False;0;0;0;False;0;False;0,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SmoothstepOpNode;166;-793.6898,447.1251;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;26;800.676,-394.5883;Inherit;True;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;131;-1191.847,-105.1862;Inherit;False;Property;_DirLightNormalScale;DirLightNormalScale(N.L強度);8;0;Create;False;0;0;0;True;0;False;0.6;0.6;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceLightPos;88;-1859.821,3281.149;Inherit;False;0;3;FLOAT4;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.PosVertexDataNode;32;213.6345,1098.211;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;15;-1783.546,706.8102;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;83;-466.9252,1867.899;Inherit;False;Property;_RimColor;Rim Color(泛光顏色);20;1;[HDR];Create;False;0;0;0;False;0;False;0,0,0,0;0.0754717,0.03560935,0.02947668,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;78;-853.4456,2049.662;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;19;286.3075,-335.1839;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;169;-1368.353,597.6141;Inherit;False;Property;_RampCelluloidNoiseValue;Ramp Celluloid Noise Value(賽璐璐陰影強度);33;0;Create;False;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;10;-670.3519,-943.9666;Inherit;False;Property;_AlbedoEmissionColor;Albedo/Emission  Color(主貼圖/高光顏色);1;2;[HDR];[Header];Create;False;0;0;0;False;0;False;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;63;1360.861,-0.1071291;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;1;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;DepthOnly;0;3;DepthOnly;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;False;False;True;False;False;False;False;0;False;-1;False;False;False;False;False;False;False;False;False;True;1;False;-1;False;False;True;1;LightMode=DepthOnly;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;61;3906.57,-716.3071;Float;False;True;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;To7/Toon Shader_Gradution;2992e84f91cbeb14eab234972e07ea9d;True;Forward;0;1;Forward;8;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;2;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;7;0;False;True;1;1;False;-1;0;False;-1;1;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;True;True;True;True;0;False;-1;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;1;LightMode=UniversalForward;False;0;Hidden/InternalErrorShader;0;0;Standard;22;Surface;0;  Blend;0;Two Sided;1;Cast Shadows;1;  Use Shadow Threshold;0;Receive Shadows;1;GPU Instancing;1;LOD CrossFade;1;Built-in Fog;1;DOTS Instancing;0;Meta Pass;1;Extra Pre Pass;1;Tessellation;0;  Phong;0;  Strength;0.5,False,-1;  Type;0;  Tess;16,False,-1;  Min;10,False,-1;  Max;25,False,-1;  Edge Length;16,False,-1;  Max Displacement;25,False,-1;Vertex Position,InvertActionOnDeselection;1;0;5;True;True;True;True;True;False;;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;62;1360.861,-0.1071291;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;1;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;ShadowCaster;0;2;ShadowCaster;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;False;-1;True;3;False;-1;False;True;1;LightMode=ShadowCaster;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;60;4028.988,457.7011;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;ExtraPrePass;0;0;Outline;5;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;True;1;1;False;-1;0;False;-1;1;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;True;1;False;-1;False;True;True;True;True;True;0;False;-1;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;0;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;64;1360.861,-0.1071291;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;1;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;Meta;0;4;Meta;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;LightMode=Meta;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
WireConnection;228;0;188;0
WireConnection;228;1;226;0
WireConnection;175;0;188;0
WireConnection;231;0;188;0
WireConnection;231;1;230;0
WireConnection;233;0;188;0
WireConnection;233;1;232;0
WireConnection;215;0;214;0
WireConnection;229;1;175;0
WireConnection;229;0;228;0
WireConnection;229;2;231;0
WireConnection;229;3;233;0
WireConnection;217;0;215;0
WireConnection;177;0;229;0
WireConnection;177;1;176;0
WireConnection;218;0;216;0
WireConnection;219;0;218;0
WireConnection;219;1;217;0
WireConnection;179;0;177;0
WireConnection;220;0;219;0
WireConnection;180;0;178;0
WireConnection;180;1;179;0
WireConnection;43;0;42;0
WireConnection;25;0;11;0
WireConnection;25;1;133;0
WireConnection;71;0;18;0
WireConnection;71;1;69;0
WireConnection;9;0;19;0
WireConnection;9;1;123;0
WireConnection;190;0;192;0
WireConnection;190;1;13;0
WireConnection;190;2;196;0
WireConnection;92;0;89;0
WireConnection;92;1;90;0
WireConnection;161;0;159;0
WireConnection;161;1;160;0
WireConnection;2;0;45;0
WireConnection;119;0;207;0
WireConnection;119;1;120;2
WireConnection;18;0;17;0
WireConnection;18;1;16;2
WireConnection;99;0;107;0
WireConnection;99;1;97;0
WireConnection;99;2;106;0
WireConnection;99;3;116;0
WireConnection;81;0;80;0
WireConnection;81;1;85;1
WireConnection;81;2;189;0
WireConnection;72;0;11;0
WireConnection;72;1;204;0
WireConnection;203;0;65;0
WireConnection;165;0;190;0
WireConnection;165;1;166;0
WireConnection;221;0;220;0
WireConnection;221;1;180;0
WireConnection;16;0;15;0
WireConnection;110;0;109;0
WireConnection;110;1;111;0
WireConnection;123;1;7;0
WireConnection;123;0;122;0
WireConnection;107;0;117;0
WireConnection;107;1;119;0
WireConnection;75;0;73;0
WireConnection;75;1;74;0
WireConnection;3;0;2;0
WireConnection;3;1;1;0
WireConnection;163;1;172;0
WireConnection;13;0;5;0
WireConnection;13;1;71;0
WireConnection;11;0;10;0
WireConnection;11;1;8;0
WireConnection;201;0;82;0
WireConnection;89;0;87;0
WireConnection;89;1;88;1
WireConnection;31;0;32;0
WireConnection;39;0;26;0
WireConnection;39;1;36;0
WireConnection;17;0;16;0
WireConnection;17;1;16;1
WireConnection;185;0;187;0
WireConnection;185;1;181;0
WireConnection;65;2;112;0
WireConnection;124;0;14;1
WireConnection;124;1;14;2
WireConnection;124;2;125;0
WireConnection;138;0;139;0
WireConnection;138;1;136;0
WireConnection;138;2;140;0
WireConnection;76;0;75;0
WireConnection;76;1;77;0
WireConnection;158;0;12;0
WireConnection;158;1;159;0
WireConnection;158;2;161;0
WireConnection;187;0;180;0
WireConnection;187;1;182;0
WireConnection;187;2;186;0
WireConnection;68;0;67;0
WireConnection;68;1;67;1
WireConnection;5;0;3;0
WireConnection;5;1;131;0
WireConnection;104;0;84;0
WireConnection;104;1;105;0
WireConnection;192;0;191;0
WireConnection;192;1;193;0
WireConnection;114;0;110;0
WireConnection;114;1;115;1
WireConnection;114;2;115;2
WireConnection;186;0;182;0
WireConnection;186;1;184;0
WireConnection;227;0;226;0
WireConnection;108;0;206;0
WireConnection;108;1;111;0
WireConnection;84;0;26;0
WireConnection;84;1;202;0
WireConnection;135;0;104;0
WireConnection;135;1;138;0
WireConnection;69;0;68;0
WireConnection;69;1;67;2
WireConnection;80;0;78;0
WireConnection;97;0;95;0
WireConnection;97;1;98;0
WireConnection;183;0;185;0
WireConnection;183;1;135;0
WireConnection;109;0;108;0
WireConnection;29;0;28;0
WireConnection;29;1;30;0
WireConnection;29;2;8;4
WireConnection;29;3;37;0
WireConnection;122;1;165;0
WireConnection;117;0;212;0
WireConnection;117;1;120;1
WireConnection;133;0;24;0
WireConnection;133;1;134;0
WireConnection;222;0;183;0
WireConnection;222;1;224;0
WireConnection;222;2;225;0
WireConnection;24;0;173;0
WireConnection;93;0;92;0
WireConnection;93;1;94;0
WireConnection;37;0;31;4
WireConnection;37;1;35;0
WireConnection;101;0;99;0
WireConnection;189;0;85;1
WireConnection;189;1;85;2
WireConnection;197;0;6;0
WireConnection;95;0;93;0
WireConnection;95;1;96;1
WireConnection;95;2;96;2
WireConnection;209;0;15;0
WireConnection;82;0;83;0
WireConnection;82;1;81;0
WireConnection;82;2;212;0
WireConnection;7;0;198;0
WireConnection;7;1;165;0
WireConnection;90;0;91;0
WireConnection;42;5;44;0
WireConnection;67;0;205;0
WireConnection;166;0;163;1
WireConnection;166;1;169;0
WireConnection;166;2;170;0
WireConnection;26;0;25;0
WireConnection;26;1;9;0
WireConnection;26;2;72;0
WireConnection;15;0;158;0
WireConnection;15;1;124;0
WireConnection;78;0;76;0
WireConnection;19;0;11;0
WireConnection;19;1;210;0
WireConnection;61;2;222;0
WireConnection;61;4;221;0
WireConnection;60;0;39;0
WireConnection;60;2;221;0
WireConnection;60;3;29;0
ASEEND*/
//CHKSM=4E63DCDF13DF01D0F4C846D26CE3B5FE93F53D31