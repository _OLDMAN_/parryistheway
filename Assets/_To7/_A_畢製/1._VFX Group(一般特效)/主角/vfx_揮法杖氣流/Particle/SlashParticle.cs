using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlashParticle : MonoBehaviour
{
    public ParticleSystem Slash01;
    public ParticleSystem Slash02;
    public ParticleSystem Slash03;
    public ParticleSystem BlueFlash;

    public void SlashParticle01()
    {
        Slash01.Play();
        BlueFlash.Play();
        StartCoroutine("StopParticle");
    }
    IEnumerator StopParticle()
    {
        yield return new WaitForSeconds(.5f);
        BlueFlash.Stop(true , ParticleSystemStopBehavior.StopEmitting);
    }
    
    public void SlashParticle02()
    {
        Slash02.Play();
        BlueFlash.Play();
        StartCoroutine("StopParticle");
    }
    public void SlashParticle03()
    {
        Slash03.Play();
        BlueFlash.Play();
        StartCoroutine("StopParticle");
    }
}
