using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vfx_Combo1To4 : MonoBehaviour
{
    // Start is called before the first frame update
    public ParticleSystem vfx_ComboDashSting; 
    public ParticleSystem vfx_ComboDashSting_Flash;

    void Combo1ParticlePlay()
    {
        vfx_ComboDashSting.Play();
        vfx_ComboDashSting_Flash.Play();
    }
    void Combo2ParticlePlay()
    {
        vfx_ComboDashSting.Play();
        vfx_ComboDashSting_Flash.Play();
    }
    void Combo3ParticlePlay()
    {
        vfx_ComboDashSting.Play();
        vfx_ComboDashSting_Flash.Play();
    }
    void Combo4ParticlePlay()
    {
        vfx_ComboDashSting.Play();
        vfx_ComboDashSting_Flash.Play();
    }
    void DashStingPlay()
    {
        vfx_ComboDashSting.Play();
        vfx_ComboDashSting_Flash.Play();
    }
}
