using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vfx_shieldHit : MonoBehaviour
{
    // Start is called before the first frame update
    
    private float disappearMaxTime;
    private float disappearSpeed;

    private bool hit = false;  //避免Update每禎執行if裡的程式碼浪費效能
    void Start()
    {
        disappearMaxTime = 1;
    }

    // Update is called once per frame
    
    void Update()
    {
        if(disappearMaxTime > 0 && hit == true)
        {
            //Debug.Log("執行if");
            gameObject.GetComponentInChildren<ParticleSystem>().Stop(true , ParticleSystemStopBehavior.StopEmitting);
            disappearMaxTime -= Time.deltaTime * disappearSpeed;
            gameObject.GetComponent<ParticleSystemRenderer>().material.SetFloat("_ShieldHit",this.disappearMaxTime);
            if(disappearMaxTime < 0.001f)
            {
                disappearMaxTime = 0;
                hit = false;
            }
        }
    }
    /// <summary>
    /// 1.設置護盾被擊中時的消失快慢 護盾將會在方法執行後逐漸消失，子物件的粒子循環將會在方法執行後消失
    /// 執行效果為  1 -= Time.deltaTime * disappearSpeed;
    /// </summary>
    /// <param name="disappearSpeed">消失速度</param>
    public void hitShield(float disappearSpeed)
    {
        this.disappearSpeed = disappearSpeed;
        hit = true;
        
    }
    /// <summary>
    /// 粒子系統撥放瞬間記得調用這個方法重置粒子系統Material的透明度
    /// </summary>
    public void playShield()
    {
        gameObject.GetComponent<ParticleSystemRenderer>().material.SetFloat("_ShieldHit",1);
    }
}
