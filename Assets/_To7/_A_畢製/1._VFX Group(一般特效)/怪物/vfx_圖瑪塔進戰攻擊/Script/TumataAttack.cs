using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TumataAttack : MonoBehaviour
{
    // Start is called before the first frame update
    public ParticleSystem Tumata_Attack01_Particle;
    public ParticleSystem Tumata_Attack02_Particle;
    public ParticleSystem Tumata_B_Attack01_Particle;
    public ParticleSystem Tumata_BangAttack02_Particle;
    public ParticleSystem Tumata_MagicAttackSkill_Particle;

    void Update()
    {
        
    }

    public void Tumata_Attack01()
    {
        Tumata_Attack01_Particle.Play();
    }

    public void Tumata_Attack02()
    {
        Tumata_Attack02_Particle.Play();
    }

    public void Tumata_B_Attack01()
    {
        Tumata_B_Attack01_Particle.Play();
    }

    public void Tumata_BangAttack02()
    {
        Tumata_BangAttack02_Particle.Play();
    }

    public void Tumata_MagicAttackSkill()
    {
        Tumata_MagicAttackSkill_Particle.Play();
    }

}
