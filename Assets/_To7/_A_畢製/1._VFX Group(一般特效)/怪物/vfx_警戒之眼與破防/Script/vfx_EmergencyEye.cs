using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;


public enum emergencyColor{ Red , Yellow , Break} 
public class vfx_EmergencyEye : MonoBehaviour
{

    // Start is called before the first frame update
    [Tooltip("警戒粒子系統拖過來")]


    [Header("下面丟 vfx_警戒之眼")]
    public GameObject emergencyParticleSystem;

    [Header("下面丟 vfx_破防之眼")]
    public GameObject breakParticleSystem; 

    [Header("下面丟 vfx_偵查之眼")]
    public GameObject detectParticleSystem;


    ParticleSystem thisParticle;
    ParticleSystem breakParticle;
    ParticleSystem detectParticle;
    Color brown = new Color (0.75f , 0.2f , 0.05f , 1f);

    public Material m;

    bool DetectEnable = false;
    bool DestorySelf = false;
    float Duration;

    private void OnEnable()
    {
        m = GetComponent<MeshRenderer>().material;
    }
    private void Start()
    {
        m = GetComponent<MeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    /// <summary>
    /// 調用怪物頭上眼睛UI
    /// </summary>
    /// <param name="egColor">輸入怪物狀態 應為"Red"或"Yellow"或"Break"</param>
    /// <param name="detectDuration">震動的持續時間</param>
    /// <param name="destoryself">是否要在紅黃眼的放大播放完後銷毀自己</param>
    public void emergency(emergencyColor egColor , float detectDuration , bool destoryself = false)
    {
        //返回持續時間
        this.Duration = detectDuration;
        this.DestorySelf = destoryself;

        //綠眼(已經移除)
        //if(egColor == emergencyColor.Green)
        //    emergencySet(Color.green * 2f , 0 , 0);

        //黃眼    
        if(egColor == emergencyColor.Yellow)
        {
            emergencySet(Color.yellow * 3f , 0 , 1);
            StartCoroutine(ParticlePlay(emergencyColor.Yellow));
            Detect(5);
        }

        //紅眼
        else if(egColor== emergencyColor.Red)
        {
            emergencySet(Color.red * 30f, 1 , 2);
            StartCoroutine(ParticlePlay(emergencyColor.Red));
            Detect(5);
        }

        //破防
        else if(egColor == emergencyColor.Break)
        {
            m.SetFloat("_BreakEyeAni" ,0.7f);
            m.SetFloat("_BreakZoom" ,0.6f);
            emergencySet(brown * 25f , 1 , 3);
            m.DOFloat(0.001f , "_BreakEyeAni" , .3f);
            m.DOFloat(1f , "_BreakZoom" , .7f);
            breakParticleSystem.SetActive(true);
            breakParticle = breakParticleSystem.GetComponent<ParticleSystem>();
            breakParticle.Play();
        }
    }

    //傳值給Shader
    public void emergencySet(Color colorSet , float noiseSet , float flipBookTileSet)
    {
        m.SetColor("_Color" , colorSet);
        m.SetFloat("_NoiseSwitch" , noiseSet);
        m.SetFloat("_FlipBookTile" , flipBookTileSet);
    }

    //震動設置
    public void Detect(float detect)
    {   
        DetectEnable = true;
        if(DetectEnable == true)
        {
            m.DOFloat(detect , "_FlowSpeed" , Duration);
            StartCoroutine(detectToZero()); 
        }

    }

    //關閉震動
    IEnumerator detectToZero()
    {
        yield return new WaitForSecondsRealtime(Duration);
        m.SetFloat("_FlowSpeed" , 0);
        DetectEnable = false; 
    }

    //震動結束後播放放大粒子系統動畫
    IEnumerator ParticlePlay(emergencyColor egColor)
    {
        //print("Duration為:" + Duration);
        yield return new WaitForSeconds(Duration);
        if(egColor == emergencyColor.Yellow)
        {
            detectParticleSystem.SetActive(true);
            detectParticle = detectParticleSystem.GetComponent<ParticleSystem>();
            detectParticle.Play();
        }
        if(egColor == emergencyColor.Red)
        {
            emergencyParticleSystem.SetActive(true);
            thisParticle = emergencyParticleSystem.GetComponent<ParticleSystem>();
            thisParticle.Play(); 
        }
        StartCoroutine(ResetAllParticle());
        //透明度降為0 並在粒子系統播放完後銷毀自己
        if (DestorySelf == true)
        {
            m.DOFloat(0 , "_Alpha" , 0.01f);
            yield return new WaitForSecondsRealtime(1f);
            Destroy(gameObject);
        }
    }

    IEnumerator ResetAllParticle()
    {
        yield return new WaitForSeconds(0.5f);
        emergencyParticleSystem.SetActive(false);
        breakParticleSystem.SetActive(false);
        detectParticleSystem.SetActive(false);
    }

}
