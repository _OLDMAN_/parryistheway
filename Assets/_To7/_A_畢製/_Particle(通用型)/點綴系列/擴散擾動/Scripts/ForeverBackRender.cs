using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class ForeverBackRender : MonoBehaviour
{
    public float Offset;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.GetChild(0).GetComponent<Transform>().position = (Vector3.Normalize(transform.position - Camera.main.transform.position)) * Offset + transform.position;
    }
}
