using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class MatVector : MonoBehaviour
{
    
    // Start is called before the first frame update
    public Material toParticleSystem;

    public Transform tragetPosition;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //print(tragetPosition.position);
        toParticleSystem.SetVector("_Position" , tragetPosition.position);
    }
}
