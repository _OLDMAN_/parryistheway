// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "TAASE/Toon Shader_Building_GardientAlpha(雲上岩橋專用)"
{
	Properties
	{
		[HideInInspector] _AlphaCutoff("Alpha Cutoff ", Range(0, 1)) = 0.5
		[ASEBegin][Header(Albedo)]_AlbedoMap1("Albedo Map(主貼圖)", 2D) = "white" {}
		[HDR]_AlbedoEmissionColor1("Albedo/Emission  Color(主貼圖/高光顏色)", Color) = (0.3301887,0.3301887,0.3301887,0)
		[Header(Normal)][Normal]_NormalMap1("Normal Map(法線貼圖)", 2D) = "bump" {}
		_NormalStrength1("Normal Strength(法線強度)", Range( 0 , 5)) = 0
		[Header(Emission)]_EmissionTexture("Emission Texture", 2D) = "white" {}
		[HDR]_EmissionColor("Emission Color", Color) = (0,0,0,1)
		_EmissionStrength("Emission Strength", Float) = 1
		[Header(Light)]_DirLightIntensity1("DirLight Intensity(日光強度)", Float) = 1
		_DirLightNormalScale1("DirLightNormalScale(N.L強度)", Float) = 0.6
		_DiffuseLightStrength1("DiffuseLight Strength(環境光強度(疊加)", Float) = 0
		_IndirectDiffuseLightNormal1("Indirect Diffuse Light Normal(漫射光法線貼圖)", 2D) = "bump" {}
		[Header(Shadow Type)]_ShadowRemapMap("Shadow Remap Map", 2D) = "white" {}
		[KeywordEnum(UseShaderGardientColor,UseShadowRemapMap)] _ShadowRemapType1("Shadow Remap Type(陰影重映射方式)(左為背光右為迎光)", Float) = 1
		_RampAddincludePower("Ramp Add(include Power)", Float) = 0
		_RampAddwithoutPower("Ramp Add(without Power)", Float) = 0
		_RampPower("Ramp Power", Range( 0 , 3)) = 1
		[Header(Rim)]_RimOffset1("Rim Offset(泛光量值)", Float) = 0
		[HDR]_RimColor1("Rim Color(泛光顏色)", Color) = (0,0,0,0)
		_RimSmoothStepMinMax1("Rim SmoothStepValue and Smoothness(泛光平滑閾值)", Vector) = (0,1,0,0)
		[Header(Specular)]_SpecularMap1("Specular Map(高光貼圖)", 2D) = "white" {}
		_SpecularPow1("Specular Pow(高光量值)", Float) = 4
		[HDR]_SpecularColor1("Specular Color(高光顏色)", Color) = (1,1,1,0)
		_SpecularSmoothStepMinMax1("Specular SmoothStep Value & Smoothness(高光值與平滑度)", Vector) = (0,0.01,0,0)
		_SpecularIntensity1("Specular Intensity(高光強度)", Range( 0 , 1)) = 0
		[Header(SRP Addition Lighting Setting)]_SpecularSRPLightSampler1("Specular SRPLight Sampler(高光燈光採樣次數)", Float) = 5
		_SpecularSRPLightSmoothstep1("Specular SRPLight Smoothstep(高光燈光平滑閾值)", Vector) = (0,1,0,0)
		_SpecularDirLightSRPLightIntensity1("Specular DirLight/SRPLight Intensity(高光受光影響程度 主光源/SRP光源))", Vector) = (1,1,0,0)
		[Header(Light Attenuation)]_LightAttValue1("Light Att Value(光衰減程度)", Float) = -0.3
		_LightAttSoft1("Light Att Soft(光衰減平滑度)", Float) = 0.46
		[Header(Celluloid Edge Noise Texture)]_RampCelluloidNoiseTexture1("Ramp Celluloid Noise Texture(賽璐璐陰影邊緣噪波貼圖)", 2D) = "white" {}
		_RampCelluloidNoiseValue1("Ramp Celluloid Noise Value(賽璐璐陰影強度)", Float) = 0
		_RampCelluloidNoiseSoftness1("Ramp Celluloid Noise Softness(賽璐璐陰影平滑度)", Float) = 1
		[Header(Alpha Setting)]_Alpha("Alpha", Range( 0 , 1)) = 1
		[Toggle(_SHADOWUSEDITHER1_ON)] _ShadowUseDither1("Shadow Use Dither(陰影隨溶解消失)", Float) = 0
		[Header(PlayerPosition Alpha)]_PlayerPositionMaskAlpha("PlayerPositionMask Alpha", Range( 0 , 1)) = 1
		[Toggle(_PLAYERMASKOPEN_ON)] _PlayerMaskOpen("PlayerMask Open", Float) = 0
		_PlayerMaskPosition1("PlayerMaskPosition", Vector) = (0,0,0,0)
		_Float5("Float 5", Float) = 1
		_BlackClipValue("Black Clip Value", Float) = 0
		_BlackClipSmoothness("Black Clip Smoothness", Float) = 1
		[Header(Camera Dither Setting)]_CamPosDither("CamPos Dither", Float) = -6
		_NoiseAlpha("Noise Alpha", Float) = 0
		[Toggle(_USECAMERADEPTHCLIP_ON)] _UseCameraDepthClip("Use Camera Depth Clip ?", Float) = 0
		[Toggle(_ONLYUSEXZAXIS_ON)] _OnlyUseXZAxis("Only Use XZ Axis", Float) = 1
		[Header(SSAO)]_AOMix("AO Mix", Range( 0 , 1)) = 0
		[Toggle(_USESSAO_ON)] _UseSSAO("Use SSAO", Float) = 0
		_AOSmoothness("AO Smoothness", Range( 0 , 1)) = 1
		_AOThresold("AO Thresold", Range( 0 , 1)) = 0
		[HDR]_AOColour("AO Colour", Color) = (0,0,0,0)
		[HDR]_BassColor("BassColor", Color) = (1,1,1,1)
		[HDR]_Color0("Color 0", Color) = (1,1,1,0)
		[Header(Height Fog Setting)]_FogHeightEnd("Fog Height End", Float) = 4
		_FogHeightStart("Fog Height Start", Float) = 0
		_HeightFogWorldPosYClipAdd("HeightFog WorldPos Y Clip Add", Float) = 0
		_NoiseFogClip("NoiseFog Clip", Float) = 0.76
		_NoiseFogSoft("NoiseFog Soft", Float) = 0.4
		[HDR]_HeightFogColor("HeightFogColor", Color) = (0,0,0,0)
		[Toggle(_USEHEIGHTFOG_ON)] _UseHeightFog("Use HeightFog ?", Float) = 0
		[Header(Fog Noise Setting)][Enum(Off,0,On,1)]_UseFogNoise("Use Fog Noise", Float) = 0
		_NoiseSpeedWorldPos("Noise Speed(World Pos)", Vector) = (0,0,0,0)
		_NoiseScale("Noise Scale", Float) = 1
		_NoiseDensityUseAdd("Noise Density(Use Add)", Float) = 0
		_NoiseStrengthUsePower("Noise Strength(Use Power)", Float) = 0
		_FogNoiseEdgeValue("Fog Noise Edge Value", Float) = 0
		_FogNoiseEdgeSmoothness("Fog Noise Edge Smoothness", Float) = 0
		_WorldNoiseYMaskClip1("世界霧Y軸噪聲衰減極值", Float) = -0.59
		_WorldNoiseYMaskStrength1("世界霧Y軸噪聲衰減強度", Range( 0 , 1000)) = -0.59
		[Header(Lerp0(In Cloud) Setting)]_Lerp0XYZOffset("Lerp0 XYZ Offset", Vector) = (0,0,0,0)
		_Lerp0Scale("Lerp0 Scale", Float) = 5
		_Lerp0YScale("Lerp0 Y Scale", Float) = 1
		_Lerp0ZScale("Lerp0 Z Scale", Float) = 1
		[Header(Lerp1(CloudTop Setting))]_Lerp1Scale("Lerp1 Scale", Float) = 1
		[Header(Vertex Offset Animation Control)]_BridgeFallDownControl("_BridgeFallDownControl", Float) = 0
		_NegateFallDown("_NegateFallDown", Range( 0 , 1)) = 0
		_Power("Power", Float) = 1
		_DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft("Distance Value/Distance Soft/Negate DistanceValue/Negate Distance Soft", Vector) = (0,0,0,0)
		[KeywordEnum(X,Z)] _Axis("Axis", Float) = 0
		_HeightControlZOffset("HeightControl(Z Offset)", Float) = 0.5
		[Toggle(_OFFSETEFFECTOPEN_ON)] _OffsetEffectOpen("Offset Effect Open", Float) = 0
		[Header(Rotate Setting)]_RotatePower("Rotate Power", Float) = 1
		_RotateLerpPower("Rotate Lerp Power", Float) = 1
		_rotateRange("rotate Range", Float) = 0
		_RotateAngleStrength("RotateAngleStrength", Float) = 1
		[Header(Gardient Lerp01 Color Setting)]_GardientLerp01ColorPower("Gardient Lerp01 Color Power", Float) = 0
		_GardientLerp01ColorClip("Gardient Lerp01 Color Clip", Float) = 0
		_GardientLerp01ColorSoft("Gardient Lerp01 Color Soft", Float) = 1
		_GardientLerp01ColorAdd("Gardient Lerp01 Color Add", Float) = 0
		[ASEEnd][HDR]_GardientLerp01EmissionColor("Gardient Lerp01 Emission Color", Color) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

		//_TessPhongStrength( "Tess Phong Strength", Range( 0, 1 ) ) = 0.5
		//_TessValue( "Tess Max Tessellation", Range( 1, 32 ) ) = 16
		//_TessMin( "Tess Min Distance", Float ) = 10
		//_TessMax( "Tess Max Distance", Float ) = 25
		//_TessEdgeLength ( "Tess Edge length", Range( 2, 50 ) ) = 16
		//_TessMaxDisp( "Tess Max Displacement", Float ) = 25
	}

	SubShader
	{
		LOD 0

		
		Tags { "RenderPipeline"="UniversalPipeline" "RenderType"="Transparent" "Queue"="AlphaTest" }
		
		Cull Off
		AlphaToMask Off
		HLSLINCLUDE
		#pragma target 5.0

		#ifndef ASE_TESS_FUNCS
		#define ASE_TESS_FUNCS
		float4 FixedTess( float tessValue )
		{
			return tessValue;
		}
		
		float CalcDistanceTessFactor (float4 vertex, float minDist, float maxDist, float tess, float4x4 o2w, float3 cameraPos )
		{
			float3 wpos = mul(o2w,vertex).xyz;
			float dist = distance (wpos, cameraPos);
			float f = clamp(1.0 - (dist - minDist) / (maxDist - minDist), 0.01, 1.0) * tess;
			return f;
		}

		float4 CalcTriEdgeTessFactors (float3 triVertexFactors)
		{
			float4 tess;
			tess.x = 0.5 * (triVertexFactors.y + triVertexFactors.z);
			tess.y = 0.5 * (triVertexFactors.x + triVertexFactors.z);
			tess.z = 0.5 * (triVertexFactors.x + triVertexFactors.y);
			tess.w = (triVertexFactors.x + triVertexFactors.y + triVertexFactors.z) / 3.0f;
			return tess;
		}

		float CalcEdgeTessFactor (float3 wpos0, float3 wpos1, float edgeLen, float3 cameraPos, float4 scParams )
		{
			float dist = distance (0.5 * (wpos0+wpos1), cameraPos);
			float len = distance(wpos0, wpos1);
			float f = max(len * scParams.y / (edgeLen * dist), 1.0);
			return f;
		}

		float DistanceFromPlane (float3 pos, float4 plane)
		{
			float d = dot (float4(pos,1.0f), plane);
			return d;
		}

		bool WorldViewFrustumCull (float3 wpos0, float3 wpos1, float3 wpos2, float cullEps, float4 planes[6] )
		{
			float4 planeTest;
			planeTest.x = (( DistanceFromPlane(wpos0, planes[0]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[0]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[0]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.y = (( DistanceFromPlane(wpos0, planes[1]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[1]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[1]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.z = (( DistanceFromPlane(wpos0, planes[2]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[2]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[2]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.w = (( DistanceFromPlane(wpos0, planes[3]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[3]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[3]) > -cullEps) ? 1.0f : 0.0f );
			return !all (planeTest);
		}

		float4 DistanceBasedTess( float4 v0, float4 v1, float4 v2, float tess, float minDist, float maxDist, float4x4 o2w, float3 cameraPos )
		{
			float3 f;
			f.x = CalcDistanceTessFactor (v0,minDist,maxDist,tess,o2w,cameraPos);
			f.y = CalcDistanceTessFactor (v1,minDist,maxDist,tess,o2w,cameraPos);
			f.z = CalcDistanceTessFactor (v2,minDist,maxDist,tess,o2w,cameraPos);

			return CalcTriEdgeTessFactors (f);
		}

		float4 EdgeLengthBasedTess( float4 v0, float4 v1, float4 v2, float edgeLength, float4x4 o2w, float3 cameraPos, float4 scParams )
		{
			float3 pos0 = mul(o2w,v0).xyz;
			float3 pos1 = mul(o2w,v1).xyz;
			float3 pos2 = mul(o2w,v2).xyz;
			float4 tess;
			tess.x = CalcEdgeTessFactor (pos1, pos2, edgeLength, cameraPos, scParams);
			tess.y = CalcEdgeTessFactor (pos2, pos0, edgeLength, cameraPos, scParams);
			tess.z = CalcEdgeTessFactor (pos0, pos1, edgeLength, cameraPos, scParams);
			tess.w = (tess.x + tess.y + tess.z) / 3.0f;
			return tess;
		}

		float4 EdgeLengthBasedTessCull( float4 v0, float4 v1, float4 v2, float edgeLength, float maxDisplacement, float4x4 o2w, float3 cameraPos, float4 scParams, float4 planes[6] )
		{
			float3 pos0 = mul(o2w,v0).xyz;
			float3 pos1 = mul(o2w,v1).xyz;
			float3 pos2 = mul(o2w,v2).xyz;
			float4 tess;

			if (WorldViewFrustumCull(pos0, pos1, pos2, maxDisplacement, planes))
			{
				tess = 0.0f;
			}
			else
			{
				tess.x = CalcEdgeTessFactor (pos1, pos2, edgeLength, cameraPos, scParams);
				tess.y = CalcEdgeTessFactor (pos2, pos0, edgeLength, cameraPos, scParams);
				tess.z = CalcEdgeTessFactor (pos0, pos1, edgeLength, cameraPos, scParams);
				tess.w = (tess.x + tess.y + tess.z) / 3.0f;
			}
			return tess;
		}
		#endif //ASE_TESS_FUNCS

		ENDHLSL

		
		Pass
		{
			
			Name "Forward"
			Tags { "LightMode"="UniversalForward" }
			
			Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
			ZWrite On
			ZTest LEqual
			Offset 0 , 0
			ColorMask RGBA
			

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#define _ALPHATEST_ON 1
			#define ASE_SRP_VERSION 100302

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/UnityInstancing.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

			#if ASE_SRP_VERSION <= 70108
			#define REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR
			#endif

			#include "Packages/com.unity.shadergraph/ShaderGraphLibrary/Functions.hlsl"
			#define ASE_NEEDS_VERT_POSITION
			#define ASE_NEEDS_VERT_NORMAL
			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#define ASE_NEEDS_FRAG_SHADOWCOORDS
			#define ASE_NEEDS_FRAG_COLOR
			#define ASE_NEEDS_FRAG_POSITION
			#pragma shader_feature_local _OFFSETEFFECTOPEN_ON
			#pragma shader_feature_local _AXIS_X _AXIS_Z
			#pragma shader_feature_local _SHADOWREMAPTYPE1_USESHADERGARDIENTCOLOR _SHADOWREMAPTYPE1_USESHADOWREMAPMAP
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma shader_feature_local _USESSAO_ON
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma shader_feature_local _USEHEIGHTFOG_ON
			#pragma shader_feature_local _PLAYERMASKOPEN_ON
			#pragma shader_feature_local _USECAMERADEPTHCLIP_ON
			#pragma shader_feature_local _ONLYUSEXZAXIS_ON
			#pragma shader_feature_local _SHADOWUSEDITHER1_ON
			#pragma __SCREEN_SPACE_OCCLUSION
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_color : COLOR;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_tangent : TANGENT;
				float4 texcoord1 : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				#ifdef ASE_FOG
				float fogFactor : TEXCOORD2;
				#endif
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_texcoord4 : TEXCOORD4;
				float4 ase_texcoord5 : TEXCOORD5;
				float4 ase_texcoord6 : TEXCOORD6;
				float4 lightmapUVOrVertexSH : TEXCOORD7;
				float4 ase_texcoord8 : TEXCOORD8;
				float4 ase_color : COLOR;
				float4 ase_texcoord9 : TEXCOORD9;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _BassColor;
			float4 _EmissionTexture_ST;
			float4 _NormalMap1_ST;
			float4 _EmissionColor;
			float4 _Color0;
			float4 _AlbedoEmissionColor1;
			float4 _AlbedoMap1_ST;
			float4 _SpecularMap1_ST;
			float4 _IndirectDiffuseLightNormal1_ST;
			float4 _DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft;
			float4 _RimColor1;
			float4 _HeightFogColor;
			float4 _AOColour;
			float4 _GardientLerp01EmissionColor;
			float4 _SpecularColor1;
			float3 _Lerp0XYZOffset;
			float3 _PlayerMaskPosition1;
			float3 _NoiseSpeedWorldPos;
			float2 _SpecularDirLightSRPLightIntensity1;
			float2 _SpecularSmoothStepMinMax1;
			float2 _RimSmoothStepMinMax1;
			float2 _SpecularSRPLightSmoothstep1;
			float _WorldNoiseYMaskClip1;
			float _NoiseScale;
			float _FogNoiseEdgeSmoothness;
			float _WorldNoiseYMaskStrength1;
			float _NoiseDensityUseAdd;
			float _SpecularPow1;
			float _UseFogNoise;
			float _FogNoiseEdgeValue;
			float _SpecularIntensity1;
			float _EmissionStrength;
			float _GardientLerp01ColorClip;
			float _GardientLerp01ColorSoft;
			float _GardientLerp01ColorPower;
			float _GardientLerp01ColorAdd;
			float _Alpha;
			float _CamPosDither;
			float _NoiseAlpha;
			float _PlayerPositionMaskAlpha;
			float _BlackClipValue;
			float _BlackClipSmoothness;
			float _NoiseStrengthUsePower;
			float _NoiseFogSoft;
			float _RotateAngleStrength;
			float _HeightFogWorldPosYClipAdd;
			float _HeightControlZOffset;
			float _BridgeFallDownControl;
			float _NegateFallDown;
			float _RotatePower;
			float _rotateRange;
			float _Power;
			float _RotateLerpPower;
			float _Lerp0Scale;
			float _Lerp0YScale;
			float _Lerp0ZScale;
			float _Lerp1Scale;
			float _RampAddincludePower;
			float _RampPower;
			float _NormalStrength1;
			float _DirLightNormalScale1;
			float _LightAttValue1;
			float _LightAttSoft1;
			float _DirLightIntensity1;
			float _RampAddwithoutPower;
			float _RampCelluloidNoiseValue1;
			float _RampCelluloidNoiseSoftness1;
			float _SpecularSRPLightSampler1;
			float _AOThresold;
			float _AOSmoothness;
			float _AOMix;
			float _DiffuseLightStrength1;
			float _RimOffset1;
			float _FogHeightEnd;
			float _FogHeightStart;
			float _NoiseFogClip;
			float _Float5;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			sampler2D _ShadowRemapMap;
			sampler2D _NormalMap1;
			sampler2D _RampCelluloidNoiseTexture1;
			sampler2D _AlbedoMap1;
			sampler2D _IndirectDiffuseLightNormal1;
			sampler2D _SpecularMap1;
			sampler2D _EmissionTexture;


			float3 RotateAroundAxis( float3 center, float3 original, float3 u, float angle )
			{
				original -= center;
				float C = cos( angle );
				float S = sin( angle );
				float t = 1 - C;
				float m00 = t * u.x * u.x + C;
				float m01 = t * u.x * u.y - S * u.z;
				float m02 = t * u.x * u.z + S * u.y;
				float m10 = t * u.x * u.y + S * u.z;
				float m11 = t * u.y * u.y + C;
				float m12 = t * u.y * u.z - S * u.x;
				float m20 = t * u.x * u.z - S * u.y;
				float m21 = t * u.y * u.z + S * u.x;
				float m22 = t * u.z * u.z + C;
				float3x3 finalMatrix = float3x3( m00, m01, m02, m10, m11, m12, m20, m21, m22 );
				return mul( finalMatrix, original ) + center;
			}
			
			
			float4 SampleGradient( Gradient gradient, float time )
			{
				float3 color = gradient.colors[0].rgb;
				UNITY_UNROLL
				for (int c = 1; c < 8; c++)
				{
				float colorPos = saturate((time - gradient.colors[c-1].w) / ( 0.00001 + (gradient.colors[c].w - gradient.colors[c-1].w)) * step(c, gradient.colorsLength-1));
				color = lerp(color, gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), gradient.type));
				}
				#ifndef UNITY_COLORSPACE_GAMMA
				color = SRGBToLinear(color);
				#endif
				float alpha = gradient.alphas[0].x;
				UNITY_UNROLL
				for (int a = 1; a < 8; a++)
				{
				float alphaPos = saturate((time - gradient.alphas[a-1].y) / ( 0.00001 + (gradient.alphas[a].y - gradient.alphas[a-1].y)) * step(a, gradient.alphasLength-1));
				alpha = lerp(alpha, gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), gradient.type));
				}
				return float4(color, alpha);
			}
			
			float3 AdditionalLightsLambert( float3 WorldPosition, float3 WorldNormal )
			{
				float3 Color = 0;
				#ifdef _ADDITIONAL_LIGHTS
				int numLights = GetAdditionalLightsCount();
				for(int i = 0; i<numLights;i++)
				{
					Light light = GetAdditionalLight(i, WorldPosition);
					half3 AttLightColor = light.color *(light.distanceAttenuation * light.shadowAttenuation);
					Color +=LightingLambert(AttLightColor, light.direction, WorldNormal);
					
				}
				#endif
				return Color;
			}
			
			float3 ASEIndirectDiffuse( float2 uvStaticLightmap, float3 normalWS )
			{
			#ifdef LIGHTMAP_ON
				return SampleLightmap( uvStaticLightmap, normalWS );
			#else
				return SampleSH(normalWS);
			#endif
			}
			
			half SampleAO17_g14( half2 In0 )
			{
				return SampleAmbientOcclusion(In0);
			}
			
			float3 mod3D289( float3 x ) { return x - floor( x / 289.0 ) * 289.0; }
			float4 mod3D289( float4 x ) { return x - floor( x / 289.0 ) * 289.0; }
			float4 permute( float4 x ) { return mod3D289( ( x * 34.0 + 1.0 ) * x ); }
			float4 taylorInvSqrt( float4 r ) { return 1.79284291400159 - r * 0.85373472095314; }
			float snoise( float3 v )
			{
				const float2 C = float2( 1.0 / 6.0, 1.0 / 3.0 );
				float3 i = floor( v + dot( v, C.yyy ) );
				float3 x0 = v - i + dot( i, C.xxx );
				float3 g = step( x0.yzx, x0.xyz );
				float3 l = 1.0 - g;
				float3 i1 = min( g.xyz, l.zxy );
				float3 i2 = max( g.xyz, l.zxy );
				float3 x1 = x0 - i1 + C.xxx;
				float3 x2 = x0 - i2 + C.yyy;
				float3 x3 = x0 - 0.5;
				i = mod3D289( i);
				float4 p = permute( permute( permute( i.z + float4( 0.0, i1.z, i2.z, 1.0 ) ) + i.y + float4( 0.0, i1.y, i2.y, 1.0 ) ) + i.x + float4( 0.0, i1.x, i2.x, 1.0 ) );
				float4 j = p - 49.0 * floor( p / 49.0 );  // mod(p,7*7)
				float4 x_ = floor( j / 7.0 );
				float4 y_ = floor( j - 7.0 * x_ );  // mod(j,N)
				float4 x = ( x_ * 2.0 + 0.5 ) / 7.0 - 1.0;
				float4 y = ( y_ * 2.0 + 0.5 ) / 7.0 - 1.0;
				float4 h = 1.0 - abs( x ) - abs( y );
				float4 b0 = float4( x.xy, y.xy );
				float4 b1 = float4( x.zw, y.zw );
				float4 s0 = floor( b0 ) * 2.0 + 1.0;
				float4 s1 = floor( b1 ) * 2.0 + 1.0;
				float4 sh = -step( h, 0.0 );
				float4 a0 = b0.xzyw + s0.xzyw * sh.xxyy;
				float4 a1 = b1.xzyw + s1.xzyw * sh.zzww;
				float3 g0 = float3( a0.xy, h.x );
				float3 g1 = float3( a0.zw, h.y );
				float3 g2 = float3( a1.xy, h.z );
				float3 g3 = float3( a1.zw, h.w );
				float4 norm = taylorInvSqrt( float4( dot( g0, g0 ), dot( g1, g1 ), dot( g2, g2 ), dot( g3, g3 ) ) );
				g0 *= norm.x;
				g1 *= norm.y;
				g2 *= norm.z;
				g3 *= norm.w;
				float4 m = max( 0.6 - float4( dot( x0, x0 ), dot( x1, x1 ), dot( x2, x2 ), dot( x3, x3 ) ), 0.0 );
				m = m* m;
				m = m* m;
				float4 px = float4( dot( x0, g0 ), dot( x1, g1 ), dot( x2, g2 ), dot( x3, g3 ) );
				return 42.0 * dot( m, px);
			}
			
			inline float Dither4x4Bayer( int x, int y )
			{
				const float dither[ 16 ] = {
			 1,  9,  3, 11,
			13,  5, 15,  7,
			 4, 12,  2, 10,
			16,  8, 14,  6 };
				int r = y * 4 + x;
				return dither[r] / 16; // same # of instructions as pre-dividing due to compiler magic
			}
			
			float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }
			float snoise( float2 v )
			{
				const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
				float2 i = floor( v + dot( v, C.yy ) );
				float2 x0 = v - i + dot( i, C.xx );
				float2 i1;
				i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
				float4 x12 = x0.xyxy + C.xxzz;
				x12.xy -= i1;
				i = mod2D289( i );
				float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
				float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
				m = m * m;
				m = m * m;
				float3 x = 2.0 * frac( p * C.www ) - 1.0;
				float3 h = abs( x ) - 0.5;
				float3 ox = floor( x + 0.5 );
				float3 a0 = x - ox;
				m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
				float3 g;
				g.x = a0.x * x0.x + h.x * x0.y;
				g.yz = a0.yz * x12.xz + h.yz * x12.yw;
				return 130.0 * dot( m, g );
			}
			
			
			VertexOutput VertexFunction ( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float3 appendResult152 = (float3(( ( 1.0 - v.ase_color.r ) - 0.5 ) , ( v.ase_color.g - 0.5 ) , ( v.ase_color.b - 0.5 )));
				float3 appendResult64 = (float3(( ( 1.0 - v.ase_color.r ) - 0.5 ) , ( v.ase_color.g - 0.5 ) , ( v.ase_color.b - _HeightControlZOffset )));
				float lerpResult300 = lerp( 0.0 , 25.0 , _NegateFallDown);
				float temp_output_299_0 = ( _BridgeFallDownControl - lerpResult300 );
				float3 appendResult276 = (float3(temp_output_299_0 , temp_output_299_0 , temp_output_299_0));
				float3 worldToObj273 = mul( GetWorldToObjectMatrix(), float4( appendResult276, 1 ) ).xyz;
				float3 rotatedValue269 = RotateAroundAxis( appendResult152, v.vertex.xyz, normalize( appendResult152 ), ( ( _RotateAngleStrength * saturate( ( pow( distance( appendResult64 , worldToObj273 ) , _RotatePower ) - _rotateRange ) ) ) * PI ) );
				float2 appendResult292 = (float2(_DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.x , _DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.y));
				float2 appendResult293 = (float2(_DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.z , _DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.w));
				float2 lerpResult288 = lerp( appendResult292 , appendResult293 , _NegateFallDown);
				float2 DIstanceValueandSoft294 = lerpResult288;
				float2 break296 = DIstanceValueandSoft294;
				float3 objToWorld180 = mul( GetObjectToWorldMatrix(), float4( appendResult64, 1 ) ).xyz;
				#if defined(_AXIS_X)
				float staticSwitch588 = objToWorld180.x;
				#elif defined(_AXIS_Z)
				float staticSwitch588 = objToWorld180.z;
				#else
				float staticSwitch588 = objToWorld180.x;
				#endif
				float temp_output_185_0 = ( staticSwitch588 + _BridgeFallDownControl );
				float ifLocalVar186 = 0;
				if( temp_output_185_0 > 0.0 )
				ifLocalVar186 = 1.0;
				else if( temp_output_185_0 == 0.0 )
				ifLocalVar186 = 0.0;
				else if( temp_output_185_0 < 0.0 )
				ifLocalVar186 = -1.0;
				float smoothstepResult139 = smoothstep( break296.x , ( break296.x + break296.y ) , ( ifLocalVar186 * pow( temp_output_185_0 , _Power ) ));
				float temp_output_159_0 = saturate( smoothstepResult139 );
				float3 lerpResult280 = lerp( ( rotatedValue269 - v.vertex.xyz ) , float3( 0,0,0 ) , pow( temp_output_159_0 , _RotateLerpPower ));
				float3 temp_output_138_0 = ( appendResult64 * _Lerp0Scale );
				float2 break256 = (temp_output_138_0).xz;
				float3 appendResult255 = (float3(break256.x , ( _Lerp0YScale * (temp_output_138_0).y ) , break256.y));
				float3 appendResult174 = (float3(_Lerp0XYZOffset.x , _Lerp0XYZOffset.y , -pow( _Lerp0XYZOffset.z , _Lerp0ZScale )));
				float3 lerpResult143 = lerp( ( appendResult255 + appendResult174 ) , ( appendResult152 * _Lerp1Scale ) , temp_output_159_0);
				#ifdef _OFFSETEFFECTOPEN_ON
				float3 staticSwitch137 = ( saturate( lerpResult280 ) + lerpResult143 );
				#else
				float3 staticSwitch137 = float3( 0,0,0 );
				#endif
				float3 VertexOffsetOutput301 = staticSwitch137;
				
				float3 ase_worldTangent = TransformObjectToWorldDir(v.ase_tangent.xyz);
				o.ase_texcoord4.xyz = ase_worldTangent;
				float3 ase_worldNormal = TransformObjectToWorldNormal(v.ase_normal);
				o.ase_texcoord5.xyz = ase_worldNormal;
				float ase_vertexTangentSign = v.ase_tangent.w * unity_WorldTransformParams.w;
				float3 ase_worldBitangent = cross( ase_worldNormal, ase_worldTangent ) * ase_vertexTangentSign;
				o.ase_texcoord6.xyz = ase_worldBitangent;
				OUTPUT_LIGHTMAP_UV( v.texcoord1, unity_LightmapST, o.lightmapUVOrVertexSH.xy );
				OUTPUT_SH( ase_worldNormal, o.lightmapUVOrVertexSH.xyz );
				
				float4 ase_clipPos = TransformObjectToHClip((v.vertex).xyz);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord9 = screenPos;
				
				o.ase_texcoord3.xy = v.ase_texcoord.xy;
				o.ase_texcoord8 = v.vertex;
				o.ase_color = v.ase_color;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord3.zw = 0;
				o.ase_texcoord4.w = 0;
				o.ase_texcoord5.w = 0;
				o.ase_texcoord6.w = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = VertexOffsetOutput301;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif
				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );
				float4 positionCS = TransformWorldToHClip( positionWS );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				VertexPositionInputs vertexInput = (VertexPositionInputs)0;
				vertexInput.positionWS = positionWS;
				vertexInput.positionCS = positionCS;
				o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				#ifdef ASE_FOG
				o.fogFactor = ComputeFogFactor( positionCS.z );
				#endif
				o.clipPos = positionCS;
				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_color : COLOR;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_tangent : TANGENT;
				float4 texcoord1 : TEXCOORD1;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_color = v.ase_color;
				o.ase_texcoord = v.ase_texcoord;
				o.ase_tangent = v.ase_tangent;
				o.texcoord1 = v.texcoord1;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_color = patch[0].ase_color * bary.x + patch[1].ase_color * bary.y + patch[2].ase_color * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				o.ase_tangent = patch[0].ase_tangent * bary.x + patch[1].ase_tangent * bary.y + patch[2].ase_tangent * bary.z;
				o.texcoord1 = patch[0].texcoord1 * bary.x + patch[1].texcoord1 * bary.y + patch[2].texcoord1 * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag ( VertexOutput IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif
				float saferPower382 = max( _RampAddincludePower , 0.0001 );
				float2 uv_NormalMap1 = IN.ase_texcoord3.xy * _NormalMap1_ST.xy + _NormalMap1_ST.zw;
				float3 unpack326 = UnpackNormalScale( tex2D( _NormalMap1, uv_NormalMap1 ), _NormalStrength1 );
				unpack326.z = lerp( 1, unpack326.z, saturate(_NormalStrength1) );
				float3 Normal337 = unpack326;
				float3 ase_worldTangent = IN.ase_texcoord4.xyz;
				float3 ase_worldNormal = IN.ase_texcoord5.xyz;
				float3 ase_worldBitangent = IN.ase_texcoord6.xyz;
				float3 tanToWorld0 = float3( ase_worldTangent.x, ase_worldBitangent.x, ase_worldNormal.x );
				float3 tanToWorld1 = float3( ase_worldTangent.y, ase_worldBitangent.y, ase_worldNormal.y );
				float3 tanToWorld2 = float3( ase_worldTangent.z, ase_worldBitangent.z, ase_worldNormal.z );
				float3 tanNormal347 = Normal337;
				float3 worldNormal347 = float3(dot(tanToWorld0,tanNormal347), dot(tanToWorld1,tanNormal347), dot(tanToWorld2,tanNormal347));
				float dotResult359 = dot( worldNormal347 , SafeNormalize(_MainLightPosition.xyz) );
				float ase_lightAtten = 0;
				Light ase_lightAtten_mainLight = GetMainLight( ShadowCoords );
				ase_lightAtten = ase_lightAtten_mainLight.distanceAttenuation * ase_lightAtten_mainLight.shadowAttenuation;
				float smoothstepResult328 = smoothstep( _LightAttValue1 , ( _LightAttValue1 + _LightAttSoft1 ) , ase_lightAtten);
				float3 temp_output_332_0 = ( smoothstepResult328 * ( _MainLightColor.rgb * _MainLightColor.a * _DirLightIntensity1 ) );
				float3 break339 = temp_output_332_0;
				float2 texCoord370 = IN.ase_texcoord3.xy * float2( 2,2 ) + float2( 0,0 );
				float smoothstepResult406 = smoothstep( _RampCelluloidNoiseValue1 , _RampCelluloidNoiseSoftness1 , tex2D( _RampCelluloidNoiseTexture1, texCoord370 ).r);
				float temp_output_412_0 = ( ( pow( saferPower382 , _RampPower ) + ( (dotResult359*_DirLightNormalScale1 + 0.5) * ( max( max( break339.x , break339.y ) , break339.z ) + 0.0 ) ) + _RampAddwithoutPower ) * smoothstepResult406 );
				float2 temp_cast_0 = (temp_output_412_0).xx;
				Gradient gradient386 = NewGradient( 0, 8, 2, float4( 0.490566, 0.490566, 0.490566, 0 ), float4( 0.4901961, 0.4901961, 0.4901961, 0.2399939 ), float4( 0.6643071, 0.6643071, 0.6643071, 0.2599985 ), float4( 0.6627451, 0.6627451, 0.6627451, 0.4899977 ), float4( 0.8228067, 0.8228067, 0.8228067, 0.5100023 ), float4( 0.8235294, 0.8235294, 0.8235294, 0.7400015 ), float4( 1, 1, 1, 0.7600061 ), float4( 1, 1, 1, 1 ), float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
				float2 temp_cast_1 = (temp_output_412_0).xx;
				#if defined(_SHADOWREMAPTYPE1_USESHADERGARDIENTCOLOR)
				float4 staticSwitch447 = SampleGradient( gradient386, temp_output_412_0 );
				#elif defined(_SHADOWREMAPTYPE1_USESHADOWREMAPMAP)
				float4 staticSwitch447 = tex2D( _ShadowRemapMap, temp_cast_0 );
				#else
				float4 staticSwitch447 = tex2D( _ShadowRemapMap, temp_cast_0 );
				#endif
				float4 RampFinal503 = staticSwitch447;
				float3 temp_cast_2 = (_SpecularSRPLightSmoothstep1.x).xxx;
				float3 temp_cast_3 = (( _SpecularSRPLightSmoothstep1.x + _SpecularSRPLightSmoothstep1.y )).xxx;
				float3 WorldPosition5_g10 = WorldPosition;
				float3 tanNormal12_g10 = Normal337;
				float3 worldNormal12_g10 = float3(dot(tanToWorld0,tanNormal12_g10), dot(tanToWorld1,tanNormal12_g10), dot(tanToWorld2,tanNormal12_g10));
				float3 WorldNormal5_g10 = worldNormal12_g10;
				float3 localAdditionalLightsLambert5_g10 = AdditionalLightsLambert( WorldPosition5_g10 , WorldNormal5_g10 );
				float3 SRPAdditionLight385 = localAdditionalLightsLambert5_g10;
				float3 smoothstepResult441 = smoothstep( temp_cast_2 , temp_cast_3 , ( ( SRPAdditionLight385 * _SpecularSRPLightSampler1 ) / _SpecularSRPLightSampler1 ));
				float3 SRPLightFinal504 = smoothstepResult441;
				float3 bakedGI13_g14 = ASEIndirectDiffuse( IN.lightmapUVOrVertexSH.xy, ase_worldNormal);
				float4 unityObjectToClipPos2_g14 = TransformWorldToHClip(TransformObjectToWorld(IN.ase_texcoord8.xyz));
				float4 computeScreenPos3_g14 = ComputeScreenPos( unityObjectToClipPos2_g14 );
				half2 In017_g14 = ( computeScreenPos3_g14 / computeScreenPos3_g14.w ).xy;
				half localSampleAO17_g14 = SampleAO17_g14( In017_g14 );
				float SF_SSAO18_g14 = localSampleAO17_g14;
				float smoothstepResult8_g14 = smoothstep( _AOThresold , ( _AOThresold + _AOSmoothness ) , SF_SSAO18_g14);
				float4 lerpResult12_g14 = lerp( ( float4( bakedGI13_g14 , 0.0 ) * _AOColour ) , _Color0 , smoothstepResult8_g14);
				float4 lerpResult20_g14 = lerp( _BassColor , ( _BassColor * lerpResult12_g14 ) , _AOMix);
				#ifdef _USESSAO_ON
				float4 staticSwitch23_g14 = lerpResult20_g14;
				#else
				float4 staticSwitch23_g14 = _BassColor;
				#endif
				float2 uv_AlbedoMap1 = IN.ase_texcoord3.xy * _AlbedoMap1_ST.xy + _AlbedoMap1_ST.zw;
				float4 tex2DNode418 = tex2D( _AlbedoMap1, uv_AlbedoMap1 );
				float4 temp_output_438_0 = ( _AlbedoEmissionColor1 * tex2DNode418 );
				float2 uv_IndirectDiffuseLightNormal1 = IN.ase_texcoord3.xy * _IndirectDiffuseLightNormal1_ST.xy + _IndirectDiffuseLightNormal1_ST.zw;
				float3 tanNormal424 = UnpackNormalScale( tex2D( _IndirectDiffuseLightNormal1, uv_IndirectDiffuseLightNormal1 ), 1.0f );
				float3 bakedGI424 = ASEIndirectDiffuse( IN.lightmapUVOrVertexSH.xy, float3(dot(tanToWorld0,tanNormal424), dot(tanToWorld1,tanNormal424), dot(tanToWorld2,tanNormal424)));
				float3 LightAtt411 = temp_output_332_0;
				float4 temp_output_467_0 = ( ( temp_output_438_0 * float4( ( bakedGI424 + _DiffuseLightStrength1 ) , 0.0 ) ) + ( ( temp_output_438_0 * float4( LightAtt411 , 0.0 ) ) * staticSwitch447 ) + ( temp_output_438_0 * float4( SRPAdditionLight385 , 0.0 ) ) );
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - WorldPosition );
				ase_worldViewDir = SafeNormalize( ase_worldViewDir );
				float3 normalizedWorldNormal = normalize( ase_worldNormal );
				float dotResult371 = dot( ase_worldViewDir , normalizedWorldNormal );
				float smoothstepResult430 = smoothstep( _RimSmoothStepMinMax1.x , ( _RimSmoothStepMinMax1.x + _RimSmoothStepMinMax1.y ) , saturate( ( 1.0 - ( dotResult371 + _RimOffset1 ) ) ));
				float4 Rim457 = ( _RimColor1 * smoothstepResult430 * float4( LightAtt411 , 0.0 ) );
				float4 temp_output_475_0 = ( temp_output_467_0 + Rim457 );
				float temp_output_13_0_g13 = _FogHeightEnd;
				float3 temp_cast_14 = (WorldPosition.y).xxx;
				float clampResult11_g13 = clamp( ( ( temp_output_13_0_g13 - distance( temp_cast_14 , float3( 0,0,0 ) ) ) / ( temp_output_13_0_g13 - _FogHeightStart ) ) , 0.0 , 1.0 );
				float temp_output_316_0 = step( WorldPosition.y , _HeightFogWorldPosYClipAdd );
				float temp_output_399_0 = ( ( 1.0 - ( 1.0 - clampResult11_g13 ) ) * temp_output_316_0 );
				float smoothstepResult415 = smoothstep( _NoiseFogClip , ( _NoiseFogClip + _NoiseFogSoft ) , temp_output_399_0);
				float temp_output_434_0 = ( temp_output_399_0 + smoothstepResult415 );
				float simplePerlin3D340 = snoise( ( ( _TimeParameters.x * _NoiseSpeedWorldPos ) + WorldPosition )*_NoiseScale );
				simplePerlin3D340 = simplePerlin3D340*0.5 + 0.5;
				float smoothstepResult367 = smoothstep( _FogNoiseEdgeValue , ( _FogNoiseEdgeValue + _FogNoiseEdgeSmoothness ) , saturate( ( simplePerlin3D340 + _NoiseDensityUseAdd ) ));
				float HeightFogWorldPosYClipStep329 = temp_output_316_0;
				float saferPower393 = max( ( smoothstepResult367 * saturate( pow( ( ( HeightFogWorldPosYClipStep329 + _WorldNoiseYMaskClip1 + WorldPosition.y ) * HeightFogWorldPosYClipStep329 ) , _WorldNoiseYMaskStrength1 ) ) ) , 0.0001 );
				float FogNoise421 = pow( saferPower393 , _NoiseStrengthUsePower );
				float lerpResult461 = lerp( temp_output_434_0 , ( temp_output_434_0 * FogNoise421 ) , _UseFogNoise);
				float HeightFog473 = lerpResult461;
				float4 lerpResult487 = lerp( temp_output_475_0 , _HeightFogColor , HeightFog473);
				#ifdef _USEHEIGHTFOG_ON
				float4 staticSwitch493 = lerpResult487;
				#else
				float4 staticSwitch493 = temp_output_475_0;
				#endif
				float3 tanNormal416 = Normal337;
				float3 worldNormal416 = float3(dot(tanToWorld0,tanNormal416), dot(tanToWorld1,tanNormal416), dot(tanToWorld2,tanNormal416));
				float dotResult436 = dot( ( ase_worldViewDir + _MainLightPosition.xyz ) , worldNormal416 );
				float smoothstepResult460 = smoothstep( _SpecularSmoothStepMinMax1.x , _SpecularSmoothStepMinMax1.y , pow( dotResult436 , _SpecularPow1 ));
				float2 uv_SpecularMap1 = IN.ase_texcoord3.xy * _SpecularMap1_ST.xy + _SpecularMap1_ST.zw;
				float4 Specular483 = ( float4( ( ( LightAtt411 * _SpecularDirLightSRPLightIntensity1.x ) + ( smoothstepResult441 * _SpecularDirLightSRPLightIntensity1.y ) ) , 0.0 ) * ( smoothstepResult460 * _SpecularIntensity1 ) * _SpecularColor1 * tex2D( _SpecularMap1, uv_SpecularMap1 ) );
				float2 uv_EmissionTexture = IN.ase_texcoord3.xy * _EmissionTexture_ST.xy + _EmissionTexture_ST.zw;
				float4 temp_output_516_0 = ( ( staticSwitch493 + Specular483 ) + ( tex2D( _EmissionTexture, uv_EmissionTexture ) * _EmissionColor * _EmissionStrength ) );
				Gradient gradient599 = NewGradient( 0, 4, 2, float4( 1, 0, 0.1166511, 0.7930724 ), float4( 1, 0.7388967, 0, 0.966186 ), float4( 1, 0.7372882, 0, 0.9960021 ), float4( 0, 0, 0, 1 ), 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
				float2 appendResult292 = (float2(_DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.x , _DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.y));
				float2 appendResult293 = (float2(_DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.z , _DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.w));
				float2 lerpResult288 = lerp( appendResult292 , appendResult293 , _NegateFallDown);
				float2 DIstanceValueandSoft294 = lerpResult288;
				float2 break296 = DIstanceValueandSoft294;
				float3 appendResult64 = (float3(( ( 1.0 - IN.ase_color.r ) - 0.5 ) , ( IN.ase_color.g - 0.5 ) , ( IN.ase_color.b - _HeightControlZOffset )));
				float3 objToWorld180 = mul( GetObjectToWorldMatrix(), float4( appendResult64, 1 ) ).xyz;
				#if defined(_AXIS_X)
				float staticSwitch588 = objToWorld180.x;
				#elif defined(_AXIS_Z)
				float staticSwitch588 = objToWorld180.z;
				#else
				float staticSwitch588 = objToWorld180.x;
				#endif
				float temp_output_185_0 = ( staticSwitch588 + _BridgeFallDownControl );
				float ifLocalVar186 = 0;
				if( temp_output_185_0 > 0.0 )
				ifLocalVar186 = 1.0;
				else if( temp_output_185_0 == 0.0 )
				ifLocalVar186 = 0.0;
				else if( temp_output_185_0 < 0.0 )
				ifLocalVar186 = -1.0;
				float smoothstepResult139 = smoothstep( break296.x , ( break296.x + break296.y ) , ( ifLocalVar186 * pow( temp_output_185_0 , _Power ) ));
				float temp_output_159_0 = saturate( smoothstepResult139 );
				float smoothstepResult589 = smoothstep( _GardientLerp01ColorClip , ( _GardientLerp01ColorClip + _GardientLerp01ColorSoft ) , pow( temp_output_159_0 , _GardientLerp01ColorPower ));
				float4 GardientLerp01Color596 = ( SampleGradient( gradient599, ( smoothstepResult589 + _GardientLerp01ColorAdd ) ) * _GardientLerp01EmissionColor );
				float4 temp_output_597_0 = ( ( ( RampFinal503 * float4( SRPLightFinal504 , 0.0 ) ) + ( staticSwitch23_g14 * temp_output_516_0 ) ) + GardientLerp01Color596 );
				
				float4 screenPos = IN.ase_texcoord9;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float4 ditherCustomScreenPos497 = ase_screenPosNorm;
				float2 clipScreen497 = ditherCustomScreenPos497.xy * _ScreenParams.xy;
				float dither497 = Dither4x4Bayer( fmod(clipScreen497.x, 4), fmod(clipScreen497.y, 4) );
				dither497 = step( dither497, _Alpha );
				float simplePerlin2D482 = snoise( ase_screenPosNorm.xy*5000.0 );
				simplePerlin2D482 = simplePerlin2D482*0.5 + 0.5;
				#ifdef _ONLYUSEXZAXIS_ON
				float staticSwitch470 = distance( (WorldPosition).xz , (_WorldSpaceCameraPos).xz );
				#else
				float staticSwitch470 = distance( WorldPosition , _WorldSpaceCameraPos );
				#endif
				float saferPower479 = max( staticSwitch470 , 0.0001 );
				#ifdef _USECAMERADEPTHCLIP_ON
				float staticSwitch509 = ( dither497 * saturate( ( simplePerlin2D482 + pow( saferPower479 , _CamPosDither ) + _NoiseAlpha ) ) );
				#else
				float staticSwitch509 = dither497;
				#endif
				float3 objToWorld486 = mul( GetObjectToWorldMatrix(), float4( IN.ase_texcoord8.xyz, 1 ) ).xyz;
				float temp_output_490_0 = distance( _PlayerMaskPosition1 , objToWorld486 );
				float temp_output_501_0 = pow( temp_output_490_0 , _Float5 );
				float smoothstepResult510 = smoothstep( _BlackClipValue , ( _BlackClipValue + _BlackClipSmoothness ) , temp_output_501_0);
				float lerpResult521 = lerp( staticSwitch509 , ( simplePerlin2D482 * _PlayerPositionMaskAlpha ) , smoothstepResult510);
				#ifdef _PLAYERMASKOPEN_ON
				float staticSwitch559 = lerpResult521;
				#else
				float staticSwitch559 = staticSwitch509;
				#endif
				
				#ifdef _SHADOWUSEDITHER1_ON
				float staticSwitch545 = 0.001;
				#else
				float staticSwitch545 = 0.0;
				#endif
				
				float3 BakedAlbedo = temp_output_597_0.rgb;
				float3 BakedEmission = 0;
				float3 Color = temp_output_597_0.rgb;
				float Alpha = staticSwitch559;
				float AlphaClipThreshold = staticSwitch545;
				float AlphaClipThresholdShadow = 0.5;

				#ifdef _ALPHATEST_ON
					clip( Alpha - AlphaClipThreshold );
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif

				#ifdef ASE_FOG
					Color = MixFog( Color, IN.fogFactor );
				#endif

				return half4( Color, Alpha );
			}

			ENDHLSL
		}

		
		Pass
		{
			
			Name "ShadowCaster"
			Tags { "LightMode"="ShadowCaster" }

			ZWrite On
			ZTest LEqual
			AlphaToMask Off

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#define _ALPHATEST_ON 1
			#define ASE_SRP_VERSION 100302

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

			#define ASE_NEEDS_VERT_POSITION
			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#pragma shader_feature_local _OFFSETEFFECTOPEN_ON
			#pragma shader_feature_local _AXIS_X _AXIS_Z
			#pragma shader_feature_local _PLAYERMASKOPEN_ON
			#pragma shader_feature_local _USECAMERADEPTHCLIP_ON
			#pragma shader_feature_local _ONLYUSEXZAXIS_ON
			#pragma shader_feature_local _SHADOWUSEDITHER1_ON
			#pragma __SCREEN_SPACE_OCCLUSION
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				float4 ase_texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _BassColor;
			float4 _EmissionTexture_ST;
			float4 _NormalMap1_ST;
			float4 _EmissionColor;
			float4 _Color0;
			float4 _AlbedoEmissionColor1;
			float4 _AlbedoMap1_ST;
			float4 _SpecularMap1_ST;
			float4 _IndirectDiffuseLightNormal1_ST;
			float4 _DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft;
			float4 _RimColor1;
			float4 _HeightFogColor;
			float4 _AOColour;
			float4 _GardientLerp01EmissionColor;
			float4 _SpecularColor1;
			float3 _Lerp0XYZOffset;
			float3 _PlayerMaskPosition1;
			float3 _NoiseSpeedWorldPos;
			float2 _SpecularDirLightSRPLightIntensity1;
			float2 _SpecularSmoothStepMinMax1;
			float2 _RimSmoothStepMinMax1;
			float2 _SpecularSRPLightSmoothstep1;
			float _WorldNoiseYMaskClip1;
			float _NoiseScale;
			float _FogNoiseEdgeSmoothness;
			float _WorldNoiseYMaskStrength1;
			float _NoiseDensityUseAdd;
			float _SpecularPow1;
			float _UseFogNoise;
			float _FogNoiseEdgeValue;
			float _SpecularIntensity1;
			float _EmissionStrength;
			float _GardientLerp01ColorClip;
			float _GardientLerp01ColorSoft;
			float _GardientLerp01ColorPower;
			float _GardientLerp01ColorAdd;
			float _Alpha;
			float _CamPosDither;
			float _NoiseAlpha;
			float _PlayerPositionMaskAlpha;
			float _BlackClipValue;
			float _BlackClipSmoothness;
			float _NoiseStrengthUsePower;
			float _NoiseFogSoft;
			float _RotateAngleStrength;
			float _HeightFogWorldPosYClipAdd;
			float _HeightControlZOffset;
			float _BridgeFallDownControl;
			float _NegateFallDown;
			float _RotatePower;
			float _rotateRange;
			float _Power;
			float _RotateLerpPower;
			float _Lerp0Scale;
			float _Lerp0YScale;
			float _Lerp0ZScale;
			float _Lerp1Scale;
			float _RampAddincludePower;
			float _RampPower;
			float _NormalStrength1;
			float _DirLightNormalScale1;
			float _LightAttValue1;
			float _LightAttSoft1;
			float _DirLightIntensity1;
			float _RampAddwithoutPower;
			float _RampCelluloidNoiseValue1;
			float _RampCelluloidNoiseSoftness1;
			float _SpecularSRPLightSampler1;
			float _AOThresold;
			float _AOSmoothness;
			float _AOMix;
			float _DiffuseLightStrength1;
			float _RimOffset1;
			float _FogHeightEnd;
			float _FogHeightStart;
			float _NoiseFogClip;
			float _Float5;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			

			float3 RotateAroundAxis( float3 center, float3 original, float3 u, float angle )
			{
				original -= center;
				float C = cos( angle );
				float S = sin( angle );
				float t = 1 - C;
				float m00 = t * u.x * u.x + C;
				float m01 = t * u.x * u.y - S * u.z;
				float m02 = t * u.x * u.z + S * u.y;
				float m10 = t * u.x * u.y + S * u.z;
				float m11 = t * u.y * u.y + C;
				float m12 = t * u.y * u.z - S * u.x;
				float m20 = t * u.x * u.z - S * u.y;
				float m21 = t * u.y * u.z + S * u.x;
				float m22 = t * u.z * u.z + C;
				float3x3 finalMatrix = float3x3( m00, m01, m02, m10, m11, m12, m20, m21, m22 );
				return mul( finalMatrix, original ) + center;
			}
			
			inline float Dither4x4Bayer( int x, int y )
			{
				const float dither[ 16 ] = {
			 1,  9,  3, 11,
			13,  5, 15,  7,
			 4, 12,  2, 10,
			16,  8, 14,  6 };
				int r = y * 4 + x;
				return dither[r] / 16; // same # of instructions as pre-dividing due to compiler magic
			}
			
			float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }
			float snoise( float2 v )
			{
				const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
				float2 i = floor( v + dot( v, C.yy ) );
				float2 x0 = v - i + dot( i, C.xx );
				float2 i1;
				i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
				float4 x12 = x0.xyxy + C.xxzz;
				x12.xy -= i1;
				i = mod2D289( i );
				float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
				float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
				m = m * m;
				m = m * m;
				float3 x = 2.0 * frac( p * C.www ) - 1.0;
				float3 h = abs( x ) - 0.5;
				float3 ox = floor( x + 0.5 );
				float3 a0 = x - ox;
				m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
				float3 g;
				g.x = a0.x * x0.x + h.x * x0.y;
				g.yz = a0.yz * x12.xz + h.yz * x12.yw;
				return 130.0 * dot( m, g );
			}
			

			float3 _LightDirection;

			VertexOutput VertexFunction( VertexInput v )
			{
				VertexOutput o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO( o );

				float3 appendResult152 = (float3(( ( 1.0 - v.ase_color.r ) - 0.5 ) , ( v.ase_color.g - 0.5 ) , ( v.ase_color.b - 0.5 )));
				float3 appendResult64 = (float3(( ( 1.0 - v.ase_color.r ) - 0.5 ) , ( v.ase_color.g - 0.5 ) , ( v.ase_color.b - _HeightControlZOffset )));
				float lerpResult300 = lerp( 0.0 , 25.0 , _NegateFallDown);
				float temp_output_299_0 = ( _BridgeFallDownControl - lerpResult300 );
				float3 appendResult276 = (float3(temp_output_299_0 , temp_output_299_0 , temp_output_299_0));
				float3 worldToObj273 = mul( GetWorldToObjectMatrix(), float4( appendResult276, 1 ) ).xyz;
				float3 rotatedValue269 = RotateAroundAxis( appendResult152, v.vertex.xyz, normalize( appendResult152 ), ( ( _RotateAngleStrength * saturate( ( pow( distance( appendResult64 , worldToObj273 ) , _RotatePower ) - _rotateRange ) ) ) * PI ) );
				float2 appendResult292 = (float2(_DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.x , _DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.y));
				float2 appendResult293 = (float2(_DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.z , _DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.w));
				float2 lerpResult288 = lerp( appendResult292 , appendResult293 , _NegateFallDown);
				float2 DIstanceValueandSoft294 = lerpResult288;
				float2 break296 = DIstanceValueandSoft294;
				float3 objToWorld180 = mul( GetObjectToWorldMatrix(), float4( appendResult64, 1 ) ).xyz;
				#if defined(_AXIS_X)
				float staticSwitch588 = objToWorld180.x;
				#elif defined(_AXIS_Z)
				float staticSwitch588 = objToWorld180.z;
				#else
				float staticSwitch588 = objToWorld180.x;
				#endif
				float temp_output_185_0 = ( staticSwitch588 + _BridgeFallDownControl );
				float ifLocalVar186 = 0;
				if( temp_output_185_0 > 0.0 )
				ifLocalVar186 = 1.0;
				else if( temp_output_185_0 == 0.0 )
				ifLocalVar186 = 0.0;
				else if( temp_output_185_0 < 0.0 )
				ifLocalVar186 = -1.0;
				float smoothstepResult139 = smoothstep( break296.x , ( break296.x + break296.y ) , ( ifLocalVar186 * pow( temp_output_185_0 , _Power ) ));
				float temp_output_159_0 = saturate( smoothstepResult139 );
				float3 lerpResult280 = lerp( ( rotatedValue269 - v.vertex.xyz ) , float3( 0,0,0 ) , pow( temp_output_159_0 , _RotateLerpPower ));
				float3 temp_output_138_0 = ( appendResult64 * _Lerp0Scale );
				float2 break256 = (temp_output_138_0).xz;
				float3 appendResult255 = (float3(break256.x , ( _Lerp0YScale * (temp_output_138_0).y ) , break256.y));
				float3 appendResult174 = (float3(_Lerp0XYZOffset.x , _Lerp0XYZOffset.y , -pow( _Lerp0XYZOffset.z , _Lerp0ZScale )));
				float3 lerpResult143 = lerp( ( appendResult255 + appendResult174 ) , ( appendResult152 * _Lerp1Scale ) , temp_output_159_0);
				#ifdef _OFFSETEFFECTOPEN_ON
				float3 staticSwitch137 = ( saturate( lerpResult280 ) + lerpResult143 );
				#else
				float3 staticSwitch137 = float3( 0,0,0 );
				#endif
				float3 VertexOffsetOutput301 = staticSwitch137;
				
				float4 ase_clipPos = TransformObjectToHClip((v.vertex).xyz);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord2 = screenPos;
				
				o.ase_texcoord3 = v.vertex;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = VertexOffsetOutput301;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif

				float3 normalWS = TransformObjectToWorldDir( v.ase_normal );

				float4 clipPos = TransformWorldToHClip( ApplyShadowBias( positionWS, normalWS, _LightDirection ) );

				#if UNITY_REVERSED_Z
					clipPos.z = min(clipPos.z, clipPos.w * UNITY_NEAR_CLIP_VALUE);
				#else
					clipPos.z = max(clipPos.z, clipPos.w * UNITY_NEAR_CLIP_VALUE);
				#endif

				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = clipPos;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				o.clipPos = clipPos;

				return o;
			}
			
			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_color : COLOR;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_color = v.ase_color;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_color = patch[0].ase_color * bary.x + patch[1].ase_color * bary.y + patch[2].ase_color * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float4 screenPos = IN.ase_texcoord2;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float4 ditherCustomScreenPos497 = ase_screenPosNorm;
				float2 clipScreen497 = ditherCustomScreenPos497.xy * _ScreenParams.xy;
				float dither497 = Dither4x4Bayer( fmod(clipScreen497.x, 4), fmod(clipScreen497.y, 4) );
				dither497 = step( dither497, _Alpha );
				float simplePerlin2D482 = snoise( ase_screenPosNorm.xy*5000.0 );
				simplePerlin2D482 = simplePerlin2D482*0.5 + 0.5;
				#ifdef _ONLYUSEXZAXIS_ON
				float staticSwitch470 = distance( (WorldPosition).xz , (_WorldSpaceCameraPos).xz );
				#else
				float staticSwitch470 = distance( WorldPosition , _WorldSpaceCameraPos );
				#endif
				float saferPower479 = max( staticSwitch470 , 0.0001 );
				#ifdef _USECAMERADEPTHCLIP_ON
				float staticSwitch509 = ( dither497 * saturate( ( simplePerlin2D482 + pow( saferPower479 , _CamPosDither ) + _NoiseAlpha ) ) );
				#else
				float staticSwitch509 = dither497;
				#endif
				float3 objToWorld486 = mul( GetObjectToWorldMatrix(), float4( IN.ase_texcoord3.xyz, 1 ) ).xyz;
				float temp_output_490_0 = distance( _PlayerMaskPosition1 , objToWorld486 );
				float temp_output_501_0 = pow( temp_output_490_0 , _Float5 );
				float smoothstepResult510 = smoothstep( _BlackClipValue , ( _BlackClipValue + _BlackClipSmoothness ) , temp_output_501_0);
				float lerpResult521 = lerp( staticSwitch509 , ( simplePerlin2D482 * _PlayerPositionMaskAlpha ) , smoothstepResult510);
				#ifdef _PLAYERMASKOPEN_ON
				float staticSwitch559 = lerpResult521;
				#else
				float staticSwitch559 = staticSwitch509;
				#endif
				
				#ifdef _SHADOWUSEDITHER1_ON
				float staticSwitch545 = 0.001;
				#else
				float staticSwitch545 = 0.0;
				#endif
				
				float Alpha = staticSwitch559;
				float AlphaClipThreshold = staticSwitch545;
				float AlphaClipThresholdShadow = 0.5;

				#ifdef _ALPHATEST_ON
					#ifdef _ALPHATEST_SHADOW_ON
						clip(Alpha - AlphaClipThresholdShadow);
					#else
						clip(Alpha - AlphaClipThreshold);
					#endif
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif
				return 0;
			}

			ENDHLSL
		}

		
		Pass
		{
			
			Name "DepthOnly"
			Tags { "LightMode"="DepthOnly" }

			ZWrite On
			ColorMask 0
			AlphaToMask Off

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#define _ALPHATEST_ON 1
			#define ASE_SRP_VERSION 100302

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

			#define ASE_NEEDS_VERT_POSITION
			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#pragma shader_feature_local _OFFSETEFFECTOPEN_ON
			#pragma shader_feature_local _AXIS_X _AXIS_Z
			#pragma shader_feature_local _PLAYERMASKOPEN_ON
			#pragma shader_feature_local _USECAMERADEPTHCLIP_ON
			#pragma shader_feature_local _ONLYUSEXZAXIS_ON
			#pragma shader_feature_local _SHADOWUSEDITHER1_ON
			#pragma __SCREEN_SPACE_OCCLUSION
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_color : COLOR;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				float4 ase_texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _BassColor;
			float4 _EmissionTexture_ST;
			float4 _NormalMap1_ST;
			float4 _EmissionColor;
			float4 _Color0;
			float4 _AlbedoEmissionColor1;
			float4 _AlbedoMap1_ST;
			float4 _SpecularMap1_ST;
			float4 _IndirectDiffuseLightNormal1_ST;
			float4 _DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft;
			float4 _RimColor1;
			float4 _HeightFogColor;
			float4 _AOColour;
			float4 _GardientLerp01EmissionColor;
			float4 _SpecularColor1;
			float3 _Lerp0XYZOffset;
			float3 _PlayerMaskPosition1;
			float3 _NoiseSpeedWorldPos;
			float2 _SpecularDirLightSRPLightIntensity1;
			float2 _SpecularSmoothStepMinMax1;
			float2 _RimSmoothStepMinMax1;
			float2 _SpecularSRPLightSmoothstep1;
			float _WorldNoiseYMaskClip1;
			float _NoiseScale;
			float _FogNoiseEdgeSmoothness;
			float _WorldNoiseYMaskStrength1;
			float _NoiseDensityUseAdd;
			float _SpecularPow1;
			float _UseFogNoise;
			float _FogNoiseEdgeValue;
			float _SpecularIntensity1;
			float _EmissionStrength;
			float _GardientLerp01ColorClip;
			float _GardientLerp01ColorSoft;
			float _GardientLerp01ColorPower;
			float _GardientLerp01ColorAdd;
			float _Alpha;
			float _CamPosDither;
			float _NoiseAlpha;
			float _PlayerPositionMaskAlpha;
			float _BlackClipValue;
			float _BlackClipSmoothness;
			float _NoiseStrengthUsePower;
			float _NoiseFogSoft;
			float _RotateAngleStrength;
			float _HeightFogWorldPosYClipAdd;
			float _HeightControlZOffset;
			float _BridgeFallDownControl;
			float _NegateFallDown;
			float _RotatePower;
			float _rotateRange;
			float _Power;
			float _RotateLerpPower;
			float _Lerp0Scale;
			float _Lerp0YScale;
			float _Lerp0ZScale;
			float _Lerp1Scale;
			float _RampAddincludePower;
			float _RampPower;
			float _NormalStrength1;
			float _DirLightNormalScale1;
			float _LightAttValue1;
			float _LightAttSoft1;
			float _DirLightIntensity1;
			float _RampAddwithoutPower;
			float _RampCelluloidNoiseValue1;
			float _RampCelluloidNoiseSoftness1;
			float _SpecularSRPLightSampler1;
			float _AOThresold;
			float _AOSmoothness;
			float _AOMix;
			float _DiffuseLightStrength1;
			float _RimOffset1;
			float _FogHeightEnd;
			float _FogHeightStart;
			float _NoiseFogClip;
			float _Float5;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			

			float3 RotateAroundAxis( float3 center, float3 original, float3 u, float angle )
			{
				original -= center;
				float C = cos( angle );
				float S = sin( angle );
				float t = 1 - C;
				float m00 = t * u.x * u.x + C;
				float m01 = t * u.x * u.y - S * u.z;
				float m02 = t * u.x * u.z + S * u.y;
				float m10 = t * u.x * u.y + S * u.z;
				float m11 = t * u.y * u.y + C;
				float m12 = t * u.y * u.z - S * u.x;
				float m20 = t * u.x * u.z - S * u.y;
				float m21 = t * u.y * u.z + S * u.x;
				float m22 = t * u.z * u.z + C;
				float3x3 finalMatrix = float3x3( m00, m01, m02, m10, m11, m12, m20, m21, m22 );
				return mul( finalMatrix, original ) + center;
			}
			
			inline float Dither4x4Bayer( int x, int y )
			{
				const float dither[ 16 ] = {
			 1,  9,  3, 11,
			13,  5, 15,  7,
			 4, 12,  2, 10,
			16,  8, 14,  6 };
				int r = y * 4 + x;
				return dither[r] / 16; // same # of instructions as pre-dividing due to compiler magic
			}
			
			float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }
			float snoise( float2 v )
			{
				const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
				float2 i = floor( v + dot( v, C.yy ) );
				float2 x0 = v - i + dot( i, C.xx );
				float2 i1;
				i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
				float4 x12 = x0.xyxy + C.xxzz;
				x12.xy -= i1;
				i = mod2D289( i );
				float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
				float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
				m = m * m;
				m = m * m;
				float3 x = 2.0 * frac( p * C.www ) - 1.0;
				float3 h = abs( x ) - 0.5;
				float3 ox = floor( x + 0.5 );
				float3 a0 = x - ox;
				m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
				float3 g;
				g.x = a0.x * x0.x + h.x * x0.y;
				g.yz = a0.yz * x12.xz + h.yz * x12.yw;
				return 130.0 * dot( m, g );
			}
			

			VertexOutput VertexFunction( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float3 appendResult152 = (float3(( ( 1.0 - v.ase_color.r ) - 0.5 ) , ( v.ase_color.g - 0.5 ) , ( v.ase_color.b - 0.5 )));
				float3 appendResult64 = (float3(( ( 1.0 - v.ase_color.r ) - 0.5 ) , ( v.ase_color.g - 0.5 ) , ( v.ase_color.b - _HeightControlZOffset )));
				float lerpResult300 = lerp( 0.0 , 25.0 , _NegateFallDown);
				float temp_output_299_0 = ( _BridgeFallDownControl - lerpResult300 );
				float3 appendResult276 = (float3(temp_output_299_0 , temp_output_299_0 , temp_output_299_0));
				float3 worldToObj273 = mul( GetWorldToObjectMatrix(), float4( appendResult276, 1 ) ).xyz;
				float3 rotatedValue269 = RotateAroundAxis( appendResult152, v.vertex.xyz, normalize( appendResult152 ), ( ( _RotateAngleStrength * saturate( ( pow( distance( appendResult64 , worldToObj273 ) , _RotatePower ) - _rotateRange ) ) ) * PI ) );
				float2 appendResult292 = (float2(_DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.x , _DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.y));
				float2 appendResult293 = (float2(_DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.z , _DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.w));
				float2 lerpResult288 = lerp( appendResult292 , appendResult293 , _NegateFallDown);
				float2 DIstanceValueandSoft294 = lerpResult288;
				float2 break296 = DIstanceValueandSoft294;
				float3 objToWorld180 = mul( GetObjectToWorldMatrix(), float4( appendResult64, 1 ) ).xyz;
				#if defined(_AXIS_X)
				float staticSwitch588 = objToWorld180.x;
				#elif defined(_AXIS_Z)
				float staticSwitch588 = objToWorld180.z;
				#else
				float staticSwitch588 = objToWorld180.x;
				#endif
				float temp_output_185_0 = ( staticSwitch588 + _BridgeFallDownControl );
				float ifLocalVar186 = 0;
				if( temp_output_185_0 > 0.0 )
				ifLocalVar186 = 1.0;
				else if( temp_output_185_0 == 0.0 )
				ifLocalVar186 = 0.0;
				else if( temp_output_185_0 < 0.0 )
				ifLocalVar186 = -1.0;
				float smoothstepResult139 = smoothstep( break296.x , ( break296.x + break296.y ) , ( ifLocalVar186 * pow( temp_output_185_0 , _Power ) ));
				float temp_output_159_0 = saturate( smoothstepResult139 );
				float3 lerpResult280 = lerp( ( rotatedValue269 - v.vertex.xyz ) , float3( 0,0,0 ) , pow( temp_output_159_0 , _RotateLerpPower ));
				float3 temp_output_138_0 = ( appendResult64 * _Lerp0Scale );
				float2 break256 = (temp_output_138_0).xz;
				float3 appendResult255 = (float3(break256.x , ( _Lerp0YScale * (temp_output_138_0).y ) , break256.y));
				float3 appendResult174 = (float3(_Lerp0XYZOffset.x , _Lerp0XYZOffset.y , -pow( _Lerp0XYZOffset.z , _Lerp0ZScale )));
				float3 lerpResult143 = lerp( ( appendResult255 + appendResult174 ) , ( appendResult152 * _Lerp1Scale ) , temp_output_159_0);
				#ifdef _OFFSETEFFECTOPEN_ON
				float3 staticSwitch137 = ( saturate( lerpResult280 ) + lerpResult143 );
				#else
				float3 staticSwitch137 = float3( 0,0,0 );
				#endif
				float3 VertexOffsetOutput301 = staticSwitch137;
				
				float4 ase_clipPos = TransformObjectToHClip((v.vertex).xyz);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord2 = screenPos;
				
				o.ase_texcoord3 = v.vertex;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = VertexOffsetOutput301;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif

				o.clipPos = TransformWorldToHClip( positionWS );
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = clipPos;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_color : COLOR;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_color = v.ase_color;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_color = patch[0].ase_color * bary.x + patch[1].ase_color * bary.y + patch[2].ase_color * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float4 screenPos = IN.ase_texcoord2;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float4 ditherCustomScreenPos497 = ase_screenPosNorm;
				float2 clipScreen497 = ditherCustomScreenPos497.xy * _ScreenParams.xy;
				float dither497 = Dither4x4Bayer( fmod(clipScreen497.x, 4), fmod(clipScreen497.y, 4) );
				dither497 = step( dither497, _Alpha );
				float simplePerlin2D482 = snoise( ase_screenPosNorm.xy*5000.0 );
				simplePerlin2D482 = simplePerlin2D482*0.5 + 0.5;
				#ifdef _ONLYUSEXZAXIS_ON
				float staticSwitch470 = distance( (WorldPosition).xz , (_WorldSpaceCameraPos).xz );
				#else
				float staticSwitch470 = distance( WorldPosition , _WorldSpaceCameraPos );
				#endif
				float saferPower479 = max( staticSwitch470 , 0.0001 );
				#ifdef _USECAMERADEPTHCLIP_ON
				float staticSwitch509 = ( dither497 * saturate( ( simplePerlin2D482 + pow( saferPower479 , _CamPosDither ) + _NoiseAlpha ) ) );
				#else
				float staticSwitch509 = dither497;
				#endif
				float3 objToWorld486 = mul( GetObjectToWorldMatrix(), float4( IN.ase_texcoord3.xyz, 1 ) ).xyz;
				float temp_output_490_0 = distance( _PlayerMaskPosition1 , objToWorld486 );
				float temp_output_501_0 = pow( temp_output_490_0 , _Float5 );
				float smoothstepResult510 = smoothstep( _BlackClipValue , ( _BlackClipValue + _BlackClipSmoothness ) , temp_output_501_0);
				float lerpResult521 = lerp( staticSwitch509 , ( simplePerlin2D482 * _PlayerPositionMaskAlpha ) , smoothstepResult510);
				#ifdef _PLAYERMASKOPEN_ON
				float staticSwitch559 = lerpResult521;
				#else
				float staticSwitch559 = staticSwitch509;
				#endif
				
				#ifdef _SHADOWUSEDITHER1_ON
				float staticSwitch545 = 0.001;
				#else
				float staticSwitch545 = 0.0;
				#endif
				
				float Alpha = staticSwitch559;
				float AlphaClipThreshold = staticSwitch545;

				#ifdef _ALPHATEST_ON
					clip(Alpha - AlphaClipThreshold);
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif
				return 0;
			}
			ENDHLSL
		}

		
		Pass
		{
			
			Name "Meta"
			Tags { "LightMode"="Meta" }

			Cull Off

			HLSLPROGRAM
			#pragma multi_compile_instancing
			#pragma multi_compile _ LOD_FADE_CROSSFADE
			#pragma multi_compile_fog
			#define ASE_FOG 1
			#define _ALPHATEST_ON 1
			#define ASE_SRP_VERSION 100302

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/MetaInput.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

			#include "Packages/com.unity.shadergraph/ShaderGraphLibrary/Functions.hlsl"
			#define ASE_NEEDS_VERT_POSITION
			#define ASE_NEEDS_VERT_NORMAL
			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#define ASE_NEEDS_FRAG_SHADOWCOORDS
			#define ASE_NEEDS_VERT_TEXTURE_COORDINATES1
			#define ASE_NEEDS_FRAG_COLOR
			#define ASE_NEEDS_FRAG_POSITION
			#pragma shader_feature_local _OFFSETEFFECTOPEN_ON
			#pragma shader_feature_local _AXIS_X _AXIS_Z
			#pragma shader_feature_local _SHADOWREMAPTYPE1_USESHADERGARDIENTCOLOR _SHADOWREMAPTYPE1_USESHADOWREMAPMAP
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS
			#pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
			#pragma multi_compile _ _SHADOWS_SOFT
			#pragma shader_feature_local _USESSAO_ON
			#pragma multi_compile _ DIRLIGHTMAP_COMBINED
			#pragma multi_compile _ LIGHTMAP_ON
			#pragma shader_feature_local _USEHEIGHTFOG_ON
			#pragma shader_feature_local _PLAYERMASKOPEN_ON
			#pragma shader_feature_local _USECAMERADEPTHCLIP_ON
			#pragma shader_feature_local _ONLYUSEXZAXIS_ON
			#pragma shader_feature_local _SHADOWUSEDITHER1_ON
			#pragma __SCREEN_SPACE_OCCLUSION
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 texcoord1 : TEXCOORD1;
				float4 texcoord2 : TEXCOORD2;
				float4 ase_color : COLOR;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_tangent : TANGENT;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				float4 ase_texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_texcoord4 : TEXCOORD4;
				float4 ase_texcoord5 : TEXCOORD5;
				float4 lightmapUVOrVertexSH : TEXCOORD6;
				float4 ase_texcoord7 : TEXCOORD7;
				float4 ase_color : COLOR;
				float4 ase_texcoord8 : TEXCOORD8;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _BassColor;
			float4 _EmissionTexture_ST;
			float4 _NormalMap1_ST;
			float4 _EmissionColor;
			float4 _Color0;
			float4 _AlbedoEmissionColor1;
			float4 _AlbedoMap1_ST;
			float4 _SpecularMap1_ST;
			float4 _IndirectDiffuseLightNormal1_ST;
			float4 _DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft;
			float4 _RimColor1;
			float4 _HeightFogColor;
			float4 _AOColour;
			float4 _GardientLerp01EmissionColor;
			float4 _SpecularColor1;
			float3 _Lerp0XYZOffset;
			float3 _PlayerMaskPosition1;
			float3 _NoiseSpeedWorldPos;
			float2 _SpecularDirLightSRPLightIntensity1;
			float2 _SpecularSmoothStepMinMax1;
			float2 _RimSmoothStepMinMax1;
			float2 _SpecularSRPLightSmoothstep1;
			float _WorldNoiseYMaskClip1;
			float _NoiseScale;
			float _FogNoiseEdgeSmoothness;
			float _WorldNoiseYMaskStrength1;
			float _NoiseDensityUseAdd;
			float _SpecularPow1;
			float _UseFogNoise;
			float _FogNoiseEdgeValue;
			float _SpecularIntensity1;
			float _EmissionStrength;
			float _GardientLerp01ColorClip;
			float _GardientLerp01ColorSoft;
			float _GardientLerp01ColorPower;
			float _GardientLerp01ColorAdd;
			float _Alpha;
			float _CamPosDither;
			float _NoiseAlpha;
			float _PlayerPositionMaskAlpha;
			float _BlackClipValue;
			float _BlackClipSmoothness;
			float _NoiseStrengthUsePower;
			float _NoiseFogSoft;
			float _RotateAngleStrength;
			float _HeightFogWorldPosYClipAdd;
			float _HeightControlZOffset;
			float _BridgeFallDownControl;
			float _NegateFallDown;
			float _RotatePower;
			float _rotateRange;
			float _Power;
			float _RotateLerpPower;
			float _Lerp0Scale;
			float _Lerp0YScale;
			float _Lerp0ZScale;
			float _Lerp1Scale;
			float _RampAddincludePower;
			float _RampPower;
			float _NormalStrength1;
			float _DirLightNormalScale1;
			float _LightAttValue1;
			float _LightAttSoft1;
			float _DirLightIntensity1;
			float _RampAddwithoutPower;
			float _RampCelluloidNoiseValue1;
			float _RampCelluloidNoiseSoftness1;
			float _SpecularSRPLightSampler1;
			float _AOThresold;
			float _AOSmoothness;
			float _AOMix;
			float _DiffuseLightStrength1;
			float _RimOffset1;
			float _FogHeightEnd;
			float _FogHeightStart;
			float _NoiseFogClip;
			float _Float5;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			sampler2D _ShadowRemapMap;
			sampler2D _NormalMap1;
			sampler2D _RampCelluloidNoiseTexture1;
			sampler2D _AlbedoMap1;
			sampler2D _IndirectDiffuseLightNormal1;
			sampler2D _SpecularMap1;
			sampler2D _EmissionTexture;


			float3 RotateAroundAxis( float3 center, float3 original, float3 u, float angle )
			{
				original -= center;
				float C = cos( angle );
				float S = sin( angle );
				float t = 1 - C;
				float m00 = t * u.x * u.x + C;
				float m01 = t * u.x * u.y - S * u.z;
				float m02 = t * u.x * u.z + S * u.y;
				float m10 = t * u.x * u.y + S * u.z;
				float m11 = t * u.y * u.y + C;
				float m12 = t * u.y * u.z - S * u.x;
				float m20 = t * u.x * u.z - S * u.y;
				float m21 = t * u.y * u.z + S * u.x;
				float m22 = t * u.z * u.z + C;
				float3x3 finalMatrix = float3x3( m00, m01, m02, m10, m11, m12, m20, m21, m22 );
				return mul( finalMatrix, original ) + center;
			}
			
			
			float4 SampleGradient( Gradient gradient, float time )
			{
				float3 color = gradient.colors[0].rgb;
				UNITY_UNROLL
				for (int c = 1; c < 8; c++)
				{
				float colorPos = saturate((time - gradient.colors[c-1].w) / ( 0.00001 + (gradient.colors[c].w - gradient.colors[c-1].w)) * step(c, gradient.colorsLength-1));
				color = lerp(color, gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), gradient.type));
				}
				#ifndef UNITY_COLORSPACE_GAMMA
				color = SRGBToLinear(color);
				#endif
				float alpha = gradient.alphas[0].x;
				UNITY_UNROLL
				for (int a = 1; a < 8; a++)
				{
				float alphaPos = saturate((time - gradient.alphas[a-1].y) / ( 0.00001 + (gradient.alphas[a].y - gradient.alphas[a-1].y)) * step(a, gradient.alphasLength-1));
				alpha = lerp(alpha, gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), gradient.type));
				}
				return float4(color, alpha);
			}
			
			float3 AdditionalLightsLambert( float3 WorldPosition, float3 WorldNormal )
			{
				float3 Color = 0;
				#ifdef _ADDITIONAL_LIGHTS
				int numLights = GetAdditionalLightsCount();
				for(int i = 0; i<numLights;i++)
				{
					Light light = GetAdditionalLight(i, WorldPosition);
					half3 AttLightColor = light.color *(light.distanceAttenuation * light.shadowAttenuation);
					Color +=LightingLambert(AttLightColor, light.direction, WorldNormal);
					
				}
				#endif
				return Color;
			}
			
			float3 ASEIndirectDiffuse( float2 uvStaticLightmap, float3 normalWS )
			{
			#ifdef LIGHTMAP_ON
				return SampleLightmap( uvStaticLightmap, normalWS );
			#else
				return SampleSH(normalWS);
			#endif
			}
			
			half SampleAO17_g14( half2 In0 )
			{
				return SampleAmbientOcclusion(In0);
			}
			
			float3 mod3D289( float3 x ) { return x - floor( x / 289.0 ) * 289.0; }
			float4 mod3D289( float4 x ) { return x - floor( x / 289.0 ) * 289.0; }
			float4 permute( float4 x ) { return mod3D289( ( x * 34.0 + 1.0 ) * x ); }
			float4 taylorInvSqrt( float4 r ) { return 1.79284291400159 - r * 0.85373472095314; }
			float snoise( float3 v )
			{
				const float2 C = float2( 1.0 / 6.0, 1.0 / 3.0 );
				float3 i = floor( v + dot( v, C.yyy ) );
				float3 x0 = v - i + dot( i, C.xxx );
				float3 g = step( x0.yzx, x0.xyz );
				float3 l = 1.0 - g;
				float3 i1 = min( g.xyz, l.zxy );
				float3 i2 = max( g.xyz, l.zxy );
				float3 x1 = x0 - i1 + C.xxx;
				float3 x2 = x0 - i2 + C.yyy;
				float3 x3 = x0 - 0.5;
				i = mod3D289( i);
				float4 p = permute( permute( permute( i.z + float4( 0.0, i1.z, i2.z, 1.0 ) ) + i.y + float4( 0.0, i1.y, i2.y, 1.0 ) ) + i.x + float4( 0.0, i1.x, i2.x, 1.0 ) );
				float4 j = p - 49.0 * floor( p / 49.0 );  // mod(p,7*7)
				float4 x_ = floor( j / 7.0 );
				float4 y_ = floor( j - 7.0 * x_ );  // mod(j,N)
				float4 x = ( x_ * 2.0 + 0.5 ) / 7.0 - 1.0;
				float4 y = ( y_ * 2.0 + 0.5 ) / 7.0 - 1.0;
				float4 h = 1.0 - abs( x ) - abs( y );
				float4 b0 = float4( x.xy, y.xy );
				float4 b1 = float4( x.zw, y.zw );
				float4 s0 = floor( b0 ) * 2.0 + 1.0;
				float4 s1 = floor( b1 ) * 2.0 + 1.0;
				float4 sh = -step( h, 0.0 );
				float4 a0 = b0.xzyw + s0.xzyw * sh.xxyy;
				float4 a1 = b1.xzyw + s1.xzyw * sh.zzww;
				float3 g0 = float3( a0.xy, h.x );
				float3 g1 = float3( a0.zw, h.y );
				float3 g2 = float3( a1.xy, h.z );
				float3 g3 = float3( a1.zw, h.w );
				float4 norm = taylorInvSqrt( float4( dot( g0, g0 ), dot( g1, g1 ), dot( g2, g2 ), dot( g3, g3 ) ) );
				g0 *= norm.x;
				g1 *= norm.y;
				g2 *= norm.z;
				g3 *= norm.w;
				float4 m = max( 0.6 - float4( dot( x0, x0 ), dot( x1, x1 ), dot( x2, x2 ), dot( x3, x3 ) ), 0.0 );
				m = m* m;
				m = m* m;
				float4 px = float4( dot( x0, g0 ), dot( x1, g1 ), dot( x2, g2 ), dot( x3, g3 ) );
				return 42.0 * dot( m, px);
			}
			
			inline float Dither4x4Bayer( int x, int y )
			{
				const float dither[ 16 ] = {
			 1,  9,  3, 11,
			13,  5, 15,  7,
			 4, 12,  2, 10,
			16,  8, 14,  6 };
				int r = y * 4 + x;
				return dither[r] / 16; // same # of instructions as pre-dividing due to compiler magic
			}
			
			float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }
			float snoise( float2 v )
			{
				const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
				float2 i = floor( v + dot( v, C.yy ) );
				float2 x0 = v - i + dot( i, C.xx );
				float2 i1;
				i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
				float4 x12 = x0.xyxy + C.xxzz;
				x12.xy -= i1;
				i = mod2D289( i );
				float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
				float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
				m = m * m;
				m = m * m;
				float3 x = 2.0 * frac( p * C.www ) - 1.0;
				float3 h = abs( x ) - 0.5;
				float3 ox = floor( x + 0.5 );
				float3 a0 = x - ox;
				m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
				float3 g;
				g.x = a0.x * x0.x + h.x * x0.y;
				g.yz = a0.yz * x12.xz + h.yz * x12.yw;
				return 130.0 * dot( m, g );
			}
			

			VertexOutput VertexFunction( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float3 appendResult152 = (float3(( ( 1.0 - v.ase_color.r ) - 0.5 ) , ( v.ase_color.g - 0.5 ) , ( v.ase_color.b - 0.5 )));
				float3 appendResult64 = (float3(( ( 1.0 - v.ase_color.r ) - 0.5 ) , ( v.ase_color.g - 0.5 ) , ( v.ase_color.b - _HeightControlZOffset )));
				float lerpResult300 = lerp( 0.0 , 25.0 , _NegateFallDown);
				float temp_output_299_0 = ( _BridgeFallDownControl - lerpResult300 );
				float3 appendResult276 = (float3(temp_output_299_0 , temp_output_299_0 , temp_output_299_0));
				float3 worldToObj273 = mul( GetWorldToObjectMatrix(), float4( appendResult276, 1 ) ).xyz;
				float3 rotatedValue269 = RotateAroundAxis( appendResult152, v.vertex.xyz, normalize( appendResult152 ), ( ( _RotateAngleStrength * saturate( ( pow( distance( appendResult64 , worldToObj273 ) , _RotatePower ) - _rotateRange ) ) ) * PI ) );
				float2 appendResult292 = (float2(_DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.x , _DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.y));
				float2 appendResult293 = (float2(_DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.z , _DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.w));
				float2 lerpResult288 = lerp( appendResult292 , appendResult293 , _NegateFallDown);
				float2 DIstanceValueandSoft294 = lerpResult288;
				float2 break296 = DIstanceValueandSoft294;
				float3 objToWorld180 = mul( GetObjectToWorldMatrix(), float4( appendResult64, 1 ) ).xyz;
				#if defined(_AXIS_X)
				float staticSwitch588 = objToWorld180.x;
				#elif defined(_AXIS_Z)
				float staticSwitch588 = objToWorld180.z;
				#else
				float staticSwitch588 = objToWorld180.x;
				#endif
				float temp_output_185_0 = ( staticSwitch588 + _BridgeFallDownControl );
				float ifLocalVar186 = 0;
				if( temp_output_185_0 > 0.0 )
				ifLocalVar186 = 1.0;
				else if( temp_output_185_0 == 0.0 )
				ifLocalVar186 = 0.0;
				else if( temp_output_185_0 < 0.0 )
				ifLocalVar186 = -1.0;
				float smoothstepResult139 = smoothstep( break296.x , ( break296.x + break296.y ) , ( ifLocalVar186 * pow( temp_output_185_0 , _Power ) ));
				float temp_output_159_0 = saturate( smoothstepResult139 );
				float3 lerpResult280 = lerp( ( rotatedValue269 - v.vertex.xyz ) , float3( 0,0,0 ) , pow( temp_output_159_0 , _RotateLerpPower ));
				float3 temp_output_138_0 = ( appendResult64 * _Lerp0Scale );
				float2 break256 = (temp_output_138_0).xz;
				float3 appendResult255 = (float3(break256.x , ( _Lerp0YScale * (temp_output_138_0).y ) , break256.y));
				float3 appendResult174 = (float3(_Lerp0XYZOffset.x , _Lerp0XYZOffset.y , -pow( _Lerp0XYZOffset.z , _Lerp0ZScale )));
				float3 lerpResult143 = lerp( ( appendResult255 + appendResult174 ) , ( appendResult152 * _Lerp1Scale ) , temp_output_159_0);
				#ifdef _OFFSETEFFECTOPEN_ON
				float3 staticSwitch137 = ( saturate( lerpResult280 ) + lerpResult143 );
				#else
				float3 staticSwitch137 = float3( 0,0,0 );
				#endif
				float3 VertexOffsetOutput301 = staticSwitch137;
				
				float3 ase_worldTangent = TransformObjectToWorldDir(v.ase_tangent.xyz);
				o.ase_texcoord3.xyz = ase_worldTangent;
				float3 ase_worldNormal = TransformObjectToWorldNormal(v.ase_normal);
				o.ase_texcoord4.xyz = ase_worldNormal;
				float ase_vertexTangentSign = v.ase_tangent.w * unity_WorldTransformParams.w;
				float3 ase_worldBitangent = cross( ase_worldNormal, ase_worldTangent ) * ase_vertexTangentSign;
				o.ase_texcoord5.xyz = ase_worldBitangent;
				OUTPUT_LIGHTMAP_UV( v.texcoord1, unity_LightmapST, o.lightmapUVOrVertexSH.xy );
				OUTPUT_SH( ase_worldNormal, o.lightmapUVOrVertexSH.xyz );
				
				float4 ase_clipPos = TransformObjectToHClip((v.vertex).xyz);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord8 = screenPos;
				
				o.ase_texcoord2.xy = v.ase_texcoord.xy;
				o.ase_texcoord7 = v.vertex;
				o.ase_color = v.ase_color;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord2.zw = 0;
				o.ase_texcoord3.w = 0;
				o.ase_texcoord4.w = 0;
				o.ase_texcoord5.w = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = VertexOffsetOutput301;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = v.ase_normal;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif

				o.clipPos = MetaVertexPosition( v.vertex, v.texcoord1.xy, v.texcoord1.xy, unity_LightmapST, unity_DynamicLightmapST );
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = o.clipPos;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_color : COLOR;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_tangent : TANGENT;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_color = v.ase_color;
				o.ase_texcoord = v.ase_texcoord;
				o.ase_tangent = v.ase_tangent;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_color = patch[0].ase_color * bary.x + patch[1].ase_color * bary.y + patch[2].ase_color * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				o.ase_tangent = patch[0].ase_tangent * bary.x + patch[1].ase_tangent * bary.y + patch[2].ase_tangent * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float saferPower382 = max( _RampAddincludePower , 0.0001 );
				float2 uv_NormalMap1 = IN.ase_texcoord2.xy * _NormalMap1_ST.xy + _NormalMap1_ST.zw;
				float3 unpack326 = UnpackNormalScale( tex2D( _NormalMap1, uv_NormalMap1 ), _NormalStrength1 );
				unpack326.z = lerp( 1, unpack326.z, saturate(_NormalStrength1) );
				float3 Normal337 = unpack326;
				float3 ase_worldTangent = IN.ase_texcoord3.xyz;
				float3 ase_worldNormal = IN.ase_texcoord4.xyz;
				float3 ase_worldBitangent = IN.ase_texcoord5.xyz;
				float3 tanToWorld0 = float3( ase_worldTangent.x, ase_worldBitangent.x, ase_worldNormal.x );
				float3 tanToWorld1 = float3( ase_worldTangent.y, ase_worldBitangent.y, ase_worldNormal.y );
				float3 tanToWorld2 = float3( ase_worldTangent.z, ase_worldBitangent.z, ase_worldNormal.z );
				float3 tanNormal347 = Normal337;
				float3 worldNormal347 = float3(dot(tanToWorld0,tanNormal347), dot(tanToWorld1,tanNormal347), dot(tanToWorld2,tanNormal347));
				float dotResult359 = dot( worldNormal347 , SafeNormalize(_MainLightPosition.xyz) );
				float ase_lightAtten = 0;
				Light ase_lightAtten_mainLight = GetMainLight( ShadowCoords );
				ase_lightAtten = ase_lightAtten_mainLight.distanceAttenuation * ase_lightAtten_mainLight.shadowAttenuation;
				float smoothstepResult328 = smoothstep( _LightAttValue1 , ( _LightAttValue1 + _LightAttSoft1 ) , ase_lightAtten);
				float3 temp_output_332_0 = ( smoothstepResult328 * ( _MainLightColor.rgb * _MainLightColor.a * _DirLightIntensity1 ) );
				float3 break339 = temp_output_332_0;
				float2 texCoord370 = IN.ase_texcoord2.xy * float2( 2,2 ) + float2( 0,0 );
				float smoothstepResult406 = smoothstep( _RampCelluloidNoiseValue1 , _RampCelluloidNoiseSoftness1 , tex2D( _RampCelluloidNoiseTexture1, texCoord370 ).r);
				float temp_output_412_0 = ( ( pow( saferPower382 , _RampPower ) + ( (dotResult359*_DirLightNormalScale1 + 0.5) * ( max( max( break339.x , break339.y ) , break339.z ) + 0.0 ) ) + _RampAddwithoutPower ) * smoothstepResult406 );
				float2 temp_cast_0 = (temp_output_412_0).xx;
				Gradient gradient386 = NewGradient( 0, 8, 2, float4( 0.490566, 0.490566, 0.490566, 0 ), float4( 0.4901961, 0.4901961, 0.4901961, 0.2399939 ), float4( 0.6643071, 0.6643071, 0.6643071, 0.2599985 ), float4( 0.6627451, 0.6627451, 0.6627451, 0.4899977 ), float4( 0.8228067, 0.8228067, 0.8228067, 0.5100023 ), float4( 0.8235294, 0.8235294, 0.8235294, 0.7400015 ), float4( 1, 1, 1, 0.7600061 ), float4( 1, 1, 1, 1 ), float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
				float2 temp_cast_1 = (temp_output_412_0).xx;
				#if defined(_SHADOWREMAPTYPE1_USESHADERGARDIENTCOLOR)
				float4 staticSwitch447 = SampleGradient( gradient386, temp_output_412_0 );
				#elif defined(_SHADOWREMAPTYPE1_USESHADOWREMAPMAP)
				float4 staticSwitch447 = tex2D( _ShadowRemapMap, temp_cast_0 );
				#else
				float4 staticSwitch447 = tex2D( _ShadowRemapMap, temp_cast_0 );
				#endif
				float4 RampFinal503 = staticSwitch447;
				float3 temp_cast_2 = (_SpecularSRPLightSmoothstep1.x).xxx;
				float3 temp_cast_3 = (( _SpecularSRPLightSmoothstep1.x + _SpecularSRPLightSmoothstep1.y )).xxx;
				float3 WorldPosition5_g10 = WorldPosition;
				float3 tanNormal12_g10 = Normal337;
				float3 worldNormal12_g10 = float3(dot(tanToWorld0,tanNormal12_g10), dot(tanToWorld1,tanNormal12_g10), dot(tanToWorld2,tanNormal12_g10));
				float3 WorldNormal5_g10 = worldNormal12_g10;
				float3 localAdditionalLightsLambert5_g10 = AdditionalLightsLambert( WorldPosition5_g10 , WorldNormal5_g10 );
				float3 SRPAdditionLight385 = localAdditionalLightsLambert5_g10;
				float3 smoothstepResult441 = smoothstep( temp_cast_2 , temp_cast_3 , ( ( SRPAdditionLight385 * _SpecularSRPLightSampler1 ) / _SpecularSRPLightSampler1 ));
				float3 SRPLightFinal504 = smoothstepResult441;
				float3 bakedGI13_g14 = ASEIndirectDiffuse( IN.lightmapUVOrVertexSH.xy, ase_worldNormal);
				float4 unityObjectToClipPos2_g14 = TransformWorldToHClip(TransformObjectToWorld(IN.ase_texcoord7.xyz));
				float4 computeScreenPos3_g14 = ComputeScreenPos( unityObjectToClipPos2_g14 );
				half2 In017_g14 = ( computeScreenPos3_g14 / computeScreenPos3_g14.w ).xy;
				half localSampleAO17_g14 = SampleAO17_g14( In017_g14 );
				float SF_SSAO18_g14 = localSampleAO17_g14;
				float smoothstepResult8_g14 = smoothstep( _AOThresold , ( _AOThresold + _AOSmoothness ) , SF_SSAO18_g14);
				float4 lerpResult12_g14 = lerp( ( float4( bakedGI13_g14 , 0.0 ) * _AOColour ) , _Color0 , smoothstepResult8_g14);
				float4 lerpResult20_g14 = lerp( _BassColor , ( _BassColor * lerpResult12_g14 ) , _AOMix);
				#ifdef _USESSAO_ON
				float4 staticSwitch23_g14 = lerpResult20_g14;
				#else
				float4 staticSwitch23_g14 = _BassColor;
				#endif
				float2 uv_AlbedoMap1 = IN.ase_texcoord2.xy * _AlbedoMap1_ST.xy + _AlbedoMap1_ST.zw;
				float4 tex2DNode418 = tex2D( _AlbedoMap1, uv_AlbedoMap1 );
				float4 temp_output_438_0 = ( _AlbedoEmissionColor1 * tex2DNode418 );
				float2 uv_IndirectDiffuseLightNormal1 = IN.ase_texcoord2.xy * _IndirectDiffuseLightNormal1_ST.xy + _IndirectDiffuseLightNormal1_ST.zw;
				float3 tanNormal424 = UnpackNormalScale( tex2D( _IndirectDiffuseLightNormal1, uv_IndirectDiffuseLightNormal1 ), 1.0f );
				float3 bakedGI424 = ASEIndirectDiffuse( IN.lightmapUVOrVertexSH.xy, float3(dot(tanToWorld0,tanNormal424), dot(tanToWorld1,tanNormal424), dot(tanToWorld2,tanNormal424)));
				float3 LightAtt411 = temp_output_332_0;
				float4 temp_output_467_0 = ( ( temp_output_438_0 * float4( ( bakedGI424 + _DiffuseLightStrength1 ) , 0.0 ) ) + ( ( temp_output_438_0 * float4( LightAtt411 , 0.0 ) ) * staticSwitch447 ) + ( temp_output_438_0 * float4( SRPAdditionLight385 , 0.0 ) ) );
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - WorldPosition );
				ase_worldViewDir = SafeNormalize( ase_worldViewDir );
				float3 normalizedWorldNormal = normalize( ase_worldNormal );
				float dotResult371 = dot( ase_worldViewDir , normalizedWorldNormal );
				float smoothstepResult430 = smoothstep( _RimSmoothStepMinMax1.x , ( _RimSmoothStepMinMax1.x + _RimSmoothStepMinMax1.y ) , saturate( ( 1.0 - ( dotResult371 + _RimOffset1 ) ) ));
				float4 Rim457 = ( _RimColor1 * smoothstepResult430 * float4( LightAtt411 , 0.0 ) );
				float4 temp_output_475_0 = ( temp_output_467_0 + Rim457 );
				float temp_output_13_0_g13 = _FogHeightEnd;
				float3 temp_cast_14 = (WorldPosition.y).xxx;
				float clampResult11_g13 = clamp( ( ( temp_output_13_0_g13 - distance( temp_cast_14 , float3( 0,0,0 ) ) ) / ( temp_output_13_0_g13 - _FogHeightStart ) ) , 0.0 , 1.0 );
				float temp_output_316_0 = step( WorldPosition.y , _HeightFogWorldPosYClipAdd );
				float temp_output_399_0 = ( ( 1.0 - ( 1.0 - clampResult11_g13 ) ) * temp_output_316_0 );
				float smoothstepResult415 = smoothstep( _NoiseFogClip , ( _NoiseFogClip + _NoiseFogSoft ) , temp_output_399_0);
				float temp_output_434_0 = ( temp_output_399_0 + smoothstepResult415 );
				float simplePerlin3D340 = snoise( ( ( _TimeParameters.x * _NoiseSpeedWorldPos ) + WorldPosition )*_NoiseScale );
				simplePerlin3D340 = simplePerlin3D340*0.5 + 0.5;
				float smoothstepResult367 = smoothstep( _FogNoiseEdgeValue , ( _FogNoiseEdgeValue + _FogNoiseEdgeSmoothness ) , saturate( ( simplePerlin3D340 + _NoiseDensityUseAdd ) ));
				float HeightFogWorldPosYClipStep329 = temp_output_316_0;
				float saferPower393 = max( ( smoothstepResult367 * saturate( pow( ( ( HeightFogWorldPosYClipStep329 + _WorldNoiseYMaskClip1 + WorldPosition.y ) * HeightFogWorldPosYClipStep329 ) , _WorldNoiseYMaskStrength1 ) ) ) , 0.0001 );
				float FogNoise421 = pow( saferPower393 , _NoiseStrengthUsePower );
				float lerpResult461 = lerp( temp_output_434_0 , ( temp_output_434_0 * FogNoise421 ) , _UseFogNoise);
				float HeightFog473 = lerpResult461;
				float4 lerpResult487 = lerp( temp_output_475_0 , _HeightFogColor , HeightFog473);
				#ifdef _USEHEIGHTFOG_ON
				float4 staticSwitch493 = lerpResult487;
				#else
				float4 staticSwitch493 = temp_output_475_0;
				#endif
				float3 tanNormal416 = Normal337;
				float3 worldNormal416 = float3(dot(tanToWorld0,tanNormal416), dot(tanToWorld1,tanNormal416), dot(tanToWorld2,tanNormal416));
				float dotResult436 = dot( ( ase_worldViewDir + _MainLightPosition.xyz ) , worldNormal416 );
				float smoothstepResult460 = smoothstep( _SpecularSmoothStepMinMax1.x , _SpecularSmoothStepMinMax1.y , pow( dotResult436 , _SpecularPow1 ));
				float2 uv_SpecularMap1 = IN.ase_texcoord2.xy * _SpecularMap1_ST.xy + _SpecularMap1_ST.zw;
				float4 Specular483 = ( float4( ( ( LightAtt411 * _SpecularDirLightSRPLightIntensity1.x ) + ( smoothstepResult441 * _SpecularDirLightSRPLightIntensity1.y ) ) , 0.0 ) * ( smoothstepResult460 * _SpecularIntensity1 ) * _SpecularColor1 * tex2D( _SpecularMap1, uv_SpecularMap1 ) );
				float2 uv_EmissionTexture = IN.ase_texcoord2.xy * _EmissionTexture_ST.xy + _EmissionTexture_ST.zw;
				float4 temp_output_516_0 = ( ( staticSwitch493 + Specular483 ) + ( tex2D( _EmissionTexture, uv_EmissionTexture ) * _EmissionColor * _EmissionStrength ) );
				Gradient gradient599 = NewGradient( 0, 4, 2, float4( 1, 0, 0.1166511, 0.7930724 ), float4( 1, 0.7388967, 0, 0.966186 ), float4( 1, 0.7372882, 0, 0.9960021 ), float4( 0, 0, 0, 1 ), 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
				float2 appendResult292 = (float2(_DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.x , _DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.y));
				float2 appendResult293 = (float2(_DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.z , _DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft.w));
				float2 lerpResult288 = lerp( appendResult292 , appendResult293 , _NegateFallDown);
				float2 DIstanceValueandSoft294 = lerpResult288;
				float2 break296 = DIstanceValueandSoft294;
				float3 appendResult64 = (float3(( ( 1.0 - IN.ase_color.r ) - 0.5 ) , ( IN.ase_color.g - 0.5 ) , ( IN.ase_color.b - _HeightControlZOffset )));
				float3 objToWorld180 = mul( GetObjectToWorldMatrix(), float4( appendResult64, 1 ) ).xyz;
				#if defined(_AXIS_X)
				float staticSwitch588 = objToWorld180.x;
				#elif defined(_AXIS_Z)
				float staticSwitch588 = objToWorld180.z;
				#else
				float staticSwitch588 = objToWorld180.x;
				#endif
				float temp_output_185_0 = ( staticSwitch588 + _BridgeFallDownControl );
				float ifLocalVar186 = 0;
				if( temp_output_185_0 > 0.0 )
				ifLocalVar186 = 1.0;
				else if( temp_output_185_0 == 0.0 )
				ifLocalVar186 = 0.0;
				else if( temp_output_185_0 < 0.0 )
				ifLocalVar186 = -1.0;
				float smoothstepResult139 = smoothstep( break296.x , ( break296.x + break296.y ) , ( ifLocalVar186 * pow( temp_output_185_0 , _Power ) ));
				float temp_output_159_0 = saturate( smoothstepResult139 );
				float smoothstepResult589 = smoothstep( _GardientLerp01ColorClip , ( _GardientLerp01ColorClip + _GardientLerp01ColorSoft ) , pow( temp_output_159_0 , _GardientLerp01ColorPower ));
				float4 GardientLerp01Color596 = ( SampleGradient( gradient599, ( smoothstepResult589 + _GardientLerp01ColorAdd ) ) * _GardientLerp01EmissionColor );
				float4 temp_output_597_0 = ( ( ( RampFinal503 * float4( SRPLightFinal504 , 0.0 ) ) + ( staticSwitch23_g14 * temp_output_516_0 ) ) + GardientLerp01Color596 );
				
				float4 screenPos = IN.ase_texcoord8;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float4 ditherCustomScreenPos497 = ase_screenPosNorm;
				float2 clipScreen497 = ditherCustomScreenPos497.xy * _ScreenParams.xy;
				float dither497 = Dither4x4Bayer( fmod(clipScreen497.x, 4), fmod(clipScreen497.y, 4) );
				dither497 = step( dither497, _Alpha );
				float simplePerlin2D482 = snoise( ase_screenPosNorm.xy*5000.0 );
				simplePerlin2D482 = simplePerlin2D482*0.5 + 0.5;
				#ifdef _ONLYUSEXZAXIS_ON
				float staticSwitch470 = distance( (WorldPosition).xz , (_WorldSpaceCameraPos).xz );
				#else
				float staticSwitch470 = distance( WorldPosition , _WorldSpaceCameraPos );
				#endif
				float saferPower479 = max( staticSwitch470 , 0.0001 );
				#ifdef _USECAMERADEPTHCLIP_ON
				float staticSwitch509 = ( dither497 * saturate( ( simplePerlin2D482 + pow( saferPower479 , _CamPosDither ) + _NoiseAlpha ) ) );
				#else
				float staticSwitch509 = dither497;
				#endif
				float3 objToWorld486 = mul( GetObjectToWorldMatrix(), float4( IN.ase_texcoord7.xyz, 1 ) ).xyz;
				float temp_output_490_0 = distance( _PlayerMaskPosition1 , objToWorld486 );
				float temp_output_501_0 = pow( temp_output_490_0 , _Float5 );
				float smoothstepResult510 = smoothstep( _BlackClipValue , ( _BlackClipValue + _BlackClipSmoothness ) , temp_output_501_0);
				float lerpResult521 = lerp( staticSwitch509 , ( simplePerlin2D482 * _PlayerPositionMaskAlpha ) , smoothstepResult510);
				#ifdef _PLAYERMASKOPEN_ON
				float staticSwitch559 = lerpResult521;
				#else
				float staticSwitch559 = staticSwitch509;
				#endif
				
				#ifdef _SHADOWUSEDITHER1_ON
				float staticSwitch545 = 0.001;
				#else
				float staticSwitch545 = 0.0;
				#endif
				
				float3 BakedAlbedo = temp_output_597_0.rgb;
				float3 BakedEmission = 0;
				float Alpha = staticSwitch559;
				float AlphaClipThreshold = staticSwitch545;

				#ifdef _ALPHATEST_ON
					clip(Alpha - AlphaClipThreshold);
				#endif

				MetaInput metaInput = (MetaInput)0;
				metaInput.Albedo = BakedAlbedo;
				metaInput.Emission = BakedEmission;
				
				return MetaFragment(metaInput);
			}
			ENDHLSL
		}
		
	}
	CustomEditor "UnityEditor.ShaderGraph.PBRMasterGUI"
	Fallback "Hidden/InternalErrorShader"
	
}
/*ASEBEGIN
Version=18900
1536;0;1280;909;2100.188;-1137.589;1;True;False
Node;AmplifyShaderEditor.CommentaryNode;308;-4181.955,5701.667;Inherit;False;2089.905;846.2246;Comment;14;575;434;415;399;396;388;387;380;372;358;357;316;315;311;FogHeight;0.3622641,0.7043549,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;312;-4167.15,7051.358;Inherit;False;1897.938;1035.197;Comment;25;421;393;392;391;369;367;360;353;352;351;350;348;346;345;342;341;340;336;335;333;330;327;324;318;317;Fog 3D Noise Setting;1,0.390566,0.390566,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;313;-8685.789,4568.443;Inherit;False;Property;_LightAttValue1;Light Att Value(光衰減程度);30;1;[Header];Create;False;1;Light Attenuation;0;0;False;0;False;-0.3;-0.23;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;309;-9825.867,3781.01;Inherit;False;1169.146;309.3865;Comment;3;337;326;321;Normal;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldPosInputsNode;315;-4127.038,6043.842;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;311;-3295.518,6525.318;Inherit;False;Property;_HeightFogWorldPosYClipAdd;HeightFog WorldPos Y Clip Add;68;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;314;-8683.789,4649.443;Inherit;False;Property;_LightAttSoft1;Light Att Soft(光衰減平滑度);31;0;Create;False;0;0;0;False;0;False;0.46;0.36;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;323;-8251.73,4812.155;Inherit;False;Property;_DirLightIntensity1;DirLight Intensity(日光強度);7;1;[Header];Create;False;1;Light;0;0;False;0;False;1;1.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LightColorNode;322;-8226.878,4684.981;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.CommentaryNode;310;-8119.88,4557.287;Inherit;False;506.8718;338.4612;Comment;3;332;328;325;Light Color & Attenuation;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;321;-9775.867,3876.971;Inherit;False;Property;_NormalStrength1;Normal Strength(法線強度);3;0;Create;False;0;0;0;True;0;False;0;2;0;5;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;317;-4022.456,7231.938;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;318;-4108.478,7320.117;Inherit;False;Property;_NoiseSpeedWorldPos;Noise Speed(World Pos);74;0;Create;True;1;Fog Noise Setting;0;0;False;0;False;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.StepOpNode;316;-2902.242,6431.101;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;320;-8426.1,4652.064;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightAttenuation;319;-8351.348,4556.633;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;326;-9440.829,3831.009;Inherit;True;Property;_NormalMap1;Normal Map(法線貼圖);2;2;[Header];[Normal];Create;False;1;Normal;0;0;True;0;False;-1;None;87dea5061a42b6b4eb7646762011dc53;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;325;-7991.306,4709.467;Inherit;False;3;3;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;329;-2639.945,6579.769;Inherit;False;HeightFogWorldPosYClipStep;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;324;-3735.456,7300.938;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldPosInputsNode;327;-4117.15,7488.681;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SmoothstepOpNode;328;-8052.686,4574.327;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;335;-3705.611,7125.533;Inherit;False;Property;_NoiseScale;Noise Scale;75;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;334;-7554.25,4866.391;Inherit;False;741.3994;303.9028;Color Remap模塊中自有陰影重映射的Gardient,因此若不先漸變為灰度圖,重映射會不正確;3;362;344;339;轉換為灰度圖;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleAddOpNode;333;-3546.456,7463.938;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;332;-7848.407,4642.348;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;337;-8881.521,3831.996;Inherit;True;Normal;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;331;-7800.19,3792.85;Inherit;False;1070.12;398.215;Comment;7;390;376;359;356;349;347;338;Normal Light(N.L);1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;330;-3916.798,7619.225;Inherit;False;329;HeightFogWorldPosYClipStep;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;336;-3870.185,7811.103;Inherit;False;Property;_WorldNoiseYMaskClip1;世界霧Y軸噪聲衰減極值;80;0;Create;False;0;0;0;False;0;False;-0.59;-0.59;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;66;-4393.334,214.04;Inherit;False;806.2585;377.5137;校正:從MAX做好的模型，頂點色不一定與unity的vertex color一致,所以必須做顏色翻轉,顏色閾值校正(遊戲技術特效課3-11,12);7;60;64;62;61;65;63;70;Model Vertex Color Correct;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;338;-7750.19,3842.85;Inherit;False;337;Normal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;290;-4170.402,1637.334;Inherit;False;Property;_NegateFallDown;_NegateFallDown;88;0;Create;False;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;339;-7505.924,4915.577;Inherit;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.NoiseGeneratorNode;340;-3410.229,7101.358;Inherit;False;Simplex3D;True;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;60;-4370.31,263.8863;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;341;-3590.093,7711.515;Inherit;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;298;-4045.63,1192.862;Inherit;False;Constant;_Float3;Float 3;23;0;Create;True;0;0;0;False;0;False;25;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;342;-3455.589,7206.28;Inherit;False;Property;_NoiseDensityUseAdd;Noise Density(Use Add);76;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;297;-4030.372,1093.097;Inherit;False;Constant;_Float2;Float 2;23;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;344;-7297.927,4915.577;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;346;-3358.091,7711.485;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;70;-4307.064,504.2219;Inherit;False;Property;_HeightControlZOffset;HeightControl(Z Offset);92;0;Create;True;1;;0;0;False;0;False;0.5;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;183;-4005.477,863.9001;Inherit;False;Property;_BridgeFallDownControl;_BridgeFallDownControl;87;1;[Header];Create;True;1;Vertex Offset Animation Control;0;0;False;0;False;0;40.9;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;350;-3361.777,7390.752;Inherit;False;Property;_FogNoiseEdgeSmoothness;Fog Noise Edge Smoothness;79;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;300;-3830.795,1143.571;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;351;-3224.442,7106.994;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;343;-7717.689,5753.437;Inherit;False;1581.267;646.9999;;13;443;433;432;430;420;413;401;398;378;374;371;361;354;Rim ;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;345;-3322.85,7312.824;Inherit;False;Property;_FogNoiseEdgeValue;Fog Noise Edge Value;78;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;348;-3598.807,7946.795;Inherit;False;Property;_WorldNoiseYMaskStrength1;世界霧Y軸噪聲衰減強度;81;0;Create;False;0;0;0;False;0;False;-0.59;-0.59;0;1000;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;347;-7451.092,3849.344;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;349;-7483.015,4010.466;Inherit;False;True;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.OneMinusNode;61;-4100.95,286.9151;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;63;-3922.582,386.4697;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;353;-3089.415,7104.883;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;361;-7667.689,6073.173;Inherit;False;True;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;358;-3849.626,6141.44;Inherit;False;Property;_FogHeightEnd;Fog Height End;66;1;[Header];Create;True;1;Height Fog Setting;0;0;False;0;False;4;4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;65;-4035.66,485.5943;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;62;-3924.518,286.7121;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector4Node;287;-4396.018,1380.151;Inherit;False;Property;_DistanceValueDistanceSoftNegateDistanceValueNegateDistanceSoft;Distance Value/Distance Soft/Negate DistanceValue/Negate Distance Soft;90;0;Create;True;0;0;0;False;0;False;0,0,0,0;-4.7,11.1,17.5,-11.5;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DotProductOpNode;359;-7219.092,3918.344;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;356;-7256.708,3830.351;Inherit;False;Property;_DirLightNormalScale1;DirLightNormalScale(N.L強度);8;0;Create;False;0;0;0;True;0;False;0.6;0.6;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;299;-3668.87,1095.444;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;355;-9850.911,4239.814;Inherit;False;740.6223;260.14;Comment;2;385;368;SRP Light;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;363;-7505.222,4303.501;Inherit;False;835.4715;413.2869;Comment;5;406;389;381;377;370;Celluloid Texture;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;362;-7048.255,4916.894;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;352;-3080.831,7345.466;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;357;-3679.442,6457.15;Inherit;False;Property;_FogHeightStart;Fog Height Start;67;0;Create;True;1;HeightFog Setting;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;360;-3146.153,7711.516;Inherit;True;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;354;-7645.725,5899.846;Inherit;False;World;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.CommentaryNode;366;-6709.516,3792.265;Inherit;False;906.9587;494.713;;6;447;423;422;412;394;384;Shadow Remap;0.6320754,0.6320754,0.6320754,1;0;0
Node;AmplifyShaderEditor.DynamicAppendNode;293;-4026.402,1479.334;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;376;-6997.47,3918.458;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0.5;False;2;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;365;-6879.99,3639.33;Inherit;False;Property;_RampAddincludePower;Ramp Add(include Power);13;0;Create;True;0;0;0;False;0;False;0;-0.19;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;64;-3739.875,402.9479;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;369;-2897.379,7711.855;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;372;-3345.121,6146.958;Inherit;False;Linear Fog;-1;;13;e327318b664bb6b4ca7fb9865de9053d;1,14,1;5;13;FLOAT;700;False;15;FLOAT;0;False;16;FLOAT3;0,0,0;False;17;FLOAT3;0,0,0;False;18;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;374;-7427.075,6271.118;Inherit;False;Property;_RimOffset1;Rim Offset(泛光量值);19;1;[Header];Create;False;1;Rim;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;364;-6910.996,3720.166;Inherit;False;Property;_RampPower;Ramp Power;15;0;Create;True;0;0;0;False;0;False;1;0.4;0;3;0;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;367;-2926.809,7105.195;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;276;-3513.055,1074.939;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DotProductOpNode;371;-7428.554,5985.607;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;368;-9573.449,4388.336;Inherit;False;SRP Additional Light;-1;;10;6c86746ad131a0a408ca599df5f40861;3,6,1,9,0,23,0;5;2;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;15;FLOAT3;0,0,0;False;14;FLOAT3;1,1,1;False;18;FLOAT;0.5;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;375;-6750.42,4914.359;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;370;-7455.222,4378.438;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;2,2;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;373;-7412.405,1858.798;Inherit;False;1707.054;658.7182;;2;400;386;Ramp Gradient;1,0.9619094,0,1;0;0
Node;AmplifyShaderEditor.DynamicAppendNode;292;-4026.402,1383.334;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TransformPositionNode;180;-3746.46,677.6484;Inherit;False;Object;World;False;Fast;True;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;387;-2968.012,6050.908;Inherit;False;Property;_NoiseFogClip;NoiseFog Clip;69;0;Create;True;1;Noise Fog Setting;0;0;False;0;False;0.76;0.76;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;390;-6732.491,3920.652;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;378;-7159.963,5985.437;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;288;-3831.88,1417.601;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TransformPositionNode;273;-3290.992,476.2236;Inherit;False;World;Object;False;Fast;True;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;377;-7432.021,4601.388;Inherit;False;Property;_RampCelluloidNoiseSoftness1;Ramp Celluloid Noise Softness(賽璐璐陰影平滑度);34;0;Create;False;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;380;-2959.272,6126.661;Inherit;False;Property;_NoiseFogSoft;NoiseFog Soft;70;0;Create;True;0;0;0;False;0;False;0.4;0.4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;381;-7433.213,4533.152;Inherit;False;Property;_RampCelluloidNoiseValue1;Ramp Celluloid Noise Value(賽璐璐陰影強度);33;0;Create;False;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;384;-6692.412,4149.553;Inherit;False;Property;_RampAddwithoutPower;Ramp Add(without Power);14;0;Create;True;0;0;0;False;0;False;0;-0.67;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;392;-2736.02,7102.498;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GradientNode;386;-6832.483,2153.661;Inherit;False;0;8;2;0.490566,0.490566,0.490566,0;0.4901961,0.4901961,0.4901961,0.2399939;0.6643071,0.6643071,0.6643071,0.2599985;0.6627451,0.6627451,0.6627451,0.4899977;0.8228067,0.8228067,0.8228067,0.5100023;0.8235294,0.8235294,0.8235294,0.7400015;1,1,1,0.7600061;1,1,1,1;1,0;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.RangedFloatNode;391;-2914.228,7522.972;Inherit;False;Property;_NoiseStrengthUsePower;Noise Strength(Use Power);77;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;382;-6603.155,3712.415;Inherit;False;True;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;379;-7996.195,6987.77;Inherit;False;2880.684;912.0061;Comment;17;565;483;481;474;469;465;460;455;451;450;436;427;419;416;405;397;395;Specular;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;383;-8108,6580.976;Inherit;False;927.4;304.3999;Comment;7;441;439;425;417;407;404;402;Specular SRPLight 光源漸變採樣計算;1,1,1,1;0;0
Node;AmplifyShaderEditor.OneMinusNode;388;-2921.713,6214.227;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;385;-9351.89,4289.814;Inherit;False;SRPAdditionLight;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;389;-7167.852,4353.501;Inherit;True;Property;_RampCelluloidNoiseTexture1;Ramp Celluloid Noise Texture(賽璐璐陰影邊緣噪波貼圖);32;1;[Header];Create;False;1;Celluloid Edge Noise Texture;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;399;-2721.448,6209.983;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;588;-3482.039,708.3353;Inherit;False;Property;_Axis;Axis;91;0;Create;True;0;0;0;False;0;False;0;0;0;True;;KeywordEnum;2;X;Z;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;402;-8011.612,6625.81;Inherit;False;385;SRPAdditionLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.Vector2Node;398;-6976.955,6233.934;Inherit;False;Property;_RimSmoothStepMinMax1;Rim SmoothStepValue and Smoothness(泛光平滑閾值);21;0;Create;False;0;0;0;True;0;False;0,1;0,1;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.PowerNode;393;-2604.089,7102.299;Inherit;False;True;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;405;-7867.41,7037.77;Inherit;False;World;True;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;189;-3302.631,1078.868;Inherit;False;Constant;_Float1;Float 1;18;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;294;-3593.626,1412.775;Inherit;False;DIstanceValueandSoft;-1;True;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SmoothstepOpNode;406;-6858.55,4382.663;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;395;-7946.195,7420.304;Inherit;False;337;Normal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;396;-2771.345,6106.266;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;275;-3107.179,-41.84255;Inherit;False;Property;_RotatePower;Rotate Power;94;1;[Header];Create;True;1;Rotate Setting;0;0;False;0;False;1;-16.9;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;403;-6790.713,2937.093;Inherit;False;546.8332;461.799;Comment;3;438;418;409;Albedo;1,1,1,1;0;0
Node;AmplifyShaderEditor.WorldSpaceLightPos;397;-7924.683,7216.687;Inherit;False;0;3;FLOAT4;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.RangedFloatNode;404;-8067,6757.976;Inherit;False;Property;_SpecularSRPLightSampler1;Specular SRPLight Sampler(高光燈光採樣次數);27;1;[Header];Create;False;1;SRP Addition Lighting Setting;0;0;False;0;False;5;5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;400;-6561.18,2148.645;Inherit;False;RampGradient;-1;True;1;0;OBJECT;;False;1;OBJECT;0
Node;AmplifyShaderEditor.DistanceOpNode;11;-3031.51,238.7903;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;401;-6918.306,5985.2;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;394;-6529.054,3896.595;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;185;-3237.917,702.5918;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;136;-2927.609,726.2072;Inherit;False;Property;_Power;Power;89;0;Create;True;0;0;0;False;0;False;1;0.55;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;188;-3298.358,980.9669;Inherit;False;Constant;_Float0;Float 0;18;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;190;-3161.803,1157.602;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;295;-2850.74,562.542;Inherit;False;294;DIstanceValueandSoft;1;0;OBJECT;;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;412;-6408.438,3897.175;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.31;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;414;-7142.163,3546.994;Inherit;True;Property;_IndirectDiffuseLightNormal1;Indirect Diffuse Light Normal(漫射光法線貼圖);10;0;Create;False;0;0;0;False;0;False;-1;None;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;421;-2439.919,7097.398;Inherit;False;FogNoise;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;408;-6627.19,3510.544;Inherit;False;691.1977;124;間接漫射光從Unity的全局照明系統獲取漫射的環境光。這相當於說它檢索周圍的光探測器的信息。;2;440;424;漫射光照;1,1,1,1;0;0
Node;AmplifyShaderEditor.Vector2Node;417;-7756.744,6730.135;Inherit;False;Property;_SpecularSRPLightSmoothstep1;Specular SRPLight Smoothstep(高光燈光平滑閾值);28;0;Create;False;0;0;0;False;0;False;0,1;9.5,-114;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SmoothstepOpNode;415;-2543.132,6032.584;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;419;-7636.683,7142.687;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;413;-6720.605,5987.022;Inherit;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;277;-2877.614,39.23443;Inherit;False;Property;_rotateRange;rotate Range;96;0;Create;True;0;0;0;False;0;False;0;-1.45;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldNormalVector;416;-7694.683,7424.687;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.PowerNode;274;-2910.404,-87.77957;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;407;-7763.582,6631.064;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SamplerNode;418;-6740.713,3168.892;Inherit;True;Property;_AlbedoMap1;Albedo Map(主貼圖);0;1;[Header];Create;False;1;Albedo;0;0;True;0;False;-1;None;84def540501466348b725485f8c135b2;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;411;-7796.203,4939.037;Inherit;False;LightAtt;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;409;-6735.212,2991.571;Inherit;False;Property;_AlbedoEmissionColor1;Albedo/Emission  Color(主貼圖/高光顏色);1;2;[HDR];[Header];Create;False;0;0;0;False;0;False;0.3301887,0.3301887,0.3301887,0;0.509434,0.509434,0.509434,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;410;-6720.75,3831.539;Inherit;False;400;RampGradient;1;0;OBJECT;;False;1;OBJECT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;420;-6662.444,6260.648;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;434;-2215.384,5856.918;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;296;-2581.683,567.9515;Inherit;False;FLOAT2;1;0;FLOAT2;0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.PowerNode;135;-2703.66,378.1538;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GradientSampleNode;423;-6247.669,3834.68;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SmoothstepOpNode;430;-6557.725,5987.6;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;428;-6618.351,3643.308;Inherit;False;Property;_DiffuseLightStrength1;DiffuseLight Strength(環境光強度(疊加);9;0;Create;False;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;436;-7409.195,7272.304;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;433;-6531.786,5803.437;Inherit;False;Property;_RimColor1;Rim Color(泛光顏色);20;1;[HDR];Create;False;0;0;0;False;0;False;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ConditionalIfNode;186;-2971.888,919.6718;Inherit;False;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;258;-3523.801,-683.1377;Inherit;False;1242.024;452.3877;最後減去自身vertex position原因為Rotate About Axis的Bug;7;270;269;268;266;265;261;260;Rotate Caculate;1,1,1,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;432;-6715.381,6399.367;Inherit;False;411;LightAtt;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;426;-2045.34,5997.207;Inherit;False;421;FogNoise;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;425;-7447.819,6758.746;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;435;-6984.088,6494.217;Inherit;False;596.8665;382.1125;Comment;3;458;453;442;日光/燈光強度調整;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;438;-6406.281,3125.592;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;427;-7410.195,7392.304;Inherit;False;Property;_SpecularPow1;Specular Pow(高光量值);23;0;Create;False;0;0;0;False;0;False;4;4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;422;-6236.001,4047.946;Inherit;True;Property;_ShadowRemapMap;Shadow Remap Map;11;1;[Header];Create;True;1;Shadow Type;0;0;False;0;False;-1;None;5c17639b73e66a64eb1bf87957b3160a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleDivideOpNode;439;-7505.97,6630.976;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.VertexColorNode;147;-3995.729,-135.5786;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;429;-6038.59,3645.932;Inherit;False;411;LightAtt;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.IndirectDiffuseLighting;424;-6594.19,3554.544;Inherit;False;Tangent;1;0;FLOAT3;0,0,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;278;-2686.709,-98.21716;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;450;-7211.195,7274.304;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;437;-3824.376,3966.797;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;445;-1819.028,5890.424;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;162;-2593.947,813.431;Inherit;False;Property;_Lerp0Scale;Lerp0 Scale;83;0;Create;True;0;0;0;False;0;False;5;-18.08;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;452;-5778.553,3600.353;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;444;-1574.813,5767.254;Inherit;False;Property;_UseFogNoise;Use Fog Noise;73;2;[Header];[Enum];Create;True;1;Fog Noise Setting;2;Off;0;On;1;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;191;-2539.829,376.1671;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;142;-2427.94,568.1376;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;441;-7316.883,6630.618;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;1,1,1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.Vector2Node;442;-6934.088,6544.217;Inherit;False;Property;_SpecularDirLightSRPLightIntensity1;Specular DirLight/SRPLight Intensity(高光受光影響程度 主光源/SRP光源));29;0;Create;False;0;0;0;False;0;False;1,1;1,1.87;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.OneMinusNode;148;-3766.367,-109.5496;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldSpaceCameraPos;431;-3711.076,4422.36;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SaturateNode;279;-2406.175,-147.4977;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;446;-6528.657,3414.931;Inherit;False;385;SRPAdditionLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;447;-5814.158,3950.83;Inherit;True;Property;_ShadowRemapType1;Shadow Remap Type(陰影重映射方式)(左為背光右為迎光);12;0;Create;False;0;0;0;True;0;False;0;1;1;True;;KeywordEnum;2;UseShaderGardientColor;UseShadowRemapMap;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.Vector2Node;451;-7406.111,7472.649;Inherit;False;Property;_SpecularSmoothStepMinMax1;Specular SmoothStep Value & Smoothness(高光值與平滑度);25;0;Create;False;0;0;0;False;0;False;0,0.01;0,0.01;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;443;-6298.821,5965.916;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;260;-3473.801,-502.6302;Inherit;False;Property;_RotateAngleStrength;RotateAngleStrength;97;0;Create;True;0;0;0;False;0;False;1;29.7;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;440;-6322.249,3565.088;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;458;-6549.623,6549.961;Inherit;True;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;176;-2667.653,1148.969;Inherit;False;Property;_Lerp0ZScale;Lerp0 Z Scale;85;0;Create;True;0;0;0;False;0;False;1;1.35;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;456;-6207.777,3396.681;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;150;-3589.935,-109.7526;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;459;-5397.218,3776.443;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;457;-6066.261,5963.159;Inherit;False;Rim;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SmoothstepOpNode;139;-2386.94,370.1377;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;149;-3583.076,90.12946;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;453;-6665.567,6741.529;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;461;-1308.604,5852.759;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;265;-3195.034,-497.4289;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;449;-3432.445,4339.104;Inherit;False;FLOAT2;0;2;2;3;1;0;FLOAT3;0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;138;-2150.789,791.4839;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;151;-3587.999,-9.995163;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;448;-3434.727,4415.819;Inherit;False;FLOAT2;0;2;2;3;1;0;FLOAT3;0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SmoothstepOpNode;460;-6993.111,7274.649;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;463;-6013.458,3133.201;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;455;-6989.2,7404.096;Inherit;False;Property;_SpecularIntensity1;Specular Intensity(高光強度);26;0;Create;False;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector3Node;69;-2744.394,902.9034;Inherit;False;Property;_Lerp0XYZOffset;Lerp0 XYZ Offset;82;1;[Header];Create;True;1;Lerp0(In Cloud) Setting;0;0;False;0;False;0,0,0;0,0,30;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SaturateNode;159;-1844.342,459.0217;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;474;-6605.2,7275.096;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;254;-1999.724,879.4297;Inherit;False;FLOAT2;0;2;2;3;1;0;FLOAT3;0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;591;-1575.369,1131.786;Inherit;False;Property;_GardientLerp01ColorSoft;Gardient Lerp01 Color Soft;100;0;Create;True;0;0;0;False;0;False;1;4.9;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;602;-1643.132,898.8297;Inherit;False;Property;_GardientLerp01ColorPower;Gardient Lerp01 Color Power;98;1;[Header];Create;True;1;Gardient Lerp01 Color Setting;0;0;False;0;False;0;112.57;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;465;-6632.397,7491.112;Inherit;False;Property;_SpecularColor1;Specular Color(高光顏色);24;1;[HDR];Create;False;0;0;0;True;0;False;1,1,1,0;1,1,1,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;152;-3413.292,-34.51707;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PowerNode;175;-2466.697,1130.471;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;464;-4990.197,3718.505;Inherit;False;457;Rim;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;473;-1068.833,5847.559;Inherit;False;HeightFog;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;251;-2215.707,701.9019;Inherit;False;Property;_Lerp0YScale;Lerp0 Y Scale;84;0;Create;True;0;0;0;False;0;False;1;-1.82;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;266;-3056.166,-422.3791;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PiNode;268;-3059.166,-497.3791;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;469;-6692.91,7666.117;Inherit;True;Property;_SpecularMap1;Specular Map(高光貼圖);22;1;[Header];Create;False;1;Specular;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;472;-6261.94,6720.995;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;590;-1576.369,1044.786;Inherit;False;Property;_GardientLerp01ColorClip;Gardient Lerp01 Color Clip;99;0;Create;True;1;Bridge Animation Gradient Color Setting;0;0;False;0;False;0;-1.83;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;252;-2014.077,786.1115;Inherit;False;FLOAT;1;1;2;3;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;462;-3422.257,4083.227;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;454;-3269.8,4363.941;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;467;-5264.186,3540.949;Inherit;True;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;478;-4995.648,3135.719;Inherit;False;473;HeightFog;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;468;-2371.819,4483.538;Inherit;False;1565.582;646.4756;Comment;18;586;582;574;572;570;569;566;534;510;505;501;500;492;491;490;486;485;476;PlayerMask;1,1,1,1;0;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;253;-1886.484,741.7446;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;282;-1975.66,109.5012;Inherit;False;Property;_RotateLerpPower;Rotate Lerp Power;95;0;Create;True;0;0;0;False;0;False;1;30;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;601;-1374.132,821.8297;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;475;-4836.941,3541.334;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;481;-5804.583,7255.056;Inherit;True;4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;466;-3194.659,2759.492;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;477;-4997.137,3307.483;Inherit;False;Property;_HeightFogColor;HeightFogColor;71;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,0;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RotateAboutAxisNode;269;-2783.897,-633.1378;Inherit;False;True;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.BreakToComponentsNode;256;-1861.058,851.9897;Inherit;False;FLOAT2;1;0;FLOAT2;0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.RangedFloatNode;471;-3154.812,4506.671;Inherit;False;Property;_CamPosDither;CamPos Dither;53;1;[Header];Create;True;1;Camera Dither Setting;0;0;False;0;False;-6;-6;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;177;-2319.359,1120.742;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;470;-3142.99,4200.845;Inherit;False;Property;_OnlyUseXZAxis;Only Use XZ Axis;57;0;Create;True;0;0;0;False;0;False;0;1;1;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;592;-1242.369,1001.786;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;487;-4700.261,3291.137;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.PowerNode;479;-2898.382,4206.579;Inherit;False;True;2;0;FLOAT;0;False;1;FLOAT;-1.46;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;604;-1332.219,1423.56;Inherit;False;Property;_GardientLerp01ColorAdd;Gardient Lerp01 Color Add;101;0;Create;True;0;0;0;False;0;False;0;0.385;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;589;-1162.566,833.4516;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;483;-5475.534,7251.229;Inherit;False;Specular;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;161;-2631.078,125.0722;Inherit;False;Property;_Lerp1Scale;Lerp1 Scale;86;1;[Header];Create;True;1;Lerp1(CloudTop Setting);0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;270;-2447.376,-514.6847;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;480;-2808.785,4093.863;Inherit;False;Property;_NoiseAlpha;Noise Alpha;55;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;482;-2999.805,2759.112;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;5000;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;255;-1732.698,783.4397;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PosVertexDataNode;476;-2321.819,4949.414;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;174;-2123.257,930.3459;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PowerNode;281;-1720.778,190.7791;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;495;-4394.313,3795.107;Inherit;False;483;Specular;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;603;-1073.446,1335.342;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;280;-1488.133,80.081;Inherit;True;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;489;-3081.108,3705.801;Inherit;False;Property;_Alpha;Alpha;45;1;[Header];Create;True;1;Alpha Setting;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;493;-4423.671,3310.452;Inherit;False;Property;_UseHeightFog;Use HeightFog ?;72;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;496;-4377.369,4384.631;Inherit;False;Property;_EmissionStrength;Emission Strength;6;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TransformPositionNode;486;-2117.774,4942.931;Inherit;False;Object;World;False;Fast;True;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SamplerNode;494;-4476.398,3981.591;Inherit;True;Property;_EmissionTexture;Emission Texture;4;1;[Header];Create;True;1;Emission;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector3Node;485;-2139.127,4533.539;Inherit;False;Property;_PlayerMaskPosition1;PlayerMaskPosition;49;0;Create;False;0;0;0;False;0;False;0,0,0;0,0,0;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;160;-2299.53,87.22184;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;484;-2989.547,3863.294;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;488;-2404.132,3953.151;Inherit;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;156;-1606.586,802.6589;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;498;-4389.824,4211.013;Inherit;False;Property;_EmissionColor;Emission Color;5;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,1;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GradientNode;599;-1292.144,1550.677;Inherit;False;0;4;2;1,0,0.1166511,0.7930724;1,0.7388967,0,0.966186;1,0.7372882,0,0.9960021;0,0,0,1;1,0;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.SaturateNode;499;-2192.331,3973.628;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;285;-1085.573,165.1446;Inherit;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DitheringNode;497;-2773.831,3801.606;Inherit;False;0;True;4;0;FLOAT;0;False;1;SAMPLER2D;;False;2;FLOAT4;0,0,0,0;False;3;SAMPLERSTATE;;False;1;FLOAT;0
Node;AmplifyShaderEditor.DistanceOpNode;490;-1893.153,4650.542;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;502;-4176.068,3665.6;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;504;-7111.661,6842.929;Inherit;False;SRPLightFinal;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;492;-1751.72,4734.239;Inherit;False;Property;_Float5;Float 5;50;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;500;-1530.62,4917.595;Inherit;False;Property;_BlackClipSmoothness;Black Clip Smoothness;52;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GradientSampleNode;600;-863.6995,1459.518;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.LerpOp;143;-1554.997,319.166;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;594;-1415.369,1207.786;Inherit;False;Property;_GardientLerp01EmissionColor;Gardient Lerp01 Emission Color;102;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,0;0.3867925,0.3867925,0.3867925,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;503;-5338.372,4130.808;Inherit;False;RampFinal;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;507;-4106.292,4156.904;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;491;-1486.45,4841.302;Inherit;False;Property;_BlackClipValue;Black Clip Value;51;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;514;-2076.3,3492.576;Inherit;False;SSAO_F;58;;14;146f1dca6fd359a4eba9e48143697ea7;0;0;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;506;-2055.271,3807.936;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;505;-1305.756,4821.225;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;516;-3935.498,3666.282;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;595;-818.6604,1043.938;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;508;-1953.27,3975.257;Inherit;False;Property;_PlayerPositionMaskAlpha;PlayerPositionMask Alpha;47;1;[Header];Create;True;1;PlayerPosition Alpha;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;501;-1594.449,4649.329;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;283;-1236.963,374.2718;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;513;-1808.491,3280.672;Inherit;False;504;SRPLightFinal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;512;-1803.197,3134.347;Inherit;False;503;RampFinal;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;511;-1534.69,3941.181;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;510;-995.0361,4537.228;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;519;-1560.194,3264.985;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;517;-1749.799,3492.576;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;515;-5957.429,4397.969;Inherit;False;1417.653;1005.605;Comment;6;578;535;531;530;525;523;Outline;1,1,1,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;596;-501.9209,1025.941;Inherit;False;GardientLerp01Color;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;509;-1908.167,3745.48;Inherit;False;Property;_UseCameraDepthClip;Use Camera Depth Clip ?;56;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;137;-1027.609,588.1409;Inherit;False;Property;_OffsetEffectOpen;Offset Effect Open;93;0;Create;True;0;0;0;False;0;False;0;0;1;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GetLocalVarNode;598;-1482.396,3691.046;Inherit;False;596;GardientLerp01Color;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.CommentaryNode;523;-5880.421,4977.317;Inherit;False;629.5074;380.8989;;4;587;554;547;543;頂點位置(物件空間轉裁減空間);1,1,1,1;0;0
Node;AmplifyShaderEditor.LerpOp;521;-1075.477,3867.896;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;301;-671.7706,740.3311;Inherit;False;VertexOffsetOutput;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;522;-7552.271,5264.269;Inherit;False;743.0731;304.7168;Comment;3;571;562;542;轉換為灰度圖;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;520;-2768.91,3390.743;Inherit;False;Constant;_Clip1;Clip(為0時陰影永存);47;0;Create;False;0;0;0;False;0;False;0.001;0.001;0;0.001;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;529;-1383.354,3394.974;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;518;-2645.416,3568.231;Inherit;False;Constant;_Float4;Float 4;47;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;537;-2969.577,3263.642;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;574;-1883.848,4539.744;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;546;-4680.395,3183.221;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SmoothstepOpNode;579;-3791.831,2593.877;Inherit;True;3;0;FLOAT;0;False;1;FLOAT;0.2;False;2;FLOAT;0.25;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;573;-3014.991,3105.117;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;568;-2214.296,3611.177;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;542;-7044.599,5317.586;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;575;-2344.621,6420.035;Inherit;False;FogTest;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;543;-5753.105,5221.826;Inherit;False;Property;_OutlineMax1;Outline Max外描邊最大閾值(Accroding to Camera Distance)(根據照相機遠近) ;17;0;Create;False;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;552;-4995.858,2826.107;Inherit;False;Property;_DissolveMode;Dissolve Mode;44;0;Create;True;0;0;0;False;0;False;0;0;0;True;;KeywordEnum;4;UV;WorldSpace;ScreenSpace;ViewSpace;Create;True;True;9;1;FLOAT4;0,0,0,0;False;0;FLOAT4;0,0,0,0;False;2;FLOAT4;0,0,0,0;False;3;FLOAT4;0,0,0,0;False;4;FLOAT4;0,0,0,0;False;5;FLOAT4;0,0,0,0;False;6;FLOAT4;0,0,0,0;False;7;FLOAT4;0,0,0,0;False;8;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;564;-5247.275,2723.18;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.BreakToComponentsNode;571;-7502.27,5316.269;Inherit;False;FLOAT3;1;0;FLOAT3;0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.ScreenPosInputsNode;544;-5507.857,3076.108;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;582;-1209.052,4538.795;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;-1;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;557;-5483.287,3251.883;Inherit;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;540;-4857.091,3006.611;Inherit;False;Property;_ClipNoiseScale1;ClipNoiseScale(死亡溶解噪點紋理大小);41;0;Create;False;0;0;0;False;0;False;10;10;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;597;-1183.808,3554.032;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.DotProductOpNode;586;-1451.364,4538.276;Inherit;True;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;581;-4193.785,2600.023;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PosVertexDataNode;547;-5851.226,5033.749;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;583;-3528.966,2815.387;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;578;-5273.1,4721.281;Inherit;True;4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ScreenPosInputsNode;585;-1861.709,4132.75;Float;False;0;False;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;548;-2539.721,2760.078;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;545;-2473.456,3520.853;Inherit;False;Property;_ShadowUseDither1;Shadow Use Dither(陰影隨溶解消失);46;0;Create;False;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;556;-2825.107,3105.566;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;1;False;4;FLOAT;-1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;569;-1759.153,4650.542;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;577;-5739.517,2741.653;Inherit;False;Property;_ClipNoiseTiling1;ClipNoiseTiling(死亡溶解噪點紋理拉伸);40;0;Create;False;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;580;-4331.416,2877.99;Inherit;False;Property;_ClipEdge1;ClipEdge(死亡溶解邊緣顏色範圍);38;0;Create;False;0;0;0;False;0;False;0.99;0.99;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NormalVertexDataNode;535;-5493.463,4648.942;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;302;-448.2476,3487.84;Inherit;False;301;VertexOffsetOutput;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;567;-3367.08,3914.724;Inherit;False;Property;_CamSub;CamSub;54;0;Create;True;0;0;0;False;0;False;0.5;-6;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;550;-3396.258,3559.162;Inherit;False;Property;_Sprint1;_Sprint(衝刺);43;0;Create;False;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;587;-5428.844,5123.122;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;562;-7294.27,5316.269;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;566;-1670.421,4539.214;Inherit;False;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TransformPositionNode;146;-2123.929,300.4088;Inherit;False;Object;World;False;Fast;True;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.UnityObjToClipPosHlpNode;554;-5673.609,5029.959;Inherit;False;1;0;FLOAT3;0,0,0;False;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;525;-4954.192,4444.956;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;524;-5265.682,2902.37;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldSpaceLightPos;584;-2320.371,2873.031;Inherit;False;0;3;FLOAT4;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SaturateNode;261;-3237.144,-285.3159;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;530;-5526.217,4464.735;Inherit;False;Property;_OutlineColor1;Outline Color(外描邊顏色);18;1;[HDR];Create;False;0;0;0;False;0;False;0.1981132,0.1981132,0.1981132,0;0.1981132,0.1981132,0.1981132,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleTimeNode;272;-3466.45,-249.2896;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;538;-4150.083,2955.865;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.05;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;541;-4633.856,2593.095;Inherit;False;Property;_DeadClip1;_DeadClip(死亡溶解控制);35;1;[Header];Create;False;1;Dead Dissolve Setting;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;526;-5274.287,3251.883;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;555;-4548.185,2783.141;Inherit;True;Simple;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;17.21;False;1;FLOAT;0
Node;AmplifyShaderEditor.LightAttenuation;565;-6597.125,7169.909;Inherit;False;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;576;-4363.664,2961.636;Inherit;False;Property;_ClipEdgeSoftness1;ClipEdgeSoftness(死亡溶解邊緣顏色平滑度);39;0;Create;False;0;0;0;False;0;False;0.01;0.01;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;531;-5592.326,4800.966;Inherit;False;Property;_OutlineWidth1;Outline Width(外描邊寬度);16;1;[Header];Create;False;1;Outline;0;0;False;0;False;0.1;0.1;0;0.1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;553;-2503.559,3110.539;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DitheringNode;558;-1645.993,4071.062;Inherit;False;0;True;4;0;FLOAT;0;False;1;SAMPLER2D;;False;2;FLOAT4;0,0,0,0;False;3;SAMPLERSTATE;;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;549;-4023.677,2840.969;Inherit;False;Property;_ClipEdgeColor1;ClipEdgeColor(死亡溶解顏色);37;1;[HDR];Create;False;0;0;0;False;0;False;7.464264,1.717473,0,0;7.464264,1.717473,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;527;-3324.806,3099.382;Inherit;False;Property;_GenerateDither;GenerateDither;36;0;Create;True;0;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;533;-5709.77,2923.936;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.WorldSpaceCameraPos;570;-2207.847,4708.744;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.SimpleSubtractOpNode;572;-1888.022,4795.014;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PosVertexDataNode;532;-3855.216,4135.753;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;528;-2717.76,2762.648;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;563;-4320.477,2790.875;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;536;-3284.993,3262.243;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.NormalizeNode;534;-1664.521,4794.714;Inherit;False;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TransformPositionNode;560;-5513.836,2919.052;Inherit;False;World;Object;False;Fast;True;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.GetLocalVarNode;551;-7828.393,5308.738;Inherit;False;385;SRPAdditionLight;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;561;-3365.985,3393.411;Inherit;False;Property;_SprintColor;Sprint Color;42;1;[HDR];Create;True;0;0;0;False;0;False;2.297397,0.9640058,0.2072162,0;2.297397,0.9640058,0.2072162,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;559;-940.4951,3708.768;Inherit;False;Property;_PlayerMaskOpen;PlayerMask Open;48;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;539;-5274.857,3076.108;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;304;-2.909763,3308.642;Float;False;True;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;TAASE/Toon Shader_Building_GardientAlpha(雲上岩橋專用);2992e84f91cbeb14eab234972e07ea9d;True;Forward;0;1;Forward;8;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;2;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Transparent=RenderType;Queue=AlphaTest=Queue=0;True;7;0;False;True;1;5;False;-1;10;False;-1;1;1;False;-1;10;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;True;True;True;True;0;False;-1;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;True;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;1;LightMode=UniversalForward;False;2;Include;;False;;Native;Pragma;__SCREEN_SPACE_OCCLUSION;False;;Custom;Hidden/InternalErrorShader;0;0;Standard;22;Surface;1;  Blend;0;Two Sided;0;Cast Shadows;1;  Use Shadow Threshold;0;Receive Shadows;1;GPU Instancing;1;LOD CrossFade;1;Built-in Fog;1;DOTS Instancing;0;Meta Pass;1;Extra Pre Pass;0;Tessellation;0;  Phong;0;  Strength;0.5,False,-1;  Type;0;  Tess;16,False,-1;  Min;10,False,-1;  Max;25,False,-1;  Edge Length;16,False,-1;  Max Displacement;25,False,-1;Vertex Position,InvertActionOnDeselection;1;0;5;False;True;True;True;True;False;;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;307;2248.458,2635.061;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;1;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;Meta;0;4;Meta;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;LightMode=Meta;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;305;2248.458,2635.061;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;1;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;ShadowCaster;0;2;ShadowCaster;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;False;-1;True;3;False;-1;False;True;1;LightMode=ShadowCaster;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;303;-2.909763,3308.642;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;1;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;ExtraPrePass;0;0;ExtraPrePass;5;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;True;1;1;False;-1;0;False;-1;0;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;True;True;True;True;0;False;-1;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;0;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;306;2248.458,2635.061;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;1;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;DepthOnly;0;3;DepthOnly;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;False;False;True;False;False;False;False;0;False;-1;False;False;False;False;False;False;False;False;False;True;1;False;-1;False;False;True;1;LightMode=DepthOnly;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
WireConnection;316;0;315;2
WireConnection;316;1;311;0
WireConnection;320;0;313;0
WireConnection;320;1;314;0
WireConnection;326;5;321;0
WireConnection;325;0;322;1
WireConnection;325;1;322;2
WireConnection;325;2;323;0
WireConnection;329;0;316;0
WireConnection;324;0;317;0
WireConnection;324;1;318;0
WireConnection;328;0;319;0
WireConnection;328;1;313;0
WireConnection;328;2;320;0
WireConnection;333;0;324;0
WireConnection;333;1;327;0
WireConnection;332;0;328;0
WireConnection;332;1;325;0
WireConnection;337;0;326;0
WireConnection;339;0;332;0
WireConnection;340;0;333;0
WireConnection;340;1;335;0
WireConnection;341;0;330;0
WireConnection;341;1;336;0
WireConnection;341;2;327;2
WireConnection;344;0;339;0
WireConnection;344;1;339;1
WireConnection;346;0;341;0
WireConnection;346;1;330;0
WireConnection;300;0;297;0
WireConnection;300;1;298;0
WireConnection;300;2;290;0
WireConnection;351;0;340;0
WireConnection;351;1;342;0
WireConnection;347;0;338;0
WireConnection;61;0;60;1
WireConnection;63;0;60;2
WireConnection;353;0;351;0
WireConnection;65;0;60;3
WireConnection;65;1;70;0
WireConnection;62;0;61;0
WireConnection;359;0;347;0
WireConnection;359;1;349;0
WireConnection;299;0;183;0
WireConnection;299;1;300;0
WireConnection;362;0;344;0
WireConnection;362;1;339;2
WireConnection;352;0;345;0
WireConnection;352;1;350;0
WireConnection;360;0;346;0
WireConnection;360;1;348;0
WireConnection;293;0;287;3
WireConnection;293;1;287;4
WireConnection;376;0;359;0
WireConnection;376;1;356;0
WireConnection;64;0;62;0
WireConnection;64;1;63;0
WireConnection;64;2;65;0
WireConnection;369;0;360;0
WireConnection;372;13;358;0
WireConnection;372;15;357;0
WireConnection;372;16;315;2
WireConnection;367;0;353;0
WireConnection;367;1;345;0
WireConnection;367;2;352;0
WireConnection;276;0;299;0
WireConnection;276;1;299;0
WireConnection;276;2;299;0
WireConnection;371;0;354;0
WireConnection;371;1;361;0
WireConnection;368;2;337;0
WireConnection;375;0;362;0
WireConnection;292;0;287;1
WireConnection;292;1;287;2
WireConnection;180;0;64;0
WireConnection;390;0;376;0
WireConnection;390;1;375;0
WireConnection;378;0;371;0
WireConnection;378;1;374;0
WireConnection;288;0;292;0
WireConnection;288;1;293;0
WireConnection;288;2;290;0
WireConnection;273;0;276;0
WireConnection;392;0;367;0
WireConnection;392;1;369;0
WireConnection;382;0;365;0
WireConnection;382;1;364;0
WireConnection;388;0;372;0
WireConnection;385;0;368;0
WireConnection;389;1;370;0
WireConnection;399;0;388;0
WireConnection;399;1;316;0
WireConnection;588;1;180;1
WireConnection;588;0;180;3
WireConnection;393;0;392;0
WireConnection;393;1;391;0
WireConnection;294;0;288;0
WireConnection;406;0;389;1
WireConnection;406;1;381;0
WireConnection;406;2;377;0
WireConnection;396;0;387;0
WireConnection;396;1;380;0
WireConnection;400;0;386;0
WireConnection;11;0;64;0
WireConnection;11;1;273;0
WireConnection;401;0;378;0
WireConnection;394;0;382;0
WireConnection;394;1;390;0
WireConnection;394;2;384;0
WireConnection;185;0;588;0
WireConnection;185;1;183;0
WireConnection;190;0;189;0
WireConnection;412;0;394;0
WireConnection;412;1;406;0
WireConnection;421;0;393;0
WireConnection;415;0;399;0
WireConnection;415;1;387;0
WireConnection;415;2;396;0
WireConnection;419;0;405;0
WireConnection;419;1;397;1
WireConnection;413;0;401;0
WireConnection;416;0;395;0
WireConnection;274;0;11;0
WireConnection;274;1;275;0
WireConnection;407;0;402;0
WireConnection;407;1;404;0
WireConnection;411;0;332;0
WireConnection;420;0;398;1
WireConnection;420;1;398;2
WireConnection;434;0;399;0
WireConnection;434;1;415;0
WireConnection;296;0;295;0
WireConnection;135;0;185;0
WireConnection;135;1;136;0
WireConnection;423;0;410;0
WireConnection;423;1;412;0
WireConnection;430;0;413;0
WireConnection;430;1;398;1
WireConnection;430;2;420;0
WireConnection;436;0;419;0
WireConnection;436;1;416;0
WireConnection;186;0;185;0
WireConnection;186;1;188;0
WireConnection;186;2;189;0
WireConnection;186;3;188;0
WireConnection;186;4;190;0
WireConnection;425;0;417;1
WireConnection;425;1;417;2
WireConnection;438;0;409;0
WireConnection;438;1;418;0
WireConnection;422;1;412;0
WireConnection;439;0;407;0
WireConnection;439;1;404;0
WireConnection;424;0;414;0
WireConnection;278;0;274;0
WireConnection;278;1;277;0
WireConnection;450;0;436;0
WireConnection;450;1;427;0
WireConnection;445;0;434;0
WireConnection;445;1;426;0
WireConnection;452;0;438;0
WireConnection;452;1;429;0
WireConnection;191;0;186;0
WireConnection;191;1;135;0
WireConnection;142;0;296;0
WireConnection;142;1;296;1
WireConnection;441;0;439;0
WireConnection;441;1;417;1
WireConnection;441;2;425;0
WireConnection;148;0;147;1
WireConnection;279;0;278;0
WireConnection;447;1;423;0
WireConnection;447;0;422;0
WireConnection;443;0;433;0
WireConnection;443;1;430;0
WireConnection;443;2;432;0
WireConnection;440;0;424;0
WireConnection;440;1;428;0
WireConnection;458;0;432;0
WireConnection;458;1;442;1
WireConnection;456;0;438;0
WireConnection;456;1;446;0
WireConnection;150;0;148;0
WireConnection;459;0;452;0
WireConnection;459;1;447;0
WireConnection;457;0;443;0
WireConnection;139;0;191;0
WireConnection;139;1;296;0
WireConnection;139;2;142;0
WireConnection;149;0;147;3
WireConnection;453;0;441;0
WireConnection;453;1;442;2
WireConnection;461;0;434;0
WireConnection;461;1;445;0
WireConnection;461;2;444;0
WireConnection;265;0;260;0
WireConnection;265;1;279;0
WireConnection;449;0;437;0
WireConnection;138;0;64;0
WireConnection;138;1;162;0
WireConnection;151;0;147;2
WireConnection;448;0;431;0
WireConnection;460;0;450;0
WireConnection;460;1;451;1
WireConnection;460;2;451;2
WireConnection;463;0;438;0
WireConnection;463;1;440;0
WireConnection;159;0;139;0
WireConnection;474;0;460;0
WireConnection;474;1;455;0
WireConnection;254;0;138;0
WireConnection;152;0;150;0
WireConnection;152;1;151;0
WireConnection;152;2;149;0
WireConnection;175;0;69;3
WireConnection;175;1;176;0
WireConnection;473;0;461;0
WireConnection;268;0;265;0
WireConnection;472;0;458;0
WireConnection;472;1;453;0
WireConnection;252;0;138;0
WireConnection;462;0;437;0
WireConnection;462;1;431;0
WireConnection;454;0;449;0
WireConnection;454;1;448;0
WireConnection;467;0;463;0
WireConnection;467;1;459;0
WireConnection;467;2;456;0
WireConnection;253;0;251;0
WireConnection;253;1;252;0
WireConnection;601;0;159;0
WireConnection;601;1;602;0
WireConnection;475;0;467;0
WireConnection;475;1;464;0
WireConnection;481;0;472;0
WireConnection;481;1;474;0
WireConnection;481;2;465;0
WireConnection;481;3;469;0
WireConnection;269;0;152;0
WireConnection;269;1;268;0
WireConnection;269;2;152;0
WireConnection;269;3;266;0
WireConnection;256;0;254;0
WireConnection;177;0;175;0
WireConnection;470;1;462;0
WireConnection;470;0;454;0
WireConnection;592;0;590;0
WireConnection;592;1;591;0
WireConnection;487;0;475;0
WireConnection;487;1;477;0
WireConnection;487;2;478;0
WireConnection;479;0;470;0
WireConnection;479;1;471;0
WireConnection;589;0;601;0
WireConnection;589;1;590;0
WireConnection;589;2;592;0
WireConnection;483;0;481;0
WireConnection;270;0;269;0
WireConnection;270;1;266;0
WireConnection;482;0;466;0
WireConnection;255;0;256;0
WireConnection;255;1;253;0
WireConnection;255;2;256;1
WireConnection;174;0;69;1
WireConnection;174;1;69;2
WireConnection;174;2;177;0
WireConnection;281;0;159;0
WireConnection;281;1;282;0
WireConnection;603;0;589;0
WireConnection;603;1;604;0
WireConnection;280;0;270;0
WireConnection;280;2;281;0
WireConnection;493;1;475;0
WireConnection;493;0;487;0
WireConnection;486;0;476;0
WireConnection;160;0;152;0
WireConnection;160;1;161;0
WireConnection;488;0;482;0
WireConnection;488;1;479;0
WireConnection;488;2;480;0
WireConnection;156;0;255;0
WireConnection;156;1;174;0
WireConnection;499;0;488;0
WireConnection;285;0;280;0
WireConnection;497;0;489;0
WireConnection;497;2;484;0
WireConnection;490;0;485;0
WireConnection;490;1;486;0
WireConnection;502;0;493;0
WireConnection;502;1;495;0
WireConnection;504;0;441;0
WireConnection;600;0;599;0
WireConnection;600;1;603;0
WireConnection;143;0;156;0
WireConnection;143;1;160;0
WireConnection;143;2;159;0
WireConnection;503;0;447;0
WireConnection;507;0;494;0
WireConnection;507;1;498;0
WireConnection;507;2;496;0
WireConnection;506;0;497;0
WireConnection;506;1;499;0
WireConnection;505;0;491;0
WireConnection;505;1;500;0
WireConnection;516;0;502;0
WireConnection;516;1;507;0
WireConnection;595;0;600;0
WireConnection;595;1;594;0
WireConnection;501;0;490;0
WireConnection;501;1;492;0
WireConnection;283;0;285;0
WireConnection;283;1;143;0
WireConnection;511;0;482;0
WireConnection;511;1;508;0
WireConnection;510;0;501;0
WireConnection;510;1;491;0
WireConnection;510;2;505;0
WireConnection;519;0;512;0
WireConnection;519;1;513;0
WireConnection;517;0;514;0
WireConnection;517;1;516;0
WireConnection;596;0;595;0
WireConnection;509;1;497;0
WireConnection;509;0;506;0
WireConnection;137;0;283;0
WireConnection;521;0;509;0
WireConnection;521;1;511;0
WireConnection;521;2;510;0
WireConnection;301;0;137;0
WireConnection;529;0;519;0
WireConnection;529;1;517;0
WireConnection;537;0;536;0
WireConnection;537;1;561;0
WireConnection;537;2;550;0
WireConnection;574;0;485;0
WireConnection;574;1;570;0
WireConnection;546;0;478;0
WireConnection;546;1;477;0
WireConnection;579;0;581;0
WireConnection;579;1;580;0
WireConnection;579;2;538;0
WireConnection;573;0;527;0
WireConnection;568;0;516;0
WireConnection;542;0;562;0
WireConnection;542;1;571;2
WireConnection;575;0;399;0
WireConnection;552;1;564;0
WireConnection;552;0;524;0
WireConnection;552;2;539;0
WireConnection;552;3;526;0
WireConnection;564;0;577;0
WireConnection;571;0;551;0
WireConnection;582;0;501;0
WireConnection;597;0;529;0
WireConnection;597;1;598;0
WireConnection;586;0;566;0
WireConnection;586;1;534;0
WireConnection;581;0;541;0
WireConnection;581;1;563;0
WireConnection;583;0;579;0
WireConnection;583;1;549;0
WireConnection;578;0;535;0
WireConnection;578;1;531;0
WireConnection;578;2;418;4
WireConnection;578;3;587;0
WireConnection;548;0;528;0
WireConnection;545;1;518;0
WireConnection;545;0;520;0
WireConnection;556;0;573;0
WireConnection;569;0;490;0
WireConnection;587;0;554;4
WireConnection;587;1;543;0
WireConnection;562;0;571;0
WireConnection;562;1;571;1
WireConnection;566;0;574;0
WireConnection;554;0;547;0
WireConnection;525;0;467;0
WireConnection;525;1;530;0
WireConnection;524;0;577;0
WireConnection;524;1;533;0
WireConnection;261;0;272;0
WireConnection;538;0;580;0
WireConnection;538;1;576;0
WireConnection;526;0;577;0
WireConnection;526;1;557;0
WireConnection;555;0;552;0
WireConnection;555;1;540;0
WireConnection;553;0;548;0
WireConnection;553;1;581;0
WireConnection;558;0;508;0
WireConnection;558;2;585;0
WireConnection;572;0;486;0
WireConnection;572;1;570;0
WireConnection;528;0;482;0
WireConnection;528;1;556;0
WireConnection;563;0;555;0
WireConnection;536;0;583;0
WireConnection;536;1;516;0
WireConnection;534;0;572;0
WireConnection;560;0;533;0
WireConnection;559;1;509;0
WireConnection;559;0;521;0
WireConnection;539;0;577;0
WireConnection;539;1;544;0
WireConnection;304;0;597;0
WireConnection;304;2;597;0
WireConnection;304;3;559;0
WireConnection;304;4;545;0
WireConnection;304;5;302;0
ASEEND*/
//CHKSM=2EEA981C431F66FF03AC3765317786EDD303EF01