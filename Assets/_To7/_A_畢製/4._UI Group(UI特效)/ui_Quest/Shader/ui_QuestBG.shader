// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ui_QuestBG"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
		[Header(Main Texture Setting)]_Texture1("Texture 1", 2D) = "white" {}
		[Header(Distort Setting)]_DistortsTexture("Distorts Texture", 2D) = "white" {}
		_DistortStrength("Distort Strength", Float) = 0.01
		_FlowGradientSpeed("Flow Gradient Speed", Float) = 0
		_FlowSpeed("Flow Speed", Float) = 0
		[Header(Mask)]_LeftMaskOffset("Left Mask Offset", Float) = 0
		_LeftMaskPower("Left Mask Power", Float) = 1
		[Header(Background Texture)]_SecondTextureBackgroundAlpha("Second Texture Background Alpha", Float) = 1
		[Header(Flash Star Setting)]_FlashStarTexture("Flash Star Texture", 2D) = "white" {}
		[HDR]_FlashStarColor("Flash Star Color", Color) = (1,1,1,0)
		_FlashStarMaskOffset("Flash Star Mask Offset", Float) = 0
		_FlowStarSpeed("Flow Star Speed", Float) = 0
		[Header(Line Texture Setting)]_LineFlowSpeed("Line Flow Speed", Float) = 0
		_LineNoiseScale("Line Noise Scale", Float) = 1
		_LineDistortSpeed("Line Distort Speed", Float) = 1.11
		_LineNoiseStrength("Line Noise Strength", Float) = 0.01
		[HDR]_LineColor("Line Color", Color) = (1,1,1,1)
		_LinePower("Line Power", Float) = 1
		_LineAlpha("Line Alpha", Float) = 1
		_Ctrl("Ctrl", Range( 0 , 1)) = 0
		_Power("Power", Float) = 1.55
		_CtrlColorOffset("Ctrl Color Offset", Float) = 0.39
		[HDR]_RightColor("Right Color", Color) = (0,0.1899264,1,0)
		[HDR]_LeftColor("Left Color", Color) = (1,0,0,0)
		_GradientAlpha("Gradient Alpha", Float) = 0.3
		_CtrlColorValue("Ctrl Color Value", Float) = 0
		_CtrlColorSoft("Ctrl Color Soft", Float) = 1
		_EdgeTexture("Edge Texture", 2D) = "white" {}
		_EdgeColor("Edge Color", Color) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

	}

	SubShader
	{
		LOD 0

		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
		
		Stencil
		{
			Ref [_Stencil]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			CompFront [_StencilComp]
			PassFront [_StencilOp]
			FailFront Keep
			ZFailFront Keep
			CompBack Always
			PassBack Keep
			FailBack Keep
			ZFailBack Keep
		}


		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask [_ColorMask]

		
		Pass
		{
			Name "Default"
		CGPROGRAM
			#define ASE_USING_SAMPLING_MACROS 1

			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			#pragma multi_compile __ UNITY_UI_CLIP_RECT
			#pragma multi_compile __ UNITY_UI_ALPHACLIP
			
			#include "UnityShaderVariables.cginc"
			#define ASE_NEEDS_FRAG_COLOR
			#if defined(SHADER_API_D3D11) || defined(SHADER_API_XBOXONE) || defined(UNITY_COMPILER_HLSLCC) || defined(SHADER_API_PSSL) || (defined(SHADER_TARGET_SURFACE_ANALYSIS) && !defined(SHADER_TARGET_SURFACE_ANALYSIS_MOJOSHADER))//ASE Sampler Macros
			#define SAMPLE_TEXTURE2D(tex,samplerTex,coord) tex.Sample(samplerTex,coord)
			#else//ASE Sampling Macros
			#define SAMPLE_TEXTURE2D(tex,samplerTex,coord) tex2D(tex,coord)
			#endif//ASE Sampling Macros
			

			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				
			};
			
			uniform fixed4 _Color;
			uniform fixed4 _TextureSampleAdd;
			uniform float4 _ClipRect;
			uniform sampler2D _MainTex;
			uniform float4 _LeftColor;
			uniform float4 _RightColor;
			uniform float _CtrlColorValue;
			uniform float _CtrlColorSoft;
			uniform float _Power;
			uniform float _Ctrl;
			uniform float _CtrlColorOffset;
			uniform float _LeftMaskOffset;
			uniform float _LeftMaskPower;
			UNITY_DECLARE_TEX2D_NOSAMPLER(_Texture1);
			uniform float4 _Texture1_ST;
			UNITY_DECLARE_TEX2D_NOSAMPLER(_DistortsTexture);
			uniform float _FlowSpeed;
			SamplerState sampler_DistortsTexture;
			uniform float _DistortStrength;
			SamplerState sampler_Texture1;
			uniform float _GradientAlpha;
			uniform float4 _FlashStarColor;
			UNITY_DECLARE_TEX2D_NOSAMPLER(_FlashStarTexture);
			uniform float _FlowStarSpeed;
			uniform float4 _FlashStarTexture_ST;
			SamplerState sampler_FlashStarTexture;
			uniform float _FlashStarMaskOffset;
			uniform float _LineAlpha;
			uniform float _LineDistortSpeed;
			uniform float _LineNoiseScale;
			uniform float _LineNoiseStrength;
			uniform float _LineFlowSpeed;
			uniform float4 _LineColor;
			uniform float _LinePower;
			uniform float _SecondTextureBackgroundAlpha;
			uniform float4 _EdgeColor;
			UNITY_DECLARE_TEX2D_NOSAMPLER(_EdgeTexture);
			uniform float4 _EdgeTexture_ST;
			SamplerState sampler_EdgeTexture;
			uniform float _FlowGradientSpeed;
			float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }
			float snoise( float2 v )
			{
				const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
				float2 i = floor( v + dot( v, C.yy ) );
				float2 x0 = v - i + dot( i, C.xx );
				float2 i1;
				i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
				float4 x12 = x0.xyxy + C.xxzz;
				x12.xy -= i1;
				i = mod2D289( i );
				float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
				float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
				m = m * m;
				m = m * m;
				float3 x = 2.0 * frac( p * C.www ) - 1.0;
				float3 h = abs( x ) - 0.5;
				float3 ox = floor( x + 0.5 );
				float3 a0 = x - ox;
				m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
				float3 g;
				g.x = a0.x * x0.x + h.x * x0.y;
				g.yz = a0.yz * x12.xz + h.yz * x12.yw;
				return 130.0 * dot( m, g );
			}
			
			float2 UnityGradientNoiseDir( float2 p )
			{
				p = fmod(p , 289);
				float x = fmod((34 * p.x + 1) * p.x , 289) + p.y;
				x = fmod( (34 * x + 1) * x , 289);
				x = frac( x / 41 ) * 2 - 1;
				return normalize( float2(x - floor(x + 0.5 ), abs( x ) - 0.5 ) );
			}
			
			float UnityGradientNoise( float2 UV, float Scale )
			{
				float2 p = UV * Scale;
				float2 ip = floor( p );
				float2 fp = frac( p );
				float d00 = dot( UnityGradientNoiseDir( ip ), fp );
				float d01 = dot( UnityGradientNoiseDir( ip + float2( 0, 1 ) ), fp - float2( 0, 1 ) );
				float d10 = dot( UnityGradientNoiseDir( ip + float2( 1, 0 ) ), fp - float2( 1, 0 ) );
				float d11 = dot( UnityGradientNoiseDir( ip + float2( 1, 1 ) ), fp - float2( 1, 1 ) );
				fp = fp * fp * fp * ( fp * ( fp * 6 - 15 ) + 10 );
				return lerp( lerp( d00, d01, fp.y ), lerp( d10, d11, fp.y ), fp.x ) + 0.5;
			}
			

			
			v2f vert( appdata_t IN  )
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID( IN );
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				UNITY_TRANSFER_INSTANCE_ID(IN, OUT);
				OUT.worldPosition = IN.vertex;
				
				
				OUT.worldPosition.xyz +=  float3( 0, 0, 0 ) ;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

				OUT.texcoord = IN.texcoord;
				
				OUT.color = IN.color * _Color;
				return OUT;
			}

			fixed4 frag(v2f IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				float2 texCoord158 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float temp_output_153_0 = ( saturate( pow( texCoord158.x , _Power ) ) + (-1.0 + (_Ctrl - 0.0) * (1.0 - -1.0) / (1.0 - 0.0)) );
				float smoothstepResult179 = smoothstep( _CtrlColorValue , ( _CtrlColorValue + _CtrlColorSoft ) , ( saturate( temp_output_153_0 ) + _CtrlColorOffset ));
				float4 lerpResult161 = lerp( _LeftColor , _RightColor , smoothstepResult179);
				float2 texCoord15 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float temp_output_20_0 = saturate( pow( ( texCoord15.x + _LeftMaskOffset ) , _LeftMaskPower ) );
				float2 uv_Texture1 = IN.texcoord.xy * _Texture1_ST.xy + _Texture1_ST.zw;
				float2 appendResult13 = (float2(_FlowSpeed , 0.0));
				float2 texCoord12 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 panner7 = ( 1.0 * _Time.y * appendResult13 + texCoord12);
				float4 tex2DNode2 = SAMPLE_TEXTURE2D( _Texture1, sampler_Texture1, ( uv_Texture1 + ( SAMPLE_TEXTURE2D( _DistortsTexture, sampler_DistortsTexture, panner7 ).r * _DistortStrength ) ) );
				float2 appendResult32 = (float2(_FlowStarSpeed , 0.0));
				float2 uv_FlashStarTexture = IN.texcoord.xy * _FlashStarTexture_ST.xy + _FlashStarTexture_ST.zw;
				float2 panner30 = ( 1.0 * _Time.y * appendResult32 + uv_FlashStarTexture);
				float2 texCoord35 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 appendResult51 = (float2(_LineDistortSpeed , 0.0));
				float2 texCoord43 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 panner50 = ( 1.0 * _Time.y * appendResult51 + texCoord43);
				float simplePerlin2D47 = snoise( panner50*_LineNoiseScale );
				simplePerlin2D47 = simplePerlin2D47*0.5 + 0.5;
				float2 appendResult44 = (float2(_LineFlowSpeed , 0.0));
				float2 panner45 = ( 1.0 * _Time.y * appendResult44 + texCoord43);
				float gradientNoise57 = UnityGradientNoise(panner50,5.0);
				gradientNoise57 = gradientNoise57*0.5 + 0.5;
				float4 temp_output_61_0 = ( ( SAMPLE_TEXTURE2D( _DistortsTexture, sampler_Texture1, ( ( simplePerlin2D47 * _LineNoiseStrength ) + panner45 ) ).r * gradientNoise57 ) * _LineColor * temp_output_20_0 );
				float4 temp_cast_0 = (_LinePower).xxxx;
				float2 uv_EdgeTexture = IN.texcoord.xy * _EdgeTexture_ST.xy + _EdgeTexture_ST.zw;
				float2 texCoord198 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float4 temp_output_164_0 = ( ( _FlashStarColor * SAMPLE_TEXTURE2D( _FlashStarTexture, sampler_FlashStarTexture, panner30 ) * saturate( ( texCoord35.x + _FlashStarMaskOffset ) ) ) + ( _LineAlpha * saturate( pow( temp_output_61_0 , temp_cast_0 ) ) ) + ( ( tex2DNode2 * tex2DNode2.a * temp_output_20_0 ) + ( SAMPLE_TEXTURE2D( _Texture1, sampler_Texture1, uv_Texture1 ) * _SecondTextureBackgroundAlpha ) ) + ( _EdgeColor * SAMPLE_TEXTURE2D( _EdgeTexture, sampler_EdgeTexture, uv_EdgeTexture ).r * saturate( pow( ( texCoord198.x + 0.07 ) , 2.0 ) ) ) );
				float3 appendResult174 = (float3(IN.color.r , IN.color.g , IN.color.b));
				float2 appendResult148 = (float2(_FlowGradientSpeed , 0.0));
				float2 texCoord149 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 panner150 = ( 1.0 * _Time.y * appendResult148 + texCoord149);
				float4 appendResult176 = (float4(( (( ( lerpResult161 * ( temp_output_20_0 * tex2DNode2.a ) * _GradientAlpha ) + temp_output_164_0 )).rgb * appendResult174 ) , ( saturate( ( ( temp_output_153_0 * SAMPLE_TEXTURE2D( _DistortsTexture, sampler_DistortsTexture, panner150 ).r ) + temp_output_153_0 ) ) * ( IN.color * temp_output_164_0 ).a * lerpResult161.a )));
				
				half4 color = appendResult176;
				
				#ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif
				
				#ifdef UNITY_UI_ALPHACLIP
				clip (color.a - 0.001);
				#endif

				return color;
			}
		ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=18900
0;73.6;894.2;719;1186.939;-472.4677;1.592624;True;False
Node;AmplifyShaderEditor.RangedFloatNode;52;-2622.06,2260.7;Inherit;False;Property;_LineDistortSpeed;Line Distort Speed;14;0;Create;True;0;0;0;False;0;False;1.11;0.34;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;51;-2412.295,2265.117;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;43;-2569.026,1680.492;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;54;-2165.689,2256.372;Inherit;False;Property;_LineNoiseScale;Line Noise Scale;13;0;Create;True;0;0;0;False;0;False;1;0.83;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;42;-2291.487,1848.141;Inherit;False;Property;_LineFlowSpeed;Line Flow Speed;12;1;[Header];Create;True;1;Line Texture Setting;0;0;False;0;False;0;0.76;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;50;-2154.675,2097.113;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;14;-2371.805,681.2335;Inherit;False;Property;_FlowSpeed;Flow Speed;4;0;Create;True;0;0;0;False;0;False;0;0.66;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;56;-1922.586,2339.834;Inherit;False;Property;_LineNoiseStrength;Line Noise Strength;15;0;Create;True;0;0;0;False;0;False;0.01;0.05;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;47;-1951.619,2107.35;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;44;-2081.722,1852.558;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;156;-1399.945,-658.8251;Inherit;False;Property;_Power;Power;20;0;Create;True;0;0;0;False;0;False;1.55;2.69;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;45;-1955.416,1682.66;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;55;-1696.885,2113.132;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;158;-1717.492,-868.2071;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;15;-1932.847,-80.46598;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;12;-2369.103,516.1996;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;13;-2163.041,685.6508;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;17;-1860.943,59.13025;Inherit;False;Property;_LeftMaskOffset;Left Mask Offset;5;1;[Header];Create;True;1;Mask;0;0;False;0;False;0;0.48;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;155;-1164.046,-844.2583;Inherit;True;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;154;-2263.605,-567.4789;Inherit;False;Property;_Ctrl;Ctrl;19;0;Create;True;0;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;7;-2036.735,515.7533;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;49;-1776.232,1681.465;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;16;-1603.667,-57.17621;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-1520.798,55.19366;Inherit;False;Property;_LeftMaskPower;Left Mask Power;6;0;Create;True;0;0;0;False;0;False;1;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;59;-2423.905,997.7109;Inherit;True;Property;_DistortsTexture;Distorts Texture;1;1;[Header];Create;True;1;Distort Setting;0;0;False;0;False;None;6551728c257b2314ea9cca06692dd63a;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.SamplerNode;46;-1647.537,1652.66;Inherit;True;Property;_TextureSample1;Texture Sample 1;2;1;[Header];Create;True;1;Distort Setting;0;0;False;0;False;21;6551728c257b2314ea9cca06692dd63a;6551728c257b2314ea9cca06692dd63a;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;11;-1629.011,721.2347;Inherit;False;Property;_DistortStrength;Distort Strength;2;0;Create;True;0;0;0;False;0;False;0.01;0.67;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;191;-1967.345,-562.7327;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-1;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;57;-1944.335,2465.173;Inherit;True;Gradient;True;True;2;0;FLOAT2;0,0;False;1;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;6;-1730.735,484.0292;Inherit;True;Property;_TextureSample2;Texture Sample 2;1;1;[Header];Create;True;1;Distort Setting;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PowerNode;18;-1311.798,-56.80634;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;157;-930.4935,-844.2584;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;58;-1217.522,1839.781;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;8;-1932.134,156.5416;Inherit;False;0;21;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;153;-898.712,-587.6539;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;60;-1211.557,2084.913;Inherit;False;Property;_LineColor;Line Color;16;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,1;2.996078,1.835294,1.34902,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;197;-715.2902,1333.83;Inherit;False;Constant;_Float0;Float 0;5;1;[Header];Create;True;1;Mask;0;0;False;0;False;0.07;0.48;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;10;-1411.551,489.9665;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;20;-1139.798,-56.80634;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;198;-787.1942,1194.234;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;40;-1929.047,1129.964;Inherit;False;Property;_FlowStarSpeed;Flow Star Speed;11;0;Create;True;0;0;0;False;0;False;0;0.38;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;181;-617.2126,-18.35245;Inherit;False;Property;_CtrlColorSoft;Ctrl Color Soft;26;0;Create;True;0;0;0;False;0;False;1;0.95;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;35;-1649.163,1238.968;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;180;-630.2126,-91.35245;Inherit;False;Property;_CtrlColorValue;Ctrl Color Value;25;0;Create;True;0;0;0;False;0;False;0;0.17;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;188;-749.0391,2100.076;Inherit;False;Property;_LinePower;Line Power;17;0;Create;True;0;0;0;False;0;False;1;4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;21;-1664.278,276.2473;Inherit;True;Property;_Texture1;Texture 1;0;1;[Header];Create;True;1;Main Texture Setting;0;0;False;0;False;eec975b3d74415245a8f22b7a4c556ed;eec975b3d74415245a8f22b7a4c556ed;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.TextureCoordinatesNode;28;-1814.317,915.0027;Inherit;False;0;29;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;37;-1482.656,1410.191;Inherit;False;Property;_FlashStarMaskOffset;Flash Star Mask Offset;10;0;Create;True;0;0;0;False;0;False;0;-0.7;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;160;-807.7469,-167.8914;Inherit;False;Property;_CtrlColorOffset;Ctrl Color Offset;21;0;Create;True;0;0;0;False;0;False;0.39;0.03;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;9;-1468.439,164.2105;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;32;-1644.394,1108.715;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;61;-792.7493,1840.048;Inherit;True;3;3;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;147;-1753.913,-308.7701;Inherit;False;Property;_FlowGradientSpeed;Flow Gradient Speed;3;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;200;-458.0142,1217.524;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;199;-375.1452,1329.894;Inherit;False;Constant;_Float1;Float 1;6;0;Create;True;0;0;0;False;0;False;2;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;166;-718.0275,-289.0161;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;2;-1302.975,137.6544;Inherit;True;Property;_Texture0;Texture 0;0;0;Create;True;0;0;0;False;0;False;-1;eec975b3d74415245a8f22b7a4c556ed;eec975b3d74415245a8f22b7a4c556ed;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;22;-1300.189,337.4151;Inherit;True;Property;_TextureSample0;Texture Sample 0;0;0;Create;True;0;0;0;False;0;False;-1;eec975b3d74415245a8f22b7a4c556ed;eec975b3d74415245a8f22b7a4c556ed;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;148;-1545.149,-304.3528;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PowerNode;187;-555.8266,1976.222;Inherit;False;False;2;0;COLOR;0,0,0,0;False;1;FLOAT;1;False;1;COLOR;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;149;-1752.939,-473.804;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;25;-1253.196,589.0134;Inherit;False;Property;_SecondTextureBackgroundAlpha;Second Texture Background Alpha;7;1;[Header];Create;True;1;Background Texture;0;0;False;0;False;1;0.63;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;182;-454.2126,-25.35245;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;159;-578.9418,-182.7488;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;36;-1239.208,1259.548;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;201;-166.1451,1217.894;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;30;-1483.44,920.6422;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;-920.1609,478.0019;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;195;-717.4568,935.062;Inherit;True;Property;_EdgeTexture;Edge Texture;27;0;Create;True;0;0;0;False;0;False;-1;None;ff274d311eafaef4d825df40993bca47;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;150;-1418.843,-474.2503;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SmoothstepOpNode;179;-399.2126,-182.3524;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;33;-1140.992,707.6263;Inherit;False;Property;_FlashStarColor;Flash Star Color;9;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,0;5.518961,1.964866,1.126908,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;163;-410.9621,-382.2085;Inherit;False;Property;_RightColor;Right Color;22;1;[HDR];Create;True;0;0;0;False;0;False;0,0.1899264,1,0;0,0,0,0.8823529;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;29;-1234.422,891.412;Inherit;True;Property;_FlashStarTexture;Flash Star Texture;8;1;[Header];Create;True;1;Flash Star Setting;0;0;False;0;False;-1;6fe20efd6e8384b4e95e762dc8c8f46d;6fe20efd6e8384b4e95e762dc8c8f46d;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;189;-405.5504,1977.874;Inherit;False;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;41;-1037.011,1269.258;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;3;-932.6675,143.2998;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.ColorNode;194;-627.4568,714.0616;Inherit;False;Property;_EdgeColor;Edge Color;28;0;Create;True;0;0;0;False;0;False;0,0,0,0;0.4150943,0.3702641,0.1660377,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;62;-738.7294,2207.673;Inherit;False;Property;_LineAlpha;Line Alpha;18;0;Create;True;0;0;0;False;0;False;1;0.2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;202;5.85487,1217.894;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;162;-411.9621,-561.2086;Inherit;False;Property;_LeftColor;Left Color;23;1;[HDR];Create;True;0;0;0;False;0;False;1,0,0,0;1.698113,0.9815713,0.4389461,0.4039216;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;196;-360.4565,719.0616;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;178;-666.2325,50.36764;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;146;-1048.821,-502.0464;Inherit;True;Property;_TextureSample3;Texture Sample 3;19;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;-872.7244,714.0648;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;169;-173.1628,35.34367;Inherit;False;Property;_GradientAlpha;Gradient Alpha;24;0;Create;True;0;0;0;False;0;False;0.3;0.07;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;161;-171.9834,-227.8351;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;23;-666.0189,145.218;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;190;-251.9714,2072.003;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;165;36.69371,-108.1627;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;164;-160.8985,159.8931;Inherit;False;4;4;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.VertexColorNode;1;310.9321,481.7461;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;151;-673.3552,-586.8722;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;168;206.057,39.84612;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;5;500.4803,135.4304;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;145;-447.4401,-752.6562;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;174;513.0881,505.0937;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;152;-224.5862,-751.8721;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;183;319.5158,-225.0533;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SwizzleNode;173;363.7576,35.53264;Inherit;False;FLOAT3;0;1;2;3;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.BreakToComponentsNode;171;640.6245,16.501;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;172;799.2393,63.49789;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;175;735.0463,271.2182;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;176;994.092,270.4229;Inherit;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;186;-54.7309,1853.17;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;1437.458,272.8752;Float;False;True;-1;2;ASEMaterialInspector;0;6;ui_QuestBG;5056123faa0c79b47ab6ad7e8bf059a4;True;Default;0;0;Default;2;False;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;True;True;True;True;True;0;True;-9;False;False;False;False;False;False;False;True;True;0;True;-5;255;True;-8;255;True;-7;0;True;-4;0;True;-6;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;2;False;-1;True;0;True;-11;False;True;5;Queue=Transparent=Queue=0;IgnoreProjector=True;RenderType=Transparent=RenderType;PreviewType=Plane;CanUseSpriteAtlas=True;False;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;0;;0;0;Standard;0;0;1;True;False;;True;0
WireConnection;51;0;52;0
WireConnection;50;0;43;0
WireConnection;50;2;51;0
WireConnection;47;0;50;0
WireConnection;47;1;54;0
WireConnection;44;0;42;0
WireConnection;45;0;43;0
WireConnection;45;2;44;0
WireConnection;55;0;47;0
WireConnection;55;1;56;0
WireConnection;13;0;14;0
WireConnection;155;0;158;1
WireConnection;155;1;156;0
WireConnection;7;0;12;0
WireConnection;7;2;13;0
WireConnection;49;0;55;0
WireConnection;49;1;45;0
WireConnection;16;0;15;1
WireConnection;16;1;17;0
WireConnection;46;0;59;0
WireConnection;46;1;49;0
WireConnection;191;0;154;0
WireConnection;57;0;50;0
WireConnection;6;0;59;0
WireConnection;6;1;7;0
WireConnection;18;0;16;0
WireConnection;18;1;19;0
WireConnection;157;0;155;0
WireConnection;58;0;46;1
WireConnection;58;1;57;0
WireConnection;153;0;157;0
WireConnection;153;1;191;0
WireConnection;10;0;6;1
WireConnection;10;1;11;0
WireConnection;20;0;18;0
WireConnection;9;0;8;0
WireConnection;9;1;10;0
WireConnection;32;0;40;0
WireConnection;61;0;58;0
WireConnection;61;1;60;0
WireConnection;61;2;20;0
WireConnection;200;0;198;1
WireConnection;200;1;197;0
WireConnection;166;0;153;0
WireConnection;2;0;21;0
WireConnection;2;1;9;0
WireConnection;22;0;21;0
WireConnection;22;1;8;0
WireConnection;148;0;147;0
WireConnection;187;0;61;0
WireConnection;187;1;188;0
WireConnection;182;0;180;0
WireConnection;182;1;181;0
WireConnection;159;0;166;0
WireConnection;159;1;160;0
WireConnection;36;0;35;1
WireConnection;36;1;37;0
WireConnection;201;0;200;0
WireConnection;201;1;199;0
WireConnection;30;0;28;0
WireConnection;30;2;32;0
WireConnection;24;0;22;0
WireConnection;24;1;25;0
WireConnection;150;0;149;0
WireConnection;150;2;148;0
WireConnection;179;0;159;0
WireConnection;179;1;180;0
WireConnection;179;2;182;0
WireConnection;29;1;30;0
WireConnection;189;0;187;0
WireConnection;41;0;36;0
WireConnection;3;0;2;0
WireConnection;3;1;2;4
WireConnection;3;2;20;0
WireConnection;202;0;201;0
WireConnection;196;0;194;0
WireConnection;196;1;195;1
WireConnection;196;2;202;0
WireConnection;178;0;20;0
WireConnection;178;1;2;4
WireConnection;146;0;59;0
WireConnection;146;1;150;0
WireConnection;34;0;33;0
WireConnection;34;1;29;0
WireConnection;34;2;41;0
WireConnection;161;0;162;0
WireConnection;161;1;163;0
WireConnection;161;2;179;0
WireConnection;23;0;3;0
WireConnection;23;1;24;0
WireConnection;190;0;62;0
WireConnection;190;1;189;0
WireConnection;165;0;161;0
WireConnection;165;1;178;0
WireConnection;165;2;169;0
WireConnection;164;0;34;0
WireConnection;164;1;190;0
WireConnection;164;2;23;0
WireConnection;164;3;196;0
WireConnection;151;0;153;0
WireConnection;151;1;146;1
WireConnection;168;0;165;0
WireConnection;168;1;164;0
WireConnection;5;0;1;0
WireConnection;5;1;164;0
WireConnection;145;0;151;0
WireConnection;145;1;153;0
WireConnection;174;0;1;1
WireConnection;174;1;1;2
WireConnection;174;2;1;3
WireConnection;152;0;145;0
WireConnection;183;0;161;0
WireConnection;173;0;168;0
WireConnection;171;0;5;0
WireConnection;172;0;152;0
WireConnection;172;1;171;3
WireConnection;172;2;183;3
WireConnection;175;0;173;0
WireConnection;175;1;174;0
WireConnection;176;0;175;0
WireConnection;176;3;172;0
WireConnection;186;1;61;0
WireConnection;0;0;176;0
ASEEND*/
//CHKSM=34797618B2DF53D3853890AAD95C5D525C1B1E5F