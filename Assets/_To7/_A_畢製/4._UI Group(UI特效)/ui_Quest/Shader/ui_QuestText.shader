// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "ui_QuestText"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
		_Ctrl("Ctrl", Range( 0 , 1)) = 0
		_LineTexture("Line Texture", 2D) = "white" {}
		_TextureSample3("Texture Sample 3", 2D) = "white" {}
		_base_mission("base_mission", 2D) = "white" {}
		[HDR]_LineColor("Line Color", Color) = (0.9056604,0.6168754,0.2853684,1)
		[HDR]_BGColor("BG Color", Color) = (0.0706301,0.6037736,0.3631165,1)
		_AlphaMul("Alpha Mul", Float) = 1
		_vfx_TextMask_01("vfx_TextMask_01", 2D) = "white" {}
		_vfx_Pencil_01("vfx_Pencil_01", 2D) = "white" {}
		[HDR]_PencilColor("Pencil Color", Color) = (1,1,1,1)
		_PencilZoom("Pencil Zoom", Float) = 0
		_vfx_Line_2("vfx_Line_2", 2D) = "white" {}
		_TextureSample2("Texture Sample 2", 2D) = "white" {}
		_Float4("Float 4", Float) = -0.2
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

	}

	SubShader
	{
		LOD 0

		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
		
		Stencil
		{
			Ref [_Stencil]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			CompFront [_StencilComp]
			PassFront [_StencilOp]
			FailFront Keep
			ZFailFront Keep
			CompBack Always
			PassBack Keep
			FailBack Keep
			ZFailBack Keep
		}


		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha, OneMinusDstColor One
		ColorMask [_ColorMask]

		
		Pass
		{
			Name "Default"
		CGPROGRAM
			
			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			#pragma multi_compile __ UNITY_UI_CLIP_RECT
			#pragma multi_compile __ UNITY_UI_ALPHACLIP
			
			#include "UnityShaderVariables.cginc"
			#define ASE_NEEDS_FRAG_COLOR

			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				
			};
			
			uniform fixed4 _Color;
			uniform fixed4 _TextureSampleAdd;
			uniform float4 _ClipRect;
			uniform sampler2D _MainTex;
			uniform sampler2D _vfx_Pencil_01;
			uniform float _PencilZoom;
			uniform float4 _PencilColor;
			uniform sampler2D _LineTexture;
			uniform float4 _LineTexture_ST;
			uniform sampler2D _TextureSample3;
			uniform float4 _TextureSample3_ST;
			uniform float4 _BGColor;
			uniform float4 _LineColor;
			uniform sampler2D _base_mission;
			uniform float4 _base_mission_ST;
			uniform float _AlphaMul;
			uniform sampler2D _vfx_Line_2;
			uniform float4 _vfx_Line_2_ST;
			uniform sampler2D _TextureSample2;
			uniform float _Ctrl;
			uniform sampler2D _vfx_TextMask_01;
			uniform float _Float4;
			inline float noise_randomValue (float2 uv) { return frac(sin(dot(uv, float2(12.9898, 78.233)))*43758.5453); }
			inline float noise_interpolate (float a, float b, float t) { return (1.0-t)*a + (t*b); }
			inline float valueNoise (float2 uv)
			{
				float2 i = floor(uv);
				float2 f = frac( uv );
				f = f* f * (3.0 - 2.0 * f);
				uv = abs( frac(uv) - 0.5);
				float2 c0 = i + float2( 0.0, 0.0 );
				float2 c1 = i + float2( 1.0, 0.0 );
				float2 c2 = i + float2( 0.0, 1.0 );
				float2 c3 = i + float2( 1.0, 1.0 );
				float r0 = noise_randomValue( c0 );
				float r1 = noise_randomValue( c1 );
				float r2 = noise_randomValue( c2 );
				float r3 = noise_randomValue( c3 );
				float bottomOfGrid = noise_interpolate( r0, r1, f.x );
				float topOfGrid = noise_interpolate( r2, r3, f.x );
				float t = noise_interpolate( bottomOfGrid, topOfGrid, f.y );
				return t;
			}
			
			float SimpleNoise(float2 UV)
			{
				float t = 0.0;
				float freq = pow( 2.0, float( 0 ) );
				float amp = pow( 0.5, float( 3 - 0 ) );
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(1));
				amp = pow(0.5, float(3-1));
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(2));
				amp = pow(0.5, float(3-2));
				t += valueNoise( UV/freq )*amp;
				return t;
			}
			

			
			v2f vert( appdata_t IN  )
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID( IN );
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				UNITY_TRANSFER_INSTANCE_ID(IN, OUT);
				OUT.worldPosition = IN.vertex;
				
				
				OUT.worldPosition.xyz +=  float3( 0, 0, 0 ) ;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

				OUT.texcoord = IN.texcoord;
				
				OUT.color = IN.color * _Color;
				return OUT;
			}

			fixed4 frag(v2f IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				float2 texCoord80 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 temp_output_5_0_g1 = texCoord80;
				float2 temp_cast_0 = (0.5).xx;
				float2 appendResult5 = (float2(1.48 , 0.0));
				float2 texCoord2 = IN.texcoord.xy * float2( 1,10 ) + float2( 0,0 );
				float2 panner3 = ( 1.0 * _Time.y * appendResult5 + texCoord2);
				float simpleNoise1 = SimpleNoise( panner3*3.0 );
				float2 appendResult12 = (float2(0.32 , 0.0));
				float2 uv_LineTexture = IN.texcoord.xy * _LineTexture_ST.xy + _LineTexture_ST.zw;
				float2 panner10 = ( 1.0 * _Time.y * appendResult12 + uv_LineTexture);
				float2 appendResult99 = (float2(0.15 , 0.0));
				float2 uv_TextureSample3 = IN.texcoord.xy * _TextureSample3_ST.xy + _TextureSample3_ST.zw;
				float2 panner101 = ( 1.0 * _Time.y * appendResult99 + uv_TextureSample3);
				float2 appendResult23 = (float2(1.0 , 0.0));
				float2 texCoord24 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 panner21 = ( 1.0 * _Time.y * appendResult23 + texCoord24);
				float2 uv_base_mission = IN.texcoord.xy * _base_mission_ST.xy + _base_mission_ST.zw;
				float2 uv_vfx_Line_2 = IN.texcoord.xy * _vfx_Line_2_ST.xy + _vfx_Line_2_ST.zw;
				float4 tex2DNode87 = tex2D( _vfx_Line_2, uv_vfx_Line_2 );
				float temp_output_111_0 = (-0.75 + (_Ctrl - 0.0) * (0.35 - -0.75) / (1.0 - 0.0));
				float2 appendResult93 = (float2(( temp_output_111_0 * 2.0 ) , 0.0));
				float2 texCoord92 = IN.texcoord.xy * float2( 1,1 ) + appendResult93;
				float4 break42 = ( ( ( tex2D( _vfx_Pencil_01, ( temp_output_5_0_g1 + ( ( temp_output_5_0_g1 - temp_cast_0 ) * _PencilZoom ) ) ) * _PencilColor ) + ( ( ( simpleNoise1 * ( ( tex2D( _LineTexture, panner10 ).r + ( tex2D( _TextureSample3, ( ( simpleNoise1 * 0.1 ) + panner101 ) ).r * 3.0 ) ) + 0.5 ) * _BGColor ) + ( tex2D( _LineTexture, panner21 ).r * _LineColor ) ) * ( tex2D( _base_mission, uv_base_mission ).a * _AlphaMul ) ) + ( tex2DNode87.r * tex2D( _TextureSample2, texCoord92 ).r * _PencilColor ) ) * IN.color );
				float2 appendResult84 = (float2(temp_output_111_0 , 0.0));
				float2 texCoord73 = IN.texcoord.xy * float2( 1,1 ) + appendResult84;
				float4 tex2DNode71 = tex2D( _vfx_TextMask_01, texCoord73 );
				float2 texCoord107 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 temp_output_5_0_g2 = texCoord107;
				float2 temp_cast_1 = (0.5).xx;
				float4 appendResult38 = (float4(break42.r , break42.g , break42.b , saturate( ( ( ( ( tex2DNode87.r * tex2DNode71.r ) + tex2DNode71.r ) * ( tex2D( _base_mission, ( temp_output_5_0_g2 + ( ( temp_output_5_0_g2 - temp_cast_1 ) * _Float4 ) ) ).a * _AlphaMul ) ) * break42.a ) )));
				
				half4 color = appendResult38;
				
				#ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif
				
				#ifdef UNITY_UI_ALPHACLIP
				clip (color.a - 0.001);
				#endif

				return color;
			}
		ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=18900
0;0;1536;812.6;2797.834;151.6552;1;True;False
Node;AmplifyShaderEditor.RangedFloatNode;4;-1901.665,-188.4571;Inherit;False;Constant;_Float0;Float 0;0;0;Create;True;0;0;0;False;0;False;1.48;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;5;-1721.03,-185.0054;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;2;-1840.687,-355.2851;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,10;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;98;-2374.564,397.3503;Inherit;False;Constant;_Float3;Float 3;0;0;Create;True;0;0;0;False;0;False;0.15;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;3;-1568.01,-355.2851;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;1;-1338.045,-360.3679;Inherit;True;Simple;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;100;-2313.586,230.522;Inherit;False;0;96;2;3;2;SAMPLER2D;;False;0;FLOAT2;0.5,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;99;-2193.929,400.8019;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;104;-1817.782,214.3869;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;16;-2628.572,-8.947861;Inherit;True;Property;_LineTexture;Line Texture;1;0;Create;True;0;0;0;False;0;False;6551728c257b2314ea9cca06692dd63a;6551728c257b2314ea9cca06692dd63a;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.RangedFloatNode;11;-1908.176,128.5011;Inherit;False;Constant;_Float1;Float 1;0;0;Create;True;0;0;0;False;0;False;0.32;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;101;-2040.909,230.522;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;12;-1727.541,131.9528;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;13;-1847.198,-38.32695;Inherit;False;0;16;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;103;-1778.616,287.3167;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;17;-2308.42,-7.74884;Inherit;False;LineTex;-1;True;1;0;SAMPLER2D;;False;1;SAMPLER2D;0
Node;AmplifyShaderEditor.RangedFloatNode;22;-1917.325,566.689;Inherit;False;Constant;_Float2;Float 2;0;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;85;-1563.981,-564.964;Inherit;False;Property;_Ctrl;Ctrl;0;0;Create;True;0;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;18;-1562.648,-110.5967;Inherit;False;17;LineTex;1;0;OBJECT;;False;1;SAMPLER2D;0
Node;AmplifyShaderEditor.SamplerNode;96;-1546.015,122.961;Inherit;True;Property;_TextureSample3;Texture Sample 3;2;0;Create;True;0;0;0;False;0;False;-1;8335b10d2e7f90348b815337cb911fbf;8335b10d2e7f90348b815337cb911fbf;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;10;-1574.521,-38.32695;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;102;-1166.907,234.229;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;3;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;23;-1736.69,570.1407;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TFHCRemapNode;111;-1283.671,-560.1123;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-0.75;False;4;FLOAT;0.35;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;24;-1856.347,399.8611;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;15;-1318.262,-68.14803;Inherit;True;Property;_TextureSample0;Texture Sample 0;0;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;26;-1571.797,327.5912;Inherit;False;17;LineTex;1;0;OBJECT;;False;1;SAMPLER2D;0
Node;AmplifyShaderEditor.PannerNode;21;-1583.67,399.8611;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;97;-986.8394,155.6853;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;91;-817.0561,-356.6771;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;20;-1012.03,-40.08157;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;34;-1124.972,-433.7753;Inherit;False;Property;_BGColor;BG Color;5;1;[HDR];Create;True;0;0;0;False;0;False;0.0706301,0.6037736,0.3631165,1;1.960784,0.9411765,0.345098,0.5254902;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;81;-627.2178,6.245361;Inherit;False;Property;_PencilZoom;Pencil Zoom;10;0;Create;True;0;0;0;False;0;False;0;0.75;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;93;-649.4382,-352.3372;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;25;-1320.312,368.2651;Inherit;True;Property;_TextureSample1;Texture Sample 1;0;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;80;-607.2178,-225.7546;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;29;-1240.168,581.8207;Inherit;False;Property;_LineColor;Line Color;4;1;[HDR];Create;True;0;0;0;False;0;False;0.9056604,0.6168754,0.2853684,1;0.4435634,0.3339622,1,0.3490196;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;84;-890.3137,-559.5758;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-276.8941,737.0768;Inherit;False;Property;_AlphaMul;Alpha Mul;6;0;Create;True;0;0;0;False;0;False;1;0.75;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;28;-888.2169,454.4351;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;92;-480.8627,-388.6682;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;19;-872.3093,-193.0694;Inherit;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;30;-544.5394,322.3481;Inherit;True;Property;_base_mission;base_mission;3;0;Create;True;0;0;0;False;0;False;-1;eec975b3d74415245a8f22b7a4c556ed;eec975b3d74415245a8f22b7a4c556ed;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.FunctionNode;79;-539.2178,-107.7546;Inherit;False;UVZoom;-1;;1;e082f9d39d853124ab639b29ae915b78;0;2;5;FLOAT2;0,0;False;10;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;27;-522.2572,49.33645;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;107;-943.0114,655.3473;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;74;-319.1103,-135.2295;Inherit;True;Property;_vfx_Pencil_01;vfx_Pencil_01;8;0;Create;True;0;0;0;False;0;False;-1;3048ff346b0089d4eb7f12fb162cc106;3048ff346b0089d4eb7f12fb162cc106;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;77;-241.3342,259.7536;Inherit;False;Property;_PencilColor;Pencil Color;9;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,1;5.992157,2.509804,0.7843137,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;95;-220.7326,-345.071;Inherit;True;Property;_TextureSample2;Texture Sample 2;13;0;Create;True;0;0;0;False;0;False;-1;f0a403d2c7e0bac448a7f15ceb6680ea;f0a403d2c7e0bac448a7f15ceb6680ea;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;110;-65.14817,422.3062;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;87;-230.3008,-776.6353;Inherit;True;Property;_vfx_Line_2;vfx_Line_2;12;0;Create;True;0;0;0;False;0;False;-1;d476c5e03d5307544a7cab3c2e11f24c;d476c5e03d5307544a7cab3c2e11f24c;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;109;-868.3478,803.5432;Inherit;False;Property;_Float4;Float 4;14;0;Create;True;0;0;0;False;0;False;-0.2;-0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;73;-638.1654,-509.7374;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;71;-210.3616,-537.7977;Inherit;True;Property;_vfx_TextMask_01;vfx_TextMask_01;7;0;Create;True;0;0;0;False;0;False;-1;d04b477edbe597c44a0e995043cb764e;1468355f7ef86ec488e62b4ce3231924;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;31;-226.8175,50.22253;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;78;-24.33423,-131.2464;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;90;239.7127,-127.6451;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.FunctionNode;108;-713.3643,656.4785;Inherit;False;UVZoom;-1;;2;e082f9d39d853124ab639b29ae915b78;0;2;5;FLOAT2;0,0;False;10;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;88;164.8148,-533.5622;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;32;109.5755,229.8546;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;76;113.4223,61.48914;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;106;-541.4119,535.433;Inherit;True;Property;_TextureSample4;Texture Sample 4;3;0;Create;True;0;0;0;False;0;False;-1;eec975b3d74415245a8f22b7a4c556ed;eec975b3d74415245a8f22b7a4c556ed;True;0;False;white;Auto;False;Instance;30;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;89;311.9748,-343.0966;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;75;242.1173,61.58937;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;46;181.6197,421.0918;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;83;493.3995,-230.1145;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;42;431.2713,45.26298;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;82;659.0139,113.6665;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;105;836.0354,221.6836;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;86;105.0915,-226.566;Inherit;False;Property;_MaskStrength;Mask Strength;11;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;38;897.826,43.23779;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;1225.497,44.16663;Float;False;True;-1;2;ASEMaterialInspector;0;6;ui_QuestText;5056123faa0c79b47ab6ad7e8bf059a4;True;Default;0;0;Default;2;True;True;2;5;False;-1;10;False;-1;5;4;False;-1;1;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;True;True;True;True;True;0;True;-9;False;False;False;False;False;False;False;True;True;0;True;-5;255;True;-8;255;True;-7;0;True;-4;0;True;-6;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;2;False;-1;True;0;True;-11;False;True;5;Queue=Transparent=Queue=0;IgnoreProjector=True;RenderType=Transparent=RenderType;PreviewType=Plane;CanUseSpriteAtlas=True;False;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;0;;0;0;Standard;0;0;1;True;False;;False;0
WireConnection;5;0;4;0
WireConnection;3;0;2;0
WireConnection;3;2;5;0
WireConnection;1;0;3;0
WireConnection;99;0;98;0
WireConnection;104;0;1;0
WireConnection;101;0;100;0
WireConnection;101;2;99;0
WireConnection;12;0;11;0
WireConnection;103;0;104;0
WireConnection;103;1;101;0
WireConnection;17;0;16;0
WireConnection;96;1;103;0
WireConnection;10;0;13;0
WireConnection;10;2;12;0
WireConnection;102;0;96;1
WireConnection;23;0;22;0
WireConnection;111;0;85;0
WireConnection;15;0;18;0
WireConnection;15;1;10;0
WireConnection;21;0;24;0
WireConnection;21;2;23;0
WireConnection;97;0;15;1
WireConnection;97;1;102;0
WireConnection;91;0;111;0
WireConnection;20;0;97;0
WireConnection;93;0;91;0
WireConnection;25;0;26;0
WireConnection;25;1;21;0
WireConnection;84;0;111;0
WireConnection;28;0;25;1
WireConnection;28;1;29;0
WireConnection;92;1;93;0
WireConnection;19;0;1;0
WireConnection;19;1;20;0
WireConnection;19;2;34;0
WireConnection;79;5;80;0
WireConnection;79;10;81;0
WireConnection;27;0;19;0
WireConnection;27;1;28;0
WireConnection;74;1;79;0
WireConnection;95;1;92;0
WireConnection;110;0;30;4
WireConnection;110;1;44;0
WireConnection;73;1;84;0
WireConnection;71;1;73;0
WireConnection;31;0;27;0
WireConnection;31;1;110;0
WireConnection;78;0;74;0
WireConnection;78;1;77;0
WireConnection;90;0;87;1
WireConnection;90;1;95;1
WireConnection;90;2;77;0
WireConnection;108;5;107;0
WireConnection;108;10;109;0
WireConnection;88;0;87;1
WireConnection;88;1;71;1
WireConnection;76;0;78;0
WireConnection;76;1;31;0
WireConnection;76;2;90;0
WireConnection;106;1;108;0
WireConnection;89;0;88;0
WireConnection;89;1;71;1
WireConnection;75;0;76;0
WireConnection;75;1;32;0
WireConnection;46;0;106;4
WireConnection;46;1;44;0
WireConnection;83;0;89;0
WireConnection;83;1;46;0
WireConnection;42;0;75;0
WireConnection;82;0;83;0
WireConnection;82;1;42;3
WireConnection;105;0;82;0
WireConnection;38;0;42;0
WireConnection;38;1;42;1
WireConnection;38;2;42;2
WireConnection;38;3;105;0
WireConnection;0;0;38;0
ASEEND*/
//CHKSM=7EDDB3D959FF54D3FE8DD94D4359CCEF538E5D5F