// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "vfx_UIOutDistortLine"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
		_ShiningUTiling1("Shining U Tiling 1", Float) = 1
		_ShiningVTiling1("Shining V Tiling 1", Float) = 0.01
		_ShiningUOffsetSpeed1("Shining U OffsetSpeed 1", Float) = 0
		_ShiningVOffsetSpeed1("Shining V OffsetSpeed 1", Float) = -1
		_DistortLineTexture1("Distort Line Texture 1", 2D) = "white" {}
		_Texture0Emi("Texture 0 Emi", Float) = 10
		[HDR]_DistortLineColor("Distort Line Color", Color) = (3.953349,1.345381,0.1655853,1)
		_UVDistortTexture("UV Distort Texture", 2D) = "white" {}
		_MaskTexture("Mask Texture", 2D) = "white" {}
		_MaskTexturePow("Mask Texture Pow", Float) = 3.32
		_EdgeTexture("Edge Texture", 2D) = "white" {}
		_EdgeMaskSize("Edge Mask Size", Float) = 1.56
		_EdgeStrength("Edge Strength", Float) = 1
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

	}

	SubShader
	{
		LOD 0

		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
		
		Stencil
		{
			Ref [_Stencil]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			CompFront [_StencilComp]
			PassFront [_StencilOp]
			FailFront Keep
			ZFailFront Keep
			CompBack Always
			PassBack Keep
			FailBack Keep
			ZFailBack Keep
		}


		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask [_ColorMask]

		
		Pass
		{
			Name "Default"
		CGPROGRAM
			
			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			#pragma multi_compile __ UNITY_UI_CLIP_RECT
			#pragma multi_compile __ UNITY_UI_ALPHACLIP
			
			#include "UnityShaderVariables.cginc"
			#define ASE_NEEDS_FRAG_COLOR

			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				
			};
			
			uniform fixed4 _Color;
			uniform fixed4 _TextureSampleAdd;
			uniform float4 _ClipRect;
			uniform sampler2D _MainTex;
			uniform sampler2D _DistortLineTexture1;
			sampler2D _Sampler6092;
			uniform float4 _DistortLineTexture1_ST;
			uniform float _ShiningUTiling1;
			uniform float _ShiningVTiling1;
			uniform float _ShiningUOffsetSpeed1;
			uniform float _ShiningVOffsetSpeed1;
			uniform sampler2D _UVDistortTexture;
			uniform float4 _UVDistortTexture_ST;
			uniform float _Texture0Emi;
			uniform float4 _DistortLineColor;
			uniform sampler2D _EdgeTexture;
			uniform float _EdgeMaskSize;
			uniform float _EdgeStrength;
			uniform sampler2D _MaskTexture;
			uniform float _MaskTexturePow;

			
			v2f vert( appdata_t IN  )
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID( IN );
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				UNITY_TRANSFER_INSTANCE_ID(IN, OUT);
				OUT.worldPosition = IN.vertex;
				
				
				OUT.worldPosition.xyz +=  float3( 0, 0, 0 ) ;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

				OUT.texcoord = IN.texcoord;
				
				OUT.color = IN.color * _Color;
				return OUT;
			}

			fixed4 frag(v2f IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				float2 temp_output_1_0_g1 = float2( 1,1 );
				float2 texCoord80_g1 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 appendResult10_g1 = (float2(( (temp_output_1_0_g1).x * texCoord80_g1.x ) , ( texCoord80_g1.y * (temp_output_1_0_g1).y )));
				float2 uv_DistortLineTexture1 = IN.texcoord.xy * _DistortLineTexture1_ST.xy + _DistortLineTexture1_ST.zw;
				float2 temp_cast_0 = (tex2D( _DistortLineTexture1, uv_DistortLineTexture1 ).r).xx;
				float2 temp_output_11_0_g1 = temp_cast_0;
				float2 texCoord81_g1 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 panner18_g1 = ( ( (temp_output_11_0_g1).x * _Time.y ) * float2( 1,0 ) + texCoord81_g1);
				float2 panner19_g1 = ( ( _Time.y * (temp_output_11_0_g1).y ) * float2( 0,1 ) + texCoord81_g1);
				float2 appendResult24_g1 = (float2((panner18_g1).x , (panner19_g1).y));
				float2 appendResult90 = (float2(_ShiningUTiling1 , _ShiningVTiling1));
				float2 appendResult91 = (float2(_ShiningUOffsetSpeed1 , _ShiningVOffsetSpeed1));
				float2 temp_output_47_0_g1 = appendResult91;
				float2 texCoord22 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 panner21 = ( 0.1 * _Time.y * float2( 0,0 ) + ( texCoord22 - ( ( texCoord22 + float2( 0.5,0.5 ) ) * -0.5 ) ));
				float2 uv_UVDistortTexture = IN.texcoord.xy * _UVDistortTexture_ST.xy + _UVDistortTexture_ST.zw;
				float2 panner33 = ( 0.007 * _Time.y * float2( -0.5,0.5 ) + uv_UVDistortTexture);
				float2 panner37 = ( 0.011 * _Time.y * float2( 0.5,-0.5 ) + uv_UVDistortTexture);
				float2 panner39 = ( 0.013 * _Time.y * float2( -0.5,-0.5 ) + uv_UVDistortTexture);
				float2 panner41 = ( 0.019 * _Time.y * float2( 0.5,0.5 ) + uv_UVDistortTexture);
				float2 temp_output_31_0_g1 = ( ( panner21 + ( (-1.0 + (( ( tex2D( _UVDistortTexture, panner33 ).r + tex2D( _UVDistortTexture, panner37 ).r + tex2D( _UVDistortTexture, panner39 ).r + tex2D( _UVDistortTexture, panner41 ).r ) / 4.0 ) - 0.0) * (1.0 - -1.0) / (1.0 - 0.0)) * 0.04 ) ) - float2( 1,1 ) );
				float2 appendResult39_g1 = (float2(frac( ( atan2( (temp_output_31_0_g1).x , (temp_output_31_0_g1).y ) / 6.28318548202515 ) ) , length( temp_output_31_0_g1 )));
				float2 panner54_g1 = ( ( (temp_output_47_0_g1).x * _Time.y ) * float2( 1,0 ) + appendResult39_g1);
				float2 panner55_g1 = ( ( _Time.y * (temp_output_47_0_g1).y ) * float2( 0,1 ) + appendResult39_g1);
				float2 appendResult58_g1 = (float2((panner54_g1).x , (panner55_g1).y));
				float4 tex2DNode8 = tex2D( _DistortLineTexture1, ( ( (tex2D( _Sampler6092, ( appendResult10_g1 + appendResult24_g1 ) )).rg * 4.2 ) + ( appendResult90 * appendResult58_g1 ) ) );
				float3 appendResult101 = (float3(tex2DNode8.r , tex2DNode8.g , tex2DNode8.b));
				float4 appendResult103 = (float4(( appendResult101 * _Texture0Emi ) , tex2DNode8.a));
				float2 texCoord57 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 temp_output_58_0 = ( texCoord57 + ( ( texCoord57 - float2( 0.5,0.5 ) ) * _EdgeMaskSize ) );
				float4 tex2DNode56 = tex2D( _EdgeTexture, temp_output_58_0 );
				float4 appendResult48 = (float4(_DistortLineColor.r , _DistortLineColor.g , _DistortLineColor.b , saturate( ( _DistortLineColor.a * saturate( ( tex2DNode56.a * _EdgeStrength * tex2DNode56.r ) ) * saturate( pow( tex2D( _MaskTexture, temp_output_58_0 ).r , _MaskTexturePow ) ) ) )));
				
				half4 color = ( appendResult103 * appendResult48 * IN.color );
				
				#ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif
				
				#ifdef UNITY_UI_ALPHACLIP
				clip (color.a - 0.001);
				#endif

				return color;
			}
		ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=18900
0;73.6;725.4;719;1257.182;409.0143;2.419117;False;False
Node;AmplifyShaderEditor.TextureCoordinatesNode;32;-3567.668,655.6031;Inherit;False;0;17;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;33;-2938.506,665.1362;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-0.5,0.5;False;1;FLOAT;0.007;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;37;-2936.242,800.1263;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.5,-0.5;False;1;FLOAT;0.011;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;39;-2932.826,1009.296;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-0.5,-0.5;False;1;FLOAT;0.013;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;41;-2933.234,1273.708;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.5,0.5;False;1;FLOAT;0.019;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TexturePropertyNode;17;-3566.908,418.0446;Inherit;True;Property;_UVDistortTexture;UV Distort Texture;7;0;Create;True;0;0;0;False;0;False;6a41d0f6780330041b8f0ee7f45b34a5;be38a29260023bc49a6e4d34d32d8b0a;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.SamplerNode;42;-2709.565,1156.752;Inherit;True;Property;_TextureSample6;Texture Sample 6;7;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;28;-2715.246,416.1896;Inherit;True;Property;_TextureSample3;Texture Sample 3;7;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;22;-1774.576,808.5087;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;34;-2714.988,661.5818;Inherit;True;Property;_TextureSample4;Texture Sample 4;7;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;40;-2711.572,895.963;Inherit;True;Property;_TextureSample5;Texture Sample 5;7;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;27;-1589.58,1077.606;Inherit;False;Constant;_Float0;Float 0;7;0;Create;True;0;0;0;False;0;False;-0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;24;-1558.521,945.2809;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;43;-2352.781,764.6636;Inherit;True;4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;57;-1521.568,1396.997;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;59;-1336.572,1666.094;Inherit;False;Property;_EdgeMaskSize;Edge Mask Size;11;0;Create;True;0;0;0;False;0;False;1.56;2.37;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;61;-1235.25,1491.175;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;44;-2135.016,764.3992;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;4;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;25;-1360.544,1058.692;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;26;-1162.634,1045.643;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;30;-1265.256,629.0929;Inherit;False;Constant;_Float1;Float 1;7;0;Create;True;0;0;0;False;0;False;0.04;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;29;-2018.034,446.7597;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-1;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;60;-1107.536,1647.181;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;93;-1441.585,-616.116;Inherit;False;Property;_ShiningUTiling1;Shining U Tiling 1;0;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;58;-836.9315,1406.697;Inherit;True;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TexturePropertyNode;80;-679.1573,875.4285;Inherit;True;Property;_EdgeTexture;Edge Texture;10;0;Create;True;0;0;0;False;0;False;27f51f4a0d8df5e4082a56c4888809cd;27f51f4a0d8df5e4082a56c4888809cd;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.RangedFloatNode;87;-1499.149,-402.4552;Inherit;False;Property;_ShiningVOffsetSpeed1;Shining V OffsetSpeed 1;3;0;Create;True;0;0;0;False;0;False;-1;-0.05;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TexturePropertyNode;10;-2013.558,-638.725;Inherit;True;Property;_DistortLineTexture1;Distort Line Texture 1;4;0;Create;True;0;0;0;False;0;False;86efb85310c1d184e930833778dc6be7;25ad72a0acb9f4c4cb927d8509cdbda2;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;23;-1166.357,451.6912;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;88;-1483.149,-480.4554;Inherit;False;Property;_ShiningUOffsetSpeed1;Shining U OffsetSpeed 1;2;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;89;-1438.585,-548.1163;Inherit;False;Property;_ShiningVTiling1;Shining V Tiling 1;1;0;Create;True;0;0;0;False;0;False;0.01;-4.2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;21;-1052.188,779.9274;Inherit;True;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;0.1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;45;-800.2479,476.9336;Inherit;True;Property;_MaskTexture;Mask Texture;8;0;Create;True;0;0;0;False;0;False;-1;None;2948c8b7491dfb448aaec5ff3f5c6942;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;56;-391.3789,873.9329;Inherit;True;Property;_Edge;Edge;11;0;Create;True;0;0;0;False;0;False;-1;27f51f4a0d8df5e4082a56c4888809cd;27f51f4a0d8df5e4082a56c4888809cd;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;54;-773.4967,736.3953;Inherit;False;Property;_MaskTexturePow;Mask Texture Pow;9;0;Create;True;0;0;0;False;0;False;3.32;2.4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;11;-1765.119,-637.0468;Inherit;True;Property;_TextureSample2;Texture Sample 2;4;0;Create;True;0;0;0;False;0;False;-1;86efb85310c1d184e930833778dc6be7;86efb85310c1d184e930833778dc6be7;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;65;-238.6575,1166.752;Inherit;False;Property;_EdgeStrength;Edge Strength;13;0;Create;True;0;0;0;False;0;False;1;25.94;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;91;-1243.15,-502.4554;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;90;-1244.586,-597.116;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;31;-887.0738,287.213;Inherit;True;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FunctionNode;92;-1090.011,-660.6862;Inherit;True;RadialUVDistortion;-1;;1;4b37d97e760456a4aa61974994d5a288;0;8;87;FLOAT2;0,0;False;60;SAMPLER2D;_Sampler6092;False;1;FLOAT2;1,1;False;11;FLOAT2;0,0;False;65;FLOAT;4.2;False;68;FLOAT2;1,1;False;47;FLOAT2;1,1;False;29;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;64;70.24474,900.6514;Inherit;True;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;55;-480.9666,599.0858;Inherit;True;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;8;-616.9271,-633.4429;Inherit;True;Property;_TextureSample1;Texture Sample 1;4;0;Create;True;0;0;0;False;0;False;-1;86efb85310c1d184e930833778dc6be7;86efb85310c1d184e930833778dc6be7;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;14;-303.4265,270.6917;Inherit;False;Property;_DistortLineColor;Distort Line Color;6;1;[HDR];Create;True;0;0;0;False;0;False;3.953349,1.345381,0.1655853,1;0.9221513,0.3186492,0.03862414,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;100;-85.59847,670.5571;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;99;-221.3083,519.9342;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;75;-494.8235,-212.4972;Inherit;False;Property;_Texture0Emi;Texture 0 Emi;5;0;Create;True;0;0;0;False;0;False;10;8.83;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;47;-53.62078,369.6417;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;101;-281.0125,-601.8951;Inherit;False;FLOAT3;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;102;-142.0572,-340.1541;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;95;120.6862,369.4437;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;48;294.1452,297.7465;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.VertexColorNode;12;122.1017,-308.0331;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;103;21.45529,-152.9525;Inherit;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;476.6877,15.28166;Inherit;False;3;3;0;FLOAT4;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;2;COLOR;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.StaticSwitch;94;643.4418,618.0153;Inherit;True;Property;_NoAddEdgeColor;No Add Edge Color;15;0;Create;True;0;0;0;False;0;False;0;1;1;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;84;-1322.453,2182.362;Inherit;False;Property;_TrueEdgeSize;True Edge Size;12;0;Create;True;0;0;0;False;0;False;1.56;-0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;86;-822.8133,1922.966;Inherit;True;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;85;-1093.418,2163.449;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ColorNode;79;100.2686,630.5823;Inherit;False;Property;_TrueEdgeColor;True Edge Color;14;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;81;-384.3122,1368.262;Inherit;True;Property;_TextureSample8;Texture Sample 8;11;0;Create;True;0;0;0;False;0;False;-1;27f51f4a0d8df5e4082a56c4888809cd;27f51f4a0d8df5e4082a56c4888809cd;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;38;-1329.1,738.006;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;82;-1507.449,1913.266;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;76;387.9759,621.1101;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;83;-1221.131,2007.444;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;1151.964,13.81565;Float;False;True;-1;2;ASEMaterialInspector;0;6;vfx_UIOutDistortLine;5056123faa0c79b47ab6ad7e8bf059a4;True;Default;0;0;Default;2;True;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;True;True;True;True;True;0;True;-9;False;False;False;False;False;False;False;True;True;0;True;-5;255;True;-8;255;True;-7;0;True;-4;0;True;-6;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;2;False;-1;True;0;True;-11;False;True;5;Queue=Transparent=Queue=0;IgnoreProjector=True;RenderType=Transparent=RenderType;PreviewType=Plane;CanUseSpriteAtlas=True;False;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;0;;0;0;Standard;0;0;1;True;False;;False;0
WireConnection;33;0;32;0
WireConnection;37;0;32;0
WireConnection;39;0;32;0
WireConnection;41;0;32;0
WireConnection;42;0;17;0
WireConnection;42;1;41;0
WireConnection;28;0;17;0
WireConnection;28;1;33;0
WireConnection;34;0;17;0
WireConnection;34;1;37;0
WireConnection;40;0;17;0
WireConnection;40;1;39;0
WireConnection;24;0;22;0
WireConnection;43;0;28;1
WireConnection;43;1;34;1
WireConnection;43;2;40;1
WireConnection;43;3;42;1
WireConnection;61;0;57;0
WireConnection;44;0;43;0
WireConnection;25;0;24;0
WireConnection;25;1;27;0
WireConnection;26;0;22;0
WireConnection;26;1;25;0
WireConnection;29;0;44;0
WireConnection;60;0;61;0
WireConnection;60;1;59;0
WireConnection;58;0;57;0
WireConnection;58;1;60;0
WireConnection;23;0;29;0
WireConnection;23;1;30;0
WireConnection;21;0;26;0
WireConnection;45;1;58;0
WireConnection;56;0;80;0
WireConnection;56;1;58;0
WireConnection;11;0;10;0
WireConnection;91;0;88;0
WireConnection;91;1;87;0
WireConnection;90;0;93;0
WireConnection;90;1;89;0
WireConnection;31;0;21;0
WireConnection;31;1;23;0
WireConnection;92;11;11;1
WireConnection;92;68;90;0
WireConnection;92;47;91;0
WireConnection;92;29;31;0
WireConnection;64;0;56;4
WireConnection;64;1;65;0
WireConnection;64;2;56;1
WireConnection;55;0;45;1
WireConnection;55;1;54;0
WireConnection;8;0;10;0
WireConnection;8;1;92;0
WireConnection;100;0;64;0
WireConnection;99;0;55;0
WireConnection;47;0;14;4
WireConnection;47;1;100;0
WireConnection;47;2;99;0
WireConnection;101;0;8;1
WireConnection;101;1;8;2
WireConnection;101;2;8;3
WireConnection;102;0;101;0
WireConnection;102;1;75;0
WireConnection;95;0;47;0
WireConnection;48;0;14;1
WireConnection;48;1;14;2
WireConnection;48;2;14;3
WireConnection;48;3;95;0
WireConnection;103;0;102;0
WireConnection;103;3;8;4
WireConnection;13;0;103;0
WireConnection;13;1;48;0
WireConnection;13;2;12;0
WireConnection;94;1;76;0
WireConnection;86;0;82;0
WireConnection;86;1;85;0
WireConnection;85;0;83;0
WireConnection;85;1;84;0
WireConnection;81;0;80;0
WireConnection;81;1;86;0
WireConnection;76;0;81;1
WireConnection;76;1;79;0
WireConnection;83;0;82;0
WireConnection;0;0;13;0
ASEEND*/
//CHKSM=5E5264A82D5B2F86EDE0511EFC39B3AF6220AC8F