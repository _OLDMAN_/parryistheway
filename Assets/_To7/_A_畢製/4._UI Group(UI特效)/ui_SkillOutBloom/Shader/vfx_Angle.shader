// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "vfx_Angle"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
		_AllControl("All Control", Range( 0 , 1)) = 0
		_FourAngleTexture("FourAngle Texture", 2D) = "white" {}
		[HDR]_Color0("Color 0", Color) = (1,1,1,1)
		_Mask04("Mask04", 2D) = "white" {}
		_TrueEdgeSize("True Edge Size", Float) = 1.56
		_CircleTextureSize("Circle Texture Size", Float) = 1.56
		_FrameTextureSize("Frame Texture Size", Float) = 1.56
		_Texture03Size("Texture03 Size", Float) = 1.56
		_Mask05("Mask05", 2D) = "white" {}
		_FlameArtwork01("FlameArtwork01", 2D) = "white" {}
		_Texture03("流光03", 2D) = "white" {}
		[HDR]_SmokeColor("Smoke Color", Color) = (1,1,1,1)
		_FrameTexture03FlowSpeed("Frame / Texture03 Flow Speed", Vector) = (0,0,0,0)
		_Texture03NoiseStrength("Texture03 Noise Strength", Float) = 1
		_DistortSpeed("Distort Speed", Float) = 1
		_CircleMaskTexture("Circle Mask Texture", 2D) = "white" {}
		_CircleTextureLum("CircleTexture Lum", Float) = 1
		_FourAngleYAdd("FourAngle Y Add", Float) = 0

	}

	SubShader
	{
		LOD 0

		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
		
		Stencil
		{
			Ref [_Stencil]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			CompFront [_StencilComp]
			PassFront [_StencilOp]
			FailFront Keep
			ZFailFront Keep
			CompBack Always
			PassBack Keep
			FailBack Keep
			ZFailBack Keep
		}


		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask [_ColorMask]

		
		Pass
		{
			Name "Default"
		CGPROGRAM
			
			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			#pragma multi_compile __ UNITY_UI_CLIP_RECT
			#pragma multi_compile __ UNITY_UI_ALPHACLIP
			
			#include "UnityShaderVariables.cginc"
			#define ASE_NEEDS_FRAG_COLOR

			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				
			};
			
			uniform fixed4 _Color;
			uniform fixed4 _TextureSampleAdd;
			uniform float4 _ClipRect;
			uniform sampler2D _MainTex;
			uniform sampler2D _Mask04;
			uniform float _AllControl;
			uniform sampler2D _FourAngleTexture;
			uniform float4 _FourAngleTexture_ST;
			uniform float _TrueEdgeSize;
			uniform float _FourAngleYAdd;
			uniform float4 _Color0;
			uniform sampler2D _FlameArtwork01;
			uniform float2 _FrameTexture03FlowSpeed;
			uniform float4 _FlameArtwork01_ST;
			uniform float _FrameTextureSize;
			uniform sampler2D _Texture03;
			uniform float _DistortSpeed;
			uniform float _Texture03Size;
			uniform float _Texture03NoiseStrength;
			uniform sampler2D _CircleMaskTexture;
			uniform float4 _CircleMaskTexture_ST;
			uniform float _CircleTextureSize;
			uniform float _CircleTextureLum;
			uniform sampler2D _Mask05;
			uniform float4 _SmokeColor;
			inline float noise_randomValue (float2 uv) { return frac(sin(dot(uv, float2(12.9898, 78.233)))*43758.5453); }
			inline float noise_interpolate (float a, float b, float t) { return (1.0-t)*a + (t*b); }
			inline float valueNoise (float2 uv)
			{
				float2 i = floor(uv);
				float2 f = frac( uv );
				f = f* f * (3.0 - 2.0 * f);
				uv = abs( frac(uv) - 0.5);
				float2 c0 = i + float2( 0.0, 0.0 );
				float2 c1 = i + float2( 1.0, 0.0 );
				float2 c2 = i + float2( 0.0, 1.0 );
				float2 c3 = i + float2( 1.0, 1.0 );
				float r0 = noise_randomValue( c0 );
				float r1 = noise_randomValue( c1 );
				float r2 = noise_randomValue( c2 );
				float r3 = noise_randomValue( c3 );
				float bottomOfGrid = noise_interpolate( r0, r1, f.x );
				float topOfGrid = noise_interpolate( r2, r3, f.x );
				float t = noise_interpolate( bottomOfGrid, topOfGrid, f.y );
				return t;
			}
			
			float SimpleNoise(float2 UV)
			{
				float t = 0.0;
				float freq = pow( 2.0, float( 0 ) );
				float amp = pow( 0.5, float( 3 - 0 ) );
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(1));
				amp = pow(0.5, float(3-1));
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(2));
				amp = pow(0.5, float(3-2));
				t += valueNoise( UV/freq )*amp;
				return t;
			}
			
			float3 mod2D289( float3 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float2 mod2D289( float2 x ) { return x - floor( x * ( 1.0 / 289.0 ) ) * 289.0; }
			float3 permute( float3 x ) { return mod2D289( ( ( x * 34.0 ) + 1.0 ) * x ); }
			float snoise( float2 v )
			{
				const float4 C = float4( 0.211324865405187, 0.366025403784439, -0.577350269189626, 0.024390243902439 );
				float2 i = floor( v + dot( v, C.yy ) );
				float2 x0 = v - i + dot( i, C.xx );
				float2 i1;
				i1 = ( x0.x > x0.y ) ? float2( 1.0, 0.0 ) : float2( 0.0, 1.0 );
				float4 x12 = x0.xyxy + C.xxzz;
				x12.xy -= i1;
				i = mod2D289( i );
				float3 p = permute( permute( i.y + float3( 0.0, i1.y, 1.0 ) ) + i.x + float3( 0.0, i1.x, 1.0 ) );
				float3 m = max( 0.5 - float3( dot( x0, x0 ), dot( x12.xy, x12.xy ), dot( x12.zw, x12.zw ) ), 0.0 );
				m = m * m;
				m = m * m;
				float3 x = 2.0 * frac( p * C.www ) - 1.0;
				float3 h = abs( x ) - 0.5;
				float3 ox = floor( x + 0.5 );
				float3 a0 = x - ox;
				m *= 1.79284291400159 - 0.85373472095314 * ( a0 * a0 + h * h );
				float3 g;
				g.x = a0.x * x0.x + h.x * x0.y;
				g.yz = a0.yz * x12.xz + h.yz * x12.yw;
				return 130.0 * dot( m, g );
			}
			
			struct Gradient
			{
				int type;
				int colorsLength;
				int alphasLength;
				float4 colors[8];
				float2 alphas[8];
			};
			
			Gradient NewGradient(int type, int colorsLength, int alphasLength, 
			float4 colors0, float4 colors1, float4 colors2, float4 colors3, float4 colors4, float4 colors5, float4 colors6, float4 colors7,
			float2 alphas0, float2 alphas1, float2 alphas2, float2 alphas3, float2 alphas4, float2 alphas5, float2 alphas6, float2 alphas7)
			{
				Gradient g;
				g.type = type;
				g.colorsLength = colorsLength;
				g.alphasLength = alphasLength;
				g.colors[ 0 ] = colors0;
				g.colors[ 1 ] = colors1;
				g.colors[ 2 ] = colors2;
				g.colors[ 3 ] = colors3;
				g.colors[ 4 ] = colors4;
				g.colors[ 5 ] = colors5;
				g.colors[ 6 ] = colors6;
				g.colors[ 7 ] = colors7;
				g.alphas[ 0 ] = alphas0;
				g.alphas[ 1 ] = alphas1;
				g.alphas[ 2 ] = alphas2;
				g.alphas[ 3 ] = alphas3;
				g.alphas[ 4 ] = alphas4;
				g.alphas[ 5 ] = alphas5;
				g.alphas[ 6 ] = alphas6;
				g.alphas[ 7 ] = alphas7;
				return g;
			}
			
			float4 SampleGradient( Gradient gradient, float time )
			{
				float3 color = gradient.colors[0].rgb;
				UNITY_UNROLL
				for (int c = 1; c < 8; c++)
				{
				float colorPos = saturate((time - gradient.colors[c-1].w) / ( 0.00001 + (gradient.colors[c].w - gradient.colors[c-1].w)) * step(c, (float)gradient.colorsLength-1));
				color = lerp(color, gradient.colors[c].rgb, lerp(colorPos, step(0.01, colorPos), gradient.type));
				}
				#ifndef UNITY_COLORSPACE_GAMMA
				color = half3(GammaToLinearSpaceExact(color.r), GammaToLinearSpaceExact(color.g), GammaToLinearSpaceExact(color.b));
				#endif
				float alpha = gradient.alphas[0].x;
				UNITY_UNROLL
				for (int a = 1; a < 8; a++)
				{
				float alphaPos = saturate((time - gradient.alphas[a-1].y) / ( 0.00001 + (gradient.alphas[a].y - gradient.alphas[a-1].y)) * step(a, (float)gradient.alphasLength-1));
				alpha = lerp(alpha, gradient.alphas[a].x, lerp(alphaPos, step(0.01, alphaPos), gradient.type));
				}
				return float4(color, alpha);
			}
			

			
			v2f vert( appdata_t IN  )
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID( IN );
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				UNITY_TRANSFER_INSTANCE_ID(IN, OUT);
				OUT.worldPosition = IN.vertex;
				
				
				OUT.worldPosition.xyz +=  float3( 0, 0, 0 ) ;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

				OUT.texcoord = IN.texcoord;
				
				OUT.color = IN.color * _Color;
				return OUT;
			}

			fixed4 frag(v2f IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				float2 temp_cast_0 = ((-8.4 + (_AllControl - 0.0) * (-11.0 - -8.4) / (1.0 - 0.0))).xx;
				float2 texCoord7 = IN.texcoord.xy * float2( 1,15 ) + temp_cast_0;
				float2 uv_FourAngleTexture = IN.texcoord.xy * _FourAngleTexture_ST.xy + _FourAngleTexture_ST.zw;
				float2 temp_output_15_0 = ( uv_FourAngleTexture + ( ( uv_FourAngleTexture - float2( 0.5,0.5 ) ) * _TrueEdgeSize ) );
				float2 appendResult73 = (float2(0.0 , _FourAngleYAdd));
				float4 tex2DNode1 = tex2D( _FourAngleTexture, ( temp_output_15_0 + appendResult73 ) );
				float4 appendResult71 = (float4(tex2DNode1.r , tex2DNode1.g , tex2DNode1.b , tex2DNode1.r));
				float2 panner20 = ( 1.0 * _Time.y * float2( 0,-0.98 ) + temp_output_15_0);
				float simpleNoise18 = SimpleNoise( panner20*5.82 );
				float2 appendResult45 = (float2(0.0 , _FrameTexture03FlowSpeed.y));
				float2 uv_FlameArtwork01 = IN.texcoord.xy * _FlameArtwork01_ST.xy + _FlameArtwork01_ST.zw;
				float2 panner26 = ( 1.0 * _Time.y * appendResult45 + ( uv_FlameArtwork01 + ( ( uv_FlameArtwork01 - float2( 0.5,0.5 ) ) * _FrameTextureSize ) ));
				float2 appendResult46 = (float2(0.0 , _FrameTexture03FlowSpeed.x));
				float2 appendResult57 = (float2(_DistortSpeed , 0.0));
				float2 texCoord39 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 temp_output_43_0 = ( texCoord39 + ( ( texCoord39 - float2( 0.5,0.5 ) ) * _Texture03Size ) );
				float2 panner54 = ( 1.0 * _Time.y * appendResult57 + temp_output_43_0);
				float2 panner53 = ( 1.0 * _Time.y * -appendResult57 + temp_output_43_0);
				float simplePerlin2D50 = snoise( ( ( panner54 + panner53 ) / float2( 2,2 ) )*15.84 );
				simplePerlin2D50 = simplePerlin2D50*0.5 + 0.5;
				float2 panner28 = ( 1.0 * _Time.y * appendResult46 + ( ( (0.0 + (simplePerlin2D50 - 0.0) * (0.1 - 0.0) / (1.0 - 0.0)) * _Texture03NoiseStrength ) + temp_output_43_0 ));
				float2 uv_CircleMaskTexture = IN.texcoord.xy * _CircleMaskTexture_ST.xy + _CircleMaskTexture_ST.zw;
				Gradient gradient79 = NewGradient( 0, 3, 2, float4( 0, 0, 0, 0 ), float4( 1, 1, 1, 0.1433127 ), float4( 0, 0, 0, 1 ), 0, 0, 0, 0, 0, float2( 1, 0 ), float2( 1, 1 ), 0, 0, 0, 0, 0, 0 );
				
				half4 color = ( IN.color * ( ( tex2D( _Mask04, texCoord7 ).r * ( appendResult71 * _Color0 ) ) + ( ( simpleNoise18 * ( ( tex2D( _FlameArtwork01, panner26 ).r + tex2D( _Texture03, panner28 ).r ) * 7.45 ) * ( ( ( tex2D( _CircleMaskTexture, ( uv_CircleMaskTexture + ( ( uv_CircleMaskTexture - float2( 0.5,0.5 ) ) * _CircleTextureSize ) ) ).r * _CircleTextureLum ) + tex2D( _Mask05, temp_output_15_0 ).r ) / 2.0 ) * _SmokeColor ) * SampleGradient( gradient79, _AllControl ).r ) ) );
				
				#ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif
				
				#ifdef UNITY_UI_ALPHACLIP
				clip (color.a - 0.001);
				#endif

				return color;
			}
		ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=18900
0;73.6;725.4;498.2;2122.841;378.582;2.783989;True;False
Node;AmplifyShaderEditor.TextureCoordinatesNode;39;-4402.503,1484.306;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;41;-4116.186,1578.484;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;40;-4217.508,1753.402;Inherit;False;Property;_Texture03Size;Texture03 Size;7;0;Create;True;0;0;0;False;0;False;1.56;-0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;58;-3922.456,1298.556;Inherit;False;Property;_DistortSpeed;Distort Speed;14;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;57;-3727.192,1279.613;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;42;-3988.47,1734.489;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.NegateNode;59;-3682.024,1393.272;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;43;-3841.092,1491.147;Inherit;True;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;53;-3495.422,1381.992;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;-0.5,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;54;-3495.893,1267.76;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.5,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;55;-3303.515,1308.589;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;56;-3193.515,1408.589;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;2,2;False;1;FLOAT2;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;50;-3141.198,1282.796;Inherit;True;Simplex2D;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;15.84;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;61;-3160.914,438.3021;Inherit;False;0;60;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;34;-3261.774,902.9305;Inherit;False;0;21;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TFHCRemapNode;51;-2957.86,1285.821;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;36;-2975.456,997.1085;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;63;-2874.596,532.4801;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;49;-3016.246,1450.55;Inherit;False;Property;_Texture03NoiseStrength;Texture03 Noise Strength;13;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;62;-2975.918,707.3983;Inherit;False;Property;_CircleTextureSize;Circle Texture Size;5;0;Create;True;0;0;0;False;0;False;1.56;-0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;35;-3076.778,1172.027;Inherit;False;Property;_FrameTextureSize;Frame Texture Size;6;0;Create;True;0;0;0;False;0;False;1.56;-0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;11;-3145.914,29.8597;Inherit;False;0;1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;12;-2960.918,298.9559;Inherit;False;Property;_TrueEdgeSize;True Edge Size;4;0;Create;True;0;0;0;False;0;False;1.56;-0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;13;-2859.596,124.0377;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.Vector2Node;44;-2631.809,1191.662;Inherit;False;Property;_FrameTexture03FlowSpeed;Frame / Texture03 Flow Speed;12;0;Create;True;0;0;0;False;0;False;0,0;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;64;-2746.881,688.4852;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;37;-2847.741,1153.114;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;48;-2729.996,1464.292;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;74;-2389.945,290.4027;Inherit;False;Property;_FourAngleYAdd;FourAngle Y Add;17;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;65;-2510.044,428.3031;Inherit;True;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;45;-2312.923,1252.724;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;46;-2310.649,1514.581;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;52;-2535.467,1464.789;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;14;-2731.881,280.0428;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;38;-2577.136,912.6305;Inherit;True;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;26;-2161.022,1224.586;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,-0.98;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;60;-1859.938,452.1248;Inherit;True;Property;_CircleMaskTexture;Circle Mask Texture;15;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;70;-1570.942,655.9451;Inherit;False;Property;_CircleTextureLum;CircleTexture Lum;16;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;73;-2187.646,199.3009;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;28;-2157.481,1465.195;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,-0.98;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;15;-2461.276,39.55965;Inherit;True;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0.5,0.5;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;72;-2032.237,54.60962;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;69;-1501.175,483.1893;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;23;-1864.664,1462.69;Inherit;True;Property;_Texture03;流光03;10;0;Create;False;0;0;0;False;0;False;-1;58dfeb2bb9db345408903716e2ce1811;58dfeb2bb9db345408903716e2ce1811;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;76;-2122.68,-335.9676;Inherit;False;Property;_AllControl;All Control;0;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;16;-1860.834,699.08;Inherit;True;Property;_Mask05;Mask05;8;0;Create;True;0;0;0;False;0;False;-1;aee7a379bb8485840a2ce40a3349f40b;aee7a379bb8485840a2ce40a3349f40b;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;21;-1860.562,1217.433;Inherit;True;Property;_FlameArtwork01;FlameArtwork01;9;0;Create;True;0;0;0;False;0;False;-1;25ad72a0acb9f4c4cb927d8509cdbda2;25ad72a0acb9f4c4cb927d8509cdbda2;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;1;-1681.844,-54.56116;Inherit;True;Property;_FourAngleTexture;FourAngle Texture;1;0;Create;True;0;0;0;False;0;False;-1;6f968f96648019743b57c6f8e96ea0eb;6f968f96648019743b57c6f8e96ea0eb;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;67;-1445.26,702.968;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;75;-1721.955,-329.1588;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-8.4;False;4;FLOAT;-11;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;24;-1528.474,1355.248;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;20;-1969.029,928.4353;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,-0.98;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ColorNode;32;-1550.929,1116.019;Inherit;False;Property;_SmokeColor;Smoke Color;11;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;7;-1483.074,-331.9837;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,15;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;4;-1397.457,233.1772;Inherit;False;Property;_Color0;Color 0;2;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,1;0,0,0,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;71;-1296.47,-26.52161;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;68;-1225.05,703.9536;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;2;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;-1372.544,1355.136;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;7.45;False;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;18;-1761.752,924.1779;Inherit;True;Simple;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;5.82;False;1;FLOAT;0
Node;AmplifyShaderEditor.GradientNode;79;-1025.577,293.6371;Inherit;False;0;3;2;0,0,0,0;1,1,1,0.1433127;0,0,0,1;1,0;1,1;0;1;OBJECT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;17;-920.8713,932.0518;Inherit;True;4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;6;-1054.6,-354.9627;Inherit;True;Property;_Mask04;Mask04;3;0;Create;True;0;0;0;False;0;False;-1;c4196ff22c48bea4ea62dabb2e873762;c4196ff22c48bea4ea62dabb2e873762;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GradientSampleNode;80;-697.4807,294.8836;Inherit;True;2;0;OBJECT;;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;3;-1089.336,-53.40871;Inherit;True;2;2;0;FLOAT4;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;-506.7775,-76.30602;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT4;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;77;-370.5471,152.1264;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;30;-246.9708,-76.58951;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.VertexColorNode;2;-505.415,-304.5706;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;33;-52.13342,-116.4723;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT4;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.OneMinusNode;78;-780.5651,187.1948;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;540.4616,-107.5148;Float;False;True;-1;2;ASEMaterialInspector;0;6;vfx_Angle;5056123faa0c79b47ab6ad7e8bf059a4;True;Default;0;0;Default;2;False;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;True;True;True;True;True;0;True;-9;False;False;False;False;False;False;False;True;True;0;True;-5;255;True;-8;255;True;-7;0;True;-4;0;True;-6;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;2;False;-1;True;0;True;-11;False;True;5;Queue=Transparent=Queue=0;IgnoreProjector=True;RenderType=Transparent=RenderType;PreviewType=Plane;CanUseSpriteAtlas=True;False;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;0;;0;0;Standard;0;0;1;True;False;;False;0
WireConnection;41;0;39;0
WireConnection;57;0;58;0
WireConnection;42;0;41;0
WireConnection;42;1;40;0
WireConnection;59;0;57;0
WireConnection;43;0;39;0
WireConnection;43;1;42;0
WireConnection;53;0;43;0
WireConnection;53;2;59;0
WireConnection;54;0;43;0
WireConnection;54;2;57;0
WireConnection;55;0;54;0
WireConnection;55;1;53;0
WireConnection;56;0;55;0
WireConnection;50;0;56;0
WireConnection;51;0;50;0
WireConnection;36;0;34;0
WireConnection;63;0;61;0
WireConnection;13;0;11;0
WireConnection;64;0;63;0
WireConnection;64;1;62;0
WireConnection;37;0;36;0
WireConnection;37;1;35;0
WireConnection;48;0;51;0
WireConnection;48;1;49;0
WireConnection;65;0;61;0
WireConnection;65;1;64;0
WireConnection;45;1;44;2
WireConnection;46;1;44;1
WireConnection;52;0;48;0
WireConnection;52;1;43;0
WireConnection;14;0;13;0
WireConnection;14;1;12;0
WireConnection;38;0;34;0
WireConnection;38;1;37;0
WireConnection;26;0;38;0
WireConnection;26;2;45;0
WireConnection;60;1;65;0
WireConnection;73;1;74;0
WireConnection;28;0;52;0
WireConnection;28;2;46;0
WireConnection;15;0;11;0
WireConnection;15;1;14;0
WireConnection;72;0;15;0
WireConnection;72;1;73;0
WireConnection;69;0;60;1
WireConnection;69;1;70;0
WireConnection;23;1;28;0
WireConnection;16;1;15;0
WireConnection;21;1;26;0
WireConnection;1;1;72;0
WireConnection;67;0;69;0
WireConnection;67;1;16;1
WireConnection;75;0;76;0
WireConnection;24;0;21;1
WireConnection;24;1;23;1
WireConnection;20;0;15;0
WireConnection;7;1;75;0
WireConnection;71;0;1;1
WireConnection;71;1;1;2
WireConnection;71;2;1;3
WireConnection;71;3;1;1
WireConnection;68;0;67;0
WireConnection;29;0;24;0
WireConnection;18;0;20;0
WireConnection;17;0;18;0
WireConnection;17;1;29;0
WireConnection;17;2;68;0
WireConnection;17;3;32;0
WireConnection;6;1;7;0
WireConnection;80;0;79;0
WireConnection;80;1;76;0
WireConnection;3;0;71;0
WireConnection;3;1;4;0
WireConnection;8;0;6;1
WireConnection;8;1;3;0
WireConnection;77;0;17;0
WireConnection;77;1;80;1
WireConnection;30;0;8;0
WireConnection;30;1;77;0
WireConnection;33;0;2;0
WireConnection;33;1;30;0
WireConnection;0;0;33;0
ASEEND*/
//CHKSM=9042C49C00F9D817FF1528AE7DA0B41FE213643B