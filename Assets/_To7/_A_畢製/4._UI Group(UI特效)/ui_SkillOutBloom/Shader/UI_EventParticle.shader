// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "UI_EventParticle"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
		[Toggle(_ONLYEDGEEMISSION_ON)] _OnlyEdgeEmission("Only Edge Emission ?", Float) = 0
		[KeywordEnum(TouchMode,ClickMode)] _Mode("Mode", Float) = 0
		[HDR]_MainColor("Main Color", Color) = (1,1,1,1)
		_AllAlpha("AllAlpha", Range( 0 , 1)) = 1
		[Header(Mask Texture)]_Mask("Mask", 2D) = "white" {}
		_InnerMask("Inner Mask ", 2D) = "white" {}
		_MaskZoom("Mask Zoom", Float) = 0
		_MaskEmi("Mask Emi", Float) = 1
		[Header(Extra Texture)]_ExtraMask("Extra Mask", 2D) = "white" {}
		_ExtraMaskOffsetX("Extra Mask Offset X", Float) = 0
		_ExtraMaskOffsetY("Extra Mask Offset Y", Float) = 0
		_ExtraMaskZoomX("Extra Mask Zoom X", Float) = 0
		_ExtraMaskZoomY("Extra Mask Zoom Y", Float) = 0
		_ExtraMaskEmi("Extra Mask Emi", Float) = 1
		[Header(Noise 1)]_NoiseTexture1("NoiseTexture 1", 2D) = "white" {}
		_NoiseTexture1ZoomX("NoiseTexture 1  Zoom X", Float) = 0
		_NoiseTexture1ZoomY("NoiseTexture 1  Zoom Y", Float) = 0
		_NoiseTexture1FlowSpeed("NoiseTexture 1 FlowSpeed", Float) = 1
		_NoiseTexture1FlowSpeedHorizontal("NoiseTexture 1 FlowSpeed Horizontal", Float) = 0
		_NoiseTexture1Emission("NoiseTexture 1 Emission", Float) = 1
		[Header(Noise 2)]_NoiseTexture2("NoiseTexture 2", 2D) = "white" {}
		_NoiseTexture2ZoomX("NoiseTexture 2  Zoom X", Float) = 0
		_NoiseTexture2ZoomY("NoiseTexture 2  Zoom Y", Float) = 0
		_NoiseTexture2FlowSpeed("NoiseTexture 2 FlowSpeed", Float) = 1
		_NoiseTexture2FlowSpeedHorizontal("NoiseTexture 2 FlowSpeed Horizontal", Float) = 0
		_NoiseTexture2Emission("NoiseTexture 2 Emission", Float) = 1
		[Header(Noise Generator)]_NoiseGeneratorZoom("NoiseGenerator  Zoom", Float) = 0
		_NoiseGeneratorFlowSpeed("NoiseGenerator FlowSpeed", Float) = 1
		_NoiseGeneratorPower("NoiseGenerator Power", Float) = 1
		[Header(Distort Texture)]_DistortTexture("Distort Texture", 2D) = "white" {}
		_DistortZoomX("Distort Zoom X", Float) = 0
		_DistortZoomY("Distort Zoom Y", Float) = 0
		_DistortFlowSpeed("Distort FlowSpeed", Float) = 1
		_DistortStrength("Distort Strength", Range( 0 , 1)) = 0
		[Header(Click Decoration Texture(Click Mode Only))]_ClickDecorationTexture("Click Decoration Texture", 2D) = "white" {}
		_ClickDecorationST("Click Decoration ST", Vector) = (1.64,1.44,0,0)
		_ClickDecorationEmiision("Click Decoration Emiision", Float) = 1
		[Header(Click Decoration Mask(Click Mode Only))]_ClickDecorationMask("Click Decoration Mask", 2D) = "white" {}
		_ClickDecorationMaskZoomX("Click Decoration Mask Zoom X", Float) = 0
		_ClickDecorationMaskZoomY("Click Decoration Mask Zoom Y", Float) = 0
		_ClickFlow("ClickFlow", Range( 0 , 1)) = 0
		_ClickFlowMinClip("ClickFlow Min Clip", Float) = 0
		_ClickFlowMaxClip("ClickFlow Max Clip", Float) = 0.6

	}

	SubShader
	{
		LOD 0

		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
		
		Stencil
		{
			Ref [_Stencil]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			CompFront [_StencilComp]
			PassFront [_StencilOp]
			FailFront Keep
			ZFailFront Keep
			CompBack Always
			PassBack Keep
			FailBack Keep
			ZFailBack Keep
		}


		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask [_ColorMask]

		
		Pass
		{
			Name "Default"
		CGPROGRAM
			
			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			#pragma multi_compile __ UNITY_UI_CLIP_RECT
			#pragma multi_compile __ UNITY_UI_ALPHACLIP
			
			#include "UnityShaderVariables.cginc"
			#define ASE_NEEDS_FRAG_COLOR
			#pragma shader_feature_local _ONLYEDGEEMISSION_ON
			#pragma shader_feature_local _MODE_TOUCHMODE _MODE_CLICKMODE

			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				
			};
			
			uniform fixed4 _Color;
			uniform fixed4 _TextureSampleAdd;
			uniform float4 _ClipRect;
			uniform sampler2D _MainTex;
			uniform float4 _MainColor;
			uniform float _ExtraMaskEmi;
			uniform sampler2D _ExtraMask;
			uniform float _ExtraMaskZoomX;
			uniform float _ExtraMaskZoomY;
			uniform float _ExtraMaskOffsetX;
			uniform float _ExtraMaskOffsetY;
			uniform sampler2D _InnerMask;
			uniform float _MaskZoom;
			uniform sampler2D _Mask;
			uniform float _MaskEmi;
			uniform float _NoiseGeneratorFlowSpeed;
			uniform float _NoiseGeneratorZoom;
			uniform float _NoiseGeneratorPower;
			uniform sampler2D _NoiseTexture1;
			uniform sampler2D _DistortTexture;
			uniform float _DistortFlowSpeed;
			uniform float _DistortZoomX;
			uniform float _DistortZoomY;
			uniform float _DistortStrength;
			uniform float _NoiseTexture1FlowSpeedHorizontal;
			uniform float _NoiseTexture1FlowSpeed;
			uniform float _NoiseTexture1ZoomX;
			uniform float _NoiseTexture1ZoomY;
			uniform float _NoiseTexture1Emission;
			uniform float _NoiseTexture2Emission;
			uniform sampler2D _NoiseTexture2;
			uniform float _NoiseTexture2FlowSpeedHorizontal;
			uniform float _NoiseTexture2FlowSpeed;
			uniform float _NoiseTexture2ZoomX;
			uniform float _NoiseTexture2ZoomY;
			uniform sampler2D _ClickDecorationTexture;
			uniform float4 _ClickDecorationST;
			uniform sampler2D _ClickDecorationMask;
			uniform float _ClickDecorationMaskZoomX;
			uniform float _ClickDecorationMaskZoomY;
			uniform float _ClickFlow;
			uniform float _ClickFlowMinClip;
			uniform float _ClickFlowMaxClip;
			uniform float _ClickDecorationEmiision;
			uniform float _AllAlpha;
			inline float noise_randomValue (float2 uv) { return frac(sin(dot(uv, float2(12.9898, 78.233)))*43758.5453); }
			inline float noise_interpolate (float a, float b, float t) { return (1.0-t)*a + (t*b); }
			inline float valueNoise (float2 uv)
			{
				float2 i = floor(uv);
				float2 f = frac( uv );
				f = f* f * (3.0 - 2.0 * f);
				uv = abs( frac(uv) - 0.5);
				float2 c0 = i + float2( 0.0, 0.0 );
				float2 c1 = i + float2( 1.0, 0.0 );
				float2 c2 = i + float2( 0.0, 1.0 );
				float2 c3 = i + float2( 1.0, 1.0 );
				float r0 = noise_randomValue( c0 );
				float r1 = noise_randomValue( c1 );
				float r2 = noise_randomValue( c2 );
				float r3 = noise_randomValue( c3 );
				float bottomOfGrid = noise_interpolate( r0, r1, f.x );
				float topOfGrid = noise_interpolate( r2, r3, f.x );
				float t = noise_interpolate( bottomOfGrid, topOfGrid, f.y );
				return t;
			}
			
			float SimpleNoise(float2 UV)
			{
				float t = 0.0;
				float freq = pow( 2.0, float( 0 ) );
				float amp = pow( 0.5, float( 3 - 0 ) );
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(1));
				amp = pow(0.5, float(3-1));
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(2));
				amp = pow(0.5, float(3-2));
				t += valueNoise( UV/freq )*amp;
				return t;
			}
			

			
			v2f vert( appdata_t IN  )
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID( IN );
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				UNITY_TRANSFER_INSTANCE_ID(IN, OUT);
				OUT.worldPosition = IN.vertex;
				
				
				OUT.worldPosition.xyz +=  float3( 0, 0, 0 ) ;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

				OUT.texcoord = IN.texcoord;
				
				OUT.color = IN.color * _Color;
				return OUT;
			}

			fixed4 frag(v2f IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				float2 texCoord49 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 temp_cast_0 = (0.5).xx;
				float2 appendResult75 = (float2(_ExtraMaskZoomX , _ExtraMaskZoomY));
				float2 appendResult73 = (float2(_ExtraMaskOffsetX , _ExtraMaskOffsetY));
				float4 tex2DNode47 = tex2D( _ExtraMask, ( ( ( texCoord49 - temp_cast_0 ) * appendResult75 ) + texCoord49 + appendResult73 ) );
				float2 texCoord5 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 temp_cast_1 = (0.5).xx;
				float2 temp_output_10_0 = ( ( ( texCoord5 - temp_cast_1 ) * _MaskZoom ) + texCoord5 );
				float4 tex2DNode102 = tex2D( _InnerMask, temp_output_10_0 );
				float2 temp_cast_2 = (0.5).xx;
				float4 tex2DNode1 = tex2D( _Mask, temp_output_10_0 );
				float temp_output_2_0 = ( tex2DNode1.r * _MaskEmi * tex2DNode1.a );
				float AllMask105 = ( ( _ExtraMaskEmi * tex2DNode47.r * tex2DNode47.a * ( 1.0 - ( tex2DNode102.r * tex2DNode102.a ) ) ) + temp_output_2_0 );
				float2 appendResult43 = (float2(0.0 , _NoiseGeneratorFlowSpeed));
				float2 texCoord36 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 temp_cast_3 = (0.5).xx;
				float2 panner42 = ( 1.0 * _Time.y * appendResult43 + ( ( ( texCoord36 - temp_cast_3 ) * _NoiseGeneratorZoom ) + texCoord36 ));
				float simpleNoise35 = SimpleNoise( panner42 );
				float NoiseGenerator45 = saturate( pow( simpleNoise35 , _NoiseGeneratorPower ) );
				float2 appendResult87 = (float2(0.0 , _DistortFlowSpeed));
				float2 texCoord81 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 temp_cast_4 = (0.5).xx;
				float2 appendResult83 = (float2(_DistortZoomX , _DistortZoomY));
				float2 panner89 = ( 1.0 * _Time.y * appendResult87 + ( ( ( texCoord81 - temp_cast_4 ) * appendResult83 ) + texCoord81 ));
				float4 tex2DNode77 = tex2D( _DistortTexture, panner89 );
				float Distort91 = ( (-1.0 + (( tex2DNode77.r * tex2DNode77.a ) - 0.0) * (1.0 - -1.0) / (1.0 - 0.0)) * _DistortStrength );
				float2 appendResult20 = (float2(_NoiseTexture1FlowSpeedHorizontal , _NoiseTexture1FlowSpeed));
				float2 texCoord12 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 temp_cast_5 = (0.5).xx;
				float2 appendResult67 = (float2(_NoiseTexture1ZoomX , _NoiseTexture1ZoomY));
				float2 panner18 = ( 1.0 * _Time.y * appendResult20 + ( ( ( texCoord12 - temp_cast_5 ) * appendResult67 ) + texCoord12 ));
				float2 appendResult28 = (float2(_NoiseTexture2FlowSpeedHorizontal , _NoiseTexture2FlowSpeed));
				float2 texCoord21 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 temp_cast_6 = (0.5).xx;
				float2 appendResult70 = (float2(_NoiseTexture2ZoomX , _NoiseTexture2ZoomY));
				float2 panner27 = ( 1.0 * _Time.y * appendResult28 + ( ( ( texCoord21 - temp_cast_6 ) * appendResult70 ) + texCoord21 ));
				float AllNoise110 = ( ( tex2D( _NoiseTexture1, ( Distort91 + panner18 ) ).r * _NoiseTexture1Emission ) + ( _NoiseTexture2Emission * tex2D( _NoiseTexture2, ( Distort91 + panner27 ) ).r ) );
				float2 texCoord118 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 temp_cast_7 = (0.5).xx;
				float2 appendResult119 = (float2(_ClickDecorationST.x , _ClickDecorationST.y));
				float2 appendResult153 = (float2(_ClickDecorationST.z , _ClickDecorationST.w));
				float4 tex2DNode114 = tex2D( _ClickDecorationTexture, ( ( ( texCoord118 - temp_cast_7 ) * appendResult119 ) + texCoord118 + appendResult153 ) );
				float2 texCoord131 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float2 temp_cast_8 = (0.5).xx;
				float2 appendResult132 = (float2(_ClickDecorationMaskZoomX , _ClickDecorationMaskZoomY));
				float2 appendResult139 = (float2(0.0 , -(_ClickFlowMinClip + (_ClickFlow - 0.0) * (_ClickFlowMaxClip - _ClickFlowMinClip) / (1.0 - 0.0))));
				#if defined(_MODE_TOUCHMODE)
				float staticSwitch149 = 0.0;
				#elif defined(_MODE_CLICKMODE)
				float staticSwitch149 = ( ( tex2DNode114.r * tex2DNode114.a ) * tex2D( _ClickDecorationMask, ( ( ( ( texCoord131 - temp_cast_8 ) * appendResult132 ) + texCoord131 ) + appendResult139 ) ).r * _ClickDecorationEmiision );
				#else
				float staticSwitch149 = 0.0;
				#endif
				float EdgeMaskOnly106 = temp_output_2_0;
				#ifdef _ONLYEDGEEMISSION_ON
				float staticSwitch101 = EdgeMaskOnly106;
				#else
				float staticSwitch101 = ( ( AllMask105 * NoiseGenerator45 * AllNoise110 ) + staticSwitch149 );
				#endif
				float2 appendResult10_g1 = (float2(0.9 , 0.9));
				float2 temp_output_11_0_g1 = ( abs( (IN.texcoord.xy*2.0 + -1.0) ) - appendResult10_g1 );
				float2 break16_g1 = ( 1.0 - ( temp_output_11_0_g1 / fwidth( temp_output_11_0_g1 ) ) );
				float4 break147 = ( _MainColor * staticSwitch101 * IN.color * saturate( min( break16_g1.x , break16_g1.y ) ) );
				float4 appendResult145 = (float4(break147.r , break147.g , break147.b , ( break147.a * _AllAlpha )));
				
				half4 color = appendResult145;
				
				#ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif
				
				#ifdef UNITY_UI_ALPHACLIP
				clip (color.a - 0.001);
				#endif

				return color;
			}
		ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=18900
0;0;1536;812.6;2456.13;-1458.308;1;True;False
Node;AmplifyShaderEditor.CommentaryNode;113;-2196.451,2958.009;Inherit;False;2045.055;502.9573;Comment;17;80;81;82;79;83;84;86;85;88;87;89;77;78;95;90;96;91;Distort;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;82;-2029.889,3178.358;Inherit;False;Constant;_Float6;Float 6;3;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;81;-2096.878,3038.809;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;80;-2146.451,3262.223;Inherit;False;Property;_DistortZoomX;Distort Zoom X;30;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;79;-2136.569,3345.566;Inherit;False;Property;_DistortZoomY;Distort Zoom Y;31;0;Create;True;0;0;0;False;0;False;0;0.51;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;83;-1890.796,3288.75;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;84;-1865.158,3038.56;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;86;-1726.451,3038.223;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;85;-1756.775,3286.065;Inherit;False;Property;_DistortFlowSpeed;Distort FlowSpeed;32;0;Create;True;0;0;0;False;0;False;1;0.02;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;87;-1502.776,3268.065;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;88;-1580.451,3038.223;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;89;-1443.821,3037.968;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.CommentaryNode;112;-2195.59,-408.2347;Inherit;False;2223.808;3217.111;Comment;78;129;125;123;122;119;118;117;114;45;100;99;64;35;42;40;43;44;39;41;38;37;36;94;27;25;98;24;23;28;29;70;21;22;26;69;30;62;63;92;20;19;97;67;110;31;61;60;11;93;18;16;15;14;66;13;17;12;130;131;132;133;134;135;136;137;138;139;140;142;141;151;152;153;154;156;157;159;158;Noise;1,1,1,1;0;0
Node;AmplifyShaderEditor.SamplerNode;77;-1210.585,3008.009;Inherit;True;Property;_DistortTexture;Distort Texture;29;1;[Header];Create;True;1;Distort Texture;0;0;False;0;False;-1;None;6a41d0f6780330041b8f0ee7f45b34a5;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;109;-2204.336,-1554.515;Inherit;False;1546.774;1027.191;Comment;29;5;7;9;6;8;50;49;54;74;10;51;71;72;75;102;73;52;103;53;47;48;3;1;104;55;2;56;106;105;Mask;1,1,1,1;0;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;5;-2154.336,-869.1379;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;69;-2144.853,508.9106;Inherit;False;Property;_NoiseTexture2ZoomY;NoiseTexture 2  Zoom Y;22;0;Create;True;0;0;0;False;0;False;0;0.65;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;26;-2132.692,430.784;Inherit;False;Property;_NoiseTexture2ZoomX;NoiseTexture 2  Zoom X;21;0;Create;True;0;0;0;False;0;False;0;5.23;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;78;-897.972,3059.717;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;21;-2094.201,203.2215;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;17;-2145.59,-110.6913;Inherit;False;Property;_NoiseTexture1ZoomX;NoiseTexture 1  Zoom X;15;0;Create;True;0;0;0;False;0;False;0;3.96;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;22;-2027.211,342.7705;Inherit;False;Constant;_Float2;Float 2;3;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-2087.347,-729.5892;Inherit;False;Constant;_Float0;Float 0;3;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;13;-2029.027,-194.5568;Inherit;False;Constant;_Float1;Float 1;3;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;66;-2135.708,-27.34802;Inherit;False;Property;_NoiseTexture1ZoomY;NoiseTexture 1  Zoom Y;16;0;Create;True;0;0;0;False;0;False;0;0.97;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;12;-2096.016,-334.1053;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;70;-1916.387,449.4356;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TFHCRemapNode;95;-761.7673,3061.893;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-1;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;23;-1862.48,202.9725;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;9;-2084.909,-642.7239;Inherit;False;Property;_MaskZoom;Mask Zoom;6;0;Create;True;0;0;0;False;0;False;0;1.65;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;67;-1889.933,-84.16505;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;14;-1864.296,-334.3542;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;36;-2137.426,2417.879;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;37;-2070.437,2557.428;Inherit;False;Constant;_Float4;Float 4;3;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;6;-1922.615,-869.3868;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;90;-843.5994,3257.863;Inherit;False;Property;_DistortStrength;Distort Strength;33;0;Create;True;0;0;0;False;0;False;0;0.05;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;98;-1781.779,355.7855;Inherit;False;Property;_NoiseTexture2FlowSpeedHorizontal;NoiseTexture 2 FlowSpeed Horizontal;24;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;41;-2067.999,2644.294;Inherit;False;Property;_NoiseGeneratorZoom;NoiseGenerator  Zoom;26;1;[Header];Create;True;1;Noise Generator;0;0;False;0;False;0;4.03;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-1755.913,-86.84914;Inherit;False;Property;_NoiseTexture1FlowSpeed;NoiseTexture 1 FlowSpeed;17;0;Create;True;0;0;0;False;0;False;1;-0.17;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;15;-1725.589,-334.6912;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;97;-1803.16,-177.3091;Inherit;False;Property;_NoiseTexture1FlowSpeedHorizontal;NoiseTexture 1 FlowSpeed Horizontal;18;0;Create;True;0;0;0;False;0;False;0;0.01;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;96;-545.7676,3061.893;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;29;-1754.097,450.478;Inherit;False;Property;_NoiseTexture2FlowSpeed;NoiseTexture 2 FlowSpeed;23;0;Create;True;0;0;0;False;0;False;1;-0.14;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;-1723.773,202.6355;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;50;-2074.37,-1175.613;Inherit;False;Constant;_Float5;Float 5;3;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;38;-1905.706,2417.63;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;49;-2141.36,-1315.162;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;54;-2071.932,-1088.747;Inherit;False;Property;_ExtraMaskZoomX;Extra Mask Zoom X;11;0;Create;True;0;0;0;False;0;False;0;1.9;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;8;-1783.909,-869.7238;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;74;-2058.81,-992.7179;Inherit;False;Property;_ExtraMaskZoomY;Extra Mask Zoom Y;12;0;Create;True;0;0;0;False;0;False;0;5.67;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;20;-1501.913,-104.8491;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;75;-1827.514,-1057.989;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;10;-1637.909,-869.7238;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;72;-2027.04,-1418.295;Inherit;False;Property;_ExtraMaskOffsetY;Extra Mask Offset Y;10;0;Create;True;0;0;0;False;0;False;0;-0.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;71;-2024.166,-1504.515;Inherit;False;Property;_ExtraMaskOffsetX;Extra Mask Offset X;9;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;25;-1577.773,202.6355;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;39;-1766.999,2417.293;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;51;-1909.639,-1315.411;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;44;-1797.323,2665.136;Inherit;False;Property;_NoiseGeneratorFlowSpeed;NoiseGenerator FlowSpeed;27;0;Create;True;0;0;0;False;0;False;1;-1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;28;-1500.097,432.4781;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;91;-376.1969,3057.518;Inherit;False;Distort;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;16;-1579.589,-334.6912;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;158;-1975.13,1892.308;Inherit;False;Property;_ClickFlowMinClip;ClickFlow Min Clip;41;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;43;-1543.323,2647.136;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;117;-1997.702,914.7266;Inherit;False;Constant;_Float3;Float 3;3;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector4Node;152;-2051.219,999.6262;Inherit;False;Property;_ClickDecorationST;Click Decoration ST;35;0;Create;True;0;0;0;False;0;False;1.64,1.44,0,0;1.65,1.44,-0.002,0.21;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.TextureCoordinatesNode;131;-2082.185,1244.034;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;136;-2189.676,1475.597;Inherit;False;Property;_ClickDecorationMaskZoomX;Click Decoration Mask Zoom X;38;0;Create;True;0;0;0;False;0;False;0;1.65;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;118;-2064.692,775.1777;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;138;-1991.087,1765.514;Inherit;False;Property;_ClickFlow;ClickFlow;40;0;Create;True;0;0;0;False;0;False;0;0.241;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;137;-2186.837,1554.723;Inherit;False;Property;_ClickDecorationMaskZoomY;Click Decoration Mask Zoom Y;39;0;Create;True;0;0;0;False;0;False;0;6.31;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;27;-1441.142,202.3804;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;73;-1777,-1472.901;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;52;-1770.932,-1315.748;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;102;-1475.458,-1071.608;Inherit;True;Property;_InnerMask;Inner Mask ;5;0;Create;True;0;0;0;False;0;False;-1;None;b05e4af899e9004458ddc36fd6605a06;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;159;-1977.13,1970.308;Inherit;False;Property;_ClickFlowMaxClip;ClickFlow Max Clip;42;0;Create;True;0;0;0;False;0;False;0.6;0.6;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;130;-2015.195,1383.583;Inherit;False;Constant;_Float7;Float 7;3;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;40;-1620.999,2417.293;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;92;-1430.036,4.018901;Inherit;False;91;Distort;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;18;-1442.958,-334.9463;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;122;-1832.97,774.9286;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TFHCRemapNode;156;-1719.941,1769.864;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;0.6;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;53;-1624.932,-1315.748;Inherit;False;3;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;42;-1484.368,2417.038;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;103;-1183.966,-1014.369;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;133;-1850.463,1243.785;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;93;-1194.935,-358.2347;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;119;-1813.094,1028.419;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;132;-1904.37,1490.249;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;94;-1181.024,182.1647;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;63;-909.2631,-19.06714;Inherit;False;Property;_NoiseTexture2Emission;NoiseTexture 2 Emission;25;0;Create;True;0;0;0;False;0;False;1;4.77;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;62;-913.5004,-95.34158;Inherit;False;Property;_NoiseTexture1Emission;NoiseTexture 1 Emission;19;0;Create;True;0;0;0;False;0;False;1;3.31;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;64;-1270.42,2634.635;Inherit;False;Property;_NoiseGeneratorPower;NoiseGenerator Power;28;0;Create;True;0;0;0;False;0;False;1;1.03;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;35;-1256.619,2412.93;Inherit;True;Simple;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;30;-986.8909,173.0466;Inherit;True;Property;_NoiseTexture2;NoiseTexture 2;20;1;[Header];Create;True;1;Noise 2;0;0;False;0;False;-1;None;58dfeb2bb9db345408903716e2ce1811;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.OneMinusNode;104;-1064.864,-1012.863;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;153;-1682.342,1075.551;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;1;-1477.319,-863.4528;Inherit;True;Property;_Mask;Mask;4;1;[Header];Create;True;1;Mask Texture;0;0;False;0;False;-1;5a6e71652efdf4b4898432605ec5c6aa;ef7f4d695323a14459b4a3ace24ce6fc;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;123;-1694.263,774.5916;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;11;-979.8986,-356.3221;Inherit;True;Property;_NoiseTexture1;NoiseTexture 1;14;1;[Header];Create;True;1;Noise 1;0;0;False;0;False;-1;None;25ad72a0acb9f4c4cb927d8509cdbda2;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;47;-1464.343,-1309.476;Inherit;True;Property;_ExtraMask;Extra Mask;8;1;[Header];Create;True;1;Extra Texture;0;0;False;0;False;-1;5a6e71652efdf4b4898432605ec5c6aa;493a4d87cc78b7242b133946019a785d;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NegateNode;157;-1548.98,1768.782;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;48;-1356.178,-1410.465;Inherit;False;Property;_ExtraMaskEmi;Extra Mask Emi;13;0;Create;True;0;0;0;False;0;False;1;6.08;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;3;-1320.132,-655.9064;Inherit;False;Property;_MaskEmi;Mask Emi;7;0;Create;True;0;0;0;False;0;False;1;2;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;134;-1711.756,1243.448;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;135;-1565.756,1243.448;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PowerNode;99;-971.9828,2415.888;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;139;-1436.1,1356.445;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;55;-1053.382,-1212.39;Inherit;False;4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;61;-662.0778,-19.06751;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;60;-663.4905,-123.5913;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;125;-1548.263,774.5916;Inherit;False;3;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;2;-1055.377,-836.8987;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;114;-1370.797,748.3325;Inherit;True;Property;_ClickDecorationTexture;Click Decoration Texture;34;1;[Header];Create;True;1;Click Decoration Texture(Click Mode Only);0;0;False;0;False;-1;None;6f968f96648019743b57c6f8e96ea0eb;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;56;-897.5471,-1018.567;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;140;-1282.862,1247.094;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;31;-287.2339,-88.43868;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;100;-807.9827,2414.888;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;129;-1111.985,1231.972;Inherit;True;Property;_ClickDecorationMask;Click Decoration Mask;37;1;[Header];Create;True;1;Click Decoration Mask(Click Mode Only);0;0;False;0;False;-1;c4196ff22c48bea4ea62dabb2e873762;c4196ff22c48bea4ea62dabb2e873762;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;110;-170.3196,-90.91775;Inherit;False;AllNoise;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;154;-973.0447,926.5555;Inherit;False;Property;_ClickDecorationEmiision;Click Decoration Emiision;36;0;Create;True;0;0;0;False;0;False;1;0.37;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;142;-918.751,810.4874;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;45;-658.1241,2411.939;Inherit;False;NoiseGenerator;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;105;-882.3623,-1151.312;Inherit;False;AllMask;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;141;-645.7065,816.4938;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;111;90.50188,26.00443;Inherit;False;110;AllNoise;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;151;-118.0676,112.7337;Inherit;False;Constant;_Float8;Float 8;40;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;108;91.53346,-140.8729;Inherit;False;105;AllMask;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;46;50.15402,-65.33557;Inherit;False;45;NoiseGenerator;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;149;113.6823,110.4791;Inherit;False;Property;_Mode;Mode;1;0;Create;True;0;0;0;False;0;False;0;0;1;True;;KeywordEnum;2;TouchMode;ClickMode;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;106;-908.2357,-841.9918;Inherit;False;EdgeMaskOnly;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;34;276.9067,-105.8873;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;107;330.3805,-184.0197;Inherit;False;106;EdgeMaskOnly;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;155;424.2271,-48.57714;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.VertexColorNode;57;399.4907,139.9669;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;101;578.5338,-148.9546;Inherit;False;Property;_OnlyEdgeEmission;Only Edge Emission ?;0;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;59;405.0585,-634.5289;Inherit;True;Rectangle;-1;;1;6b23e0c975270fb4084c354b2c83366a;0;3;1;FLOAT2;0,0;False;2;FLOAT;0.9;False;3;FLOAT;0.9;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;4;402.1904,-394.9747;Inherit;False;Property;_MainColor;Main Color;2;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,1;3.732132,3.029613,1.36113,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;58;963.8489,-166.9709;Inherit;False;4;4;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;143;957.4358,44.48755;Inherit;False;Property;_AllAlpha;AllAlpha;3;0;Create;True;0;0;0;False;0;False;1;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.BreakToComponentsNode;147;1099.408,-167.5554;Inherit;False;COLOR;1;0;COLOR;0,0,0,0;False;16;FLOAT;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4;FLOAT;5;FLOAT;6;FLOAT;7;FLOAT;8;FLOAT;9;FLOAT;10;FLOAT;11;FLOAT;12;FLOAT;13;FLOAT;14;FLOAT;15
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;148;1236.925,-62.03638;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;145;1394.32,-167.1608;Inherit;False;FLOAT4;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;1634.94,-166.5242;Float;False;True;-1;2;ASEMaterialInspector;0;6;UI_EventParticle;5056123faa0c79b47ab6ad7e8bf059a4;True;Default;0;0;Default;2;False;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;True;True;True;True;True;0;True;-9;False;False;False;False;False;False;False;True;True;0;True;-5;255;True;-8;255;True;-7;0;True;-4;0;True;-6;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;2;False;-1;True;0;True;-11;False;True;5;Queue=Transparent=Queue=0;IgnoreProjector=True;RenderType=Transparent=RenderType;PreviewType=Plane;CanUseSpriteAtlas=True;False;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;0;;0;0;Standard;0;0;1;True;False;;False;0
WireConnection;83;0;80;0
WireConnection;83;1;79;0
WireConnection;84;0;81;0
WireConnection;84;1;82;0
WireConnection;86;0;84;0
WireConnection;86;1;83;0
WireConnection;87;1;85;0
WireConnection;88;0;86;0
WireConnection;88;1;81;0
WireConnection;89;0;88;0
WireConnection;89;2;87;0
WireConnection;77;1;89;0
WireConnection;78;0;77;1
WireConnection;78;1;77;4
WireConnection;70;0;26;0
WireConnection;70;1;69;0
WireConnection;95;0;78;0
WireConnection;23;0;21;0
WireConnection;23;1;22;0
WireConnection;67;0;17;0
WireConnection;67;1;66;0
WireConnection;14;0;12;0
WireConnection;14;1;13;0
WireConnection;6;0;5;0
WireConnection;6;1;7;0
WireConnection;15;0;14;0
WireConnection;15;1;67;0
WireConnection;96;0;95;0
WireConnection;96;1;90;0
WireConnection;24;0;23;0
WireConnection;24;1;70;0
WireConnection;38;0;36;0
WireConnection;38;1;37;0
WireConnection;8;0;6;0
WireConnection;8;1;9;0
WireConnection;20;0;97;0
WireConnection;20;1;19;0
WireConnection;75;0;54;0
WireConnection;75;1;74;0
WireConnection;10;0;8;0
WireConnection;10;1;5;0
WireConnection;25;0;24;0
WireConnection;25;1;21;0
WireConnection;39;0;38;0
WireConnection;39;1;41;0
WireConnection;51;0;49;0
WireConnection;51;1;50;0
WireConnection;28;0;98;0
WireConnection;28;1;29;0
WireConnection;91;0;96;0
WireConnection;16;0;15;0
WireConnection;16;1;12;0
WireConnection;43;1;44;0
WireConnection;27;0;25;0
WireConnection;27;2;28;0
WireConnection;73;0;71;0
WireConnection;73;1;72;0
WireConnection;52;0;51;0
WireConnection;52;1;75;0
WireConnection;102;1;10;0
WireConnection;40;0;39;0
WireConnection;40;1;36;0
WireConnection;18;0;16;0
WireConnection;18;2;20;0
WireConnection;122;0;118;0
WireConnection;122;1;117;0
WireConnection;156;0;138;0
WireConnection;156;3;158;0
WireConnection;156;4;159;0
WireConnection;53;0;52;0
WireConnection;53;1;49;0
WireConnection;53;2;73;0
WireConnection;42;0;40;0
WireConnection;42;2;43;0
WireConnection;103;0;102;1
WireConnection;103;1;102;4
WireConnection;133;0;131;0
WireConnection;133;1;130;0
WireConnection;93;0;92;0
WireConnection;93;1;18;0
WireConnection;119;0;152;1
WireConnection;119;1;152;2
WireConnection;132;0;136;0
WireConnection;132;1;137;0
WireConnection;94;0;92;0
WireConnection;94;1;27;0
WireConnection;35;0;42;0
WireConnection;30;1;94;0
WireConnection;104;0;103;0
WireConnection;153;0;152;3
WireConnection;153;1;152;4
WireConnection;1;1;10;0
WireConnection;123;0;122;0
WireConnection;123;1;119;0
WireConnection;11;1;93;0
WireConnection;47;1;53;0
WireConnection;157;0;156;0
WireConnection;134;0;133;0
WireConnection;134;1;132;0
WireConnection;135;0;134;0
WireConnection;135;1;131;0
WireConnection;99;0;35;0
WireConnection;99;1;64;0
WireConnection;139;1;157;0
WireConnection;55;0;48;0
WireConnection;55;1;47;1
WireConnection;55;2;47;4
WireConnection;55;3;104;0
WireConnection;61;0;63;0
WireConnection;61;1;30;1
WireConnection;60;0;11;1
WireConnection;60;1;62;0
WireConnection;125;0;123;0
WireConnection;125;1;118;0
WireConnection;125;2;153;0
WireConnection;2;0;1;1
WireConnection;2;1;3;0
WireConnection;2;2;1;4
WireConnection;114;1;125;0
WireConnection;56;0;55;0
WireConnection;56;1;2;0
WireConnection;140;0;135;0
WireConnection;140;1;139;0
WireConnection;31;0;60;0
WireConnection;31;1;61;0
WireConnection;100;0;99;0
WireConnection;129;1;140;0
WireConnection;110;0;31;0
WireConnection;142;0;114;1
WireConnection;142;1;114;4
WireConnection;45;0;100;0
WireConnection;105;0;56;0
WireConnection;141;0;142;0
WireConnection;141;1;129;1
WireConnection;141;2;154;0
WireConnection;149;1;151;0
WireConnection;149;0;141;0
WireConnection;106;0;2;0
WireConnection;34;0;108;0
WireConnection;34;1;46;0
WireConnection;34;2;111;0
WireConnection;155;0;34;0
WireConnection;155;1;149;0
WireConnection;101;1;155;0
WireConnection;101;0;107;0
WireConnection;58;0;4;0
WireConnection;58;1;101;0
WireConnection;58;2;57;0
WireConnection;58;3;59;0
WireConnection;147;0;58;0
WireConnection;148;0;147;3
WireConnection;148;1;143;0
WireConnection;145;0;147;0
WireConnection;145;1;147;1
WireConnection;145;2;147;2
WireConnection;145;3;148;0
WireConnection;0;0;145;0
ASEEND*/
//CHKSM=58FF90951DFA5EFEF52209C5857BC2D5E955D79C