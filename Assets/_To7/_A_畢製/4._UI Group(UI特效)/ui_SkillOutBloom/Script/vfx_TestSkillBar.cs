using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class vfx_TestSkillBar : MonoBehaviour
{
    // Start is called before the first frame update
    public Material vfx_Alpha;
    public Material vfx_AllControl;
    public bool effectOpen = false;
    public float cd;
    public GameObject TouchBar;
    public GameObject ClickBar;
    void Start()
    {
        TouchBar.SetActive(true);
        ClickBar.SetActive(false);
        effectOpen = false;
        //vfx_AllControl.SetFloat("_AllControl" , 0);
        //vfx_Alpha.SetFloat("_Alpha" , 1);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && effectOpen == false)
        {
            ClickBar.SetActive(true);
            vfx_AllControl.SetFloat("_ClickFlow" , 0);
            vfx_AllControl.SetFloat("_AllAlpha" , 1);
            vfx_Alpha.SetFloat("_AllAlpha" , 1);
            effectOpen = true;
            TouchBar.SetActive(false);
            StartCoroutine("CoolTime");
        }
        if (effectOpen == true)
        {
            vfx_Alpha.DOFloat(0 , "_AllAlpha" , cd);
            vfx_AllControl.DOFloat(0 , "_AllAlpha" , cd);
            vfx_AllControl.DOFloat(1 , "_ClickFlow" , cd);
        }
    }

    IEnumerator CoolTime ()
    {
        yield return new WaitForSecondsRealtime(cd);
        effectOpen = false;
        TouchBar.SetActive(true);
        ClickBar.SetActive(false);
    }

}
