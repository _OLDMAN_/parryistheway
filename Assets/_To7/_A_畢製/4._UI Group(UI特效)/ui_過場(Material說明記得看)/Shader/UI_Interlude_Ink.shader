// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "UI_Interlude_Ink"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
		_InkControl("_InkControl", Range( 0 , 1)) = 0
		_Fade("橡皮擦->0 / 毛筆->1(_Fade)", Range( 0 , 1)) = 0
		_InkTexture("InkTexture", 2D) = "white" {}
		[HDR]_Color0("Color 0", Color) = (1,1,1,1)
		_NoiseTexture("Noise Texture", 2D) = "white" {}
		_Noise1TextureScale("Noise 1 Texture Scale", Vector) = (1,1,0,0)
		_Noise1TextureRotate("Noise 1 Texture Rotate", Float) = 0
		_Noise2Texture("Noise 2 Texture", 2D) = "white" {}
		_Noise2TextureRotate("Noise 2 Texture Rotate", Float) = 0
		_Noise2TextureScale("Noise 2 Texture Scale", Float) = 1
		_NoiseAddRemap("NoiseAddRemap", Float) = 1
		_MaxClip("MaxClip", Float) = 1.57
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

	}

	SubShader
	{
		LOD 0

		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
		
		Stencil
		{
			Ref [_Stencil]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			CompFront [_StencilComp]
			PassFront [_StencilOp]
			FailFront Keep
			ZFailFront Keep
			CompBack Always
			PassBack Keep
			FailBack Keep
			ZFailBack Keep
		}


		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask [_ColorMask]

		
		Pass
		{
			Name "Default"
		CGPROGRAM
			
			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			#pragma multi_compile __ UNITY_UI_CLIP_RECT
			#pragma multi_compile __ UNITY_UI_ALPHACLIP
			
			#include "UnityShaderVariables.cginc"
			#define ASE_NEEDS_FRAG_COLOR

			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				
			};
			
			uniform fixed4 _Color;
			uniform fixed4 _TextureSampleAdd;
			uniform float4 _ClipRect;
			uniform sampler2D _MainTex;
			uniform float _InkControl;
			uniform float _MaxClip;
			uniform sampler2D _InkTexture;
			uniform float4 _InkTexture_ST;
			uniform float _Fade;
			uniform sampler2D _Noise2Texture;
			uniform float _Noise2TextureScale;
			uniform float _Noise2TextureRotate;
			uniform sampler2D _NoiseTexture;
			uniform float2 _Noise1TextureScale;
			uniform float _Noise1TextureRotate;
			uniform float _NoiseAddRemap;
			uniform float4 _Color0;
					float2 voronoihash21( float2 p )
					{
						
						p = float2( dot( p, float2( 127.1, 311.7 ) ), dot( p, float2( 269.5, 183.3 ) ) );
						return frac( sin( p ) *43758.5453);
					}
			
					float voronoi21( float2 v, float time, inout float2 id, inout float2 mr, float smoothness )
					{
						float2 n = floor( v );
						float2 f = frac( v );
						float F1 = 8.0;
						float F2 = 8.0; float2 mg = 0;
						for ( int j = -1; j <= 1; j++ )
						{
							for ( int i = -1; i <= 1; i++ )
						 	{
						 		float2 g = float2( i, j );
						 		float2 o = voronoihash21( n + g );
								o = ( sin( time + o * 6.2831 ) * 0.5 + 0.5 ); float2 r = f - g - o;
								float d = 0.5 * dot( r, r );
						 //		if( d<F1 ) {
						 //			F2 = F1;
						 			float h = smoothstep(0.0, 1.0, 0.5 + 0.5 * (F1 - d) / smoothness); F1 = lerp(F1, d, h) - smoothness * h * (1.0 - h);mg = g; mr = r; id = o;
						 //		} else if( d<F2 ) {
						 //			F2 = d;
						 //		}
						 	}
						}
						return F1;
					}
			

			
			v2f vert( appdata_t IN  )
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID( IN );
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				UNITY_TRANSFER_INSTANCE_ID(IN, OUT);
				OUT.worldPosition = IN.vertex;
				
				
				OUT.worldPosition.xyz +=  float3( 0, 0, 0 ) ;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

				OUT.texcoord = IN.texcoord;
				
				OUT.color = IN.color * _Color;
				return OUT;
			}

			fixed4 frag(v2f IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				float2 uv_InkTexture = IN.texcoord.xy * _InkTexture_ST.xy + _InkTexture_ST.zw;
				float4 tex2DNode2 = tex2D( _InkTexture, uv_InkTexture );
				float temp_output_50_0 = ( 1.0 - tex2DNode2.r );
				float lerpResult51 = lerp( tex2DNode2.r , temp_output_50_0 , _Fade);
				float2 texCoord12 = IN.texcoord.xy * float2( 1,1 ) + float2( 0,0 );
				float cos41 = cos( _Noise2TextureRotate );
				float sin41 = sin( _Noise2TextureRotate );
				float2 rotator41 = mul( (texCoord12*_Noise2TextureScale + 0.0) - float2( 0,0 ) , float2x2( cos41 , -sin41 , sin41 , cos41 )) + float2( 0,0 );
				float4 tex2DNode37 = tex2D( _Noise2Texture, rotator41 );
				float cos11 = cos( _Noise1TextureRotate );
				float sin11 = sin( _Noise1TextureRotate );
				float2 rotator11 = mul( (texCoord12*_Noise1TextureScale + 0.0) - float2( 0,0 ) , float2x2( cos11 , -sin11 , sin11 , cos11 )) + float2( 0,0 );
				float mulTime24 = _Time.y * 0.9;
				float time21 = mulTime24;
				float voronoiSmooth0 = 0.43;
				float2 coords21 = texCoord12 * 15.34;
				float2 id21 = 0;
				float2 uv21 = 0;
				float voroi21 = voronoi21( coords21, time21, id21, uv21, voronoiSmooth0 );
				float temp_output_43_0 = (( 1.0 - _NoiseAddRemap ) + (( tex2DNode37.r * tex2D( _NoiseTexture, rotator11 ).r * ( ( tex2DNode37.r * voroi21 ) + 1.37 ) ) - 0.0) * (_NoiseAddRemap - ( 1.0 - _NoiseAddRemap )) / (1.0 - 0.0));
				float temp_output_6_0 = step( (0.0 + (_InkControl - 0.0) * (_MaxClip - 0.0) / (1.0 - 0.0)) , ( lerpResult51 + temp_output_43_0 ) );
				float4 appendResult32 = (float4((( IN.color * temp_output_6_0 * _Color0 )).rgb , temp_output_6_0));
				
				half4 color = appendResult32;
				
				#ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif
				
				#ifdef UNITY_UI_ALPHACLIP
				clip (color.a - 0.001);
				#endif

				return color;
			}
		ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=18900
0;73.6;808.6;719;1269.686;216.3553;1.593836;True;False
Node;AmplifyShaderEditor.RangedFloatNode;40;-2287.149,934.1334;Inherit;False;Property;_Noise2TextureScale;Noise 2 Texture Scale;9;0;Create;True;0;0;0;False;0;False;1;0.12;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;12;-2262.616,602.725;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;42;-2351.139,1197.179;Inherit;False;Property;_Noise2TextureRotate;Noise 2 Texture Rotate;8;0;Create;True;0;0;0;False;0;False;0;-0.7;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;38;-2004.328,812.5721;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT;1;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RotatorNode;41;-1849.586,923.7514;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleTimeNode;24;-1927.753,1076.23;Inherit;False;1;0;FLOAT;0.9;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;23;-1811.817,1209.312;Inherit;False;Constant;_Float1;Float 1;4;0;Create;True;0;0;0;False;0;False;0.43;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;16;-2277.233,749.893;Inherit;False;Property;_Noise1TextureScale;Noise 1 Texture Scale;5;0;Create;True;0;0;0;False;0;False;1,1;5,5;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SamplerNode;37;-1667.727,788.3706;Inherit;True;Property;_Noise2Texture;Noise 2 Texture;7;0;Create;True;0;0;0;False;0;False;-1;61cc7efca356b98409b2b661181ebeeb;61cc7efca356b98409b2b661181ebeeb;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;13;-2289.862,1055.277;Inherit;False;Property;_Noise1TextureRotate;Noise 1 Texture Rotate;6;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ScaleAndOffsetNode;14;-1992.451,600.8434;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;1,0;False;2;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.VoronoiNode;21;-1637.419,1025.064;Inherit;True;0;0;1;0;1;False;1;False;True;4;0;FLOAT2;0,0;False;1;FLOAT;22.4;False;2;FLOAT;15.34;False;3;FLOAT;0;False;3;FLOAT;0;FLOAT2;1;FLOAT2;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;26;-1264.661,971.1648;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;25;-1304.71,1117.113;Inherit;False;Constant;_Float2;Float 2;4;0;Create;True;0;0;0;False;0;False;1.37;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RotatorNode;11;-1739.992,599.4473;Inherit;False;3;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;2;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;28;-1067.648,1026.328;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;2;-1701.22,-225.4288;Inherit;True;Property;_InkTexture;InkTexture;2;0;Create;True;0;0;0;False;0;False;-1;aab499390f479564f845d3d617ba12b9;aab499390f479564f845d3d617ba12b9;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;3;-1454.162,570.9771;Inherit;True;Property;_NoiseTexture;Noise Texture;4;0;Create;True;0;0;0;False;0;False;-1;36ada77200269134f905e49618155867;369abc6bb7fad2d46945cf12513b3f94;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;44;-1373.45,362.8768;Inherit;False;Property;_NoiseAddRemap;NoiseAddRemap;10;0;Create;True;0;0;0;False;0;False;1;0.37;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;50;-1380.602,7.537727;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.OneMinusNode;45;-1146.45,328.8768;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;52;-1518.342,236.3488;Inherit;False;Property;_Fade;橡皮擦->0 / 毛筆->1(_Fade);1;0;Create;False;0;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;20;-1049.097,798.4284;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;51;-1170.331,-197.6572;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;7;-572.9291,599.9412;Inherit;False;Property;_InkControl;_InkControl;0;0;Create;True;0;0;0;False;0;False;0;0;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;46;-545.5117,379.8597;Inherit;False;Property;_MaxClip;MaxClip;11;0;Create;True;0;0;0;False;0;False;1.57;1.635;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;43;-962.1382,302.7999;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;-1;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;10;-279.3231,227.0652;Inherit;False;5;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;0;False;4;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;17;-323.0392,-63.43343;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;4;-60.79227,293.358;Inherit;False;Property;_Color0;Color 0;3;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,1;0,0,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.VertexColorNode;1;-8.753937,-160.1896;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StepOpNode;6;-53.04477,44.9301;Inherit;True;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;29;239.5114,72.26135;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SwizzleNode;35;423.6409,157.0298;Inherit;False;FLOAT3;0;1;2;3;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.DynamicAppendNode;32;630.0098,17.80165;Inherit;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;5;-652.793,72.34248;Inherit;True;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;47;-1704.127,15.66333;Inherit;True;Property;_InkTextureMinus;InkTexture Minus;12;0;Create;True;0;0;0;False;0;False;-1;cdf54ed09698a0b4085089da743b36d9;cdf54ed09698a0b4085089da743b36d9;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.StaticSwitch;49;-1129.305,-22.74063;Inherit;False;Property;_Keyword0;Keyword 0;13;0;Create;True;0;0;0;False;0;False;0;0;0;True;;KeywordEnum;2;Positive;Negative;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;834.0211,17.54776;Float;False;True;-1;2;ASEMaterialInspector;0;6;UI_Interlude_Ink;5056123faa0c79b47ab6ad7e8bf059a4;True;Default;0;0;Default;2;False;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;True;True;True;True;True;0;True;-9;False;False;False;False;False;False;False;True;True;0;True;-5;255;True;-8;255;True;-7;0;True;-4;0;True;-6;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;2;False;-1;True;0;True;-11;False;True;5;Queue=Transparent=Queue=0;IgnoreProjector=True;RenderType=Transparent=RenderType;PreviewType=Plane;CanUseSpriteAtlas=True;False;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;0;;0;0;Standard;0;0;1;True;False;;False;0
WireConnection;38;0;12;0
WireConnection;38;1;40;0
WireConnection;41;0;38;0
WireConnection;41;2;42;0
WireConnection;37;1;41;0
WireConnection;14;0;12;0
WireConnection;14;1;16;0
WireConnection;21;0;12;0
WireConnection;21;1;24;0
WireConnection;21;3;23;0
WireConnection;26;0;37;1
WireConnection;26;1;21;0
WireConnection;11;0;14;0
WireConnection;11;2;13;0
WireConnection;28;0;26;0
WireConnection;28;1;25;0
WireConnection;3;1;11;0
WireConnection;50;0;2;1
WireConnection;45;0;44;0
WireConnection;20;0;37;1
WireConnection;20;1;3;1
WireConnection;20;2;28;0
WireConnection;51;0;2;1
WireConnection;51;1;50;0
WireConnection;51;2;52;0
WireConnection;43;0;20;0
WireConnection;43;3;45;0
WireConnection;43;4;44;0
WireConnection;10;0;7;0
WireConnection;10;4;46;0
WireConnection;17;0;51;0
WireConnection;17;1;43;0
WireConnection;6;0;10;0
WireConnection;6;1;17;0
WireConnection;29;0;1;0
WireConnection;29;1;6;0
WireConnection;29;2;4;0
WireConnection;35;0;29;0
WireConnection;32;0;35;0
WireConnection;32;3;6;0
WireConnection;5;0;51;0
WireConnection;5;1;43;0
WireConnection;49;0;50;0
WireConnection;0;0;32;0
ASEEND*/
//CHKSM=DC8E5149ED07EB1ABC6FC842ADCFE582B3B5EAD3