using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionToShader : MonoBehaviour
{
    private MeshRenderer Trans;

    private void Start()
    {
        Trans = GetComponent<MeshRenderer>();
    }

    public void UpdateGrass()
    {
        Vector3 playerPosition = PlayerController.Instance.transform.position;
        Trans.material.SetVector("_PlayerLocation", new Vector4(playerPosition.x, playerPosition.y, playerPosition.z, 0));
    }
}