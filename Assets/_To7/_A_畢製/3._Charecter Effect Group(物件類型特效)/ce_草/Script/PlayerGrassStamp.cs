using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGrassStamp : MonoBehaviour
{
    private void Update()
    {
        FindNearGrass();
    }

    private void FindNearGrass()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 1, LayerMask.GetMask("Grass"));
        foreach (Collider c in colliders)
        {
            PositionToShader p = c.GetComponent<PositionToShader>();
            if (p != null)
            {
                p.UpdateGrass();
            }
        }
    }
}