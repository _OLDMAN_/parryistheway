using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vfx_Tumatuma_EffectAniEvent : MonoBehaviour
{
    private GameObject player;
    public GameObject Block_Particle;
    public GameObject Combo1_Particle;
    public ParticleSystem Combo1_Cohesion_Particle;
    public GameObject Combo2_Particle;
    public GameObject Flat360Attack_Particle;
    public GameObject CastSpell_Particle;
    public GameObject RotateAttack_Particle;
    public GameObject JumpAttack_Particle;

    private void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    void Boss_TumaTuma_Block()
    {
        Instantiate(Block_Particle);
    }
    void Boss_TumaTuma_Combo1()
    {
        Instantiate(Combo1_Particle, transform.position, transform.rotation);
    }
    void Boss_TumaTuma_Combo1_Cohesion()
    {
        Combo1_Cohesion_Particle.Play();
    }
    void Boss_TumaTuma_Combo2()
    {
        Instantiate(Combo2_Particle, transform.position, transform.rotation);
    }
    void Boss_TumaTuma_Flat360Attack()
    {
        Instantiate(Flat360Attack_Particle, transform.position, transform.rotation);
    }
    void Boss_TumaTuma_CastSpell()
    {
        Instantiate(CastSpell_Particle, player.transform.position + Vector3.up * 0.1f, Quaternion.identity); ;
    }
    void Boss_TumaTuma_Rotate360Attack()
    {
        Instantiate(RotateAttack_Particle, transform.position, transform.rotation);
    }
    void Boss_TumaTuma_JumpAttack()
    {
        Vector3 forward = transform.forward;
        forward.y = 0;
        Instantiate(JumpAttack_Particle, transform.position + forward * 2f + Vector3.up * 0.1f, transform.rotation);
    }
}

