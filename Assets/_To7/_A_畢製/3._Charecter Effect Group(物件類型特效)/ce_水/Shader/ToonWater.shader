// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "To7/ToonWater"
{
	Properties
	{
		[HideInInspector] _EmissionColor("Emission Color", Color) = (1,1,1,1)
		[HideInInspector] _AlphaCutoff("Alpha Cutoff ", Range(0, 1)) = 0.5
		[ASEBegin][Header(Depth Setting)]_Depth("Depth", Float) = 0
		_DeepRange("DeepRange", Float) = 1
		_DepthAllCtrl("Depth All Ctrl", Float) = 0
		[HDR][Header(Color)]_DeepColor("DeepColor", Color) = (0,0,0,0)
		[HDR]_ShallowColor("ShallowColor", Color) = (0,0,0,0)
		[HDR]_FresnelColor("Fresnel Color", Color) = (1,1,1,0)
		_FresnelPow("Fresnel Pow", Float) = 1
		[HDR]_AllColor("All Color", Color) = (1,1,1,0)
		_TextureSample0("Texture Sample 0", 2D) = "bump" {}
		_Alpha("Alpha", Float) = 0
		[Header(Normal Setting)]_NormalScale("NormalScale", Float) = 1
		_NormalSpeed("Normal Speed", Float) = 0
		_NormalMap("Normal Map", 2D) = "white" {}
		[Header(Reflection)]_ReflectionDistortion("Reflection Distortion", Float) = 1
		_Bump("Bump", 2D) = "bump" {}
		_SmoothOcclusionFresnelScaleFresnelPow("Smooth/Occlusion/FresnelScale/FresnelPow", Vector) = (1,1,1,5)
		[Header(Refraction)]_UnderWaterDistortion("UnderWater Distortion", Float) = 1
		_RefractionReflectionEffect("Refraction/Reflection Effect", Float) = 0
		_RefractionNormalSize("Refraction Normal Size", Float) = 0
		[Toggle(_REFRACTIONINCLUDETRANSPARENT_ON)] _RefractionIncludeTransparent("Refraction Include Transparent ?", Float) = 0
		[Header(Caustics Setting)]_CausticsSacle("Caustics Sacle", Float) = 8
		_CausticsMap("Caustics Map", 2D) = "white" {}
		_CausticsDepth("Caustics Depth", Float) = 1
		_CausticsAlpha("Caustics Alpha", Range( 0 , 1)) = 0
		[Header(Shore Setting)]_ShoreRange("Shore Range", Float) = 1
		[HDR]_ShoreColor("ShoreColor", Color) = (1,1,1,1)
		_ShoreEdgeWidth("Shore Edge Width", Range( 0 , 1)) = 0
		_ShoreEdgeIntensity("Shore Edge Intensity", Float) = 1
		_AllShoreSubstract("All Shore Substract", Float) = 0
		[Header(Foam Setting)]_FoamRange("Foam Range", Float) = 1
		_FoamSpeed("Foam Speed", Float) = 1
		_FoamFreq("Foam Freq", Float) = 10
		_FoamBlend("Foam Blend", Range( 0 , 1)) = 0
		_FoamNoiseSize("Foam Noise Size", Vector) = (10,10,0,0)
		_FoamDistortGenerate("Foam Distort Generate", Vector) = (5,5,0,0)
		_FoamDissolve("Foam Dissolve", Float) = 1
		_FoamWidth("Foam Width", Float) = 0
		[HDR]_FoamColor("Foam Color", Color) = (1,1,1,1)
		[Toggle(_USEFOAM_ON)] _UseFoam("Use Foam ?", Float) = 0
		[Header(Light Setting)][Enum(Off,0,On,1)]_UseDirectionLight("Use Direction Light", Float) = 0
		_LightDistortStrength("Light Distort Strength", Range( 0 , 1)) = 0
		[HDR]_LightColorBlend("Light Color Blend", Color) = (0,0,0,0)
		_LightEdgeValue("Light Edge Value", Float) = 0
		_LightEdgeSmoothness("Light Edge Smoothness", Float) = 0
		_LightSpecular("Light Specular", Float) = 0
		_Gloss("Gloss", Float) = 0
		_SRPAdditionLightSmoothness("SRP Addition Light Smoothness", Float) = 0
		[HDR]_SRPAdditionSpecularColor("SRP Addition Specular Color", Color) = (0,0,0,0)
		_SparklingTexture("Sparkling Texture", 2D) = "white" {}
		_SparklingStep("Sparkling Step", Float) = 0.5
		_SparklingXYSpeedDistortXYSpeed("Sparkling XY Speed / Distort XY Speed", Vector) = (0,0,0,0)
		_DistortNoiseSize("Distort Noise Size", Float) = 3
		_DistortNoiseStrengthX("Distort Noise Strength X", Float) = 0
		_DistortNoiseStrengthY("Distort Noise Strength Y", Float) = 0
		[Header(Wave Setting)]_WaveASpeedXYStepnesslength("WaveA(SpeedXY,Stepness,length)", Vector) = (1,1,2,50)
		_WaveBSpeedXYStepnesslength("WaveB(SpeedXY,Stepness,length)", Vector) = (1,1,2,50)
		_WaveCSpeedXYStepnesslength("WaveC(SpeedXY,Stepness,length)", Vector) = (1,1,2,50)
		[HDR]_WaveColor("WaveColor", Color) = (0.3396226,0.3396226,0.3396226,1)
		[Toggle(_USEVERTEXOFFSET_ON)] _UseVertexOffset("Use VertexOffset ?", Float) = 0
		[Header(Mask Setting)]_MaskTexture("Mask Texture", 2D) = "white" {}
		[KeywordEnum(R,G,B,A)] _MaskTextureChannel("Mask Texture Channel", Float) = 0
		_MaskPower("Mask Power", Float) = 1
		[ASEEnd]_MaskAdd("Mask Add", Float) = 0
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

		_TessPhongStrength( "Phong Tess Strength", Range( 0, 1 ) ) = 0.5
		_TessValue( "Max Tessellation", Range( 1, 32 ) ) = 16
		//_TessMin( "Tess Min Distance", Float ) = 10
		//_TessMax( "Tess Max Distance", Float ) = 25
		//_TessEdgeLength ( "Tess Edge length", Range( 2, 50 ) ) = 16
		//_TessMaxDisp( "Tess Max Displacement", Float ) = 25
	}

	SubShader
	{
		LOD 0

		
		Tags { "RenderPipeline"="UniversalPipeline" "RenderType"="Transparent" "Queue"="Transparent" }
		
		Cull Back
		AlphaToMask Off
		HLSLINCLUDE
		#pragma target 5.0

		#ifndef ASE_TESS_FUNCS
		#define ASE_TESS_FUNCS
		float4 FixedTess( float tessValue )
		{
			return tessValue;
		}
		
		float CalcDistanceTessFactor (float4 vertex, float minDist, float maxDist, float tess, float4x4 o2w, float3 cameraPos )
		{
			float3 wpos = mul(o2w,vertex).xyz;
			float dist = distance (wpos, cameraPos);
			float f = clamp(1.0 - (dist - minDist) / (maxDist - minDist), 0.01, 1.0) * tess;
			return f;
		}

		float4 CalcTriEdgeTessFactors (float3 triVertexFactors)
		{
			float4 tess;
			tess.x = 0.5 * (triVertexFactors.y + triVertexFactors.z);
			tess.y = 0.5 * (triVertexFactors.x + triVertexFactors.z);
			tess.z = 0.5 * (triVertexFactors.x + triVertexFactors.y);
			tess.w = (triVertexFactors.x + triVertexFactors.y + triVertexFactors.z) / 3.0f;
			return tess;
		}

		float CalcEdgeTessFactor (float3 wpos0, float3 wpos1, float edgeLen, float3 cameraPos, float4 scParams )
		{
			float dist = distance (0.5 * (wpos0+wpos1), cameraPos);
			float len = distance(wpos0, wpos1);
			float f = max(len * scParams.y / (edgeLen * dist), 1.0);
			return f;
		}

		float DistanceFromPlane (float3 pos, float4 plane)
		{
			float d = dot (float4(pos,1.0f), plane);
			return d;
		}

		bool WorldViewFrustumCull (float3 wpos0, float3 wpos1, float3 wpos2, float cullEps, float4 planes[6] )
		{
			float4 planeTest;
			planeTest.x = (( DistanceFromPlane(wpos0, planes[0]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[0]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[0]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.y = (( DistanceFromPlane(wpos0, planes[1]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[1]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[1]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.z = (( DistanceFromPlane(wpos0, planes[2]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[2]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[2]) > -cullEps) ? 1.0f : 0.0f );
			planeTest.w = (( DistanceFromPlane(wpos0, planes[3]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos1, planes[3]) > -cullEps) ? 1.0f : 0.0f ) +
						  (( DistanceFromPlane(wpos2, planes[3]) > -cullEps) ? 1.0f : 0.0f );
			return !all (planeTest);
		}

		float4 DistanceBasedTess( float4 v0, float4 v1, float4 v2, float tess, float minDist, float maxDist, float4x4 o2w, float3 cameraPos )
		{
			float3 f;
			f.x = CalcDistanceTessFactor (v0,minDist,maxDist,tess,o2w,cameraPos);
			f.y = CalcDistanceTessFactor (v1,minDist,maxDist,tess,o2w,cameraPos);
			f.z = CalcDistanceTessFactor (v2,minDist,maxDist,tess,o2w,cameraPos);

			return CalcTriEdgeTessFactors (f);
		}

		float4 EdgeLengthBasedTess( float4 v0, float4 v1, float4 v2, float edgeLength, float4x4 o2w, float3 cameraPos, float4 scParams )
		{
			float3 pos0 = mul(o2w,v0).xyz;
			float3 pos1 = mul(o2w,v1).xyz;
			float3 pos2 = mul(o2w,v2).xyz;
			float4 tess;
			tess.x = CalcEdgeTessFactor (pos1, pos2, edgeLength, cameraPos, scParams);
			tess.y = CalcEdgeTessFactor (pos2, pos0, edgeLength, cameraPos, scParams);
			tess.z = CalcEdgeTessFactor (pos0, pos1, edgeLength, cameraPos, scParams);
			tess.w = (tess.x + tess.y + tess.z) / 3.0f;
			return tess;
		}

		float4 EdgeLengthBasedTessCull( float4 v0, float4 v1, float4 v2, float edgeLength, float maxDisplacement, float4x4 o2w, float3 cameraPos, float4 scParams, float4 planes[6] )
		{
			float3 pos0 = mul(o2w,v0).xyz;
			float3 pos1 = mul(o2w,v1).xyz;
			float3 pos2 = mul(o2w,v2).xyz;
			float4 tess;

			if (WorldViewFrustumCull(pos0, pos1, pos2, maxDisplacement, planes))
			{
				tess = 0.0f;
			}
			else
			{
				tess.x = CalcEdgeTessFactor (pos1, pos2, edgeLength, cameraPos, scParams);
				tess.y = CalcEdgeTessFactor (pos2, pos0, edgeLength, cameraPos, scParams);
				tess.z = CalcEdgeTessFactor (pos0, pos1, edgeLength, cameraPos, scParams);
				tess.w = (tess.x + tess.y + tess.z) / 3.0f;
			}
			return tess;
		}
		#endif //ASE_TESS_FUNCS

		ENDHLSL

		
		Pass
		{
			
			Name "Forward"
			Tags { "LightMode"="UniversalForward" }
			
			Blend SrcAlpha OneMinusSrcAlpha, One OneMinusSrcAlpha
			ZWrite Off
			ZTest LEqual
			Offset 0 , 0
			ColorMask RGBA
			

			HLSLPROGRAM
			#define _RECEIVE_SHADOWS_OFF 1
			#pragma multi_compile_instancing
			#define TESSELLATION_ON 1
			#pragma require tessellation tessHW
			#pragma hull HullFunction
			#pragma domain DomainFunction
			#define ASE_PHONG_TESSELLATION
			#define ASE_FIXED_TESSELLATION
			#define ASE_ABSOLUTE_VERTEX_POS 1
			#define ASE_SRP_VERSION 100302
			#define REQUIRE_OPAQUE_TEXTURE 1
			#define REQUIRE_DEPTH_TEXTURE 1

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/UnityInstancing.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"

			#if ASE_SRP_VERSION <= 70108
			#define REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR
			#endif

			#define ASE_NEEDS_VERT_POSITION
			#define ASE_NEEDS_VERT_NORMAL
			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#pragma shader_feature_local _USEVERTEXOFFSET_ON
			#pragma shader_feature_local _USEFOAM_ON
			#pragma shader_feature_local _REFRACTIONINCLUDETRANSPARENT_ON
			#pragma shader_feature_local _MASKTEXTURECHANNEL_R _MASKTEXTURECHANNEL_G _MASKTEXTURECHANNEL_B _MASKTEXTURECHANNEL_A
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_tangent : TANGENT;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				#ifdef ASE_FOG
				float fogFactor : TEXCOORD2;
				#endif
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_texcoord4 : TEXCOORD4;
				float4 ase_texcoord5 : TEXCOORD5;
				float4 ase_texcoord6 : TEXCOORD6;
				float4 ase_texcoord7 : TEXCOORD7;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _WaveASpeedXYStepnesslength;
			float4 _FoamColor;
			float4 _ShoreColor;
			float4 _FresnelColor;
			float4 _LightColorBlend;
			float4 _SRPAdditionSpecularColor;
			float4 _ShallowColor;
			float4 _WaveColor;
			float4 _SmoothOcclusionFresnelScaleFresnelPow;
			float4 _SparklingXYSpeedDistortXYSpeed;
			float4 _Bump_ST;
			float4 _DeepColor;
			float4 _WaveBSpeedXYStepnesslength;
			float4 _MaskTexture_ST;
			float4 _AllColor;
			float4 _SparklingTexture_ST;
			float4 _WaveCSpeedXYStepnesslength;
			float2 _FoamNoiseSize;
			float2 _FoamDistortGenerate;
			float _ShoreEdgeWidth;
			float _ShoreEdgeIntensity;
			float _LightEdgeValue;
			float _LightEdgeSmoothness;
			float _LightDistortStrength;
			float _Gloss;
			float _MaskPower;
			float _SRPAdditionLightSmoothness;
			float _UseDirectionLight;
			float _SparklingStep;
			float _DistortNoiseStrengthY;
			float _DistortNoiseStrengthX;
			float _LightSpecular;
			float _DistortNoiseSize;
			float _FoamRange;
			float _FoamSpeed;
			float _UnderWaterDistortion;
			float _NormalScale;
			float _NormalSpeed;
			float _RefractionNormalSize;
			float _CausticsSacle;
			float _CausticsDepth;
			float _CausticsAlpha;
			float _ReflectionDistortion;
			float _RefractionReflectionEffect;
			float _Depth;
			float _DepthAllCtrl;
			float _DeepRange;
			float _FresnelPow;
			float _ShoreRange;
			float _AllShoreSubstract;
			float _FoamBlend;
			float _MaskAdd;
			float _FoamWidth;
			float _FoamFreq;
			float _FoamDissolve;
			float _Alpha;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			sampler2D _NormalMap;
			sampler2D _TextureSample0;
			sampler2D _GrabPassTransparent;
			sampler2D _CausticsMap;
			uniform float4 _CameraDepthTexture_TexelSize;
			sampler2D _Bump;
			sampler2D _SparklingTexture;
			sampler2D _MaskTexture;


			float GerstnerWave235( float3 position, inout float3 tangent, inout float3 binormal, float4 wave )
			{
				float steepness = wave.z*0.01;
				float wavelength = wave.w;
				float k = 2 * 3.14159265359 / wavelength;
				float c = sqrt(9.8 / k);
				float2 d = normalize(wave.xy);
				float f = k * (dot(d, position.xz) - c * _Time.y);
				float a = steepness / k;
							tangent += float3(
								-d.x * d.x * (steepness * sin(f)),
								d.x * (steepness * cos(f)),
								-d.x * d.y * (steepness * sin(f))
							);
							binormal += float3(
								-d.x * d.y * (steepness * sin(f)),
								d.y * (steepness * cos(f)),
								-d.y * d.y * (steepness * sin(f))
							);
							return float3(
								d.x * (a * cos(f)),
								a * sin(f),
								d.y * (a * cos(f))
							);
			}
			
			float GerstnerWave243( float3 position, inout float3 tangent, inout float3 binormal, float4 wave )
			{
				float steepness = wave.z*0.01;
				float wavelength = wave.w;
				float k = 2 * 3.14159265359 / wavelength;
				float c = sqrt(9.8 / k);
				float2 d = normalize(wave.xy);
				float f = k * (dot(d, position.xz) - c * _Time.y);
				float a = steepness / k;
							tangent += float3(
								-d.x * d.x * (steepness * sin(f)),
								d.x * (steepness * cos(f)),
								-d.x * d.y * (steepness * sin(f))
							);
							binormal += float3(
								-d.x * d.y * (steepness * sin(f)),
								d.y * (steepness * cos(f)),
								-d.y * d.y * (steepness * sin(f))
							);
							return float3(
								d.x * (a * cos(f)),
								a * sin(f),
								d.y * (a * cos(f))
							);
			}
			
			float GerstnerWave250( float3 position, inout float3 tangent, inout float3 binormal, float4 wave )
			{
				float steepness = wave.z*0.01;
				float wavelength = wave.w;
				float k = 2 * 3.14159265359 / wavelength;
				float c = sqrt(9.8 / k);
				float2 d = normalize(wave.xy);
				float f = k * (dot(d, position.xz) - c * _Time.y);
				float a = steepness / k;
							tangent += float3(
								-d.x * d.x * (steepness * sin(f)),
								d.x * (steepness * cos(f)),
								-d.x * d.y * (steepness * sin(f))
							);
							binormal += float3(
								-d.x * d.y * (steepness * sin(f)),
								d.y * (steepness * cos(f)),
								-d.y * d.y * (steepness * sin(f))
							);
							return float3(
								d.x * (a * cos(f)),
								a * sin(f),
								d.y * (a * cos(f))
							);
			}
			
			inline float4 ASE_ComputeGrabScreenPos( float4 pos )
			{
				#if UNITY_UV_STARTS_AT_TOP
				float scale = -1.0;
				#else
				float scale = 1.0;
				#endif
				float4 o = pos;
				o.y = pos.w * 0.5f;
				o.y = ( pos.y - o.y ) * _ProjectionParams.x * scale + o.y;
				return o;
			}
			
			float2 UnStereo( float2 UV )
			{
				#if UNITY_SINGLE_PASS_STEREO
				float4 scaleOffset = unity_StereoScaleOffset[ unity_StereoEyeIndex ];
				UV.xy = (UV.xy - scaleOffset.zw) / scaleOffset.xy;
				#endif
				return UV;
			}
			
			float3 InvertDepthDirURP75_g5( float3 In )
			{
				float3 result = In;
				#if !defined(ASE_SRP_VERSION) || ASE_SRP_VERSION <= 70301 || ASE_SRP_VERSION == 70503 || ASE_SRP_VERSION >= 80301 
				result *= float3(1,1,-1);
				#endif
				return result;
			}
			
			inline float noise_randomValue (float2 uv) { return frac(sin(dot(uv, float2(12.9898, 78.233)))*43758.5453); }
			inline float noise_interpolate (float a, float b, float t) { return (1.0-t)*a + (t*b); }
			inline float valueNoise (float2 uv)
			{
				float2 i = floor(uv);
				float2 f = frac( uv );
				f = f* f * (3.0 - 2.0 * f);
				uv = abs( frac(uv) - 0.5);
				float2 c0 = i + float2( 0.0, 0.0 );
				float2 c1 = i + float2( 1.0, 0.0 );
				float2 c2 = i + float2( 0.0, 1.0 );
				float2 c3 = i + float2( 1.0, 1.0 );
				float r0 = noise_randomValue( c0 );
				float r1 = noise_randomValue( c1 );
				float r2 = noise_randomValue( c2 );
				float r3 = noise_randomValue( c3 );
				float bottomOfGrid = noise_interpolate( r0, r1, f.x );
				float topOfGrid = noise_interpolate( r2, r3, f.x );
				float t = noise_interpolate( bottomOfGrid, topOfGrid, f.y );
				return t;
			}
			
			float SimpleNoise(float2 UV)
			{
				float t = 0.0;
				float freq = pow( 2.0, float( 0 ) );
				float amp = pow( 0.5, float( 3 - 0 ) );
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(1));
				amp = pow(0.5, float(3-1));
				t += valueNoise( UV/freq )*amp;
				freq = pow(2.0, float(2));
				amp = pow(0.5, float(3-2));
				t += valueNoise( UV/freq )*amp;
				return t;
			}
			
			//https://www.shadertoy.com/view/XdXGW8
			float2 GradientNoiseDir( float2 x )
			{
				const float2 k = float2( 0.3183099, 0.3678794 );
				x = x * k + k.yx;
				return -1.0 + 2.0 * frac( 16.0 * k * frac( x.x * x.y * ( x.x + x.y ) ) );
			}
			
			float GradientNoise( float2 UV, float Scale )
			{
				float2 p = UV * Scale;
				float2 i = floor( p );
				float2 f = frac( p );
				float2 u = f * f * ( 3.0 - 2.0 * f );
				return lerp( lerp( dot( GradientNoiseDir( i + float2( 0.0, 0.0 ) ), f - float2( 0.0, 0.0 ) ),
						dot( GradientNoiseDir( i + float2( 1.0, 0.0 ) ), f - float2( 1.0, 0.0 ) ), u.x ),
						lerp( dot( GradientNoiseDir( i + float2( 0.0, 1.0 ) ), f - float2( 0.0, 1.0 ) ),
						dot( GradientNoiseDir( i + float2( 1.0, 1.0 ) ), f - float2( 1.0, 1.0 ) ), u.x ), u.y );
			}
			
			float3 AdditionalLightsSpecular( float3 WorldPosition, float3 WorldNormal, float3 WorldView, float3 SpecColor, float Smoothness )
			{
				float3 Color = 0;
				#ifdef _ADDITIONAL_LIGHTS
				Smoothness = exp2(10 * Smoothness + 1);
				int numLights = GetAdditionalLightsCount();
				for(int i = 0; i<numLights;i++)
				{
					Light light = GetAdditionalLight(i, WorldPosition);
					half3 AttLightColor = light.color *(light.distanceAttenuation * light.shadowAttenuation);
					Color += LightingSpecular(AttLightColor, light.direction, WorldNormal, WorldView, half4(SpecColor, 0), Smoothness);	
				}
				#endif
				return Color;
			}
			
			
			VertexOutput VertexFunction ( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float3 ase_worldPos = mul(GetObjectToWorldMatrix(), v.vertex).xyz;
				float3 position235 = ase_worldPos;
				float3 tangent235 = float3( 1,0,0 );
				float3 binormal235 = float3( 0,0,1 );
				float4 wave235 = _WaveASpeedXYStepnesslength;
				float localGerstnerWave235 = GerstnerWave235( position235 , tangent235 , binormal235 , wave235 );
				float3 position243 = ase_worldPos;
				float3 tangent243 = tangent235;
				float3 binormal243 = binormal235;
				float4 wave243 = _WaveBSpeedXYStepnesslength;
				float localGerstnerWave243 = GerstnerWave243( position243 , tangent243 , binormal243 , wave243 );
				float3 position250 = ase_worldPos;
				float3 tangent250 = tangent243;
				float3 binormal250 = binormal243;
				float4 wave250 = _WaveCSpeedXYStepnesslength;
				float localGerstnerWave250 = GerstnerWave250( position250 , tangent250 , binormal250 , wave250 );
				float3 temp_output_238_0 = ( ase_worldPos + localGerstnerWave235 + localGerstnerWave243 + localGerstnerWave250 );
				float3 worldToObj239 = mul( GetWorldToObjectMatrix(), float4( temp_output_238_0, 1 ) ).xyz;
				float3 VertexOffsetPos241 = worldToObj239;
				#ifdef _USEVERTEXOFFSET_ON
				float3 staticSwitch262 = VertexOffsetPos241;
				#else
				float3 staticSwitch262 = v.vertex.xyz;
				#endif
				
				float3 normalizeResult245 = normalize( cross( binormal243 , tangent243 ) );
				float3 worldToObjDir246 = mul( GetWorldToObjectMatrix(), float4( normalizeResult245, 0 ) ).xyz;
				float3 WaveVertexNormal247 = worldToObjDir246;
				#ifdef _USEVERTEXOFFSET_ON
				float3 staticSwitch263 = WaveVertexNormal247;
				#else
				float3 staticSwitch263 = v.ase_normal;
				#endif
				
				float4 ase_clipPos = TransformObjectToHClip((v.vertex).xyz);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord3 = screenPos;
				float3 ase_worldTangent = TransformObjectToWorldDir(v.ase_tangent.xyz);
				o.ase_texcoord5.xyz = ase_worldTangent;
				float3 ase_worldNormal = TransformObjectToWorldNormal(v.ase_normal);
				o.ase_texcoord6.xyz = ase_worldNormal;
				float ase_vertexTangentSign = v.ase_tangent.w * unity_WorldTransformParams.w;
				float3 ase_worldBitangent = cross( ase_worldNormal, ase_worldTangent ) * ase_vertexTangentSign;
				o.ase_texcoord7.xyz = ase_worldBitangent;
				
				o.ase_texcoord4.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord4.zw = 0;
				o.ase_texcoord5.w = 0;
				o.ase_texcoord6.w = 0;
				o.ase_texcoord7.w = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = staticSwitch262;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif
				v.ase_normal = staticSwitch263;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );
				float4 positionCS = TransformWorldToHClip( positionWS );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				VertexPositionInputs vertexInput = (VertexPositionInputs)0;
				vertexInput.positionWS = positionWS;
				vertexInput.positionCS = positionCS;
				o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				#ifdef ASE_FOG
				o.fogFactor = ComputeFogFactor( positionCS.z );
				#endif
				o.clipPos = positionCS;
				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				float4 ase_tangent : TANGENT;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord = v.ase_texcoord;
				o.ase_tangent = v.ase_tangent;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				o.ase_tangent = patch[0].ase_tangent * bary.x + patch[1].ase_tangent * bary.y + patch[2].ase_tangent * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag ( VertexOutput IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif
				float4 screenPos = IN.ase_texcoord3;
				float4 ase_grabScreenPos = ASE_ComputeGrabScreenPos( screenPos );
				float4 ase_grabScreenPosNorm = ase_grabScreenPos / ase_grabScreenPos.w;
				float2 temp_output_40_0 = ( (WorldPosition).xz / _NormalScale );
				float mulTime46 = _TimeParameters.x * 0.01;
				float temp_output_44_0 = ( _NormalSpeed * mulTime46 );
				float3 SurfaceNormal49 = BlendNormal( tex2D( _NormalMap, ( temp_output_40_0 + temp_output_44_0 ) ).rgb , UnpackNormalScale( tex2D( _TextureSample0, ( ( temp_output_40_0 * 2.0 ) + ( temp_output_44_0 * 0.5 ) ) ), 1.0f ) );
				float3 temp_cast_1 = (_RefractionNormalSize).xxx;
				float4 temp_output_94_0 = ( ase_grabScreenPosNorm + float4( ( _UnderWaterDistortion * 0.01 * (float3( -1,-1,-1 ) + (SurfaceNormal49 - float3( -1,-1,-1 )) * (temp_cast_1 - float3( -1,-1,-1 )) / (float3( 1,1,1 ) - float3( -1,-1,-1 ))) ) , 0.0 ) );
				float4 fetchOpaqueVal89 = float4( SHADERGRAPH_SAMPLE_SCENE_COLOR( temp_output_94_0.xy ), 1.0 );
				float3 temp_cast_4 = (_RefractionNormalSize).xxx;
				#ifdef _REFRACTIONINCLUDETRANSPARENT_ON
				float4 staticSwitch423 = tex2D( _GrabPassTransparent, temp_output_94_0.xy );
				#else
				float4 staticSwitch423 = fetchOpaqueVal89;
				#endif
				float4 sceneColor148 = staticSwitch423;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float2 UV22_g6 = ase_screenPosNorm.xy;
				float2 localUnStereo22_g6 = UnStereo( UV22_g6 );
				float2 break64_g5 = localUnStereo22_g6;
				float clampDepth69_g5 = SHADERGRAPH_SAMPLE_SCENE_DEPTH( ase_screenPosNorm.xy );
				#ifdef UNITY_REVERSED_Z
				float staticSwitch38_g5 = ( 1.0 - clampDepth69_g5 );
				#else
				float staticSwitch38_g5 = clampDepth69_g5;
				#endif
				float3 appendResult39_g5 = (float3(break64_g5.x , break64_g5.y , staticSwitch38_g5));
				float4 appendResult42_g5 = (float4((appendResult39_g5*2.0 + -1.0) , 1.0));
				float4 temp_output_43_0_g5 = mul( unity_CameraInvProjection, appendResult42_g5 );
				float3 temp_output_46_0_g5 = ( (temp_output_43_0_g5).xyz / (temp_output_43_0_g5).w );
				float3 In75_g5 = temp_output_46_0_g5;
				float3 localInvertDepthDirURP75_g5 = InvertDepthDirURP75_g5( In75_g5 );
				float4 appendResult49_g5 = (float4(localInvertDepthDirURP75_g5 , 1.0));
				float3 PositionFromDepth114 = (mul( unity_CameraToWorld, appendResult49_g5 )).xyz;
				float2 temp_output_116_0 = ( (PositionFromDepth114).xz / _CausticsSacle );
				float2 _CausticsSpeedXY = float2(8,8);
				float mulTime122 = _TimeParameters.x * 0.005;
				float mulTime146 = _TimeParameters.x * 0.0025;
				float screenDepth135 = LinearEyeDepth(SHADERGRAPH_SAMPLE_SCENE_DEPTH( ase_screenPosNorm.xy ),_ZBufferParams);
				float distanceDepth135 = abs( ( screenDepth135 - LinearEyeDepth( ase_screenPosNorm.z,_ZBufferParams ) ) / ( _CausticsDepth ) );
				float temp_output_136_0 = saturate( distanceDepth135 );
				float4 Caustics126 = ( min( tex2D( _CausticsMap, ( temp_output_116_0 + ( _CausticsSpeedXY * mulTime122 ) ) ) , tex2D( _CausticsMap, ( -temp_output_116_0 + ( _CausticsSpeedXY * mulTime146 ) ) ) ) * saturate( ( pow( ( 1.0 - temp_output_136_0 ) , 1.0 ) / 1.0 ) ) );
				float4 UnderWaterColor96 = ( sceneColor148 + ( Caustics126 * _CausticsAlpha ) );
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - WorldPosition );
				ase_worldViewDir = normalize(ase_worldViewDir);
				float2 uv_Bump = IN.ase_texcoord4.xy * _Bump_ST.xy + _Bump_ST.zw;
				float3 tex2DNode82 = UnpackNormalScale( tex2D( _Bump, uv_Bump ), 1.0f );
				float3 temp_output_84_0 = ( tex2DNode82 + ( SurfaceNormal49 * ( _ReflectionDistortion * 1.0 ) ) );
				float3 ase_worldTangent = IN.ase_texcoord5.xyz;
				float3 ase_worldNormal = IN.ase_texcoord6.xyz;
				float3 ase_worldBitangent = IN.ase_texcoord7.xyz;
				float3 tanToWorld0 = float3( ase_worldTangent.x, ase_worldBitangent.x, ase_worldNormal.x );
				float3 tanToWorld1 = float3( ase_worldTangent.y, ase_worldBitangent.y, ase_worldNormal.y );
				float3 tanToWorld2 = float3( ase_worldTangent.z, ase_worldBitangent.z, ase_worldNormal.z );
				float3 tanNormal73 = temp_output_84_0;
				half3 reflectVector73 = reflect( -ase_worldViewDir, float3(dot(tanToWorld0,tanNormal73), dot(tanToWorld1,tanNormal73), dot(tanToWorld2,tanNormal73)) );
				float3 indirectSpecular73 = GlossyEnvironmentReflection( reflectVector73, 1.0 - _SmoothOcclusionFresnelScaleFresnelPow.x, _SmoothOcclusionFresnelScaleFresnelPow.y );
				float fresnelNdotV78 = dot( ase_worldNormal, ase_worldViewDir );
				float fresnelNode78 = ( 0.0 + _SmoothOcclusionFresnelScaleFresnelPow.z * pow( 1.0 - fresnelNdotV78, _SmoothOcclusionFresnelScaleFresnelPow.w ) );
				float3 lerpResult77 = lerp( float3( 0,0,0 ) , indirectSpecular73 , fresnelNode78);
				float3 ReflectionColor68 = lerpResult77;
				float3 position235 = WorldPosition;
				float3 tangent235 = float3( 1,0,0 );
				float3 binormal235 = float3( 0,0,1 );
				float4 wave235 = _WaveASpeedXYStepnesslength;
				float localGerstnerWave235 = GerstnerWave235( position235 , tangent235 , binormal235 , wave235 );
				float3 position243 = WorldPosition;
				float3 tangent243 = tangent235;
				float3 binormal243 = binormal235;
				float4 wave243 = _WaveBSpeedXYStepnesslength;
				float localGerstnerWave243 = GerstnerWave243( position243 , tangent243 , binormal243 , wave243 );
				float3 position250 = WorldPosition;
				float3 tangent250 = tangent243;
				float3 binormal250 = binormal243;
				float4 wave250 = _WaveCSpeedXYStepnesslength;
				float localGerstnerWave250 = GerstnerWave250( position250 , tangent250 , binormal250 , wave250 );
				float3 temp_output_238_0 = ( WorldPosition + localGerstnerWave235 + localGerstnerWave243 + localGerstnerWave250 );
				float clampResult255 = clamp( (( temp_output_238_0 - WorldPosition )).y , 0.0 , 1.0 );
				float4 WaveColor258 = ( _WaveColor * clampResult255 );
				#ifdef _USEVERTEXOFFSET_ON
				float4 staticSwitch266 = ( float4( ReflectionColor68 , 0.0 ) + WaveColor258 );
				#else
				float4 staticSwitch266 = float4( ReflectionColor68 , 0.0 );
				#endif
				float screenDepth107 = LinearEyeDepth(SHADERGRAPH_SAMPLE_SCENE_DEPTH( ase_screenPosNorm.xy ),_ZBufferParams);
				float distanceDepth107 = abs( ( screenDepth107 - LinearEyeDepth( ase_screenPosNorm.z,_ZBufferParams ) ) / ( _RefractionReflectionEffect ) );
				float4 lerpResult99 = lerp( UnderWaterColor96 , staticSwitch266 , saturate( distanceDepth107 ));
				float screenDepth16 = LinearEyeDepth(SHADERGRAPH_SAMPLE_SCENE_DEPTH( ase_screenPosNorm.xy ),_ZBufferParams);
				float distanceDepth16 = abs( ( screenDepth16 - LinearEyeDepth( ase_screenPosNorm.z,_ZBufferParams ) ) / ( _Depth ) );
				float temp_output_12_0 = saturate( distanceDepth16 );
				float WaterDepth15 = pow( temp_output_12_0 , _DepthAllCtrl );
				float4 lerpResult21 = lerp( _DeepColor , _ShallowColor , saturate( exp( ( -WaterDepth15 / _DeepRange ) ) ));
				float fresnelNdotV32 = dot( ase_worldNormal, ase_worldViewDir );
				float fresnelNode32 = ( 0.0 + 1.0 * pow( max( 1.0 - fresnelNdotV32 , 0.0001 ), _FresnelPow ) );
				float4 lerpResult31 = lerp( lerpResult21 , _FresnelColor , fresnelNode32);
				float4 WaterColor29 = lerpResult31;
				float waterDeepNew156 = ( WorldPosition.y - (PositionFromDepth114).y );
				float waterShore163 = saturate( exp( ( -waterDeepNew156 / _ShoreRange ) ) );
				float3 ShoreColor168 = (( waterShore163 * _ShoreColor )).rgb;
				float4 lerpResult171 = lerp( ( lerpResult99 + WaterColor29 ) , float4( ShoreColor168 , 0.0 ) , ( waterShore163 + _AllShoreSubstract ));
				float temp_output_186_0 = saturate( ( waterDeepNew156 / _FoamRange ) );
				float smoothstepResult195 = smoothstep( _FoamBlend , 1.0 , ( temp_output_186_0 + 0.1 ));
				float temp_output_187_0 = ( 1.0 - temp_output_186_0 );
				float temp_output_212_0 = ( temp_output_187_0 - _FoamWidth );
				float2 texCoord227 = IN.ase_texcoord4.xy * _FoamDistortGenerate + float2( 0,0 );
				float2 panner229 = ( 1.0 * _Time.y * float2( 0.3,-0.5 ) + texCoord227);
				float simpleNoise226 = SimpleNoise( panner229*2.66 );
				float2 texCoord204 = IN.ase_texcoord4.xy * float2( 1,1 ) + float2( 0,0 );
				float gradientNoise202 = GradientNoise(( simpleNoise226 + ( texCoord204 * _FoamNoiseSize ) ),1.0);
				gradientNoise202 = gradientNoise202*0.5 + 0.5;
				float4 FoamColor217 = ( ( 1.0 - smoothstepResult195 ) * step( temp_output_212_0 , ( ( sin( ( ( temp_output_187_0 * _FoamFreq ) + ( _FoamSpeed * _TimeParameters.x ) + ( temp_output_187_0 * ( _FoamFreq * 0.7 ) ) ) ) + gradientNoise202 ) - _FoamDissolve ) ) * _FoamColor );
				float4 lerpResult232 = lerp( lerpResult171 , float4( (FoamColor217).rgb , 0.0 ) , (FoamColor217).a);
				#ifdef _USEFOAM_ON
				float4 staticSwitch267 = lerpResult232;
				#else
				float4 staticSwitch267 = lerpResult171;
				#endif
				float smoothstepResult174 = smoothstep( ( 1.0 - _ShoreEdgeWidth ) , 1.0 , waterShore163);
				float ShoreEdge179 = ( smoothstepResult174 * _ShoreEdgeIntensity );
				float4 temp_output_272_0 = ( _AllColor * max( ( staticSwitch267 + ShoreEdge179 ) , float4( 0,0,0,0 ) ) );
				float4 AllNodewithoutLight419 = temp_output_272_0;
				float3 Bump382 = tex2DNode82;
				float3 lerpResult383 = lerp( Bump382 , SurfaceNormal49 , _LightDistortStrength);
				float3 tanNormal349 = lerpResult383;
				float3 worldNormal349 = float3(dot(tanToWorld0,tanNormal349), dot(tanToWorld1,tanNormal349), dot(tanToWorld2,tanNormal349));
				float3 normalizeResult376 = normalize( ( ase_worldViewDir + _MainLightPosition.xyz ) );
				float dotResult350 = dot( worldNormal349 , normalizeResult376 );
				float smoothstepResult354 = smoothstep( _LightEdgeValue , ( _LightEdgeValue + _LightEdgeSmoothness ) , saturate( dotResult350 ));
				float temp_output_386_0 = ( pow( smoothstepResult354 , ( 128.0 * _LightSpecular ) ) * _Gloss );
				float3 WorldPosition13_g4 = WorldPosition;
				float3 tanNormal12_g4 = lerpResult383;
				float3 worldNormal12_g4 = float3(dot(tanToWorld0,tanNormal12_g4), dot(tanToWorld1,tanNormal12_g4), dot(tanToWorld2,tanNormal12_g4));
				float3 WorldNormal13_g4 = worldNormal12_g4;
				float3 WorldView13_g4 = ase_worldViewDir;
				float3 SpecColor13_g4 = _SRPAdditionSpecularColor.rgb;
				float Smoothness13_g4 = _SRPAdditionLightSmoothness;
				float3 localAdditionalLightsSpecular13_g4 = AdditionalLightsSpecular( WorldPosition13_g4 , WorldNormal13_g4 , WorldView13_g4 , SpecColor13_g4 , Smoothness13_g4 );
				float4 temp_output_369_0 = ( ( _MainLightColor * temp_output_386_0 * _LightColorBlend * _MainLightColor.a ) + float4( localAdditionalLightsSpecular13_g4 , 0.0 ) );
				float2 appendResult414 = (float2(_SparklingXYSpeedDistortXYSpeed.x , _SparklingXYSpeedDistortXYSpeed.y));
				float2 uv_SparklingTexture = IN.ase_texcoord4.xy * _SparklingTexture_ST.xy + _SparklingTexture_ST.zw;
				float2 panner397 = ( 1.0 * _Time.y * appendResult414 + uv_SparklingTexture);
				float2 appendResult415 = (float2(_SparklingXYSpeedDistortXYSpeed.z , _SparklingXYSpeedDistortXYSpeed.w));
				float2 texCoord407 = IN.ase_texcoord4.xy * float2( 1,1 ) + float2( 0,0 );
				float2 panner410 = ( 1.0 * _Time.y * appendResult415 + texCoord407);
				float simpleNoise405 = SimpleNoise( panner410*_DistortNoiseSize );
				float2 appendResult417 = (float2(_DistortNoiseStrengthX , _DistortNoiseStrengthY));
				float temp_output_399_0 = step( _SparklingStep , tex2D( _SparklingTexture, ( panner397 + ( saturate( simpleNoise405 ) * appendResult417 ) ) ).r );
				float4 lerpResult418 = lerp( AllNodewithoutLight419 , temp_output_369_0 , saturate( temp_output_399_0 ));
				float4 Light362 = lerpResult418;
				float LightSpecularLerpValue389 = temp_output_386_0;
				float4 lerpResult388 = lerp( temp_output_272_0 , Light362 , LightSpecularLerpValue389);
				float4 lerpResult366 = lerp( temp_output_272_0 , lerpResult388 , _UseDirectionLight);
				
				float WaterAlpha36 = (lerpResult31).a;
				float2 uv_MaskTexture = IN.ase_texcoord4.xy * _MaskTexture_ST.xy + _MaskTexture_ST.zw;
				float4 tex2DNode428 = tex2D( _MaskTexture, uv_MaskTexture );
				#if defined(_MASKTEXTURECHANNEL_R)
				float staticSwitch433 = tex2DNode428.r;
				#elif defined(_MASKTEXTURECHANNEL_G)
				float staticSwitch433 = tex2DNode428.g;
				#elif defined(_MASKTEXTURECHANNEL_B)
				float staticSwitch433 = tex2DNode428.b;
				#elif defined(_MASKTEXTURECHANNEL_A)
				float staticSwitch433 = tex2DNode428.a;
				#else
				float staticSwitch433 = tex2DNode428.r;
				#endif
				float MaskOutput441 = saturate( ( pow( staticSwitch433 , _MaskPower ) + _MaskAdd ) );
				
				float3 BakedAlbedo = 0;
				float3 BakedEmission = 0;
				float3 Color = lerpResult366.rgb;
				float Alpha = ( ( WaterAlpha36 * MaskOutput441 ) + _Alpha );
				float AlphaClipThreshold = 0.5;
				float AlphaClipThresholdShadow = 0.5;

				#ifdef _ALPHATEST_ON
					clip( Alpha - AlphaClipThreshold );
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif

				#ifdef ASE_FOG
					Color = MixFog( Color, IN.fogFactor );
				#endif

				return half4( Color, Alpha );
			}

			ENDHLSL
		}

		
		Pass
		{
			
			Name "DepthOnly"
			Tags { "LightMode"="DepthOnly" }

			ZWrite On
			ColorMask 0
			AlphaToMask Off

			HLSLPROGRAM
			#define _RECEIVE_SHADOWS_OFF 1
			#pragma multi_compile_instancing
			#define TESSELLATION_ON 1
			#pragma require tessellation tessHW
			#pragma hull HullFunction
			#pragma domain DomainFunction
			#define ASE_PHONG_TESSELLATION
			#define ASE_FIXED_TESSELLATION
			#define ASE_ABSOLUTE_VERTEX_POS 1
			#define ASE_SRP_VERSION 100302
			#define REQUIRE_DEPTH_TEXTURE 1

			#pragma prefer_hlslcc gles
			#pragma exclude_renderers d3d11_9x

			#pragma vertex vert
			#pragma fragment frag

			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
			#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/ShaderGraphFunctions.hlsl"
			#include "Packages/com.unity.render-pipelines.core/ShaderLibrary/Color.hlsl"

			#define ASE_NEEDS_VERT_POSITION
			#define ASE_NEEDS_VERT_NORMAL
			#define ASE_NEEDS_FRAG_WORLD_POSITION
			#pragma shader_feature_local _USEVERTEXOFFSET_ON
			#pragma shader_feature_local _MASKTEXTURECHANNEL_R _MASKTEXTURECHANNEL_G _MASKTEXTURECHANNEL_B _MASKTEXTURECHANNEL_A
			#pragma multi_compile _ _ADDITIONAL_LIGHTS_VERTEX _ADDITIONAL_LIGHTS
			#pragma multi_compile _ _ADDITIONAL_LIGHT_SHADOWS


			struct VertexInput
			{
				float4 vertex : POSITION;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct VertexOutput
			{
				float4 clipPos : SV_POSITION;
				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 worldPos : TEXCOORD0;
				#endif
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
				float4 shadowCoord : TEXCOORD1;
				#endif
				float4 ase_texcoord2 : TEXCOORD2;
				float4 ase_texcoord3 : TEXCOORD3;
				float4 ase_texcoord4 : TEXCOORD4;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
			};

			CBUFFER_START(UnityPerMaterial)
			float4 _WaveASpeedXYStepnesslength;
			float4 _FoamColor;
			float4 _ShoreColor;
			float4 _FresnelColor;
			float4 _LightColorBlend;
			float4 _SRPAdditionSpecularColor;
			float4 _ShallowColor;
			float4 _WaveColor;
			float4 _SmoothOcclusionFresnelScaleFresnelPow;
			float4 _SparklingXYSpeedDistortXYSpeed;
			float4 _Bump_ST;
			float4 _DeepColor;
			float4 _WaveBSpeedXYStepnesslength;
			float4 _MaskTexture_ST;
			float4 _AllColor;
			float4 _SparklingTexture_ST;
			float4 _WaveCSpeedXYStepnesslength;
			float2 _FoamNoiseSize;
			float2 _FoamDistortGenerate;
			float _ShoreEdgeWidth;
			float _ShoreEdgeIntensity;
			float _LightEdgeValue;
			float _LightEdgeSmoothness;
			float _LightDistortStrength;
			float _Gloss;
			float _MaskPower;
			float _SRPAdditionLightSmoothness;
			float _UseDirectionLight;
			float _SparklingStep;
			float _DistortNoiseStrengthY;
			float _DistortNoiseStrengthX;
			float _LightSpecular;
			float _DistortNoiseSize;
			float _FoamRange;
			float _FoamSpeed;
			float _UnderWaterDistortion;
			float _NormalScale;
			float _NormalSpeed;
			float _RefractionNormalSize;
			float _CausticsSacle;
			float _CausticsDepth;
			float _CausticsAlpha;
			float _ReflectionDistortion;
			float _RefractionReflectionEffect;
			float _Depth;
			float _DepthAllCtrl;
			float _DeepRange;
			float _FresnelPow;
			float _ShoreRange;
			float _AllShoreSubstract;
			float _FoamBlend;
			float _MaskAdd;
			float _FoamWidth;
			float _FoamFreq;
			float _FoamDissolve;
			float _Alpha;
			#ifdef TESSELLATION_ON
				float _TessPhongStrength;
				float _TessValue;
				float _TessMin;
				float _TessMax;
				float _TessEdgeLength;
				float _TessMaxDisp;
			#endif
			CBUFFER_END
			uniform float4 _CameraDepthTexture_TexelSize;
			sampler2D _MaskTexture;


			float GerstnerWave235( float3 position, inout float3 tangent, inout float3 binormal, float4 wave )
			{
				float steepness = wave.z*0.01;
				float wavelength = wave.w;
				float k = 2 * 3.14159265359 / wavelength;
				float c = sqrt(9.8 / k);
				float2 d = normalize(wave.xy);
				float f = k * (dot(d, position.xz) - c * _Time.y);
				float a = steepness / k;
							tangent += float3(
								-d.x * d.x * (steepness * sin(f)),
								d.x * (steepness * cos(f)),
								-d.x * d.y * (steepness * sin(f))
							);
							binormal += float3(
								-d.x * d.y * (steepness * sin(f)),
								d.y * (steepness * cos(f)),
								-d.y * d.y * (steepness * sin(f))
							);
							return float3(
								d.x * (a * cos(f)),
								a * sin(f),
								d.y * (a * cos(f))
							);
			}
			
			float GerstnerWave243( float3 position, inout float3 tangent, inout float3 binormal, float4 wave )
			{
				float steepness = wave.z*0.01;
				float wavelength = wave.w;
				float k = 2 * 3.14159265359 / wavelength;
				float c = sqrt(9.8 / k);
				float2 d = normalize(wave.xy);
				float f = k * (dot(d, position.xz) - c * _Time.y);
				float a = steepness / k;
							tangent += float3(
								-d.x * d.x * (steepness * sin(f)),
								d.x * (steepness * cos(f)),
								-d.x * d.y * (steepness * sin(f))
							);
							binormal += float3(
								-d.x * d.y * (steepness * sin(f)),
								d.y * (steepness * cos(f)),
								-d.y * d.y * (steepness * sin(f))
							);
							return float3(
								d.x * (a * cos(f)),
								a * sin(f),
								d.y * (a * cos(f))
							);
			}
			
			float GerstnerWave250( float3 position, inout float3 tangent, inout float3 binormal, float4 wave )
			{
				float steepness = wave.z*0.01;
				float wavelength = wave.w;
				float k = 2 * 3.14159265359 / wavelength;
				float c = sqrt(9.8 / k);
				float2 d = normalize(wave.xy);
				float f = k * (dot(d, position.xz) - c * _Time.y);
				float a = steepness / k;
							tangent += float3(
								-d.x * d.x * (steepness * sin(f)),
								d.x * (steepness * cos(f)),
								-d.x * d.y * (steepness * sin(f))
							);
							binormal += float3(
								-d.x * d.y * (steepness * sin(f)),
								d.y * (steepness * cos(f)),
								-d.y * d.y * (steepness * sin(f))
							);
							return float3(
								d.x * (a * cos(f)),
								a * sin(f),
								d.y * (a * cos(f))
							);
			}
			

			VertexOutput VertexFunction( VertexInput v  )
			{
				VertexOutput o = (VertexOutput)0;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(o);

				float3 ase_worldPos = mul(GetObjectToWorldMatrix(), v.vertex).xyz;
				float3 position235 = ase_worldPos;
				float3 tangent235 = float3( 1,0,0 );
				float3 binormal235 = float3( 0,0,1 );
				float4 wave235 = _WaveASpeedXYStepnesslength;
				float localGerstnerWave235 = GerstnerWave235( position235 , tangent235 , binormal235 , wave235 );
				float3 position243 = ase_worldPos;
				float3 tangent243 = tangent235;
				float3 binormal243 = binormal235;
				float4 wave243 = _WaveBSpeedXYStepnesslength;
				float localGerstnerWave243 = GerstnerWave243( position243 , tangent243 , binormal243 , wave243 );
				float3 position250 = ase_worldPos;
				float3 tangent250 = tangent243;
				float3 binormal250 = binormal243;
				float4 wave250 = _WaveCSpeedXYStepnesslength;
				float localGerstnerWave250 = GerstnerWave250( position250 , tangent250 , binormal250 , wave250 );
				float3 temp_output_238_0 = ( ase_worldPos + localGerstnerWave235 + localGerstnerWave243 + localGerstnerWave250 );
				float3 worldToObj239 = mul( GetWorldToObjectMatrix(), float4( temp_output_238_0, 1 ) ).xyz;
				float3 VertexOffsetPos241 = worldToObj239;
				#ifdef _USEVERTEXOFFSET_ON
				float3 staticSwitch262 = VertexOffsetPos241;
				#else
				float3 staticSwitch262 = v.vertex.xyz;
				#endif
				
				float3 normalizeResult245 = normalize( cross( binormal243 , tangent243 ) );
				float3 worldToObjDir246 = mul( GetWorldToObjectMatrix(), float4( normalizeResult245, 0 ) ).xyz;
				float3 WaveVertexNormal247 = worldToObjDir246;
				#ifdef _USEVERTEXOFFSET_ON
				float3 staticSwitch263 = WaveVertexNormal247;
				#else
				float3 staticSwitch263 = v.ase_normal;
				#endif
				
				float4 ase_clipPos = TransformObjectToHClip((v.vertex).xyz);
				float4 screenPos = ComputeScreenPos(ase_clipPos);
				o.ase_texcoord2 = screenPos;
				float3 ase_worldNormal = TransformObjectToWorldNormal(v.ase_normal);
				o.ase_texcoord3.xyz = ase_worldNormal;
				
				o.ase_texcoord4.xy = v.ase_texcoord.xy;
				
				//setting value to unused interpolator channels and avoid initialization warnings
				o.ase_texcoord3.w = 0;
				o.ase_texcoord4.zw = 0;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					float3 defaultVertexValue = v.vertex.xyz;
				#else
					float3 defaultVertexValue = float3(0, 0, 0);
				#endif
				float3 vertexValue = staticSwitch262;
				#ifdef ASE_ABSOLUTE_VERTEX_POS
					v.vertex.xyz = vertexValue;
				#else
					v.vertex.xyz += vertexValue;
				#endif

				v.ase_normal = staticSwitch263;

				float3 positionWS = TransformObjectToWorld( v.vertex.xyz );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				o.worldPos = positionWS;
				#endif

				o.clipPos = TransformWorldToHClip( positionWS );
				#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR) && defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					VertexPositionInputs vertexInput = (VertexPositionInputs)0;
					vertexInput.positionWS = positionWS;
					vertexInput.positionCS = clipPos;
					o.shadowCoord = GetShadowCoord( vertexInput );
				#endif
				return o;
			}

			#if defined(TESSELLATION_ON)
			struct VertexControl
			{
				float4 vertex : INTERNALTESSPOS;
				float3 ase_normal : NORMAL;
				float4 ase_texcoord : TEXCOORD0;

				UNITY_VERTEX_INPUT_INSTANCE_ID
			};

			struct TessellationFactors
			{
				float edge[3] : SV_TessFactor;
				float inside : SV_InsideTessFactor;
			};

			VertexControl vert ( VertexInput v )
			{
				VertexControl o;
				UNITY_SETUP_INSTANCE_ID(v);
				UNITY_TRANSFER_INSTANCE_ID(v, o);
				o.vertex = v.vertex;
				o.ase_normal = v.ase_normal;
				o.ase_texcoord = v.ase_texcoord;
				return o;
			}

			TessellationFactors TessellationFunction (InputPatch<VertexControl,3> v)
			{
				TessellationFactors o;
				float4 tf = 1;
				float tessValue = _TessValue; float tessMin = _TessMin; float tessMax = _TessMax;
				float edgeLength = _TessEdgeLength; float tessMaxDisp = _TessMaxDisp;
				#if defined(ASE_FIXED_TESSELLATION)
				tf = FixedTess( tessValue );
				#elif defined(ASE_DISTANCE_TESSELLATION)
				tf = DistanceBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, tessValue, tessMin, tessMax, GetObjectToWorldMatrix(), _WorldSpaceCameraPos );
				#elif defined(ASE_LENGTH_TESSELLATION)
				tf = EdgeLengthBasedTess(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams );
				#elif defined(ASE_LENGTH_CULL_TESSELLATION)
				tf = EdgeLengthBasedTessCull(v[0].vertex, v[1].vertex, v[2].vertex, edgeLength, tessMaxDisp, GetObjectToWorldMatrix(), _WorldSpaceCameraPos, _ScreenParams, unity_CameraWorldClipPlanes );
				#endif
				o.edge[0] = tf.x; o.edge[1] = tf.y; o.edge[2] = tf.z; o.inside = tf.w;
				return o;
			}

			[domain("tri")]
			[partitioning("fractional_odd")]
			[outputtopology("triangle_cw")]
			[patchconstantfunc("TessellationFunction")]
			[outputcontrolpoints(3)]
			VertexControl HullFunction(InputPatch<VertexControl, 3> patch, uint id : SV_OutputControlPointID)
			{
			   return patch[id];
			}

			[domain("tri")]
			VertexOutput DomainFunction(TessellationFactors factors, OutputPatch<VertexControl, 3> patch, float3 bary : SV_DomainLocation)
			{
				VertexInput o = (VertexInput) 0;
				o.vertex = patch[0].vertex * bary.x + patch[1].vertex * bary.y + patch[2].vertex * bary.z;
				o.ase_normal = patch[0].ase_normal * bary.x + patch[1].ase_normal * bary.y + patch[2].ase_normal * bary.z;
				o.ase_texcoord = patch[0].ase_texcoord * bary.x + patch[1].ase_texcoord * bary.y + patch[2].ase_texcoord * bary.z;
				#if defined(ASE_PHONG_TESSELLATION)
				float3 pp[3];
				for (int i = 0; i < 3; ++i)
					pp[i] = o.vertex.xyz - patch[i].ase_normal * (dot(o.vertex.xyz, patch[i].ase_normal) - dot(patch[i].vertex.xyz, patch[i].ase_normal));
				float phongStrength = _TessPhongStrength;
				o.vertex.xyz = phongStrength * (pp[0]*bary.x + pp[1]*bary.y + pp[2]*bary.z) + (1.0f-phongStrength) * o.vertex.xyz;
				#endif
				UNITY_TRANSFER_INSTANCE_ID(patch[0], o);
				return VertexFunction(o);
			}
			#else
			VertexOutput vert ( VertexInput v )
			{
				return VertexFunction( v );
			}
			#endif

			half4 frag(VertexOutput IN  ) : SV_TARGET
			{
				UNITY_SETUP_INSTANCE_ID(IN);
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				#if defined(ASE_NEEDS_FRAG_WORLD_POSITION)
				float3 WorldPosition = IN.worldPos;
				#endif
				float4 ShadowCoords = float4( 0, 0, 0, 0 );

				#if defined(ASE_NEEDS_FRAG_SHADOWCOORDS)
					#if defined(REQUIRES_VERTEX_SHADOW_COORD_INTERPOLATOR)
						ShadowCoords = IN.shadowCoord;
					#elif defined(MAIN_LIGHT_CALCULATE_SHADOWS)
						ShadowCoords = TransformWorldToShadowCoord( WorldPosition );
					#endif
				#endif

				float4 screenPos = IN.ase_texcoord2;
				float4 ase_screenPosNorm = screenPos / screenPos.w;
				ase_screenPosNorm.z = ( UNITY_NEAR_CLIP_VALUE >= 0 ) ? ase_screenPosNorm.z : ase_screenPosNorm.z * 0.5 + 0.5;
				float screenDepth16 = LinearEyeDepth(SHADERGRAPH_SAMPLE_SCENE_DEPTH( ase_screenPosNorm.xy ),_ZBufferParams);
				float distanceDepth16 = abs( ( screenDepth16 - LinearEyeDepth( ase_screenPosNorm.z,_ZBufferParams ) ) / ( _Depth ) );
				float temp_output_12_0 = saturate( distanceDepth16 );
				float WaterDepth15 = pow( temp_output_12_0 , _DepthAllCtrl );
				float4 lerpResult21 = lerp( _DeepColor , _ShallowColor , saturate( exp( ( -WaterDepth15 / _DeepRange ) ) ));
				float3 ase_worldViewDir = ( _WorldSpaceCameraPos.xyz - WorldPosition );
				ase_worldViewDir = normalize(ase_worldViewDir);
				float3 ase_worldNormal = IN.ase_texcoord3.xyz;
				float fresnelNdotV32 = dot( ase_worldNormal, ase_worldViewDir );
				float fresnelNode32 = ( 0.0 + 1.0 * pow( max( 1.0 - fresnelNdotV32 , 0.0001 ), _FresnelPow ) );
				float4 lerpResult31 = lerp( lerpResult21 , _FresnelColor , fresnelNode32);
				float WaterAlpha36 = (lerpResult31).a;
				float2 uv_MaskTexture = IN.ase_texcoord4.xy * _MaskTexture_ST.xy + _MaskTexture_ST.zw;
				float4 tex2DNode428 = tex2D( _MaskTexture, uv_MaskTexture );
				#if defined(_MASKTEXTURECHANNEL_R)
				float staticSwitch433 = tex2DNode428.r;
				#elif defined(_MASKTEXTURECHANNEL_G)
				float staticSwitch433 = tex2DNode428.g;
				#elif defined(_MASKTEXTURECHANNEL_B)
				float staticSwitch433 = tex2DNode428.b;
				#elif defined(_MASKTEXTURECHANNEL_A)
				float staticSwitch433 = tex2DNode428.a;
				#else
				float staticSwitch433 = tex2DNode428.r;
				#endif
				float MaskOutput441 = saturate( ( pow( staticSwitch433 , _MaskPower ) + _MaskAdd ) );
				
				float Alpha = ( ( WaterAlpha36 * MaskOutput441 ) + _Alpha );
				float AlphaClipThreshold = 0.5;

				#ifdef _ALPHATEST_ON
					clip(Alpha - AlphaClipThreshold);
				#endif

				#ifdef LOD_FADE_CROSSFADE
					LODDitheringTransition( IN.clipPos.xyz, unity_LODFade.x );
				#endif
				return 0;
			}
			ENDHLSL
		}

	
	}
	CustomEditor "UnityEditor.ShaderGraph.PBRMasterGUI"
	Fallback "Hidden/InternalErrorShader"
	
}
/*ASEBEGIN
Version=18900
0;0;1536;812.6;-24.47137;-3937.616;1;True;False
Node;AmplifyShaderEditor.CommentaryNode;18;-1078.938,-1113.655;Inherit;False;817.181;231.4932;Comment;9;0;16;17;12;15;4;3;2;270;Water Depth;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;17;-1028.938,-1039.683;Inherit;False;Property;_Depth;Depth;0;1;[Header];Create;True;1;Depth Setting;0;0;False;0;False;0;0.66;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DepthFade;16;-871.9758,-1058.021;Inherit;False;True;False;True;2;1;FLOAT3;0,0,0;False;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;269;-670.504,-928.804;Inherit;False;Property;_DepthAllCtrl;Depth All Ctrl;2;0;Create;True;0;0;0;False;0;False;0;2.68;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;12;-628.6278,-1057.687;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;270;-489.504,-1058.804;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;15;-354.559,-1063.655;Inherit;False;WaterDepth;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;28;-1577.636,-834.387;Inherit;False;1309.459;658.3312;Comment;13;33;31;32;20;22;25;23;19;24;26;27;21;34;Water Color;0.4242613,0.7461712,0.990566,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;22;-1527.636,-412.9339;Inherit;False;15;WaterDepth;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;440;340.0301,4351.038;Inherit;False;1318.334;358.8804;Comment;9;433;434;435;438;437;432;439;428;441;Mask Texture;0.2735849,0.04779364,0.001032375,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;25;-1346.13,-324.0366;Inherit;False;Property;_DeepRange;DeepRange;1;0;Create;True;0;0;0;False;0;False;1;3.55;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;23;-1317.13,-409.0366;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CommentaryNode;240;279.4273,705.8204;Inherit;False;2674.751;1028.776;Comment;21;239;241;238;247;246;245;244;250;251;243;249;235;236;237;252;253;255;257;256;258;254;Vertex Animation;1,0.2490566,0.2490566,1;0;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;432;390.0301,4424.542;Inherit;False;0;428;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleDivideOpNode;24;-1168.129,-409.0366;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ExpOpNode;26;-1029.129,-409.0366;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;428;605.7988,4401.038;Inherit;True;Property;_MaskTexture;Mask Texture;60;1;[Header];Create;True;1;Mask Setting;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector4Node;237;386.3778,1334.523;Inherit;False;Property;_WaveASpeedXYStepnesslength;WaveA(SpeedXY,Stepness,length);54;1;[Header];Create;True;1;Wave Setting;0;0;False;0;False;1,1,2,50;1,-1.08,0.36,1.8;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.WorldPosInputsNode;236;359.7274,1097.208;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.CustomExpressionNode;235;795.6263,1095.92;Inherit;False;float steepness = wave.z*0.01@$float wavelength = wave.w@$float k = 2 * 3.14159265359 / wavelength@$float c = sqrt(9.8 / k)@$float2 d = normalize(wave.xy)@$float f = k * (dot(d, position.xz) - c * _Time.y)@$float a = steepness / k@$			tangent += float3($				-d.x * d.x * (steepness * sin(f)),$				d.x * (steepness * cos(f)),$				-d.x * d.y * (steepness * sin(f))$			)@$			binormal += float3($				-d.x * d.y * (steepness * sin(f)),$				d.y * (steepness * cos(f)),$				-d.y * d.y * (steepness * sin(f))$			)@$			return float3($				d.x * (a * cos(f)),$				a * sin(f),$				d.y * (a * cos(f))$			)@;1;False;4;True;position;FLOAT3;0,0,0;In;;Inherit;False;True;tangent;FLOAT3;1,0,0;InOut;;Inherit;False;True;binormal;FLOAT3;0,0,1;InOut;;Inherit;False;True;wave;FLOAT4;0,0,0,0;In;;Inherit;False;GerstnerWave;True;False;0;4;0;FLOAT3;0,0,0;False;1;FLOAT3;1,0,0;False;2;FLOAT3;0,0,1;False;3;FLOAT4;0,0,0,0;False;3;FLOAT;0;FLOAT3;2;FLOAT3;3
Node;AmplifyShaderEditor.RangedFloatNode;435;980.1888,4594.519;Inherit;False;Property;_MaskPower;Mask Power;62;0;Create;True;0;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;433;907.9988,4424.333;Inherit;False;Property;_MaskTextureChannel;Mask Texture Channel;61;0;Create;True;0;0;0;False;0;False;0;0;0;True;;KeywordEnum;4;R;G;B;A;Create;True;True;9;1;FLOAT;0;False;0;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;4;FLOAT;0;False;5;FLOAT;0;False;6;FLOAT;0;False;7;FLOAT;0;False;8;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;20;-1104.319,-602.9054;Inherit;False;Property;_ShallowColor;ShallowColor;4;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,0;0.1122107,0.2835311,0.3018868,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector4Node;249;726.6425,1327.251;Inherit;False;Property;_WaveBSpeedXYStepnesslength;WaveB(SpeedXY,Stepness,length);55;0;Create;True;0;0;0;False;0;False;1,1,2,50;-0.5,-0.5,0.09,1.6;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;34;-921.3913,-291.7729;Inherit;False;Property;_FresnelPow;Fresnel Pow;6;0;Create;True;0;0;0;False;0;False;1;10;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;19;-1101.439,-784.387;Inherit;False;Property;_DeepColor;DeepColor;3;2;[HDR];[Header];Create;True;1;Color;0;0;False;0;False;0,0,0,0;0.197864,0.2405314,0.2641509,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;27;-912.1302,-409.0366;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.CustomExpressionNode;243;1075.923,1222.04;Inherit;False;float steepness = wave.z*0.01@$float wavelength = wave.w@$float k = 2 * 3.14159265359 / wavelength@$float c = sqrt(9.8 / k)@$float2 d = normalize(wave.xy)@$float f = k * (dot(d, position.xz) - c * _Time.y)@$float a = steepness / k@$			tangent += float3($				-d.x * d.x * (steepness * sin(f)),$				d.x * (steepness * cos(f)),$				-d.x * d.y * (steepness * sin(f))$			)@$			binormal += float3($				-d.x * d.y * (steepness * sin(f)),$				d.y * (steepness * cos(f)),$				-d.y * d.y * (steepness * sin(f))$			)@$			return float3($				d.x * (a * cos(f)),$				a * sin(f),$				d.y * (a * cos(f))$			)@;1;False;4;True;position;FLOAT3;0,0,0;In;;Inherit;False;True;tangent;FLOAT3;1,0,0;InOut;;Inherit;False;True;binormal;FLOAT3;0,0,1;InOut;;Inherit;False;True;wave;FLOAT4;0,0,0,0;In;;Inherit;False;GerstnerWave;True;False;0;4;0;FLOAT3;0,0,0;False;1;FLOAT3;1,0,0;False;2;FLOAT3;0,0,1;False;3;FLOAT4;0,0,0,0;False;3;FLOAT;0;FLOAT3;2;FLOAT3;3
Node;AmplifyShaderEditor.FresnelNode;32;-748.3271,-382.0599;Inherit;False;Standard;WorldNormal;ViewDir;False;True;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;33;-749.5176,-547.8284;Inherit;False;Property;_FresnelColor;Fresnel Color;5;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,0;0.3686275,0.7294118,2,0.772549;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;438;1173.189,4594.519;Inherit;False;Property;_MaskAdd;Mask Add;63;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector4Node;251;1046.984,1501.581;Inherit;False;Property;_WaveCSpeedXYStepnesslength;WaveC(SpeedXY,Stepness,length);56;0;Create;True;0;0;0;False;0;False;1,1,2,50;1.5,1,7,1.5;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PowerNode;434;1180.189,4428.519;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;21;-828.594,-677.3137;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.CrossProductOpNode;244;1339,1247.651;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CustomExpressionNode;250;1457.393,1436.825;Inherit;False;float steepness = wave.z*0.01@$float wavelength = wave.w@$float k = 2 * 3.14159265359 / wavelength@$float c = sqrt(9.8 / k)@$float2 d = normalize(wave.xy)@$float f = k * (dot(d, position.xz) - c * _Time.y)@$float a = steepness / k@$			tangent += float3($				-d.x * d.x * (steepness * sin(f)),$				d.x * (steepness * cos(f)),$				-d.x * d.y * (steepness * sin(f))$			)@$			binormal += float3($				-d.x * d.y * (steepness * sin(f)),$				d.y * (steepness * cos(f)),$				-d.y * d.y * (steepness * sin(f))$			)@$			return float3($				d.x * (a * cos(f)),$				a * sin(f),$				d.y * (a * cos(f))$			)@;1;False;4;True;position;FLOAT3;0,0,0;In;;Inherit;False;True;tangent;FLOAT3;1,0,0;InOut;;Inherit;False;True;binormal;FLOAT3;0,0,1;InOut;;Inherit;False;True;wave;FLOAT4;0,0,0,0;In;;Inherit;False;GerstnerWave;True;False;0;4;0;FLOAT3;0,0,0;False;1;FLOAT3;1,0,0;False;2;FLOAT3;0,0,1;False;3;FLOAT4;0,0,0,0;False;3;FLOAT;0;FLOAT3;2;FLOAT3;3
Node;AmplifyShaderEditor.LerpOp;31;-421.1332,-676.0275;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;437;1335.189,4429.519;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;238;1670.419,1047.69;Inherit;False;4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SwizzleNode;35;-194.1702,-571.174;Inherit;False;FLOAT;3;1;2;3;1;0;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;439;1493.564,4429.602;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;245;1492.67,1250.993;Inherit;False;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;36;-17.65308,-571.0536;Inherit;False;WaterAlpha;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TransformDirectionNode;246;1929.819,1334.705;Inherit;False;World;Object;False;Fast;False;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.TransformPositionNode;239;2166.034,1141.673;Inherit;False;World;Object;False;Fast;True;1;0;FLOAT3;0,0,0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.CommentaryNode;261;441.7459,-474.406;Inherit;False;2608.653;971.7057;Comment;41;108;260;182;242;234;220;180;259;173;86;233;107;172;232;171;70;181;37;30;109;100;99;262;265;266;267;271;272;273;274;276;277;364;365;366;367;388;390;419;442;443;Combine;0,0,0,1;0;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;441;1443.431,4596.637;Inherit;False;MaskOutput;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;443;2440.302,240.2626;Inherit;False;441;MaskOutput;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;37;2330.039,151.9697;Inherit;False;36;WaterAlpha;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;241;2432.803,1154.903;Inherit;False;VertexOffsetPos;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;247;2265.272,1336.916;Inherit;False;WaveVertexNormal;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;216;659.0946,2439.388;Inherit;False;1686.773;1548.534;Comment;12;215;214;211;199;210;208;209;218;221;222;223;231;Foam;1,0.1075471,0.822345,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;170;-2178.552,5247.782;Inherit;False;1717.064;638.3745;Comment;18;178;177;174;176;175;168;167;157;165;164;166;162;161;160;159;163;158;179;Shore;0.3921166,0.9528302,0.13843,1;0;0
Node;AmplifyShaderEditor.GetLocalVarNode;248;2210.436,596.2166;Inherit;False;247;WaveVertexNormal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;215;709.0946,2791.993;Inherit;False;1266.23;362.9922;Comment;9;212;194;184;185;183;186;187;193;213;Foam Size/Frequency;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;57;-1996.705,-114.7942;Inherit;False;1733.4;558.4332;Comment;18;42;48;38;39;40;41;44;43;46;47;52;51;53;55;54;50;49;56;Surface Normal;0.9970069,0.7113208,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;211;1181.111,2489.388;Inherit;False;792.1373;290.6279;Comment;4;198;196;195;197;Foam Mask;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;97;-1936.796,1312.745;Inherit;False;1665.996;669.2245;Comment;16;106;92;95;93;90;96;91;89;94;148;149;150;278;346;347;423;Refraction;0.9712226,1,0.4471698,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;214;758.8065,3262.043;Inherit;False;1083.501;725.8778;Comment;12;202;206;188;191;190;204;205;207;192;189;226;230;Foam Generate;1,1,1,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;169;-3147.08,4262.986;Inherit;False;800.6616;373.5347;Comment;5;152;153;155;154;156;waterDeepNew;1,1,1,1;0;0
Node;AmplifyShaderEditor.RangedFloatNode;274;2657.503,294.2884;Inherit;False;Property;_Alpha;Alpha;8;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;442;2625.302,158.2626;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;242;2222.985,385.5822;Inherit;False;241;VertexOffsetPos;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.CommentaryNode;125;-2296.364,4061.675;Inherit;False;2320.277;1043.323;Caustics ;28;121;133;147;143;142;141;120;117;140;130;138;144;135;131;113;111;124;129;122;123;145;146;115;118;137;114;116;136;Caustics ;0.6158444,0.3595586,0.9433962,1;0;0
Node;AmplifyShaderEditor.CommentaryNode;361;-2005.567,2077.821;Inherit;False;2441.393;808.2449;Comment;37;360;357;355;356;354;352;386;348;359;387;375;351;376;374;353;379;378;384;358;385;383;377;373;368;372;370;349;369;350;389;393;396;397;398;399;400;420;Light;0.990566,0.6069273,0.4429512,1;0;0
Node;AmplifyShaderEditor.NormalVertexDataNode;264;2247.344,457.5836;Inherit;False;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PosVertexDataNode;265;2242.756,259.1511;Inherit;False;0;0;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.CommentaryNode;69;-1629.74,558.4055;Inherit;False;1363.2;650.2615;Comment;13;63;64;62;65;66;77;73;78;82;84;85;87;382;Reflection;0.06981128,0.5747119,1,1;0;0
Node;AmplifyShaderEditor.DepthFade;135;-1966.004,4716.252;Inherit;False;True;False;True;2;1;FLOAT3;0,0,0;False;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;180;2000.643,76.19606;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;157;-2128.552,5298.782;Inherit;False;156;waterDeepNew;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;258;2677.118,969.5818;Inherit;False;WaveColor;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;163;-1398.553,5297.782;Inherit;False;waterShore;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;178;-1021.231,5439.335;Inherit;False;Property;_ShoreEdgeIntensity;Shore Edge Intensity;27;0;Create;True;0;0;0;False;0;False;1;0.12;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;221;1151.505,3123.421;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.7;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;122;-1662.714,4487.656;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;173;1101.13,156.783;Inherit;False;163;waterShore;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;44;-1558.705,197.0596;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;141;-830.9958,4323.621;Inherit;True;Property;_TextureSample1;Texture Sample 1;21;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Instance;121;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;386;-482.2074,2267.242;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;399;170.9538,2699.261;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;174;-1023.557,5301.657;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;116;-1286.972,4135.457;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SwizzleNode;167;-1420.553,5553.782;Inherit;False;FLOAT3;0;1;2;3;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.Vector4Node;85;-1208.149,1000.223;Inherit;False;Property;_SmoothOcclusionFresnelScaleFresnelPow;Smooth/Occlusion/FresnelScale/FresnelPow;14;0;Create;True;0;0;0;False;0;False;1,1,1,5;1,0.72,1,2.6;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;188;1423.296,3401.104;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SaturateNode;162;-1550.553,5303.782;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;253;1705.907,1203.28;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;117;-1569.972,4236.457;Inherit;False;Property;_CausticsSacle;Caustics Sacle;20;1;[Header];Create;True;1;Caustics Setting;0;0;False;0;False;8;4.73;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;68;-175.4181,730.0298;Inherit;False;ReflectionColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;412;-391.8401,2809.627;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;223;982.4219,3175.974;Inherit;False;Constant;_Float7;Float 7;30;0;Create;True;0;0;0;False;0;False;0.7;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;179;-649.9565,5301.791;Inherit;False;ShoreEdge;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PowerNode;138;-1324.179,4818.148;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;113;-1871.365,4129.355;Inherit;False;FLOAT3;0;1;2;3;1;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;149;-581.6421,1380.453;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;222;1304.743,3117.247;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;271;2138.701,-134.8353;Inherit;False;Property;_AllColor;All Color;7;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,0;0.6140619,0.7561568,0.8867924,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;209;1713.307,3156.588;Inherit;False;Property;_FoamDissolve;Foam Dissolve;35;0;Create;True;0;0;0;False;0;False;1;1.45;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;396;-866.2877,2693.581;Inherit;False;0;393;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;87;-831.4717,560.1574;Inherit;True;Property;_CubeMap;Cube Map;15;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;LockedToCube;False;Object;-1;Auto;Cube;8;0;SAMPLERCUBE;;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.Vector2Node;206;829.8362,3733.092;Inherit;False;Property;_FoamNoiseSize;Foam Noise Size;33;0;Create;True;0;0;0;False;0;False;10,10;43.06,66.85;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;177;-788.4554,5302.894;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ExpOpNode;161;-1672.553,5302.782;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;77;-370.0577,734.0793;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;205;1074.212,3623.068;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;53;-1202.466,191.2389;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;55;-1530.466,328.2389;Inherit;False;Constant;_Float2;Float 2;9;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;389;-260.0707,2550.24;Inherit;False;LightSpecularLerpValue;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;365;2523.551,-275.7819;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;364;2275.312,-281.6769;Inherit;False;362;Light;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;346;-612.8114,1686.658;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;52;-1524.466,80.23895;Inherit;False;Constant;_Float1;Float 1;9;0;Create;True;0;0;0;False;0;False;2;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;115;-1446.256,4126.604;Inherit;False;FLOAT2;0;2;2;3;1;0;FLOAT3;0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SwizzleNode;153;-2866.218,4520.986;Inherit;False;FLOAT;1;1;2;3;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;379;-1145.344,2359.123;Inherit;False;Constant;_Float8;Float 8;50;0;Create;True;0;0;0;False;0;False;128;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;51;-1358.466,61.23891;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;276;1351.208,163.7692;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;367;2436.205,-379.736;Inherit;False;Property;_UseDirectionLight;Use Direction Light;39;2;[Header];[Enum];Create;True;1;Light Setting;2;Off;0;On;1;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;96;-459.4901,1375.745;Inherit;False;UnderWaterColor;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;185;790.6879,2983.168;Inherit;False;Property;_FoamRange;Foam Range;29;1;[Header];Create;True;1;Foam Setting;0;0;False;0;False;1;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;228;502.9944,3908.55;Inherit;False;Property;_FoamDistortGenerate;Foam Distort Generate;34;0;Create;True;0;0;0;False;0;False;5,5;6.7,300;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleTimeNode;146;-1626.383,4562.811;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.NegateNode;142;-1081.715,4260.014;Inherit;False;1;0;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.OneMinusNode;140;-1549.823,4721.196;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.Vector4Node;413;-1426.52,2833.959;Inherit;False;Property;_SparklingXYSpeedDistortXYSpeed;Sparkling XY Speed / Distort XY Speed;50;0;Create;True;0;0;0;False;0;False;0,0,0,0;0,0,0,0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DynamicAppendNode;414;-742.913,2854.814;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.DynamicAppendNode;415;-1097.462,2911.589;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;156;-2575.218,4354.986;Inherit;False;waterDeepNew;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.PannerNode;397;-586.0593,2697.502;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;159;-1787.553,5303.782;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;407;-1394.271,3109.913;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.NegateNode;158;-1916.553,5303.782;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;126;329.3562,4112.053;Inherit;False;Caustics;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.NegateNode;129;-1539.04,4646.247;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ScreenColorNode;89;-1162.115,1438.312;Inherit;False;Global;_GrabScreen0;Grab Screen 0;13;0;Create;True;0;0;0;False;0;False;Object;-1;False;False;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SaturateNode;136;-1708.012,4720.735;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;427;-1289.207,1219.705;Inherit;True;Property;_TextureSample2;Texture Sample 2;61;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;278;-1849.193,1878.865;Inherit;False;Property;_RefractionNormalSize;Refraction Normal Size;18;0;Create;True;0;0;0;False;0;False;0;1.1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMinOpNode;144;-343.452,4116.161;Inherit;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;92;-1801.165,1585.793;Inherit;False;Property;_UnderWaterDistortion;UnderWater Distortion;16;1;[Header];Create;True;1;Refraction;0;0;False;0;False;1;10.29;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;409;-791.2358,3408.523;Inherit;False;Property;_DistortNoiseStrengthX;Distort Noise Strength X;52;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TFHCRemapNode;106;-1505.95,1788.707;Inherit;False;5;0;FLOAT3;0,0,0;False;1;FLOAT3;-1,-1,-1;False;2;FLOAT3;1,1,1;False;3;FLOAT3;-1,-1,-1;False;4;FLOAT3;4,4,4;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;93;-1799.165,1693.793;Inherit;False;Constant;_Float5;Float 5;13;0;Create;True;0;0;0;False;0;False;0.01;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;91;-1389.473,1611.771;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;94;-1420.146,1404.221;Inherit;False;2;2;0;FLOAT4;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.ColorNode;166;-1792.553,5643.782;Inherit;False;Property;_ShoreColor;ShoreColor;25;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,1;0.254717,0.254717,0.254717,0;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;95;-1838.918,1783.306;Inherit;False;49;SurfaceNormal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.TexturePropertyNode;426;-1894.799,1215.873;Inherit;True;Global;_GrabPassTransparent;_GrabPassTransparent;59;1;[HideInInspector];Create;True;0;0;0;False;0;False;None;;False;white;Auto;Texture2D;-1;0;2;SAMPLER2D;0;SAMPLERSTATE;1
Node;AmplifyShaderEditor.SimpleAddOpNode;273;2858.503,158.2884;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;417;-533.1963,3426.032;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SaturateNode;421;329.6733,2737.638;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;408;-401.8079,3113.594;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;114;-1693.365,4128.355;Inherit;False;PositionFromDepth;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;42;-1170.683,-56.10139;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;66;-1528.74,1088.267;Inherit;False;Constant;_Float3;Float 3;11;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;197;1231.112,2642.388;Inherit;False;Property;_FoamBlend;Foam Blend;32;0;Create;True;0;0;0;False;0;False;0;0.45;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;190;1015.126,3491.965;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;183;982.7015,2846.676;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;123;-1815.17,4485.928;Inherit;False;Constant;_Float4;Float 4;18;0;Create;True;0;0;0;False;0;False;0.005;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;194;932.9883,3036.469;Inherit;False;Property;_FoamFreq;Foam Freq;31;0;Create;True;0;0;0;False;0;False;10;15.77;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.FunctionNode;111;-2246.365,4134.355;Inherit;False;Reconstruct World Position From Depth;-1;;5;e7094bcbcc80eb140b2a3dbe6a861de8;0;0;1;FLOAT4;0
Node;AmplifyShaderEditor.RangedFloatNode;145;-1818.11,4559.356;Inherit;False;Constant;_Float6;Float 6;18;0;Create;True;0;0;0;False;0;False;0.0025;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;165;-1557.553,5559.782;Inherit;False;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;137;-2169.646,4735.76;Inherit;False;Property;_CausticsDepth;Caustics Depth;22;0;Create;True;0;0;0;False;0;False;1;3.88;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.Vector2Node;124;-1872.442,4319.194;Inherit;False;Constant;_CausticsSpeedXY;Caustics Speed XY;18;0;Create;True;0;0;0;False;0;False;8,8;0,0;0;3;FLOAT2;0;FLOAT;1;FLOAT;2
Node;AmplifyShaderEditor.OneMinusNode;175;-1241.913,5422.616;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;176;-1514.217,5429.095;Inherit;False;Property;_ShoreEdgeWidth;Shore Edge Width;26;0;Create;True;0;0;0;False;0;False;0;1;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;160;-2012.553,5410.782;Inherit;False;Property;_ShoreRange;Shore Range;24;1;[Header];Create;True;1;Shore Setting;0;0;False;0;False;1;16.16;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.WorldPosInputsNode;155;-2963.218,4312.986;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.LerpOp;99;1175.331,-331.406;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;143;-978.7148,4358.014;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;152;-3097.08,4521.121;Inherit;False;114;PositionFromDepth;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;134;91.62962,4117.369;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;186;1111.314,2845.263;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMaxOpNode;182;2142.643,76.19606;Inherit;False;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;133;-746.2878,4719.885;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;147;-1313.564,4479.868;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SamplerNode;121;-831.1145,4107.845;Inherit;True;Property;_CausticsMap;Caustics Map;21;0;Create;True;0;0;0;False;0;False;-1;None;d663d27896c87a44d9100f4ac464eaa8;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.PannerNode;410;-1108.842,3108.908;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;168;-1229.553,5552.782;Inherit;False;ShoreColor;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;256;2513.279,973.5927;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;120;-1366.999,4325.787;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;164;-1782.553,5508.782;Inherit;False;163;waterShore;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;357;-963.7515,2659.834;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;360;-900.5937,2114.168;Inherit;False;Property;_LightColorBlend;Light Color Blend;41;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,0;766.9961,241.7356,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;189;1027.591,3376.792;Inherit;False;Property;_FoamSpeed;Foam Speed;30;0;Create;True;0;0;0;False;0;False;1;-2.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;181;1816.419,340.1961;Inherit;False;179;ShoreEdge;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;65;-1584.74,994.267;Inherit;False;Property;_ReflectionDistortion;Reflection Distortion;12;1;[Header];Create;True;1;Reflection;0;0;False;0;False;1;5.37;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;356;-1212.751,2722.834;Inherit;False;Property;_LightEdgeSmoothness;Light Edge Smoothness;43;0;Create;True;0;0;0;False;0;False;0;0.01;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;359;-313.5781,2129.066;Inherit;False;4;4;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;3;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.PannerNode;229;1022.649,3894.162;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0.3,-0.5;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;353;-1155.929,2428.691;Inherit;False;Property;_LightSpecular;Light Specular;44;0;Create;True;0;0;0;False;0;False;0;44.14;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DotProductOpNode;350;-1142.673,2267.745;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;48;-1036.705,-52.94036;Inherit;True;Property;_NormalMap;Normal Map;11;0;Create;True;0;0;0;False;0;False;48;None;e67cbb57d1280cb4f96af95dce0828a7;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleAddOpNode;230;1346.185,3683.839;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.ClampOpNode;255;2305.699,995.3563;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;82;-1534.784,647.6156;Inherit;True;Property;_Bump;Bump;13;0;Create;True;0;0;0;False;0;False;-1;c3bb1d5298b924d80959c9f822ffe414;None;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;260;549.2504,-222.9779;Inherit;False;258;WaveColor;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;226;1227.017,3883.656;Inherit;True;Simple;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;2.66;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;118;-1132.172,4135.633;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;384;-1856.92,2252.77;Inherit;False;382;Bump;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LightColorNode;348;-530.8351,2137.821;Inherit;False;0;3;COLOR;0;FLOAT3;1;FLOAT;2
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;378;-995.4203,2361.695;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;49;-489.7047,57.05962;Inherit;False;SurfaceNormal;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleAddOpNode;373;-1137.293,2143.196;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;63;-1371.334,1028.733;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;54;-1357.466,215.2389;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.5;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;70;539.5178,-313.285;Inherit;False;68;ReflectionColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;217;2543.024,2624.208;Inherit;False;FoamColor;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;369;-36.17096,2272.757;Inherit;True;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.StaticSwitch;266;869.5366,-311.5028;Inherit;False;Property;_Keyword1;Keyword 1;58;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Reference;262;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;393;-261.7209,2694.838;Inherit;True;Property;_SparklingTexture;Sparkling Texture;48;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ViewDirInputsCoordNode;374;-1656.539,2462.974;Inherit;False;World;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.RangedFloatNode;400;7.153862,2610.862;Inherit;False;Property;_SparklingStep;Sparkling Step;49;0;Create;True;0;0;0;False;0;False;0.5;0.5;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;202;1506.181,3613.062;Inherit;True;Gradient;True;False;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;354;-821.4873,2268.083;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;231;1935.366,3039.577;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.NormalizeNode;376;-1351.539,2534.974;Inherit;False;False;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;387;-1098.039,2516.386;Inherit;False;Property;_Gloss;Gloss;45;0;Create;True;0;0;0;False;0;False;0;0.02;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StepOpNode;210;2085.696,2929.536;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;212;1809.724,2929.574;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;232;1775.527,85.42676;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.DepthFade;107;748.2054,-95.21638;Inherit;False;True;False;True;2;1;FLOAT3;0,0,0;False;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.FresnelNode;78;-852.2654,949.3293;Inherit;False;Standard;WorldNormal;ViewDir;False;False;5;0;FLOAT3;0,0,1;False;4;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;1;False;3;FLOAT;5;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;171;1555.58,-11.70784;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SaturateNode;377;-997.9835,2269.426;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;370;-667.5305,2616.652;Inherit;False;Property;_SRPAdditionLightSmoothness;SRP Addition Light Smoothness;46;0;Create;True;0;0;0;False;0;False;0;1;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;277;1127.208,261.7692;Inherit;False;Property;_AllShoreSubstract;All Shore Substract;28;0;Create;True;0;0;0;False;0;False;0;-2.52;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;416;-785.7834,3492.076;Inherit;False;Property;_DistortNoiseStrengthY;Distort Noise Strength Y;53;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;227;808.8395,3890.162;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;30;1130.879,-143.8262;Inherit;False;29;WaterColor;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;84;-1035.123,656.9529;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;267;1805.829,-77.92681;Inherit;False;Property;_UseFoam;Use Foam ?;38;0;Create;True;0;0;0;False;0;False;0;0;1;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;47;-1946.705,274.0596;Inherit;False;Constant;_Float0;Float 0;8;0;Create;True;0;0;0;False;0;False;0.01;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SamplerNode;50;-1032.705,164.0597;Inherit;True;Property;_TextureSample0;Texture Sample 0;8;0;Create;True;0;0;0;False;0;False;-1;None;None;True;0;False;bump;Auto;True;Instance;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.GetLocalVarNode;220;1338.122,329.5337;Inherit;False;217;FoamColor;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;213;1620.466,3003.1;Inherit;False;Property;_FoamWidth;Foam Width;36;0;Create;True;0;0;0;False;0;False;0;0.72;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;100;808.3312,-424.406;Inherit;False;96;UnderWaterColor;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;355;-1187.489,2638.834;Inherit;False;Property;_LightEdgeValue;Light Edge Value;42;0;Create;True;0;0;0;False;0;False;0;0.97;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;207;1798.884,3357.101;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;86;1373.758,-166.7568;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;75;-167.4554,-389.636;Inherit;False;waterFresnel;-1;True;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;43;-1790.681,190.6321;Inherit;False;Property;_NormalSpeed;Normal Speed;10;0;Create;True;0;0;0;False;0;False;0;-2.4;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;198;1398.112,2539.388;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0.1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;108;469.9448,49.84232;Inherit;False;Property;_RefractionReflectionEffect;Refraction/Reflection Effect;17;0;Create;True;0;0;0;False;0;False;0;13.8;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;263;2532.647,486.0587;Inherit;False;Property;_Keyword1;Keyword 1;58;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Reference;262;True;True;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldNormalVector;349;-1382.506,2143.912;Inherit;False;False;1;0;FLOAT3;0,0,1;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.OneMinusNode;196;1793.248,2623.816;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GetLocalVarNode;172;1343.13,18.78299;Inherit;False;168;ShoreColor;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;109;992.3689,-92.07683;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;29;-193.7336,-682.2092;Inherit;False;WaterColor;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;204;801.6813,3620.704;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SwizzleNode;254;2140.869,989.8893;Inherit;False;FLOAT;1;1;2;3;1;0;FLOAT3;0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.IndirectSpecularLight;73;-850.0426,763.9872;Inherit;False;Tangent;3;0;FLOAT3;0,0,1;False;1;FLOAT;1;False;2;FLOAT;1;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldSpaceLightDirHlpNode;351;-1703.572,2644.745;Inherit;False;False;1;0;FLOAT;0;False;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.GetLocalVarNode;62;-1579.74,883.267;Inherit;False;49;SurfaceNormal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.PowerNode;352;-634.4548,2267.083;Inherit;False;False;2;0;FLOAT;0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;268;-497.504,-948.804;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.GrabScreenPosition;90;-1811.871,1398.328;Inherit;False;0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SwizzleNode;39;-1709.863,-64.79423;Inherit;False;FLOAT2;0;2;2;3;1;0;FLOAT3;0,0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.GetLocalVarNode;150;-826.4707,1638.075;Inherit;False;126;Caustics;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;154;-2727.218,4358.986;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleTimeNode;46;-1786.705,279.0596;Inherit;False;1;0;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;372;-672.7094,2438.542;Inherit;False;Property;_SRPAdditionSpecularColor;SRP Addition Specular Color;47;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,0;1,1,1,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SwizzleNode;233;1593.527,336.4268;Inherit;False;FLOAT;3;1;2;3;1;0;COLOR;0,0,0,0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;375;-1472.539,2535.974;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SaturateNode;422;-600.7632,3210.49;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;208;1950.862,3130.125;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SmoothstepOpNode;195;1557.249,2622.816;Inherit;False;3;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;131;-1279.382,4965.039;Inherit;False;Constant;_CausticsRange;Caustics Range;17;0;Create;True;0;0;0;False;0;False;1;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleDivideOpNode;130;-1077.383,4721.039;Inherit;False;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;41;-1713.863,9.205744;Inherit;False;Property;_NormalScale;NormalScale;9;1;[Header];Create;True;1;Normal Setting;0;0;False;0;False;1;39.63;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.StaticSwitch;262;2537.647,370.0587;Inherit;False;Property;_UseVertexOffset;Use VertexOffset ?;58;0;Create;True;0;0;0;False;0;False;0;0;1;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;FLOAT3;0,0,0;False;0;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT3;0,0,0;False;4;FLOAT3;0,0,0;False;5;FLOAT3;0,0,0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.WorldPosInputsNode;38;-1883.986,-59.08321;Inherit;False;0;4;FLOAT3;0;FLOAT;1;FLOAT;2;FLOAT;3
Node;AmplifyShaderEditor.GetLocalVarNode;358;-1887.788,2330.465;Inherit;False;49;SurfaceNormal;1;0;OBJECT;;False;1;FLOAT3;0
Node;AmplifyShaderEditor.GrabScreenPosition;425;-1612.844,1229.053;Inherit;False;0;0;5;FLOAT4;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleSubtractOpNode;252;1883.067,1046.963;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.LerpOp;383;-1633.347,2286.801;Inherit;False;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.RangedFloatNode;406;-1111.153,3250.481;Inherit;False;Property;_DistortNoiseSize;Distort Noise Size;51;0;Create;True;0;0;0;False;0;False;3;3;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;148;-775.6421,1373.453;Inherit;False;sceneColor;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RangedFloatNode;385;-1960.702,2425.827;Inherit;False;Property;_LightDistortStrength;Light Distort Strength;40;0;Create;True;0;0;0;False;0;False;0;0.32;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;218;2026.965,2469.485;Inherit;False;Property;_FoamColor;Foam Color;37;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,1;3.688605,3.688605,3.688605,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RegisterLocalVarNode;362;581.5347,2265.551;Inherit;False;Light;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;419;2159.743,-355.031;Inherit;False;AllNodewithoutLight;-1;True;1;0;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;199;2183.468,2629.426;Inherit;False;3;3;0;FLOAT;0;False;1;FLOAT;0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;366;2879.759,30.35944;Inherit;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleAddOpNode;259;740.25,-264.9779;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;420;36.12276,2495.837;Inherit;False;419;AllNodewithoutLight;1;0;OBJECT;;False;1;COLOR;0
Node;AmplifyShaderEditor.RegisterLocalVarNode;382;-1224.517,703.8973;Inherit;False;Bump;-1;True;1;0;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SinOpNode;192;1555.365,3403.828;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;64;-1209.087,885.1802;Inherit;False;2;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.StaticSwitch;423;-937.2516,1488.982;Inherit;False;Property;_RefractionIncludeTransparent;Refraction Include Transparent ?;19;0;Create;True;0;0;0;False;0;False;0;0;0;True;;Toggle;2;Key0;Key1;Create;True;True;9;1;COLOR;0,0,0,0;False;0;COLOR;0,0,0,0;False;2;COLOR;0,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;0,0,0,0;False;5;COLOR;0,0,0,0;False;6;COLOR;0,0,0,0;False;7;COLOR;0,0,0,0;False;8;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;184;759.0946,2841.993;Inherit;False;156;waterDeepNew;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.BlendNormalsNode;56;-708.6932,61.56707;Inherit;False;0;3;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;193;1407.108,3015.946;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SwizzleNode;234;1581.527,235.4268;Inherit;False;FLOAT3;0;1;2;3;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.OneMinusNode;187;1244.813,2925.18;Inherit;False;1;0;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;347;-1080.392,1774.249;Inherit;False;Property;_CausticsAlpha;Caustics Alpha;23;0;Create;True;0;0;0;False;0;False;0;0.46;0;1;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;191;1260.796,3427.021;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.ColorNode;257;2190.125,811.0565;Inherit;False;Property;_WaveColor;WaveColor;57;1;[HDR];Create;True;0;0;0;False;0;False;0.3396226,0.3396226,0.3396226,1;0.2068886,0.5254112,0.5566038,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleDivideOpNode;40;-1554.863,-59.79421;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.FunctionNode;368;-304.0862,2298.442;Inherit;False;SRP Additional Light;-1;;4;6c86746ad131a0a408ca599df5f40861;3,6,2,9,0,23,0;5;2;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;15;FLOAT3;0,0,0;False;14;FLOAT3;1,1,1;False;18;FLOAT;0.5;False;1;FLOAT3;0
Node;AmplifyShaderEditor.NoiseGeneratorNode;405;-876.7052,3105.795;Inherit;True;Simple;True;True;2;0;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT;0
Node;AmplifyShaderEditor.LerpOp;388;2603.715,-155.4434;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;272;2354.61,34.88568;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.GetLocalVarNode;390;2195.057,-206.8568;Inherit;False;389;LightSpecularLerpValue;1;0;OBJECT;;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;398;363.6269,2201;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.LerpOp;418;352.0106,2398.637;Inherit;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;1;3267.209,31.8599;Float;False;True;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;To7/ToonWater;2992e84f91cbeb14eab234972e07ea9d;True;Forward;0;1;Forward;8;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Transparent=RenderType;Queue=Transparent=Queue=0;True;7;0;False;True;1;5;False;-1;10;False;-1;1;1;False;-1;10;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;True;True;True;True;0;False;-1;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;2;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;1;LightMode=UniversalForward;False;0;Hidden/InternalErrorShader;0;0;Standard;22;Surface;1;  Blend;0;Two Sided;1;Cast Shadows;0;  Use Shadow Threshold;0;Receive Shadows;0;GPU Instancing;1;LOD CrossFade;0;Built-in Fog;0;DOTS Instancing;0;Meta Pass;0;Extra Pre Pass;0;Tessellation;1;  Phong;1;  Strength;0.5,False,-1;  Type;0;  Tess;1.4,False,-1;  Min;10,False,-1;  Max;25,False,-1;  Edge Length;16,False,-1;  Max Displacement;25,False,-1;Vertex Position,InvertActionOnDeselection;0;0;5;False;True;False;True;False;False;;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;-670.6996,-932.1618;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;ExtraPrePass;0;0;ExtraPrePass;5;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;True;1;1;False;-1;0;False;-1;0;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;True;True;True;True;0;False;-1;False;False;False;False;False;False;False;True;False;255;False;-1;255;False;-1;255;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;1;False;-1;True;3;False;-1;True;True;0;False;-1;0;False;-1;True;0;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;3;0,0;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;DepthOnly;0;3;DepthOnly;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;False;False;True;False;False;False;False;0;False;-1;False;False;False;False;False;False;False;False;False;True;1;False;-1;False;False;True;1;LightMode=DepthOnly;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;4;0,0;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;Meta;0;4;Meta;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;LightMode=Meta;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;2;0,0;Float;False;False;-1;2;UnityEditor.ShaderGraph.PBRMasterGUI;0;3;New Amplify Shader;2992e84f91cbeb14eab234972e07ea9d;True;ShadowCaster;0;2;ShadowCaster;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;3;RenderPipeline=UniversalPipeline;RenderType=Opaque=RenderType;Queue=Geometry=Queue=0;True;0;0;False;False;False;False;False;False;False;False;False;False;False;False;True;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;False;True;1;False;-1;True;3;False;-1;False;True;1;LightMode=ShadowCaster;False;0;Hidden/InternalErrorShader;0;0;Standard;0;False;0
WireConnection;16;0;17;0
WireConnection;12;0;16;0
WireConnection;270;0;12;0
WireConnection;270;1;269;0
WireConnection;15;0;270;0
WireConnection;23;0;22;0
WireConnection;24;0;23;0
WireConnection;24;1;25;0
WireConnection;26;0;24;0
WireConnection;428;1;432;0
WireConnection;235;0;236;0
WireConnection;235;3;237;0
WireConnection;433;1;428;1
WireConnection;433;0;428;2
WireConnection;433;2;428;3
WireConnection;433;3;428;4
WireConnection;27;0;26;0
WireConnection;243;0;236;0
WireConnection;243;1;235;2
WireConnection;243;2;235;3
WireConnection;243;3;249;0
WireConnection;32;3;34;0
WireConnection;434;0;433;0
WireConnection;434;1;435;0
WireConnection;21;0;19;0
WireConnection;21;1;20;0
WireConnection;21;2;27;0
WireConnection;244;0;243;3
WireConnection;244;1;243;2
WireConnection;250;0;236;0
WireConnection;250;1;243;2
WireConnection;250;2;243;3
WireConnection;250;3;251;0
WireConnection;31;0;21;0
WireConnection;31;1;33;0
WireConnection;31;2;32;0
WireConnection;437;0;434;0
WireConnection;437;1;438;0
WireConnection;238;0;236;0
WireConnection;238;1;235;0
WireConnection;238;2;243;0
WireConnection;238;3;250;0
WireConnection;35;0;31;0
WireConnection;439;0;437;0
WireConnection;245;0;244;0
WireConnection;36;0;35;0
WireConnection;246;0;245;0
WireConnection;239;0;238;0
WireConnection;441;0;439;0
WireConnection;241;0;239;0
WireConnection;247;0;246;0
WireConnection;442;0;37;0
WireConnection;442;1;443;0
WireConnection;135;0;137;0
WireConnection;180;0;267;0
WireConnection;180;1;181;0
WireConnection;258;0;256;0
WireConnection;163;0;162;0
WireConnection;221;0;194;0
WireConnection;221;1;223;0
WireConnection;122;0;123;0
WireConnection;44;0;43;0
WireConnection;44;1;46;0
WireConnection;141;1;143;0
WireConnection;386;0;352;0
WireConnection;386;1;387;0
WireConnection;399;0;400;0
WireConnection;399;1;393;1
WireConnection;174;0;163;0
WireConnection;174;1;175;0
WireConnection;116;0;115;0
WireConnection;116;1;117;0
WireConnection;167;0;165;0
WireConnection;188;0;193;0
WireConnection;188;1;191;0
WireConnection;188;2;222;0
WireConnection;162;0;161;0
WireConnection;68;0;77;0
WireConnection;412;0;397;0
WireConnection;412;1;408;0
WireConnection;179;0;177;0
WireConnection;138;0;140;0
WireConnection;113;0;111;0
WireConnection;149;0;148;0
WireConnection;149;1;346;0
WireConnection;222;0;187;0
WireConnection;222;1;221;0
WireConnection;87;1;84;0
WireConnection;177;0;174;0
WireConnection;177;1;178;0
WireConnection;161;0;159;0
WireConnection;77;1;73;0
WireConnection;77;2;78;0
WireConnection;205;0;204;0
WireConnection;205;1;206;0
WireConnection;53;0;51;0
WireConnection;53;1;54;0
WireConnection;389;0;386;0
WireConnection;365;0;364;0
WireConnection;365;1;272;0
WireConnection;346;0;150;0
WireConnection;346;1;347;0
WireConnection;115;0;114;0
WireConnection;153;0;152;0
WireConnection;51;0;40;0
WireConnection;51;1;52;0
WireConnection;276;0;173;0
WireConnection;276;1;277;0
WireConnection;96;0;149;0
WireConnection;146;0;145;0
WireConnection;142;0;116;0
WireConnection;140;0;136;0
WireConnection;414;0;413;1
WireConnection;414;1;413;2
WireConnection;415;0;413;3
WireConnection;415;1;413;4
WireConnection;156;0;154;0
WireConnection;397;0;396;0
WireConnection;397;2;414;0
WireConnection;159;0;158;0
WireConnection;159;1;160;0
WireConnection;158;0;157;0
WireConnection;126;0;134;0
WireConnection;129;0;136;0
WireConnection;89;0;94;0
WireConnection;136;0;135;0
WireConnection;427;0;426;0
WireConnection;427;1;94;0
WireConnection;144;0;121;0
WireConnection;144;1;141;0
WireConnection;106;0;95;0
WireConnection;106;4;278;0
WireConnection;91;0;92;0
WireConnection;91;1;93;0
WireConnection;91;2;106;0
WireConnection;94;0;90;0
WireConnection;94;1;91;0
WireConnection;273;0;442;0
WireConnection;273;1;274;0
WireConnection;417;0;409;0
WireConnection;417;1;416;0
WireConnection;421;0;399;0
WireConnection;408;0;422;0
WireConnection;408;1;417;0
WireConnection;114;0;113;0
WireConnection;42;0;40;0
WireConnection;42;1;44;0
WireConnection;183;0;184;0
WireConnection;183;1;185;0
WireConnection;165;0;164;0
WireConnection;165;1;166;0
WireConnection;175;0;176;0
WireConnection;99;0;100;0
WireConnection;99;1;266;0
WireConnection;99;2;109;0
WireConnection;143;0;142;0
WireConnection;143;1;147;0
WireConnection;134;0;144;0
WireConnection;134;1;133;0
WireConnection;186;0;183;0
WireConnection;182;0;180;0
WireConnection;133;0;130;0
WireConnection;147;0;124;0
WireConnection;147;1;146;0
WireConnection;121;1;118;0
WireConnection;410;0;407;0
WireConnection;410;2;415;0
WireConnection;168;0;167;0
WireConnection;256;0;257;0
WireConnection;256;1;255;0
WireConnection;120;0;124;0
WireConnection;120;1;122;0
WireConnection;357;0;355;0
WireConnection;357;1;356;0
WireConnection;359;0;348;0
WireConnection;359;1;386;0
WireConnection;359;2;360;0
WireConnection;359;3;348;2
WireConnection;229;0;227;0
WireConnection;350;0;349;0
WireConnection;350;1;376;0
WireConnection;48;1;42;0
WireConnection;230;0;226;0
WireConnection;230;1;205;0
WireConnection;255;0;254;0
WireConnection;226;0;229;0
WireConnection;118;0;116;0
WireConnection;118;1;120;0
WireConnection;378;0;379;0
WireConnection;378;1;353;0
WireConnection;49;0;56;0
WireConnection;63;0;65;0
WireConnection;63;1;66;0
WireConnection;54;0;44;0
WireConnection;54;1;55;0
WireConnection;217;0;199;0
WireConnection;369;0;359;0
WireConnection;369;1;368;0
WireConnection;266;1;70;0
WireConnection;266;0;259;0
WireConnection;393;1;412;0
WireConnection;202;0;230;0
WireConnection;354;0;377;0
WireConnection;354;1;355;0
WireConnection;354;2;357;0
WireConnection;231;0;212;0
WireConnection;231;1;226;0
WireConnection;376;0;375;0
WireConnection;210;0;212;0
WireConnection;210;1;208;0
WireConnection;212;0;187;0
WireConnection;212;1;213;0
WireConnection;232;0;171;0
WireConnection;232;1;234;0
WireConnection;232;2;233;0
WireConnection;107;0;108;0
WireConnection;78;2;85;3
WireConnection;78;3;85;4
WireConnection;171;0;86;0
WireConnection;171;1;172;0
WireConnection;171;2;276;0
WireConnection;377;0;350;0
WireConnection;227;0;228;0
WireConnection;84;0;82;0
WireConnection;84;1;64;0
WireConnection;267;1;171;0
WireConnection;267;0;232;0
WireConnection;50;1;53;0
WireConnection;207;0;192;0
WireConnection;207;1;202;0
WireConnection;86;0;99;0
WireConnection;86;1;30;0
WireConnection;75;0;32;0
WireConnection;198;0;186;0
WireConnection;263;1;264;0
WireConnection;263;0;248;0
WireConnection;349;0;383;0
WireConnection;196;0;195;0
WireConnection;109;0;107;0
WireConnection;29;0;31;0
WireConnection;254;0;252;0
WireConnection;73;0;84;0
WireConnection;73;1;85;1
WireConnection;73;2;85;2
WireConnection;352;0;354;0
WireConnection;352;1;378;0
WireConnection;268;0;12;0
WireConnection;268;1;269;0
WireConnection;39;0;38;0
WireConnection;154;0;155;2
WireConnection;154;1;153;0
WireConnection;46;0;47;0
WireConnection;233;0;220;0
WireConnection;375;0;374;0
WireConnection;375;1;351;0
WireConnection;422;0;405;0
WireConnection;208;0;207;0
WireConnection;208;1;209;0
WireConnection;195;0;198;0
WireConnection;195;1;197;0
WireConnection;130;0;138;0
WireConnection;130;1;131;0
WireConnection;262;1;265;0
WireConnection;262;0;242;0
WireConnection;252;0;238;0
WireConnection;252;1;253;0
WireConnection;383;0;384;0
WireConnection;383;1;358;0
WireConnection;383;2;385;0
WireConnection;148;0;423;0
WireConnection;362;0;418;0
WireConnection;419;0;272;0
WireConnection;199;0;196;0
WireConnection;199;1;210;0
WireConnection;199;2;218;0
WireConnection;366;0;272;0
WireConnection;366;1;388;0
WireConnection;366;2;367;0
WireConnection;259;0;70;0
WireConnection;259;1;260;0
WireConnection;382;0;82;0
WireConnection;192;0;188;0
WireConnection;64;0;62;0
WireConnection;64;1;63;0
WireConnection;423;1;89;0
WireConnection;423;0;427;0
WireConnection;56;0;48;0
WireConnection;56;1;50;0
WireConnection;193;0;187;0
WireConnection;193;1;194;0
WireConnection;234;0;220;0
WireConnection;187;0;186;0
WireConnection;191;0;189;0
WireConnection;191;1;190;0
WireConnection;40;0;39;0
WireConnection;40;1;41;0
WireConnection;368;2;383;0
WireConnection;368;14;372;0
WireConnection;368;18;370;0
WireConnection;405;0;410;0
WireConnection;405;1;406;0
WireConnection;388;0;272;0
WireConnection;388;1;364;0
WireConnection;388;2;390;0
WireConnection;272;0;271;0
WireConnection;272;1;182;0
WireConnection;398;0;369;0
WireConnection;398;1;399;0
WireConnection;418;0;420;0
WireConnection;418;1;369;0
WireConnection;418;2;421;0
WireConnection;1;2;366;0
WireConnection;1;3;273;0
WireConnection;1;5;262;0
WireConnection;1;6;263;0
ASEEND*/
//CHKSM=C7A6F8E8C2E199DC197CF7A84139AFA2C49C732F