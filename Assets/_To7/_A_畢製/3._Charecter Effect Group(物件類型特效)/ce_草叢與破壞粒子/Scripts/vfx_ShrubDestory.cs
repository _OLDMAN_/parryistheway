using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class vfx_ShrubDestory : MonoBehaviour
{   [Header("放入 vfx_草叢破壞 粒子系統 ,本腳本掛載於 ce_shrub_01/ce_Shrub_02 欲置物中")]
    public GameObject vfx_shrubParticleSystem;
    public void vfx_ShrubDisappear()
    {
        GameObject this_Particle = Instantiate(vfx_shrubParticleSystem) as GameObject;
        Destroy(this_Particle , 2f);
        Destroy(this.gameObject);
    }
}
