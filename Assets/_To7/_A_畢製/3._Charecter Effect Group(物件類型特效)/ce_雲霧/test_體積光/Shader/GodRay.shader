Shader "lsc/RaytraceShader"
{
    Properties
    {
        _MainTex ("Texture", 2D) = "white" {}
        _raytrace_step_count("rayrace step count", Int) = 5
        _scale("scale", float) = 1.0
    }
    SubShader
    {
        // No culling or depth
        Cull Off ZWrite Off ZTest Always
 
        Pass
        {
            HLSLPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS
            #pragma multi_compile _ _MAIN_LIGHT_SHADOWS_CASCADE
            #pragma multi_compile_fragment _ _ADDITIONAL_LIGHT_SHADOWS
            #pragma multi_compile_fragment _ _SHADOWS_SOFT
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Lighting.hlsl"
 
            struct appdata
            {
                float4 vertex : POSITION;
                float2 uv : TEXCOORD0;
            };
 
            struct v2f
            {
                float2 uv : TEXCOORD0;
                float4 vertex : SV_POSITION;
                float4 screen_pos : TEXCOORD1;
            };
 
            float4x4 _mtx_view_inv;
            float4x4 _mtx_proj_inv;
            TEXTURE2D_X_FLOAT(_CameraDepthTexture);
            SAMPLER(sampler_CameraDepthTexture);
 
            v2f vert (appdata v)
            {
                v2f o;
 
                VertexPositionInputs vertexInput = GetVertexPositionInputs(v.vertex.xyz);
                o.vertex = vertexInput.positionCS;
                o.screen_pos = ComputeScreenPos(o.vertex);
 
                o.uv = v.uv;
 
                return o;
            }
 
            sampler2D _MainTex;
            int _raytrace_step_count;
            float _scale;
 
            float4 cal_world_pos_by_dep(float ndc_dep, float2 screen_space, out float4 view_pos)
            {
                // 取出非线性深度与视深度
                float linearDepthZ = LinearEyeDepth(ndc_dep, _ZBufferParams);
                // 屏幕转ndc
                float4 ndc_pos;
                ndc_pos.xy = screen_space * 2.0 - 1.0;
                ndc_pos.zw = float2(ndc_dep, 1);
                // 添加齐次因子
                ndc_pos = ndc_pos * linearDepthZ;
                // 转成观察与世界坐标
                view_pos = mul(_mtx_proj_inv, ndc_pos);
                float4 world_pos = mul(_mtx_view_inv, float4(view_pos.xyz, 1));
 
                return world_pos;
            }
 
 
            float4 frag (v2f i) : SV_Target
            {
                float4 col = tex2D(_MainTex, i.uv);
 
                // 插值后的屏幕坐标去除齐次因子
                float2 screen_space = i.screen_pos.xy / i.screen_pos.w;
                // 取出非线性深度
                float org_depth = SAMPLE_TEXTURE2D_X(_CameraDepthTexture, sampler_CameraDepthTexture, screen_space).x;
                // 计算世界坐标
                float4 view_pos;
                float4 world_pos = cal_world_pos_by_dep(org_depth, screen_space, view_pos);
 
                float3 cam_wpos = GetCameraPositionWS();
                float3 v_step = (world_pos - cam_wpos) / _raytrace_step_count;
 
                float3 rt_start = cam_wpos;
                float shadow_atten = 0;
                UNITY_LOOP
                for (int i = 0; i < _raytrace_step_count; i++)//循环,超级低效
                {
                    float4 shadow_coord = TransformWorldToShadowCoord(rt_start);
                    rt_start += v_step;
 
                    Light mainLight = GetMainLight(shadow_coord);//这样产生了级联阴影采样
                    shadow_atten += mainLight.shadowAttenuation;
                }
 
                shadow_atten = (shadow_atten / _raytrace_step_count) * _scale;
 
                col.rgb = col.rgb * shadow_atten;
 
                return col;
            }
            ENDHLSL
        }
    }
}

