using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class RadiusBlurCenterPosition : MonoBehaviour
{
    // Start is called before the first frame update
    public Material RadiusBlurMaterial;
    private Camera cam;
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 ScreenBlurPos = cam.WorldToViewportPoint(transform.position);
        RadiusBlurMaterial.SetVector("_PlayerPosition" , ScreenBlurPos);
    }
}
