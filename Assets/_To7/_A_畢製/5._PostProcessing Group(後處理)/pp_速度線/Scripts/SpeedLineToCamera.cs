using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[ExecuteInEditMode]
public class SpeedLineToCamera : MonoBehaviour
{
    public GameObject testbox;
    void Update()
    {
        transform.forward = Vector3.Normalize(Camera.main.transform.position - transform.position);

        //Quaternion q = Quaternion.identity;
        //q.SetLookRotation(Camera.main.transform.forward);
        //this.transform.rotation = q;
        //GetComponentInChildren<ParticleSystem>().emission = em;
        //Burst();
    }
    
    public void Burst()
    {
        //計算Y=0平面上的照相機與碰撞點位置
        Vector3 collisionPosition = new Vector3(transform.position.x , 0 , transform.position.z);
        Vector3 cameraPosition = new Vector3(Camera.main.transform.position.x , 0 , Camera.main.transform.position.z);

        //計算Y=0平面上的照相機與碰撞點距離
        float Distance = Vector3.Distance(collisionPosition , cameraPosition);
        //給訂粒子數量限制
        Vector2 maxParticle = new Vector2(50 , 200);
        //從螢幕空間返回距離相機最近處與最遠處的世界座標 以用於重映射粒子數量
        Vector3 worldPositionLeftBottom = Camera.main.ScreenToWorldPoint(new Vector3(960 , 540 ,Camera.main.transform.position.y));
        Vector3 worldPositionRightTop = Camera.main.ScreenToWorldPoint(new Vector3(1920 , 1080 ,Camera.main.transform.position.y));

        //計算最近處與最遠處世界座標與相機的距離
        float cameraNearDistance = Vector3.Distance(worldPositionLeftBottom , cameraPosition);
        float cameraFarDistance = Vector3.Distance(worldPositionRightTop , cameraPosition);


        //testbox.transform.position = new Vector3(worldPositionLeftBottom.x , 0 , worldPositionLeftBottom.z);

        //距離與粒子數量重映射
        //float particleCount = 0;
        float particleCount = Mathf.Clamp(map(Distance , cameraNearDistance , cameraFarDistance , maxParticle.x , maxParticle.y) , maxParticle.x , maxParticle.y);
        
        Debug.Log(particleCount);
        var em = transform.GetChild(0).GetComponent<ParticleSystem>().emission;
        em.SetBursts(new[]{new ParticleSystem.Burst(0f, particleCount)});
    }

    float map(float s, float a1, float a2, float b1, float b2)
{
    return b1 + (s-a1)*(b2-b1)/(a2-a1);
}
}
