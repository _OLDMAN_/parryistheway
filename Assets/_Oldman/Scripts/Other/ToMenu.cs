using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ToMenu : MonoBehaviour
{
    public float introSFXDelaySec;
    public float nextSceneDelaySec;

    private AudioSource source;

    private void Start()
    {
        source = GetComponent<AudioSource>();
        Invoke("PlayIntroSFX", introSFXDelaySec);
        Invoke("LoadScene", nextSceneDelaySec);
    }

    private void LoadScene()
    {
        SceneManager.LoadScene("Menu");
    }

    private void PlayIntroSFX()
    {
        source.Play();
    }
}