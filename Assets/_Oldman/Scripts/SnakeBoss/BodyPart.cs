using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPart : MonoBehaviour
{
    private Snake snake;
    private int key;
    private TransformStruct nextTransform;

    private float moveRate = 0.1f;

    public void Init(int key, Snake s, float moveRate)
    {
        this.key = key;
        snake = s;
        this.moveRate = moveRate;
    }

    public void UpdateNextTrans()
    {
        nextTransform = snake.NextTransform(key);
        key++;
    }

    private void Update()
    {
        MoveToNextKey();
    }

    private void MoveToNextKey()
    {
        //if ((transform.position - nextTransform.position).magnitude < arrivedDistance) return;
        transform.position = Vector3.Lerp(transform.position, nextTransform.position, moveRate);
        transform.rotation = Quaternion.Lerp(transform.rotation, nextTransform.rotation, moveRate);
        transform.localScale = Vector3.Lerp(transform.localScale, nextTransform.localScale, moveRate);
    }

    private void OnTriggerEnter(Collider other)
    {
    }
}