using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snake : MonoBehaviour
{
    [SerializeField] private GameObject PartPrefab;

    private List<BodyPart> parts = new List<BodyPart>();

    public int partNum;
    [SerializeField] private float followRate;

    private Dictionary<int, TransformStruct> traceDic = new Dictionary<int, TransformStruct>();

    private int key = 0;
    public float updateTimesPerSec;
    private float updateTime;
    private float timeElapsed = 0f;

    private void Start()
    {
        updateTime = 1 / updateTimesPerSec;
        InitialTraceAndBodyPart();
    }

    public void InitialTraceAndBodyPart()
    {
        for (int i = 0; i > -partNum; i--)
        {
            TransformStruct t = transform.ToStruct();
            t.position -= Vector3.down * i;
            traceDic.Add(i, transform.ToStruct());

            BodyPart part = Instantiate(PartPrefab, t.position, t.rotation).GetComponent<BodyPart>();
            part.Init(i, this, followRate);
            parts.Add(part);
        }
    }

    private void Update()
    {
        UpdateTrace();
    }

    private void UpdateTrace()
    {
        timeElapsed += Time.deltaTime;
        if (timeElapsed < updateTime)
        {
            return;
        }
        else
        {
            timeElapsed -= updateTime;
        }

        LeaveTrace();
        DeleteUsedTrace();
    }

    public void LeaveTrace()
    {
        key++;
        traceDic.Add(key, transform.ToStruct());
        UpdateParts();
    }

    private void UpdateParts()
    {
        foreach (BodyPart b in parts)
        {
            b.UpdateNextTrans();
        }
    }

    private void DeleteUsedTrace()
    {
        if (traceDic.Count < partNum || traceDic.Count < partNum * 3) return;

        Dictionary<int, TransformStruct> newDic = new Dictionary<int, TransformStruct>();
        for (int i = key; i > key - partNum; i--)
        {
            newDic.Add(i, traceDic[i]);
        }

        traceDic = newDic;
    }

    public TransformStruct NextTransform(int key)
    {
        if (key + 1 <= this.key)
        {
            return traceDic[key + 1];
        }
        else
        {
            Debug.Log("bodypart key overflow");
            return traceDic[key];
        }
    }

    private void OnDrawGizmos()
    {
        foreach (TransformStruct t in traceDic.Values)
        {
            Gizmos.DrawSphere(t.position, 0.1f);
            Gizmos.color = Color.red;
        }
    }
}