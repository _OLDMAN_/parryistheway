using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;

public class HealthEffect : MonoBehaviour
{
    public Image image;
    public float fadeSec;
    public Color defaultColor;
    public Color brightColor;

    private void ResetProperty()
    {
        image.color = defaultColor;
    }

    public void PlayEffect(Vector3 pos, Transform parent, float xScale)
    {
        transform.position = pos;
        ResetProperty();
        transform.localScale = new Vector3(xScale, transform.localScale.y, transform.localScale.z);
        transform.DOScaleY(1.5f, fadeSec).From(1);
        image.DOColor(brightColor, fadeSec * 0.3f).onComplete += () => image.DOColor(Color.clear, fadeSec * 0.7f).onComplete += () => ObjectPoolManager.Instance.Recycle(this);
    }
}