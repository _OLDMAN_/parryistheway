using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damagable : MonoBehaviour
{
    public Transform user;
    public LayerMask canDamageLayer;
    public bool debug;
    public float offectZ;
    public Vector3 halfExtents = Vector3.one;
    protected int damage;
    public Attack attack;

    public virtual void DealDamage()
    {
        bool isHitEnemy = false;
        Collider[] colliders = Physics.OverlapBox(user.position + (user.forward * offectZ), halfExtents / 2, user.rotation, canDamageLayer);

        foreach (Collider c in colliders)
        {
            IDestroyable destroyable = c.GetComponent<IDestroyable>();
            IHurtable hurtable = c.GetComponent<IHurtable>();
            IBreakable breakable = c.GetComponent<IBreakable>();

            destroyable?.Damage(1);
            if (hurtable != null)
            {
                if (attack.skillIndex == 3)
                {                    
                    hurtable.ReduceHurtCount(1);
                    breakable?.ReduceBreakCount(1);
                    hurtable.Damage(user, damage, true);
                }
                else
                {                    
                    hurtable.ReduceHurtCount(1);
                    hurtable.Damage(user, damage);
                }
                isHitEnemy = true;
            }
        }
        if (isHitEnemy == true && attack.skillIndex == 3)
            GamefeelManager.Instance.HeavyAttackFeel();
    }

    private void OnDrawGizmos()
    {
        if (debug == false) return;

        Gizmos.color = Color.green;
        Matrix4x4 rotationMatrix = Matrix4x4.TRS(user.transform.position, user.transform.rotation, user.transform.lossyScale);
        Gizmos.matrix = rotationMatrix;
        Gizmos.DrawWireCube(new Vector3(0, 0, offectZ), halfExtents);
    }
}