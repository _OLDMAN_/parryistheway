using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDestroyable
{
    public bool isDestroy { get; set; }
    public int destroyCounter { get; set; }
    public void Damage(int count = 1);
    public void Destroy();
}
