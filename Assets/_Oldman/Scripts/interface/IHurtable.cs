using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHurtable
{
    public int hurtCounter { get; set; }
    void Damage(Transform attacker, int damage, bool heavyDamage = false);
    void KnockBack(Vector3 backDir, float distance = 0f);
    void ReduceHurtCount(int hurtCount = 1);
}