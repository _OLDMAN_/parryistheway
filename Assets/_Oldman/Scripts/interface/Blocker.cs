using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface Blocker
{
    bool isBlocking { get; set; }
}
