using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBlockable
{
    bool canBlock { get; set; }
    void Block(GameObject attacker);
    Transform target { get; }
}