using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBreakable
{
    public int breakCounter { get; set; }
    public void ReduceBreakCount(int breakCount = 1);
    public void Break();
}
