﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum PlayerAni
{
    Idle,
    Move,
    Dash,
    Combo_01,
    Combo_02,
    Combo_03,
    HeavyAttack,
    DashAttack,
    Block,
    Hurt,
    Die,
    ShieldDash,
    ThrowShield,
    ThrowSpear,
    SweepAttack,
    DashSting,
    Kick
}

public class PlayerAnimation : MonoBehaviour
{
    private Animator animator;
    private CharacterController characterController;
    private string currentAniName = "Idle";
    private List<PlayerAni> NotRootMotionAniTypeList = new List<PlayerAni>() { PlayerAni.Move, PlayerAni.Dash, PlayerAni.DashAttack, PlayerAni.Block, PlayerAni.Idle };
    public bool isBlending { get; private set; }

    [Tooltip("percent to blend to next animation ,apply to all animation , 0~1")]
    public float blendTime = 0.1f;

    public List<AnimationHolder> animationholders = new List<AnimationHolder>();

    private void Start()
    {
        animator = GetComponent<Animator>();
        characterController = GetComponent<CharacterController>();
    }

    /// <param name="blendPlay">crossfade the animation.</param>
    /// <param name="skipWhenRepeat">don't play the animation again when current animation is the same one as desired animation.</param>
    public void PlayAnimation(PlayerAni type, bool skipWhenRepeat = true, bool isBlendPlay = true)
    {
        string targetAniName = type.ToString();
        if (currentAniName == targetAniName && skipWhenRepeat) return;

        currentAniName = targetAniName;

        if (isBlendPlay)
        {
            BlendPlay();
        }
        else
        {
            ForcePlay();
        }
    }
    private void ForcePlay()
    {
        animator.Play(currentAniName);
    }
    private void BlendPlay()
    {
        animator.CrossFade(currentAniName, blendTime);
    }

    public AnimationHolder GetAnimationHolder(string name)
    {
        for (int i = 0; i < animationholders.Count; i++)
        {
            if (animationholders[i].name == name)
            {
                return animationholders[i];
            }
        }

        Debug.Log("AnimationHolder：" + name + " Not Found");
        return null;
    }
    private void OnAnimatorMove()
    {
        foreach (PlayerAni notRootmotionAniType in NotRootMotionAniTypeList)
        {
            if (currentAniName == notRootmotionAniType.ToString())
            {
                return;
            }
        }

        var p = transform.position;
        Vector3 moveDir = animator.deltaPosition;
        characterController.Move(moveDir.GetZeroY());
    }
}

[Serializable]
public class AnimationHolder
{
    public string name;
    public AnimationClip clip;
    public float playSpeed = 1f;
    public float MovePercent = 1f;

    public float Length
    {
        get
        {
            if (clip == null) return -1;

            return clip.length / playSpeed;
        }
    }

    public float LengthNoScale
    {
        get
        {
            if (clip == null) return -1;

            return clip.length;
        }
    }
}