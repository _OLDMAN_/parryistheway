﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Image hpImage;
    public float hpImageLength;
    public GameObject effectPrefab;
    private HealthEffect effectCompo;

    private Transform lookTarget;
    private float currentHpPercent = 1;

    private void Start()
    {
        lookTarget = Camera.main.transform;
        effectCompo = effectPrefab.GetComponent<HealthEffect>();
    }

    private void Update()
    {
        transform.rotation = Quaternion.Euler(45, 0, 0);
    }

    public void ModifyBar(float percent)
    {
        float dis = currentHpPercent - percent;
        currentHpPercent = percent;
        hpImage.fillAmount = currentHpPercent;

        HealthEffect effect = ObjectPoolManager.Instance.Reuse(effectCompo);

        Vector3 pos = hpImage.transform.position + (hpImage.transform.right * (currentHpPercent + dis * 0.5f) * hpImageLength);

        if (effect == null)
        {
            effect = Instantiate(effectPrefab, pos, transform.rotation).GetComponent<HealthEffect>();
        }
        effect.PlayEffect(pos, transform, dis);
    }
}