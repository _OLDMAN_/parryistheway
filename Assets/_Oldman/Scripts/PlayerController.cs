using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.InputSystem;
using DefaultInputActions;

public class PlayerController : MonoBehaviourSingleton<PlayerController>
{
    //reference
    [HideInInspector] public Animator animator;
    [HideInInspector] public new PlayerAnimation animation;
    private CharacterController characterController;
    private PlayerActionManager playerActionManager;

    public Vector2 input;
    public Vector3 targetDir;
    public float rotateLerpSpeed;
    public float moveSpeed;
    public LayerMask obsLayer;

    public bool canCtrl { get; set; } = true;
    public bool inItemInteractRange { get; set; } = false;
    [HideInInspector] public bool isSkilling;

    public DefaultInputAction defaultInputAction;

    public bool lockY;

    public override void Awake()
    {
        defaultInputAction = new DefaultInputAction();
    }

    private void OnEnable()
    {
        defaultInputAction.Enable();
    }

    private void OnDisable()
    {
        defaultInputAction.Disable();
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
        animation = GetComponent<PlayerAnimation>();
        playerActionManager = GetComponent<PlayerActionManager>();
        characterController = GetComponent<CharacterController>();
    }

    private void Update()
    {
        if (canCtrl == false) return;

        input = defaultInputAction.Player.Move.ReadValue<Vector2>();
        //input = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        Vector3 cameraForward = Camera.main.transform.forward;
        cameraForward.y = 0;
        targetDir = (Camera.main.transform.right * input.x + cameraForward.normalized * input.y).normalized;

        if (defaultInputAction.Player.Block.triggered && TeachManager.Instance.unLockBlockAndBlockAttack)
        {
            playerActionManager.Block();
        }
        else if (defaultInputAction.Player.Attack.triggered && TeachManager.Instance.unLockAttackAndDashAttack)
        {
            playerActionManager.Attack();
        }
        else if (defaultInputAction.Player.Dash.triggered && playerActionManager.dash.isCD == false && playerActionManager.hurt.isHurting == false && isSkilling == false && inItemInteractRange == false && TeachManager.Instance.unLockDash)
        {
            playerActionManager.Dash();
        }

        if (!playerActionManager.isInAction && TeachManager.Instance.unLockMove)
        {
            RotateToDir();
            Move();
        }
    }

    private void LateUpdate()
    {
        if (lockY == false) return;
        Vector3 tempPos = transform.position;
        tempPos.y = 0;
        transform.position = tempPos;
    }

    public void RotateToDir(bool forceRotate = false)
    {
        if (forceRotate)
        {
            if (targetDir != Vector3.zero)
            {
                transform.rotation = Quaternion.LookRotation(targetDir.GetZeroY());
            }
            return;
        }

        if (targetDir != Vector3.zero)
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDir.GetZeroY()), rotateLerpSpeed);
    }

    public void RotateToDir(Transform target)
    {
        if (target == null) return;

        Vector3 dir = target.position - transform.position;
        transform.forward = new Vector3(dir.x, 0, dir.z);
    }

    public void Move()
    {
        float speed = Mathf.Clamp(input.magnitude, 0, 1);
        animator.SetFloat("RunSpeed", speed);
        if (input == Vector2.zero)
        {
            animation.PlayAnimation(PlayerAni.Idle);
        }
        else
        {
            characterController.SimpleMove(targetDir * moveSpeed);
            animation.PlayAnimation(PlayerAni.Move);
        }
    }

    public void AIMove()
    {
        float speed = Mathf.Clamp(input.magnitude, 0, 1);
        animator.SetFloat("RunSpeed", speed);
        if (input == Vector2.zero)
        {
            animation.PlayAnimation(PlayerAni.Idle);
        }
        else
        {
            characterController.Move(targetDir * moveSpeed * Time.deltaTime);
            animation.PlayAnimation(PlayerAni.Move);
        }
    }

    public void SetPos(Vector3 pos)
    {
        characterController.enabled = false;
        transform.position = pos;
        characterController.enabled = true;
    }

}