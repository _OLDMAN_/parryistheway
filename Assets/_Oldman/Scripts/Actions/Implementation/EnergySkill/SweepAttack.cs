using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SweepAttack : CharacterAction
{
    public SweepAttackData data;

    [HideInInspector] public int bounsDamage = 0;
    [HideInInspector] public int bounsRange = 0;

    public bool energyIsEnough
    { get { return PlayerEnergy.Instance.energy >= data.needEnergy; } }

    public override void StartAction()
    {
        Hurt.isInvincible = true;
        base.StartAction();
        coroutines.Add(StartCoroutine(DamageFrame()));
        playerController.isSkilling = true;
    }

    private IEnumerator DamageFrame()
    {
        yield return new WaitForSeconds(GetAnimationLength() * (18f / 42f));
        bool isHit = false;
        SoundManager.Instance.PlaySound("PlayerSweepAttack");
        GameObject g = ParticleManager.Instance.GetParticle("PlayerSweepAttack", transform.position, transform.rotation);
        g.transform.localScale = Vector3.one * (1 + bounsRange / 100f);
        Collider[] colliders = Physics.OverlapSphere(transform.position, CountBounsRange(data.radius), data.damageLayer);
        foreach (Collider collider in colliders)
        {
            Vector3 dir = collider.transform.position - transform.position;
            dir.y = 0;
            if (Vector3.Angle(transform.forward, dir) < data.angle / 2f)
            {
                IDestroyable destroyable = collider.GetComponent<IDestroyable>();
                IHurtable hurtable = collider.GetComponent<IHurtable>();
                IBreakable breakable = collider.GetComponent<IBreakable>();

                destroyable?.Damage(1);
                if (hurtable == null) continue;
                hurtable.ReduceHurtCount(1);
                breakable?.ReduceBreakCount(data.breakValue);
                hurtable.Damage(transform, CountBounsDamage(data.damage), true);

                isHit = true;
            }
        }
        if (PlayerAction.Instance.skillHit != null && isHit == true)
            PlayerAction.Instance.skillHit.Invoke();
    }

    private int CountBounsDamage(int damage, bool hitOnly = false)
    {
        if (hitOnly == false)
            return damage * Mathf.RoundToInt(1 + bounsDamage / 100f);
        else
            return damage * Mathf.RoundToInt(1 + (bounsDamage + PlayerAction.Instance.hitOneEnemyBouns) / 100f);
    }

    private float CountBounsRange(float range)
    {
        return range * (1 + bounsRange / 100f);
    }

    protected override void EndAction()
    {
        Hurt.isInvincible = false;
        playerController.isSkilling = false;
        base.EndAction();
    }
    private void OnDrawGizmos()
    {
        if (data == null) return;
        if (data.debug == false) return;

        Gizmos.color = Color.yellow;
        GizmosExtensions.DrawWireArc(transform.position, transform.forward, data.angle, data.radius);
    }
}