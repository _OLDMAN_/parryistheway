using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldIns : MonoBehaviour
{
    public LayerMask damageLayer;
    private PoolRecycle poolRecycle;

    private int hitDamage;
    private float speed;
    private Vector3 moveDir;

    private int arcDamage;
    private float arcAngle;
    private float arcRadius;

    public int breakValue;

    private void Start()
    {
        poolRecycle = GetComponent<PoolRecycle>();
    }

    private void Update()
    {
        transform.position += moveDir * Time.deltaTime * speed;
        //transform.Rotate(Vector3.right * 10, Space.Self);
    }

    public void SetValue(Vector3 moveDir, float speed, int hitDamage, int arcDamage, float arcAngle, float arcRadius, int breakValue)
    {
        this.moveDir = moveDir;
        this.hitDamage = hitDamage;
        this.speed = speed;
        this.arcDamage = arcDamage;
        this.arcAngle = arcAngle;
        this.arcRadius = arcRadius;
        this.breakValue = breakValue;
        transform.forward = moveDir;
    }

    private void OnTriggerEnter(Collider other)
    {
        IHurtable hurtable = other.GetComponent<IHurtable>();
        IBreakable breakable = other.GetComponent<IBreakable>();
        IDestroyable destroyable = other.GetComponent<IDestroyable>();

        if (other.CompareTag("Enemy"))
        {
            breakable?.ReduceBreakCount(breakValue);
            hurtable?.ReduceHurtCount(1);
            hurtable?.Damage(transform, hitDamage);
            if (PlayerAction.Instance.skillHit != null)
                PlayerAction.Instance.skillHit.Invoke();
            Break();
            DestroyBullet();
        }
        else if (other.CompareTag("DestructibleObs") || other.CompareTag("TowerTrap"))
        {
            destroyable?.Damage(hitDamage);
            Break();
            DestroyBullet();
        }
        else if (other.CompareTag("Wall"))
        {
            Break();
            DestroyBullet();
        }
    }

    private void Break()
    {
        SoundManager.Instance.PlaySound("PlayerThrowShieldHit");
        ParticleManager.Instance.PlayParticle("PlayerThrowShieldHit", transform.position, transform.rotation);
        Collider[] colliders = Physics.OverlapSphere(transform.position, arcRadius, damageLayer);
        foreach (Collider collider in colliders)
        {
            IHurtable hurtable = collider.GetComponent<IHurtable>();
            IBreakable breakable = collider.GetComponent<IBreakable>();
            IDestroyable destroyable = collider.GetComponent<IDestroyable>();
            destroyable?.Damage();
            if (hurtable == null) return;

            Vector3 dir = collider.transform.position - transform.position;
            dir.y = 0;
            float angle = Vector3.Angle(moveDir, dir);
            if (angle < arcAngle / 2f)
            {
                hurtable.ReduceHurtCount(1);
                breakable?.ReduceBreakCount(breakValue);
                hurtable.Damage(transform, arcDamage, true);
            }
        }
    }

    private void DestroyBullet()
    {
        poolRecycle.Recycle();
    }
}