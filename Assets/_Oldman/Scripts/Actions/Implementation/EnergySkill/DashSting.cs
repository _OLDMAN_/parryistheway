using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashSting : CharacterAction
{
    public DashStingData data;

    public Transform spear;

    [HideInInspector] public int bounsDamage = 0;
    [HideInInspector] public int bounsRange = 0;

    public override void StartAction()
    {
        base.StartAction();
        ParticleManager.Instance.PlayParticle("PlayerDashStingStart", transform.position, transform.rotation);
        ParticleManager.Instance.PlayParticle("PlayerDashStingHold", spear.position, spear.rotation, spear);
        coroutines.Add(StartCoroutine(DamageFrame()));
        playerController.isSkilling = true;
        Hurt.isInvincible = true;
    }

    private IEnumerator DamageFrame()
    {
        yield return new WaitForSeconds(GetAnimationLength() * (15f / 35f));
        bool isHit = false;
        SoundManager.Instance.PlaySound("PlayerDashSting");
        GameObject g = ParticleManager.Instance.GetParticle("PlayerDashSting", transform.position, transform.rotation);
        g.transform.localScale = Vector3.one * (1 + bounsRange / 100f);
        Collider[] colliders = Physics.OverlapBox(transform.position + transform.rotation * data.dashOffset, CountBounsRange(data.dashSize), transform.rotation, data.damageLayer);
        foreach (Collider collider in colliders)
        {
            IDestroyable destroyable = collider.GetComponent<IDestroyable>();
            IHurtable hurtable = collider.GetComponent<IHurtable>();
            IBreakable breakable = collider.GetComponent<IBreakable>();
            destroyable?.Damage(1);
            hurtable?.ReduceHurtCount(1);
            breakable?.ReduceBreakCount(data.breakValue);
            hurtable?.KnockBack(transform.forward, 2);
            hurtable?.Damage(transform, CountBounsDamage(data.damage), true);

            if (hurtable != null)
                isHit = true;
        }
        if (PlayerAction.Instance.skillHit != null && isHit == true)
            PlayerAction.Instance.skillHit.Invoke();
    }

    protected override void EndAction()
    {
        Hurt.isInvincible = false;
        playerController.isSkilling = false;
        base.EndAction();
    }

    private int CountBounsDamage(int damage, bool hitOnly = false)
    {
        if (hitOnly == false)
            return damage * Mathf.RoundToInt(1 + bounsDamage / 100f);
        else
            return damage * Mathf.RoundToInt(1 + (bounsDamage + PlayerAction.Instance.hitOneEnemyBouns) / 100f);
    }

    private Vector3 CountBounsRange(Vector3 size)
    {
        return size * (1 + bounsRange / 100f);
    }

    private void OnDrawGizmos()
    {
        if (data == null) return;
        if (data.debug == false) return;

        Gizmos.color = Color.yellow;

        Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        Gizmos.matrix = rotationMatrix;
        Gizmos.DrawWireCube(data.dashOffset, data.dashSize);
    }
}