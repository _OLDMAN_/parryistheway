using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowSpear : CharacterAction
{
    public ThrowSpearData data;
    public Transform handPos;

    [HideInInspector] public int bounsDamage = 0;
    [HideInInspector] public int bounsRange = 0;

    public override void StartAction()
    {
        Hurt.isInvincible = true;
        base.StartAction();
        coroutines.Add(StartCoroutine(ThrowFrame()));
        playerController.isSkilling = true;
    }

    private IEnumerator ThrowFrame()
    {
        yield return new WaitForSeconds(GetAnimationLength() * (29f / 59f));
        SoundManager.Instance.PlaySound("PlayerThrowSpear");
        GameObject obj = ParticleManager.Instance.GetParticle("PlayerThrowSpear", handPos.position, transform.rotation);
        obj.transform.localScale = CountBounsSize();
        obj.GetComponent<SpearIns>().SetValue(transform.forward, data.throwSpeed, CountBounsDamage(data.throwDamage), data.breakValue);
    }

    private int CountBounsDamage(int damage, bool hitOnly = false)
    {
        if (hitOnly == false)
            return damage * Mathf.RoundToInt(1 + bounsDamage / 100f);
        else
            return damage * Mathf.RoundToInt(1 + (bounsDamage + PlayerAction.Instance.hitOneEnemyBouns) / 100f);
    }

    private Vector3 CountBounsSize()
    {
        return Vector3.one * (1 + bounsRange / 100f);
    }

    protected override void EndAction()
    {
        playerController.isSkilling = false;
        Hurt.isInvincible = false;
        base.EndAction();
    }
}