using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpearIns : MonoBehaviour
{
    private PoolRecycle poolRecycle;

    private int hitDamage;
    private float speed;
    private Vector3 moveDir;
    private bool isDead;

    public int breakValue;

    private void OnEnable()
    {
        isDead = false;
    }

    private void Start()
    {
        poolRecycle = GetComponent<PoolRecycle>();
    }

    private void Update()
    {
        if (isDead == true) return;
        transform.position += moveDir * Time.deltaTime * speed;
        //transform.Rotate(Vector3.forward * 10, Space.Self);
    }

    public void SetValue(Vector3 moveDir, float speed, int hitDamage, int breakValue)
    {
        this.moveDir = moveDir;
        this.hitDamage = hitDamage;
        this.speed = speed;
        this.breakValue = breakValue;
        transform.forward = moveDir;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (isDead == true) return;

        IHurtable hurtable = other.GetComponent<IHurtable>();
        IBreakable breakable = other.GetComponent<IBreakable>();
        IDestroyable destroyable = other.GetComponent<IDestroyable>();

        hurtable?.ReduceHurtCount(1);
        breakable?.ReduceBreakCount(breakValue);
        hurtable?.Damage(transform, hitDamage);
        destroyable?.Damage();
        //if (hurtable != null)
        //{
        //    if (other.CompareTag("Enemy"))
        //    {                
        //        hurtable.ReduceHurtCount(1);
        //        breakable?.ReduceBreakCount(breakValue);
        //        hurtable.Damage(transform, hitDamage);

        //        if (PlayerAction.Instance.skillHit != null)
        //            PlayerAction.Instance.skillHit.Invoke();
        //    }
        //    else if (other.CompareTag("DestructibleObs") || other.CompareTag("TowerTrap"))
        //    {
        //        hurtable.Damage(transform, hitDamage);
        //    }
        //}

        if (other.CompareTag("Wall"))
        {
            DestroyBullet();
        }
    }

    private void DestroyBullet()
    {
        isDead = true;
        SoundManager.Instance.PlaySound("PlayerThrowSpearHit");
        ParticleManager.Instance.PlayParticle("BulletHit", transform.position + transform.forward * 3, Quaternion.identity);
        poolRecycle.Recycle(false);
    }
}