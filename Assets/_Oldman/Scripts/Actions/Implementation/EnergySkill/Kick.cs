using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Kick : CharacterAction
{
    public KickData data;
    [HideInInspector] public int bounsDamage = 0;
    [HideInInspector] public int bounsRange = 0;

    public override void StartAction()
    {
        Hurt.isInvincible = true;
        base.StartAction();
        coroutines.Add(StartCoroutine(DamageFrame()));
        playerController.isSkilling = true;
    }

    private IEnumerator DamageFrame()
    {
        yield return new WaitForSeconds(GetAnimationLength() * (19f / 35f));
        bool isHit = false;
        ParticleManager.Instance.PlayParticle("PlayerKick", transform.position, transform.rotation);
        SoundManager.Instance.PlaySound("PlayerKick");
        Collider[] colliders = Physics.OverlapBox(transform.position + transform.rotation * data.kickOffset, CountBounsRange(data.kickSize), transform.rotation,data.damageLayer);
        foreach (Collider collider in colliders)
        {
            IHurtable hurtable = collider.GetComponent<IHurtable>();
            IBreakable breakable = collider.GetComponent<IBreakable>();
            IDestroyable destroyable = collider.GetComponent<IDestroyable>();

            destroyable?.Damage(1);
            if (hurtable == null) continue;
            ParticleManager.Instance.PlayParticle("PlayerKickEnemy", collider.transform.position, Quaternion.LookRotation(collider.transform.position - transform.position));
            hurtable.ReduceHurtCount(1);
            breakable?.ReduceBreakCount(data.breakValue);
            hurtable.KnockBack(transform.forward, data.knockBackdDistance);
            hurtable.Damage(transform, CountBounsDamage(data.damage), true);
            isHit = true;
        }
        if (PlayerAction.Instance.skillHit != null && isHit == true)
            PlayerAction.Instance.skillHit.Invoke();
    }

    private int CountBounsDamage(int damage, bool hitOnly = false)
    {
        if (hitOnly == false)
            return damage * Mathf.RoundToInt(1 + bounsDamage / 100f);
        else
            return damage * Mathf.RoundToInt(1 + (bounsDamage + PlayerAction.Instance.hitOneEnemyBouns) / 100f);
    }

    private Vector3 CountBounsRange(Vector3 size)
    {
        return size * (1 + bounsRange / 100f);
    }

    protected override void EndAction()
    {
        base.EndAction();
        Hurt.isInvincible = false;
        playerController.isSkilling = false;
    }
    private void OnDrawGizmos()
    {
        if (data == null) return;
        if (data.debug == false) return;

        Gizmos.color = Color.yellow;
        Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        Gizmos.matrix = rotationMatrix;
        Gizmos.DrawWireCube(data.kickOffset, data.kickSize);
    }
}