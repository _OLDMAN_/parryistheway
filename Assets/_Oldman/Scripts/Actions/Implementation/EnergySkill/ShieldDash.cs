using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldDash : CharacterAction
{
    public ShieldDashData data;

    [HideInInspector] public int bounsDamage = 0;
    [HideInInspector] public int bounsRange = 0;

    private List<GameObject> hitList = new List<GameObject>();
    private bool dashEnd;
    private GameObject dashP;

    public override void StartAction()
    {
        base.StartAction();
        SoundManager.Instance.PlaySound("PlayerShieldDash");
        dashP = ParticleManager.Instance.GetParticle("PlayerShieldDash", transform.position, transform.rotation, transform);
        dashP.transform.localScale = Vector3.one * (1 + bounsRange / 100f);
        coroutines.Add(StartCoroutine(ArcAttackFrame()));
        playerController.isSkilling = true;
        Hurt.isInvincible = true;
        dashEnd = false;
    }

    public override void UpdateAction()
    {
        if (dashEnd == true) return;
        bool isHit = false;
        Collider[] colliders = Physics.OverlapBox(transform.position + transform.rotation * data.dashOffset, CountBounsSize(data.dashSize), transform.rotation, data.damageLayer);
        foreach (Collider collider in colliders)
        {
            if (hitList.Contains(collider.gameObject) == true) continue;

            IDestroyable destroyable = collider.GetComponent<IDestroyable>();
            IHurtable hurtable = collider.GetComponent<IHurtable>();
            IBreakable breakable = collider.GetComponent<IBreakable>();

            destroyable?.Damage(1);
            if (hurtable != null)
            {
                StartCoroutine(StartHitCD(collider));
                breakable?.ReduceBreakCount(data.breakValue);
                hurtable.KnockBack(transform.forward, 2);
                hurtable.Damage(transform, CountBounsDamage(data.dashDamage), true);
                isHit = true;
            }
        }
        if (PlayerAction.Instance.skillHit != null && isHit == true)
            PlayerAction.Instance.skillHit.Invoke();
    }

    private IEnumerator ArcAttackFrame()
    {
        yield return new WaitForSeconds(GetAnimationLength() * (70f / 109f));
        bool isHit = false;
        dashEnd = true;
        dashP.GetComponent<PoolRecycle>().Recycle();
        GameObject g = ParticleManager.Instance.GetParticle("PlayerShieldDashArc", transform.position, transform.rotation);
        SoundManager.Instance.PlaySound("PlayerShieldDashArc");
        g.transform.localScale = Vector3.one * (1 + bounsRange / 100f);
        GamefeelManager.Instance.ShakeCamera(2, 0.2f);
        Collider[] colliders = Physics.OverlapSphere(transform.position, CountBounsRange(data.radius), data.damageLayer);
        foreach (Collider collider in colliders)
        {
            IHurtable hurtable = collider.GetComponent<IHurtable>();
            IBreakable breakable = collider.GetComponent<IBreakable>();
            if (hurtable != null)
            {
                hurtable.ReduceHurtCount(1);
                hurtable.Damage(transform, CountBounsDamage(data.arcDamage), true);
                hurtable.KnockBack(transform.forward, data.arcKnockBack);
                breakable?.ReduceBreakCount(data.breakValue);
                isHit = true;
            }
        }
        if (PlayerAction.Instance.skillHit != null && isHit == true)
            PlayerAction.Instance.skillHit.Invoke();
    }

    private IEnumerator StartHitCD(Collider collider)
    {
        hitList.Add(collider.gameObject);
        yield return new WaitForSeconds(data.dashDamageCD);
        hitList.Remove(collider.gameObject);
    }

    protected override void EndAction()
    {
        base.EndAction();
        Hurt.isInvincible = false;
        playerController.isSkilling = false;
    }

    private int CountBounsDamage(int damage, bool hitOnly = false)
    {
        if (hitOnly == false)
            return damage * Mathf.RoundToInt(1 + bounsDamage / 100f);
        else
            return damage * Mathf.RoundToInt(1 + (bounsDamage + PlayerAction.Instance.hitOneEnemyBouns) / 100f);
    }

    private float CountBounsRange(float range)
    {
        return range * (1 + bounsRange / 100f);
    }

    private Vector3 CountBounsSize(Vector3 size)
    {
        return size * (1 + bounsRange / 100f);
    }

    private void OnDrawGizmos()
    {
        if (data == null) return;
        if (data.debug == false) return;

        Gizmos.color = Color.yellow;
        GizmosExtensions.DrawWireArc(transform.position, transform.forward, data.arcAngle, data.radius);

        Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        Gizmos.matrix = rotationMatrix;
        Gizmos.DrawWireCube(data.dashOffset, data.dashSize);
    }
}