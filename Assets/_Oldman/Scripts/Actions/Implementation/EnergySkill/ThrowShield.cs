using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThrowShield : CharacterAction
{
    public ThrowShieldData data;
    public Transform handPos;

    [HideInInspector] public int bounsDamage = 0;
    [HideInInspector] public int bounsRange = 0;

    public override void StartAction()
    {
        base.StartAction();
        coroutines.Add(StartCoroutine(ThrowShieldFrame()));
        playerController.isSkilling = true;
        Hurt.isInvincible = true;
    }

    private IEnumerator ThrowShieldFrame()
    {
        yield return new WaitForSeconds(GetAnimationLength() * (29f / 60f));
        SoundManager.Instance.PlaySound("PlayerThrowShield");
        GameObject obj = ParticleManager.Instance.GetParticle("PlayerThrowShield", handPos.position, transform.rotation);
        obj.transform.localScale = CountBounsSize();
        obj.GetComponent<ShieldIns>().SetValue(transform.forward, data.throwSpeed, CountBounsDamage(data.throwDamage, true), CountBounsDamage(data.arcDamage), data.arcAngle, data.arcRadius, data.breakValue);
    }

    protected override void EndAction()
    {
        Hurt.isInvincible = false;
        playerController.isSkilling = false;
        base.EndAction();
    }

    private int CountBounsDamage(int damage, bool hitOnly = false)
    {
        if (hitOnly == false)
            return damage * Mathf.RoundToInt(1 + bounsDamage / 100f);
        else
            return damage * Mathf.RoundToInt(1 + (bounsDamage + PlayerAction.Instance.hitOneEnemyBouns) / 100f);
    }

    private Vector3 CountBounsSize()
    {
        return Vector3.one * (1 + bounsRange / 100f);
    }

    private void OnDrawGizmos()
    {
        if (data == null) return;
        if (data.debug == false) return;

        Gizmos.color = Color.yellow;
        GizmosExtensions.DrawWireArc(transform.position, transform.forward, data.arcAngle, data.arcRadius);
    }
}