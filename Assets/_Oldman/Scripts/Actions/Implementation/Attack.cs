using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.InputSystem;

public class Attack : CharacterAction
{
    public List<AttackSkill> SkillList = new List<AttackSkill>();
    public int skillIndex;
    public int SkillLoopMax;
    public float ResetLoopSec;
    public float DashAttackResetSec;
    public Spear spear;

    private LayerMask groundMask;

    public float autoAimDistance;
    public LayerMask trapLayer;
    public LayerMask enemyLayer;

    private void Awake()
    {
        groundMask = LayerMask.GetMask("Ground");
    }

    public override void InitiateAction()
    {
        DecideSkill();
        base.InitiateAction();
    }

    public override void StartAction()
    {
        base.StartAction();
        Aim();
        coroutines.Add(StartCoroutine(DoDealDamage()));
    }

    public void Aim(float distance = 0)
    {
        RotateToEnemy(distance);

        //if (Gamepad.current != null)
        //{
        //    RotateToEnemy();
        //}
        //else
        //{
        //    RotateToMouse();
        //}
    }

    public void RotateToMouse()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out RaycastHit hit, 100, groundMask);
        Vector3 dir = hit.point - transform.position;
        dir.y = 0;
        dir.Normalize();
        Physics.Raycast(transform.position + Vector3.up + Vector3.Cross(Vector3.up, dir) * spear.halfExtents.x / 2, dir, out RaycastHit autoRightHit, spear.offectZ + spear.halfExtents.z / 2f, enemyLayer);
        Physics.Raycast(transform.position + Vector3.up + Vector3.Cross(dir, Vector3.up) * spear.halfExtents.x / 2, dir, out RaycastHit autoLeftHit, spear.offectZ + spear.halfExtents.z / 2f, enemyLayer);
        if (autoRightHit.collider != null && autoLeftHit.collider != null)
        {
            Vector3 autoDir = (autoRightHit.distance > autoLeftHit.distance ? autoLeftHit.collider.transform.position : autoRightHit.collider.transform.position) - transform.position;
            autoDir.y = 0;
            transform.forward = autoDir;
        }
        else if (autoRightHit.collider != null)
        {
            Vector3 autoDir = autoRightHit.collider.transform.position - transform.position;
            autoDir.y = 0;
            transform.forward = autoDir;
        }
        else if (autoLeftHit.collider != null)
        {
            Vector3 autoDir = autoLeftHit.collider.transform.position - transform.position;
            autoDir.y = 0;
            transform.forward = autoDir;
        }
        else if (hit.collider != null)
            transform.forward = dir.normalized;
    }

    public void RotateToEnemy(float distance = 0)
    {
        if (distance == 0)
            distance = autoAimDistance;
        Collider[] enemys = Physics.OverlapSphere(transform.position, distance, enemyLayer);
        if (enemys.Length == 0)
        {
            RotateToTrap();
            return;
        }
        Collider nearestEnemy = null;
        foreach (Collider c in enemys)
        {
            if (nearestEnemy != null)
            {
                nearestEnemy = Vector3.Distance(c.transform.position, transform.position) < Vector3.Distance(nearestEnemy.transform.position, transform.position) ? c : nearestEnemy;
            }
            else
            {
                nearestEnemy = c;
            }
        }
        Vector3 autoDir = nearestEnemy.transform.position - transform.position;
        autoDir.y = 0;
        transform.forward = autoDir;
    }

    private void RotateToTrap()
    {
        Collider[] traps = Physics.OverlapSphere(transform.position, autoAimDistance, trapLayer);
        if (traps.Length == 0) return;

        Collider nearestTrap = null;
        foreach (Collider c in traps)
        {
            if (nearestTrap != null)
                nearestTrap = Vector3.Distance(c.transform.position, transform.position) < Vector3.Distance(nearestTrap.transform.position, transform.position) ? c : nearestTrap;
            else
                nearestTrap = c;
        }
        Vector3 autoDir = nearestTrap.transform.position - transform.position;
        autoDir.y = 0;
        transform.forward = autoDir;
    }

    public IEnumerator DoDealDamage()
    {
        PlayAttackSound();
        yield return new WaitForSeconds(GetAnimationLength() * SkillList[skillIndex].AttackTime);
        PlaySkillParticle();
        spear.DealDamage();
    }

    private void DecideSkill()
    {
        CharacterAction lastAction = playerActionManager.lastAction;
        if (lastAction != null && lastAction.GetType().ToString() == "Dash" && Time.time - lastAction.startTime < DashAttackResetSec)
        {
            if (PlayerGem.Instance.dashAttack.skill != null)
            {
                Aim(99);
                PlayerGem.Instance.dashAttack.skill.Cast();
            }
            else
                skillIndex = 4;
        }
        else
        {
            if (Time.time - startTime > ResetLoopSec)
            {
                skillIndex = 0;
            }
            else
            {
                if (skillIndex == 4)
                    skillIndex = 0;
                else
                    skillIndex++;
                skillIndex = (int)Mathf.Repeat(skillIndex, SkillLoopMax);
            }
        }
        aniData = SkillList[skillIndex].aniData;
    }

    public void PlaySkillParticle()
    {
        if (skillIndex == 4)
            ParticleManager.Instance.PlayParticle("PlayerDashAttack", transform.position + Vector3.up, transform.rotation);
        else if (skillIndex == 3)
            ParticleManager.Instance.PlayParticle("PlayerHeavyAttack", transform.position + transform.forward * 4 + Vector3.up, transform.rotation);
        else
            ParticleManager.Instance.PlayParticle("PlayerAttack", transform.position + transform.forward * 4 + Vector3.up, transform.rotation);
    }

    public void PlayAttackSound()
    {
        if (skillIndex == 3)
            SoundManager.Instance.PlaySound("PlayerHeavyAttack");
        else if (skillIndex == 4)
            SoundManager.Instance.PlaySound("PlayerDashAttack");
        else
            SoundManager.Instance.PlaySound("PlayerAttack");
    }

    protected override void EndAction()
    {
        base.EndAction();
    }
}

[System.Serializable]
public class AttackSkill
{
    public int damage;
    public float AttackTime;
    public ActionAnimationData aniData;
}