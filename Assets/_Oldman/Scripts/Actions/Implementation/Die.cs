using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Die : CharacterAction
{
    public bool isDead;

    public override void StartAction()
    {
        base.StartAction();
        isDead = true;
        Dead();
    }

    public void Dead()
    {
        PlayerController.Instance.canCtrl = false;
        RoomManager.Instance.RestartCurrentRoom();
    }
}
