using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Block : CharacterAction
{
    public bool debug;
    public float radius;

    public float aniCanBlockPercentage = 0.25f;

    public LayerMask blockableLayer;
    private LayerMask groundMask;

    private BlockAttack blockAttack;
    private PlayerEnergy playerEnergy;
    private bool isBlocked;

    public float autoBlockDistance;

    private float canBlockTime {
        get {
            return GetAnimationLength() * aniCanBlockPercentage;
        }
    }

    [HideInInspector] public bool canBlock;

    private void Awake()
    {
        groundMask = LayerMask.GetMask("Ground");
    }
    public override void Start()
    {
        base.Start();
        blockAttack = GetComponent<BlockAttack>();
        playerEnergy = GetComponent<PlayerEnergy>();
    }

    public override void StartAction()
    {
        Aim();
        isBlocked = false;
        canBlock = true;
        coroutines.Add(StartCoroutine(CanBlockTimeEnd()));
        base.StartAction();
        ParticleManager.Instance.PlayParticle("PlayerWieldShield", transform.position, transform.rotation, transform);
        SoundManager.Instance.PlaySound("PlayerUseParry");

    }

    public void Aim()
    {
        if (Gamepad.current != null)
        {
            RotateToEnemy();
        }
        else
        {
            RotateToMouse();
        }
    }

    private void RotateToMouse()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        Physics.Raycast(ray, out RaycastHit hit, 1000, groundMask);
        Vector3 dir = hit.point - transform.position;
        dir.y = 0;
        transform.forward = dir;
    }

    public void RotateToEnemy()
    {
        Collider[] enemys = Physics.OverlapSphere(transform.position, autoBlockDistance, blockableLayer);
        if (enemys.Length == 0) return;

        Collider nearestEnemy = null;
        foreach (Collider c in enemys)
        {
            if (nearestEnemy != null)
            {
                nearestEnemy = Vector3.Distance(c.transform.position, transform.position) < Vector3.Distance(nearestEnemy.transform.position, transform.position) ? c : nearestEnemy;
            }
            else
            {
                nearestEnemy = c;
            }
        }
        Vector3 autoDir = nearestEnemy.transform.position - transform.position;
        autoDir.y = 0;
        transform.forward = autoDir;
    }

    public override void PlayAnimation()
    {
        playerAnimation.PlayAnimation(aniData.actionAnimation, false, true);
    }

    private IEnumerator CanBlockTimeEnd()
    {
        yield return new WaitForSeconds(canBlockTime);
        canBlock = false;
    }

    public override void UpdateAction()
    {
        if (isBlocked == true && PlayerController.Instance.defaultInputAction.Player.Block.triggered)
        {
            isBlocked = false;
            ForceEnd();
            playerActionManager.Block();
        }

        if (canBlock == false) return;

        CheckCanBlock();
    }

    public void CheckCanBlock()
    {
        bool _isBlock = false;
        Collider[] colliders = Physics.OverlapCapsule(transform.position, transform.position + Vector3.up, radius, blockableLayer);

        foreach (Collider collider in colliders)
        {
            IBlockable block = collider.GetComponent<IBlockable>();
            if (block == null) continue;
            else if (block.canBlock == false) continue;
            block.Block(gameObject);
            //PlayBlockParticle(collider.transform.position, block.target.position);
            PlayBlockSound();
            AddEnergy();
            isBlocked = true;
            _isBlock = true;
        }
        if (_isBlock == true)
        {
            GamefeelManager.Instance.PlayerBlockFeel();
            blockAttack.BlockSuccess();
            if (QuestManager.Instance.currentQuest != null)
                if (QuestManager.Instance.currentQuest.GetType() == typeof(Quest_BlockOnce))
                    QuestManager.Instance.currentQuest.QuestComplete();
            if (PlayerAction.Instance.succesBlock != null)
                PlayerAction.Instance.succesBlock.Invoke();
        }
    }

    public void OtherBlockSuccess()
    {
        isBlocked = true;
        GamefeelManager.Instance.PlayerBlockFeel();
        AddEnergy();
        blockAttack.BlockSuccess();
    }

    public void PlayBlockParticle(Vector3 startPos, Vector3 targetPos)
    {
        ParticleManager.Instance.PlayParticle("PlayerSuckEnergy", transform.position, transform.rotation, transform);
        GameObject blockP = ParticleManager.Instance.GetParticle("PlayerBlock", startPos, Quaternion.LookRotation(targetPos - startPos));
        ParticleManager.Instance.PlayParticle("EnemyLaserHit", targetPos, Quaternion.identity);
        SoundManager.Instance.PlaySound("ParryLazer");
        GamefeelManager.Instance.ShakeCamera(3, 0.2f);
        float distance = Vector3.Distance(targetPos, startPos) / 10f;
        blockP.transform.SetScaleZ(distance);
    }

    private void PlayBlockSound()
    {
        SoundManager.Instance.PlaySound("PlayerParry");
    }

    private void AddEnergy()
    {
        if (TeachManager.Instance.unLockBlockBurst == false) return;
        playerEnergy.energy++;
    }

    protected override void EndAction()
    {
        canBlock = false;
        base.EndAction();
    }

    private void OnDrawGizmos()
    {
        if (debug == false) return;
        Gizmos.color = Color.blue;
        GizmosExtensions.DrawCircle(transform.position, radius);
        //GizmosExtensions.DrawWireArc(transform.position, transform.forward, angle, radius);
    }
}