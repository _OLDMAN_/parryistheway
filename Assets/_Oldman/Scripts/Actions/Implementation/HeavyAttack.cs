using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeavyAttack : CharacterAction
{
    private Attack attack;
    private void Awake()
    {
        attack = GetComponent<Attack>();
    }
    public override void StartAction()
    {
        base.StartAction();
        attack.skillIndex = 3;
        StartCoroutine(attack.DoDealDamage());
        Hurt.isInvincible = true;
    }

    protected override void EndAction()
    {
        Hurt.isInvincible = false;
        base.EndAction();
    }
}
