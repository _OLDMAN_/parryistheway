using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Dash : CharacterAction
{
    public LayerMask obstacleLayer;
    public bool debug;
    public float dashDistance = 1f;
    public float needTime = 0.5f;
    private float dashDistanceCount;
    private float dashSpeed;
    private Vector3 dashDir;
    public bool isCD { get; set; }
    public int maxUse = 2;
    private int useCount = 2;
    public float CD;
    private float cdTimer;
    private bool useDashAttack = false;

    public float invincibleTime = 0.2f;
    private float invincibleTimer;

    public Coroutine dashCoroutine;
    private void Update()
    {
        if (useCount < maxUse)
        {
            cdTimer -= Time.deltaTime;
            if (cdTimer <= 0)
            {
                useCount = maxUse;
                isCD = false;
            }
        }
    }

    public void StopDash()
    {
        if(dashCoroutine != null)
        StopCoroutine(dashCoroutine);
    }
    public override void StartAction()
    {
        base.StartAction();
        Hurt.isInvincible = true;
        invincibleTimer = invincibleTime;
        CountDashDistance();
        dashCoroutine = StartCoroutine(DoStartDash());
        SoundManager.Instance.PlaySound("PlayerDash");
        ParticleManager.Instance.PlayParticle("PlayerDash", transform.position, Quaternion.LookRotation(dashDir));
    }

    private void CountDashDistance()
    {
        if (playerController.targetDir != Vector3.zero)
            dashDir = playerController.targetDir.normalized;
        else
            dashDir = transform.forward;

        Vector3 dashPoint = transform.position + dashDir * dashDistance;
        Physics.CapsuleCast(transform.position, transform.position + Vector3.up, 0.55f, dashDir, out RaycastHit hitWall, 100, LayerMask.GetMask("Wall"));
        Collider[] dashHits = Physics.OverlapCapsule(dashPoint, dashPoint + Vector3.up, 0.55f, obstacleLayer);

        RaycastHit forwardHit = new RaycastHit(), backHit = new RaycastHit();
        List<RaycastHit> forwardHits = new List<RaycastHit>();
        List<RaycastHit> backHits = new List<RaycastHit>();
        forwardHits.AddRange(Physics.CapsuleCastAll(transform.position - dashDir, transform.position - dashDir + Vector3.up, 0.55f, dashDir, 1000, obstacleLayer));
        backHits.AddRange(Physics.CapsuleCastAll(dashPoint + dashDir * 300, dashPoint + dashDir * 300 + Vector3.up, 0.55f, -dashDir, 500, obstacleLayer));
        Collider nearlyB = NearlyCollider(backHits);
        Collider nearlyF = NearlyCollider(forwardHits);
        for (int i = 0; i < backHits.Count; i++)
            if (backHits[i].collider == nearlyB)
                backHit = backHits[i];
        for (int i = 0; i < forwardHits.Count; i++)
            if (forwardHits[i].collider == nearlyF)
                forwardHit = forwardHits[i];

        //�p��Ĥ��L�h��ĹL�h���I�M�Z��
        Vector3 forwardPos = forwardHit.point - dashDir * 0.56f;
        Vector3 backPos = backHit.point + dashDir * 0.56f;
        float forwardDistance = Vector3.Distance(transform.position, new Vector3(forwardPos.x, 0, forwardPos.z));
        float backDistacne = Vector3.Distance(transform.position, new Vector3(backPos.x, 0, backPos.z));

        if (dashHits.Length == 0)
            dashDistanceCount = dashDistance;
        else if (IsOverHalf(dashPoint, forwardHit.point, backHit.point))
        {
            Collider[] hits = Physics.OverlapCapsule(backPos, backPos + Vector3.up, 0.55f, obstacleLayer);
            if (hits.Length == 0)
                dashDistanceCount = backDistacne;
            else
            {
                Physics.CapsuleCast(transform.position, transform.position + Vector3.up, 0.55f, dashDir, out RaycastHit endHit, dashDistance, obstacleLayer);
                dashDistanceCount = endHit.distance;
            }
        }
        else
        {
            Collider[] hits = Physics.OverlapCapsule(forwardPos, forwardPos + Vector3.up, 0.55f, obstacleLayer);
            if (hits.Length == 0)
                dashDistanceCount = forwardDistance;
            else
            {
                Physics.CapsuleCast(transform.position, transform.position + Vector3.up, 0.55f, dashDir, out RaycastHit endHit, dashDistance, obstacleLayer);
                dashDistanceCount = endHit.distance;
            }
        }

        if (hitWall.collider != null && dashDistanceCount > hitWall.distance)
            dashDistanceCount = hitWall.distance - 0.1f;
    }

    private Collider NearlyCollider(List<RaycastHit> hits)
    {
        float maxDistance = float.MaxValue;
        Collider nearlyCollider = null;
        foreach (RaycastHit hit in hits)
        {
            if (hit.distance < maxDistance)
            {
                nearlyCollider = hit.collider;
                maxDistance = hit.distance;
            }
        }
        return nearlyCollider;
    }

    private bool IsOverHalf(Vector3 checkPos, Vector3 startPos, Vector3 endPos)
    {
        checkPos.y = 0;
        startPos.y = 0;
        endPos.y = 0;
        float forwardDistance = Vector3.Distance(checkPos, startPos);
        float backDistance = Vector3.Distance(checkPos, endPos);

        if (forwardDistance > backDistance)
            return true;
        else
            return false;
    }

    private IEnumerator DoStartDash()
    {
        playerController.RotateToDir(true);
        IgnoreCollision();
        useCount--;
        cdTimer = CD;
        if (useCount <= 0)
            isCD = true;

        useDashAttack = false;

        dashSpeed = dashDistanceCount / needTime;
        while (dashDistanceCount > 0)
        {
            dashDistanceCount -= Time.deltaTime * dashSpeed;
            transform.position += dashDir * Time.deltaTime * dashSpeed;
            yield return null;
        }
        IgnoreCollision(false);
    }

    private void IgnoreCollision(bool ignore = true)
    {
        Physics.IgnoreLayerCollision(9, 10, ignore);
        Physics.IgnoreLayerCollision(9, 12, ignore);
        Physics.IgnoreLayerCollision(9, 13, ignore);
        Physics.IgnoreLayerCollision(9, 14, ignore);
    }

    public override void UpdateAction()
    {
        if (playerController.defaultInputAction.Player.Attack.triggered && useDashAttack == false && TeachManager.Instance.unLockAttackAndDashAttack)
        {
            ForceEnd();

            playerActionManager.Attack();
            useDashAttack = true;
        }
        invincibleTimer -= Time.deltaTime;
        if (invincibleTimer <= 0)
            Hurt.isInvincible = false;
    }

    protected override void EndAction()
    {
        invincibleTimer = 0;
        Hurt.isInvincible = false;
        IgnoreCollision(false);
        base.EndAction();
    }

    private void OnDrawGizmos()
    {
        if (debug == false) return;

        Gizmos.color = Color.red;
        Gizmos.DrawSphere(transform.position + transform.forward * dashDistance, 0.55f);
        Gizmos.color = Color.green;
        Gizmos.DrawSphere(transform.position + transform.forward * dashDistanceCount, 0.55f);
    }
}