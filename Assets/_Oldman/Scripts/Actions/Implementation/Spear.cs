using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spear : Damagable
{
    public Vector3 dashAttackOffset = Vector3.up;
    public float dashAttackRadius = 1;

    public void CountDamage()
    {
        damage = PlayerBounsValue.Instance.GetBounsBaseDamage(attack.SkillList[attack.skillIndex].damage);
        //damage = attack.SkillList[attack.skillIndex].damage;
    }

    public override void DealDamage()
    {
        CountDamage();
        if (attack.skillIndex == 4)
        {
            Collider[] colliders = Physics.OverlapSphere(user.position + dashAttackOffset, dashAttackRadius, canDamageLayer);

            foreach (Collider c in colliders)
            {
                IDestroyable destroyable = c.transform.GetComponent<IDestroyable>();
                IHurtable hurtable = c.transform.GetComponent<IHurtable>();
                destroyable?.Damage(1);
                hurtable?.ReduceHurtCount(1);
                hurtable?.Damage(user, damage);
            }
        }
        else if(attack.skillIndex == 3)
        {
            base.DealDamage();
        }
        else
            base.DealDamage();
    }

    private void OnDrawGizmos()
    {
        if (debug == false) return;
        Gizmos.color = Color.red;
        GizmosExtensions.DrawCircle(user.position + dashAttackOffset, dashAttackRadius);

        Matrix4x4 rotationMatrix = Matrix4x4.TRS(user.position, user.rotation, user.lossyScale);
        Gizmos.matrix = rotationMatrix;
        Gizmos.DrawWireCube(new Vector3(0, 0, offectZ), halfExtents);
    }
}