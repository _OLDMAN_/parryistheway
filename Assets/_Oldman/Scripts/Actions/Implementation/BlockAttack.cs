using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockAttack : MonoBehaviour
{
    public LayerMask trapLayer;
    public LayerMask enemyLayer;
    public AutoSkillBar skillBar;

    private PlayerActionManager actionManager;
    private PlayerGem playerGem;
    private Attack attack;
    private PlayerEnergy playerEnergy;

    public float blockAttackResetSec = 0.3f;
    private float blockTimer;

    private void Start()
    {
        attack = GetComponent<Attack>();
        actionManager = GetComponent<PlayerActionManager>();
        playerGem = GetComponent<PlayerGem>();
        playerEnergy = GetComponent<PlayerEnergy>();
    }

    private void Update()
    {
        if (blockTimer > 0)
        {
            if (PlayerController.Instance.defaultInputAction.Player.Attack.triggered)
                DoBlockAttack();
            blockTimer -= Time.deltaTime;
        }
    }

    public void BlockSuccess()
    {
        if (playerEnergy.isEnergyFull == true)
        {
            if (QuestManager.Instance.currentQuest != null)
                if (QuestManager.Instance.currentQuest.GetType() == typeof(Quest_BlockAutoAttackOnce))
                    QuestManager.Instance.currentQuest.QuestComplete();

            RotateToEnemy();

            if (playerGem.autoBlockSkill.skill != null)
                playerGem.autoBlockSkill.skill.Cast();
            else
                actionManager.ParryAttack();
            playerEnergy.energy = 0;
        }
        else
            blockTimer = blockAttackResetSec;
    }

    private void DoBlockAttack()
    {
        if (QuestManager.Instance.currentQuest.GetType() == typeof(Quest_BlockAttackOnce))
            QuestManager.Instance.currentQuest.QuestComplete();

        blockTimer = 0;
        RotateToEnemy();

        if (playerGem.blockSkill.skill != null)
            playerGem.blockSkill.skill.Cast();
        else
            actionManager.ParryAttack();
    }

    public void RotateToEnemy()
    {
        Collider[] enemys = Physics.OverlapSphere(transform.position, 99, enemyLayer);
        if (enemys.Length == 0)
        {
            RotateToTrap();
            return;
        }
        Collider nearestEnemy = null;
        foreach (Collider c in enemys)
        {
            if (nearestEnemy != null)
            {
                nearestEnemy = Vector3.Distance(c.transform.position, transform.position) < Vector3.Distance(nearestEnemy.transform.position, transform.position) ? c : nearestEnemy;
            }
            else
            {
                nearestEnemy = c;
            }
        }
        Vector3 autoDir = nearestEnemy.transform.position - transform.position;
        autoDir.y = 0;
        transform.forward = autoDir;
    }

    private void RotateToTrap()
    {
        Collider[] traps = Physics.OverlapSphere(transform.position, 99, trapLayer);
        if (traps.Length == 0) return;

        Collider nearestTrap = null;
        foreach (Collider c in traps)
        {
            if (nearestTrap != null)
                nearestTrap = Vector3.Distance(c.transform.position, transform.position) < Vector3.Distance(nearestTrap.transform.position, transform.position) ? c : nearestTrap;
            else
                nearestTrap = c;
        }
        Vector3 autoDir = nearestTrap.transform.position - transform.position;
        autoDir.y = 0;
        transform.forward = autoDir;
    }
}
