using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Hurt : CharacterAction
{
    [HideInInspector] public bool isHurting;
    private List<Material> materials = new List<Material>();
    public bool isCD{get {return hurtTimer > 0;}}
    public float hurtCD;
    public float knockbackDistance;
    private float hurtTimer;

    public static bool isInvincible;

    private void Awake()
    {
        Renderer[] renderer = GetComponentsInChildren<Renderer>();
        foreach (Renderer r in renderer)
            materials.Add(r.material);
    }

    void Update()
    {
        if(hurtTimer > 0)
            hurtTimer -= Time.deltaTime;
    }

    public override void StartAction()
    {
        base.StartAction();
        isHurting = true;
        hurtTimer = hurtCD;
    }

    public void Dodeal(int damage, Transform attacker, bool knockBack, bool lookHurtDir = true)
    {
        if (isInvincible == true) return;
        PlayerHP.Instance.Damage(damage);
        Vector3 dir = attacker.position - transform.position;
        dir.y = 0;
        dir.Normalize();
        HurtStart(dir, lookHurtDir);
        if (knockBack == true)
            Knockback(dir);
        ParticleManager.Instance.PlayParticle("PlayerHurt", transform.position + Vector3.up, Quaternion.identity);
        SoundManager.Instance.PlaySound("PlayerHurt");
    }

    public void Dodeal(int damage, Vector3 dir, bool knockBack, bool lookHurtDir = true)
    {
        if (isInvincible == true) return;
        PlayerHP.Instance.Damage(damage);
        dir.y = 0;
        HurtStart(-dir, lookHurtDir);
        if (knockBack == true)
            Knockback(-dir);
        ParticleManager.Instance.PlayParticle("PlayerHurt", transform.position + Vector3.up, Quaternion.identity);
        SoundManager.Instance.PlaySound("PlayerHurt");
    }

    private void HurtStart(Vector3 dir, bool lookHurtDir)
    {
        //rotate
        if (lookHurtDir == true)
            transform.forward = new Vector3(dir.x, 0, dir.z);

        //change color
        Blink();
    }

    private void Blink()
    {
        foreach (Material m in materials)
        {
            if(m.HasProperty("_Color"))
            m.DOColor(Color.red, 0.2f / 4).onComplete += () => m.DOColor(Color.white, 0.2f / 4).onComplete += () => m.DOColor(Color.red, 0.2f / 4).onComplete += () => m.DOColor(Color.white, 0.2f / 4);
        }
    }

    private void Knockback(Vector3 dir)
    {
        StartCoroutine(StartKnockback(-dir));
    }

    private IEnumerator StartKnockback(Vector3 dir)
    {
        float time = 0.2f;
        float counter = knockbackDistance;
        float speed = counter / time;
        while(counter > 0)
        {
            characterController.SimpleMove(dir * speed);
            counter -= Time.deltaTime * speed;
            yield return null;
        }
    }

    protected override void EndAction()
    {
        isHurting = false;
        base.EndAction();
    }
}