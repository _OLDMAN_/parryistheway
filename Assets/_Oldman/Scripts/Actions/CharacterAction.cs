using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAction : MonoBehaviour
{
    #region reference

    protected PlayerActionManager playerActionManager;
    protected PlayerController playerController;
    protected CharacterController characterController;
    protected PlayerAnimation playerAnimation;

    #endregion reference

    public ActionAnimationData aniData;

    protected List<Coroutine> coroutines = new List<Coroutine>();
    [HideInInspector] public float startTime;

    public virtual void Start()
    {
        playerActionManager = GetComponent<PlayerActionManager>();
        characterController = GetComponent<CharacterController>();
        playerAnimation = GetComponent<PlayerAnimation>();
        playerController = GetComponent<PlayerController>();
    }

    public virtual void InitiateAction()
    {
        startTime = Time.time;
    }

    public virtual void StartAction()
    {
        SetActionEndTime();
    }

    public virtual void SetActionEndTime()
    {
        float actionLength = GetAnimationLength();
        float actionEndTime = actionLength * aniData.ActionEndPercentage;
        coroutines.Add(StartCoroutine(TickToEndAction(actionEndTime)));
    }

    protected float GetAnimationLength()
    {
        return playerAnimation.GetAnimationHolder(aniData.actionAnimation.ToString()).Length;
    }

    public virtual void PlayAnimation()
    {
        playerAnimation.PlayAnimation(aniData.actionAnimation);
    }

    public virtual void UpdateAction()
    {
    }

    protected virtual void EndAction()
    {
        //foreach (Coroutine c in coroutines)
        //    if (c != null)
        //        StopCoroutine(c);
        playerActionManager.RemoveCurrentAction();
    }

    public void ForceEnd()
    {
        foreach (Coroutine c in coroutines)
        {
            if(c != null)
            StopCoroutine(c);
        }
        EndAction();
    }

    private IEnumerator TickToEndAction(float aniSec)
    {
        yield return new WaitForSeconds(aniSec);
        EndAction();
    }
}