using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerActionManager : MonoBehaviourSingleton<PlayerActionManager>
{
    private CharacterAction currentAction;
    private CharacterAction tempAction;
    public CharacterAction lastAction;
    #region Actions
    public Dash dash { get; private set; }
    public Hurt hurt { get; private set; }
    private Block block;
    private Attack attack;
    private Die die;
    private HeavyAttack parryAttack;

    private ShieldDash shieldDash;
    private ThrowShield throwShield;
    private ThrowSpear throwSpear;
    private SweepAttack sweepAttack;
    private DashSting dashSting;
    private Kick kick;
    #endregion Actions
    public bool isInAction
    {
        get
        {
            return currentAction != null;
        }
    }

    public override void Awake()
    {
        base.Awake();
        GetSkillReference();
    }
    private void GetSkillReference()
    {
        dash = GetComponent<Dash>();
        block = GetComponent<Block>();
        attack = GetComponent<Attack>();
        hurt = GetComponent<Hurt>();
        die = GetComponent<Die>();
        parryAttack = GetComponent<HeavyAttack>();
        shieldDash = GetComponent<ShieldDash>();
        throwShield = GetComponent<ThrowShield>();
        throwSpear = GetComponent<ThrowSpear>();
        sweepAttack = GetComponent<SweepAttack>();
        dashSting = GetComponent<DashSting>();
        kick = GetComponent<Kick>();
        
    }

    private void Update()
    {
        if (isInAction)
        {
            currentAction.UpdateAction();
        }
    }

    private void AddAction<T>(T action) where T : CharacterAction
    {
        tempAction = action;

        if (isInAction)
        {
            if (IsActionAbleToInterupt())
            {
                currentAction.ForceEnd();
                ExcuteNextAction();
            }
            else
            {
                return;
            }
        }
        else
        {
            ExcuteNextAction();
        }
    }

    private bool IsActionAbleToInterupt()
    {
        // can interupt and not the same action
        var isInteruptableAndNotTheSame = tempAction.aniData.interuptOtherAnimation && !(currentAction.GetType() == tempAction.GetType());

        return isInteruptableAndNotTheSame;
    }

    public void RemoveCurrentAction()
    {
        if (currentAction != null)
        {
            lastAction = currentAction;
        }
        currentAction = null;
    }

    private void ExcuteNextAction()
    {
        currentAction = tempAction;
        currentAction.InitiateAction();
        currentAction.StartAction();
        currentAction.PlayAnimation();
    }

    public void Dash()
    {
        if (die.isDead == true) return;
        AddAction(dash);
    }

    public void Block()
    {
        if (die.isDead == true) return;
        AddAction(block);
    }

    public void Attack()
    {
        if (die.isDead == true) return;
        AddAction(attack);
    }

    public void ParryAttack()
    {
        if (die.isDead == true) return;
        AddAction(parryAttack);
    }

    public void ShieldDash()
    {
        if (die.isDead == true) return;
        AddAction(shieldDash);
    }
    public void ThrowShield()
    {
        if (die.isDead == true) return;
        AddAction(throwShield);
    }
    public void ThrowSpear()
    {
        if (die.isDead == true) return;
        AddAction(throwSpear);
    }
    public void SweepAttack()
    {
        if (die.isDead == true) return;
        AddAction(sweepAttack);
    }
    public void DashSting()
    {
        if (die.isDead == true) return;
        AddAction(dashSting);
    }
    public void Kick()
    {
        if (die.isDead == true) return;
        AddAction(kick);
    }
    public void Die()
    {
        AddAction(die);
    }

    public void StartHurt(int damage, Transform attacker, bool knockBack = false)
    {
        if(hurt.isCD == true || Hurt.isInvincible == true) return;
        AddAction(hurt);
        hurt.Dodeal(damage, attacker, knockBack);
    }

    public void StartHurt(int damage, Vector3 dir, bool knockBack = false)
    {
        if (hurt.isCD == true || Hurt.isInvincible == true) return;
        AddAction(hurt);
        hurt.Dodeal(damage, dir, knockBack);
    }

}