using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ActionAnimationData
{
    public PlayerAni actionAnimation;
    public bool interuptOtherAnimation = false;
    public float ActionEndPercentage = 1f; //0~1
}