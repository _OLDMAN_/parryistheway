using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;

public class StatueWatch : InteractableObject
{
    public CinemachineVirtualCamera watchCamera;
    public GameObject dialogueCanvas;
    public TMPro.TMP_Text TMPText;
    public string context;

    private bool isInteractLocked = true;
    [HideInInspector] public bool isTriggerOnce = false; 
    private void Start()
    {
        TMPText.text = context;
    }
    protected override void Interact()
    {
        base.Interact();
        LookStatue();
        isTriggerOnce = true;
    }
    public void LookStatue()
    {
        watchCamera.enabled = true;
        dialogueCanvas.SetActive(true);
    }
    public void StopLook()
    {
        watchCamera.enabled = false;
        dialogueCanvas.SetActive(false);
    }

    protected override void OnTriggerStay(Collider other)
    {
        if (isInteractLocked) return;
        if (canUse == false || playerEneter == true) return;

        if (other.CompareTag("Player"))
        {
            playerEneter = true;
            ShowHint();
            PlayerController.Instance.inItemInteractRange = true;
            PlayerHP.Instance.SwitchHP(false);
        }
    }
    protected override void OnTriggerExit(Collider other)
    {
        if (isInteractLocked) return;
        if (other.CompareTag("Player"))
        {
            playerEneter = false;
            CloseHint();
            PlayerController.Instance.inItemInteractRange = false;
            StopLook();
            PlayerHP.Instance.SwitchHP(true);
        }
    }

    public void UnlockInteract()
    {
        isInteractLocked = false;
    }

}
