using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(Animator))]
public class IKControl : MonoBehaviour
{
    protected Animator animator;

    public bool ikActive = false;
    public LayerMask lookAtLayer;
    public Transform lookObj = null;

    private float timeElapsed = 0f;
    public float updateInterval = 1f;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void Update()
    {
        if (timeElapsed < updateInterval)
        {
            timeElapsed += Time.deltaTime;
            return;
        }
        timeElapsed -= updateInterval;

        UpdateLookAtPoint();
    }

    private void UpdateLookAtPoint()
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position, 5, lookAtLayer);
        if (colliders.Length > 0)
        {
            lookObj = colliders[0].transform;
        }
    }

    //a callback for calculating IK
    private void OnAnimatorIK()
    {
        if (animator)
        {
            //if the IK is active, set the position and rotation directly to the goal.
            if (ikActive)
            {
                // Set the look target position, if one has been assigned
                if (lookObj != null)
                {
                    animator.SetLookAtWeight(1, 0, 1, 0, 0.6f);
                    animator.SetLookAtPosition(lookObj.position);
                }
            }
        }
        //if the IK is not active, set the position and rotation of the hand and body back to the original position
        else
        {
            animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 0);
            animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 0);
            animator.SetLookAtWeight(0);
        }
    }
}