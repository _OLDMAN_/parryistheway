using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DefaultInputActions;

public class PlayerCtrlMove : MonoBehaviour
{
    //reference
    [HideInInspector] public Animator animator;
    [HideInInspector] public new PlayerAnimation animation;
    private CharacterController characterController;
    private PlayerActionManager playerActionManager;

    public Vector2 input;
    public Vector3 targetDir;
    public float rotateLerpSpeed;
    public float moveSpeed;
    public LayerMask obsLayer;
    public bool canCtrl { get; set; } = true;
    public bool inItemInteractRange { get; set; } = false;
    [HideInInspector] public bool isSkilling;

    public DefaultInputAction defaultInputAction;

    public void Awake()
    {
        defaultInputAction = new DefaultInputAction();
    }

    private void OnEnable()
    {
        defaultInputAction.Enable();
    }

    private void OnDisable()
    {
        defaultInputAction.Disable();
    }

    private void Start()
    {
        animator = GetComponent<Animator>();
        animation = GetComponent<PlayerAnimation>();
        characterController = GetComponent<CharacterController>();
    }

    private void Update()
    {
        if (canCtrl == false) return;

        input = defaultInputAction.Player.Move.ReadValue<Vector2>();
        Vector3 cameraForward = Camera.main.transform.forward;
        cameraForward.y = 0;
        targetDir = (Camera.main.transform.right * input.x + cameraForward.normalized * input.y).normalized;
        RotateToDir();
        Move();
    }

    private void LateUpdate()
    {
        Vector3 tempPos = transform.position;
        tempPos.y = 0;
        transform.position = tempPos;
    }

    public void RotateToDir(bool forceRotate = false)
    {
        if (forceRotate)
        {
            if (targetDir != Vector3.zero)
            {
                transform.forward = targetDir;
            }
            return;
        }

        if (targetDir != Vector3.zero)
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(targetDir), rotateLerpSpeed);
    }

    public void RotateToDir(Transform target)
    {
        if (target == null) return;

        Vector3 dir = target.position - transform.position;
        transform.forward = new Vector3(dir.x, 0, dir.z);
    }

    public void Move()
    {
        float speed = Mathf.Clamp(input.magnitude, 0, 1);
        animator.SetFloat("RunSpeed", speed);
        if (input == Vector2.zero)
        {
            animation.PlayAnimation(PlayerAni.Idle);
        }
        else
        {
            characterController.SimpleMove(targetDir * moveSpeed);
            animation.PlayAnimation(PlayerAni.Move);
        }
    }

    public void SetPos(Vector3 pos)
    {
        characterController.enabled = false;
        transform.position = pos;
        characterController.enabled = true;
    }

}
