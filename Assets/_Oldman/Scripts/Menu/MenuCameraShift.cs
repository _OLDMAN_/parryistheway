﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCameraShift : MonoBehaviour
{
    public static MenuCameraShift instance;
    public Vector3 introRotate;
    public float introSec;
    public bool IsIntroPlaying = false;

    Vector3 originEuler;
    Vector3 targetEuler;
    public Vector2 CameraRotateVertical = new Vector2(1f, -1f);
    public Vector2 CameraRotateHorizontal = new Vector2(-1.5f, 1.5f);
    public float lerpSpeed = 0.025f;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        //CameraSlideIn();
    }

    public void CameraSlideIn()
    {
        transform.DORotate(transform.eulerAngles + introRotate, introSec).From().onComplete += () =>
        {
           originEuler = transform.eulerAngles;
           IsIntroPlaying = false;
        };
    }
    public void Update()
    {
        if (IsIntroPlaying) return;

        MapMousePosToCameraLookPos();
        LerpCameraLookPos();
    }


    public void MapMousePosToCameraLookPos()
    {
        //左右看對應滑鼠X，上下看對應滑鼠Y
        targetEuler.x = originEuler.x + Map(Input.mousePosition.y, 0, Screen.height, CameraRotateVertical.x, CameraRotateVertical.y);
        targetEuler.y = originEuler.y + Map(Input.mousePosition.x, 0, Screen.width, CameraRotateHorizontal.x, CameraRotateHorizontal.y);
    }
    public void LerpCameraLookPos()
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(targetEuler), lerpSpeed);
    }

    public float Map(float x, float in_min, float in_max, float out_min, float out_max)
    {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

}
