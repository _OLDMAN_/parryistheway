using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

public class OptionsPanel : MonoBehaviour
{
    //public Slider masterVolumeSlider;
    public Slider BGMVolumeSlider;
    public Slider SFXVolumeSlider;
    public AudioMixer masterMixer;

    public GameObject optionObj;

    public GameObject openSelected;

    private void Start()
    {
        //SetMasterVolume(masterVolumeSlider.value);
        SetBGMVolume(BGMVolumeSlider.value);
        SetSFXVolume(SFXVolumeSlider.value);
    }

    private void OnEnable()
    {
    }

    //private void Update()
    //{
    //    if (Gamepad.current.startButton.IsActuated() || Keyboard.current.escapeKey.IsActuated())
    //    {
    //        TogglePanel();
    //    }
    //}

    public void TogglePanel(bool onoff)
    {
        optionObj.SetActive(onoff);
        if (onoff)
        {
            EventSystem.current.SetSelectedGameObject(openSelected);
        }
    }

    public void SetMasterVolume(float volume)
    {
        masterMixer.SetFloat("MasterVolume", volume);
    }

    public void SetBGMVolume(float volume)
    {
        masterMixer.SetFloat("BGMVolume", volume);
    }

    public void SetSFXVolume(float volume)
    {
        masterMixer.SetFloat("SFXVolume", volume);
    }

    public void ReturnMenu()
    {
        SoundManager.Instance.BackToMenu();
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void PlayUISound(string name)
    {
        SoundManager.Instance.PlaySound(name);
    }
}