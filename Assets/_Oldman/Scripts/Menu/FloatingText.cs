using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using Unity.Mathematics;

public class FloatingText : MonoBehaviour
{
    public TMP_Text text;
    public float inSec;
    public float outSec;

    [Range(0, 3)]
    public float moveSpeed;
    [Range(0, 3)]
    public float moveDistance;
    bool isFading = false;
    Vector3 oriPos;
    Vector3 targetDir;


    private void Start()
    {
        text.fontMaterial.SetFloat(ShaderUtilities.ID_FaceDilate, -0.6f);
        oriPos = transform.position;
    }
    private void Update()
    {
        transform.position = oriPos + (Mathf.Sin(Time.time * moveSpeed) * text.transform.up) * moveDistance;
    }
    void SetRandomTargetDir()
    {
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player"))
            StartCoroutine(FadeIn());
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
            StartCoroutine(FadeOut());
    }
    IEnumerator FadeIn()
    {
        if (isFading) yield break;

        isFading = true;
        float dilate = -0.6f;
        while (dilate < 0)
        {
            dilate += Time.deltaTime * (1f / inSec);

            text.fontMaterial.SetFloat(ShaderUtilities.ID_FaceDilate, dilate);

            yield return null;
        }

        text.fontMaterial.SetFloat(ShaderUtilities.ID_FaceDilate, 0);


    }
    IEnumerator FadeOut()
    {
        float dilate = 0f;
        while (dilate > -0.6f)
        {
            dilate -= Time.deltaTime * (1f / inSec);

            text.fontMaterial.SetFloat(ShaderUtilities.ID_FaceDilate, dilate);

            yield return null;
        }

        text.fontMaterial.SetFloat(ShaderUtilities.ID_FaceDilate, -0.6f);
        isFading = false;
    }

}
