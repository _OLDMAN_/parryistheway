﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class UI_Animation : MonoBehaviour
{
    RectTransform rect;
    private void Start()
    {
        rect = GetComponent<RectTransform>();
    }

    public void FadeIn()
    {

    }
    public void FadeOut()
    {

    }
    public void HighLight()
    {
        rect.DOScale(1.1f, 0.3f);
    }
    public void Normal()
    {
        rect.DOScale(1, 0.3f);
    }
    public void PointerDown()
    {
        rect.DOScale(0.9f, 0.1f);
    }
    public void PointerUp()
    {
        rect.DOScale(1f, 0.1f);
    }

}
