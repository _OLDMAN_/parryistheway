﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.InputSystem;
using Cinemachine;
using UnityEngine.EventSystems;

public class MainMenu : MonoBehaviourSingleton<MainMenu>
{
    public GameObject mainCamera;
    public GameObject endCamera;

    public Image black;
    public float blackFadeInSec;
    public float blackFadeOutSec;

    public GameObject title;
    public List<TextMeshProUGUI> textUGUI = new List<TextMeshProUGUI>();
    public OptionsPanel options;
    public GameObject buttons;
    public GameObject credits;
    public float panelFadeSec;
    public float panelScaleSec;

    public List<Button> buttonList = new List<Button>();
    private Coroutine blend;
    private bool isMoveing;

    private bool optionIsOn = false;

    public GameObject firstSelected;

    private void Start()
    {
        //StartCoroutine(TitleFadeIn());
        BlackFadeIn();
        options = FindObjectOfType<OptionsPanel>(true);
    }

    private void Update()
    {
        if (Gamepad.current != null)
        {
            if (Gamepad.current.buttonEast.wasPressedThisFrame)
            {
                CloseCredit();
                CloseSetting();
                SelectFirstBtn();
            }

            if ((Gamepad.current.startButton.wasPressedThisFrame) && !optionIsOn)
            {
                OpenSetting();
            }
            else if ((Gamepad.current.startButton.wasPressedThisFrame) && optionIsOn)
            {
                CloseSetting();
                SelectFirstBtn();
            }
        }

        if ((Keyboard.current.escapeKey.wasPressedThisFrame) && !optionIsOn)
        {
            OpenSetting();
        }
        else if ((Keyboard.current.escapeKey.wasPressedThisFrame) && optionIsOn)
        {
            CloseSetting();
            SelectFirstBtn();
        }

        if (isMoveing == true)
        {
            if (Input.GetMouseButtonDown(0))
            {
                StartCoroutine(SkipBlend());
                isMoveing = false;
            }
        }
    }

    private void SelectFirstBtn()
    {
        if (firstSelected == null && Gamepad.current != null) return;
        EventSystem.current.SetSelectedGameObject(firstSelected);
    }

    public void BlackFadeOut()
    {
        black.DOFade(1, blackFadeOutSec).From(0);
    }
    public void BlackFadeOut(System.Action onComplete)
    {
        black.DOFade(1, blackFadeOutSec).From(0).OnComplete(() =>{
            onComplete.Invoke();
        });
    }

    public void BlackFadeOut(string sceneName)
    {
        black.DOFade(1, blackFadeOutSec).From(0).onComplete += () => SceneManager.LoadScene(sceneName);
    }

    public void BlackFadeIn()
    {
        black.DOFade(0, blackFadeInSec).From(1).SetEase(Ease.InCubic);
    }

    public void OpenSetting()
    {
        optionIsOn = true;
        options.TogglePanel(true);
        ToggleTitleAndButtons(false);

        options.transform.DOScale(1, panelScaleSec).From(0);
        options.GetComponent<Image>().DOFade(1f, panelFadeSec);
    }

    public void CloseSetting()
    {
        optionIsOn = false;
        ToggleTitleAndButtons(true);
        options.transform.DOScale(0, panelScaleSec).onComplete += () => options.TogglePanel(false); ;
        options.GetComponent<Image>().DOFade(0, panelFadeSec);
    }

    public void OpenCredit()
    {
        credits.SetActive(true);
        ToggleTitleAndButtons(true);
        credits.transform.DOScale(1, panelScaleSec).From(0);
        credits.GetComponent<Image>().DOFade(1f, panelFadeSec);
    }

    public void CloseCredit()
    {
        if (credits == null) return;
        ToggleTitleAndButtons(true);
        credits.transform.DOScale(0, panelScaleSec).onComplete += () => credits.SetActive(false); ;
        credits.GetComponent<Image>().DOFade(0, panelFadeSec);
    }

    private void ToggleTitleAndButtons(bool onoff)
    {
        if (title != null) title?.SetActive(onoff);
        if (buttons != null) buttons.SetActive(onoff);
    }

    public void GameStart()
    {
        MenuCameraShift.instance.IsIntroPlaying = true;
        blend = StartCoroutine(Blend());
        //StartCoroutine(TitleFadeOut());
        foreach (Button button in buttons.GetComponentsInChildren<Button>())
            button.interactable = false;
    }

    private IEnumerator Blend()
    {
        isMoveing = true;
        endCamera.SetActive(true);
        yield return new WaitForSeconds(2);
        isMoveing = false;
        BlackFadeOut("Teach");
        //BlackFadeOut(() => testLoad.Instance.LoadScene("Teach"));
    }

    private IEnumerator SkipBlend()
    {
        StopCoroutine(blend);
        mainCamera.GetComponent<CinemachineBrain>().enabled = false;
        yield return null;
        mainCamera.GetComponent<CinemachineBrain>().enabled = true;
        BlackFadeOut("Teach");
        //BlackFadeOut(() => testLoad.Instance.LoadScene("Teach"));
    }

    private IEnumerator TitleFadeOut()
    {
        float dilate = 0;

        while (dilate > -0.5f)
        {
            dilate -= Time.deltaTime;

            foreach (TextMeshProUGUI u in textUGUI)
            {
                u.fontMaterial.SetFloat(ShaderUtilities.ID_FaceDilate, dilate);
            }
            yield return null;
        }
    }

    public void LoadScene(string sceneName)
    {
        foreach (Button b in buttonList)
        {
            b.interactable = false;
        }
        BlackFadeOut(sceneName);
    }

    public void Exit()
    {
        Application.Quit();
    }
}