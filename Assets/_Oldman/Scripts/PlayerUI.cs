using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUI : MonoBehaviour
{
    private bool active = false;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Alpha6))
        {
            PlayerHP.Instance.SwitchHP(active);
            active = !active;
        }
    }
}