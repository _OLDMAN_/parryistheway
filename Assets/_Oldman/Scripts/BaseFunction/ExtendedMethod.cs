using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtendedMethod
{
    public static TransformStruct ToStruct(this Transform trans)
    {
        TransformStruct s;
        s.position = trans.position;
        s.rotation = trans.rotation;
        s.localScale = trans.localScale;
        return s;
    }
}

public struct TransformStruct
{
    public Vector3 position, localScale;
    public Quaternion rotation;
}