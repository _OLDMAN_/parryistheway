﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ObjectPoolManager : MonoBehaviourSingleton<ObjectPoolManager>
{
    /// <summary>
    /// 物件池字典
    /// string:被回收的物件類別(key)
    /// object:被回收的物件實體(val)
    /// </summary>
    private Dictionary<string, object> poolList = new Dictionary<string, object>();

    private Dictionary<string, Transform> poolDrawer = new Dictionary<string, Transform>();
    private void Start()
    {
        DontDestroyOnLoad(gameObject);
    }

    public void Recycle<T>(T type, string typeName = "", float delay = 0)
    {
        StartCoroutine(DoRecycle<T>(type, typeName, delay));
    }

    private IEnumerator DoRecycle<T>(T type, string typeName = "", float delay = 0)
    {
        yield return new WaitForSeconds(delay);
        Queue<T> pool = GetPool(type, typeName);

        pool.Enqueue(type);
        string name = typeName.Length > 0 ? typeName : type.GetType().Name;
        (type as MonoBehaviour).transform.SetParent(poolDrawer[name]);
        (type as MonoBehaviour).gameObject.SetActive(false);
        //(type as MonoBehaviour).transform.position = Vector3.right * 100;
    }

    public T Reuse<T>(T type, string typeName = "")
    {
        Queue<T> pool = GetPool(type, typeName);

        if (pool.Count > 0)
        {
            T obj = pool.Dequeue();
            (obj as MonoBehaviour).gameObject.SetActive(true);
            return obj;
        }
        else
        {
            return default;
        }
    }

    private Queue<T> GetPool<T>(T type, string typeName = "")
    {
        string name = typeName.Length > 0 ? typeName : type.GetType().Name;

        if (!poolList.ContainsKey(name))
        {
            poolList.Add(name, new Queue<T>());

            Transform drawer = new GameObject(name).transform;
            drawer.SetParent(transform);
            poolDrawer.Add(name, drawer);
        }

        return (Queue<T>)poolList[name];
    }
}