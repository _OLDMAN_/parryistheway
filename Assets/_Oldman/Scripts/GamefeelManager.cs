using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.InputSystem;
using DG.Tweening;

public class GamefeelManager : MonoBehaviourSingleton<GamefeelManager>
{
    public Parameter_GameFeel feelParameter;
    private Coroutine slowTimeCoroutine;

    private GameObject currentCamera;
    private CinemachineVirtualCamera currentCmCamera;
    private CinemachineBasicMultiChannelPerlin cmBaseValue;
    private float shakeTimer;
    private float shakeTimerTotal;
    private float startingIntensity;

    public static float stopSpeed = 1;

    private void Update()
    {
        if (shakeTimer > 0)
        {
            shakeTimer -= Time.deltaTime;
            if (shakeTimer <= 0)
            {
                cmBaseValue.m_AmplitudeGain = Mathf.Lerp(startingIntensity, 0, (1 - shakeTimer / shakeTimerTotal));
            }

            Gamepad.current?.SetMotorSpeeds(startingIntensity * 0.005f, startingIntensity * 0.008f);
        }
        else
        {
            Gamepad.current?.SetMotorSpeeds(0, 0);
        }
    }

    private void GetCurrentCM()
    {
        currentCamera = CinemachineCore.Instance.GetActiveBrain(0).ActiveVirtualCamera.VirtualCameraGameObject;
        currentCmCamera = currentCamera.GetComponent<CinemachineVirtualCamera>();
        cmBaseValue = currentCmCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();
    }

    public void ShakeCamera(float intensity, float time)
    {
        GetCurrentCM();
        cmBaseValue.m_AmplitudeGain = intensity;
        startingIntensity = intensity;
        shakeTimerTotal = time;
        shakeTimer = time;
    }
    private IEnumerator DelayShakeCamera(float intensity, float time, float delayTime)
    {
        yield return new WaitForSecondsRealtime(time);
        ShakeCamera(intensity, time);
    }

    public void StartSlowTime(float slowSpeed, float time)
    {
        if (slowTimeCoroutine == null)
            slowTimeCoroutine = StartCoroutine(DelayResetTimeScale(time));
        else
        {
            StopCoroutine(slowTimeCoroutine);
            slowTimeCoroutine = StartCoroutine(DelayResetTimeScale(time));
        }
        Time.timeScale = slowSpeed;
    }
    public void StartSlowTime(float slowSpeed, float time, System.Action onComplete)
    {
        Time.timeScale = slowSpeed;
        if (slowTimeCoroutine == null)
            slowTimeCoroutine = StartCoroutine(DelayResetTimeScale(time, onComplete));
        else
        {
            StopCoroutine(slowTimeCoroutine);
            slowTimeCoroutine = StartCoroutine(DelayResetTimeScale(time, onComplete));
        }
    }
    private IEnumerator DelayResetTimeScale(float time, System.Action onComplete = null)
    {
        yield return new WaitForSecondsRealtime(time);
        onComplete?.Invoke();
        Time.timeScale = 1;
    }
    private void SlowTime(SlowTime slowTimeSetting, System.Action onComplete = null)
    {
        StartCoroutine(BackSlowTime(slowTimeSetting, onComplete));
    }
    private IEnumerator BackSlowTime(SlowTime slowTimeSetting, System.Action onComplete = null)
    {
        Time.timeScale = slowTimeSetting.intensity;
        yield return new WaitForSecondsRealtime(slowTimeSetting.time);
        if (slowTimeSetting.lerpToggle == true)
        {
            while (stopSpeed < 1)
            {
                stopSpeed += Time.deltaTime / slowTimeSetting.lerpTime;
                Time.timeScale = stopSpeed;
                yield return null;
            }
        }

        Time.timeScale = 1;
        stopSpeed = 1;
        onComplete?.Invoke();
    }
    private void StopTime(StopFeel stopFeelSetting)
    {
        stopSpeed = stopFeelSetting.intensity;
        SetCustomTimeSpeed();
        StartCoroutine(BackCustomSlowTime(stopFeelSetting));
    }
    private IEnumerator BackCustomSlowTime(StopFeel stopFeelSetting, System.Action onComplete = null)
    {
        yield return new WaitForSecondsRealtime(stopFeelSetting.time);
        if(stopFeelSetting.lerpToggle == true)
        {
            while (stopSpeed < 1)
            {
                stopSpeed += Time.deltaTime / stopFeelSetting.lerpTime;
                SetCustomTimeSpeed();
                //Time.timeScale = Mathf.MoveTowards(Time.timeScale, 1, 1 / feelParameter.stopLerpTime * startDeltatime);
                yield return null;
            }
        }
        else
        {
            stopSpeed = 1;
        }
        SetCustomTimeSpeed();
        onComplete?.Invoke();
    }
    private void SetCustomTimeSpeed()
    {
        foreach (GameObject o in WaveManager.enemy)
            o.GetComponentInChildren<Animator>().speed = stopSpeed;
        PlayerController.Instance.animator.speed = stopSpeed;
        DOTween.timeScale = stopSpeed;
    }

    public void LastKill()
    {
        Debug.Log("�̫�@��");
        SlowTime(feelParameter.lastAttackSlow);
        ShakeCamera(feelParameter.lastAttackShake.intensity, feelParameter.lastAttackShake.time);
    }

    public void HeavyAttackFeel()
    {
        SlowTime(feelParameter.heavyAttackSlow);
        ShakeCamera(3f, 0.1f);
    }

    public void PlayerBlockFeel()
    {
        //StopTime(feelParameter.blockAttackStop);
        //StartCoroutine(DelayShakeCamera(2f, 0.1f, 0.15f));
        SlowTime(feelParameter.blockAttackSlow);
        ShakeCamera(3f, 0.2f);
    }
    public void BossLastKill()
    {
        SlowTime(feelParameter.bossLastAttackSlow);
    }

}