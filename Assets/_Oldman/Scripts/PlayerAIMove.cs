using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using System;

public class PlayerAIMove : MonoBehaviourSingleton<PlayerAIMove>
{
    private PlayerController playerCtrl;

    private void Start()
    {
        playerCtrl = GetComponent<PlayerController>();
    }
    public void StartAIMove(Vector3 startPos, Vector3 endPos, bool walkEndCanCtrl = true, Action onComplete = null)
    {
        StartCoroutine(AIMove(startPos, endPos, walkEndCanCtrl, onComplete));
    }
    public void StartAIMove(Vector3 endPos, bool walkEndCanCtrl = true, Action onComplete = null)
    {
        StartCoroutine(AIMove(endPos, walkEndCanCtrl, onComplete));
    }
    public void StartAIMove(Vector3 startPos, Vector3[] pathPoints, bool walkEndCanCtrl = true, Action onComplete = null)
    {
        StartCoroutine(AIMove(startPos, pathPoints, walkEndCanCtrl, onComplete));
    }
    public void StartAIMove(Vector3[] pathPoints, bool walkEndCanCtrl = true, Action onComplete = null)
    {
        StartCoroutine(AIMove(pathPoints, walkEndCanCtrl, onComplete));
    }

    private IEnumerator AIMove(Vector3 startPos, Vector3 endPos, bool walkEndCanCtrl, Action onComplete = null)
    {
        playerCtrl.GetComponent<Dash>().StopDash();
        playerCtrl.canCtrl = false;
        PlayerController.Instance.SetPos(startPos);
        PlayerController.Instance.lockY = false;
        playerCtrl.input = Vector2.one;
        playerCtrl.targetDir = (endPos - startPos).normalized;
        float maxDistance = Vector3.Distance(startPos, endPos);

        while (Vector3.Distance(transform.position.GetZeroY(), endPos.GetZeroY()) > 1f && maxDistance > 0)
        {
            maxDistance -= playerCtrl.moveSpeed * Time.deltaTime;
            playerCtrl.AIMove();
            playerCtrl.RotateToDir();
            yield return null;
        }

        PlayerController.Instance.lockY = true;
        playerCtrl.animation.PlayAnimation(PlayerAni.Idle);
        playerCtrl.canCtrl = walkEndCanCtrl;
        if (onComplete != null)
            onComplete();
    }
    private IEnumerator AIMove(Vector3 endPos, bool walkEndCanCtrl, Action onComplete = null)
    {
        PlayerController.Instance.lockY = false;
        playerCtrl.GetComponent<Dash>().StopDash();
        Vector3 startPos = transform.position;
        playerCtrl.canCtrl = false;
        playerCtrl.input = Vector2.one;
        playerCtrl.targetDir = (endPos - startPos).normalized;
        float maxDistance = Vector3.Distance(startPos, endPos);

        while (Vector3.Distance(transform.position.GetZeroY(), endPos.GetZeroY()) > 1f && maxDistance > 0)
        {
            maxDistance -= playerCtrl.moveSpeed * Time.deltaTime;
            playerCtrl.AIMove();
            playerCtrl.RotateToDir();
            yield return null;
        }
        PlayerController.Instance.lockY = true;
        playerCtrl.animation.PlayAnimation(PlayerAni.Idle);
        playerCtrl.canCtrl = walkEndCanCtrl;
        if (onComplete != null)
            onComplete();
    }
    private IEnumerator AIMove(Vector3 startPos, Vector3[] pathPoints, bool walkEndCanCtrl, Action onComplete = null)
    {
        int pointNum = 0;
        playerCtrl.GetComponent<Dash>().StopDash();
        playerCtrl.canCtrl = false;
        PlayerController.Instance.SetPos(startPos);
        playerCtrl.input = Vector2.one;
        PlayerController.Instance.lockY = false;
        while (pointNum < pathPoints.Length)
        {
            playerCtrl.targetDir = (pathPoints[pointNum] - (pointNum == 0 ? startPos : pathPoints[pointNum - 1])).normalized;
            float maxDistance = Vector3.Distance(pathPoints[pointNum], (pointNum == 0 ? startPos : pathPoints[pointNum - 1]));
            while (Vector3.Distance(transform.position.GetZeroY(), pathPoints[pointNum].GetZeroY()) > 1f && maxDistance > 0)
            {
                maxDistance -= playerCtrl.moveSpeed * Time.deltaTime;
                playerCtrl.AIMove();
                playerCtrl.RotateToDir();
                yield return null;
            }
            pointNum++;
        }
        PlayerController.Instance.lockY = true;
        playerCtrl.animation.PlayAnimation(PlayerAni.Idle);
        playerCtrl.canCtrl = walkEndCanCtrl;
        if (onComplete != null)
            onComplete();
    }
    private IEnumerator AIMove(Vector3[] pathPoints, bool walkEndCanCtrl, Action onComplete = null)
    {
        int pointNum = 0;
        playerCtrl.GetComponent<Dash>().StopDash();
        Vector3 startPos = transform.position;
        playerCtrl.canCtrl = false;
        playerCtrl.input = Vector2.one;
        PlayerController.Instance.lockY = false;
        while (pointNum < pathPoints.Length)
        {
            playerCtrl.targetDir = (pathPoints[pointNum] - (pointNum == 0 ? startPos : pathPoints[pointNum - 1])).normalized;
            float maxDistance = Vector3.Distance(pathPoints[pointNum], (pointNum == 0 ? startPos : pathPoints[pointNum - 1]));
            while (Vector3.Distance(transform.position.GetZeroY(), pathPoints[pointNum].GetZeroY()) > 1f && maxDistance > 0)
            {
                maxDistance -= playerCtrl.moveSpeed * Time.deltaTime;
                playerCtrl.AIMove();
                playerCtrl.RotateToDir();
                yield return null;
            }
            pointNum++;
        }
        PlayerController.Instance.lockY = true;
        playerCtrl.animation.PlayAnimation(PlayerAni.Idle);
        playerCtrl.canCtrl = walkEndCanCtrl;
        if (onComplete != null)
            onComplete();
    }
}
