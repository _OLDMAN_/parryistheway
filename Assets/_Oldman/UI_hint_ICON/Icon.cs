using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Icon : MonoBehaviour
{
    protected float secElapsed = 0;
    [SerializeField]private float secPerTick = 0.5f;


    private void Update()
    {
        UpdateTick();
    }

    void UpdateTick()
    {
        secElapsed += Time.deltaTime;
        if(secElapsed > secPerTick)
        {
            secElapsed -= secPerTick;
            UpdateAnimation();
        }
    }

    protected virtual void UpdateAnimation()
    {

    }
}
