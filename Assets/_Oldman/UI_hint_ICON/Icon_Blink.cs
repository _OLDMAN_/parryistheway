using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class Icon_Blink : Icon
{
    protected int index = 0;
    [SerializeField] protected List<Image> DecorationSprite = new List<Image>();

    public bool openOnOffCycle;
    private bool isOnOff = true;

    [Header("BlinkFadeInOut")]
    public bool openFadeBlink;
    public float maxImageAlpha;
    public float fadeSec;
    private void Start()
    {
        foreach (var image in DecorationSprite)
        {
            image.color = new Color(image.color.r, image.color.g, image.color.b,0);
        }
    }
    protected override void UpdateAnimation()
    {
        isOnOff = !isOnOff;
        if (openOnOffCycle && isOnOff == false)
        {
            foreach (var image in DecorationSprite)
            {
                HideDecoration(image);
            }
            return;
        }

        for (int i = 0; i < DecorationSprite.Count; i++)
        {
            if (DecorationSprite[i] == null) continue;

            if (i == index)
            {
                ShowDecoration(DecorationSprite[i]);
            }
            else
            {
                HideDecoration(DecorationSprite[i]);
            }
        }

        index = (index + 1) % DecorationSprite.Count;
    }

    void ShowDecoration(Image image)
    {
        if (openFadeBlink)
        {
            image.DOFade(maxImageAlpha, fadeSec);
        }
        else
        {
            image.DOFade(maxImageAlpha,0);
        }
    }

    void HideDecoration(Image image)
    {
        if (openFadeBlink)
        {
            image.DOFade(0, fadeSec);
        }
        else
        {
            image.DOFade(0, 0);
        }
    }
}
