using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Icon_Rotate : Icon
{
    public Transform origin;
    public GameObject rotateDecoration;

    public float rotateOffset;
    public float rotateSpeed;

    protected override void UpdateAnimation()
    {
        float time = Time.time * rotateSpeed;
        rotateDecoration.transform.position = origin.position + new Vector3(Mathf.Cos(time), Mathf.Sin(time), 0) * rotateOffset;
    }
}
