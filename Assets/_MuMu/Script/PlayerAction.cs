using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Events;

public class PlayerAction : MonoBehaviourSingleton<PlayerAction>
{
    public Action skillHit;
    public Action succesBlock;

    public Coroutine moveSpeedUpCoroutine;

    public int hitOneEnemyBouns = 0;
    public float moveSpeedUpDuring = 0;
    public int moveSpeedUpBouns = 0;

    private float playerOriSpeed;
    private GameObject moveSpeedUpObj;

    private void Start()
    {
        playerOriSpeed = PlayerController.Instance.moveSpeed;
    }

    public void AddSkillHitEnemyMoveSpeedUp()
    {
        if (skillHit == null)
            skillHit += MoveSpeedUp;
    }
    public void RemoveSkillHitEnemyMoveSpeedUp()
    {
        if (moveSpeedUpBouns <= 0)
        {
            skillHit -= MoveSpeedUp;
            moveSpeedUpDuring = 0;
        }
    }

    private void MoveSpeedUp()
    {
        if (moveSpeedUpCoroutine != null)
            StopCoroutine(moveSpeedUpCoroutine);
        if (moveSpeedUpObj != null)
        {
            moveSpeedUpObj.GetComponent<PoolRecycle>().Recycle();
            moveSpeedUpObj = null;
        }
        moveSpeedUpCoroutine = StartCoroutine(DoMoveSpeedUp());
    }

    private IEnumerator DoMoveSpeedUp()
    {
        PlayerController.Instance.moveSpeed = playerOriSpeed * PercentToDecimal(moveSpeedUpBouns);
        moveSpeedUpObj = ParticleManager.Instance.GetParticle("PlayerMoveSpeedUp", transform.position, Quaternion.identity, transform);
        moveSpeedUpObj.transform.parent = transform;
        yield return new WaitForSeconds(moveSpeedUpDuring);
        moveSpeedUpObj.GetComponent<PoolRecycle>().Recycle();
        moveSpeedUpObj = null;
        PlayerController.Instance.moveSpeed = playerOriSpeed;
    }

    private float PercentToDecimal(int bouns)
    {
        return 1 + bouns / 100f;
    }
}
