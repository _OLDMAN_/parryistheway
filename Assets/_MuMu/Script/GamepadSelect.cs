using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;

public class GamepadSelect : MonoBehaviour
{
    public GameObject firstButton;

    private void Update()
    {
        if (Gamepad.current != null)
        {
            if (Gamepad.current.leftStick.IsActuated() && EventSystem.current.currentSelectedGameObject == null)
            {
                EventSystem.current.SetSelectedGameObject(firstButton);
            }
        }
    }
}