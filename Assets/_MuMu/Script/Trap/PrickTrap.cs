using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PrickTrap : MonoBehaviour, IStatusUseAble
{
    public Transform prick;

    public int damage;
    public float cd = 0.5f;

    private bool isPlayerEnter;
    public bool canUse { get; set; } = true;

    public void StopUse()
    {
        canUse = false;
    }

    public void ResetUse()
    {
        canUse = true;
        isPlayerEnter = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (canUse == false) return;

        if (other.CompareTag("Player"))
        {
            isPlayerEnter = true;
            if (canUse == true)
            {
                canUse = false;
                StartCoroutine(DoPrick(other));
            }
        }
    }

    private IEnumerator DoPrick(Collider other)
    {
        prick.DOLocalMoveY(-0.7f, 0.2f).From(-1.5f).SetEase(Ease.InExpo);
        yield return new WaitForSeconds(0.2f);
        ParticleManager.Instance.PlayParticle("PrickAttack", transform.position, Quaternion.identity);
        SoundManager.Instance.PlaySound("SpikeUp");
        prick.DOLocalMoveY(0, 0.1f).From(-0.7f).SetEase(Ease.InExpo);
        yield return new WaitForSeconds(0.1f);
        if (isPlayerEnter == true)
        {
            ParticleManager.Instance.PlayParticle("PrickHitPlayer", other.transform.position + Vector3.up, Quaternion.identity);
            other.GetComponent<PlayerActionManager>().StartHurt(damage, transform, true);
        }
        yield return new WaitForSeconds(0.1f);
        prick.DOLocalMoveY(-1.5f, 0.5f).SetEase(Ease.InExpo).From(0);
        yield return new WaitForSeconds(cd + 0.5f);
        canUse = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
            isPlayerEnter = false;
    }
}
