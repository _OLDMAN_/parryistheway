using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BombAwl : DestroyableObject
{
    public float bombTime;
    public float bombRange;
    public int bombDamage;
    public float ParticleUnitSize = 5;

    public bool isUsing { get; set; } = true;

    public override void Awake()
    {
        base.Awake();
        isDestroy = false;
    }

    //����
    public override void Damage(int count = 1)
    {
        if (isDestroy == true) return;

        destroyCounter -= count;
        if(destroyCounter <= 0)
            Destroy();
    }

    public override void Destroy()
    {
        isDestroy = true;
        GameObject obj = ParticleManager.Instance.GetParticle("AwlBombWarning", transform.position + Vector3.up * 0.1f, Quaternion.identity);
        obj.transform.localScale = Vector3.one * bombRange;
        StartCoroutine(DelayBomb());
    }

    private IEnumerator DelayBomb()
    {
        SoundManager.Instance.PlaySound("AwlHit");
        GetComponent<ObjectShake>().Shake();
        yield return new WaitForSeconds(bombTime);
        GamefeelManager.Instance.ShakeCamera(5, 0.2f);
        SoundManager.Instance.PlaySound("AwlBomb");
        GameObject obj = ParticleManager.Instance.GetParticle("AwlBomb", transform.position + Vector3.up * 0.05f, Quaternion.identity);
        obj.transform.localScale = Vector3.one * bombRange * 2 / ParticleUnitSize;
        Collider[] hits = Physics.OverlapCapsule(transform.position, transform.position + Vector3.up, bombRange);
        foreach (Collider hit in hits)
        {
            IDestroyable destroyable = hit.GetComponent<IDestroyable>();
            IHurtable hurt = hit.GetComponent<IHurtable>();
            hurt?.Damage(transform, bombDamage);
            destroyable?.Damage();

            if (hit.CompareTag("Player"))
                hit.GetComponent<PlayerActionManager>().StartHurt(bombDamage, transform, true);
        }
        Destroy(gameObject);
    }

    //�e�z���d��
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        GizmosExtensions.DrawCircle(transform.position, bombRange);
    }
}
