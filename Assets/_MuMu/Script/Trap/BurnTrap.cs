using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BurnTrap : MonoBehaviour
{
    private PoolRecycle poolRecycle;

    public float burnNeedSec = 0.5f;
    public float burnDuringSec = 1f;
    public float burnSpaceSec = 0.3f;
    public int burnDamage = 2;

    private float burnTimer;
    private float burnSpaceTimer;

    private void Start()
    {
        poolRecycle = GetComponent<PoolRecycle>();
        //StartCoroutine(DelayRecycle(burnDuringSec));
    }

    private IEnumerator DelayRecycle(float time)
    {
        yield return new WaitForSeconds(time);
        poolRecycle.Recycle();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;

        burnTimer = burnNeedSec;
        burnSpaceTimer = burnSpaceSec;
    }

    private void OnTriggerStay(Collider other)
    {
        if (!other.CompareTag("Player")) return;

        if(burnTimer <= 0)
        {
            if(burnSpaceTimer <= 0)
            {
                other.GetComponent<Hurt>().Dodeal(burnDamage, transform, false);

                burnSpaceTimer = burnSpaceSec;
            }
            else
            {
                burnSpaceTimer -= Time.deltaTime;
            }
        }
        else
        {
            burnTimer -= Time.deltaTime;
        }
    }
}
