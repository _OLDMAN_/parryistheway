using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerTrapWarningArea : MonoBehaviour
{
    public TowerTrap towerTrap;

    private void OnTriggerStay(Collider other)
    {
        if (towerTrap.isCD == true) return;

        if (other.CompareTag("Player"))
            towerTrap.Shot();
    }
}
