using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombPillar : DestroyableObject, IStatusUseAble
{
    public GameObject oriObj;
    public GameObject breakObj;

    public int bombDamage;
    public float bombRange;

    public bool canUse { get; set; } = true;

    public override void Awake()
    {
        base.Awake();
        InitialValue();
    }

    private void InitialValue()
    {
        gameObject.layer = LayerMask.NameToLayer("DestructibleObs");
        oriObj.SetActive(true);
        breakObj.SetActive(false);
        isDestroy = false;
    }

    public void StopUse()
    {
        canUse = false;
    }

    public void ResetUse()
    {
        InitialValue();
        canUse = true;
    }

    public override void Damage(int count = 1)
    {
        if (isDestroy == true || canUse == false) return;

        destroyCounter -= count;

        if (destroyCounter <= 0)
            Destroy();
        else
        {
            ParticleManager.Instance.PlayParticle("PillarHit", transform.position, Quaternion.identity);
            SoundManager.Instance.PlaySound("PillarHit");
        }
    }

    public override void Destroy()
    {
        GameObject obj = ParticleManager.Instance.GetParticle("Bomb", transform.position + Vector3.up, Quaternion.identity);
        obj.transform.localScale = Vector3.one * 2;
        ParticleManager.Instance.PlayParticle("PillarBreak", transform.position, Quaternion.identity);
        SoundManager.Instance.PlaySound("PillarBreak");
        GamefeelManager.Instance.ShakeCamera(2, 0.2f);
        oriObj.SetActive(false);
        breakObj.SetActive(true);

        Collider[] hits = Physics.OverlapCapsule(transform.position, transform.position + Vector3.up, bombRange);
        foreach (Collider hit in hits)
        {
            IHurtable hurt = hit.GetComponent<IHurtable>();
            if (hurt != null)
                hurt.Damage(transform, bombDamage);

            if (hit.CompareTag("Player"))
                hit.GetComponent<PlayerActionManager>().StartHurt(bombDamage, transform, true);
        }
        gameObject.layer = default;
        isDestroy = true;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, bombRange);
    }
}