using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class TowerTrap : DestroyableObject, IStatusUseAble
{
    public GameObject oriObj;
    public GameObject breakObj;
    public GameObject bulletPrefab;
    public Transform muzzle;

    public float attackCD = 2f;
    public float bulletSpeed = 30f;
    public int bulletDamage = 30;
    public float respawnTime = 7f;

    public bool canUse { get; set; } = true;
    [HideInInspector]
    public bool isCD = false;

    private Coroutine shotCoroutine;

    public override void Awake()
    {
        base.Awake();
        ResetUse();
    }

    //��l��
    private void InitialValue()
    {
        gameObject.layer = LayerMask.NameToLayer("DestructibleObs");
        oriObj.SetActive(true);
        breakObj.SetActive(false);
        isCD = false;
        isDestroy = false;
    }

    public void StopUse()
    {
        StopAllCoroutines();
        canUse = false;
    }

    public void ResetUse()
    {
        InitialValue();
        canUse = true;
    }

    //�g��
    public void Shot()
    {
        if (isDestroy == true || canUse == false) return;
        shotCoroutine = StartCoroutine(StartShot());
    }

    private IEnumerator StartShot()
    {
        isCD = true;
        SoundManager.Instance.PlaySound("TrapWarning");
        GameObject preP = ParticleManager.Instance.GetParticle("PreShoot", muzzle.position, Quaternion.identity);
        preP.transform.localScale = Vector3.one * 2;
        yield return new WaitForSeconds(0.45f);
        SoundManager.Instance.PlaySound("Shoot");
        GameObject bullet = Instantiate(bulletPrefab, muzzle.position, muzzle.rotation);
        bullet.GetComponent<EnemyBullet>().SetBullet(bulletSpeed, bulletDamage, muzzle.forward, transform);
        bullet.GetComponent<EnemyBullet>().towerBullet = true;
        yield return new WaitForSeconds(attackCD);
        isCD = false;
    }

    public override void Destroy()
    {
        isDestroy = true;
        SoundManager.Instance.PlaySound("TowerTrapBomb");
        GameObject p = ParticleManager.Instance.GetParticle("TowerBomb", transform.position, Quaternion.identity);
        p.transform.localScale = Vector3.one * 2f;
        oriObj.SetActive(false);
        breakObj.SetActive(true);
        gameObject.layer = default;
        if (shotCoroutine != null)
            StopCoroutine(shotCoroutine);
        Respawn();
    }

    //����
    private void Respawn()
    {
        StartCoroutine(DoRespawn());
    }

    private IEnumerator DoRespawn()
    {
        yield return new WaitForSeconds(respawnTime);
        foreach (MeshRenderer meshRenderer in oriObj.GetComponentsInChildren<MeshRenderer>())
            meshRenderer.material.DOFloat(0, "_DeadClip", 1.5f).From(1);
        oriObj.SetActive(true);
        yield return new WaitForSeconds(1.5f);
        InitialValue();
    }
}
