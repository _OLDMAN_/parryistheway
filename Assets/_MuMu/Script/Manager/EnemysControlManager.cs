using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemysControlManager : MonoBehaviourSingleton<EnemysControlManager>
{
    [HideInInspector]
    public List<Melee01ActionManager> melee01s;
    [HideInInspector]
    public List<Melee02ActionManager> melee02s;
    [HideInInspector]
    public List<Range01ActionManager> range01s;
    public List<Range02ActionManager> range02s;
    public List<Fire03ActionManager> fire03s;

    public int maxAttacker = 2;

    private int canAttackEnemyCount { get { return melee01s.Count + melee02s.Count + range01s.Count + range02s.Count + fire03s.Count; } }
    public int currentAttackerCount;

    private void Update()
    {
        while(canAttackEnemyCount > 0 && currentAttackerCount < maxAttacker)
        {
            int count = 0;
            int r = -1;
            while (count == 0)
            {
                r = Random.Range(0, 5);
                if (r == 0)
                    count = melee01s.Count;
                else if (r == 1)
                    count = melee02s.Count;
                else if (r == 2)
                    count = range01s.Count;
                else if (r == 3)
                    count = range02s.Count;
                else if(r == 4)
                    count = fire03s.Count;
            }
            int num = Random.Range(0, count);
            if (r == 0)
                melee01s[num].Attack();
            else if (r == 1)
                melee02s[num].Attack();
            else if (r == 2)
                range01s[num].Attack();
            else if (r == 3)
                range02s[num].Attack();
            else if (r == 4)
                fire03s[num].Attack();
            currentAttackerCount++;
        }
    }
}
