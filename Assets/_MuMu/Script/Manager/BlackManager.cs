using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class BlackManager : MonoBehaviourSingleton<BlackManager>
{
    public Image black;
    public Image inkBlack;
    public bool fadeInOnAwke;
    public float blackFadeInSec;
    public float blackFadeOutSec;

    public override void Awake()
    {
        base.Awake();
        if (fadeInOnAwke)
            BlackFadeIn();
    }
    public void SetBlackInk()
    {
        Color temp = inkBlack.color;
        temp.a = 1;
        inkBlack.color = temp;
    }
    public void SetBlackPanel()
    {
        Color temp = black.color;
        temp.a = 1;
        black.color = temp;
    }
    private void Start()
    {
        inkBlack.material = Instantiate(inkBlack.material);
    }

    public void CloseInkPanel()
    {
        inkBlack.gameObject.SetActive(false);
    }

    public void BlackFadeOut()
    {
        black.DOFade(1, blackFadeOutSec).From(0);
    }

    public void BlackFadeOut(float time)
    {
        black.DOFade(1, time).From(0);
    }

    public void BlackFadeOut(float time, System.Action onComplete)
    {
        black.DOFade(1, time).From(0).OnComplete(()=> onComplete());
    }
    public void InkBlackFadeOut(float time)
    {
        inkBlack.gameObject.SetActive(true);
        inkBlack.material.SetFloat("_Fade", 1);
        inkBlack.material.DOFloat(0, "_InkControl", time).SetEase(Ease.Linear).From(1);
        SoundManager.Instance.PlaySound("Paint");
    }
    public void InkBlackFadeOut(float time, System.Action onComplete)
    {
        inkBlack.gameObject.SetActive(true);
        inkBlack.material.SetFloat("_Fade", 1);
        inkBlack.material.DOFloat(0, "_InkControl", time).SetEase(Ease.Linear).From(1).OnComplete(()=> onComplete());
        SoundManager.Instance.PlaySound("Paint");
    }

    public void BlackFadeOut(string sceneName)
    {
        black.DOFade(1, blackFadeOutSec).From(0).onComplete += ()=> SceneManager.LoadScene(sceneName);
    }

    public void BlackFadeIn()
    {
        black.DOFade(0, blackFadeInSec).From(1).SetEase(Ease.InCubic);
    }

    public void BlackFadeIn(float time)
    {
        black.DOFade(0, time).From(1).SetEase(Ease.InCubic);
    }

    public void BlackFadeIn(float time, System.Action onComplete)
    {
        black.DOFade(0, time).From(1).SetEase(Ease.InCubic).OnComplete(()=> onComplete());
    }

    public void BlackFadeIn(float time, float delayTime)
    {
        StartCoroutine(DelayBlack(time, delayTime));
    }

    private IEnumerator DelayBlack(float time, float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        black.DOFade(0, time).From(1).SetEase(Ease.InCubic);
    }
    public void InkBlackFadeIn(float time)
    {
        inkBlack.material.SetFloat("_Fade", 0);
        inkBlack.material.DOFloat(1, "_InkControl", time).SetEase(Ease.Linear).From(0).OnComplete(() => inkBlack.gameObject.SetActive(false));
        SoundManager.Instance.PlaySound("Paint");
    }

    public void InkBlackFadeIn(float time, float delayTime)
    {
        StartCoroutine(DelayInkBlack(time, delayTime));
        SoundManager.Instance.PlaySound("Paint");
    }

    private IEnumerator DelayInkBlack(float time, float delayTime)
    {
        yield return new WaitForSeconds(delayTime);
        inkBlack.material.SetFloat("_Fade", 0);
        inkBlack.material.DOFloat(1, "_InkControl", time).SetEase(Ease.Linear).From(0).OnComplete(() => inkBlack.gameObject.SetActive(false));
        SoundManager.Instance.PlaySound("Paint");
    }
}
