using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolRecycle : MonoBehaviour
{
    private ParticleSystem particle;
    public bool autoRecycle = true;
    public float recycleTime = 1f;

    private void OnEnable()
    {
        if (autoRecycle)
            StartCoroutine(DelayRecycle());
    }

    private void Start()
    {
        particle = GetComponent<ParticleSystem>();
    }

    public void Recycle(bool forceParticle = true)
    {
        if (forceParticle == true)
            ObjectPoolManager.Instance.Recycle(this, gameObject.name);
        else
            StartCoroutine(StopEmitting());
    }

    private IEnumerator DelayRecycle()
    {
        yield return new WaitForSeconds(recycleTime);
        ObjectPoolManager.Instance.Recycle(this, gameObject.name);
    }

    private IEnumerator StopEmitting()
    {
        particle.transform.parent = null;
        particle.Stop(true, ParticleSystemStopBehavior.StopEmitting);
        yield return new WaitForSeconds(2);
        ObjectPoolManager.Instance.Recycle(this, gameObject.name);
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
