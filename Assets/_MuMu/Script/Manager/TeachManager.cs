using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeachManager : MonoBehaviourSingleton<TeachManager>
{
    public bool isTeach = true;
    public bool isClear;
    public bool unLockMove = true;
    public bool unLockDash;
    public bool unLockAttackAndDashAttack;
    public bool unLockBlockAndBlockAttack;
    public bool unLockBlockBurst;
    public GameObject playerComboHint;

    public void SetPlayerComboHint(bool b)
    {
        playerComboHint.SetActive(b);
    }
}