using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleManager : MonoBehaviourSingleton<ParticleManager>
{
    public List<ParticleGroup> particleList = new List<ParticleGroup>();
    private PoolRecycle particleMono;

    public void PlayParticle(string particleName, Vector3 pos, Quaternion rotation, Transform parent = null, float delay = 0f)
    {
        Particle particle = ReadParticle(particleName);
        if (particle == null) return;
        if (delay == 0)
            DoPlayParticle(particle, pos, rotation, parent);
        else
            StartCoroutine(DelayPlayParticle(particle, pos, rotation, parent, delay));
    }
    public void PlayParticle(string particleName, Vector3 pos, Quaternion rotation, Vector3 scale, Transform parent = null)
    {
        Particle particle = ReadParticle(particleName);
        if (particle == null) return;
            DoPlayParticle(particle, pos, rotation, scale, parent);
    }

    public GameObject GetParticle(string particleName, Vector3 pos, Quaternion rotation, Transform parent = null)
    {
        Particle particle = ReadParticle(particleName);
        PoolRecycle p = ObjectPoolManager.Instance.Reuse(particle.prefab.GetComponent<PoolRecycle>(), particle.prefab.name + "(Clone)");
        if (p == null)
        {
            p = Instantiate(particle.prefab, pos, rotation, parent).GetComponent<PoolRecycle>();
        }
        else
        {
            if (parent != null)
                p.transform.parent = parent;
            p.transform.position = pos;
            p.transform.rotation = rotation;
        }

        return p.gameObject;
    }
    public GameObject GetParticle(string particleName, Vector3 pos, Quaternion rotation, Vector3 scale, Transform parent = null)
    {
        Particle particle = ReadParticle(particleName);
        PoolRecycle p = ObjectPoolManager.Instance.Reuse(particle.prefab.GetComponent<PoolRecycle>(), particle.prefab.name + "(Clone)");
        if (p == null)
        {
            p = Instantiate(particle.prefab, pos, rotation, parent).GetComponent<PoolRecycle>();
            p.transform.localScale = scale;
        }
        else
        {
            if (parent != null)
                p.transform.parent = parent;
            p.transform.position = pos;
            p.transform.rotation = rotation;
            p.transform.localScale = scale;
        }

        return p.gameObject;
    }

    public GameObject GetParticlePrefab(string particleName)
    {
        Particle particle = ReadParticle(particleName);
        return particle.prefab;
    }

    private IEnumerator DelayPlayParticle(Particle particle, Vector3 pos, Quaternion rotation, Transform parent, float delay)
    {
        yield return new WaitForSeconds(delay);
        particleMono = ObjectPoolManager.Instance.Reuse(particleMono, particle.prefab.name + "(Clone)");
        if (particleMono == null)
        {
            particleMono = Instantiate(particle.prefab, pos, rotation, parent).GetComponent<PoolRecycle>();
        }
        else
        {
            if (parent != null)
                particleMono.transform.parent = parent;
            particleMono.transform.position = pos;
            particleMono.transform.rotation = rotation;
        }
    }

    private void DoPlayParticle(Particle particle, Vector3 pos, Quaternion rotation, Transform parent)
    {
        particleMono = ObjectPoolManager.Instance.Reuse(particleMono, particle.prefab.name + "(Clone)");
        if (particleMono == null)
        {
            particleMono = Instantiate(particle.prefab, pos, rotation, parent).GetComponent<PoolRecycle>();
        }
        else
        {
            if (parent != null)
                particleMono.transform.parent = parent;
            particleMono.transform.position = pos;
            particleMono.transform.rotation = rotation;
        }
    }
    private void DoPlayParticle(Particle particle, Vector3 pos, Quaternion rotation, Vector3 scale, Transform parent = null)
    {
        particleMono = ObjectPoolManager.Instance.Reuse(particleMono, particle.prefab.name + "(Clone)");
        if (particleMono == null)
        {
            particleMono = Instantiate(particle.prefab, pos, rotation, parent).GetComponent<PoolRecycle>();
            particleMono.transform.localScale = scale;
        }
        else
        {
            if (parent != null)
                particleMono.transform.parent = parent;
            particleMono.transform.position = pos;
            particleMono.transform.rotation = rotation;
            particleMono.transform.localScale = scale;
        }
    }

    public Particle ReadParticle(string particleName)
    {
        foreach (ParticleGroup pg in particleList)
        {
            foreach (Particle p in pg.particles)
            {
                if (p.name == particleName)
                {
                    return p;
                }
            }
        }

        Debug.Log("Particle not found:" + particleName);
        return null;
    }
}

[System.Serializable]
public class Particle
{
    public string name;
    public GameObject prefab;
}

[System.Serializable]
public class ParticleGroup
{
    public string name;
    public List<Particle> particles;
}
