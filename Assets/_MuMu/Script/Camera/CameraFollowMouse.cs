using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.InputSystem;

public class CameraFollowMouse : MonoBehaviour
{
    private CinemachineVirtualCamera vcam;
    private CinemachineTransposer vcamTrans;
    private Vector3 oriOffect;
    public float lerpSpeed = 1f;
    public float sensitive = 1f;

    private Vector3 offset;

    private void Start()
    {
        vcam = GetComponent<CinemachineVirtualCamera>();
        vcamTrans = vcam.GetCinemachineComponent<CinemachineTransposer>();
        oriOffect = vcamTrans.m_FollowOffset;
        vcam.m_Follow = PlayerController.Instance.transform;
    }

    private void Update()
    {
        CountOffset();
    }

    public void CountOffset()
    {
        if (Gamepad.current != null)
        {
            Vector2 input = PlayerController.Instance.defaultInputAction.Player.Look.ReadValue<Vector2>();
            offset = new Vector3(input.x, 0, input.y);
        }
        else
        {
            Vector3 mousePos = Camera.main.ScreenToViewportPoint(Input.mousePosition);
            Vector3 cameraForward = Camera.main.transform.forward;
            cameraForward.y = 0;
            offset = Camera.main.transform.right * (mousePos.x - 0.5f) + cameraForward * (mousePos.y - 0.5f);
        }

        vcamTrans.m_FollowOffset = Vector3.Lerp(vcamTrans.m_FollowOffset, oriOffect + offset * sensitive, lerpSpeed * Time.deltaTime);
    }
}