using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundMono : MonoBehaviour
{
    private Sound Sound;

    public void MarkPlayingSound(Sound sound)
    {
        Sound = sound;
        Sound.playingNumber += 1;
    }

    private void EndPlayingSound()
    {
        Sound.playingNumber -= 1;
    }

    private void OnDisable()
    {
        EndPlayingSound();
    }
}