using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using DG.Tweening;
using UnityEngine.Audio;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : MonoBehaviourSingleton<SoundManager>
{
    public AudioMixerGroup mixerGroup;
    public List<SoundGroup> soundList = new List<SoundGroup>();
    private AudioSource audioSource;
    private AudioSource bgmAudioSource;
    private SoundMono soundMono;

    private AudioMixerSnapshot paused;
    private AudioMixerSnapshot unpaused;

    private Sound CurrentBGM;
    public float multiplePlayVolumeRestraint;

    private void Update()
    {
    }

    public override void Awake()
    {
        base.Awake();
        bgmAudioSource = GetComponent<AudioSource>();
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += PlayBGM;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= PlayBGM;
    }

    public void PlaySound(string soundName)
    {
        Sound sound = GetSound(soundName);
        if (sound == null) return;
        StartCoroutine(DelayPlaySound(sound, 0));
    }

    public void PlaySound(string soundName, float delay = 0f)
    {
        Sound sound = GetSound(soundName);
        if (sound == null) return;
        StartCoroutine(DelayPlaySound(sound, delay));
    }

    private IEnumerator DelayPlaySound(Sound sound, float delay)
    {
        yield return new WaitForSeconds(delay);
        soundMono = ObjectPoolManager.Instance.Reuse(soundMono, "SoundInstance");
        if (soundMono == null)
        {
            soundMono = new GameObject("SoundInstance").AddComponent<SoundMono>();
            audioSource = soundMono.gameObject.AddComponent<AudioSource>();
        }
        audioSource = soundMono.GetComponent<AudioSource>();

        SetAudioSource(sound);
        audioSource.Play();
        soundMono.MarkPlayingSound(sound);
        ObjectPoolManager.Instance.Recycle(soundMono, "SoundInstance", audioSource.clip.length);
    }

    public Sound GetSound(string soundName)
    {
        foreach (SoundGroup sg in soundList)
        {
            foreach (Sound s in sg.sounds)
            {
                if (s.name == soundName)
                {
                    return s;
                }
            }
        }

        Debug.Log("Sound not found " + soundName);
        return null;
    }

    public void SetAudioSource(Sound sound)
    {
        audioSource.clip = sound.clip;

        audioSource.volume = sound.volume / (1 + sound.playingNumber * multiplePlayVolumeRestraint);

        float randomness = Random.Range(-sound.pitchRandomness, sound.pitchRandomness);
        audioSource.pitch = sound.pitch + randomness;
        audioSource.outputAudioMixerGroup = mixerGroup;
    }

    private void PlayBGM(Scene scene, LoadSceneMode loadmode)
    {
        CurrentBGM = GetSound(scene.name);
        if (CurrentBGM == null) return;
        StartCoroutine(DoPlayBGM(CurrentBGM));
    }

    private IEnumerator DoPlayBGM(Sound s)
    {
        bgmAudioSource.clip = s.clip;
        bgmAudioSource.Play();

        bgmAudioSource.DOFade(0, 2);

        yield return new WaitForSeconds(2f);

        bgmAudioSource.DOFade(s.volume, 2).From(0);
    }

    private void CheckRepeatBGM()
    {
        if (!bgmAudioSource.isPlaying)
        {
            Scene s = SceneManager.GetActiveScene();
            PlayBGM(s, LoadSceneMode.Single);
        }
    }

    public void BackToMenu()
    {
        PlayerController.Instance.DestroySelf();
        LevelManager.Instance.DestroySelf();
        MainMenu.Instance.DestroySelf(); ;
        TeachManager.Instance.DestroySelf();
        RoomManager.Instance.DestroySelf();
        WaveManager.Instance.DestroySelf();
        GemManager.Instance.DestroySelf();
        BlackManager.Instance.DestroySelf();
        SceneManager.LoadScene("Menu");
    }
}

[System.Serializable]
public class Sound
{
    public string name;
    public AudioClip clip;

    [Range(0f, 1f)]
    public float volume = 0.5f;

    [Range(0.9f, 1.1f)]
    public float pitch = 1f;

    [Range(0f, 0.5f)]
    public float pitchRandomness = 0;

    [HideInInspector]
    public int playingNumber;
}

[System.Serializable]
public class SoundGroup
{
    public string name;
    public List<Sound> sounds;
}