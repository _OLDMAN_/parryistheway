using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    public static void SetPosX(this Transform t, float value)
    {
        Vector3 tempPos = t.position;
        t.position = new Vector3(value, tempPos.y, tempPos.z);
    }
    public static void SetPosY(this Transform t, float value)
    {
        Vector3 tempPos = t.position;
        t.position = new Vector3(tempPos.x, value, tempPos.z);
    }
    public static void SetPosZ(this Transform t, float value)
    {
        Vector3 tempPos = t.position;
        t.position = new Vector3(tempPos.x, tempPos.y, value);
    }
    public static void SetScaleX(this Transform t, float value)
    {
        Vector3 tempScale = t.localScale;
        t.localScale = new Vector3(value, tempScale.y, tempScale.z);
    }
    public static void SetScaleY(this Transform t, float value)
    {
        Vector3 tempScale = t.localScale;
        t.localScale = new Vector3(tempScale.x, value, tempScale.z);
    }
    public static void SetScaleZ(this Transform t, float value)
    {
        Vector3 tempScale = t.localScale;
        t.localScale = new Vector3(tempScale.x, tempScale.y, value);
    }
    public static void SetPosYZero(this Transform t)
    {
        Vector3 pos = t.position;
        t.position = new Vector3(pos.x, 0, pos.z);
    }
    public static Vector3 GetOnlyY(this Vector3 pos)
    {
        Vector3 temp = pos;
        temp.x = 0;
        temp.z = 0;
        return temp;
    }
    public static Vector3 GetZeroY(this Vector3 pos, float up = 0)
    {
        Vector3 temp = pos;
        temp.y = up;
        return temp;
    }

    public static string GetWeight(this Dictionary<string, int> dictionary)
    {
        int total = 0;
        foreach (int t in dictionary.Values)
            total += t;
        int ranNum = Random.Range(0, total + 1);
        int counter = 0;
        foreach(var temp in dictionary)
        {
            counter += temp.Value;
            if(ranNum <= counter)
            {
                return temp.Key;
            }
        }
        return "";
    }

    public static List<T> GetRandom<T>(this List<T> nums, int count)
    {
        if(count > nums.Count)
        {
            Debug.LogError("要取的個數大於數組長度");
            return null;
        }

        List<T> result = new List<T>();
        List<int> id = new List<int>();

        for(int i = 0; i < nums.Count; i++)
        {
            id.Add(i);
        }

        int r;
        while(id.Count > nums.Count - count)
        {
            r = Random.Range(0, id.Count);
            result.Add(nums[id[r]]);
            id.Remove(id[r]);
        }
        return result;
    }

    public static int[] GetRandomNum(this int[] num)
    {
        for (int i = 0; i < num.Length; i++)
        {
            int temp = num[i];
            int randomIndex = Random.Range(0, num.Length);
            num[i] = num[randomIndex];
            num[randomIndex] = temp;
        }
        return num;
    }
}
