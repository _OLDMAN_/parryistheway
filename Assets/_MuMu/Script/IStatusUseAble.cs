using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IStatusUseAble
{
    public bool canUse { get; set; }
    public void ResetUse();
    public void StopUse();
}
