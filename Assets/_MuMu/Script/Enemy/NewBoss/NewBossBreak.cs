using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NewBossBreak : MonoBehaviour, IBlockable
{
    private NewBossCtrl bossCtrl;
    private Rigidbody rb;

    public List<Transform> hands;
    public int breakCounter;
    public float dizzyTime = 3;
    private List<Transform> currentHands;

    public bool canBlock { get; set; }
    public Transform target { get; }

    public bool isBreak;

    private void Start()
    {
        bossCtrl = GetComponent<NewBossCtrl>();
        rb = GetComponent<Rigidbody>();
        //breakCounter = breakCount;
    }

    public void ReduceBreakCount()
    {
        breakCounter--;
        if (breakCounter <= 0)
            Break();
    }

    public void Break()
    {
        isBreak = true;
        rb.isKinematic = false;
        StartCoroutine(DoDizzy());
    }

    private IEnumerator DoDizzy()
    {
        yield return new WaitForSeconds(dizzyTime);
        rb.isKinematic = true;
        transform.DORotate(new Vector3(0, 360, 0), 0.5f, RotateMode.WorldAxisAdd);
        transform.DOMoveY(1, 0.5f).OnComplete(()=> {
            breakCounter = 6;
            Recycle();
        });
    }
    private void Recycle()
    {
        StartCoroutine(DelayRecycle());
    }
    private IEnumerator DelayRecycle()
    {
        int count = 6;
        while (count > 0)
        {
            count--;
            hands[count].GetComponent<NewBossBullet>().RecycleBackOri();
            yield return new WaitForSeconds(0.1f);
        }
        yield return new WaitForSeconds(2f);
        isBreak = false;
        bossCtrl.Attack();
    }
    public void ResetAllHand()
    {
        foreach (Transform hand in hands)
            hand.GetComponent<NewBossBullet>().ResetOri();
    }
    private void BreakOne()
    {
        currentHands.Clear();
        foreach (Transform hand in hands)
        {
            if (hand.GetComponent<NewBossBullet>().isBreak == false)
                currentHands.Add(hand);
        }
        int r = Random.Range(0, currentHands.Count);
        currentHands[r].GetComponent<NewBossBullet>().Block(gameObject);
    }
    public void Block(GameObject attacker)
    {
        ReduceBreakCount();
    }
}
