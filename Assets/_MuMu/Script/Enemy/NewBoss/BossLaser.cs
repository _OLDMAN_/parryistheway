using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossLaser : MonoBehaviour
{
    public int damage = 10;
    public float damageCD = 0.2f;
    private float timer;

    private void Update()
    {
        if (timer > 0)
            timer -= Time.deltaTime;
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("Player") && timer <= 0)
        {
            other.GetComponent<Hurt>().Dodeal(damage, transform.parent, true);
            timer = damageCD;
        }
    }
}
