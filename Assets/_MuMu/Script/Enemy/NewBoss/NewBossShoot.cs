using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBossShoot : MonoBehaviour
{
    private NewBossCtrl bossCtrl;
    private GameObject player;
    public List<Transform> hands;
    private List<Transform> currentHands = new List<Transform>();
    private float shootIntervals = 0.2f;
    private List<float[]> shootMode = new List<float[]>();

    private void Start()
    {
        player = GameObject.FindWithTag("Player");
        bossCtrl = GetComponent<NewBossCtrl>();
        shootMode.Add(new float[] { shootIntervals + 0.15f, shootIntervals, shootIntervals, shootIntervals, shootIntervals, shootIntervals });
        shootMode.Add(new float[] { shootIntervals, shootIntervals + 0.2f, shootIntervals, shootIntervals, shootIntervals, shootIntervals });
    }

    public void Shoot()
    {
        StartCoroutine(DoShoot());
    }
    private IEnumerator DoShoot()
    {
        //ParticleManager.Instance.PlayParticle("BossPreShoot", transform.position.GetZeroY(0.1f), Quaternion.LookRotation(GetPlayerDir()));
        //yield return new WaitForSeconds(1);
        GetCurrentHand();
        int count = 0;
        int randomMode = Random.Range(0, 2);
        float[] shootInterval = shootMode[randomMode];
        bool follow = randomMode == 1 ? true : false;
        while (count < currentHands.Count)
        {
            ParticleManager.Instance.PlayParticle("BossPreShoot", transform.position.GetZeroY(0.1f), Quaternion.identity);
            yield return new WaitForSeconds(0.1f);
            currentHands[count].GetComponent<NewBossBullet>().ShootPlayer(follow);
            yield return new WaitForSeconds(shootInterval[count]);
            count++;
        }
        yield return new WaitForSeconds(1);
        StartCoroutine(DelayRecycle());
    }

    private IEnumerator DelayRecycle()
    {
        GetCurrentHand();
        int count = currentHands.Count;
        if (count == 0) yield break;
        while (count > 0)
        {
            count--;
            currentHands[count].GetComponent<NewBossBullet>().ShootBackOri(50);
            yield return new WaitForSeconds(0.1f);
        }
        yield return new WaitForSeconds(0.5f);
        bossCtrl.Attack();
        //Shoot();
    }

    private void GetCurrentHand()
    {
        currentHands.Clear();
        foreach (Transform hand in hands)
        {
            if (hand.GetComponent<NewBossBullet>().isBreak == false)
                currentHands.Add(hand);
        }
    }

    private Vector3 GetPlayerDir()
    {
        return (player.transform.position.GetZeroY() - transform.position.GetZeroY()).normalized;
    }
}
