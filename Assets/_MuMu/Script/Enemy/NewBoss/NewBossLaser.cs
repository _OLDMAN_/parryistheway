using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NewBossLaser : MonoBehaviour
{
    private Animator animator;
    public GameObject laser;
    private NewBossBreak bossBreak;
    private NewBossStatus status;

    private void Start()
    {
        animator = GetComponent<Animator>();
        bossBreak = GetComponent<NewBossBreak>();
        status = GetComponent<NewBossStatus>();
    }

    public void StartLaser()
    {
        animator.enabled = true;
        animator.SetTrigger("Laser");
        StartCoroutine(LaserRotate());
    }

    private IEnumerator LaserRotate()
    {
        yield return new WaitForSeconds(1);
        laser.SetActive(true);
        transform.DORotate(new Vector3(0, 360, 0), 3, RotateMode.WorldAxisAdd);
    }

    public void StartAnimationLaser()
    {
        animator.enabled = true;
        animator.SetTrigger("Laser");
        StartCoroutine(AnimationRotate());
    }
    private IEnumerator AnimationRotate()
    {
        yield return new WaitForSeconds(1);
        laser.SetActive(true);
        transform.DORotate(new Vector3(0, -180, 0), 0.5f).SetEase(Ease.InCubic).OnComplete(()=> {
            GamefeelManager.Instance.ShakeCamera(1, 2.5f);
            BlackManager.Instance.BlackFadeOut(2, ()=> {
                laser.SetActive(false);
                animator.enabled = false;
                bossBreak.ResetAllHand();
                //status.StartStatusTwoAnimation();
            });
        });
    }
}
