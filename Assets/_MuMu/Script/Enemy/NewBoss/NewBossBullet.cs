using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NewBossBullet : MonoBehaviour, IBlockable
{
    public NewBossBreak bossBreak;
    private GameObject player;
    private Transform parent;
    private Vector3 oriPos;
    private Quaternion oriQuaternion;
    private Rigidbody rb;
    public GameObject flyParticle;

    public float moveSpeed = 50;

    public bool canBlock { get; set; }
    public Transform target { get { return transform; } }

    private bool isFlying;
    private bool canDamage;
    public bool isBreak;

    private void Start()
    {
        player = GameObject.FindWithTag("Player");
        Physics.IgnoreCollision(player.GetComponent<Collider>(), GetComponent<Collider>());
        rb = GetComponent<Rigidbody>();
        parent = transform.parent;
        oriPos = transform.localPosition;
        oriQuaternion = transform.localRotation;
    }
    public void ShootBackOri(float speed)
    {
        flyParticle.SetActive(false);
        transform.DOShakePosition(0.4f, 0.2f, 150).SetEase(Ease.InQuart).OnComplete(()=> {
            StartCoroutine(ShootBack(speed));
        });
    }
    private IEnumerator ShootBack(float speed)
    {
        isFlying = true;
        rb.isKinematic = true;
        while (Vector3.Distance(transform.position.GetZeroY(), oriPos.GetZeroY() + parent.position.GetZeroY()) > 0.3f)
        {
            Vector3 dir = (oriPos + parent.position - transform.position).GetZeroY().normalized;
            transform.forward = dir;
            transform.position += dir * speed * Time.deltaTime;
            yield return null;
        }
        transform.parent = parent;
        transform.localPosition = oriPos;
        transform.localRotation = oriQuaternion;
        isFlying = false;
    }

    public void ResetOri()
    {
        transform.parent = parent;
        transform.localPosition = oriPos;
        transform.localRotation = oriQuaternion;
    }

    public void ShootPlayer(bool follow)
    {
        StartCoroutine(DoFly(follow));
    }

    private IEnumerator DoFly(bool follow)
    {
        //Play Shoot SFX
        isFlying = true;
        canDamage = true;
        canBlock = true;
        transform.SetParent(null);

        Vector3 dir;
        if (PlayerController.Instance.input == Vector2.zero)
            dir = -GetPlayerDir();
        else
            dir = transform.position.GetZeroY() - HitPoint().GetZeroY();
        transform.forward = dir;
        flyParticle.SetActive(true);
        ParticleManager.Instance.PlayParticle("BossShoot", transform.position, transform.rotation);
        while (isFlying == true)
        {
            if(follow == true)
            {
                float angle = Vector3.Angle(-transform.forward.GetZeroY(), GetPlayerDir());
                float distance = Vector3.Distance(transform.position.GetZeroY(), player.transform.position.GetZeroY());
                if (angle < 10 && distance < 5)
                    transform.forward = Vector3.Lerp(transform.forward, -GetPlayerDir(), 5 * Time.deltaTime);
            }

            transform.position += -transform.forward * moveSpeed * Time.deltaTime;
            yield return null;
        }
    }

    public void RecycleBackOri()
    {
        rb.isKinematic = true;

        transform.DOMoveY(1f, 1f).OnComplete(()=> {
            StartCoroutine(DoRecycleBack());
        });
    }

    private IEnumerator DoRecycleBack()
    {
        Vector3 dir = (oriPos + parent.position - transform.position).GetZeroY().normalized;
        while (Vector3.Distance(transform.position.GetZeroY(), oriPos.GetZeroY() + parent.position.GetZeroY()) > 0.3f && Vector3.Dot(transform.forward, dir) > 0)
        {
            dir = (oriPos + parent.position - transform.position).GetZeroY().normalized;
            transform.forward = dir;
            transform.position += dir * 40 * Time.deltaTime;
            yield return null;
        }
        transform.parent = parent;
        transform.localPosition = oriPos;
        transform.localRotation = oriQuaternion;
        isBreak = false;
    }

    public void Block(GameObject attacker)
    {
        StopFly();
        flyParticle.SetActive(false);
        rb.isKinematic = false;
        float distance = Vector3.Distance(attacker.transform.position.GetZeroY(), transform.position.GetZeroY());
        Vector2 randPos = Random.insideUnitCircle * distance;
        Vector3 pos = attacker.transform.position.GetZeroY() + new Vector3(randPos.x, 0, randPos.y);
        while(Vector3.Angle(GetPlayerDir(), pos - transform.position.GetZeroY()) > 60)
        {
            randPos = Random.insideUnitCircle * distance;
            pos = attacker.transform.position.GetZeroY() + new Vector3(randPos.x, 0, randPos.y);
        }
        rb.AddExplosionForce(Random.Range(10, 15), pos, 10, 0, ForceMode.Impulse);
        isBreak = true;
        bossBreak.ReduceBreakCount();
    }

    private Vector3 GetPlayerDir()
    {
        return (player.transform.position.GetZeroY() - transform.position.GetZeroY()).normalized;
    }

    public Vector3 HitPoint()
    {
        Vector3 hitPoint = Vector3.zero;
        Vector3 D = transform.position - player.transform.position;
        Vector3 aircraftDirection = player.transform.TransformDirection(Vector3.forward);
        float THETA = Vector3.Angle(D, aircraftDirection);
        float DD = D.magnitude;
        float A = 1 - Mathf.Pow((moveSpeed / PlayerController.Instance.moveSpeed), 2);
        float B = -(2 * DD * Mathf.Cos(THETA * Mathf.Deg2Rad));
        float C = DD * DD;
        float DELTA = B * B - 4 * A * C;
        if (DELTA >= 0)
        {
            float F1 = (-B + Mathf.Sqrt(B * B - 4 * A * C)) / (2 * A);
            float F2 = (-B - Mathf.Sqrt(B * B - 4 * A * C)) / (2 * A);
            if (F1 < F2)
                F1 = F2;
            hitPoint = player.transform.position + aircraftDirection * F1;
        }
        return hitPoint;
    }


    private void StopFly()
    {
        isFlying = false;
        canBlock = false;
        canDamage = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Wall") && isFlying == true)
        {
            StopFly();
            ParticleManager.Instance.PlayParticle("BossBulletHitWall", other.ClosestPoint(transform.position), transform.rotation);
        }
        else if (other.CompareTag("Player") && canDamage == true)
        {
            other.GetComponent<Hurt>().Dodeal(10, transform, true);
            ParticleManager.Instance.PlayParticle("BossBulletHitPlayer", transform.position, transform.rotation);
        }
    }
}
