﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using DG.Tweening;

public enum BossStatus { FirstStage, SecondStage }
public class NewBossStatus : MonoBehaviour
{
    private BossManagerF ctrl;
    public BossComboEvent comboEvent;
    public BossHandF hand_LF;
    public BossHandF hand_RF;

    public BossStatus stage;
    public GameObject timeLineObj;
    public PlayableDirector playableDirector;
    public bool movieing;
    public Transform hand_L;
    public Transform hand_R;
    public GameObject ground;
    public GameObject newWater;
    public Transform playerNewPos;

    private void Start()
    {
        ctrl = GetComponent<BossManagerF>();
    }

    public void EnterNextStatus()
    {
        movieing = true;
        stage = BossStatus.SecondStage;
        PlayerController.Instance.canCtrl = false;
        PlayerController.Instance.animation.PlayAnimation(PlayerAni.Idle);
        ctrl.comboEvent.ResetEmpty();
        hand_LF.StopAction();
        hand_RF.StopAction();
        ctrl.shoot.Break();
        StartCoroutine(GoToNextStatus());
    }

    private IEnumerator GoToNextStatus()
    {
        //Boss歸正
        ctrl.bossBreak.Relax();
        ctrl.comboEvent.Relax(false);
        ctrl.ctrl.Relax(ctrl.bossBreak.recoverTime);

        yield return new WaitForSeconds(ctrl.bossBreak.recoverTime);
        //開啟TimeLine攝影機
        hand_L.parent = transform;
        hand_R.parent = transform;
        transform.DOMoveY(12, 1.5f);
        timeLineObj.SetActive(true);
        yield return new WaitForSeconds(2f);
        //等待攝影機過度
        playableDirector.Play();
        yield return new WaitForSecondsRealtime((float)playableDirector.duration);
        //boss下來
        comboEvent.ChangeAnimatorController(BossStatus.SecondStage);
        transform.DOMoveY(1.4f, 1.5f);
        timeLineObj.SetActive(false);
        yield return new WaitForSeconds(1.5f);
        hand_L.parent = null;
        hand_R.parent = null;
        PlayerController.Instance.canCtrl = true;
        movieing = false;
        yield return new WaitForSeconds(1.5f);
        ctrl.ctrl.StartAction();
    }

    public void SetStatus2Object()
    {
        ground.SetActive(false);
        newWater.SetActive(true);
        PlayerController.Instance.SetPos(playerNewPos.position);
    }
}
