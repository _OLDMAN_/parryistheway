using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBossCtrl : MonoBehaviour
{
    private NewBossBreak bossBreak;
    private NewBossShoot shoot;
    private NewBossLaser laser;
    private NewBossStatus status;

    private Vector3 sprintDir;
    private GameObject player;
    public float sprintSpeed = 15;
    public float sprintRotateSpeed = 10;
    private float _sprintSpeed;
    private float _sprintRotateSpeed;
    private bool isSprint;

    private int randSprintCount { get { return Random.Range(3, 6); } }
    private int sprintCounter;

    private void Start()
    {
        player = GameObject.FindWithTag("Player");
        sprintDir = GetPlayerDir();
        sprintCounter = randSprintCount;
        _sprintSpeed = sprintSpeed;
        _sprintRotateSpeed = sprintRotateSpeed;
        bossBreak = GetComponent<NewBossBreak>();
        shoot = GetComponent<NewBossShoot>();
        laser = GetComponent<NewBossLaser>();
        status = GetComponent<NewBossStatus>();
    }

    private void Update()
    {
        if (isSprint == true)
            Sprint();
    }

    public void Attack()
    {
        if(status.stage == BossStatus.FirstStage)
        {
            int r = Random.Range(0, 2);
            if (r == 0)
                StartSprint();
            else if (r == 1)
                shoot.Shoot();
        }
        else if(status.stage == BossStatus.SecondStage)
        {
            int r = Random.Range(0, 2);
            if (r == 0)
                laser.StartLaser();
            else if (r == 1)
                shoot.Shoot();
        }
    }

    private IEnumerator SlowDonw()
    {
        sprintDir = GetPlayerDir();
        float slowDownSec = 2;
        float perSpeed = sprintSpeed / slowDownSec;
        float perRotateSpeed = sprintRotateSpeed / slowDownSec;
        while (_sprintSpeed > 0)
        {
            _sprintSpeed -= Time.deltaTime * perSpeed;
            _sprintRotateSpeed -= Time.deltaTime * perRotateSpeed;
            transform.position += sprintDir * _sprintSpeed * Time.deltaTime;
            transform.Rotate(0, 0, _sprintRotateSpeed);
            yield return null;
        }
        Attack();
    }

    private void StartSprint()
    {
        isSprint = true;
        sprintCounter = randSprintCount;
        ChangeSprintDir();
        Sprint();
    }

    private void Sprint()
    {
        _sprintSpeed = Mathf.Lerp(_sprintSpeed, sprintSpeed, Time.deltaTime * 10);
        _sprintRotateSpeed = Mathf.Lerp(_sprintRotateSpeed, sprintRotateSpeed, Time.deltaTime * 10);
        transform.position += sprintDir * _sprintSpeed * Time.deltaTime;
        transform.Rotate(0, 0, _sprintRotateSpeed);
    }

    private void ChangeSprintDir()
    {
        Vector3 dir;
        if (PlayerController.Instance.input == Vector2.zero)
            dir = GetPlayerDir();
        else
            dir = (HitPoint().GetZeroY() - transform.position.GetZeroY()).normalized;
        sprintDir = dir;
    }

    private void HitWall()
    {
        sprintCounter--;
        if (sprintCounter <= 0)
        {
            isSprint = false;
            StartCoroutine(SlowDonw());
        }
        else
            ChangeSprintDir();
    }

    private Vector3 GetPlayerDir()
    {
        return (player.transform.position.GetZeroY() - transform.position.GetZeroY()).normalized;
    }

    public void StopAllCorutine()
    {
        isSprint = false;
        StopAllCoroutines();
        shoot.StopAllCoroutines();
        bossBreak.StopAllCoroutines();
        bossBreak.ResetAllHand();
    }

    public Vector3 HitPoint()
    {
        Vector3 hitPoint = Vector3.zero;
        Vector3 D = transform.position - player.transform.position;
        Vector3 aircraftDirection = player.transform.TransformDirection(Vector3.forward);
        float THETA = Vector3.Angle(D, aircraftDirection);
        float DD = D.magnitude;
        float A = 1 - Mathf.Pow((sprintSpeed / PlayerController.Instance.moveSpeed), 2);
        float B = -(2 * DD * Mathf.Cos(THETA * Mathf.Deg2Rad));
        float C = DD * DD;
        float DELTA = B * B - 4 * A * C;
        if (DELTA >= 0)
        {
            float F1 = (-B + Mathf.Sqrt(B * B - 4 * A * C)) / (2 * A);
            float F2 = (-B - Mathf.Sqrt(B * B - 4 * A * C)) / (2 * A);
            if (F1 < F2)
                F1 = F2;
            hitPoint = player.transform.position + aircraftDirection * F1;
        }
        return hitPoint;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Wall") && isSprint == true)
            HitWall();
        else if (other.CompareTag("Player"))
            other.GetComponent<Hurt>().Dodeal(10, transform, true);
    }
}
