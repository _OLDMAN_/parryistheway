using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBossDie : MonoBehaviour
{
    public bool isDead;

    public void Die()
    {
        if (isDead == true) return;
        isDead = true;
        StartCoroutine(StartDeadProcess());
    }

    private IEnumerator StartDeadProcess()
    {
        PlayerController.Instance.canCtrl = false;
        GamefeelManager.Instance.StartSlowTime(0.15f, 1.5f);
        yield return new WaitForSecondsRealtime(1.5f);
        PlayerController.Instance.animation.PlayAnimation(PlayerAni.Idle);
        BossManager.Instance.BossDead();
    }
}
