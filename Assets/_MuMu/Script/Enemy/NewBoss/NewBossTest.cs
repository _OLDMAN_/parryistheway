using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NewBossTest : MonoBehaviour
{
    private Vector3 sprintDir;
    private GameObject player;
    public float sprintSpeed = 5;

    public LayerMask shootObsLayer;

    private void Start()
    {
        player = GameObject.FindWithTag("Player");
        sprintDir = GetPlayerDir();
    }

    private Vector3 GetPlayerDir()
    {
        return (player.transform.position.GetZeroY() - transform.position.GetZeroY()).normalized;
    }
    private Vector3 GetPlayerDir(Vector3 pos)
    {
        return (player.transform.position.GetZeroY() - pos.GetZeroY()).normalized;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Wall"))
            sprintDir = GetPlayerDir();
        else if (other.CompareTag("Player"))
        {
            other.GetComponent<Hurt>().Dodeal(10, transform, true);
        }
    }
}
