using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using DG.Tweening;

public class BossHandF : MonoBehaviour
{
    public BossManagerF bossManager;
    public NewBossStatus status;
    public Transform warningSpot;
    private Animator animator;
    private Transform player;
    private Rigidbody rb;
    public Vector3 oriOffset;
    private Vector3 oriRot = new Vector3(0, 180, 0);

    public LayerMask hitLayer;
    public AnimationCurve shakeCurve;
    public float maxReachDistance;

    [Header("Short")]
    public float shortWaitSec = 0.5f;
    public float shortDamageRadius = 5;
    public int shortDamage = 10;
    [Header("Long")]
    public float longWaitSec = 2f;
    public float longDamageRadius = 8;
    public int longDamage = 25;

    private const float FALLTIME = 10;
    private bool isFalling;
    private Vector3 targetDirDelta;
    private bool shortHit;
    private bool withCircle;
    private Tween downTween;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        animator = GetComponent<Animator>();
        player = GameObject.FindWithTag("Player").transform;
    }

    public void Relax(float time)
    {
        transform.DOMove(bossManager.ctrl.oriPos + oriOffset, time);
        transform.DORotate(oriRot, time);
    }
    public void StopAction()
    {
        StopAllCoroutines();
        downTween.Kill();
        CancelInvoke();
        animator.SetBool("Break", false);
        animator.ResetTrigger("Hit");
        animator.ResetTrigger("HitCircle");
        animator.Play("Idle");
        animator.speed = 1;
        isFalling = false;
        rb.isKinematic = true;
    }
    public void Break()
    {
        isFalling = false;
        animator.SetBool("Break", true);
        StopAllCoroutines();
        DOTween.Kill(gameObject);
        CancelInvoke();
        animator.speed = 1;
        rb.isKinematic = false;
    }
    public void BreakRecover(float time)
    {
        animator.SetBool("Break", false);
        rb.isKinematic = true;
        Relax(time);
    }

    public void HitTargetPointCircle()
    {
        animator.SetTrigger("HitCircle");
        shortHit = false;
        withCircle = true;
    }

    public void HitTargetPoint(bool shortHit)
    {
        animator.SetTrigger("Hit");
        this.shortHit = shortHit;
        withCircle = false;
    }

    public void FallFrame()
    {
        animator.speed = 0;
        float waitSec = shortHit ? shortWaitSec : longWaitSec;
        if (shortHit)
        {
            ParticleManager.Instance.PlayParticle("BossHandWarningShort", warningSpot.position, warningSpot.rotation, warningSpot);
            SoundManager.Instance.PlaySound("BossHandWarningShort");
        }
        else
        {
            ParticleManager.Instance.PlayParticle("BossHandWarningLong", warningSpot.position, warningSpot.rotation, warningSpot);
            SoundManager.Instance.PlaySound("BossHandWarningLong");
        }

        transform.DOShakePosition(waitSec, 0.5f, 80).SetEase(shakeCurve);
        Invoke("FallDown", waitSec);
    }

    private void FallDown()
    {
        animator.speed = 1;

        Vector3 landPoint = player.transform.position.GetZeroY();
        Vector3 vector = landPoint - transform.position.GetZeroY();
        if(Vector3.Magnitude(vector) > maxReachDistance)
        {
            Vector3 dir = vector.normalized;
            landPoint = transform.position.GetZeroY() + dir * maxReachDistance;
        }
        downTween = transform.DOMove(landPoint, (FALLTIME - 1) / 30).OnComplete(() => {
            if (withCircle == true)
                SpawnCircle();
        });
        transform.rotation = Quaternion.LookRotation(player.position.GetZeroY() - transform.position.GetZeroY());
        isFalling = true;
    }

    public void HitFrame()
    {
        if (isFalling == false) return;
        GamefeelManager.Instance.ShakeCamera(3, 0.2f);
        SoundManager.Instance.PlaySound("BossHitWater");
        ParticleManager.Instance.PlayParticle("BossHitWater", transform.position.GetZeroY(0.1f), Quaternion.identity);
        isFalling = false;
        float damageRadius = shortHit ? shortDamageRadius : longDamageRadius;
        int damage = shortHit ? shortDamage : longDamage;
        DamageCircle(damageRadius, damage);
        if (status.stage == BossStatus.FirstStage)
            bossManager.ctrl.SpawnAwl();
    }

    private void SpawnCircle()
    {
        ParticleManager.Instance.PlayParticle("BossBombCircle", transform.position.GetZeroY(0.1f), Quaternion.identity);
        StartCoroutine(DuringAttack(transform.position.GetZeroY()));
    }

    private IEnumerator DuringAttack(Vector3 landPoint)
    {
        int count = 1;
        float innerRadius = 3f;
        float outRadius = 4f;
        while (count < 3)
        {
            yield return new WaitForSeconds(2f);
            DamageBoderCircle(innerRadius * count, outRadius * count, longDamage , landPoint);
            count++;
        }
    }

    private void DamageCircle(float radius, int damage)
    {
        Collider[] colliders = Physics.OverlapSphere(transform.position.GetZeroY(), radius, hitLayer);
        foreach (Collider collider in colliders)
        {
            IDestroyable destroyable = collider.GetComponent<IDestroyable>();
            destroyable?.Damage();
            if (collider.CompareTag("Player"))
                collider.GetComponent<PlayerActionManager>().StartHurt(damage, transform);
        }
    }
    private void DamageBoderCircle(float innerRadius, float outRadius, int damage , Vector3 center)
    {
        SoundManager.Instance.PlaySound("CircleExplode");

        Collider[] outColliders = Physics.OverlapSphere(center, outRadius, hitLayer);
        Collider[] innerColliders = Physics.OverlapSphere(center, innerRadius, hitLayer);

        foreach(Collider collider in outColliders.Except(innerColliders))
        {
            Debug.Log("circle" + collider.name);
            IDestroyable destroyable = collider.GetComponent<IDestroyable>();
            destroyable?.Damage();
            if (collider.CompareTag("Player"))
                collider.GetComponent<PlayerActionManager>().StartHurt(damage, transform);
        }
    }

    private void OnAnimatorMove()
    {
        Vector3 deltaPos = animator.deltaPosition;
        if (isFalling == true)
        {
            transform.position += deltaPos.GetOnlyY();
            return;
        }
        else
            transform.position += deltaPos;
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position,shortDamageRadius);
    }
}
