using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class NewBossHealth : EnemyHealth, IHurtable, IBreakable
{
    public RectTransform canvas; 
    private Animator animator;
    private BossBreakF bossBreak;
    private BossDeadF bossDie;
    private NewBossStatus status;
    private List<Material> materials = new List<Material>();

    public int maxHurtCount = 4;
    public int hurtCounter { get; set; }
    public int breakCounter { get; set; }

    public int noShieldBounsDamage = 100;
    public float damageFontSize = 1.5f;

    public bool superArmor;

    private void Start()
    {
        hurtCounter = maxHurtCount;
        animator = GetComponent<Animator>();
        bossDie = GetComponent<BossDeadF>();
        status = GetComponent<NewBossStatus>();
        bossBreak = GetComponent<BossBreakF>();
        Renderer[] renderer = GetComponentsInChildren<Renderer>();
        foreach (Renderer r in renderer)
            materials.Add(r.material);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad6))
            Damage(transform, (int)(maxHP / 2), true);
    }
    public void Damage(Transform attacker, int damage, bool heavyDamage)
    {
        if (status.movieing == true || bossDie.die == true) return;
        if (bossBreak.isBreak == true)
        {
            damage = Mathf.RoundToInt(damage * (1 + noShieldBounsDamage / 100f));
        }
        else
        {
            ParticleManager.Instance.PlayParticle("EnemyShield", transform.position + Vector3.down * 3, Quaternion.identity, Vector3.one * 3, transform);
        }
        HP -= damage;
        Blink();
        SpawnDamageText(damage, heavyDamage);
        if (HP <= 0)
            bossDie.Die();
        else if (HP <= maxHP / 2 && status.stage == BossStatus.FirstStage)
        {
            GamefeelManager.Instance.BossLastKill();
            status.EnterNextStatus();
            HP = maxHP / 2;
        }
        else if (bossBreak.breakCounter <= 0)
        {
            Break();
            if (hurtCounter <= 0)
                hurtCounter = maxHurtCount;
        }
        else if (hurtCounter <= 0)
        {
            animator.SetTrigger("Hurt");
            hurtCounter = maxHurtCount;
        }
        info.healthImage.fillAmount = HP / maxHP;
    }
    public void ReduceHurtCount(int count)
    {
        if (bossBreak.isBreak == true || superArmor == true) return;
        hurtCounter -= count;
    }
    public void ReduceBreakCount(int count)
    {
        if (bossBreak.isBreak == true) return;
        bossBreak.breakCounter -= count;
    }
    public void Break()
    {
        ParticleManager.Instance.PlayParticle("EnemyShieldBreak", transform.position + Vector3.down * 3, Quaternion.identity, Vector3.one * 3, transform);
        bossBreak.Break();
    }
    //�C���ܤ�
    private void Blink()
    {
        float HurtSec = 0.2f;
        foreach (Material m in materials)
        {
            if (m.HasProperty("_Sprint"))
                m.DOFloat(1f, "_Sprint", HurtSec / 4).onComplete += () => m.DOFloat(0, "_Sprint", HurtSec / 4).onComplete += () => m.DOFloat(1f, "_Sprint", HurtSec / 4).onComplete += () => m.DOFloat(0, "_Sprint", HurtSec / 4);
        }
    }
    public void KnockBack(Vector3 dir, float distance)
    {

    }
    protected override void SpawnDamageText(float value, bool heavyAttack = false)
    {
        float absValue = Mathf.Abs(value);
        Vector3 spawnPos = transform.position + damageSpawnOffset;

        DamageText damageText = Instantiate(info.damageTextPrefab, spawnPos, info.canvas.transform.rotation, canvas.transform).GetComponent<DamageText>();

        Vector3 effectPos = spawnPos + new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(0, 0.5f), 0);
        damageText.TextEffect(effectPos, heavyAttack);
        damageText.tmpText.fontSize = damageFontSize;
        damageText.tmpText.text = absValue.ToString();
    }
}
