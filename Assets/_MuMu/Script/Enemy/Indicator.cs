using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Indicator : MonoBehaviour
{
    public void StartIndicat(float time)
    {
        StartCoroutine(DoMaterial(time));
        //transform.GetChild(0).GetComponent<ParticleSystemRenderer>().material.DOFloat(0.5f, "_Second", time).From(0);
    }
    private IEnumerator DoMaterial(float time)
    {
        float perSpeed = 0.5f / time;
        float timer = 0;
        var m = transform.GetChild(0).GetComponent<ParticleSystemRenderer>().material;
        m.SetFloat("_Second", 0);
        while (timer < time)
        {
            timer += Time.deltaTime * GamefeelManager.stopSpeed;
            m.SetFloat("_Second", timer * perSpeed);
            yield return null;
        }
    }
}
