using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BossHandAni { Idle, Hit, HitRecover, Break }

public class BossHandAnimation : MonoBehaviour
{
    private Animator animator;
    private string aniName = "Idle";
    private float blendEndTime = 0;
    private float blendTimeElapsed = 1;

    public float movePercent { get; set; }
    public bool isBlending { get; private set; }

    //Inspector variables
    [Tooltip("Time to blend to new animation,percentage 0~1")]
    public float blendTime = 0.1f;

    public List<AnimationHolder> animationholders = new List<AnimationHolder>();

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        BlendAnimation();
    }

    /// <param name="blendPlay">crossfade the animation.</param>
    /// <param name="skipWhenRepeat">don't play the animation again when current animation is the same one as desired animation.</param>
    public void PlayAnimation(BossHandAni name, bool skipWhenRepeat = true, bool blendPlay = true)
    {
        string n = name.ToString();
        if (aniName == n && skipWhenRepeat) return;

        aniName = n;

        if (blendPlay)
        {
            StartBlend();
        }
        else
        {
            ForcePlay();
        }
    }

    private void ForcePlay()
    {
        movePercent = GetAnimationHolder(aniName).MovePercent;
        blendTimeElapsed = 0;
        blendEndTime = 0;
        animator.Play(aniName);
        isBlending = false;
    }

    private void StartBlend()
    {
        movePercent = GetAnimationHolder(aniName).MovePercent;
        blendTimeElapsed = 0;
        blendEndTime = GetAnimationHolder(aniName).Length * blendTime;
        animator.CrossFadeInFixedTime(aniName, blendEndTime);
        isBlending = true;
    }

    private void BlendAnimation()
    {
        if (blendTimeElapsed < blendEndTime)
        {
            blendTimeElapsed += Time.deltaTime;
            return;
        }
        EndBlend();
    }

    private void EndBlend()
    {
        animator.Play(aniName);
        isBlending = false;
    }

    private float Remap(float value, float from1, float to1, float from2, float to2)
    {
        return to2 - (value - from1) / (to1 - from1) * (to2 - from2);
    }

    public AnimationHolder GetAnimationHolder(string name)
    {
        for (int i = 0; i < animationholders.Count; i++)
        {
            if (animationholders[i].name == name)
            {
                return animationholders[i];
            }
        }
        return null;
    }
}
