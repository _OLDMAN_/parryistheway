using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BossFlyHand : MonoBehaviour, IBlockable
{
    public BossFlyAttack flyAttack;

    private Vector3 oriIdlePos;
    private Vector3 enterPos;
    private Transform player;

    public bool isRight;
    public AnimationCurve normalAttackCurve;
    public int damage;
    private int waveCount;
    private float waitTimer;
    private bool canAttack;
    public bool waveEnd;

    private bool isFlyAttack;
    public Transform target { get; set; }
    public bool canBlock { get; set; } = false;
    private Tween flyAttackTween;
    private int currentSkillDamage;
    private GameObject handFlyParticle;

    private void Start()
    {
        target = transform;
        player = GameObject.FindWithTag("Player").transform;
        oriIdlePos = transform.position;
        enterPos = transform.position + transform.forward * 13;
    }

    public void StartAttack(int wave, float waitTime)
    {
        waitTimer = waitTime;
        waveCount = wave;
        waveEnd = false;
        Enter();
    }

    public void Enter()
    {
        transform.DOMove(enterPos, 2f).From(oriIdlePos).OnComplete(()=> {
            FlyAttack();
        });
    }

    public void Exit()
    {
        transform.DORotateQuaternion(Quaternion.LookRotation(enterPos - transform.position), 0.5f).OnComplete(()=> {
            transform.DOMove(enterPos, 1f).OnComplete(() => {
                transform.DOLocalRotate(Vector3.zero, 0.5f).OnComplete(()=> {
                    transform.DOMove(oriIdlePos, 1f).OnComplete(()=> {
                        waveEnd = true;
                    });
                });
            });
        });
        
    }

    private IEnumerator RandAttack()
    {
        yield return new WaitForSeconds(waitTimer);
        int r = Random.Range(0, 4);
        if (r == 0)
            FlyAttack();
        else if (r == 1)
            NormalAttack();
        else if (r == 2)
            WalkAttack();
        else if (r == 3)
            HeavyAttack();
    }

    public void NormalAttack()
    {
        currentSkillDamage = flyAttack.normalHitDamage;
        StartCoroutine(DoNormalAttack());
    }

    private IEnumerator DoNormalAttack()
    {
        while (GetPlayerDistance(transform.position) > 3 || !(Vector3.Dot(transform.forward, GetPlayerDir(transform.position)) > 0 && Vector3.Angle(transform.forward, GetPlayerDir(transform.position)) < 10))
        {
            Vector3 moveDir = GetPlayerDir(transform.position);
            if(GetPlayerDistance(transform.position) > 3)
                transform.position += moveDir * Time.deltaTime * 12;
            if (Vector3.Dot(transform.forward, GetPlayerDir(transform.position)) < 0 || Vector3.Angle(transform.forward, GetPlayerDir(transform.position)) > 5)
                transform.forward = Vector3.Lerp(transform.forward, moveDir, Time.deltaTime * 20);
            yield return null;
        }

        transform.DOMove(transform.position - transform.forward, 0.6f).onComplete += () => {
            canAttack = true;
            transform.DOMove(transform.position + transform.forward, 0.4f).SetEase(normalAttackCurve).onComplete += () => {
                AttatkEnd();
            };
        };
    }

    public void WalkAttack()
    {
        currentSkillDamage = flyAttack.walkDamage;
        StartCoroutine(DoWalkAttack());
    }

    public IEnumerator DoWalkAttack()
    {
        Vector3 dir = GetPlayerDir(transform.position);
        dir.y = 0;
        dir.Normalize();
        Physics.BoxCast(transform.position, transform.GetComponent<BoxCollider>().size, dir, out RaycastHit hit, transform.rotation, 100, LayerMask.GetMask("Wall"));
        while (!(Vector3.Dot(transform.forward, GetPlayerDir(transform.position)) > 0 && Vector3.Angle(transform.forward, GetPlayerDir(transform.position)) < 10))
        {
            Vector3 moveDir = GetPlayerDir(transform.position);
            transform.forward = Vector3.Lerp(transform.forward, moveDir, Time.deltaTime * 15);
            yield return null;
        }
        yield return new WaitForSeconds(0.5f);
        int jumpCount = Mathf.RoundToInt(hit.distance / 3f);
        if (jumpCount == 0)
        {
            transform.DOLocalRotate(new Vector3(-35, transform.localEulerAngles.y, transform.localEulerAngles.z), 0.4f).onComplete += () => { transform.DOLocalRotate(new Vector3(0, transform.localEulerAngles.y, transform.localEulerAngles.z), 0.2f); };
            transform.DOJump(transform.position, 2, 1, 0.6f).SetEase(Ease.InQuart).OnComplete(() => {
                canAttack = true;
                GamefeelManager.Instance.ShakeCamera(3f, 0.15f);
                ParticleManager.Instance.PlayParticle("BossHitWater", new Vector3(transform.position.x, 0, transform.position.z) + transform.forward * 0.5f, Quaternion.identity);
            });
            jumpCount--;
            yield return null;
            canAttack = false;
        }
        while (jumpCount > 0)
        {
            transform.DOLocalRotate(new Vector3(-35, transform.localEulerAngles.y, transform.localEulerAngles.z), 0.4f).onComplete += () => { transform.DOLocalRotate(new Vector3(0, transform.localEulerAngles.y, transform.localEulerAngles.z), 0.2f); };
            transform.DOJump(transform.position + transform.forward * 3f, 2, 1, 0.6f).SetEase(Ease.InQuart).OnComplete(() => {
                canAttack = true;
                GamefeelManager.Instance.ShakeCamera(3f, 0.15f);
                ParticleManager.Instance.PlayParticle("BossHitWater", new Vector3(transform.position.x, 0, transform.position.z) + transform.forward * 0.5f, Quaternion.identity);
            });
            jumpCount--;
            yield return null;
            canAttack = false;
            yield return new WaitForSeconds(1f);
        }
        AttatkEnd();
    }

    public void HeavyAttack()
    {
        transform.DORotateQuaternion(Quaternion.LookRotation(GetPlayerDir(transform.position)), 0.5f).OnComplete(() => {
            transform.DOLocalRotate(new Vector3(90, transform.localEulerAngles.y, isRight ? 90:-90), 1f);
            transform.DOLocalMoveY(5, 1f).onComplete += () => { StartCoroutine(DoHeavyAttack()); };
        });
    }

    private IEnumerator DoHeavyAttack()
    {
        int count = 3;
        while (count > 0)
        {
            transform.DOLocalMoveY(2, 0.3f).SetEase(Ease.OutBack).onComplete += () => transform.DOLocalMoveY(5, 0.7f);
            yield return new WaitForSeconds(0.15f);
            GamefeelManager.Instance.ShakeCamera(3, 0.2f);
            ParticleManager.Instance.PlayParticle("BossHitWater", new Vector3(transform.position.x, 1, transform.position.z), Quaternion.identity);
            GameObject o = ParticleManager.Instance.GetParticle("BossFlyHeavyAttack", new Vector3(transform.position.x, 1, transform.position.z), Quaternion.LookRotation(isRight ? transform.right : -transform.right));
            o.GetComponent<FlyHeavyBullet>().damage = flyAttack.waterDamage;
            yield return new WaitForSeconds(0.85f);
            count--;
        }
        transform.DOLocalMoveY(2, 0.3f);
        transform.DORotateQuaternion(Quaternion.LookRotation(isRight ? transform.right : -transform.right), 0.3f).onComplete += () => AttatkEnd();
    }

    private void FlyAttack()
    {
        currentSkillDamage = flyAttack.flyDamage;
        StartCoroutine(RotateLerpToPlayer());
    }

    private IEnumerator RotateLerpToPlayer()
    {
        Vector3 dir;
        while (true)
        {
            dir = player.position - transform.position;
            dir.y = 0;
            transform.forward = Vector3.Lerp(transform.forward, dir, Time.deltaTime);
            if (Vector3.Angle(dir, transform.forward) < 2 && Vector3.Dot(dir, transform.forward) > 0)
                break;
            yield return null;
        }

        Vector3 pos = transform.position;
        pos.y = 0.1f;
        GameObject p = ParticleManager.Instance.GetParticle("AttackIndicator", pos, Quaternion.LookRotation(dir));
        p.transform.GetChild(0).GetComponent<ParticleSystemRenderer>().material.DOFloat(0.5f, "_Second", 1f).From(0).OnComplete(() => {
            GameObject pf = ParticleManager.Instance.GetParticle("AttackIndicatorBurst", pos, Quaternion.LookRotation(dir));
            pf.GetComponent<IndicatorFlash>().Flash(2);
            p.GetComponent<PoolRecycle>().Recycle();
            StartCoroutine(DoFlyAttack(dir));
        });
    }

    private IEnumerator DoFlyAttack(Vector3 dir)
    {
        yield return new WaitForSeconds(0.4f);
        canBlock = true;
        isFlyAttack = true;
        canAttack = true;
        handFlyParticle = ParticleManager.Instance.GetParticle("BossHandFlyAttack", transform.position, transform.rotation, transform);
        Physics.BoxCast(transform.position, transform.GetComponent<BoxCollider>().size, dir, out RaycastHit hit, transform.rotation, 100, LayerMask.GetMask("Wall"));
        if (hit.collider != null)
            flyAttackTween = transform.DOMove(transform.position + transform.forward * (hit.distance + 1f), 0.37f).SetEase(Ease.OutQuart).OnComplete(()=>{
                handFlyParticle.GetComponent<PoolRecycle>().Recycle(false);
                handFlyParticle = null;
                AttatkEnd();
                canAttack = false;
                isFlyAttack = false;
                canBlock = false;
            });
    }

    public void AttatkEnd()
    {
        canAttack = false;
        waveCount--;
        if (waveCount > 0)
            StartCoroutine(RandAttack());
        else
            Exit();
    }

    public void Block(GameObject attacker)
    {
        flyAttackTween.Kill();
        handFlyParticle.GetComponent<PoolRecycle>().Recycle(false);
        KnockBack(true);
        canAttack = false;
        isFlyAttack = false;
        canBlock = false;
    }

    private void KnockBack(bool block = false)
    {
        GamefeelManager.Instance.ShakeCamera(3, 0.2f);
        transform.DOMove(transform.position - transform.forward * 3, 0.3f).OnComplete(()=> {
            if (block)
                ReduceWaveCount();
            else
                AttatkEnd();
        });
    }

    private void ReduceWaveCount()
    {
        waveCount -= 3;
        AttatkEnd();
    }

    private Vector3 GetPlayerDir(Vector3 startPos)
    {
        Vector3 dir = player.position - startPos;
        dir.y = 0;
        return dir.normalized;
    }

    private float GetPlayerDistance(Vector3 startPos)
    {
        startPos.y = 0;
        Vector3 playerPos = player.transform.position;
        playerPos.y = 0;
        return Vector3.Distance(startPos, playerPos);
    }

    private void OnTriggerStay(Collider other)
    {
        if (canAttack == false) return;

        if (other.CompareTag("Player"))
        {
            other.GetComponent<PlayerActionManager>().StartHurt(currentSkillDamage, transform.forward, true);
            flyAttackTween.Kill();
            handFlyParticle.GetComponent<PoolRecycle>().Recycle(false);
            if (isFlyAttack == true)
            {
                KnockBack();
                isFlyAttack = false;
            }
            canAttack = false;
            canBlock = false;
        }
    }
}
