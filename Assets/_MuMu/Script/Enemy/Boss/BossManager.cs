using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
using UnityEngine.SceneManagement;

public class BossManager : MonoBehaviourSingleton<BossManager>
{
    public ParticleSystem deadParticle;
    public GameObject endLookBossCM;

    public void RestartBossRoom()
    {
        BlackManager.Instance.BlackFadeOut(1.5f, () => {
            PlayerHP.Instance.ResetValue();
            SceneManager.LoadScene("Boss");
        });
    }

    public void BossDead()
    {
        StartCoroutine(DoBossDead());
    }

    private IEnumerator DoBossDead()
    {
        yield return new WaitForSeconds(0.5f);
        endLookBossCM.SetActive(true);
        yield return new WaitForSeconds(2.3f);
        deadParticle.Play();
        yield return new WaitForSeconds(2f);
        GamefeelManager.Instance.ShakeCamera(4f, 0.5f);
        yield return new WaitForSeconds(0.5f);
        BlackManager.Instance.BlackFadeOut(1f, ()=> {
            PlayerHP.Instance.ResetValue();
            FindObjectOfType<BossBlackDialogue>().StartDialogue();
        });
    }
}
