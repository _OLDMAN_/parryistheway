using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ShotMode
{
    A,
    B,
    C,
    D,
    E
}

public class BossShotAttack : MonoBehaviour
{
    private BossCtrl bossCtrl;
    private BossFlyAttack bossFly;
    public GameObject bulletPrefab;
    public Transform muzzle;
    private GameObject player;

    public ShotMode shotMode;
    public int attackWave;
    public float waveWaitTime;
    private int waveCount;

    public ShotMode01 shotMode01;
    public ShotMode02 shotMode02;
    public ShotMode03 shotMode03;
    public ShotMode04 shotMode04;
    public ShotMode05 shotMode05;

    private void Start()
    {
        player = GameObject.FindWithTag("Player");
        bossCtrl = GetComponent<BossCtrl>();
        bossFly = GetComponent<BossFlyAttack>();
        
    }

    public void StopAllAction()
    {
        StopAllCoroutines();
    }

    public void StartAttack()
    {
        waveCount = attackWave;
        StartCoroutine(DelayRandMode(0));
    }

    private IEnumerator DelayRandMode(float time)
    {
        yield return new WaitForSeconds(time);
        int r = Random.Range(0, 5);
        if (r == 0)
            StartCoroutine(ShotMode1());
        else if (r == 1)
            StartCoroutine(ShotMode2());
        else if (r == 2)
            StartCoroutine(ShotMode3());
        else if (r == 3)
            StartCoroutine(ShotMode4());
        else if (r == 4)
            StartCoroutine(ShotMode5());
    }

    private void ModeEnd()
    {
        waveCount--;
        if (waveCount > 0)
            StartCoroutine(DelayRandMode(waveWaitTime));
        else
        {
            bossCtrl.CloseHand();
            bossFly.StartAttack();
        }
    }

    public IEnumerator ShotMode1()
    {
        int bulletCount = shotMode01.bulletCount;
        int angle = 0;
        int perAngle = shotMode01.perAngle;
        while (bulletCount > 0)
        {
            bulletCount--;
            if (angle >= shotMode01.angle / 2 || angle <= -shotMode01.angle / 2)
                perAngle = -perAngle;

            
            angle += perAngle;
            Vector3 dir = Quaternion.Euler(0, angle, 0) * muzzle.transform.forward;
            GameObject bullet = Instantiate(bulletPrefab, muzzle.position, Quaternion.LookRotation(dir));
            bullet.GetComponent<EnemyBullet>().SetBullet(10, 10, dir, transform);
            yield return new WaitForSeconds(shotMode01.rate);
        }
        ModeEnd();
    }

    public IEnumerator ShotMode2()
    {
        int bulletCount = shotMode02.bulletCount;
        float angle;
        while (bulletCount > 0)
        {
            bulletCount--;
            angle = Random.Range(-shotMode02.angle / 2, shotMode02.angle / 2);
            Vector3 dir = Quaternion.Euler(0, angle, 0) * muzzle.transform.forward;
            GameObject bullet = Instantiate(bulletPrefab, muzzle.position, Quaternion.LookRotation(dir));
            bullet.GetComponent<EnemyBullet>().SetBullet(10, 10, dir, transform);
            yield return new WaitForSeconds(shotMode02.rate);
        }
        ModeEnd();
    }
    public IEnumerator ShotMode3()
    {
        int bulletCount = shotMode03.bulletCount;
        float angle;
        while (bulletCount > 0)
        {
            bulletCount--;
            angle = Random.Range(-shotMode03.angle / 2, shotMode03.angle / 2);
            Vector3 playerDir = player.transform.position - muzzle.transform.position;
            playerDir.y = 0;
            Vector3 dir = Quaternion.Euler(0, angle, 0) * playerDir.normalized;
            GameObject bullet = Instantiate(bulletPrefab, muzzle.position, Quaternion.LookRotation(dir));
            bullet.GetComponent<EnemyBullet>().SetBullet(10, 10, dir, transform);
            yield return new WaitForSeconds(shotMode03.rate);
        }
        ModeEnd();
    }
    public IEnumerator ShotMode4()
    {
        int bulletCount = shotMode04.bulletCount;
        float perAngle = shotMode04.angle / (shotMode04.devide - 1);
        float startAngle = -shotMode04.angle / 2;
        while (bulletCount > 0)
        {
            bulletCount-= shotMode04.devide;
            for (int i = 0; i < shotMode04.devide; i++)
            {
                Vector3 dir = Quaternion.Euler(0, startAngle + perAngle * i, 0) * muzzle.forward;
                GameObject bullet = Instantiate(bulletPrefab, muzzle.position, Quaternion.LookRotation(dir));
                bullet.GetComponent<EnemyBullet>().SetBullet(10, 10, dir, transform);
            }
            yield return new WaitForSeconds(shotMode04.waitSec);
        }
        ModeEnd();
    }
    public IEnumerator ShotMode5()
    {
        int bulletCount = shotMode05.bulletCount;
        float perAngle = shotMode05.angle / (shotMode05.devide - 1);
        float startAngle = -shotMode05.angle / 2;
        while (bulletCount > 0)
        {
            bulletCount -= shotMode05.devide;
            for (int i = 0; i < shotMode05.devide; i++)
            {
                Vector3 playerDir = player.transform.position - transform.position;
                playerDir.y = 0;
                Vector3 dir = Quaternion.Euler(0, startAngle + perAngle * i, 0) * playerDir.normalized;
                GameObject bullet = Instantiate(bulletPrefab, muzzle.position, Quaternion.LookRotation(dir));
                bullet.GetComponent<EnemyBullet>().SetBullet(10, 10, dir, transform);
            }
            yield return new WaitForSeconds(shotMode05.waitSec);
        }
        ModeEnd();
    }
}
[System.Serializable]
public struct ShotMode01
{
    public int bulletCount;
    public float rate;
    [Range(0, 360)]
    public int angle;
    public int perAngle;
    public float bulletSpeed;
    public float bulletDamage;
}

[System.Serializable]
public struct ShotMode02
{
    public int bulletCount;
    public float rate;
    [Range(0, 360)]
    public int angle;
    public float bulletSpeed;
    public float bulletDamage;
}

[System.Serializable]
public struct ShotMode03
{
    public int bulletCount;
    public float rate;
    [Range(0, 360)]
    public int angle;
    public float bulletSpeed;
    public float bulletDamage;
}

[System.Serializable]
public struct ShotMode04
{
    public int bulletCount;
    public float waitSec;
    [Range(0, 360)]
    public int angle;
    public int devide;
    public float bulletSpeed;
    public float bulletDamage;
}

[System.Serializable]
public struct ShotMode05
{
    public int bulletCount;
    public float waitSec;
    [Range(0, 360)]
    public int angle;
    public int devide;
    public float bulletSpeed;
    public float bulletDamage;
}

