using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlyHeavyBullet : MonoBehaviour
{
    public int damage;
    public float moveSpeed;

    public float damageCD;
    private float timer;

    void Update()
    {
        transform.position += transform.forward * Time.deltaTime * moveSpeed;
        if (timer > 0)
            timer -= Time.deltaTime;
    }

    private void OnTriggerStay(Collider other)
    {
        if(other.CompareTag("Player") && timer <= 0)
        {
            timer = damageCD;
            other.GetComponent<PlayerActionManager>().StartHurt(damage, transform.forward, true);
        }
    }
}
