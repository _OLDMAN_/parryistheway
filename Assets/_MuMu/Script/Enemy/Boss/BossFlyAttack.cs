using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BossFlyAttack : MonoBehaviour
{
    private BossCtrl bossCtrl;
    public int attackWave;
    public float waveWaitTime;

    public int normalHitDamage = 10;
    public int flyDamage = 10;
    public int walkDamage = 10;
    public int waterDamage = 10;

    public bool isFlyingAttack
    {
        get
        {
            return !(rightCtrl.waveEnd && leftCtrl.waveEnd);
        }
    }

    private bool isStart;
    public BossFlyHand rightCtrl;
    public BossFlyHand leftCtrl;

    private void Start()
    {
        //StartAttack();
        bossCtrl = GetComponent<BossCtrl>();
    }

    private void Update()
    {
        if (isStart == true && isFlyingAttack == false)
        {
            bossCtrl.OpenHand();
            isStart = false;
        }
    }

    public void StartAttack()
    {
        rightCtrl.StartAttack(attackWave, waveWaitTime);
        leftCtrl.StartAttack(attackWave, waveWaitTime);
        isStart = true;
    }

    public void StopCurrentAction()
    {
    }
}