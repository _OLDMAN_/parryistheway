using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossCtrl : MonoBehaviour
{
    private BossMeleeAttack bossMeleeAttack;
    private BossHandAnimation ani;
    public Transform muzzle;

    public bool isOpen;

    private void Start()
    {
        bossMeleeAttack = GetComponent<BossMeleeAttack>();
        ani = GetComponent<BossHandAnimation>();
    }

    public void OpenHand()
    {
        isOpen = true;
        //ani.PlayAnimation(BossHandAni.Open);
        StartCoroutine(DelayMeleeAttack());
    }

    public IEnumerator DelayMeleeAttack()
    {
        yield return new WaitForSeconds(1);
        bossMeleeAttack.StartAttack();
    }

    public void CloseHand()
    {
        isOpen = false;
       // ani.PlayAnimation(BossHandAni.Close);
    }
}
