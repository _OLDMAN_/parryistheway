using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBreak : MonoBehaviour
{
    private BossShotAttack bossShot;
    private BossMeleeAttack bossMelee;
    private BossAnimationEvent bossEvent;
    private BossHandAnimation bossAni;
    public Transform heartPos;

    public int breakCount = 10;
    public float dizzyTime;
    private int breakCounter;
    public Vector3 offset = Vector3.zero;
    public float radius = 1;
    public float shieldRotateSpeed = 3;
    private GameObject rotateCenter;
    private List<GameObject> shieldList = new List<GameObject>();
    public bool isBreaking;

    private void Start()
    {
        bossShot = GetComponent<BossShotAttack>();
        bossEvent = GetComponent<BossAnimationEvent>();
        bossMelee = GetComponent<BossMeleeAttack>();
        bossAni = GetComponent<BossHandAnimation>();
        breakCounter = breakCount;
        if (rotateCenter == null)
        {
            rotateCenter = new GameObject("ShieldCenter");
            rotateCenter.transform.position = heartPos.transform.position + offset;
        }
        AddShield(breakCounter);
    }

    private void Update()
    {
        if (shieldList.Count != 0)
            rotateCenter.transform.Rotate(0, shieldRotateSpeed, 0);
    }

    public void ReduceBreakTime(int count)
    {
        if (isBreaking == true) return;

        RemoveShield(count);
        breakCounter -= count;
        if (breakCounter <= 0)
        {
            ParticleManager.Instance.PlayParticle("ShieldBreak", heartPos.position, heartPos.rotation);
            Break();
        }
        else
            ParticleManager.Instance.PlayParticle("Shield", heartPos.position, heartPos.rotation);
    }
    private void AddShield(int count)
    {
        for (int i = 0; i < count; i++)
            shieldList.Add(ParticleManager.Instance.GetParticle("EnemyShieldCount", rotateCenter.transform.position, Quaternion.identity, rotateCenter.transform));
        RefreshShieldPos();
    }

    public void RemoveShield(int count)
    {
        if (shieldList.Count == 0) return;

        if (count > shieldList.Count)
            count = shieldList.Count;
        for (int i = 0; i < count; i++)
        {
            GameObject o = shieldList[0];
            shieldList.Remove(o);
            o.GetComponent<PoolRecycle>().Recycle();
        }

        RefreshShieldPos();
    }

    private void RefreshShieldPos()
    {
        if (shieldList.Count == 0) return;

        float perAngle = 360f / shieldList.Count;
        for (int i = 0; i < shieldList.Count; i++)
        {
            shieldList[i].transform.position = rotateCenter.transform.position + Quaternion.Euler(0, perAngle * i, 0) * rotateCenter.transform.forward * radius;
        }
    }

    public void Break(bool isReplay = true)
    {
        isBreaking = true;
        bossAni.PlayAnimation(BossHandAni.Break);
        StopAllAction();
        if (isReplay == true)
            StartCoroutine(Replay());
    }

    public void StopAllAction()
    {
        bossShot.StopAllAction();
        bossEvent.StopAllAction();
        bossMelee.StopAllAction();
    }

    private IEnumerator Replay()
    {
        yield return new WaitForSeconds(dizzyTime);
        bossAni.PlayAnimation(BossHandAni.Idle);
        yield return new WaitForSeconds(1);
        breakCounter = breakCount;
        isBreaking = false;
        bossMelee.StartAttack();
    }
}
