using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Mode03Setting
{
    public int waveCount = 3;
    public int fallCount = 5;
}

public class BossMeleeAttack : MonoBehaviour
{
    private BossShotAttack bossShot;
    private BossHandAnimation ani;
    private BossAnimationEvent aniEvent;

    public int attackWave;
    [HideInInspector]
    public int waveCounter;
    public float waveWaitTime;

    public int mode01HandDamage = 10;
    public int mode02HandDamage = 10;
    public int mode02SkillDamage = 10;
    public int mode03HandDamage = 10;

    public Mode03Setting mode03Setting;
    [HideInInspector] public int meleeMode03WaveCounter;

    private void Start()
    {
        bossShot = GetComponent<BossShotAttack>();
        ani = GetComponent<BossHandAnimation>();
        aniEvent = GetComponent<BossAnimationEvent>();
        waveCounter = attackWave;
    }

    public void StopAllAction()
    {
        StopAllCoroutines();
    }

    public void StartAttack()
    {
        StartCoroutine( RandAttack(0));
    }

    public void AttackEnd()
    {
        waveCounter--;
        if (waveCounter > 0)
            StartCoroutine(RandAttack(waveWaitTime));
        else
            bossShot.StartAttack();
    }

    private IEnumerator RandAttack(float time)
    {
        yield return new WaitForSeconds(time);
        int r = Random.Range(1, 4);
        if (r == 1)
            MeleeMode01();
        else if (r == 2)
            MeleeMode02();
        else
            MeleeMode03();
    }

    public void MeleeMode01()
    {
        //ani.PlayAnimation(BossHandAni.Attack);
        aniEvent.attackMode = 1;
    }
    public void MeleeMode02()
    {
        //ani.PlayAnimation(BossHandAni.AttackDuring);
        aniEvent.attackMode = 2;
    }
    public void MeleeMode03()
    {
        meleeMode03WaveCounter = mode03Setting.waveCount;
        //ani.PlayAnimation(BossHandAni.AttackLoop);
        aniEvent.attackMode = 3;
    }
}
