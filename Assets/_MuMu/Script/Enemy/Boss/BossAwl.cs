using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAwl : MonoBehaviour
{
    private Rigidbody rb;
    public float gravityScale;

    private GameObject warningObj;
    private bool isHit;

    private void OnEnable()
    {
        isHit = false;
    }

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        warningObj = ParticleManager.Instance.GetParticle("BossAwlWarning", new Vector3(transform.position.x, 0.1f, transform.position.z), Quaternion.identity);
        warningObj.transform.localScale = Vector3.one * 0.7f;
    }

    void Update()
    {
        Vector3 gravity = Physics.gravity * gravityScale;
        rb.AddForce(gravity, ForceMode.Acceleration);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (isHit == true) return;

        ParticleManager.Instance.PlayParticle("BossAwlHitWater", transform.position, Quaternion.identity);
        SoundManager.Instance.PlaySound("CrystalDrop");
        warningObj.GetComponent<PoolRecycle>().Recycle();
        isHit = true;
    }
}
