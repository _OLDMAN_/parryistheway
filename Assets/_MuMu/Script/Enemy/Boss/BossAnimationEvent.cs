using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossAnimationEvent : MonoBehaviour
{
    private BossMeleeAttack meleeAttack;
    private BossHandAnimation ani;

    public Transform leftAttackWarningPos;
    public Transform rightAttackWarningPos;
    public Transform leftHand;
    public Transform rightHand;
    public Transform fallItemArea;

    public int attackMode;
    private LayerMask playerLayer;
    private GameObject player;

    private void Start()
    {
        meleeAttack = GetComponent<BossMeleeAttack>();
        ani = GetComponent<BossHandAnimation>();
        player = GameObject.FindWithTag("Player");
        playerLayer = LayerMask.GetMask("Player");
    }

    public void StopAllAction()
    {
        StopAllCoroutines();
    }

    public void AttackWarning()
    {
        //GameObject leftObj = ParticleManager.Instance.GetParticle("VFX_AttackWarning", leftAttackWarningPos.position, Quaternion.identity);
        //GameObject rightObj = ParticleManager.Instance.GetParticle("VFX_AttackWarning", rightAttackWarningPos.position, Quaternion.identity);
        //leftObj.transform.DOScale(Vector3.one * 3f, 1.5f).From(Vector3.zero).onComplete += ()=> { leftObj.GetComponent<PoolRecycle>().Recycle(); };
        //rightObj.transform.DOScale(Vector3.one * 3f, 1.5f).From(Vector3.zero).onComplete += () => { rightObj.GetComponent<PoolRecycle>().Recycle(); };
    }

    public void AttackFrame()
    {
        if (attackMode == 1)
            Mode01();
        else if (attackMode == 2)
            Mode02();
        else
            Mode03();
    }

    public void AttackEndFrame()
    {
        meleeAttack.AttackEnd();
    }

    private void Mode01()
    {
        GamefeelManager.Instance.ShakeCamera(3, 0.2f);
        SoundManager.Instance.PlaySound("BossHitWater");
        ParticleManager.Instance.PlayParticle("BossHitWater", leftHand.position, Quaternion.identity);
        ParticleManager.Instance.PlayParticle("BossHitWater", rightHand.position, Quaternion.identity);
        Collider[] leftC = Physics.OverlapCapsule(leftHand.position, leftHand.position + Vector3.up, 3.5f, playerLayer);
        Collider[] rightC = Physics.OverlapCapsule(rightHand.position, rightHand.position + Vector3.up, 3.5f, playerLayer);
        if (leftC.Length != 0 || rightC.Length != 0)
            player.GetComponent<PlayerActionManager>().StartHurt(meleeAttack.mode01HandDamage, transform, true);
    }
    private void Mode02()
    {
        GamefeelManager.Instance.ShakeCamera(3, 0.2f);
        SoundManager.Instance.PlaySound("BossHitWater");
        ParticleManager.Instance.PlayParticle("BossHitWater", leftHand.position, Quaternion.identity);
        ParticleManager.Instance.PlayParticle("BossHitWater", rightHand.position, Quaternion.identity);
        ParticleManager.Instance.PlayParticle("BossBombCircle", rightHand.position, Quaternion.identity);
        ParticleManager.Instance.PlayParticle("BossBombCircle", leftHand.position, Quaternion.identity);
        CheckHitPlayer(3.5f, meleeAttack.mode02HandDamage);
        StartCoroutine(DuringAttack());
    }
    private IEnumerator DuringAttack()
    {
        int count = 1;
        float radius = 3.5f;
        while (count < 3)
        {
            yield return new WaitForSeconds(2f);
            CheckHitPlayer((radius - 0.5f) * count, (radius + 0.5f) * count, meleeAttack.mode02SkillDamage);
            count++;
        }
        ani.PlayAnimation(BossHandAni.Idle);
    }

    public void Mode03()
    {
        meleeAttack.meleeMode03WaveCounter--;
        CheckHitPlayer(3.5f, meleeAttack.mode03HandDamage);
        SoundManager.Instance.PlaySound("BossHitWater");
        GamefeelManager.Instance.ShakeCamera(3, 0.2f);
        StartCoroutine(FallItem());
        if (meleeAttack.meleeMode03WaveCounter <= 0)
            ani.PlayAnimation(BossHandAni.Idle);
    }

    private IEnumerator FallItem()
    {
        ParticleManager.Instance.PlayParticle("BossHitWater", leftHand.position, Quaternion.identity);
        ParticleManager.Instance.PlayParticle("BossHitWater", rightHand.position, Quaternion.identity);

        int count = meleeAttack.mode03Setting.fallCount;
        while(count > 0)
        {
            Vector3 spawnPos;
            while (true)
            {
                spawnPos = fallItemArea.position + Vector3.right * Random.Range(-fallItemArea.localScale.x / 2, fallItemArea.localScale.x / 2) + Vector3.forward * Random.Range(-fallItemArea.localScale.z / 2, fallItemArea.localScale.z / 2);
                Collider[] c = Physics.OverlapCapsule(spawnPos, spawnPos + Vector3.up * 10, ParticleManager.Instance.GetParticlePrefab("BossAwl").GetComponent<CapsuleCollider>().radius, LayerMask.GetMask("Awl"));
                if (c.Length == 0)
                    break;
            }

            ParticleManager.Instance.PlayParticle("BossAwl", spawnPos + Vector3.up * 10, Quaternion.identity);
            count--;
            yield return new WaitForSeconds(0.3f);
        }
    }

    private void CheckHitPlayer(float radius, int damage)
    {
        Collider[] leftC = Physics.OverlapSphere(leftHand.position, radius, playerLayer);
        Collider[] rightC = Physics.OverlapSphere(rightHand.position, radius, playerLayer);
        if (leftC.Length != 0 || rightC.Length != 0)
            player.GetComponent<PlayerActionManager>().StartHurt(10, transform, true);
    }

    private void CheckHitPlayer(float innerRadius, float outRadius , int damage)
    {
        Collider[] leftInnerC = Physics.OverlapSphere(leftHand.position, innerRadius, playerLayer);
        Collider[] leftOutC = Physics.OverlapSphere(leftHand.position, outRadius, playerLayer);

        Collider[] rightInnerC = Physics.OverlapSphere(rightHand.position, innerRadius, playerLayer);
        Collider[] rightOutC = Physics.OverlapSphere(rightHand.position, outRadius, playerLayer);

        if ((leftInnerC.Length == 0 && leftOutC.Length != 0) || (rightInnerC.Length == 0 && rightOutC.Length != 0))
            player.GetComponent<PlayerActionManager>().StartHurt(10, transform, true);
    }
}
