using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Melee01KeepAway : Melee01Action
{
    private Melee01Warning warning;
    private Melee01Ctrl ctrl;

    public float keepAwaySpeed = 1;
    [Range(0, 1)]
    public float warnSecPercentage = 0.5f;
    private Vector3 movePos;
    private Vector3 moveDir;

    private void Awake()
    {
        GetComp();
    }

    //獲取腳本
    private void GetComp()
    {
        warning = GetComponent<Melee01Warning>();
        ctrl = GetComponent<Melee01Ctrl>();
    }

    //逃跑開始
    public override void StartAction()
    {
        base.StartAction();
        agent.isStopped = false;
        ctrl._currentMoveSpeed = keepAwaySpeed;
        CountKeepAwayPoint();
    }

    public override void UpdateAction()
    {
        if (warning.attackRequidTimer <= warning.attackCD * warnSecPercentage || warning.isOnWarningRange == false)
        {
            ForceEnd();
        }

        float dot = Vector3.Dot(GetPlayerDir(), moveDir);
        if (agent.remainingDistance <= agent.stoppingDistance || dot > 0)
            CountKeepAwayPoint();
        RotateToMoveDir();
    }

    public override void EndAction()
    {
        base.EndAction();
    }

    //計算逃跑點
    private void CountKeepAwayPoint()
    {
        NavMeshHit hit = default;
        int minDistance = 1;
        int maxDistance = 2;
        int maxCount = 100;
        while (maxCount > 0)
        {
            Vector3 randomPos = Random.insideUnitSphere * 5 + transform.position;
            NavMesh.SamplePosition(randomPos, out hit, 10, NavMesh.AllAreas);
            Vector3 moveDir = hit.position - transform.position;
            moveDir.y = 0;
            float dot = Vector3.Dot(GetPlayerDir(), moveDir);
            maxCount--;
            if (hit.distance >= minDistance && hit.distance <= maxDistance && dot <= 0)
                break;
        }

        if (hit.hit == false)
            movePos = transform.position;
        else
            movePos = hit.position;
        moveDir = movePos - transform.position;
        moveDir.y = 0;
        agent.SetDestination(movePos);
    }

    //轉向移動方向
    private void RotateToMoveDir()
    {
        transform.forward = Vector3.Lerp(transform.forward, agent.velocity, 15 * Time.deltaTime);
    }

    //設定結束時間
    public override void SetActionEndTime()
    {
        
    }
}
