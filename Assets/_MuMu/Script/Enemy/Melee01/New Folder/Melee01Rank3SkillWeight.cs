using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Melee01SkillWeight
{
    public Melee01Ani name;
    public int weight;
}
public class Melee01Rank3SkillWeight : MonoBehaviour
{
    public List<Melee01SkillWeight> skillWeights;
    private Dictionary<string, int> dic = new Dictionary<string, int>();

    private void Start()
    {
        foreach (var skill in skillWeights)
        {
            dic.Add(skill.name.ToString(), skill.weight);
        }
    }

    public Melee01Ani GetWeightSkill()
    {
        string aniName = dic.GetWeight();
        if (aniName == Melee01Ani.Attack.ToString())
            return Melee01Ani.Attack;
        else if (aniName == Melee01Ani.Skill01_1.ToString())
            return Melee01Ani.Skill01_1;
        else if (aniName == Melee01Ani.Bite.ToString())
            return Melee01Ani.Bite;

        Debug.Log("No Find Skill");
        return Melee01Ani.Idle;
    }
}
