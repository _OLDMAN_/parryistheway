using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee01Rank3_Attack : Melee01Attack
{
    private Melee01Rank3SkillWeight skillWeight;

    public override void Awake()
    {
        base.Awake();
        skillWeight = GetComponent<Melee01Rank3SkillWeight>();
    }
    public override void AttackFrame()
    {
        ParticleManager.Instance.PlayParticle("Melee01Rank3Attack", transform.position + transform.forward * transform.localScale.x, transform.rotation, transform);
        SoundManager.Instance.PlaySound("Melee03CommonAttack");
        Collider[] colliders = Physics.OverlapBox(transform.position + transform.rotation * attackAreaOffset, attackArea, transform.rotation, playerLayer);
        if (colliders.Length != 0)
        {
            colliders[0].GetComponent<PlayerActionManager>().StartHurt(damage, transform);
        }
    }
    public override void StartAction()
    {
        Melee01Ani skill = skillWeight.GetWeightSkill();
        if (skill == Melee01Ani.Attack)
            base.StartAction();
        else if (skill == Melee01Ani.Skill01_1)
            actionManager.Skill01();
        else if (skill == Melee01Ani.Bite)
            actionManager.Bite();
    }
}
