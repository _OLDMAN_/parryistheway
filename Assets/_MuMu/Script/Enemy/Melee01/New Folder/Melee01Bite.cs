using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Melee01Bite : Melee01Action
{
    private Melee01Warning warning;

    public LayerMask obsLayer;
    public float maxSprintDistance = 5;
    public int damage;

    public bool debug;
    public Vector3 attackAreaOffset;
    public Vector3 attackArea;
    Tween sprintT;

    public override void Start()
    {
        base.Start();
        warning = GetComponent<Melee01Warning>();
    }

    public override void StartAction()
    {
        base.StartAction();
        hurt.superArmor = true;
        StopAgent();
        transform.forward = GetPlayerDir();
        ParticleManager.Instance.PlayParticle("Melee03BitePrepare", transform.position,transform.rotation,transform);
        SoundManager.Instance.PlaySound("Melee03BitePrepare");
    }

    public void SprintFrame()
    {
        float distance = maxSprintDistance;
        RaycastHit hit = GetCapCastHit(transform.forward, maxSprintDistance, obsLayer);
        if (hit.collider != null)
            distance = hit.distance;

        sprintT = transform.DOMove(transform.position + transform.forward * distance, 9f / 30f).SetEase(Ease.InCubic).OnUpdate(()=> {
            RaycastHit hit = GetCapCastHit(transform.forward, 3, obsLayer);
            if (hit.collider != null)
                sprintT.Kill();
        });
        tweens.Add(sprintT);
    }

    public void BiteFrame()
    {
        ParticleManager.Instance.PlayParticle("Melee03Bite",transform.position,transform.rotation,transform);
        SoundManager.Instance.PlaySound("Melee03Bite");
        Collider[] colliders = Physics.OverlapBox(transform.position + transform.rotation * attackAreaOffset, attackArea, transform.rotation, LayerMask.GetMask("Player"));
        if (colliders.Length != 0)
        {
            colliders[0].GetComponent<PlayerActionManager>().StartHurt(damage, transform);
        }
    }

    public override void EndAction()
    {
        warning.attackRequidTimer = warning.attackRequidSecRand;
        hurt.superArmor = false;
        base.EndAction();
    }

    private void OnDrawGizmos()
    {
        if (debug == false) return;

        Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        Gizmos.matrix = rotationMatrix;
        Gizmos.DrawWireCube(attackAreaOffset, attackArea);
    }
}
