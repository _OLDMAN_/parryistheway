using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Melee01Skill01 : Melee01Action
{
    private Melee01Warning warning;

    public int damage;

    public bool debug;
    public int radius = 5;
    private float oriY;

    private void Awake()
    {
        warning = GetComponent<Melee01Warning>();
    }

    public override void Start()
    {
        base.Start();
        oriY = agent.baseOffset * transform.localScale.y;
    }

    public override void StartAction()
    {
        base.StartAction();
        Skill01();
    }

    //�ޯ�01
    private void Skill01()
    {
        agent.enabled = false;
        Vector3 playerInput = PlayerController.Instance.targetDir;
        Vector3 targetPos;
        if (playerInput != Vector3.zero)
            targetPos = player.transform.position + player.transform.forward * 2;
        else
            targetPos = player.transform.position;
        targetPos.y = 0.1f;
        GameObject o = ParticleManager.Instance.GetParticle("Melee03SkillWarning", targetPos, Quaternion.identity);
        o.transform.localScale = new Vector3(3f, 3f, 3f) * radius;
        transform.forward = targetPos - transform.position;
        targetPos.y = oriY;
        Tween jump = transform.DOMoveY(5, GetAnimationLength(Melee01Ani.Skill01_1));
        jump.OnComplete(() =>{ Dive(targetPos); });
        tweens.Add(jump);
    }

    //�U��
    private void Dive(Vector3 targetPos)
    {
        PlayAnimation(Melee01Ani.Skill01_2);
        Tween dive = transform.DOMove(targetPos, GetAnimationLength(Melee01Ani.Skill01_2) * 13f / 25f);
        dive.SetEase(Ease.InCirc);
        dive.OnComplete(() => { HitFrame(); });
        tweens.Add(dive);
    }

    //�P�w�ˮ`��
    private void HitFrame()
    {
        Vector3 pos = transform.position;
        pos.y = 0;
        ParticleManager.Instance.PlayParticle("Melee03Skill", pos, Quaternion.identity);
        SoundManager.Instance.PlaySound("Melee03GroundSlam");
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius, LayerMask.GetMask("Player"));
        foreach (Collider c in colliders)
            c.GetComponent<PlayerActionManager>().StartHurt(damage, transform, true);
    }

    public override void EndAction()
    {
        warning.attackRequidTimer = warning.attackRequidSecRand;
        agent.enabled = true;
        base.EndAction();
    }

    //�]�w�ޯ൲��ɶ� ���_ + ���U
    public override void SetActionEndTime()
    {
        float actionLength = 0;
        actionLength += GetAnimationLength(aniData.ani);
        actionLength += GetAnimationLength(Melee01Ani.Skill01_2);
        StartCoroutine(TickToEndAction(actionLength));
    }
    private void OnDrawGizmos()
    {
        if (debug == false) return;
        Gizmos.color = Color.cyan;
        GizmosExtensions.DrawCircle(transform.position, 5);
    }
}
