using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Melee01Hurt : Melee01Action, IHurtable, IBreakable
{
    private Melee01Warning warning;
    private Melee01Dead dead;
    private Melee01Break breakAction;
    private EnemyHealth health;

    private List<Material> materials = new List<Material>();
    public LayerMask obsLayer;

    public int maxHurtCount = 3;
    public float noShieldBounsDamage = 50;
    public float shieldArmor;
    public Vector3 shieldOffset;
    public Vector3 shieldScale;
    private Vector3 hurtDir;

    public int hurtCounter { get; set; }
    public int breakCounter { get; set; }

    [HideInInspector] public bool isKnockBack;
    [HideInInspector] public bool superArmor;

    private void Awake()
    {
        hurtCounter = maxHurtCount;
        GetComp();
        SetMaterials();
    }

    //�]�m�n�C���ܤƪ�����y
    private void SetMaterials()
    {
        Renderer[] renderer = GetComponentsInChildren<Renderer>();
        foreach (Renderer r in renderer)
            materials.Add(r.material);
    }

    //����}��
    private void GetComp()
    {
        warning = GetComponent<Melee01Warning>();
        dead = GetComponent<Melee01Dead>();
        breakAction = GetComponent<Melee01Break>();
        health = GetComponent<EnemyHealth>();
    }

    //���˶}�l
    public override void StartAction()
    {
        base.StartAction();
        transform.forward = hurtDir;
        agent.isStopped = true;
        agent.velocity = Vector3.zero;
    }

    //���˵���
    public override void EndAction()
    {
        base.EndAction();
        //actionManager.KeepAway();
        agent.isStopped = false;
    }

    //����
    public void Damage(Transform attacker, int damage, bool heavyDamage = false)
    {
        if (dead.isDie == true) return;
        EnemyBullet bullet = attacker.GetComponent<EnemyBullet>();
        if (bullet != null)
            hurtDir = -bullet.dir;
        else
            hurtDir = attacker.position - transform.position;
        hurtDir.y = 0;
        hurtDir.Normalize();

        SoundManager.Instance.PlaySound("EnemyHurt01");
        SoundManager.Instance.PlaySound("EnemyHurt02");
        ParticleManager.Instance.PlayParticle("EnemyHurt", transform.position + hurtDir * 0.5f, Quaternion.LookRotation(-hurtDir));

        Blink();
        Damage(damage, heavyDamage);
    }

    //���˨��ˬd�O�_���`
    private void Damage(int damage, bool heavyDamage = false)
    {
        if (breakAction.isBreaking == true)
        {
            damage = Mathf.RoundToInt(damage * (1 + noShieldBounsDamage / 100f));
        }
        else
        {
            damage = Mathf.RoundToInt(damage * (1 - shieldArmor / 100f));
            ParticleManager.Instance.PlayParticle("EnemyShield", transform.position + shieldOffset, Quaternion.identity, shieldScale, transform);
        }

        health.Damage(damage, heavyDamage);

        if (health.HP <= 0)
            actionManager.Dead();
        else if (breakAction.breakCounter <= 0)
        {
            Break();
            if (hurtCounter <= 0)
                hurtCounter = maxHurtCount;
        }
        else if (hurtCounter <= 0)
        {
            actionManager.Hurt();
            hurtCounter = maxHurtCount;
        }
        else if(warning.isWarning == true)
        {
            actionManager.KeepAway();
        }
    }

    public void ReduceBreakCount(int breakCount)
    {
        if (breakAction.isBreaking == true) return;

        breakAction.breakCounter -= breakCount;
    }

    public void Break()
    {
        actionManager.Break();
        breakAction.breakCounter = breakAction.maxBreakCount;
        ParticleManager.Instance.PlayParticle("EnemyShieldBreak", transform.position + shieldOffset, Quaternion.LookRotation(hurtDir), shieldScale, transform);
    }

    public void ReduceHurtCount(int hurtCount)
    {
        if (breakAction.isBreaking == true || superArmor == true) return;
        hurtCounter -= hurtCount;
    }

    //�C���ܤ�
    private void Blink()
    {
        float HurtSec = 0.2f;
        foreach (Material m in materials)
        {
            if (m.HasProperty("_Sprint"))
                m.DOFloat(1f, "_Sprint", HurtSec / 4).onComplete += () => m.DOFloat(0, "_Sprint", HurtSec / 4).onComplete += () => m.DOFloat(1f, "_Sprint", HurtSec / 4).onComplete += () => m.DOFloat(0, "_Sprint", HurtSec / 4);
        }
    }

    //���h
    public void KnockBack(Vector3 backDir, float distance)
    {
        RaycastHit hit = GetCapCastHit(backDir, distance, obsLayer);
        if (hit.collider != null)
            distance = hit.distance;

        StartCoroutine(DoKnockBack(backDir, distance));
    }

    private IEnumerator DoKnockBack(Vector3 dir, float distance)
    {
        isKnockBack = true;
        float speed = distance / 0.3f;
        while (distance > 0)
        {
            transform.position += dir * Time.deltaTime * speed * GamefeelManager.stopSpeed;
            distance -= Time.deltaTime * speed * GamefeelManager.stopSpeed;
            yield return null;
        }
        isKnockBack = false;
    }
}