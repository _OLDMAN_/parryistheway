using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee01Attack : Melee01Action
{
    private Melee01Ctrl ctrl;
    private Melee01Warning warning;

    protected LayerMask playerLayer;
    public int damage;

    public bool debug;
    public Vector3 attackAreaOffset;
    public Vector3 attackArea;

    private GameObject indicator;
    private bool attack;

    public virtual void Awake()
    {
        ctrl = GetComponent<Melee01Ctrl>();
        warning = GetComponent<Melee01Warning>();
        playerLayer = LayerMask.GetMask("Player");
    }

    //�����}�l
    public override void StartAction()
    {
        hurt.superArmor = true;
        agent.SetDestination(player.transform.position);
        ctrl._currentMoveSpeed = 15;
        attack = false;
    }

    public override void UpdateAction()
    {
        if (attack == true) return;
        agent.SetDestination(player.transform.position);
        transform.forward = Vector3.Lerp(transform.forward, GetPlayerDir(), Time.deltaTime * 10);
        if (agent.remainingDistance <= agent.stoppingDistance && Vector3.Angle(transform.forward, GetPlayerDir()) < 25)
        {
            ctrl._currentMoveSpeed = ctrl.moveSpeed;
            animator.PlayAnimation(aniData.ani);
            ParticleManager.Instance.PlayParticle("Melee01AttackPrepare", transform.position, transform.rotation, transform);
            SetActionEndTime();
            StopAgent();
            attack = true;
        }
    }

    //��������
    public override void EndAction()
    {
        EnemysControlManager.Instance.currentAttackerCount--;
        warning.attackRequidTimer = warning.attackRequidSecRand;
        hurt.superArmor = false;
        base.EndAction();
    }

    public void SpawnIndicator()
    {
        Vector3 pos = transform.position;
        pos.y = 0.05f;
        indicator = ParticleManager.Instance.GetParticle("AttackIndicatorSmall", pos, transform.rotation);
        indicator.GetComponent<Indicator>().StartIndicat(0.1f);
    }
    public void SpawnIndicatorBurst()
    {
        GameObject g = ParticleManager.Instance.GetParticle("AttackIndicatorBurstSmall", indicator.transform.position, indicator.transform.rotation);
        g.GetComponent<IndicatorFlash>().Flash(2);
        indicator.GetComponent<PoolRecycle>().Recycle();
    }

    //�����ʵe��
    public virtual void AttackFrame()
    {
        ParticleManager.Instance.PlayParticle("Melee01Attack", transform.position + transform.forward * transform.localScale.x, transform.rotation, transform);
        Collider[] colliders = Physics.OverlapBox(transform.position + transform.rotation * attackAreaOffset, attackArea, transform.rotation, playerLayer);
        if(colliders.Length != 0)
        {
            colliders[0].GetComponent<PlayerActionManager>().StartHurt(damage, transform);
        }
    }

    //�]�w�����ɶ� = �e�n + ���� + ��n
    public override void SetActionEndTime()
    {
        float actionLength = 0;
        actionLength += GetAnimationLength(aniData.ani);
        StartCoroutine(TickToEndAction(actionLength));
    }

    //�e�����d��
    private void OnDrawGizmos()
    {
        if (debug == false) return;

        Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        Gizmos.matrix = rotationMatrix;
        Gizmos.DrawWireCube(attackAreaOffset, attackArea);
    }
}
