using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Melee01AnimationData
{
    public Melee01Ani ani;
    public bool interuptOtherAnimation = false;
    public bool interuptOwnAnimation = false;
}

public enum Melee01Ani
{
    Idle, Move, Attack, Hurt, Break, Dizzy, BreakRecover, Dead, Skill01_1, Skill01_2, Bite
}
