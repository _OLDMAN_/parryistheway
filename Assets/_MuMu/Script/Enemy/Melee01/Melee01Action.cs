using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using DG.Tweening;

public class Melee01Action : MonoBehaviour
{
    protected Melee01Hurt hurt;
    protected Melee01ActionManager actionManager;
    protected Melee01Animation animator;
    protected NavMeshAgent agent;
    protected GameObject player;
    protected CapsuleCollider m_collider;

    public Melee01AnimationData aniData;
    public List<Tween> tweens = new List<Tween>();

    public virtual void Start()
    {
        hurt = GetComponent<Melee01Hurt>();
        actionManager = GetComponent<Melee01ActionManager>();
        agent = GetComponent<NavMeshAgent>();
        m_collider = GetComponent<CapsuleCollider>();
        animator = GetComponent<Melee01Animation>();
        player = GameObject.FindWithTag("Player");
    }

    public virtual void StartAction()
    {
        animator.PlayAnimation(aniData.ani, !aniData.interuptOwnAnimation);
        SetActionEndTime();
    }

    public virtual void UpdateAction()
    {

    }

    public virtual void EndAction()
    {
        actionManager.RemoveCurrentAction();
    }

    public void PlayAnimation(Melee01Ani ani)
    {
        animator.PlayAnimation(ani);
    }

    public virtual void SetActionEndTime()
    {
        AnimationHolder holder = animator.GetAnimationHolder(aniData.ani.ToString());
        if (holder == null || holder.clip == null) return;

        float actionLength = GetAnimationLength(aniData.ani);
        StartCoroutine(TickToEndAction(actionLength));
    }

    protected float GetAnimationLength(Melee01Ani ani)
    {
        return animator.GetAnimationHolder(ani.ToString()).Length;
    }

    public void ForceEnd()
    {
        foreach(Tween tween in tweens)
            tween.Kill();
        StopAllCoroutines();
        EndAction();
    }

    protected IEnumerator TickToEndAction(float aniSec)
    {
        float time = aniSec;
        while (time > 0)
        {
            time -= Time.deltaTime * GamefeelManager.stopSpeed;
            yield return null;
        }
        EndAction();
    }

    protected void StopAgent()
    {
        agent.isStopped = true;
        agent.velocity = Vector3.zero;
    }
    protected void StartAgent()
    {
        agent.isStopped = false;
        agent.speed = 1;
    }

    protected float GetPlayerDistance()
    {
        Vector3 ownPos = transform.position;
        ownPos.y = 0;
        Vector3 playerPos = player.transform.position;
        playerPos.y = 0;
        return Vector3.Distance(ownPos, playerPos);
    }

    protected Vector3 GetPlayerDir()
    {
        return (player.transform.position.GetZeroY() - transform.position.GetZeroY()).normalized;
    }

    protected RaycastHit GetCapCastHit(Vector3 dir, float distance, LayerMask layerMask)
    {
        RaycastHit hit;
        Vector3 ownPos = new Vector3(transform.position.x, 0, transform.position.z);
        Physics.CapsuleCast(ownPos, ownPos + Vector3.up, m_collider.radius, dir, out hit, distance, layerMask);

        return hit;
    }
}
