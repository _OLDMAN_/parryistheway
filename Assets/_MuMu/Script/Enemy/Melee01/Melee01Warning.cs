using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Melee01Warning : Melee01Action
{
    protected Melee01Ctrl ctrl;
    protected Melee01Attack attack;

    public float attackCD;

    public bool debug;
    public float warningRadius = 5f;
    public float exitWarningRadius = 8f;

    [HideInInspector] public float attackRequidTimer;
    public float attackRequidSecRand {
        get {
            return Random.Range(attackCD - attackCD / 4, attackCD + attackCD / 4);
        }
    }

    [HideInInspector] public bool isOnWarningRange;
    [HideInInspector] public bool isWarning;

    private void Awake()
    {
        GetComp();
    }

    protected bool readyAttack;

    //獲取自身腳本
    private void GetComp()
    {
        attack = GetComponent<Melee01Attack>();
        ctrl = GetComponent<Melee01Ctrl>();
        hurt = GetComponent<Melee01Hurt>();
    }

    private void Update()
    {
        if ((GetPlayerDistance() < warningRadius || isOnWarningRange == true) && actionManager.isAction == false)
            actionManager.Warning();

        if (isOnWarningRange == true)
        {
            if (attackRequidTimer > 0)
                attackRequidTimer -= Time.deltaTime;
            if (GetPlayerDistance() > exitWarningRadius)
                isOnWarningRange = false;
        }
    }

    //警戒開始
    public override void StartAction()
    {
        base.StartAction();
        StartAgent();
        ctrl._currentMoveSpeed = ctrl.moveSpeed;
        isOnWarningRange = true;
        readyAttack = false;
        isWarning = true;
    }

    //警戒中
    public override void UpdateAction()
    {
        if (readyAttack == true) return;

        if (attackRequidTimer <= 0)
        {
            EnemysControlManager.Instance.melee01s.Add(actionManager);
            readyAttack = true;
        }
        else
        {
            agent.SetDestination(player.transform.position);
        }

        transform.forward = Vector3.Lerp(transform.forward, agent.velocity, Time.deltaTime * ctrl.rotateSpeed);

        if (isOnWarningRange == false)
            ForceEnd();
    }

    //警戒結束
    public override void EndAction()
    {
        isWarning = false;
        base.EndAction();
        if (readyAttack == true)
            EnemysControlManager.Instance.melee01s.Remove(actionManager);
    }

    public override void SetActionEndTime()
    {

    }
    private void OnDrawGizmos()
    {
        if (debug == false) return;

        Gizmos.color = Color.red;
        GizmosExtensions.DrawCircle(transform.position, warningRadius);
        Gizmos.color = Color.yellow;
        GizmosExtensions.DrawCircle(transform.position, exitWarningRadius);
    }
}
