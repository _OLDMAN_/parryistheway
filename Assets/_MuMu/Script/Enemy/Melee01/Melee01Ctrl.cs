using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Melee01Ctrl : MonoBehaviour
{
    private new Melee01Animation animation;
    private Melee01ActionManager actionManager;
    private NavMeshAgent agent;
    private GameObject player;

    public float moveSpeed = 5f;
    public float rotateSpeed = 1f;

    public float moveSec = 3f;
    public float idleSec = 1f;

    private float moveTimer;
    private float idleTimer;
    [HideInInspector] public float _currentMoveSpeed;
    [HideInInspector] public float currentMoveSpeed;
    private Vector3 moveTarget;
    private bool isMove;

    void Start()
    {
        player = GameObject.FindWithTag("Player");
        GetComp();
        InitialValue();
        ChangeMoveDir();
    }

    //獲取腳本
    private void GetComp()
    {
        actionManager = GetComponent<Melee01ActionManager>();
        agent = GetComponent<NavMeshAgent>();
        animation = GetComponent<Melee01Animation>();
    }

    //初始化值
    private void InitialValue()
    {
        agent.updateRotation = false;
        _currentMoveSpeed = moveSpeed;
        moveTimer = moveSec;
    }

    void Update()
    {
        currentMoveSpeed = _currentMoveSpeed * GamefeelManager.stopSpeed;
        agent.speed = currentMoveSpeed;
        if (actionManager.isAction) return;
        //if ((GetPlayerDistance() < warning.warningRadius || warning.isOnWarningRange == true) && warning.isCD == false)
        //    actionManager.Warning();
        if (isMove)
            Move();
        else
            Idle();
    }

    //移動
    private void Move()
    {
        if (moveTimer <= 0)
        {
            if (HaveNavMesh())
            {
                agent.isStopped = true;
                agent.velocity = Vector3.zero;
            }
            moveTimer = moveSec;
            isMove = false;
            animation.PlayAnimation(Melee01Ani.Idle);
        }
        else
            moveTimer -= Time.deltaTime;

        RotateToDir();
        if (HaveNavMesh())
        {
            if (agent.remainingDistance <= agent.stoppingDistance)
                ChangeMoveDir();

            agent.SetDestination(moveTarget);
        }
    }

    //待機
    private void Idle()
    {
        if (idleTimer <= 0)
        {
            if (HaveNavMesh())
                agent.isStopped = false;
            idleTimer = idleSec;
            isMove = true;
            animation.PlayAnimation(Melee01Ani.Move);
        }
        else
        {
            idleTimer -= Time.deltaTime;
        }
    }

    //轉向某方向
    private void RotateToDir(bool forceRotate = false)
    {
        transform.forward = Vector3.Lerp(transform.forward, agent.velocity, rotateSpeed * Time.deltaTime * GamefeelManager.stopSpeed);
    }

    //改變移動方向
    private void ChangeMoveDir()
    {
        NavMeshHit hit = default;
        int minDistance = Random.Range(3, 6);
        int maxCount = 100;
        while (maxCount > 0)
        {
            Vector3 randomPos = Random.insideUnitSphere * 5 + transform.position;
            NavMesh.SamplePosition(randomPos, out hit, 10, NavMesh.AllAreas);
            maxCount--;
            if (hit.distance >= minDistance)
                break;
        }
        if (hit.hit == false)
            moveTarget = transform.position;
        else
            moveTarget = hit.position;
    }

    //是否有NavMesh
    private bool HaveNavMesh()
    {
        NavMeshData data = FindObjectOfType<NavMeshSurface>().navMeshData;
        if (data != null)
            return true;

        return false;
    }

    //獲取與玩家的距離
    private float GetPlayerDistance()
    {
        Vector3 ownPos = transform.position;
        ownPos.y = 0;
        Vector3 playerPos = player.transform.position;
        playerPos.y = 0;
        return Vector3.Distance(ownPos, playerPos);
    }
}
