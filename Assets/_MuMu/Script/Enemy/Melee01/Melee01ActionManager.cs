using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee01ActionManager : MonoBehaviour
{
    public bool isAction { get { return actionList.Count > 0; } }
    private List<Melee01Action> actionList = new List<Melee01Action>();
    private Melee01Action tempAction;
    public Melee01Action currentAction
    {
        get
        {
            if (isAction)
                return actionList[0];
            return null;
        }
    }
    public Melee01Action lastAction;

    protected Melee01Warning warning;
    protected Melee01Attack attack;
    protected Melee01Hurt hurt;
    protected Melee01Break breakAction;
    protected Melee01Dead dead;
    protected Melee01KeepAway keepAway;
    protected Melee01Skill01 skill01;
    protected Melee01Bite bite;

    private void Start()
    {
        warning = GetComponent<Melee01Warning>();
        attack = GetComponent<Melee01Attack>();
        hurt = GetComponent<Melee01Hurt>();
        breakAction = GetComponent<Melee01Break>();
        dead = GetComponent<Melee01Dead>();
        keepAway = GetComponent<Melee01KeepAway>();
        skill01 = GetComponent<Melee01Skill01>();
        bite = GetComponent<Melee01Bite>();
    }

    private void Update()
    {
        if (isAction)
            actionList[0].UpdateAction();
    }

    public void Warning()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Melee01Ani.Dead) return;
        AddAction(warning);
    }
    public void Attack()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Melee01Ani.Dead) return;
        AddAction(attack);
    }

    public void Skill01()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Melee01Ani.Dead) return;
        AddAction(skill01);
    }
    public void Bite()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Melee01Ani.Dead) return;
        AddAction(bite);
    }
    public void KeepAway()
    {
        if (currentAction != null)
        {
            if (currentAction.aniData.ani == Melee01Ani.Dead) return;
            if (currentAction.aniData.ani == Melee01Ani.Break) return;
            if (currentAction.aniData.ani == Melee01Ani.Hurt) return;
            if (currentAction.aniData.ani == Melee01Ani.Attack) return;
        }
        AddAction(keepAway);
    }

    public void Hurt()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Melee01Ani.Dead) return;
        AddAction(hurt);
    }

    public void Break()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Melee01Ani.Dead) return;
        AddAction(breakAction);
    }

    public void Dead()
    {
        AddAction(dead);
    }

    private void AddAction<T>(T status) where T : Melee01Action
    {
        tempAction = status;

        if (isAction)
        {
            if (IsActionAbleToInterupt())
            {
                actionList[0].ForceEnd();
            }
            else
            {
                return;
            }
        }

        ExcuteNextAction();
    }

    private bool IsActionAbleToInterupt()
    {
        if (tempAction.aniData.interuptOtherAnimation && actionList[0].GetType().ToString() != tempAction.GetType().ToString())
            return true;
        else if (tempAction.aniData.interuptOwnAnimation)
            return true;

        return false;
    }

    private void ExcuteNextAction()
    {
        actionList.Add(tempAction);
        actionList[0].StartAction();
        //actionList[0].PlayAnimation(actionList[0].aniData.ani);
    }

    public void RemoveCurrentAction()
    {
        lastAction = actionList[0];
        actionList.Clear();
    }
}
