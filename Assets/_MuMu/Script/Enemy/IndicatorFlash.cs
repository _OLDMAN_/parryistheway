using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IndicatorFlash : MonoBehaviour
{
    private ParticleSystem pSystem;

    private void Start()
    {
        pSystem = GetComponent<ParticleSystem>();
    }

    public void Flash(int count)
    {
        StartCoroutine(DoFlash(count - 1));
    }

    private IEnumerator DoFlash(int count)
    {
        while(count > 0)
        {
            float time = 0.15f;
            while (time > 0)
            {
                time -= Time.deltaTime * GamefeelManager.stopSpeed;
                yield return null;
            }
            pSystem.Simulate(0.0f, true, true);
            pSystem.Play();
            count--;
        }
    }
}
