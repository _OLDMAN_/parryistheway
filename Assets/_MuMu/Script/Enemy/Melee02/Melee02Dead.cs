using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee02Dead : Melee02Action
{
    private Melee02Attack attack;

    private void Awake()
    {
        attack = GetComponent<Melee02Attack>();
    }

    //���`�}�l
    public override void StartAction()
    {
        base.StartAction();
        SetDie();
    }

    //���`�]�w
    private void SetDie()
    {
        gameObject.layer = default;
        animator.StopAni();
        if (attack.attackIndicator != null)
            attack.attackIndicator.GetComponent<PoolRecycle>().Recycle();
        SoundManager.Instance.PlaySound("EnemyDie");
        ParticleManager.Instance.PlayParticle("EnemyDieBomb", transform.position + Vector3.back + Vector3.up, Quaternion.identity);
        WaveManager.enemy.Remove(gameObject);
        WaveManager.lastPos = transform.position;
        StartCoroutine(DeadDelay());
    }
    public override void SetActionEndTime()
    {

    }
    //���`����
    public override void EndAction()
    {
        base.EndAction();
    }
    private IEnumerator DeadDelay()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.CompareTag("HealthCanvas")) continue;
            transform.GetChild(i).gameObject.SetActive(false);
        }
        yield return new WaitForSeconds(1);
        Destroy(gameObject);
    }
}
