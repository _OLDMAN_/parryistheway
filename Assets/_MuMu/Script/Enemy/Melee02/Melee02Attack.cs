using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Melee02Attack : Melee02Action, IBlockable
{
    private Melee02Warning warning;
    private Melee02Hurt hurt;

    public LayerMask obsLayer;
    public LayerMask obsAndPlayerLayer;

    public int damage;
    public float attackRadius;
    public float dashSpeed;
    public float dashSpeedDecrease;

    public int continueAttack = 3;
    private int blockCounter;

    [HideInInspector] public GameObject attackIndicator;
    [HideInInspector] public Vector3 dashDir;
    private bool stopUpdateDashDir;
    private bool isBlocked;

    public bool canBlock { get; set; }
    public Transform target { get { return transform; } }

    private void Awake()
    {
        hurt = GetComponent<Melee02Hurt>();
        warning = GetComponent<Melee02Warning>();
        blockCounter = continueAttack;
    }

    //攻擊開始
    public override void StartAction()
    {
        base.StartAction();
        ParticleManager.Instance.PlayParticle("Melee02PreAttack", transform.position, transform.rotation);
        hurt.superArmor = true;
        isBlocked = false;
        stopUpdateDashDir = false;
    }

    //攻擊中
    public override void UpdateAction()
    {
        if (stopUpdateDashDir == true) return;

        RaycastHit checkDir = GetCapCastHit(GetPlayerDir(), 100, obsAndPlayerLayer, 2);
        if (checkDir.collider != null)
        {
            if (checkDir.collider.CompareTag("Player"))
                dashDir = GetPlayerDir();
        }
        if (attackIndicator != null)
            attackIndicator.transform.forward = dashDir;
    }

    //攻擊結束
    public override void EndAction()
    {
        EnemysControlManager.Instance.currentAttackerCount--;
        hurt.superArmor = false;
        base.EndAction();
        if (isBlocked == false)
        {
            warning.attackTimer = warning.attackRequidSecRand;
            if (attackIndicator != null)
            {
                attackIndicator.GetComponent<PoolRecycle>().Recycle();
                attackIndicator = null;
            }
        }
        else if (blockCounter > 0)
        {
            warning.attackTimer = 0.1f;
            actionManager.Attack();
        }
        else
            warning.attackTimer = warning.attachCD;
    }
    public void PreEnd()
    {
        animator.PlayAnimation(Melee02Ani.Attack);
    }
    //攻擊偵
    public void AttackFrame()
    {
        SoundManager.Instance.PlaySound("Shoot");
        ParticleManager.Instance.PlayParticle("Melee02Attack", transform.position, Quaternion.LookRotation(Vector3.up), transform);
        StartCoroutine(DashAttack());
    }

    //生成持續指示器
    public void SpawnAttackIndicator()
    {
        attackIndicator = ParticleManager.Instance.GetParticle("AttackIndicator", new Vector3(transform.position.x, 0.1f, transform.position.z), Quaternion.LookRotation(GetPlayerDir()));
        attackIndicator.GetComponent<Indicator>().StartIndicat(0.5f);
    }

    //生成閃爍指示器
    public void SpawnAttackIndicatorBurstFrame()
    {
        GameObject o = ParticleManager.Instance.GetParticle("AttackIndicatorBurst", new Vector3(transform.position.x, 0.1f, transform.position.z), Quaternion.LookRotation(dashDir));
        o.GetComponent<IndicatorFlash>().Flash(2);

        attackIndicator.GetComponent<PoolRecycle>().Recycle();
        attackIndicator = null;
        stopUpdateDashDir = true;
    }

    //衝刺
    private IEnumerator DashAttack()
    {
        float dashSpeedCount = dashSpeed;
        canBlock = true;
        while (dashSpeedCount > 2 && canBlock == true )
        {
            Collider[] colliders = GetCapOverLap(dashDir * 0.2f, obsAndPlayerLayer);
            foreach (Collider collider in colliders)
            {
                if (collider.CompareTag("Player"))
                    collider.GetComponent<PlayerActionManager>().StartHurt(damage, dashDir, true);
            }
            if (colliders.Length != 0)
                break;
            dashSpeedCount -= Time.deltaTime * dashSpeedDecrease;
            transform.position += Time.deltaTime * dashSpeedCount * dashDir * GamefeelManager.stopSpeed;
            yield return null;
        }
        canBlock = false;
    }
    public void Block(GameObject attacker)
    {
        canBlock = false;
        blockCounter--;
        isBlocked = true;
        ParticleManager.Instance.PlayParticle("EnemyBlock", attacker.transform.position, attacker.transform.rotation);
        StartCoroutine(BlockedBack());
        if (blockCounter <= 0)
        {
            actionManager.Break();
            blockCounter = continueAttack;
        }
    }
    //玩家擋回
    private IEnumerator BlockedBack()
    {
        float backDistance = 2f;
        RaycastHit hit = GetCapCastHit(-dashDir, backDistance, obsLayer);
        if (hit.collider != null)
            backDistance = hit.distance;
        float speed = backDistance / 0.3f;
        while (backDistance > 0)
        {
            transform.position += -dashDir * Time.deltaTime * speed;
            backDistance -= Time.deltaTime * speed;
            yield return null;
        }
    }

    //設定結束時間 = 前搖 + 攻擊
    public override void SetActionEndTime()
    {
        float actionLength = 0;
        actionLength += GetAnimationLength(aniData.ani);
        actionLength += GetAnimationLength(Melee02Ani.Attack);
        coroutines.Add(StartCoroutine(TickToEndAction(actionLength)));
    }

    //畫攻擊範圍
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        GizmosExtensions.DrawCircle(transform.position, attackRadius);
    }
}