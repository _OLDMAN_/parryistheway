using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee02Break : Melee02Action
{
    public int maxBreakCount = 10;
    public float dizzyTime = 2;
    public Vector3 dizzyOffset = new Vector3(0, 2, 0);

    [HideInInspector] public int breakCounter;
    [HideInInspector] public bool isBreaking;

    private void Awake()
    {
        breakCounter = maxBreakCount;
    }

    //破防開始
    public override void StartAction()
    {
        base.StartAction();
        SoundManager.Instance.PlaySound("ShieldBreak");
        isBreaking = true;
        agent.isStopped = true;
        agent.velocity = Vector3.zero;
        coroutines.Add(StartCoroutine(Dizzy()));
    }
    private IEnumerator Dizzy()
    {
        float time = GetAnimationLength(Melee02Ani.Break) + dizzyTime;
        GameObject dizzyObj = ParticleManager.Instance.GetParticle("EnemyDizzy", transform.position.GetZeroY() + dizzyOffset, Quaternion.identity, transform);
        while (time > 0)
        {
            time -= Time.deltaTime * GamefeelManager.stopSpeed;
            yield return null;
        }
        dizzyObj.GetComponent<PoolRecycle>().Recycle();
    }
    //破防結束
    public override void EndAction()
    {
        base.EndAction();
        isBreaking = true;
        breakCounter = maxBreakCount;
    }

    //設定破防結束時間 = 破防前搖 + 暈眩時間 + 恢復時間
    public override void SetActionEndTime()
    {
        float actionLength = 0;
        actionLength += GetAnimationLength(aniData.ani);
        actionLength += GetAnimationLength(Melee02Ani.BreakRecover);
        actionLength += dizzyTime;
        coroutines.Add(StartCoroutine(TickToEndAction(actionLength)));
    }
}
