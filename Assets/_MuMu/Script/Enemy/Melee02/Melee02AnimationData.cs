using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Melee02AnimationData
{
    public Melee02Ani ani;
    public bool interuptOtherAnimation = false;
    public bool interuptOwnAnimation = false;
}

public enum Melee02Ani
{
    Idle, Move, Attack, PreAttack, Hurt, Break, BreakRecover, Dead
}
