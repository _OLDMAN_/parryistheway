using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Melee02Ctrl : MonoBehaviour
{
    private Melee02ActionManager actionManager;
    private Melee02Warning warning;

    private NavMeshAgent agent;
    private GameObject player;

    public float moveSpeed = 5f;
    public float rotateSpeed = 1f;

    public float moveSec = 3f;
    public float idleSec = 1f;

    private float moveTimer;
    private float idleTimer;

    private float oriMoveSpeed;
    public Vector3 moveTarget;
    private bool isMove;

    void Start()
    {
        actionManager = GetComponent<Melee02ActionManager>();
        warning = GetComponent<Melee02Warning>();
        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        agent.speed = moveSpeed;
        player = GameObject.FindWithTag("Player");
        oriMoveSpeed = moveSpeed;
        moveTimer = moveSec;
        ChangeMovePos();
    }

    void Update()
    {
        if (actionManager.isAction) return;

        if (isMove)
            Move();
        else
            Idle();
    }

    private void Move()
    {
        if (moveTimer <= 0)
        {
            if (HaveNavMesh())
            {
                agent.isStopped = true;
                agent.velocity = Vector3.zero;
            }

            moveTimer = moveSec;
            isMove = false;
        }
        else
        {
            moveTimer -= Time.deltaTime;
            transform.Rotate(Vector3.up * rotateSpeed * Mathf.Clamp(moveTimer, 0,1));
        }


        if (HaveNavMesh())
        {
            if (agent.remainingDistance <= agent.stoppingDistance)
                ChangeMovePos();

            agent.SetDestination(moveTarget);
        }
    }

    private void Idle()
    {
        if (idleTimer <= 0)
        {
            if (HaveNavMesh())
                agent.isStopped = false;
            idleTimer = idleSec;
            isMove = true;
        }
        else
            idleTimer -= Time.deltaTime;
    }

    private void ChangeMovePos()
    {
        NavMeshHit hit = default;
        int minDistance = Random.Range(3, 6);
        int maxCount = 100;
        while (maxCount > 0)
        {
            Vector3 randomPos = Random.insideUnitSphere * 5 + transform.position;
            NavMesh.SamplePosition(randomPos, out hit, 10, NavMesh.AllAreas);
            maxCount--;
            if (hit.distance >= minDistance)
                break;
        }
        if (hit.hit == false)
            moveTarget = transform.position;
        else
            moveTarget = hit.position;
    }

    private bool HaveNavMesh()
    {
        NavMeshData data = FindObjectOfType<NavMeshData>();
        if (data != null)
            return true;

        return false;
    }

    private float GetPlayerDistance()
    {
        Vector3 ownPos = transform.position;
        ownPos.y = 0;
        Vector3 playerPos = player.transform.position;
        playerPos.y = 0;
        return Vector3.Distance(ownPos, playerPos);
    }
}
