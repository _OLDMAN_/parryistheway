using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Melee02Warning : Melee02Action
{
    public LayerMask obsAndPlayerLayer;
    private Melee02Attack attack;
    private Melee02Hurt hurt;

    public float attachCD;
    
    public bool debug;
    public float warningRadius = 5f;
    public float exitWarningRadius = 8f;

    [HideInInspector] public float attackTimer;
    public float attackRequidSecRand { 
        get {
            return Random.Range(attachCD - attachCD / 4, attachCD + attachCD / 4);
        } 
    }

    [HideInInspector] public bool isOnWarningRange;
    [HideInInspector] public bool isFindAttack;

    private bool isWarning;
    private bool readyAttack;

    private void Awake()
    {
        attack = GetComponent<Melee02Attack>();
        hurt = GetComponent<Melee02Hurt>();
    }

    private void Update()
    {
        if ((GetPlayerDistance() < warningRadius || isOnWarningRange == true) && isWarning == false && actionManager.isAction == false)
            actionManager.Warning();

        if (isOnWarningRange == true)
        {
            if (attackTimer > 0)
                attackTimer -= Time.deltaTime;
            if (GetPlayerDistance() > exitWarningRadius)
                isOnWarningRange = false;
        }
    }

    public override void StartAction()
    {
        base.StartAction();
        Initialize();
    }

    //初始化值
    private void Initialize()
    {
        agent.updateRotation = false;
        agent.isStopped = false;
        isFindAttack = false;
        isOnWarningRange = true;
        agent.speed = 1;
        readyAttack = false;
    }

    public override void UpdateAction()
    {
        if (readyAttack == true) return;

        if (attackTimer <= 0)
        {
            if (hurt.isKnockBack == true) return;

            RaycastHit hit = GetCapCastHit(GetPlayerDir(), 100, obsAndPlayerLayer, 2);
            if (hit.collider != null)
            {
                if (hit.collider.CompareTag("Player") && GetPlayerDistance() < attack.attackRadius)
                {
                    EnemysControlManager.Instance.melee02s.Add(actionManager);
                    readyAttack = true;
                }
            }
        }

        agent.SetDestination(player.transform.position);

        if (GetPlayerDistance() - attack.attackRadius > 0)
            transform.Rotate(Vector3.up * 3 * Mathf.Clamp(GetPlayerDistance() - attack.attackRadius, 0, 1));
        else
            transform.Rotate(Vector3.up * 3 * Mathf.Clamp(attackTimer, 0, 1));

        if (isOnWarningRange == false)
        {
            ForceEnd();
        }

    }
    //警戒結束
    public override void EndAction()
    {
        base.EndAction();
        if (readyAttack == true)
            EnemysControlManager.Instance.melee02s.Remove(actionManager);
        agent.isStopped = true;
        agent.velocity = Vector3.zero;
    }
    public override void SetActionEndTime()
    {

    }

    //畫警戒範圍
    private void OnDrawGizmos()
    {
        if (debug == false) return;

        Gizmos.color = Color.red;
        GizmosExtensions.DrawCircle(transform.position, warningRadius);
        Gizmos.color = Color.yellow;
        GizmosExtensions.DrawCircle(transform.position, exitWarningRadius);
    }
}
