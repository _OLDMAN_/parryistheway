using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee02AniFrame : MonoBehaviour
{
    public Melee02Attack attack;

    public void PreEnd()
    {
        attack.PreEnd();
    }

    public void AttackFrame()
    {
        attack.AttackFrame();
    }

    public void SpawnAttackIndicatorFrame()
    {
        attack.SpawnAttackIndicator();
    }

    public void SpawnAttackIndicatorBurstFrame()
    {
        attack.SpawnAttackIndicatorBurstFrame();
    }
}
