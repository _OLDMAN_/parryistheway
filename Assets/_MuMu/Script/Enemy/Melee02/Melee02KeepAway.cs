using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Melee02KeepAway : Melee02Action
{
    private Melee02Warning warning;

    public float keepAwaySpeed = 1;
    [Range(0, 1)]
    public float warnSecPercentage = 0.5f;
    private float keepAwayTime;
    private Vector3 movePos;
    private Vector3 moveDir;

    private void Awake()
    {
        warning = GetComponent<Melee02Warning>();
    }

    //逃離開始
    public override void StartAction()
    {
        keepAwayTime = warnSecPercentage * warning.attachCD;
        base.StartAction();
        agent.isStopped = false;
        agent.speed = keepAwaySpeed;
        ChangeKeepAwayPoint();
    }

    //逃離中
    public override void UpdateAction()
    {
        if (warning.attackTimer <= warning.attachCD * warnSecPercentage || warning.isOnWarningRange == false)
        {
            ForceEnd();
        }

        float dot = Vector3.Dot(GetPlayerDir(), moveDir);
        if (agent.remainingDistance <= agent.stoppingDistance || dot > 0)
            ChangeKeepAwayPoint();
        MoveRotate();
    }

    //改變逃離點
    private void ChangeKeepAwayPoint()
    {
        NavMeshHit hit = default;
        int minDistance = Random.Range(3, 6);
        int maxCount = 100;
        while (maxCount > 0)
        {
            Vector3 randomPos = Random.insideUnitSphere * 5 + transform.position;
            NavMesh.SamplePosition(randomPos, out hit, 10, NavMesh.AllAreas);
            Vector3 moveDir = hit.position - transform.position;
            moveDir.y = 0;
            maxCount--;
            float dot = Vector3.Dot(GetPlayerDir(), moveDir);
            if (hit.distance >= minDistance && dot <= 0)
                break;
        }

        if (hit.hit == false)
            movePos = transform.position;
        else
            movePos = hit.position;
        moveDir = movePos - transform.position;
        moveDir.y = 0;
        agent.SetDestination(movePos);
    }

    //旋轉
    private void MoveRotate()
    {
        transform.Rotate(Vector3.up * 3);
    }

    //設定結束時間
    public override void SetActionEndTime()
    {
        coroutines.Add(StartCoroutine(TickToEndAction(keepAwayTime)));
    }

    //逃離結束
    public override void EndAction()
    {
        base.EndAction();
    }
}
