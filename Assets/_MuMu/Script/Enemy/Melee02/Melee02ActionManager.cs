using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Melee02ActionManager : MonoBehaviour
{
    public bool isAction { get { return actionList.Count > 0; } }
    private List<Melee02Action> actionList = new List<Melee02Action>();
    private Melee02Action tempAction;
    public Melee02Action currentAction
    {
        get
        {
            if (isAction)
                return actionList[0];
            return null;
        }
    }
    public Melee02Action lastAction;

    private Melee02Warning warning;
    private Melee02Attack attack;
    private Melee02Hurt hurt;
    private Melee02Break breakAction;
    private Melee02Dead dead;
    private Melee02KeepAway keepAway;

    private void Start()
    {
        warning = GetComponent<Melee02Warning>();
        attack = GetComponent<Melee02Attack>();
        hurt = GetComponent<Melee02Hurt>();
        breakAction = GetComponent<Melee02Break>();
        dead = GetComponent<Melee02Dead>();
        keepAway = GetComponent<Melee02KeepAway>();
    }

    private void Update()
    {
        if (isAction)
            actionList[0].UpdateAction();
    }

    public void Warning()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Melee02Ani.Dead) return;
        AddAction(warning);
    }

    public void Attack()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Melee02Ani.Dead) return;
        AddAction(attack);
    }
    public void KeepAway()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Melee02Ani.Dead) return;
        AddAction(keepAway);
    }

    public void Hurt()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Melee02Ani.Dead) return;
        AddAction(hurt);
    }

    public void Break()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Melee02Ani.Dead) return;
        AddAction(breakAction);
    }

    public void Dead()
    {
        AddAction(dead);
    }

    private void AddAction<T>(T status) where T : Melee02Action
    {
        tempAction = status;

        if (isAction)
        {
            if (IsActionAbleToInterupt())
            {
                actionList[0].ForceEnd();
            }
            else
            {
                return;
            }
        }

        ExcuteNextAction();
    }

    private bool IsActionAbleToInterupt()
    {
        if (tempAction.aniData.interuptOtherAnimation && !(actionList[0].GetType().ToString() == tempAction.GetType().ToString()))
            return true;
        else if (tempAction.aniData.interuptOwnAnimation)
            return true;

        return false;
    }

    private void ExcuteNextAction()
    {
        actionList.Add(tempAction);
        actionList[0].StartAction();
        actionList[0].PlayAnimation(actionList[0].aniData.ani);
    }

    public void RemoveCurrentAction()
    {
        lastAction = actionList[0];
        actionList.Clear();
    }
}
