using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Melee02Action : MonoBehaviour
{
    protected Melee02ActionManager actionManager;
    protected EnemyAnimation animator;
    protected NavMeshAgent agent;
    protected GameObject player;
    protected PlayerController playerCtrl;
    protected CapsuleCollider m_collider;

    protected List<Coroutine> coroutines = new List<Coroutine>();
    public Melee02AnimationData aniData;

    public virtual void Start()
    {
        actionManager = GetComponent<Melee02ActionManager>();
        m_collider = GetComponent<CapsuleCollider>();
        animator = GetComponent<EnemyAnimation>();
        agent = GetComponent<NavMeshAgent>();
        player = GameObject.FindWithTag("Player");
        playerCtrl = player.GetComponent<PlayerController>();
    }

    public virtual void StartAction()
    {
        SetActionEndTime();
    }

    public virtual void UpdateAction()
    {

    }

    public virtual void EndAction()
    {
        actionManager.RemoveCurrentAction();
    }

    public void PlayAnimation(Melee02Ani ani)
    {
        animator.PlayAnimation(ani);
    }

    public virtual void SetActionEndTime()
    {
        if (animator.GetAnimationHolder(aniData.ani.ToString()).clip != null)
        {
            float actionLength = GetAnimationLength(aniData.ani);
            coroutines.Add(StartCoroutine(TickToEndAction(actionLength)));
        }
    }

    protected float GetAnimationLength(Melee02Ani ani)
    {
        return animator.GetAnimationHolder(ani.ToString()).Length;
    }

    public void ForceEnd()
    {
        foreach (Coroutine c in coroutines)
        {
            StopCoroutine(c);
        }
        EndAction();
    }

    protected IEnumerator TickToEndAction(float aniSec)
    {
        yield return new WaitForSeconds(aniSec);
        EndAction();
    }

    protected float GetPlayerDistance()
    {
        return Vector3.Distance(player.transform.position.GetZeroY(), transform.position.GetZeroY());
    }

    protected Vector3 GetPlayerDir()
    {
        return (player.transform.position.GetZeroY() - transform.position.GetZeroY()).normalized;
    }

    protected Collider[] GetCapOverLap(Vector3 startOffset, LayerMask layerMask)
    {
        Vector3 ownPos = new Vector3(transform.position.x, 0, transform.position.z);
        Collider [] colliders = Physics.OverlapCapsule(startOffset + ownPos, startOffset + ownPos + Vector3.up, m_collider.radius, layerMask);

        return colliders;
    }

    protected RaycastHit GetCapCastHit(Vector3 dir, float distance, LayerMask layerMask, float offset = 0)
    {
        RaycastHit hit;
        Vector3 ownPos = new Vector3(transform.position.x, 0, transform.position.z);
        dir.y = 0;
        Physics.CapsuleCast(ownPos + offset * -dir, ownPos + offset * -dir + Vector3.up, m_collider.radius, dir, out hit, distance, layerMask);

        return hit;
    }
    protected RaycastHit GetCapCastHit(Vector3 dir, float radius, float distance, LayerMask layerMask, float offset = 0)
    {
        RaycastHit hit;
        Vector3 ownPos = new Vector3(transform.position.x, 0, transform.position.z);
        dir.y = 0;
        Physics.CapsuleCast(ownPos + offset * -dir, ownPos + offset * -dir + Vector3.up, radius, dir, out hit, distance, layerMask);

        return hit;
    }
}
