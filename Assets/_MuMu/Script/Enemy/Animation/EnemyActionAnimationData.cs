using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class EnemyActionAnimationData
{
    public string ani;
    public bool interuptOtherAnimation = false;
    public bool interuptOwnAnimation = false;
}
