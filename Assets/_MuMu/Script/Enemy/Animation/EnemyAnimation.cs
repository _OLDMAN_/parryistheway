using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAnimation : MonoBehaviour
{
    public Animator animator;
    protected string aniName = "Idle";
    private float blendEndTime = 0;
    private float blendTimeElapsed = 1;

    public bool isBlending { get; private set; }

    //Inspector variables
    [Tooltip("Time to blend to new animation,percentage 0~1")]
    public float blendTime = 0.1f;

    public List<AnimationHolder> aniList = new List<AnimationHolder>();
    public void StopAni()
    {
        animator.speed = 0;
    }

    public virtual void Start()
    {
        animator = GetComponent<Animator>();
    }

    private void Update()
    {
        if (isBlending == false) return;
        BlendAnimation();
    }

    public virtual void PlayAnimation<T>(T name, bool skipWhenRepeat = true, bool blendPlay = true)
    {
        string n = name.ToString();
        if (aniName == n && skipWhenRepeat) return;

        aniName = n;

        if (blendPlay)
        {
            StartBlend();
        }
        else
        {
            ForcePlay();
        }
    }
    private void ForcePlay()
    {
        blendTimeElapsed = 0;
        blendEndTime = 0;
        animator.Play(aniName);
        isBlending = false;
    }

    private void StartBlend()
    {
        blendTimeElapsed = 0;
        blendEndTime = GetAnimationHolder(aniName).Length * blendTime;
        animator.CrossFadeInFixedTime(aniName, blendEndTime);
        isBlending = true;
    }

    private void BlendAnimation()
    {
        if (isBlending == false) return;
        if (blendTimeElapsed < blendEndTime)
        {
            blendTimeElapsed += Time.deltaTime;
            return;
        }
        EndBlend();
    }

    private void EndBlend()
    {
        animator.Play(aniName);
        isBlending = false;
    }

    public AnimationHolder GetAnimationHolder(string name)
    {
        for (int i = 0; i < aniList.Count; i++)
        {
            if (aniList[i].name == name)
            {
                return aniList[i];
            }
        }
        return null;
    }
}
