using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range01Action : MonoBehaviour
{
    protected Range01Hurt hurt;
    protected Range01ActionManager actionManager;
    protected Range01Animation animator;
    protected GameObject player;
    protected CapsuleCollider m_collider;

    public Range01AnimationData aniData;

    public virtual void Start()
    {
        hurt = GetComponent<Range01Hurt>();
        actionManager = GetComponent<Range01ActionManager>();
        m_collider = GetComponent<CapsuleCollider>();
        animator = GetComponent<Range01Animation>();
        player = GameObject.FindWithTag("Player");
    }

    public virtual void StartAction()
    {
        SetActionEndTime();
    }

    public virtual void UpdateAction()
    {

    }

    public virtual void EndAction()
    {
        actionManager.RemoveCurrentAction();
    }

    public void PlayAnimation(Range01Ani ani)
    {
        animator.PlayAnimation(ani);
    }

    public virtual void SetActionEndTime()
    {
        if (animator.GetAnimationHolder(aniData.ani.ToString()).clip != null)
        {
            float actionLength = GetAnimationLength(aniData.ani);
            StartCoroutine(TickToEndAction(actionLength));
        }
    }

    protected float GetAnimationLength(Range01Ani ani)
    {
        return animator.GetAnimationHolder(ani.ToString()).Length;
    }

    public void ForceEnd()
    {
        StopAllCoroutines();
        EndAction();
    }

    protected IEnumerator TickToEndAction(float aniSec)
    {
        yield return new WaitForSeconds(aniSec);
        EndAction();
    }

    protected float GetPlayerDistance()
    {
        return Vector3.Distance(player.transform.position.GetZeroY(), transform.position.GetZeroY());
    }

    protected Vector3 GetPlayerDir()
    {
        return (player.transform.position.GetZeroY() - transform.position.GetZeroY()).normalized;
    }

    protected RaycastHit GetCapCastHit(Vector3 dir, float distance, LayerMask layerMask)
    {
        RaycastHit hit = new RaycastHit();
        Vector3 ownPos = new Vector3(transform.position.x, 0, transform.position.z);
        Physics.CapsuleCast(ownPos, ownPos + Vector3.up, m_collider.radius, dir, out hit, distance, layerMask);

        return hit;
    }
}
