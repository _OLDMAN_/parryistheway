using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range01ActionManager : MonoBehaviour
{
    public bool isAction { get { return actionList.Count > 0; } }
    private List<Range01Action> actionList = new List<Range01Action>();
    private Range01Action tempAction;
    public Range01Action currentAction
    {
        get
        {
            if (isAction)
                return actionList[0];
            return null;
        }
    }
    public Range01Action lastAction;

    private Range01Warning warning;
    private Range01Attack attack;
    private Range01Hurt hurt;
    private Range01Break breakAction;
    private Range01Dead dead;
    private Range01Skill01 skill01;
    private Range01Drail drail;

    private void Start()
    {
        warning = GetComponent<Range01Warning>();
        attack = GetComponent<Range01Attack>();
        hurt = GetComponent<Range01Hurt>();
        breakAction = GetComponent<Range01Break>();
        dead = GetComponent<Range01Dead>();
        skill01 = GetComponent<Range01Skill01>();
        drail = GetComponent<Range01Drail>();
    }

    private void Update()
    {
        if (isAction)
            actionList[0].UpdateAction();
    }

    public void Warning()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Range01Ani.Dead) return;
        AddAction(warning);
    }

    public void Attack()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Range01Ani.Dead) return;
        AddAction(attack);
    }

    public void Skill01()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Range01Ani.Dead) return;
        AddAction(skill01);
    }

    public void Hurt()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Range01Ani.Dead) return;
        AddAction(hurt);
    }

    public void Break()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Range01Ani.Dead) return;
        AddAction(breakAction);
    }
    public void Drail()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Range01Ani.Dead) return;
        AddAction(drail);
    }

    public void Dead()
    {
        AddAction(dead);
    }

    private void AddAction<T>(T status) where T : Range01Action
    {
        tempAction = status;

        if (isAction)
        {
            if (IsActionAbleToInterupt())
            {
                actionList[0].ForceEnd();
            }
            else
            {
                return;
            }
        }

        ExcuteNextAction();
    }

    private bool IsActionAbleToInterupt()
    {
        if (tempAction.aniData.interuptOtherAnimation && !(actionList[0].GetType().ToString() == tempAction.GetType().ToString()))
            return true;
        else if (tempAction.aniData.interuptOwnAnimation)
            return true;

        return false;
    }

    private void ExcuteNextAction()
    {
        actionList.Add(tempAction);
        actionList[0].StartAction();
        actionList[0].PlayAnimation(actionList[0].aniData.ani);
    }

    public void RemoveCurrentAction()
    {
        lastAction = actionList[0];
        actionList.Clear();
    }
}
