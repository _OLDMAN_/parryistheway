using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range01Break : Range01Action
{
    public int breakCount = 10;
    public float dizzyTime = 2;

    [HideInInspector] public int breakCounter;
    [HideInInspector] public bool isBreaking;

    private void Awake()
    {
        breakCounter = breakCount;
    }

    //破防開始
    public override void StartAction()
    {
        base.StartAction();
        SoundManager.Instance.PlaySound("ShieldBreak");
        isBreaking = true;
    }

    public void BreakEnd()
    {
        animator.PlayAnimation(Range01Ani.Dizzy);
        StartCoroutine(DizzyEnd());
    }

    //暈眩動畫結束
    private IEnumerator DizzyEnd()
    {
        GameObject dizzyObj = ParticleManager.Instance.GetParticle("EnemyDizzy", transform.position.GetZeroY() + Vector3.up * 2, Quaternion.identity, transform);
        yield return new WaitForSeconds(dizzyTime);
        dizzyObj.GetComponent<PoolRecycle>().Recycle();
        animator.PlayAnimation(Range01Ani.BreakRecover);
    }

    //破防結束
    public override void EndAction()
    {
        base.EndAction();
        isBreaking = false;
        breakCounter = breakCount;
    }

    //設定結束時間 = 破防前搖 + 暈眩時間 + 恢復時間
    public override void SetActionEndTime()
    {
        float actionLength = 0;
        actionLength += GetAnimationLength(aniData.ani);
        actionLength += dizzyTime;
        actionLength += GetAnimationLength(Range01Ani.BreakRecover);
        StartCoroutine(TickToEndAction(actionLength));
    }
}