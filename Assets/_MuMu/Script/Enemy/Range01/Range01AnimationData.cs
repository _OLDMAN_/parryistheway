using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Range01AnimationData
{
    public Range01Ani ani;
    public bool interuptOtherAnimation = false;
    public bool interuptOwnAnimation = false;
}

public enum Range01Ani
{
    Idle, PreAttack, Attack, Hurt, Break, Dizzy, BreakRecover, Dead, Drail, DrailBack, Skill01
}
