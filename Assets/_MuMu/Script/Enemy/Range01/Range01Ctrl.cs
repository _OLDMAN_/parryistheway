using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range01Ctrl : MonoBehaviour
{
    private GameObject player;
    public GameObject rotater;

    public float rotateSpeed = 1f;

    void Start()
    {
        player = GameObject.FindWithTag("Player");
    }

    //��V���a
    public void LookPlayer()
    {
        Vector3 dir = player.transform.position - transform.position;
        dir.y = 0;
        dir.Normalize();
        rotater.transform.rotation = Quaternion.Lerp(rotater.transform.rotation, Quaternion.LookRotation(-dir), rotateSpeed * Time.deltaTime);
    }
}
