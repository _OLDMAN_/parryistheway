using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.AI;

public class Range01Drail : Range01Action
{
    private EnemyHealth health;
    public float radius = 4; 

    public override void Start()
    {
        base.Start();
        health = GetComponent<EnemyHealth>();
    }

    public override void StartAction()
    {
        base.StartAction();
        ParticleManager.Instance.PlayParticle("Range01Drail", transform.position, transform.rotation);
        SoundManager.Instance.PlaySound("FlowerDrill");
    }

    public void DrailDown()
    {
        hurt.invincible = true;
        health.HideHealth();
        transform.DOMoveY(-4, 1.5f).OnComplete(()=> {
            Vector2 rand2 = Random.insideUnitCircle * radius;
            Vector3 targetPos = new Vector3(rand2.x, transform.position.y, rand2.y) + player.transform.position;
            NavMesh.SamplePosition(targetPos, out NavMeshHit hit, 5, NavMesh.AllAreas);
            transform.position = new Vector3(hit.position.x, transform.position.y, hit.position.z);
            transform.eulerAngles = new Vector3(0, Random.Range(0, 360), 0);
            StartCoroutine(DelayDrailUp(targetPos));
        });
    }

    private IEnumerator DelayDrailUp(Vector3 targetPos)
    {
        ParticleManager.Instance.PlayParticle("Range01DrailWarning", targetPos.GetZeroY(), transform.rotation);
        SoundManager.Instance.PlaySound("FlowerDrill");
        yield return new WaitForSeconds(1);
        PlayAnimation(Range01Ani.DrailBack);
    }

    public void DrailUp()
    {
        Vector3 pos = transform.position;
        ParticleManager.Instance.PlayParticle("Range01DrailUp", pos.GetZeroY(), transform.rotation);
        SoundManager.Instance.PlaySound("FlowerDrillUp");
        transform.DOMoveY(0, 0.7f).SetEase(Ease.OutCubic).OnComplete(() => {
            health.DisplayHealth();
            ForceEnd();
        });
    }
    public override void EndAction()
    {
        hurt.invincible = false;
        base.EndAction();
    }

    public override void SetActionEndTime()
    {
        
    }
}
