﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FlowerSkill01Type
{ 周圍隨機拋射, 定向隨機噴射 }

[System.Serializable]
public class FlowerSkill01Setting
{
    public bool debug;
    public FlowerSkill01Type type;
    public float innerRadius;
    public float outRadius;

    [Range(0, 360)]
    public int angle;
    public bool splitAngleWithCount;
    public int count;
    public float needTime;
    public int splitAngle
    {
        get { return Mathf.RoundToInt(angle / (float)count); }
    }
}

public class Range01Skill01 : Range01Action
{
    private Range01Warning warning;
    private Range01Ctrl ctrl;

    public Transform muzzle;
    public FlowerSkill01Setting skill01_Around;
    public FlowerSkill01Setting skill01_Range;

    private void Awake()
    {
        ctrl = GetComponent<Range01Ctrl>();
        warning = GetComponent<Range01Warning>();
    }

    public override void StartAction()
    {
        base.StartAction();
    }

    //技能攻擊偵
    public void Skill01AttackFrame()
    {
        int randNum = Random.Range(0, 2);
        if (randNum == 0)
            Shot(skill01_Around);
        else
            Shot(skill01_Range);
    }

    //射擊
    private void Shot(FlowerSkill01Setting setting)
    {
        SoundManager.Instance.PlaySound("PlayerThrowShield");
        for (int i = 0; i < setting.count; i++)
        {
            int angle = 0;
            if(setting.splitAngleWithCount == true)
                angle = setting.splitAngle * (i + 1);
            else
                angle = Random.Range(-setting.angle / 2, setting.angle / 2);
            float radius = Random.Range(setting.innerRadius, setting.outRadius);
            Vector3 targetPos = transform.position + Quaternion.Euler(0, angle - 90, 0) * GetPlayerDir() * radius;

            GameObject g = ParticleManager.Instance.GetParticle("Range01Skill01", muzzle.transform.position, transform.rotation);
            GameObject p = ParticleManager.Instance.GetParticle("Range01Skill01Warning", targetPos.GetZeroY(0.1f), Quaternion.identity, Vector3.one * 3.5f);
            g.GetComponent<FlowerSkillBullet>().SetFallPoint(targetPos, setting.needTime, p);
        }
    }

    public override void EndAction()
    {
        warning.attackTimer = warning.attackRequidSecRand;
        base.EndAction();
    }
    private void OnValidate()
    {
        skill01_Around.innerRadius = Mathf.Clamp(skill01_Around.innerRadius, 0, skill01_Around.outRadius);
        skill01_Around.outRadius = Mathf.Clamp(skill01_Around.outRadius, skill01_Around.innerRadius, float.MaxValue);

        skill01_Range.innerRadius = Mathf.Clamp(skill01_Range.innerRadius, 0, skill01_Range.outRadius);
        skill01_Range.outRadius = Mathf.Clamp(skill01_Range.outRadius, skill01_Range.innerRadius, float.MaxValue);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        if (skill01_Around.debug == true)
        {
            GizmosExtensions.DrawHollowCircleArea(transform.position, skill01_Around.innerRadius, skill01_Around.outRadius);

        }

        if (skill01_Range.debug == true)
        {
            GizmosExtensions.DrawWireArc(transform.position, -transform.forward, skill01_Range.angle, skill01_Range.innerRadius);
            GizmosExtensions.DrawWireArc(transform.position, -transform.forward, skill01_Range.angle, skill01_Range.outRadius);
        }
    }
}