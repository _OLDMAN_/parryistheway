using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Range01SkillWeight
{
    public Range01Ani name;
    public int weight;
}

public class Range01Rank3SkillWeight : MonoBehaviour
{
    public List<Range01SkillWeight> skillWeights;
    private Dictionary<string, int> dic = new Dictionary<string, int>();

    private void Start()
    {
        foreach(var skill in skillWeights)
        {
            dic.Add(skill.name.ToString(), skill.weight);
        }
    }

    public Range01Ani GetWeightSkill()
    {
        string aniName = dic.GetWeight();
        if (aniName == Range01Ani.PreAttack.ToString())
            return Range01Ani.PreAttack;
        else if (aniName == Range01Ani.Drail.ToString())
            return Range01Ani.Drail;
        else if (aniName == Range01Ani.Skill01.ToString())
            return Range01Ani.Skill01;

        Debug.Log("No Find Skill");
        return Range01Ani.Idle;
    }
}
