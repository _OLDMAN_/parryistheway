using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range01Rank3_Attack : Range01Attack
{
    private Range01Rank3SkillWeight skillWeight;
    public override void Awake()
    {
        base.Awake();
        skillWeight = GetComponent<Range01Rank3SkillWeight>();
    }

    public override void StartAction()
    {
        Range01Ani skill = skillWeight.GetWeightSkill();
        if (skill == Range01Ani.PreAttack)
            base.StartAction();
        else if (skill == Range01Ani.Skill01)
            actionManager.Skill01();
        else if (skill == Range01Ani.Drail)
            actionManager.Drail();
    }
}
