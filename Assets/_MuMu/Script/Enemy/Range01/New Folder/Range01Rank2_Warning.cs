using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range01Rank2_Warning : Range01Warning
{
    public override void UpdateAction()
    {
        if (attackTimer <= 0)
        {
            ForceEnd();
            actionManager.Attack();
        }

        if (isOnWarningRange == false)
        {
            ForceEnd();
            attackTimer = Random.Range(attackCD - attackCD / 4, attackCD + attackCD / 4);
        }

        ctrl.rotater.transform.forward = Vector3.Lerp(ctrl.rotater.transform.forward, -GetPlayerDir(), ctrl.rotateSpeed * Time.deltaTime);
    }
}
