using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range01Warning : Range01Action
{
    protected Range01Ctrl ctrl;
    protected Range01Attack attack;

    public float attackCD;

    public bool debug;
    public float warningRadius = 5f;
    public float exitWarningRadius = 8f;

    [HideInInspector] public float attackTimer;
    [HideInInspector] public bool isOnWarningRange;

    public float attackRequidSecRand {
        get {
            return Random.Range(attackCD - attackCD / 4, attackCD + attackCD / 4);
        }
    }

    protected bool readyAttack;

    private void Awake()
    {
        ctrl = GetComponent<Range01Ctrl>();
        attack = GetComponent<Range01Attack>();
    }

    private void Update()
    {
        if ((GetPlayerDistance() < warningRadius || isOnWarningRange == true) && actionManager.isAction == false)
            actionManager.Warning();

        if (isOnWarningRange == true)
        {
            if (attackTimer > 0)
                attackTimer -= Time.deltaTime;
            if (GetPlayerDistance() > exitWarningRadius)
                isOnWarningRange = false;
        }
    }

    //警戒開始
    public override void StartAction()
    {
        base.StartAction();
        isOnWarningRange = true;
        readyAttack = false;
    }

    public override void UpdateAction()
    {
        if (readyAttack == true) return;

        if (attackTimer <= 0)
        {
            EnemysControlManager.Instance.range01s.Add(actionManager);
            readyAttack = true;
            //ForceEnd();
            //actionManager.Attack();
        }

        if (isOnWarningRange == false)
        {
            ForceEnd();
            attackTimer = attackRequidSecRand;
        }

        ctrl.rotater.transform.rotation = Quaternion.Lerp(ctrl.rotater.transform.rotation, Quaternion.LookRotation(-GetPlayerDir()), ctrl.rotateSpeed * Time.deltaTime);
    }

    //警戒結束
    public override void EndAction()
    {
        if (readyAttack == true)
            EnemysControlManager.Instance.range01s.Remove(actionManager);
        base.EndAction();
    }

    public override void SetActionEndTime()
    {
        
    }

    //畫警戒範圍
    private void OnDrawGizmos()
    {
        if (debug == false) return;

        Gizmos.color = Color.red;
        GizmosExtensions.DrawCircle(transform.position, warningRadius);
        Gizmos.color = Color.yellow;
        GizmosExtensions.DrawCircle(transform.position, exitWarningRadius);
    }
}
