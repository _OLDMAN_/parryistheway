using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range01Dead : Range01Action
{
    private Range01Attack attack;

    public void Awake()
    {
        attack = GetComponent<Range01Attack>();
    }

    public override void StartAction()
    {
        base.StartAction();
        gameObject.layer = default;
        animator.StopAni();
        if (attack.attackIndicator != null)
            attack.attackIndicator.GetComponent<PoolRecycle>().Recycle();
        SoundManager.Instance.PlaySound("EnemyDie");
        ParticleManager.Instance.PlayParticle("EnemyDieBomb", transform.position + Vector3.back + Vector3.up, Quaternion.identity);
        WaveManager.enemy.Remove(gameObject);
        WaveManager.lastPos = transform.position;
        StartCoroutine(DeadDelay());
    }

    public override void EndAction()
    {
        base.EndAction();
    }
    public override void SetActionEndTime()
    {

    }
    private IEnumerator DeadDelay()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.CompareTag("HealthCanvas")) continue;
            transform.GetChild(i).gameObject.SetActive(false);
        }
        float time = 1f;
        while (time > 0)
        {
            time -= Time.deltaTime * GamefeelManager.stopSpeed;
            yield return null;
        }
        Destroy(gameObject);
    }
}
