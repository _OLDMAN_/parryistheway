using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range01Attack : Range01Action
{
    private Range01Ctrl ctrl;
    private Range01Warning warning;

    public GameObject bulletPrefab;
    public Transform muzzle;
    public Transform preAttackTransform;
    public bool followBullet;
    public int bulletDamage;
    public float bulletSpeed;

    [HideInInspector] public GameObject attackIndicator;
    private bool lookPlayer;

    public int maxContinueAttack;
    private int continueCounter;
    private int randContinueCount 
    { 
        get { return Random.Range(0, maxContinueAttack); } 
    }
    private bool comboStart;

    public virtual void Awake()
    {
        ctrl = GetComponent<Range01Ctrl>();
        warning = GetComponent<Range01Warning>();
    }

    //攻擊開始 
    public override void StartAction()
    {
        base.StartAction();
        hurt.superArmor = true;
        if (comboStart == false)
        {
            continueCounter = randContinueCount;
            comboStart = true;
        }
        lookPlayer = true;
        SoundManager.Instance.PlaySound("Range01Charge");
        ParticleManager.Instance.PlayParticle("Range01Accumulate", preAttackTransform.position, preAttackTransform.rotation, preAttackTransform);
    }

    public override void UpdateAction()
    {
        if (lookPlayer == true)
        {
            ctrl.LookPlayer();
            if (attackIndicator != null)
                attackIndicator.transform.forward = -ctrl.rotater.transform.forward;
        }
    }

    //攻擊結束
    public override void EndAction()
    {
        EnemysControlManager.Instance.currentAttackerCount--;
        if (continueCounter <= 0)
        {
            warning.attackTimer = warning.attackRequidSecRand;
            comboStart = false;
        }
        else
            continueCounter--;
        hurt.superArmor = false;
        base.EndAction();
    }

    //生成紅色持續指示器
    public void SpawnAttackIndicator()
    {
        attackIndicator = ParticleManager.Instance.GetParticle("AttackIndicator", new Vector3(transform.position.x, 0.1f, transform.position.z), muzzle.rotation);
        attackIndicator.GetComponent<Indicator>().StartIndicat(0.3f);
    }

    //生成閃爍指示器
    public void SpawnAttackIndicatorBurstFrame()
    {
        lookPlayer = false;
        ParticleManager.Instance.PlayParticle("Range01Attack", preAttackTransform.position, preAttackTransform.rotation, preAttackTransform);
        GameObject o = ParticleManager.Instance.GetParticle("AttackIndicatorBurst", new Vector3(transform.position.x, 0.1f, transform.position.z), attackIndicator.transform.rotation);
        o.GetComponent<IndicatorFlash>().Flash(2);
        attackIndicator.GetComponent<PoolRecycle>().Recycle();
        attackIndicator = null;
    }

    public void PreAttackEnd()
    {
        PlayAnimation(Range01Ani.Attack);
    }

    //攻擊偵
    public void AttackFrame()
    {
        Vector3 dir = muzzle.forward;
        dir.y = 0;

        SoundManager.Instance.PlaySound("Shoot");
        GameObject bullet = Instantiate(bulletPrefab, muzzle.position, muzzle.rotation);
        EnemyBullet bulletCtrl = bullet.GetComponent<EnemyBullet>();
        bulletCtrl.SetBullet(bulletSpeed, bulletDamage, dir, transform, followBullet);
    }

    //設定攻擊結束時間 = 前搖 + 攻擊
    public override void SetActionEndTime()
    {
        float actionLength = 0;
        actionLength += GetAnimationLength(aniData.ani);
        actionLength += GetAnimationLength(Range01Ani.Attack);
        StartCoroutine(TickToEndAction(actionLength));
    }
}