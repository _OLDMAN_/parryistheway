using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Range01Hurt : Range01Action, IHurtable, IBreakable
{
    private List<Material> materials = new List<Material>();
    private Range01Ctrl ctrl;
    private Range01Break breakAction;
    private EnemyHealth ui;

    public int maxHurtCount = 5;
    public float noShieldBounsDamage = 50;
    public float shieldArmor;
    public Vector3 shieldOffset;
    public Vector3 shieldScale;

    public int breakCounter { get; set; }
    public int hurtCounter { get; set; }
    private Vector3 hurtDir;

    [HideInInspector] public bool superArmor;
    [HideInInspector] public bool invincible;

    private void Awake()
    {
        hurtCounter = maxHurtCount;
        GetComp();
        SetMaterials();
    }

    //����}��
    private void GetComp()
    {
        ctrl = GetComponent<Range01Ctrl>();
        breakAction = GetComponent<Range01Break>();
        ui = GetComponent<EnemyHealth>();
    }

    //�]�w�n�ܤƪ�����y
    private void SetMaterials()
    {
        Renderer[] renderer = GetComponentsInChildren<Renderer>();
        foreach (Renderer r in renderer)
            materials.Add(r.material);
    }

    //���˶}�l
    public override void StartAction()
    {
        ctrl.rotater.transform.forward = -hurtDir;
        base.StartAction();
    }

    //���˵���
    public override void EndAction()
    {
        base.EndAction();
    }

    //����
    public void Damage(Transform attacker, int damage, bool heavyDamage = false)
    {
        if (invincible == true) return;

        EnemyBullet bullet = attacker.GetComponent<EnemyBullet>();
        if (bullet != null)
            hurtDir = -bullet.dir;
        else
            hurtDir = attacker.position - transform.position;
        hurtDir.y = 0;
        hurtDir.Normalize();

        SoundManager.Instance.PlaySound("EnemyHurt01");
        SoundManager.Instance.PlaySound("EnemyHurt02");
        ParticleManager.Instance.PlayParticle("EnemyHurt", transform.position + hurtDir + Vector3.up, Quaternion.LookRotation(-hurtDir));

        Blink();
        Damage(damage, heavyDamage);
    }

    public void KnockBack(Vector3 backDir, float distance)
    {

    }

    public void ReduceHurtCount(int hurtCount)
    {
        if (superArmor == true || breakAction.isBreaking == true) return;
        hurtCounter -= hurtCount;
    }

    //���˨��ˬd�O�_���`
    private void Damage(int damage, bool heavyDamage)
    {
        if (breakAction.isBreaking == true)
        {
            damage = Mathf.RoundToInt(damage * (1 + noShieldBounsDamage / 100f));
        }
        else
        {
            damage = Mathf.RoundToInt(damage * (1 - shieldArmor / 100f));
            ParticleManager.Instance.PlayParticle("EnemyShield", transform.position + shieldOffset, Quaternion.identity, transform);
        }

        ui.Damage(damage, heavyDamage);

        if (ui.HP <= 0)
            actionManager.Dead();
        else if (breakAction.breakCounter <= 0)
        {
            Break();
            if (hurtCounter <= 0)
                hurtCounter = maxHurtCount;
        }
        else if (hurtCounter <= 0)
        {
            actionManager.Hurt();
            hurtCounter = maxHurtCount;
        }
    }

    public void ReduceBreakCount(int breakCount)
    {
        if (breakAction.isBreaking == true) return;
        breakAction.breakCounter -= breakCount;
    }

    public void Break()
    {
        actionManager.Break();
        breakAction.breakCounter = breakAction.breakCount;
        ParticleManager.Instance.PlayParticle("EnemyShieldBreak", transform.position + shieldOffset, Quaternion.LookRotation(hurtDir), shieldScale, transform);
    }

    //�C���ܤ�
    private void Blink()
    {
        float HurtSec = 0.2f;
        foreach (Material m in materials)
        {
            if (m.HasProperty("_Sprint"))
                m.DOFloat(0.5f, "_Sprint", HurtSec / 4).onComplete += ()=> m.DOFloat(0, "_Sprint", HurtSec / 4).onComplete += ()=> m.DOFloat(0.5f, "_Sprint", HurtSec / 4).onComplete += ()=> m.DOFloat(0, "_Sprint", HurtSec / 4);
        }
    }
}