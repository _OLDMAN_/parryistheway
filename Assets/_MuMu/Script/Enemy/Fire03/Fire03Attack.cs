using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire03Attack : Fire03Action
{
    public Fire03AttackCombo combos;

    public override void StartAction()
    {
        base.StartAction();
        combos.PlayRandCombo();
    }
    public override void EndAction()
    {
        base.EndAction();
    }
    public override void SetActionEndTime()
    {
        
    }
}
