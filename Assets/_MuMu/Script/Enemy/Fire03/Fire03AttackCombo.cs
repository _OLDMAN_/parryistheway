using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire03AttackCombo : MonoBehaviour
{
    private Fire03ActionManager actionManager;
    private Fire03Warning warning;
    private Fire03Break fire03Break;
    private Fire03Hurt hurt;

    public List<Fire03ComboListData> comboList;
    private Fire03ComboListData currentComboList;
    private int comboNum;

    public bool isCombo;

    private void Start()
    {
        hurt = GetComponent<Fire03Hurt>();
        fire03Break = GetComponent<Fire03Break>();
        actionManager = GetComponent<Fire03ActionManager>();
        warning = GetComponent<Fire03Warning>();
    }

    public void PlayRandCombo()
    {
        isCombo = true;
        int r = Random.Range(0, comboList.Count);
        currentComboList = comboList[r];
        comboNum = 0;
        PlayCombo();
    }

    public void NextCombo()
    {
        if (fire03Break.breakCounter <= 0 || hurt.hurtCounter <= 0) return;
        if (comboNum < currentComboList.combos.Count - 1)
        {
            comboNum++;
            PlayCombo();
        }
        else
        {
            ComboComplete();
        }
    }

    private void PlayCombo()
    {
        Fire03Ani ani = currentComboList.combos[comboNum].ani;
        if (ani == Fire03Ani.Attack01)
            actionManager.Attack01();
        else if (ani == Fire03Ani.Attack02)
            actionManager.Attack02();
        else if (ani == Fire03Ani.Attack03)
            actionManager.Attack03();
        else if (ani == Fire03Ani.BackShoot)
            actionManager.BackShoot();
        else if (ani == Fire03Ani.Dash)
            actionManager.Dash();
    }

    private void ComboComplete()
    {
        warning.attackRequidTimer = warning.attackRequidSecRand;
        EnemysControlManager.Instance.currentAttackerCount--;
        isCombo = false;
    }
    public void Break()
    {
        isCombo = false;
        EnemysControlManager.Instance.currentAttackerCount--;
        Debug.Log("break");
    }
}
