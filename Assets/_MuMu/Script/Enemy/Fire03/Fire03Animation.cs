using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire03Animation : EnemyAnimation
{
    public override void PlayAnimation<Fire03Ani>(Fire03Ani name, bool skipWhenRepeat = true, bool blendPlay = true)
    {
        base.PlayAnimation(name, skipWhenRepeat, blendPlay);
    }
}
