using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "EnemyCombo/Fire03/Combo", fileName = "newCombo", order = 0)]
public class Fire03ComboData : ScriptableObject
{
    public Fire03Ani ani;
}
