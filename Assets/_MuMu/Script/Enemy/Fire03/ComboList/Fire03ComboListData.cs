using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "EnemyCombo/Fire03/ComboList", fileName = "ComboList", order = 0)]
public class Fire03ComboListData : ScriptableObject
{
    public List<Fire03ComboData> combos;
}
