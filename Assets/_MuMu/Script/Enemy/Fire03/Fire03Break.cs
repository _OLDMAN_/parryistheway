using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire03Break : Fire03Action
{
    public int maxBreakCount = 1;
    public float dizzyTime = 2;

    public Vector3 dizzyOffset = new Vector3(0, 2, 0);

    [HideInInspector] public int breakCounter;
    [HideInInspector] public bool isBreaking;

    private void Awake()
    {
        breakCounter = maxBreakCount;
    }

    //破防開始
    public override void StartAction()
    {
        base.StartAction();
        if (combo.isCombo == true)
            combo.Break();
        SoundManager.Instance.PlaySound("ShieldBreak");
        StartCoroutine(DizzyEnd());
        StopAgent();
        isBreaking = true;
    }

    //暈眩動畫結束
    private IEnumerator DizzyEnd()
    {
        float time = GetAnimationLength(Fire03Ani.Break) + dizzyTime;
        GameObject dizzyObj = ParticleManager.Instance.GetParticle("EnemyDizzy", transform.position.GetZeroY() + dizzyOffset, Quaternion.identity, transform);
        while (time > 0)
        {
            time -= Time.deltaTime * GamefeelManager.stopSpeed;
            yield return null;
        }
        dizzyObj.GetComponent<PoolRecycle>().Recycle();
    }

    //破防結束
    public override void EndAction()
    {
        isBreaking = false;
        breakCounter = maxBreakCount;
        base.EndAction();
    }

    //設定破防結束時間 = 破防前搖 + 暈眩時間 + 恢復時間
    public override void SetActionEndTime()
    {
        float actionLength = 0;
        actionLength += GetAnimationLength(aniData.ani);
        actionLength += dizzyTime;
        //actionLength += GetAnimationLength(Fire03Ani.BreakRecover);
        StartCoroutine(TickToEndAction(actionLength));
    }
}
