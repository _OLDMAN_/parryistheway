using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Fire03AnimationData
{
    public Fire03Ani ani;
    public bool interuptOtherAnimation = false;
    public bool interuptOwnAnimation = false;
}

public enum Fire03Ani
{
    Idle, Move, Def01, Def02, Def03, Attack01, Attack02, Attack03, ReAttack01, ReAttack03, BackShoot, Hurt, Break, Dead, Dash
}
