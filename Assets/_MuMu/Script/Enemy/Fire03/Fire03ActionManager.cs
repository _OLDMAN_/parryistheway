using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire03ActionManager : MonoBehaviour
{
    public bool isAction { get { return actionList.Count > 0; } }
    private List<Fire03Action> actionList = new List<Fire03Action>();
    private Fire03Action tempAction;
    public Fire03Action currentAction
    {
        get
        {
            if (isAction)
                return actionList[0];
            return null;
        }
    }
    public Fire03Action lastAction;

    protected Fire03Warning warning;
    protected Fire03Attack attack;
    protected Fire03Attack01 attack01;
    protected Fire03Attack02 attack02;
    protected Fire03Attack03 attack03;
    protected Fire03BackShoot backShoot;
    protected Fire03Hurt hurt;
    protected Fire03Break breakAction;
    protected Fire03Dead dead;
    protected Fire03Dash dash;

    private void Start()
    {
        warning = GetComponent<Fire03Warning>();
        attack = GetComponent<Fire03Attack>();
        hurt = GetComponent<Fire03Hurt>();
        breakAction = GetComponent<Fire03Break>();
        dead = GetComponent<Fire03Dead>();
        attack01 = GetComponent<Fire03Attack01>();
        attack02 = GetComponent<Fire03Attack02>();
        attack03 = GetComponent<Fire03Attack03>();
        backShoot = GetComponent<Fire03BackShoot>();
        dash = GetComponent<Fire03Dash>();
    }

    private void Update()
    {
        if (isAction)
            actionList[0].UpdateAction();
    }

    public void Warning()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Fire03Ani.Dead) return;
        AddAction(warning);
    }
    public void Attack()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Fire03Ani.Dead) return;
        AddAction(attack);
    }
    public void Attack01()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Fire03Ani.Dead) return;
        AddAction(attack01);
    }
    public void Attack02()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Fire03Ani.Dead) return;
        AddAction(attack02);
    }
    public void Attack03()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Fire03Ani.Dead) return;
        AddAction(attack03);
    }
    public void BackShoot()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Fire03Ani.Dead) return;
        AddAction(backShoot);
    }

    public void Hurt()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Fire03Ani.Dead) return;
        AddAction(hurt);
    }
    public void Dash()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Fire03Ani.Dead) return;
        AddAction(dash);
    }

    public void Break()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Fire03Ani.Dead) return;
        AddAction(breakAction);
    }

    public void Dead()
    {
        AddAction(dead);
    }

    private void AddAction<T>(T status) where T : Fire03Action
    {
        tempAction = status;

        if (isAction)
        {
            if (IsActionAbleToInterupt())
            {
                actionList[0].ForceEnd();
            }
            else
            {
                return;
            }
        }

        ExcuteNextAction();
    }

    private bool IsActionAbleToInterupt()
    {
        if (tempAction.aniData.interuptOtherAnimation && actionList[0].GetType().ToString() != tempAction.GetType().ToString())
            return true;
        else if (tempAction.aniData.interuptOwnAnimation)
            return true;

        return false;
    }

    private void ExcuteNextAction()
    {
        actionList.Add(tempAction);
        actionList[0].StartAction();
        //actionList[0].PlayAnimation(actionList[0].aniData.ani);
    }

    public void RemoveCurrentAction()
    {
        lastAction = actionList[0];
        actionList.Clear();
    }
}
