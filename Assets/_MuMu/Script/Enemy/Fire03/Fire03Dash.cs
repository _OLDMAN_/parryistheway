using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Fire03Dash : Fire03Action
{
    public bool debug;
    public float maxDistacne;
    public float minStopDistance = 1;
    public float sprintTime;
    public AnimationCurve sprintCurve;

    public LayerMask obsLayer;
    private Tween sprintTween;

    public override void StartAction()
    {
        base.StartAction();
        transform.forward = GetPlayerDir();
        if (GetPlayerDistance() < 2)
            EndAction();
        else
            DoSprint();
    }
    private void DoSprint()
    {
        Vector3 point = transform.position + transform.forward * maxDistacne;
        sprintTween = transform.DOMove(point, sprintTime).SetEase(sprintCurve);
        sprintTween.OnUpdate(() => {
            RaycastHit hit = GetCapCastHit(transform.forward, minStopDistance, obsLayer);
            if (hit.collider != null)
            {
                sprintTween.Kill();
                EndAction();
            }
        });
        sprintTween.OnComplete(() => {
            EndAction();
        });
    }

    public override void EndAction()
    {
        base.EndAction();
        if (sprintTween != null)
            sprintTween.Kill();
        combo.NextCombo();
    }

    public override void SetActionEndTime()
    {
        
    }
    private void OnDrawGizmos()
    {
        if (debug == false) return;

        Gizmos.color = Color.cyan;
        GizmosExtensions.DrawCircle(transform.position, maxDistacne);
    }
}
