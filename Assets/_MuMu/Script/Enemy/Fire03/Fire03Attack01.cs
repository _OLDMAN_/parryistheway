using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire03Attack01 : Fire03Action
{
    private bool lookPlayer;
    public ParticleSystem attack01PrepareParticle;

    public int damage;
    public bool debug;
    public float attackRadius = 2;
    public Vector3 offset;

    public override void StartAction()
    {
        base.StartAction();
        hurt.superArmor = true;
        attack01PrepareParticle.Play();
        SoundManager.Instance.PlaySound("Fire03Attack01Prepare");
        lookPlayer = true;
    }

    public override void UpdateAction()
    {
        if (lookPlayer == true)
            transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(GetPlayerDir()), 5 * Time.deltaTime);
    }

    public override void EndAction()
    {
        base.EndAction();
        hurt.superArmor = false;
        combo.NextCombo();
    }

    public void Attack01Frame()
    {
        lookPlayer = false;
        Collider[] collisions = Physics.OverlapSphere(transform.position + transform.rotation * offset, attackRadius);
        foreach(Collider c in collisions)
        {
            if (c.CompareTag("Player"))
            {
                if (c.GetComponent<Block>().canBlock == true)
                {
                    hurt.Block();
                    c.GetComponent<Block>().OtherBlockSuccess();
                    ParticleManager.Instance.PlayParticle("EnemyBlock", transform.position, transform.rotation);
                    break;
                }
                else
                {
                    c.GetComponent<PlayerActionManager>().StartHurt(damage, transform, true);
                    ParticleManager.Instance.PlayParticle("Fire03Attack01", transform.position, transform.rotation);
                    SoundManager.Instance.PlaySound("Fire03Attack01");
                    break;
                }
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (debug == false) return;

        Matrix4x4 rotationMatrix = Matrix4x4.TRS(transform.position, transform.rotation, Vector3.one);
        Gizmos.matrix = rotationMatrix;
        Gizmos.color = Color.cyan;
        Gizmos.DrawWireSphere(offset, attackRadius);
    }
}
