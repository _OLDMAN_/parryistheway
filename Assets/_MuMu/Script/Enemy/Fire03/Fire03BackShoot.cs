using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Fire03BackShoot : Fire03Action
{
    public GameObject bulletPrefab;
    public Transform muzzle;

    public int bulletDamage = 7;
    public float bulletSpeed = 40;

    public float maxBackDistacne = 3;
    public float sprintTime = 0.7f;

    public LayerMask obsLayer;
    private Tween sprintTween;

    public override void StartAction()
    {
        base.StartAction();
        transform.forward = GetPlayerDir();
        hurt.superArmor = true;
        ParticleManager.Instance.PlayParticle("Fire03BackShootWarning", muzzle.position, muzzle.rotation);
        SoundManager.Instance.PlaySound("BossShootWarningShort");
    }

    public void DOBackSprint()
    {
        Vector3 point = transform.position - transform.forward * maxBackDistacne;
        sprintTween = transform.DOMove(point, sprintTime).SetEase(Ease.OutBack);
        sprintTween.OnUpdate(() => {
            RaycastHit hit = GetCapCastHit(-transform.forward, 0.5f, obsLayer);
            if (hit.collider != null)
            {
                sprintTween.Kill();
                EndAction();
            }
        });
        sprintTween.OnComplete(() => {
            EndAction();
        });
    }
    public void BackShootFrame()
    {
        GameObject bullet = Instantiate(bulletPrefab, muzzle.position, muzzle.rotation);
        bullet.GetComponent<EnemyBullet>().SetBullet(bulletSpeed, bulletDamage, GetPlayerDir(), transform);
        SoundManager.Instance.PlaySound("Shoot");
    }

    public override void SetActionEndTime()
    {

    }
    public override void EndAction()
    {
        base.EndAction();
        if (sprintTween != null)
            sprintTween.Kill();
        hurt.superArmor = false;
        combo.NextCombo();
    }
}
