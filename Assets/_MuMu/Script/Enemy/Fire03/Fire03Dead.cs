using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire03Dead : Fire03Action
{
    [HideInInspector] public bool isDie;

    public override void StartAction()
    {
        base.StartAction();
        DieSet();
    }

    //???`?]?m
    private void DieSet()
    {
        gameObject.layer = default;
        animator.StopAni();
        SoundManager.Instance.PlaySound("EnemyDie");
        ParticleManager.Instance.PlayParticle("EnemyDieBomb", transform.position + Vector3.back + Vector3.up, Quaternion.identity);
        WaveManager.enemy.Remove(gameObject);
        WaveManager.lastPos = transform.position;
        StartCoroutine(DeadDelay());
    }
    public override void SetActionEndTime()
    {

    }

    public override void EndAction()
    {
        base.EndAction();
    }
    private IEnumerator DeadDelay()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.CompareTag("HealthCanvas")) continue;
            transform.GetChild(i).gameObject.SetActive(false);
        }
        float time = 1f;
        while (time > 0)
        {
            time -= Time.deltaTime * GamefeelManager.stopSpeed;
            yield return null;
        }
        Destroy(gameObject);
    }
}
