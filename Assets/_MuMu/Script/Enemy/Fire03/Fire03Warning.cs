using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire03Warning : Fire03Action
{
    protected Fire03Ctrl ctrl;
    protected Fire03Attack attack;

    public float moveSpeed;
    public float attackCD;

    public bool warningDebug;
    public float minDistance = 3;
    public float maxDistance = 5;

    public bool debug;
    public float warningRadius = 5f;
    public float exitWarningRadius = 8f;

    [HideInInspector] public float attackRequidTimer;
    public float attackRequidSecRand
    {
        get
        {
            return Random.Range(attackCD - attackCD / 4, attackCD + attackCD / 4);
        }
    }
    public GameObject defParticle;
    [HideInInspector] public bool isOnWarningRange;

    private void Awake()
    {
        GetComp();
    }

    protected bool readyAttack;

    //獲取自身腳本
    private void GetComp()
    {
        attack = GetComponent<Fire03Attack>();
        ctrl = GetComponent<Fire03Ctrl>();
    }

    private void Update()
    {
        if ((GetPlayerDistance() < warningRadius || isOnWarningRange == true) && actionManager.isAction == false)
            actionManager.Warning();

        if (isOnWarningRange == true)
        {
            if (attackRequidTimer > 0)
                attackRequidTimer -= Time.deltaTime;
            if (GetPlayerDistance() > exitWarningRadius)
                isOnWarningRange = false;
        }
    }

    //警戒開始
    public override void StartAction()
    {
        base.StartAction();
        StartAgent();
        isOnWarningRange = true;
        readyAttack = false;
        defParticle.SetActive(true);
    }

    //警戒中
    public override void UpdateAction()
    {
        if (GetPlayerDistance() > exitWarningRadius)
            ForceEnd();

        if (attackRequidTimer <= 0)
        {
            if (readyAttack == false)
            {
                EnemysControlManager.Instance.fire03s.Add(actionManager);
                readyAttack = true;
            }
        }
        else
        {
            LookPlayerWarning();
        }
    }

    //警戒結束
    public override void EndAction()
    {
        defParticle.SetActive(false);
        base.EndAction();
        if (readyAttack == true)
            EnemysControlManager.Instance.fire03s.Remove(actionManager);
    }

    private void LookPlayerWarning()
    {
        if (GetPlayerDistance() > 5 && GetPlayerDistance() < 8)
            agent.velocity = Vector3.zero;
        else if (GetPlayerDistance() < 5)
            agent.velocity = -GetPlayerDir() * moveSpeed;
        else if (GetPlayerDistance() > 8)
            agent.velocity = GetPlayerDir() * moveSpeed;
        RotateToPlayer();
    }

    //面向玩家
    protected void RotateToPlayer()
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(GetPlayerDir()), Time.deltaTime * ctrl.rotateSpeed);
    }

    //面向移動方向
    protected void RotateToVelecity()
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(agent.velocity), Time.deltaTime * ctrl.rotateSpeed);
    }

    public override void SetActionEndTime()
    {
        
    }

    private void OnDrawGizmos()
    {
        if (debug == true)
        {
            Gizmos.color = Color.red;
            GizmosExtensions.DrawCircle(transform.position, warningRadius);
            Gizmos.color = Color.yellow;
            GizmosExtensions.DrawCircle(transform.position, exitWarningRadius);
        }

        if(warningDebug == true)
        {
            Gizmos.color = Color.green;
            GizmosExtensions.DrawHollowCircleArea(transform.position, minDistance, maxDistance);
        }
    }
}
