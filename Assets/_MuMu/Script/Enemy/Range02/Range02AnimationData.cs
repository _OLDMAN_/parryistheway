using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Range02AnimationData
{
    public Range02Ani ani;
    public bool interuptOtherAnimation = false;
    public bool interuptOwnAnimation = false;
}

public enum Range02Ani 
{ 
    Idle, Move, Attack, Hurt, Break, Dead, Def01, Def02, Def03, Skill01_1, Skill01_2
}
