using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range02Attack : Range02Action
{
    private Range02Warning warning;

    public bool debug;
    public float minAttackRadius;
    public float maxAttackRadius;
    public int bulletSpeed;
    public int bulletDamage;
    public Transform muzzle;
    public ParticleSystem prepareParticle;

    public GameObject bulletPrefab;

    [HideInInspector] public bool isAttacking;

    [HideInInspector] public GameObject attackIndicator;
    [HideInInspector] public GameObject attackIndicatorBurst;

    private void Awake()
    {
        warning = GetComponent<Range02Warning>();
    }
    //攻擊開始
    public override void StartAction()
    {
        base.StartAction();
        ParticleManager.Instance.PlayParticle("Range02Warning", transform.position, transform.rotation, transform);
        prepareParticle.Play();
        agent.isStopped = true;
        agent.velocity = Vector3.zero;
    }

    //攻擊中
    public override void UpdateAction()
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(GetPlayerDir()), Time.deltaTime * 6);
        if (attackIndicator != null)
            attackIndicator.transform.forward = transform.forward;
        if(attackIndicatorBurst != null)
            attackIndicatorBurst.transform.forward = transform.forward;
    }

    //攻擊結束
    public override void EndAction()
    {
        warning.attackTimer = warning.attackRequidSecRand;
        EnemysControlManager.Instance.currentAttackerCount--;
        base.EndAction();
    }

    //生成持續指示器
    public void SpawnAttackIndicatorFrame()
    {
        attackIndicator = ParticleManager.Instance.GetParticle("AttackIndicator", new Vector3(transform.position.x, 0.1f, transform.position.z), Quaternion.LookRotation(GetPlayerDir()));
        attackIndicator.GetComponent<Indicator>().StartIndicat(0.5f);
    }

    //生成閃爍指示器
    public void SpawnAttackIndicatorBurstFrame()
    {
        attackIndicatorBurst = ParticleManager.Instance.GetParticle("AttackIndicatorBurst", new Vector3(transform.position.x, 0.1f, transform.position.z), attackIndicator.transform.rotation);
        attackIndicatorBurst.GetComponent<IndicatorFlash>().Flash(2);
        attackIndicator.GetComponent<PoolRecycle>().Recycle();
        attackIndicator = null;
    }

    //播放攻擊粒子
    public void PlayAttackParticle()
    {
        ParticleManager.Instance.PlayParticle("Range02Attack", transform.position, transform.rotation, transform);
    }
    public void AttackFrame()
    {
        SoundManager.Instance.PlaySound("Shoot");
        GameObject bullet = Instantiate(bulletPrefab, muzzle.position, muzzle.rotation);
        EnemyBullet bulletCtrl = bullet.GetComponent<EnemyBullet>();
        if (Vector3.Angle(muzzle.forward, GetPlayerDir(muzzle.position)) < 30)
            bulletCtrl.SetBullet(bulletSpeed, bulletDamage, GetPlayerDir(muzzle.position), transform);
        else
            bulletCtrl.SetBullet(bulletSpeed, bulletDamage, muzzle.forward, transform);
    }
    //左攻擊偵
    //public void AttackFrameLeft()
    //{
    //    SoundManager.Instance.PlaySound("Shoot");
    //    GameObject bullet = Instantiate(bulletPrefab, leftMuzzle.position, leftMuzzle.rotation);
    //    EnemyBullet bulletCtrl = bullet.GetComponent<EnemyBullet>();
    //    if (Vector3.Angle(leftMuzzle.forward, GetPlayerDir(leftMuzzle.position)) < 30)
    //        bulletCtrl.SetBullet(bulletSpeed, bulletDamage, GetPlayerDir(leftMuzzle.position), transform);
    //    else
    //        bulletCtrl.SetBullet(bulletSpeed, bulletDamage, leftMuzzle.forward, transform);
    //}

    //右攻擊偵
    //public void AttackFrameRight()
    //{
    //    SoundManager.Instance.PlaySound("Shoot");
    //    GameObject bullet = Instantiate(bulletPrefab, rightMuzzle.position, rightMuzzle.rotation);
    //    EnemyBullet bulletCtrl = bullet.GetComponent<EnemyBullet>();
    //    if (Vector3.Angle(rightMuzzle.forward, GetPlayerDir(rightMuzzle.position)) < 30)
    //        bulletCtrl.SetBullet(bulletSpeed, bulletDamage, GetPlayerDir(rightMuzzle.position), transform);
    //    else
    //        bulletCtrl.SetBullet(bulletSpeed, bulletDamage, rightMuzzle.forward, transform);
    //}

    //畫攻擊範圍
    private void OnDrawGizmos()
    {
        if (debug == false) return;
        Gizmos.color = Color.green;
        GizmosExtensions.DrawCircle(transform.position, minAttackRadius);
        GizmosExtensions.DrawCircle(transform.position, maxAttackRadius);
    }
}
