using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Range02Warning : Range02Action
{
    protected Range02Ctrl ctrl;
    protected Range02Attack attack;
    protected Range02Hurt hurt;

    public float moveSpeed;
    public float attackCD;

    public bool debug;
    public float warningRadius = 5f;
    public float exitWarningRadius = 8f;

    [HideInInspector] public float attackTimer;
    public float attackRequidSecRand {
        get {
            return Random.Range(attackCD - attackCD / 4, attackCD + attackCD / 4);
        }
    }

    [HideInInspector] public bool isOnWarningRange;
    protected Vector3 movePos;
    protected Vector3 moveDir;

    private bool readyAttack;

    private void Awake()
    {
        GetComp();
    }

    //獲取腳本
    private void GetComp()
    {
        attack = GetComponent<Range02Attack>();
        ctrl = GetComponent<Range02Ctrl>();
        hurt = GetComponent<Range02Hurt>();
    }

    private void Update()
    {
        if ((GetPlayerDistance() < warningRadius || isOnWarningRange == true) && actionManager.isAction == false)
            actionManager.Warning();

        if (isOnWarningRange == true)
        {
            if (attackTimer > 0)
                attackTimer -= Time.deltaTime;
            if (GetPlayerDistance() > exitWarningRadius)
                isOnWarningRange = false;
        }
    }

    //警戒開始
    public override void StartAction()
    {
        base.StartAction();
        InitialValue();
    }

    //初始化值
    private void InitialValue()
    {
        agent.isStopped = false;
        isOnWarningRange = true;
        readyAttack = false;
    }

    //警戒中
    public override void UpdateAction()
    {
        if (GetPlayerDistance() > exitWarningRadius)
            ForceEnd();

        if (readyAttack == true) return;

        if(attackTimer <= 0)
        {
            if(readyAttack == false)
            {
                EnemysControlManager.Instance.range02s.Add(actionManager);
                readyAttack = true;
            }
        }
        else
        {
            LookPlayerWarning();
        }
    }

    private void LookPlayerWarning()
    {
        if(GetPlayerDistance() > 5 && GetPlayerDistance() < 8)
            agent.velocity = Vector3.zero;
        else if (GetPlayerDistance() < 5)
            agent.velocity = -GetPlayerDir() * moveSpeed;
        else if (GetPlayerDistance() > 8)
            agent.velocity = GetPlayerDir() * moveSpeed;
        RotateToPlayer();
    }

    //改變逃離方向
    protected void ChangeKeepAwayPoint()
    {
        NavMeshHit hit;
        int minDistance = Random.Range(3, 6);
        while (true)
        {
            Vector3 randomPos = Random.insideUnitSphere * 10 + transform.position;
            NavMesh.SamplePosition(randomPos, out hit, 10, NavMesh.AllAreas);
            Vector3 moveDir = hit.position - transform.position;
            moveDir.y = 0;
            float dot = Vector3.Dot(GetPlayerDir(), moveDir);
            if (hit.distance >= minDistance && dot <= 0)
                break;
        }

        movePos = hit.position;
        moveDir = hit.position - transform.position;
        moveDir.y = 0;
        agent.SetDestination(movePos);
    }

    //面向玩家
    protected void RotateToPlayer()
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(GetPlayerDir()), Time.deltaTime * ctrl.rotateSpeed);
    }

    //面向移動方向
    protected void RotateToVelecity()
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(agent.velocity), Time.deltaTime * ctrl.rotateSpeed);
    }
    public override void SetActionEndTime()
    {

    }
    //警戒結束
    public override void EndAction()
    {
        if(readyAttack == true)
            EnemysControlManager.Instance.range02s.Remove(actionManager);
        base.EndAction();
        agent.isStopped = true;
        agent.velocity = Vector3.zero;
    }

    //畫警戒範圍
    private void OnDrawGizmos()
    {
        if (debug == false) return;

        Gizmos.color = Color.red;
        GizmosExtensions.DrawCircle(transform.position, warningRadius);
        Gizmos.color = Color.yellow;
        GizmosExtensions.DrawCircle(transform.position, exitWarningRadius);
    }
}
