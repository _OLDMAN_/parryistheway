using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range02Hurt : Range02Action, IHurtable, IBreakable
{
    private Range02Skill skill;
    private Range02Break breakAction;
    private EnemyHealth ui;

    private List<Material> materials = new List<Material>();
    public LayerMask obsLayer;

    public int maxHurtCount = 10;
    public float noShieldBounsDamage = 50;
    public float shieldArmor;
    public Vector3 shieldOffset;
    public Vector3 shieldScale;
    private Vector3 hurtDir;
    public int hurtCounter { get; set; }
    public int breakCounter { get; set; }

    [HideInInspector] public bool isKnockBack;
    [HideInInspector] public bool superArmor;

    private void Awake()
    {
        hurtCounter = maxHurtCount;
        GetComp();
        SetMaterials();
    }

    //����}��
    private void GetComp()
    {
        skill = GetComponent<Range02Skill>();
        breakAction = GetComponent<Range02Break>();
        ui = GetComponent<EnemyHealth>();
    }

    //�]�m�n�C���ܤƪ�����y
    private void SetMaterials()
    {
        Renderer[] renderer = GetComponentsInChildren<Renderer>();
        foreach (Renderer r in renderer)
            materials.Add(r.material);
    }

    //���˶}�l
    public override void StartAction()
    {
        base.StartAction();
        transform.forward = hurtDir;
        agent.isStopped = true;
        agent.velocity = Vector3.zero;
    }

    //���˵���
    public override void EndAction()
    {
        base.EndAction();
        agent.isStopped = false;
    }

    //����
    public void Damage(Transform attacker, int damage, bool heavyDamage = false)
    {
        EnemyBullet bullet = attacker.GetComponent<EnemyBullet>();
        if (bullet != null)
            hurtDir = -bullet.dir;
        else
            hurtDir = attacker.position - transform.position;
        hurtDir.y = 0;
        hurtDir.Normalize();

        SoundManager.Instance.PlaySound("EnemyHurt01");
        SoundManager.Instance.PlaySound("EnemyHurt02");
        ParticleManager.Instance.PlayParticle("EnemyHurt", transform.position + hurtDir * 0.5f, Quaternion.LookRotation(-hurtDir));

        Blink();
        Damage(damage, heavyDamage);
    }

    public void ReduceHurtCount(int hurtCount)
    {
        if (superArmor == true || breakAction.isBreaking == true) return;
        if (skill != null)
            if (skill.isSkilling == true) return;

        hurtCounter -= hurtCount;
    }
    private void Damage(int damage, bool heavyDamage = false)
    {
        if (breakAction.isBreaking == true)
        {
            damage = Mathf.RoundToInt(damage * (1 + noShieldBounsDamage / 100f));
        }
        else
        {
            damage = Mathf.RoundToInt(damage * (1 - shieldArmor / 100f));
            ParticleManager.Instance.PlayParticle("EnemyShield", transform.position + shieldOffset, Quaternion.identity, transform);
        }

        ui.Damage(damage, heavyDamage);

        if (ui.HP <= 0)
            actionManager.Dead();
        else if (breakAction.breakCounter <= 0)
        {
            Break();
            if(hurtCounter <= 0)
                hurtCounter = maxHurtCount;
        }
        else if(hurtCounter <= 0)
        {
            actionManager.Hurt();
            hurtCounter = maxHurtCount;
        }
    }

    public void ReduceBreakCount(int breakCount)
    {
        if(breakAction.isBreaking == true) return;
        breakAction.breakCounter -= breakCount;
    }

    public void Break()
    {
        actionManager.Break();
        breakAction.breakCounter = breakAction.maxBreakCount;
        ParticleManager.Instance.PlayParticle("EnemyShieldBreak", transform.position + shieldOffset, Quaternion.LookRotation(hurtDir), shieldScale, transform);
    }

    //�C���ܤ�
    private void Blink()
    {
        float HurtSec = 0.2f;
        foreach (Material m in materials)
        {
            if (m.HasProperty("_Sprint"))
                m.DOFloat(0.5f, "_Sprint", HurtSec / 4).onComplete += ()=> m.DOFloat(0, "_Sprint", HurtSec / 4).onComplete += ()=> m.DOFloat(0.5f, "_Sprint", HurtSec / 4).onComplete += ()=> m.DOFloat(0, "_Sprint", HurtSec / 4);
        }
    }

    //���h
    public void KnockBack(Vector3 dir, float distance)
    {
        RaycastHit hit = GetCapCastHit(dir, distance, obsLayer);
        if (hit.collider != null)
            distance = hit.distance;

        StartCoroutine(BackMove(dir, distance));
    }

    private IEnumerator BackMove(Vector3 dir, float distance)
    {
        isKnockBack = true;
        float speed = distance / 0.3f;
        while (distance > 0)
        {
            transform.position += dir * Time.deltaTime * speed * GamefeelManager.stopSpeed;
            distance -= Time.deltaTime * speed * GamefeelManager.stopSpeed;
            yield return null;
        }
        isKnockBack = false;
    }
}