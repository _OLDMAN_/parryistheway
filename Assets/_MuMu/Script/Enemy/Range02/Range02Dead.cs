using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range02Dead : Range02Action
{
    private Range02Attack attack;

    private void Awake()
    {
        attack = GetComponent<Range02Attack>();
    }

    public override void StartAction()
    {
        base.StartAction();
        gameObject.layer = default;
        animator.StopAni();
        if (attack.attackIndicator != null)
            attack.attackIndicator.GetComponent<PoolRecycle>().Recycle();
        SoundManager.Instance.PlaySound("EnemyDie");
        ParticleManager.Instance.PlayParticle("EnemyDieBomb", transform.position + Vector3.back + Vector3.up, Quaternion.identity);
        WaveManager.enemy.Remove(gameObject);
        WaveManager.lastPos = transform.position;
        StartCoroutine(DeadDelay());
    }

    public override void EndAction()
    {
        base.EndAction();
    }

    private IEnumerator DeadDelay()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.CompareTag("HealthCanvas")) continue;
            transform.GetChild(i).gameObject.SetActive(false);
        }
        float time = 1f;
        while (time > 0)
        {
            time -= Time.deltaTime * GamefeelManager.stopSpeed;
            yield return null;
        }
        Destroy(gameObject);
    }

    public override void SetActionEndTime()
    {
        
    }
}
