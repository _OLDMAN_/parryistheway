using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range02ActionManager : MonoBehaviour
{
    public bool isAction { get { return actionList.Count > 0; } }
    private List<Range02Action> actionList = new List<Range02Action>();
    private Range02Action tempAction;
    public Range02Action currentAction
    {
        get
        {
            if (isAction)
                return actionList[0];
            return null;
        }
    }
    public Range02Action lastAction;

    private Range02Warning warning;
    private Range02Attack attack;
    private Range02Hurt hurt;
    private Range02Break breakAction;
    private Range02Dead dead;
    private Range02KeepAway keepAway;
    private Range02Skill skill;
    private Range02Def def;

    private void Start()
    {
        warning = GetComponent<Range02Warning>();
        attack = GetComponent<Range02Attack>();
        hurt = GetComponent<Range02Hurt>();
        breakAction = GetComponent<Range02Break>();
        dead = GetComponent<Range02Dead>();
        keepAway = GetComponent<Range02KeepAway>();
        skill = GetComponent<Range02Skill>();
        def = GetComponent<Range02Def>();
    }

    private void Update()
    {
        if (isAction)
            actionList[0].UpdateAction();
    }

    public void Warning()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Range02Ani.Dead) return;
        AddAction(warning);
    }
    public void Attack()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Range02Ani.Dead) return;
        AddAction(attack);
    }
    public void Skill01()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Range02Ani.Dead) return;
        AddAction(skill);
    }
    public void Def()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Range02Ani.Dead) return;
        AddAction(def);
    }

    public void KeepAway()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Range02Ani.Dead) return;
        AddAction(keepAway);
    }

    public void Hurt()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Range02Ani.Dead) return;
        AddAction(hurt);
    }

    public void Break()
    {
        if (currentAction != null)
            if (currentAction.aniData.ani == Range02Ani.Dead) return;
        AddAction(breakAction);
    }

    public void Dead()
    {
        AddAction(dead);
    }

    private void AddAction<T>(T status) where T : Range02Action
    {
        tempAction = status;

        if (isAction)
        {
            if (IsActionAbleToInterupt())
            {
                actionList[0].ForceEnd();
            }
            else
            {
                return;
            }
        }

        ExcuteNextAction();
    }

    private bool IsActionAbleToInterupt()
    {
        if (tempAction.aniData.interuptOtherAnimation && !(actionList[0].GetType().ToString() == tempAction.GetType().ToString()))
            return true;
        else if (tempAction.aniData.interuptOwnAnimation)
            return true;

        return false;
    }

    private void ExcuteNextAction()
    {
        actionList.Add(tempAction);
        actionList[0].StartAction();
        actionList[0].PlayAnimation(actionList[0].aniData.ani);
    }

    public void RemoveCurrentAction()
    {
        lastAction = actionList[0];
        actionList.Clear();
    }
}
