using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Range02Action : MonoBehaviour
{
    protected Range02ActionManager actionManager;
    protected Range02Animation animator;
    protected NavMeshAgent agent;
    protected GameObject player;
    protected PlayerController playerCtrl;
    protected CapsuleCollider m_collider;

    public Range02AnimationData aniData;

    public virtual void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        actionManager = GetComponent<Range02ActionManager>();
        m_collider = GetComponent<CapsuleCollider>();
        animator = GetComponent<Range02Animation>();
        player = GameObject.FindWithTag("Player");
        playerCtrl = player.GetComponent<PlayerController>();
    }

    public virtual void StartAction()
    {
        SetActionEndTime();
    }

    public virtual void UpdateAction()
    {

    }

    public virtual void EndAction()
    {
        actionManager.RemoveCurrentAction();
    }

    public void PlayAnimation(Range02Ani ani)
    {
        animator.PlayAnimation(ani);
    }

    public virtual void SetActionEndTime()
    {
        if (animator.GetAnimationHolder(aniData.ani.ToString()).clip != null)
        {
            float actionLength = GetAnimationLength(aniData.ani);
            StartCoroutine(TickToEndAction(actionLength));
        }
    }

    protected float GetAnimationLength(Range02Ani ani)
    {
        return animator.GetAnimationHolder(ani.ToString()).Length;
    }

    public void ForceEnd()
    {
        StopAllCoroutines();
        EndAction();
    }

    protected IEnumerator TickToEndAction(float aniSec)
    {
        yield return new WaitForSeconds(aniSec);
        EndAction();
    }

    protected float GetPlayerDistance()
    {
        return Vector3.Distance(player.transform.position.GetZeroY(), transform.position.GetZeroY());
    }
    protected Vector3 GetPlayerDir()
    {
        return (player.transform.position.GetZeroY() - transform.position.GetZeroY()).normalized;
    }
    protected Vector3 GetPlayerDir(Vector3 ownPos)
    {
        return (player.transform.position.GetZeroY() - ownPos.GetZeroY()).normalized;
    }

    protected RaycastHit GetCapCastHit(Vector3 dir, float distance, LayerMask layerMask)
    {
        RaycastHit hit = new RaycastHit();
        Vector3 ownPos = new Vector3(transform.position.x, 0, transform.position.z);
        Physics.CapsuleCast(ownPos, ownPos + Vector3.up, m_collider.radius, dir, out hit, distance, layerMask);

        return hit;
    }
}
