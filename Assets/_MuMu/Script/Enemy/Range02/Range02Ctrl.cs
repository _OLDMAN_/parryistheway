using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Range02Ctrl : MonoBehaviour
{
    private new Range02Animation animation;
    private Range02ActionManager actionManager;
    private NavMeshAgent agent;

    public float moveSpeed = 5f;
    public float rotateSpeed = 1f;

    public float moveSec = 3f;
    public float idleSec = 1f;

    private float moveTimer;
    private float idleTimer;
    private float oriMoveSpeed;
    private Vector3 moveTarget;
    private bool isMove;

    void Start()
    {
        GetComp();
        InitialValue();
        ChangeMoveDir();
    }

    //獲取腳本
    private void GetComp()
    {
        actionManager = GetComponent<Range02ActionManager>();
        agent = GetComponent<NavMeshAgent>();
        animation = GetComponent<Range02Animation>();
    }

    //初始化值
    private void InitialValue()
    {
        agent.updateRotation = false;
        agent.speed = moveSpeed;
        oriMoveSpeed = moveSpeed;
        moveTimer = moveSec;
    }

    void Update()
    {
        if (actionManager.isAction) return;

        if (isMove)
            Move();
        else
            Idle();
    }

    //移動
    private void Move()
    {
        animation.PlayAnimation("Idle");
        if (moveTimer <= 0)
        {
            if (HaveNavMesh())
            {
                agent.isStopped = true;
                agent.velocity = Vector3.zero;
            }
            moveTimer = moveSec;
            isMove = false;
        }
        else
            moveTimer -= Time.deltaTime;

        RotateToDir();
        if (HaveNavMesh())
        {
            if (agent.remainingDistance <= agent.stoppingDistance)
                ChangeMoveDir();

            agent.SetDestination(moveTarget);
        }
    }

    //待機
    private void Idle()
    {
        if (idleTimer <= 0)
        {
            if (HaveNavMesh())
                agent.isStopped = false;
            idleTimer = idleSec;
            isMove = true;
        }
        else
        {
            idleTimer -= Time.deltaTime;
        }
    }

    //面向移動方向
    private void RotateToDir(bool forceRotate = false)
    {
        transform.forward = Vector3.Lerp(transform.forward, agent.velocity, rotateSpeed * Time.deltaTime);
    }

    //改變移動方向
    private void ChangeMoveDir()
    {
        NavMeshHit hit = default;
        int minDistance = Random.Range(3, 6);
        int maxFindCount = 100;
        while (maxFindCount > 0)
        {
            Vector3 randomPos = Random.insideUnitSphere * 5 + transform.position;
            NavMesh.SamplePosition(randomPos, out hit, 10, NavMesh.AllAreas);
            maxFindCount--;
            if (hit.distance >= minDistance)
                break;
        }
        if (hit.hit == false)
            moveTarget = transform.position;
        else
            moveTarget = hit.position;
    }

    //受否有NavMesh
    private bool HaveNavMesh()
    {
        NavMeshData data = FindObjectOfType<NavMeshSurface>().navMeshData;
        if (data != null)
            return true;

        return false;
    }
}
