using UnityEngine;
using UnityEngine.AI;

public class Range02KeepAway : Range02Action
{
    private Range02Warning warning;
    private Range02Attack attack;
    private Range02Ctrl ctrl;

    public float keepAwaySpeed = 1;
    public float warnSecPercentage = 0.5f;
    private float keepAwayTime;
    private Vector3 movePos;
    private Vector3 moveDir;

    public bool isKeepAway;

    private void Awake()
    {
        GetOwnComponent();
    }
    private void GetOwnComponent()
    {
        warning = GetComponent<Range02Warning>();
        ctrl = GetComponent<Range02Ctrl>();
    }


    public override void StartAction()
    {
        keepAwayTime = warnSecPercentage * warning.attackCD;
        base.StartAction();
        agent.isStopped = false;
        agent.speed = keepAwaySpeed;
        isKeepAway = true;
        ChangeKeepAwayPoint();
    }

    public override void UpdateAction()
    {
        if (warning.attackTimer <= warning.attackCD * warnSecPercentage || warning.isOnWarningRange == false)
        {
            isKeepAway = false;
            ForceEnd();
        }

        if (GetPlayerDistance() > attack.maxAttackRadius) return;
        float dot = Vector3.Dot(GetPlayerDir(), moveDir);
        if (agent.remainingDistance <= agent.stoppingDistance || dot > 0)
            ChangeKeepAwayPoint();
        RotateToMoveDir();
    }

    private void ChangeKeepAwayPoint()
    {
        NavMeshHit hit;
        int minDistance = Random.Range(3, 6);
        while (true)
        {
            Vector3 randomPos = Random.insideUnitSphere * 10 + transform.position;
            NavMesh.SamplePosition(randomPos, out hit, 10, NavMesh.AllAreas);
            Vector3 moveDir = hit.position - transform.position;
            moveDir.y = 0;
            float dot = Vector3.Dot(GetPlayerDir(), moveDir);
            if (hit.distance >= minDistance && dot <= 0)
                break;
        }

        movePos = hit.position;
        moveDir = movePos - transform.position;
        moveDir.y = 0;
        agent.SetDestination(movePos);
    }

    private void RotateToMoveDir()
    {
        transform.forward = Vector3.Lerp(transform.forward, agent.velocity, ctrl.rotateSpeed * Time.deltaTime);
    }

    public override void SetActionEndTime()
    {
        StartCoroutine(TickToEndAction(keepAwayTime));
    }

    public override void EndAction()
    {
        base.EndAction();
    }
}
