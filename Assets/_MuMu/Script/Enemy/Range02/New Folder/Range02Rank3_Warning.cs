using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range02Rank3_Warning : Range02Warning
{
    public LayerMask obsAndPlayer;
    //ĵ�٤�
    public override void UpdateAction()
    {
        if (GetPlayerDistance() > exitWarningRadius)
            ForceEnd();

        if (attackTimer <= 0)
        {
            if (hurt.isKnockBack == true) return;

            Vector3 ownDir = transform.forward;
            ownDir.y = 0;
            float angle = Vector3.Angle(ownDir, GetPlayerDir());
            RaycastHit hit = GetCapCastHit(GetPlayerDir(), 100, obsAndPlayer);
            bool isHitPlayer = false;
            if (hit.collider != null)
                isHitPlayer = hit.collider.CompareTag("Player");

            if (GetPlayerDistance() <= attack.maxAttackRadius && isHitPlayer)
            {
                ForceEnd();
                int randNum = Random.Range(0, 2);
                if (randNum == 0)
                    actionManager.Attack();
                else
                    actionManager.Skill01();
            }
            else
            {
                agent.SetDestination(player.transform.position);
                RotateToVelecity();
            }

            //if(isHitPlayer)
            //    RotateToPlayer();
        }
        else
        {
            //if (GetPlayerDistance() < attack.minAttackRadius)
            //{
            //    if (inRange == true)
            //    {
            //        moveDir = -GetPlayerDir();
            //        inRange = false;
            //    }
            //    float dot = Vector3.Dot(GetPlayerDir(), moveDir);
            //    if (agent.remainingDistance <= agent.stoppingDistance || dot > 0)
            //        ChangeKeepAwayPoint();

            //    RotateToVelecity();
            //}
            //else if (GetPlayerDistance() >= attack.maxAttackRadius)
            //{
            //    inRange = true;
            //    moveDir = agent.velocity;
            //    agent.SetDestination(player.transform.position);
            //    RotateToVelecity();
            //}
            //else
            //{
            //    inRange = true;
            //    agent.velocity = Vector3.zero;
            //    RotateToPlayer();
            //}
        }
    }
}
