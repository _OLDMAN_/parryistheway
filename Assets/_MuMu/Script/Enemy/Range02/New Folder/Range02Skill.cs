using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class Range02Skill : Range02Action
{
    private Range02Warning warning;
    public bool debug;
    public float sprintSpeed = 20;
    public float waitTime;
    public float rotateSpeed;
    public float radius = 2;
    [Range(0, 360)]
    public int arcAngle = 200;
    public int damage = 20;

    public bool isSkilling;
    public bool lookPlayer;

    private void Awake()
    {
        warning = GetComponent<Range02Warning>();
    }

    public override void StartAction()
    {
        base.StartAction();
        lookPlayer = false;
        isSkilling = true;
        transform.forward = GetPlayerDir();
    }

    public override void UpdateAction()
    {
        Debug.Log("技能");
        if (lookPlayer == false)
        {
            transform.position += GetPlayerDir() * Time.deltaTime * sprintSpeed;
            if (GetPlayerDistance() <= 3)
            {
                StartCoroutine(DelayAttack());
                lookPlayer = true;
            }
        }
        if (lookPlayer == true)
            RotateToPlayer();
    }

    private IEnumerator DelayAttack()
    {
        PlayAnimation(Range02Ani.Skill01_1);
        yield return new WaitForSeconds(GetAnimationLength(Range02Ani.Skill01_1));
        yield return new WaitForSeconds(waitTime - 0.5f);
        //ParticleManager.Instance.PlayParticle("Range02PreSkillAttack", transform.position, transform.rotation);
        yield return new WaitForSeconds(0.5f);
        PlayAnimation(Range02Ani.Skill01_2);
        ParticleManager.Instance.PlayParticle("Range02SkillAttack", transform.position, transform.rotation);
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius, LayerMask.GetMask("Player"));
        foreach (Collider collider in colliders)
        {
            Vector3 dir = collider.transform.position - transform.position;
            dir.y = 0;
            float angle = Vector3.Angle(transform.forward, dir);
            if (angle < arcAngle / 2f)
            {
                collider.GetComponent<PlayerActionManager>().StartHurt(damage, transform);
            }
        }
        yield return new WaitForSeconds(GetAnimationLength(Range02Ani.Skill01_2));
        EndAction();
    }

    public override void EndAction()
    {
        warning.attackTimer = warning.attackRequidSecRand;
        isSkilling = false;
        lookPlayer = false;
        base.EndAction();
    }

    private void RotateToPlayer()
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(GetPlayerDir()), Time.deltaTime * rotateSpeed);
    }

    //設定攻擊時間 = 前搖 + 攻擊
    public override void SetActionEndTime()
    {
        //float actionLength = 0;
        //foreach (EnemyAni a in aniData.ani)
        //    actionLength += GetAnimationLength(a);
        //actionLength += waitTime;
        //coroutines.Add(StartCoroutine(TickToEndAction(actionLength)));
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        if(debug == true)
        GizmosExtensions.DrawWireArc(transform.position, transform.forward, arcAngle, radius);
    }
}
