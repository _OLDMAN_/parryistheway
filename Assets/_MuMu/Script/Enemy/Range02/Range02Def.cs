using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range02Def : Range02Action
{
    private Range02Ctrl ctrl;

    public override void Start()
    {
        base.Start();
        ctrl = GetComponent<Range02Ctrl>();
    }
    public override void StartAction()
    {
        base.StartAction();
        StartCoroutine(ToDef02Animation());
    }
    public override void UpdateAction()
    {
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.LookRotation(GetPlayerDir()), Time.deltaTime * ctrl.rotateSpeed);
    }
    private IEnumerator ToDef02Animation()
    {
        yield return new WaitForSeconds(GetAnimationLength(Range02Ani.Def01));
        animator.PlayAnimation(Range02Ani.Def02);
    }


    public override void SetActionEndTime()
    {
        
    }
}
