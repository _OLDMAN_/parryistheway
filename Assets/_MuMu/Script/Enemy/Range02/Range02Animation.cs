using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Range02Animation : EnemyAnimation
{
    public override void PlayAnimation<Range02Ani>(Range02Ani name, bool skipWhenRepeat = true, bool blendPlay = true)
    {
        base.PlayAnimation(name, skipWhenRepeat, blendPlay);
    }
}
