using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class EnemyHealth : MonoBehaviour
{
    public HealthInfo info;

    public float maxHP = 100;
    [HideInInspector] public float HP;

    public Vector3 damageSpawnOffset;
    private bool isOpen;

    public virtual void Awake()
    {
        HP = maxHP;
    }
    public void Damage(float value, bool heavyDamage = false)
    {
        if (isOpen == false)
        {
            info.health.SetActive(true);
            isOpen = true;
        }
        HP -= value;
        info.healthImage.fillAmount = HP / maxHP;
        SpawnDamageText(value, heavyDamage);
    }

    public void Heal(float value)
    {
        HP += value;
        float fillPer = HP / maxHP;
        info.healthImage.fillAmount = fillPer;
        info.followImage.fillAmount = fillPer;
        SpawnDamageText(value);
    }

    public void HideHealth()
    {
        info.health.SetActive(false);
    }

    public void DisplayHealth()
    {
        info.health.SetActive(true);
    }

    protected virtual void SpawnDamageText(float value, bool heavyAttack = false)
    {
        float absValue = Mathf.Abs(value);
        Vector3 spawnPos = info.health.transform.position;

        DamageText damageText = Instantiate(info.damageTextPrefab, spawnPos + damageSpawnOffset, info.canvas.transform.rotation, info.canvas.transform).GetComponent<DamageText>();
        Vector2 uiSize = info.healthImage.GetComponent<RectTransform>().sizeDelta / 2;
        Vector3 effectPos = spawnPos + new Vector3(Random.Range(-uiSize.x, uiSize.x), Random.Range(0, 0.5f), 0);
        damageText.tmpText.text = absValue.ToString();
        damageText.TextEffect(effectPos, heavyAttack);
    }
}
