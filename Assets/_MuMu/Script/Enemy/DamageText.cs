using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class DamageText : MonoBehaviour
{
    public float time;
    public TMP_Text tmpText;
    private Tween moveTween;
    private Tween scaleTween;
    private Tween fadeTween;

    public void TextEffect(Vector3 movePos, bool heavyEffect = false)
    {
        if (heavyEffect == false)
        {
            tmpText.color = Color.white;
            scaleTween = transform.DOScale(Vector3.one * 0.4f, time).From(Vector3.one);
        }
        else
        {
            tmpText.color = Color.red;
            scaleTween = transform.DOScale(Vector3.one * 0.4f, time).From(Vector3.one * 1.5f);
        }
        moveTween = transform.DOMove(movePos , time);
        fadeTween = tmpText.DOFade(0.3f, time).From(1f);
        StartCoroutine(Recycle());
    }


    public IEnumerator Recycle()
    {
        yield return new WaitForSeconds(time);
        Destroy(gameObject);
    }

    private void OnDestroy()
    {
        moveTween.Kill();
        scaleTween.Kill();
        fadeTween.Kill();
    }
}
