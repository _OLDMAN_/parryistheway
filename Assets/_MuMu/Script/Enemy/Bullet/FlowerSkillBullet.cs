using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlowerSkillBullet : MonoBehaviour
{
    private GameObject fallPointObj;

    private Vector3 speed; 
    private Vector3 Gravity;
    private float g = -29.4f;
    private float dTime = 0;
    public int damage = 10;
    public float damageRange = 0.2f;

    public void SetFallPoint(Vector3 fallPos, float needTime, GameObject fallPointObj = null)
    {
        this.fallPointObj = fallPointObj;
        Gravity = Vector3.zero;
        dTime = 0;
        speed = new Vector3(
            (fallPos.x - transform.position.x) / needTime,
            (fallPos.y - transform.position.y) / needTime - 0.5f * g * needTime,
            (fallPos.z - transform.position.z) / needTime);
    }

    void Update()
    {
        Gravity.y = g * (dTime += Time.deltaTime);
        Vector3 velocity = speed + Gravity;
        transform.position += velocity * Time.deltaTime;
        transform.forward = velocity;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Ground"))
        {
            Collider[] colliders = Physics.OverlapSphere(transform.position, damageRange, LayerMask.GetMask("Player"));
            foreach (Collider c in colliders)
                c.GetComponent<PlayerActionManager>().StartHurt(damage, transform);
            ParticleManager.Instance.PlayParticle("Range01Skill01Hit", transform.position, Quaternion.identity);
            GetComponent<PoolRecycle>().Recycle();
            fallPointObj.GetComponent<PoolRecycle>().Recycle();
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, damageRange);
    }
}
