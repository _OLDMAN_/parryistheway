using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBullet : MonoBehaviour, IBlockable
{
    private Rigidbody rb;
    protected Transform player;

    private bool follow;
    [HideInInspector] public float speed;
    [HideInInspector] public int damage;

    [HideInInspector] public Vector3 dir;

    public Transform target { get; set; }
    public bool canBlock { get; set; } = true;
    public bool towerBullet { get; set; } = false;

    private void OnEnable()
    {
        canBlock = true;
    }

    private void Start()
    {
        player = GameObject.FindWithTag("Player").transform;
        rb = GetComponent<Rigidbody>();
    }

    public void Update()
    {
        if (follow == true)
            FollowMove();
        else
            Move();
    }

    public void Move()
    {
        rb.velocity = dir * speed;
        transform.forward = dir;
    }

    public void FollowMove()
    {
        Vector3 playerDir = player.transform.position - transform.position;
        playerDir.y = 0;
        float angle = Vector3.Angle(transform.forward, playerDir);
        int maxFollowAngle = 30;
        float rotateSpeed = 10f;
        if (angle <= maxFollowAngle)
            dir = Vector3.Lerp(dir, playerDir, rotateSpeed * Time.deltaTime);
        transform.forward = dir;
        rb.velocity = transform.forward * speed;
    }

    public void SetBullet(float speed, int damage, Vector3 dir, Transform target, bool lightFollow = false)
    {
        this.speed = speed;
        this.damage = damage;
        this.dir = dir;
        follow = lightFollow;
        this.target = target;
    }

    public void Block(GameObject attacker)
    {
        attacker.GetComponent<Block>().PlayBlockParticle(transform.position, target.position);
        IHurtable hurtable = target.GetComponent<IHurtable>();
        IBreakable breakable = target.GetComponent<IBreakable>();
        IDestroyable destroyable = target.GetComponent<IDestroyable>();

        breakable?.ReduceBreakCount(1);
        hurtable?.Damage(transform, 0);
        destroyable?.Damage();
        //ParticleManager.Instance.PlayParticle("EnemyRaserHit", target.position, Quaternion.identity);
        Destroy(gameObject);
    }

    private void OnTriggerEnter(Collider other)
    {
        IHurtable hurt = other.GetComponent<IHurtable>();
        IDestroyable destroyable = other.GetComponent<IDestroyable>();

        if (other.CompareTag("Player") && canBlock == true)
        {
            other.GetComponent<PlayerActionManager>().StartHurt(damage, transform.forward);
            DestroyBullet();
        }
        else if (hurt != null)
        {
            if(other.CompareTag("Enemy") && canBlock == false)
            {
                hurt.Damage(transform, damage);
                DestroyBullet();
            }
            else if(other.CompareTag("DestructibleObs"))
            {
                hurt.Damage(transform, damage);
                DestroyBullet();
            }
            else if(other.CompareTag("TowerTrap") && towerBullet == false)
            {
                hurt.Damage(transform, damage);
                DestroyBullet();
            }
        }
        else if(destroyable != null)
        {
            destroyable?.Damage();
        }
        else
        {
            SoundManager.Instance.PlaySound("BulletHitWall");
            DestroyBullet();
        }
    }

    private void DestroyBullet()
    {
        ParticleManager.Instance.PlayParticle("BulletHit", transform.position, Quaternion.identity);
        Destroy(gameObject);
    }
}
