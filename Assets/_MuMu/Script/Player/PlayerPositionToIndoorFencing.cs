using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPositionToIndoorFencing : MonoBehaviour
{
    public MeshRenderer FencingMeshRenderer;
    public Vector3 PositionOffset;
    void Update()
    {
        if(FencingMeshRenderer != null)
        {
            FencingMeshRenderer.material.SetVector("_PlayerMaskPosition" , transform.position + PositionOffset);
        }
    }
}
