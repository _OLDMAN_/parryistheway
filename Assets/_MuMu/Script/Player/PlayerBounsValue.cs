using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class PlayerBounsValue : MonoBehaviourSingleton<PlayerBounsValue>
{
    public TMP_Text hpBounsText;
    public TMP_Text baseDamageBounsText;
    public TMP_Text skillDamageBounsText;

    private int hpOri = 100;
    private int baseDamageOri = 100;
    private int skillDamageOri = 100;

    public int hpBouns = 0;
    public int baseDamageBouns = 0;
    public int skillDamageBouns = 0;

    private int currentHpBouns { get { return hpOri + hpBouns; } }
    private int currentBaseDamageBouns { get { return baseDamageOri + baseDamageBouns; } }
    private int currentSkillDamageBouns { get { return skillDamageOri + skillDamageBouns; } }

    public int playerOriHp;

    private void Start()
    {
        //hpBounsText = GemManager.Instance.hpObj;
        //baseDamageBounsText = GemManager.Instance.damageObj;
        //skillDamageBounsText = GemManager.Instance.skillDamage;
        playerOriHp = PlayerHP.Instance.maxHP;
    }

    public void AddHpBouns(int count)
    {
        DoHpAni(count);
    }

    public void ReduceHpBouns(int count)
    {
        DoHpAni(-count);
    }

    private void DoHpAni(int count)
    {
        float oriSize = hpBounsText.fontSize;
        float fontSize = hpBounsText.fontSize;
        Tween sizeChange = DOTween.To(() => fontSize, x => fontSize = x, fontSize + 10, 0.15f);
        sizeChange.SetLoops(-1, LoopType.Yoyo);
        sizeChange.OnUpdate(() => { hpBounsText.fontSize = fontSize; });
        Tween hpLerp = DOTween.To(() => hpBouns, x => hpBouns = x, hpBouns + count, 0.6f);
        hpLerp.OnUpdate(() => { hpBounsText.text = currentHpBouns.ToString() + "%"; });
        hpLerp.OnComplete(() => { sizeChange.Kill(); hpBounsText.fontSize = oriSize; SetMaxHp(); });
    }

    public void AddBaseDamageBouns(int count)
    {
        DoBaseDamageAni(count);
    }
    public void ReduceBaseDamageBouns(int count)
    {
        DoBaseDamageAni(-count);
    }
    private void DoBaseDamageAni(int count)
    {
        float oriSize = baseDamageBounsText.fontSize;
        float fontSize = baseDamageBounsText.fontSize;
        Tween sizeChange = DOTween.To(() => fontSize, x => fontSize = x, fontSize + 10, 0.15f);
        sizeChange.SetLoops(-1, LoopType.Yoyo);
        sizeChange.OnUpdate(() => { baseDamageBounsText.fontSize = fontSize; });
        Tween lerp = DOTween.To(() => baseDamageBouns, x => baseDamageBouns = x, baseDamageBouns + count, 0.6f);
        lerp.OnUpdate(() => { baseDamageBounsText.text = currentBaseDamageBouns.ToString() + "%"; });
        lerp.OnComplete(() => { sizeChange.Kill(); baseDamageBounsText.fontSize = oriSize; });
    }

    public void AddSkillDamageBouns(int count)
    {
        DoSkillDamageAni(count);
    }
    public void ReduceSkillDamageBouns(int count)
    {
        DoSkillDamageAni(-count);
    }
    private void DoSkillDamageAni(int count)
    {
        float oriSize = skillDamageBounsText.fontSize;
        float fontSize = skillDamageBounsText.fontSize;
        Tween sizeChange = DOTween.To(() => fontSize, x => fontSize = x, fontSize + 10, 0.15f);
        sizeChange.SetLoops(-1, LoopType.Yoyo);
        sizeChange.OnUpdate(() => { skillDamageBounsText.fontSize = fontSize; });
        Tween lerp = DOTween.To(() => skillDamageBouns, x => skillDamageBouns = x, skillDamageBouns + count, 0.6f);
        lerp.OnUpdate(() => { skillDamageBounsText.text = currentSkillDamageBouns.ToString() + "%"; });
        lerp.OnComplete(() => { sizeChange.Kill(); skillDamageBounsText.fontSize = oriSize; });
    }

    public void SetMaxHp()
    {
        int oriMaxHp = PlayerHP.Instance.maxHP;
        int bounsHp = Mathf.RoundToInt(playerOriHp * (currentHpBouns / 100f));
        PlayerHP.Instance.maxHP = bounsHp;
        PlayerHP.Instance.Heal(bounsHp - oriMaxHp);
    }

    public int GetBounsBaseDamage(int damage)
    {
        return Mathf.RoundToInt(damage * (1 + currentBaseDamageBouns / 100f));
    }

    public int GetBounsSkillDamage(int damage)
    {
        return Mathf.RoundToInt(damage * (1 + currentSkillDamageBouns / 100f));
    }
}
