using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class PlayerHealth : MonoBehaviour
{
    public Text text;
    public Image bar;
    public Image followBar;
    public float followSpeed = 0.5f;

    public RectTransform recttransform;

    private Vector2 origin;
    private void Start()
    {
        origin = recttransform.anchoredPosition;
    }

    public void SlideOut()
    {
        recttransform.DOAnchorPosY(origin.y - Camera.main.pixelHeight * 0.3f, 1f);
    }
    public void SlideIn()
    {
        recttransform.DOAnchorPosY(origin.y , 1f);
    }

}
