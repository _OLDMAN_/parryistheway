using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new ThrowSpear", menuName = "PlayerSkill/ThrowSpear")]
public class ThrowSpearData : PlayerSkill
{
    [Header("數值")]
    public int throwDamage = 100;
    public float throwSpeed = 40;

    public override void Cast()
    {
        PlayerActionManager.Instance.ThrowSpear();
    }

    public override string Detail()
    {
        return "飛矛";
    }
}
