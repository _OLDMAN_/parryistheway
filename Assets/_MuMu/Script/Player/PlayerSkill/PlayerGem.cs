using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NewGem
{
    public PlayerSkill skill;
    public List<SecondSkill> secondSkills;

    public NewGem(PlayerSkill _skill, List<SecondSkill> _secondSkills)
    {
        skill = _skill;
        secondSkills = _secondSkills;
    }
}

public enum NewGemType { Dash, Block, Q }

public class PlayerGem : MonoBehaviourSingleton<PlayerGem>
{
    public NewGem dashAttack;
    public NewGem autoBlockSkill;
    public NewGem blockSkill;

    public NewGem good;

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Keypad0))
        {
            TeachManager.Instance.unLockDash = true;
            TeachManager.Instance.unLockBlockBurst = true;
            TeachManager.Instance.unLockBlockAndBlockAttack = true;
            TeachManager.Instance.unLockAttackAndDashAttack = true;
            dashAttack = good;
        }
    }
}
