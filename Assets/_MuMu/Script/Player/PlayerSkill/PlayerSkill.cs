using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SkillQuality { Purple, Gold }
public class PlayerSkill : ScriptableObject
{
    public SkillQuality quality;
    public int weight;
    public int secondSkillCount;

    [Header("技能圖示")]
    public Sprite skillIcon;

    [Header("所需能量和打斷值")]
    public int needEnergy = 1;
    public int breakValue = 1;

    [Header("傷害層數")]
    public LayerMask damageLayer;

    public virtual void Cast()
    {
        Debug.Log("NoSkillFuntion");
    }

    public virtual string Detail()
    {
        return "No Details";
    }
}
