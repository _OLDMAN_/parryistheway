using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new Kick", menuName = "PlayerSkill/Kick")]
public class KickData : PlayerSkill
{
    [Header("�ƭ�")]
    public int damage = 30;
    public float knockBackdDistance = 3;

    [Header("�ޯ�d��")]
    public bool debug;
    public Vector3 kickOffset;
    public Vector3 kickSize = Vector3.one;

    public override void Cast()
    {
        PlayerActionManager.Instance.Kick();
    }
    public override string Detail()
    {
        return "踢擊";
    }
}
