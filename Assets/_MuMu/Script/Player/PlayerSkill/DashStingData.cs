using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new DashSting", menuName = "PlayerSkill/DashSting")]
public class DashStingData : PlayerSkill
{
    [Header("�ƭ�")]
    public int damage = 20;

    [Header("�ޯ�d��")]
    public bool debug;
    public Vector3 dashOffset;
    public Vector3 dashSize = Vector3.one;

    public override void Cast()
    {
        PlayerActionManager.Instance.DashSting();
    }
    public override string Detail()
    {
        return "雷槍";
    }
}
