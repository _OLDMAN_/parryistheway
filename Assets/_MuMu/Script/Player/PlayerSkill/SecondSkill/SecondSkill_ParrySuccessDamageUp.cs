using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SecondSkill/ParrySuccessDamageUp", fileName = "newParrySuccessDamageUp", order = 0)]
public class SecondSkill_ParrySuccessDamageUp : SecondSkill
{
    public int duration;

    public override string Detail()
    {
        return "格擋後" + duration + "秒內此技能傷害增加" + value + "%";
    }
}
