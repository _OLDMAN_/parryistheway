using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SecondSkill/RangeUp", fileName = "newRangeUp", order = 0)]
public class SecondSkill_RangeUp : SecondSkill
{
    public override string Detail()
    {
        return "此技能的範圍增加" + value + "%";
    }
}
