using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SecondSkill/MoveSpeedUp", fileName = "newMoveSpeedUp", order = 0)]
public class SecondSkill_MoveSpeedUp : SecondSkill
{
    public int duration;

    public override string Detail()
    {
        return "技能擊中後" + duration + "秒內移動速度增加" + value + "%";
    }
}
