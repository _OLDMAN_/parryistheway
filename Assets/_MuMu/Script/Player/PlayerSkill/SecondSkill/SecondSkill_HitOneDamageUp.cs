using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "SecondSkill/HitOneDamageUp", fileName = "newHitOneDamageUp", order = 0)]
public class SecondSkill_HitOneDamageUp : SecondSkill
{
    public override string Detail()
    {
        return "技能只擊中一名敵人時傷害增加" + value + "%";
    }
}
