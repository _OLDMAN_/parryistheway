using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SecondSkill : ScriptableObject
{
    public int weight;
    public int value;

    public virtual string Detail()
    {
        return "No Details";
    }
}
