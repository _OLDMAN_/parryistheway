using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewMaxHpUp : Enhance
{
    public NewMaxHpUp(EnhanceInfo info, string _detail) : base(info, _detail)
    {
        quality = info.quality;
        value = info.value;
        detail = _detail;
    }

    public override void Cast()
    {
        PlayerBounsValue.Instance.AddHpBouns(value);
    }
}
