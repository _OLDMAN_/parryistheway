using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class EnhanceInfo
{
    public int weight;
    public EnhanceQuality quality;
    public int value;
}

public enum EnhanceQuality { Blue, Purple, Gold }

public class EnhanceData : ScriptableObject
{
    public int weight;
    public List<EnhanceInfo> enhanceQuality;

    public virtual string Detail(EnhanceQuality quality)
    {
        return "";
    }

    public virtual Enhance GetEnhance()
    {
        Dictionary<EnhanceInfo, int> weightDict = new Dictionary<EnhanceInfo, int>();
        foreach (EnhanceInfo enhance in enhanceQuality)
            weightDict.Add(enhance, enhance.weight);

        int totalweight = 0;
        foreach (var weight in weightDict.Values)
            totalweight += weight;

        int ranNum = Random.Range(0, totalweight);
        int counter = 0;
        foreach (var temp in weightDict)
        {
            counter += temp.Value;
            if (ranNum < counter)
                return new Enhance(temp.Key, Detail(temp.Key.quality));
        }

        return null;
    }
}
