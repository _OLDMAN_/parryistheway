﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newAttackDamageBouns", menuName = "Enhance/AttackDamageBouns", order = 0)]
public class AttackDamageBounsData : EnhanceData
{
    public override string Detail(EnhanceQuality quality)
    {
        foreach(EnhanceInfo enhance in enhanceQuality)
        {
            if (enhance.quality == quality)
                return "增加" + enhance.value + "%普通攻擊傷害";
        }
        return "";
    }

    public override Enhance GetEnhance()
    {
        Dictionary<EnhanceInfo, int> weightDict = new Dictionary<EnhanceInfo, int>();
        foreach (EnhanceInfo enhance in enhanceQuality)
            weightDict.Add(enhance, enhance.weight);

        int totalweight = 0;
        foreach (var weight in weightDict.Values)
            totalweight += weight;

        int ranNum = Random.Range(0, totalweight);
        int counter = 0;
        foreach (var temp in weightDict)
        {
            counter += temp.Value;
            if (ranNum < counter)
                return new AttackDamageBouns(temp.Key, Detail(temp.Key.quality));
        }

        return null;
    }
}
