using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Enhance
{
    public EnhanceQuality quality;
    public int value;
    public string detail;

    public Enhance(EnhanceInfo info, string _detail)
    {
        quality = info.quality;
        value = info.value;
        detail = _detail;
    }

    public virtual void Cast()
    {

    }
}
