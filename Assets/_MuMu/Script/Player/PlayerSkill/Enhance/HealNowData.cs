using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newHealNow", menuName = "Enhance/HealNow", order = 0)]
public class HealNowData : EnhanceData
{
    public override string Detail(EnhanceQuality quality)
    {
        foreach (EnhanceInfo enhance in enhanceQuality)
        {
            if (enhance.quality == quality)
                return "立即回復" + enhance.value + "%生命";
        }
        return "";
    }
    public override Enhance GetEnhance()
    {
        Dictionary<EnhanceInfo, int> weightDict = new Dictionary<EnhanceInfo, int>();
        foreach (EnhanceInfo enhance in enhanceQuality)
            weightDict.Add(enhance, enhance.weight);

        int totalweight = 0;
        foreach (var weight in weightDict.Values)
            totalweight += weight;

        int ranNum = Random.Range(0, totalweight);
        int counter = 0;
        foreach (var temp in weightDict)
        {
            counter += temp.Value;
            if (ranNum < counter)
                return new HealNow(temp.Key, Detail(temp.Key.quality));
        }

        return null;
    }
}
