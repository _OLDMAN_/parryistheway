using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackDamageBouns : Enhance
{
    public AttackDamageBouns(EnhanceInfo info, string _detail) : base(info, _detail)
    {
        quality = info.quality;
        value = info.value;
        detail = _detail;
    }

    public override void Cast()
    {
        PlayerBounsValue.Instance.AddBaseDamageBouns(value);
    }
}
