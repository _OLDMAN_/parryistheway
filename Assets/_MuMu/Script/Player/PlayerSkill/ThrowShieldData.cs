using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new ThrowShield", menuName = "PlayerSkill/ThrowShield")]
public class ThrowShieldData : PlayerSkill
{
    [Header("數值")]
    public float throwSpeed = 3f;
    public int throwDamage = 80;

    [Header("爆炸數值")]
    public bool debug;
    public int arcDamage = 40;
    public float arcRadius = 6.5f;
    [Range(0, 360)]
    public float arcAngle = 120f;

    public override void Cast()
    {
        PlayerActionManager.Instance.ThrowShield();
    }
    public override string Detail()
    {
        return "飛盾";
    }
}
