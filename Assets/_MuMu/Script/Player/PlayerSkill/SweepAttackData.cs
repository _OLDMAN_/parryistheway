using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new SweepAttack", menuName = "PlayerSkill/SweepAttack")]
public class SweepAttackData : PlayerSkill
{
    [Header("數值")]
    public int damage = 30;

    public bool debug;
    public float radius = 7f;
    [Range(0, 360)]
    public float angle = 90f;

    public override void Cast()
    {
        PlayerActionManager.Instance.SweepAttack();
    }
    public override string Detail()
    {
        return "橫掃攻擊";
    }
}
