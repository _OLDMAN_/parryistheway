using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new ShieldDash", menuName = "PlayerSkill/ShieldDash")]
public class ShieldDashData : PlayerSkill
{
    [Header("數值")]
    public int dashDamage = 20;
    public float dashDamageCD = 0.3f;

    [Header("�ޯ�d��")]
    public bool debug;
    public Vector3 dashOffset;
    public Vector3 dashSize = Vector3.one;
    public float radius = 2.5f;

    [Header("�z���ƭ�")]
    public int arcDamage = 40;
    public float arcKnockBack = 3f;
    public float arcRadius = 2.5f;
    [Range(0, 360)]
    public float arcAngle = 200f;

    public override void Cast()
    {
        PlayerActionManager.Instance.ShieldDash();
    }
    public override string Detail()
    {
        return "盾衝";
    }
}
