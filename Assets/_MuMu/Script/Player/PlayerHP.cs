using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class PlayerHP : MonoBehaviourSingleton<PlayerHP>
{
    private Die die;
    public int maxHP;
    private int hp;
    public int HP 
    {
        get { return hp; }
        set 
        {
            if (value > maxHP)
                hp = maxHP;
            else if (value <= 0)
                PlayerActionManager.Instance.Die();
            hp = Mathf.Clamp(value, 0, maxHP);
            OnHPChange();
        }
    }
    public PlayerHealth health;

    private bool isLowHp;
    private bool isMaxHp;

    public Material bloodMaterial;

    public override void Awake()
    {
        base.Awake();
        HP = maxHP;
    }

    void Start()
    {
        Modify();
        die = GetComponent<Die>();
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad1))
        {
            PlayerBounsValue.Instance.hpBouns += 100;
            PlayerBounsValue.Instance.SetMaxHp();
        }
        else if (Input.GetKeyDown(KeyCode.Keypad2))
            Heal(100);
        ModifyFollowBar();
    }

    private void OnHPChange()
    {
        CheckAction();
        Modify();
    }

    private void Modify()
    {
        health.text.text = hp + "/" + maxHP;
        health.bar.fillAmount = hp / (float)maxHP;
    }

    private void CheckAction()
    {
        if (hp <= maxHP / 4 && isLowHp == false)
        {
            bloodMaterial.DOFloat(1, "_BloodEdge", 1);
            isLowHp = true;
        }
        else if (hp > maxHP / 4 && isLowHp == true)
        {
            bloodMaterial.DOFloat(0, "_BloodEdge", 1);
            isLowHp = false;
        }

        if (hp >= maxHP && isMaxHp == false)
        {
            isMaxHp = true;
        }
        else if (hp < maxHP && isMaxHp == true)
        {
            isMaxHp = false;
        }
    }

    private void ModifyFollowBar()
    {
        if (health.followBar.fillAmount > health.bar.fillAmount)
            health.followBar.fillAmount -= Time.deltaTime * health.followSpeed;
        else
            health.followBar.fillAmount = health.bar.fillAmount;
    }

    public void Damage(int count)
    {
        hp -= count;

        if (hp <= 0 && die.isDead == false)
            PlayerActionManager.Instance.Die();

        Modify();
    }

    public void Heal(int value)
    {
        ParticleManager.Instance.PlayParticle("PlayerHeal", transform.position, Quaternion.identity);
        hp += value;

        Modify();
    }
    public void HealPercent(float percent)
    {
        ParticleManager.Instance.PlayParticle("PlayerHeal", transform.position, Quaternion.identity);
        hp += Mathf.RoundToInt(maxHP * (percent / 100));
        if(hp > maxHP)
            hp = maxHP;

        Modify();
    }

    public void ResetValue()
    {
        hp = maxHP;
        die.isDead = false;
        Hurt.isInvincible = false;
        Modify();
    }

    private void OnApplicationQuit()
    {
        bloodMaterial.SetFloat("_BloodEdge", 0);
    }

    public void SwitchHP(bool active)
    {
        if (active)
        {
            health.SlideIn();
        }
        else
        {
            health.SlideOut();
        }
    }
}
