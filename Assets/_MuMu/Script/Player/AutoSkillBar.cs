using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoSkillBar : MonoBehaviour
{
    public GameObject fillParticle;
    public Image skillIcon;
    public Text cdText;
    public Image cdFrame;

    public void SkillNotReady()
    {
        fillParticle.SetActive(false);
        Color color = skillIcon.color;
        color.a = 0.15f;
        skillIcon.color = color;
    }

    public void SkillReady()
    {
        fillParticle.SetActive(true);
        Color color = skillIcon.color;
        color.a = 0.8f;
        skillIcon.color = color;
        cdText.gameObject.SetActive(false);
        cdFrame.fillAmount = 0;
    }
}
