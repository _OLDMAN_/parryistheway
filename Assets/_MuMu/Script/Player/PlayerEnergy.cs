using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerEnergy : MonoBehaviourSingleton<PlayerEnergy>
{
    public int maxEnergy = 3;
    private int _energy;
    public int energy
    {
        get { return _energy; }
        set 
        {
            if (value > maxEnergy)
                _energy = maxEnergy;
            else
                _energy = value;
            OnEnergyChange();
        }
    }

    public bool isEnergyFull 
    {
        get { return energy == maxEnergy ? true : false; }
    }

    public GameObject energy_1;
    public GameObject energy_2;
    public ParticleSystem energy_3;

    private void OnEnergyChange()
    {
        energy_1.SetActive(false);
        energy_2.SetActive(false);
        if (energy == 1)
            energy_1.SetActive(true);
        else if (energy == 2)
            energy_2.SetActive(true);
        else if (energy == 3)
            energy_3.Play();
    }

    private void LateUpdate()
    {
        if(energy_1.activeSelf)
            energy_1.transform.rotation = Quaternion.identity;
        if(energy_2.activeSelf)
            energy_2.transform.rotation = Quaternion.identity;
    }
}
