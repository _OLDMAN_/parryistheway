using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class RoomBase : MonoBehaviour
{
    public RoomSetting roomSetting;

    public Action OnRoomEnter;
    public Action OnPlayerWalkInEnd;
    public Action OnRoomClear;
    public Action OnRoomRestart;

    public virtual void Awake()
    {
        SetEvent();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Keypad3))
        {
            PlayerController.Instance.SetPos(roomSetting.endPos[roomSetting.endPos.Length - 1].position);
        }
    }
    public void SetEvent()
    {
        OnRoomEnterSet();
        OnPlayerWalkInEndSet();
        OnRoomClearSet();
        OnRoomRestartSet();
    }

    public virtual void OnRoomEnterSet()
    {
        OnRoomEnter += WaveManager.Instance.ClearEnemy;
        OnRoomEnter += OpenEneterDoor;
        OnRoomEnter += InkScreenFadeIn;
        OnRoomEnter += PlayerWalkIn;
        OnRoomEnter += NavMeshBake.Instance.BakeNavMesh;
    }

    public virtual void OnPlayerWalkInEndSet()
    {
        OnPlayerWalkInEnd += CloseEnterDoor;
        OnPlayerWalkInEnd += CloseFocusCamera;
        OnPlayerWalkInEnd += QuestManager.Instance.QuestStart;
    }

    public virtual void OnRoomClearSet()
    {
        OnRoomClear += OpenExitDoor;
        OnRoomClear += StopTrap;
    }

    public virtual void OnRoomRestartSet()
    {
        OnRoomRestart += BlackOut;
    }

    protected void BlackOut()
    {
        BlackManager.Instance.InkBlackFadeOut(1, () =>
        {
            PlayerHP.Instance.ResetValue();
            BlackManager.Instance.InkBlackFadeIn(1, 0.5f);
            OpenEneterDoor();
            PlayerAIMove.Instance.StartAIMove(roomSetting.startPos.position, GetPathPoints(roomSetting.endPos), true, () => {
                roomSetting.enterDoor?.CloseDoor();
                roomSetting.enterWall.SetActive(true);
            });
        });
    }

    protected void OpenEneterDoor()
    {
        roomSetting.enterWall.SetActive(false);
        roomSetting.enterDoor?.ResetDoor();
    }
    protected void CloseEnterDoor()
    {
        foreach (ExitDoor ex in roomSetting.exitDoors) 
            ex.CloseDoor();
        roomSetting.enterDoor?.CloseDoor();
        roomSetting.enterWall.SetActive(true);
    }
    protected void CloseFocusCamera()
    {
        roomSetting.startFocusCamera.SetActive(false);
    }
    protected void InkScreenFadeIn()
    {
        BlackManager.Instance.InkBlackFadeIn(0.2f);
    }

    protected void PlayerWalkIn()
    {
        PlayerAIMove.Instance.StartAIMove(roomSetting.startPos.position, GetPathPoints(roomSetting.endPos), true, ()=> OnPlayerWalkInEnd?.Invoke());
    }

    protected virtual void OpenExitDoor()
    {
        foreach (ExitDoor exDoor in roomSetting.exitDoors)
            exDoor.OpenDoor();
        roomSetting.exitWall.SetActive(false);
    }

    public void StopTrap()
    {
        foreach (GameObject trap in roomSetting.trapList)
            trap.GetComponent<IStatusUseAble>()?.StopUse();
    }

    protected Vector3[] GetPathPoints(Transform[] transforms)
    {
        Vector3[] pathPoints = new Vector3[transforms.Length];
        for (int i = 0; i < transforms.Length; i++)
            pathPoints[i] = transforms[i].position;
        return pathPoints;
    }

}
