﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RoomSetting : MonoBehaviour
{
    [Header("初始設定")]
    public GameObject startFocusCamera;
    public Transform startPos;
    public Transform[] endPos;
    public SceneName sceneName;
    [Header("出入牆壁設定")]
    public GameObject enterWall;
    public GameObject exitWall;
    [Header("出入門設定")]
    public DoorAnimation enterDoor;
    public List<ExitDoor> exitDoors;
    [Header("任務設定")]
    public QuestBoard questBoard;
    public NpcDialogue npcDialogue;
    public TeachBoxBoard teachBoxBoard;
    public GemSmoothTeachBox gemTeachBox;
    [Header("敵人類設定")]
    public List<GameObject> trapList;
    public List<Transform> enemySpawnAreas;
}