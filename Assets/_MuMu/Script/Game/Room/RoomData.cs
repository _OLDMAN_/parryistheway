using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newRoomData", menuName = "NewRoomData")]
public class RoomData : ScriptableObject
{
    public string roomName
    {
        get { return roomPrefab.name; }
    }
    public GameObject roomPrefab;
    public WaveData wave;
}
