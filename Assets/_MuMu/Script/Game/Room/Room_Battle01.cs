using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room_Battle01 : RoomBase
{
    public override void OnPlayerWalkInEndSet()
    {
        base.OnPlayerWalkInEndSet();
        TeachManager.Instance.SetPlayerComboHint(true);
    }
}
