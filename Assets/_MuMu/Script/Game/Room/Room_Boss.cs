using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room_Boss : RoomBase
{
    public BossManagerF bossManagerF;
    public BossCtrlF bossCtrl;
    public static bool startPlay = false;

    public override void Awake()
    {
        base.Awake();
        FindUICamera.Instance.Find();
        BlackManager.Instance.SetBlackPanel();
    }
    private void Start()
    {
        if(startPlay == true)
        {
            RoomManager.Instance.FindCurrentRoom();
            OnRoomEnter.Invoke();
            startPlay = false;
        }
    }
    public override void OnRoomEnterSet()
    {
        OnRoomEnter += WaveManager.Instance.ClearEnemy;
        OnRoomEnter += SetPlayerStatus;
        OnRoomEnter += OpenEneterDoor;
        OnRoomEnter += BlackScreenFadeIn;
        OnRoomEnter += PlayerWalkInDelay;
        OnRoomEnter += BlackManager.Instance.CloseInkPanel;
    }

    public override void OnPlayerWalkInEndSet()
    {
        OnPlayerWalkInEnd += CloseEnterDoor;
        OnPlayerWalkInEnd += bossCtrl.StartAction;
    }

    public override void OnRoomClearSet()
    {
        
    }
    
    private void SetPlayerStatus()
    {
        PlayerController.Instance.canCtrl = false;
        PlayerController.Instance.SetPos(roomSetting.startPos.position);
    }

    private void PlayerWalkInDelay()
    {
        StartCoroutine(DoPlayerWalkIn());
    }

    private IEnumerator DoPlayerWalkIn()
    {
        yield return new WaitForSeconds(1);
        CloseFocusCamera();
        PlayerAIMove.Instance.StartAIMove(roomSetting.startPos.position, GetPathPoints(roomSetting.endPos), true, () => OnPlayerWalkInEnd?.Invoke());

    }
    private void BlackScreenFadeIn()
    {
        BlackManager.Instance.BlackFadeIn(2f);
    }

    public override void OnRoomRestartSet()
    {
        OnRoomRestart += bossManagerF.RestartBossRoom;
    }
}
