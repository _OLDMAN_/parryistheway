using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Room_Tutorial05 : RoomBase
{
    public PlayableDirector sceneName;
    public override void OnRoomEnterSet()
    {
        base.OnRoomEnterSet();
        OnRoomEnter += sceneName.Play;
    }
}
