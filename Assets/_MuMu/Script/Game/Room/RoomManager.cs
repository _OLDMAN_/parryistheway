using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomManager : MonoBehaviourSingleton<RoomManager>
{
    public List<RoomData> rooms;
    public int roomNum;
    private List<GameObject> roomsObj = new List<GameObject>();
    public RoomData currentRoomData 
    { 
        get 
        {
            if (roomNum < rooms.Count)
                return rooms[roomNum];
            return null;
        } 
    }
    public RoomBase currentRoom;
    public RoomSetting currentRoomSetting
    {
        get { return currentRoom.roomSetting; }
    }
    public void FindCurrentRoom()
    {
        currentRoom = FindObjectOfType<RoomBase>();
    }
    public void RoomStart(bool autotRoomStart = true)
    {
        SetRoomData();
        RoomInitial();
        if (autotRoomStart == true)
            StartCoroutine(DelayStart());
    }
    private void SetRoomData()
    {
        rooms = LevelManager.Instance.currentLevelData.rooms;
        if (rooms.Count > 1)
            SpawnAllRoom();
    }
    private IEnumerator DelayStart()
    {
        yield return null;
        currentRoom.OnRoomEnter.Invoke();
    }
    private void SpawnAllRoom()
    {
        Transform roomsParent = new GameObject("RoomParent").transform;
        for (int i = 0; i < rooms.Count; i++)
        {
            GameObject room = Instantiate(rooms[i].roomPrefab, roomsParent);
            roomsObj.Add(room);
            room.SetActive(false);
        }
    }

    private void RoomInitial()
    {
        roomNum = 0;
        RoomSet();
    }

    private void RoomSet()
    {
        if (rooms.Count > 1)
        {
            roomsObj[roomNum].SetActive(true);
            currentRoom = roomsObj[roomNum].GetComponent<RoomBase>();
        }
        else
            currentRoom = FindObjectOfType<RoomBase>();
        GemManager.Instance.shop.GetComponent<Canvas>().worldCamera = GameObject.FindWithTag("UICamera").GetComponent<Camera>();

        if (currentRoom.roomSetting.questBoard == null) return;
        QuestManager.Instance.questBoard = currentRoom.roomSetting.questBoard;
        QuestManager.Instance.quests = currentRoom.roomSetting.questBoard.quests;
    }

    public void RoomClear()
    {
        currentRoom.OnRoomClear.Invoke();
    }

    public void EnterNextRoom()
    {
        if (roomsObj[roomNum] != null)
            roomsObj[roomNum].SetActive(false);
        roomNum++;
        if (roomNum < rooms.Count)
        {
            RoomSet();
            currentRoom.OnRoomEnter.Invoke();
        }
        else
            LevelManager.Instance.EnterNextLevel();
    }
    public void RestartCurrentRoom()
    {
        currentRoom.OnRoomRestart?.Invoke();
    }
}
