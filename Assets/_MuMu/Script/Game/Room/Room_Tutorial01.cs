using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;

public class Room_Tutorial01 : RoomBase
{
    public PlayableDirector sceneName;
    public override void Awake()
    {
        base.Awake();
        BlackManager.Instance.SetBlackPanel();
    }
    public override void OnRoomEnterSet()
    {
        OnRoomEnter += sceneName.Play;
        OnRoomEnter += OpenEneterDoor;
        OnRoomEnter += BlackScreenFadeIn;
        OnRoomEnter += PlayerWalkIn;
        OnRoomEnter += NavMeshBake.Instance.BakeNavMesh;
    }
    public override void OnPlayerWalkInEndSet()
    {
        base.OnPlayerWalkInEndSet();
        OnPlayerWalkInEnd += roomSetting.npcDialogue.StartDialogue;
    }

    private void BlackScreenFadeIn()
    {
        BlackManager.Instance.BlackFadeIn(1f);
    }
}
