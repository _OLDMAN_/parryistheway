using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newLevelData", menuName = "NewLevelData")]
public class LevelData : ScriptableObject
{
    public string sceneName;
    public List<RoomData> rooms;
}
