using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviourSingleton<LevelManager>
{
    private int levelNum = 0;
    public List<LevelData> levels;
    public LevelData currentLevelData;
    private void Start()
    {
        LevelInitial();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad8))
        {
            TeachManager.Instance.isTeach = false;
            TeachManager.Instance.unLockBlockAndBlockAttack = true;
            TeachManager.Instance.unLockDash = true;
            TeachManager.Instance.unLockAttackAndDashAttack = true;
            Time.timeScale = 1;
            EnterNextLevel();
        }
        else if (Input.GetKeyDown(KeyCode.Keypad9))
        {
            TeachManager.Instance.isTeach = false;
            TeachManager.Instance.unLockBlockAndBlockAttack = true;
            TeachManager.Instance.unLockDash = true;
            TeachManager.Instance.unLockAttackAndDashAttack = true;
            Time.timeScale = 1;
            levelNum = levels.Count - 2;
            EnterNextLevel();
        }
    }

    public void LevelInitial()
    {
        levelNum = 0;
        currentLevelData = levels[levelNum];
        if (currentLevelData.sceneName != "Teach")
            RoomManager.Instance.RoomStart();
        else
            RoomManager.Instance.RoomStart(false);
    }

    public void EnterNextLevel()
    {
        levelNum++;
        if (levelNum < levels.Count)
        {
            currentLevelData = levels[levelNum];
            StartCoroutine(StartLoadScene(currentLevelData.sceneName));
        }
    }

    private IEnumerator StartLoadScene(string sceneName)
    {
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        yield return new WaitUntil(() => asyncLoad.isDone == true);
        FindUICamera.Instance.Find();
        RoomManager.Instance.RoomStart();
    }
}
