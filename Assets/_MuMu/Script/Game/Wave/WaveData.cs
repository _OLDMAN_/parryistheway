using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct Wave
{
    public List<EnemyInfo> enemys;
}

[System.Serializable]
public struct EnemyInfo
{
    public GameObject enemyPrefab;
    public int count;
}

[CreateAssetMenu(fileName = "newWaveData", menuName = "NewWaveData")]
public class WaveData : ScriptableObject
{
    public List<Wave> waves;
}
