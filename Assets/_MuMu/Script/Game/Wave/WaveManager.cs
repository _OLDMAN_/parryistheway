using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviourSingleton<WaveManager>
{
    public List<WaveData> rank1;
    public List<WaveData> rank2;
    public List<WaveData> rank3;

    private WaveData currentWaveData;
    private RoomSetting currentRoomSetting;

    public Transform player;
    public Transform nearlyEnemy
    {
        get
        {
            GameObject nearestEnemy = null;
            foreach (GameObject c in enemy)
            {
                if (nearestEnemy != null)
                {
                    nearestEnemy = Vector3.Distance(c.transform.position, player.position) < Vector3.Distance(nearestEnemy.transform.position, player.position) ? c : nearestEnemy;
                }
                else
                {
                    nearestEnemy = c;
                }
            }

            if (nearestEnemy == null)
                return null;
            else
                return nearestEnemy.transform;
        }
    }
    public Vector3 nearlyPoint
    {
        get
        {
            Vector3 nearestPoint = Vector3.zero;
            foreach (Vector3 c in nearlyPoints)
            {
                if (nearestPoint != null)
                {
                    nearestPoint = Vector3.Distance(c, player.position) < Vector3.Distance(nearestPoint, player.position) ? c : nearestPoint;
                }
                else
                {
                    nearestPoint = c;
                }
            }
            return nearestPoint;
        }
    }
    public static List<GameObject> enemy = new List<GameObject>();
    public List<Vector3> nearlyPoints = new List<Vector3>();

    private bool enemyStart = false;
    private int waveNum;
    public int rank = 1;
    [HideInInspector] public bool clearAllWave;
    [HideInInspector] public bool isWaveing;

    [Header("怪物生成設定")]
    public float spawnSpace;
    public LayerMask spawnLayer;
    public GameObject spawnPrefab;
    public static Vector3 lastPos;

    private WaveData GetRandWaveData()
    {
        if(rank == 1)
        {
            int r = Random.Range(0, rank1.Count);
            return rank1[r];
        }
        else if(rank == 2)
        {
            int r = Random.Range(0, rank2.Count);
            return rank2[r];
        }
        else
        {
            int r = Random.Range(0, rank3.Count);
            return rank3[r];
        }
    }
    private void Start()
    {
        player = GameObject.FindWithTag("Player").transform;
    }
    private void Update()
    {
        if (clearAllWave == true) return;

        if (enemyStart == true && enemy.Count <= 0)
            WaveClear();
    }
    public void ClearEnemy()
    {
        StopAllCoroutines();
        enemy.Clear();
        nearlyPoints.Clear();
    }
    public void EndWave()
    {
        clearAllWave = true;
        foreach (GameObject e in enemy)
        {
            e.GetComponent<Melee01ActionManager>()?.Dead();
            e.GetComponent<Melee02ActionManager>()?.Dead();
            e.GetComponent<Range01ActionManager>()?.Dead();
            e.GetComponent<Range02ActionManager>()?.Dead();
        }
    }

    private void SetData()
    {
        currentWaveData = RoomManager.Instance.currentRoomData.wave;
        currentRoomSetting = RoomManager.Instance.currentRoomSetting;
        if (currentWaveData == null)
            currentWaveData = GetRandWaveData();
    }

    //波數開始
    public void WaveStart()
    {
        SetData();
        clearAllWave = false;
        isWaveing = true;
        waveNum = 0;
        SpawnWaveEnemys();
    }

    //每個波數勝利
    private void WaveClear()
    {
        if (currentWaveData == null) return;

        enemyStart = false;
        waveNum++;

        if (waveNum < currentWaveData.waves.Count)
            SpawnWaveEnemys();
        else
        {
            GamefeelManager.Instance.LastKill();
            clearAllWave = true;
            isWaveing = false;
        }
    }

    //波數生成敵人
    private void SpawnWaveEnemys()
    {
        enemy.Clear();
        nearlyPoints.Clear();
        int currentEnemyCount = 0;
        List<Vector3> spawnPos = new List<Vector3>();
        int spawnPosNum = 0;
        for (int i = 0; i < currentWaveData.waves[waveNum].enemys.Count; i++)
            currentEnemyCount += currentWaveData.waves[waveNum].enemys[i].count;

        List<Transform> randTransform = currentRoomSetting.enemySpawnAreas.GetRandom(currentEnemyCount);
        foreach(Transform t in randTransform)
            spawnPos.Add(t.position);

        for (int i = 0; i < currentWaveData.waves[waveNum].enemys.Count; i++)  //幾種怪
        {
            for (int k = 0; k < currentWaveData.waves[waveNum].enemys[i].count; k++)  //生成怪的數量
            {
                StartCoroutine(SpawnParticleAndEnemy(currentWaveData.waves[waveNum].enemys[i].enemyPrefab, spawnPos[spawnPosNum]));
                spawnPosNum++;
            }
        }
    }

    private IEnumerator SpawnParticleAndEnemy(GameObject enemyPrefab, Vector3 pos)
    {
        SoundManager.Instance.PlaySound("Summon");
        ParticleManager.Instance.GetParticle("SpawnWarning", pos + Vector3.up * 0.1f, Quaternion.identity);
        nearlyPoints.Add(pos);
        yield return new WaitForSeconds(2);

        GameObject o = Instantiate(enemyPrefab, pos, Quaternion.identity);
        enemy.Add(o);
        enemyStart = true;
    }
}
