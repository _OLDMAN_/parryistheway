using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public class QuestManager : MonoBehaviourSingleton<QuestManager>
{
    private int questNum = 0;
    public List<Quest> quests = new List<Quest>();
    public Quest currentQuest;

    public QuestBoard questBoard;

    public Action OnQuestStart;
    public Action OnQuestComplete;

    private void LateUpdate()
    {
        if (currentQuest != null)
            quests[questNum].QuestUpdata();
    }

    public void QuestStart()
    {
        if (questBoard == null) return;
        questNum = 0;
        currentQuest = quests[questNum];
        currentQuest.QuestStart();
        questBoard.InitialQuest(currentQuest);
        questBoard.QuestBoardSlideIn();
        //OnQuestStart?.Invoke();
    }

    public void NextQuest()
    {
        questNum++;
        if (questNum < quests.Count)
        {
            currentQuest = quests[questNum];
            currentQuest.QuestStart();
        }
        else
        {
            RoomQuestComplete();
        }
        questBoard.AnimateNextQuest(currentQuest);
    }

    public void RoomQuestComplete()
    {
        currentQuest = null;

        RoomManager.Instance.RoomClear();
    }

    public void QuestBoardHide()
    {
        questBoard.QuestBoardSlideOut();
    }

    public void QuestBoardDisplay()
    {
        questBoard.QuestBoardSlideIn();
    }
    
}
