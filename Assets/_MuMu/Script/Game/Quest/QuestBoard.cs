using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;
using UnityEditor;

public class QuestBoard : MonoBehaviour
{
    int index = 0;
    public Canvas canvas;
    private RectTransform rect;
    public Transform board;
    public Image textBG;
    private Material textBGMaterial;
    private float materialCtrl = 0;
    Quest currentQuest;

    public List<TextMeshProUGUI> textSlot = new List<TextMeshProUGUI>();

    [Header("QuestAnimation")]

    public float questCompleteSec;
    public float questshiftSec;
    public float questFadeInSec;
    public float fadeDistance;
    public float textHighlightSec;
    public List<Quest> quests = new List<Quest>();

    private void Start()
    {
        rect = GetComponent<RectTransform>();
        textBGMaterial = textBG.material;
        textBGMaterial.SetFloat("_Ctrl", 0);
        SetUICamera();
    }
    public void InitialQuest(Quest quest)
    {
        StartCoroutine(DoInitialQuest(quest));
    }
    IEnumerator DoInitialQuest(Quest quest)
    {
        currentQuest = quest;
        yield return new  WaitForSeconds(questFadeInSec);
        StartCoroutine(DoTweenQuestTextAppear());
    }
    private void SetUICamera()
    {
        canvas.worldCamera = GameObject.FindGameObjectWithTag("UICamera").GetComponent<Camera>();
    }
    public void AnimateNextQuest(Quest quest)
    {
        StartCoroutine(DoAnimateNextQuest(quest));
    }

    IEnumerator DoAnimateNextQuest(Quest quest)
    {
        currentQuest = quest;
        textSlot[index].DOFade(0.15f, questCompleteSec);
        textBGMaterial.SetFloat("_Ctrl", 0);
        

        yield return new WaitForSeconds(questCompleteSec);
        if (index + 1 >= quests.Count) 
        {
            yield break; 
        }

        textSlot[index].rectTransform.DOAnchorPosY(textSlot[index].rectTransform.position.y + fadeDistance, questshiftSec);
        if(index > 0)
        {
            textSlot[index-1].DOFade(0f, 0.2f);
        }

        index++;
        yield return new WaitForSeconds(questshiftSec);

        StartCoroutine(DoTweenQuestTextAppear());
    }
    IEnumerator DoTweenQuestTextAppear()
    {
        textSlot[index].gameObject.SetActive(true);
        textSlot[index].text = currentQuest.questContext;
        textSlot[index].rectTransform.position = new Vector3(250, textSlot[index].rectTransform.position.y, textSlot[index].rectTransform.position.z);
        textSlot[index].rectTransform.DOAnchorPosX(20, textHighlightSec).SetEase(Ease.Flash);

        materialCtrl = 0;
        DOTween.To(() => materialCtrl, x => materialCtrl = x, 1, textHighlightSec);

        SoundManager.Instance.PlaySound("Quest");
        while (materialCtrl != 1)
        {
            textBGMaterial.SetFloat("_Ctrl", materialCtrl);
            yield return null;
        }
        textBGMaterial.SetFloat("_Ctrl", 1);
    }

    public void QuestBoardSlideIn()
    {
        StartCoroutine(DoQuestBoardSlideIn());
    }
    private IEnumerator DoQuestBoardSlideIn()
    {
        rect.anchoredPosition = new Vector2( Camera.main.pixelWidth * 1.3f, rect.anchoredPosition.y);
        rect.DOAnchorPosX(-250, 0.5f);
        yield return new WaitForSeconds(0.5f);

    }
    public void QuestBoardSlideOut()
    {
        rect.DOAnchorPosX(Camera.main.pixelWidth * 1.3f, 0.5f);
    }
}
