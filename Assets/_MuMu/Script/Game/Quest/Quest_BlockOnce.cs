using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest_BlockOnce : Quest
{
    public override void QuestUpdata()
    {
        if (WaveManager.Instance.isWaveing == false)
            WaveManager.Instance.WaveStart();
    }
}
