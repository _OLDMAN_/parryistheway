using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest_KillCurrentEnemys : Quest
{
    public override void QuestStart()
    {
        base.QuestStart();
        questIndicator.gameObject.SetActive(true);
    }

    public override void QuestUpdata()
    {
        base.QuestUpdata();

        if (WaveManager.Instance.nearlyEnemy != null)
        {
            Quaternion dir = Quaternion.LookRotation(WaveManager.Instance.nearlyEnemy.position.GetZeroY() - questIndicator.position.GetZeroY());
            questIndicator.rotation = Quaternion.Lerp(questIndicator.rotation, dir, Time.deltaTime * questRotateSpeed);
        }
        else if (WaveManager.Instance.nearlyPoint != Vector3.zero)
        {
            Quaternion dir = Quaternion.LookRotation(WaveManager.Instance.nearlyPoint.GetZeroY() - questIndicator.position.GetZeroY());
            questIndicator.rotation = Quaternion.Lerp(questIndicator.rotation, dir, Time.deltaTime * questRotateSpeed);
        }

        if (WaveManager.Instance.clearAllWave == true)
        {
            QuestComplete();
        }
    }

    public override void QuestComplete()
    {
        questIndicator.gameObject.SetActive(false);
        base.QuestComplete();
    }
}
