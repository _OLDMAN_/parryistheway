using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest_PickUpGem : Quest
{
    private Transform gemTransform;
    public override void QuestStart()
    {
        base.QuestStart();
        gemTransform = GemManager.Instance.SpawnGem(WaveManager.lastPos).transform;
        questIndicator.gameObject.SetActive(true);
    }

    public override void QuestUpdata()
    {
        base.QuestUpdata();
        if(gemTransform != null)
        {
            Quaternion dir = Quaternion.LookRotation(gemTransform.position.GetZeroY() - questIndicator.position.GetZeroY());
            questIndicator.rotation = Quaternion.Lerp(questIndicator.rotation, dir, Time.deltaTime * questRotateSpeed);
        }
    }

    public override void QuestComplete()
    {
        questIndicator.gameObject.SetActive(false);
        base.QuestComplete();
    }
}
