using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest_PickUpItem : Quest
{
    public Transform item;

    public override void QuestStart()
    {
        base.QuestStart();
        questIndicator.gameObject.SetActive(true);
    }

    public override void QuestUpdata()
    {
        base.QuestUpdata();
        Quaternion dir = Quaternion.LookRotation(item.position.GetZeroY() - questIndicator.position.GetZeroY());
        questIndicator.rotation = Quaternion.Lerp(questIndicator.rotation, dir, Time.deltaTime * questRotateSpeed);
    }

    public override void QuestComplete()
    {
        questIndicator.gameObject.SetActive(false);
        base.QuestComplete();
    }
}
