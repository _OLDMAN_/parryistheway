using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest_ReachSomewhere : Quest
{
    private bool used = false;

    public override void QuestStart()
    {
        base.QuestStart();
        questIndicator.gameObject.SetActive(true);
    }

    public override void QuestUpdata()
    {
        base.QuestUpdata();
        if (questIndicator == null) return;
        Quaternion dir = Quaternion.LookRotation(transform.position.GetZeroY() - questIndicator.position.GetZeroY());
        questIndicator.rotation = Quaternion.Lerp(questIndicator.rotation, dir, Time.deltaTime * questRotateSpeed);
    }

    public override void QuestComplete()
    {
        questIndicator.gameObject.SetActive(false);
        base.QuestComplete();
    }

    private void OnTriggerEnter(Collider other)
    {
        if(isActive == false) return;

        if(other.tag == "Player" && used == false)
        {
            used = true;
            QuestComplete();
        }
    }
}
