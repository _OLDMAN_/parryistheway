using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest_WatchStatue : Quest
{
    public StatueWatch statue;

    public override void QuestStart()
    {
        base.QuestStart();
        questIndicator.gameObject.SetActive(true);
        statue.UnlockInteract();
    }
    public override void QuestUpdata()
    {
        base.QuestUpdata();
        Quaternion dir = Quaternion.LookRotation(statue.transform.position.GetZeroY() - questIndicator.position.GetZeroY());
        questIndicator.rotation = Quaternion.Lerp(questIndicator.rotation, dir, Time.deltaTime * questRotateSpeed);
        if (statue.isTriggerOnce)
        {
            QuestComplete();
        }
    }

    public override void QuestComplete()
    {
        questIndicator.gameObject.SetActive(false);
        base.QuestComplete();
    }
}
