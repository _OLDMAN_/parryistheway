using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest : MonoBehaviour
{
    protected float questRotateSpeed = 5;
    protected Transform questIndicator;
    public string questContext;
    protected bool isActive;

    private void Start()
    {
        questIndicator = GameObject.FindWithTag("QuestIndicator").transform.GetChild(0);
    }

    public virtual void QuestStart()
    {
        isActive = true;
    }

    public virtual void QuestUpdata()
    {
        if (isActive == false) return;
    }

    public virtual void QuestComplete()
    {
        if (isActive == false) return;
        QuestManager.Instance.NextQuest();
        isActive = false;
    }
}
