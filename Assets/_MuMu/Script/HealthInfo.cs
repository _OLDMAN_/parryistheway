using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthInfo : MonoBehaviour
{
    public float followSpeed = 2;
    public GameObject canvas;
    public GameObject health;
    public Image healthImage;
    public Image followImage;
    public GameObject damageTextPrefab;

    private void Update()
    {
        if (followImage.fillAmount > healthImage.fillAmount)
            followImage.fillAmount -= Time.deltaTime * followSpeed;
    }
    private void LateUpdate()
    {
        transform.rotation = Quaternion.Euler(45, -45, 0);
    }

}
