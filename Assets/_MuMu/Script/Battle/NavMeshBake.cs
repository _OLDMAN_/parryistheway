using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavMeshBake : MonoBehaviourSingleton<NavMeshBake>
{
    public NavMeshSurface surface;

    public void BakeNavMesh()
    {
         surface.BuildNavMesh();
    }
}
