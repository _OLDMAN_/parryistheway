using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VinePoison : MonoBehaviour
{
    public float burnNeedSec = 0.5f;
    public float burnDuringSec = 3f;
    public float burnSpaceSec = 1f;
    public int burnDamage = 5;

    private float burnTimer;
    private float burnSpaceTimer;
    private bool canDamage;

    private void OnEnable()
    {
        canDamage = true;
        StartCoroutine(CanDamageTime());
    }

    private IEnumerator CanDamageTime()
    {
        yield return new WaitForSeconds(burnDuringSec);
        canDamage = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player") || canDamage == false) return;

        burnTimer = burnNeedSec;
        burnSpaceTimer = burnSpaceSec;
    }

    private void OnTriggerStay(Collider other)
    {
        if (!other.CompareTag("Player") || canDamage == false) return;

        if (burnTimer <= 0)
        {
            if (burnSpaceTimer <= 0)
            {
                other.GetComponent<Hurt>().Dodeal(burnDamage, transform, false, false);

                burnSpaceTimer = burnSpaceSec;
            }
            else
            {
                burnSpaceTimer -= Time.deltaTime;
            }
        }
        else
        {
            burnTimer -= Time.deltaTime;
        }
    }
}
