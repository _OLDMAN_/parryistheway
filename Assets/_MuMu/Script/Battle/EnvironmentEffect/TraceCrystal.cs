using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class TraceCrystal : MonoBehaviour
{
    public int damage = 20;
    public float radius = 3;

    public NavMeshAgent agent;
    private bool isUsed;

    private void OnEnable()
    {
        isUsed = false;
        agent.isStopped = false;
    }

    void Update()
    {
        if (agent.enabled == true)
            agent.SetDestination(PlayerController.Instance.transform.position);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player") && isUsed == false)
        {
            agent.isStopped = true;
            StartCoroutine(StartBomb());
            isUsed = true;
        }
    }

    private IEnumerator StartBomb()
    {
        GameObject g = ParticleManager.Instance.GetParticle("BombPreview", transform.position, Quaternion.identity);
        g.transform.localScale = g.transform.localScale * radius;
        GetComponent<ObjectShake>().Shake();
        yield return new WaitForSeconds(1);
        GameObject bomb = ParticleManager.Instance.GetParticle("TraceCrystalBomb", transform.position, Quaternion.identity);
        bomb.transform.localScale = Vector3.one * 2;
        if (Vector3.Distance(transform.position, PlayerController.Instance.transform.position) < radius)
            PlayerActionManager.Instance.StartHurt(damage, transform, true);
        Destroy(gameObject);
    }
    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
