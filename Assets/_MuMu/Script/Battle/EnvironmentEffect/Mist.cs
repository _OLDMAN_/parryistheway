using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mist : MonoBehaviour
{
    public float moveSpeed = 0.1f;
    private Vector3 moveDir;

    private void OnEnable()
    {
        Vector2 randVector2 = Random.insideUnitCircle;
        moveDir = new Vector3(randVector2.x, 0, randVector2.y);
    }

    void Update()
    {
        transform.position += moveDir * Time.deltaTime;
    }
}
