using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Vine : MonoBehaviour
{
    public float cd;
    private bool isCD;

    private void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag("Player") && isCD == false)
        {
            ParticleManager.Instance.PlayParticle("VinePoison", transform.position, Quaternion.identity);
            StartCoroutine(StartCD());
            isCD = true;
        }
    }

    private IEnumerator StartCD()
    {
        yield return new WaitForSeconds(cd);
        isCD = false;
    }
}
