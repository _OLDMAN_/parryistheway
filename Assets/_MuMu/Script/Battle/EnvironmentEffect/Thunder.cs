using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Thunder : MonoBehaviour
{
    public float damageFrameSec = 3f;
    public float radius;
    public int damage;

    private void OnEnable()
    {
        StartCoroutine(Damage());
    }

    private IEnumerator Damage()
    {
        yield return new WaitForSeconds(damageFrameSec);
        Collider[] colliders = Physics.OverlapSphere(transform.position, radius, LayerMask.GetMask("Player"));
        foreach (Collider collider in colliders)
            collider.GetComponent<PlayerActionManager>().StartHurt(damage, transform, true);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, radius);
    }
}
