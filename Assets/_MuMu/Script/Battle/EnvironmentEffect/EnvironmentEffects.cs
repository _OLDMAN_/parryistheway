using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EnvironmentEffectType { 無, 落雷, 迷霧, 藤蔓, 追蹤晶體 }

[System.Serializable]
public class EnvironmentWeight
{
    public int weight;
    public EnvironmentEffectType type;
}

public class EnvironmentEffects : MonoBehaviour
{
    public List<EnvironmentWeight> effectsWeight;
    public Transform thunderRoomEffectPos;
    public Transform vineRoomEffectPos;
    public List<Transform> thunderArea;
    public List<Transform> mistArea;
    public List<Transform> traceCrystalArea;
    public GameObject vineObj;


    private void Update()
    {

        //if(Input.GetKeyDown(KeyCode.Alpha1))
        //    EnvironmentEffectManager.Instance.StartEffect(this, EnvironmentEffectType.落雷);
        //else if(Input.GetKeyDown(KeyCode.Alpha2))
        //    EnvironmentEffectManager.Instance.StartEffect(this, EnvironmentEffectType.藤蔓);
        //else if (Input.GetKeyDown(KeyCode.Alpha3))
        //    EnvironmentEffectManager.Instance.StartEffect(this, EnvironmentEffectType.迷霧);
        //else if (Input.GetKeyDown(KeyCode.Alpha4))
        //    EnvironmentEffectManager.Instance.StartEffect(this, EnvironmentEffectType.追蹤晶體);
    }

    //回傳環境效果
    public EnvironmentEffectType RandomEnvironmentEffectAndAction()
    {
        int total = 0;
        foreach (EnvironmentWeight effect in effectsWeight)
            total += effect.weight;

        int randNum = Random.Range(1, total + 1);
        int count = 0;

        foreach (EnvironmentWeight effect in effectsWeight)
        {
            count += effect.weight;
            if (randNum <= count)
                return effect.type;
        }

        return EnvironmentEffectType.無;
    }
}
