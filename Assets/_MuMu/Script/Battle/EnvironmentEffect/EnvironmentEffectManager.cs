﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentEffectManager : MonoBehaviourSingleton<EnvironmentEffectManager>
{
    [Header("落雷")]
    public Vector2 thunderCD;

    [Header("迷霧")]
    public Vector2 mistCD;

    [Header("追蹤晶體")]
    public float spawnSec = 10f;

    private bool effectStart;

    //開始場地效果
    public void StartEffect(EnvironmentEffects setting, EnvironmentEffectType effect)
    {
        if (effect == EnvironmentEffectType.無) return;

        effectStart = true;
        if (effect == EnvironmentEffectType.落雷)
            StartThunder(setting.thunderArea, setting.thunderRoomEffectPos.position);
        else if (effect == EnvironmentEffectType.迷霧)
            StartMist(setting.mistArea);
        else if (effect == EnvironmentEffectType.藤蔓)
            StartVine(setting.vineObj, setting.vineRoomEffectPos.position);
        else if (effect == EnvironmentEffectType.追蹤晶體)
            StartTraceCrystal(setting.traceCrystalArea);
    }

    //停止場地效果
    public void StopEffect()
    {
        effectStart = false;
        StopAllCoroutines();
    }

    //落雷
    private void StartThunder(List<Transform> area, Vector3 thunderRoomEffectPos)
    {
        StartCoroutine(DoThunder(area, thunderRoomEffectPos));
    }

    private IEnumerator DoThunder(List<Transform> area, Vector3 thunderRoomEffectPos)
    {
        ParticleManager.Instance.PlayParticle("ThunderRoom", thunderRoomEffectPos, Quaternion.identity);
        while (effectStart == true)
        {
            int randNum = Random.Range(0, area.Count);
            float randX = Random.Range(-area[randNum].localScale.x / 2, area[randNum].localScale.x / 2);
            float randZ = Random.Range(-area[randNum].localScale.z / 2, area[randNum].localScale.z / 2);
            Vector3 pos = area[randNum].position + new Vector3(randX, 0, randZ);
            ParticleManager.Instance.PlayParticle("Thunder", pos, Quaternion.identity);

            float randCD = Random.Range(thunderCD.x, thunderCD.y);
            yield return new WaitForSeconds(randCD);
        }
    }

    //迷霧
    private void StartMist(List<Transform> area)
    {
        StartCoroutine(SpawnMist(area));
    }

    private IEnumerator SpawnMist(List<Transform> area)
    {
        while (effectStart == true)
        {
            int randNum = Random.Range(0, area.Count);
            float randX = Random.Range(-area[randNum].localScale.x / 2, area[randNum].localScale.x / 2);
            float randZ = Random.Range(-area[randNum].localScale.z / 2, area[randNum].localScale.z / 2);
            Vector3 pos = area[randNum].position + new Vector3(randX, 0, randZ);
            ParticleManager.Instance.PlayParticle("Mist", pos + Vector3.up * 2, Quaternion.identity);

            float randCD = Random.Range(mistCD.x, mistCD.y);
            yield return new WaitForSeconds(randCD);
        }
    }


    //藤蔓
    private void StartVine(GameObject obj, Vector3 vineRoomEffectPos)
    {
        ParticleManager.Instance.PlayParticle("ThunderRoom", vineRoomEffectPos, Quaternion.identity);
        obj.SetActive(true);
    }

    //追蹤晶體
    private void StartTraceCrystal(List<Transform> area)
    {
        StartCoroutine(SpawnTraceCrystal(area));
    }

    private IEnumerator SpawnTraceCrystal(List<Transform> area)
    {
        while (effectStart == true)
        {
            int randNum = Random.Range(0, area.Count);
            float randX = Random.Range(-area[randNum].localScale.x / 2, area[randNum].localScale.x / 2);
            float randZ = Random.Range(-area[randNum].localScale.z / 2, area[randNum].localScale.z / 2);
            Vector3 pos = area[randNum].position + new Vector3(randX, 0, randZ);
            ParticleManager.Instance.PlayParticle("TraceCrystal", pos, Quaternion.identity);

            yield return new WaitForSeconds(spawnSec);
        }
    }
}
