using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AlphaObject : MonoBehaviour
{
    public float alpha = 0.3f;
    public LayerMask mask;
    private RaycastHit lastHit;

    private const string ALPHA = "_Alpha";
    void Update()
    {
        RaycastHit hit;
        float distance = Vector3.Distance(transform.position, Camera.main.transform.position);
        Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, distance, mask);
        if (hit.collider != null && hit.collider != lastHit.collider)
        {
            var m = hit.collider.GetComponent<Renderer>().material;
            if(m.HasProperty(ALPHA))
                m.DOFloat(alpha, ALPHA, 0.2f);
        }
        if (lastHit.collider != null)
        {
            if (lastHit.collider != hit.collider)
            {
                var m = lastHit.collider.GetComponent<Renderer>().material;
                if (m.HasProperty(ALPHA))
                    m.DOFloat(1, ALPHA, 0.2f);
                lastHit = hit;
            }
        }
        if (lastHit.collider == null)
            lastHit = hit;
    }
}
