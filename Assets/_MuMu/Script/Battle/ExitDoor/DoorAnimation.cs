using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class DoorAnimation : MonoBehaviour
{
    public ExitDoor exitDoor;
    public GameObject doorObj;
    public ParticleSystem rockParticle;
    public ParticleSystem winParticle;
    private Renderer[] branches;

    [Header("�Ѽ�")]
    public float downSec = 0.7f;
    public AnimationCurve downCurve;

    private const string BRANCHES_ALPHA = "_WidthOffset";


    private void Awake()
    {
        branches = GetComponentsInChildren<Renderer>();
    }

    public void CloseDoor()
    {
        SoundManager.Instance.PlaySound("DoorMove");
        rockParticle.Play();
        GetComponent<BoxCollider>().enabled = true;
        doorObj.transform.DOLocalMoveY(0, 1.5f).From(-4).OnComplete(() => {
            foreach (Renderer r in branches)
                if (r.material.HasProperty(BRANCHES_ALPHA))
                    r.material.DOFloat(1, BRANCHES_ALPHA, 0.7f);
        });
    }
    public virtual void OpenDoor()
    {
        foreach (Renderer r in branches)
            if (r.material.HasProperty(BRANCHES_ALPHA))
                r.material.DOFloat(0, BRANCHES_ALPHA, 0.7f);
        Invoke("Open", 0.7f);
    }

    public void ResetDoor()
    {
        foreach (Renderer r in branches)
            if (r.material.HasProperty(BRANCHES_ALPHA))
                r.material.SetFloat(BRANCHES_ALPHA, 0);
        doorObj.transform.localPosition = doorObj.transform.localPosition.GetZeroY(-4);
        GetComponent<BoxCollider>().enabled = false;
    }


    private void Open()
    {
        SoundManager.Instance.PlaySound("DoorMove");
        rockParticle.Play();
        doorObj.transform.DOLocalMoveY(-4, downSec).SetEase(downCurve).From(0).OnComplete(() => {
            GetComponent<BoxCollider>().enabled = false;
            exitDoor.opened = true;
        });
    }
}
