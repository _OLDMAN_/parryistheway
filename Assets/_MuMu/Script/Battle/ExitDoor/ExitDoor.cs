using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitDoor : MonoBehaviour
{
    public Transform startPos;
    public Transform endPos;

    public DoorAnimation doorAnimation;

    public bool opened;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad7))
            RoomManager.Instance.EnterNextRoom();
    }

    public void CloseDoor()
    {
        doorAnimation.CloseDoor();
    }
    public virtual void OpenDoor()
    {
        doorAnimation.OpenDoor();
    }

    public virtual void EnterNextRoom()
    {
        //QuestManager.Instance.RoomQuestQuit();
        PlayerAIMove.Instance.StartAIMove(startPos.position, false, () => {
            PlayerAIMove.Instance.StartAIMove(endPos.position, false, ()=>{
                RoomManager.Instance.EnterNextRoom();
            });
        });
        BlackManager.Instance.InkBlackFadeOut(0.7f);
    }

    private void OnTriggerStay(Collider other)
    {
        if (opened == true && other.CompareTag("Player"))
        {
            EnterNextRoom();
            opened = false;
        }
    }
}
