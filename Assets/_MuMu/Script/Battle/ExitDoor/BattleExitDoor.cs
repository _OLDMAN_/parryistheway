using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BattleExitDoor : ExitDoor
{
    public GameObject canvas;
    public Image showUI;
    public Material[] materials;
    private int randRank;

    private void Start()
    {
        randRank = Random.Range(0, 3);
        if(LevelManager.Instance.currentLevelData.sceneName == "CloudTop")
            showUI.material = materials[3];
        else
            showUI.material = materials[randRank];
    }

    public override void EnterNextRoom()
    {
        WaveManager.Instance.rank = randRank;
        base.EnterNextRoom();
    }

    public override void OpenDoor()
    {
        base.OpenDoor();
        canvas.SetActive(true);
    }
}
