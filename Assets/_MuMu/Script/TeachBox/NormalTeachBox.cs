using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class NormalTeachBox : MonoBehaviour
{
    public void SlideLeftIn()
    {
        transform.DOMoveX(Camera.main.pixelWidth - 200, 0.5f).From(Camera.main.pixelWidth + 300);
    }

    public void SlideRightOut()
    {
        transform.DOMoveX(Camera.main.pixelWidth + 300, 0.5f);
    }
}
