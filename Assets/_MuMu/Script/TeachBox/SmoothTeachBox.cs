using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using UnityEngine.EventSystems;

[System.Serializable]
public class MainTeachBox
{
    public GameObject obj;
    public List<RectTransform> moveTarget;
    public Image blackPanel;
    public Image arrow;
    public TMP_Text teachText;
}

public class SmoothTeachBox : MonoBehaviour
{
    public MainTeachBox mainTeachBox;
    public List<RectTransform> teachBoxList;
    protected int teachBoxNum = 0;
    protected bool isStart;

    private void Update()
    {
        //if (Input.GetKeyDown(KeyCode.B))
        //    StartGemTeach();
        if ((PlayerController.Instance.defaultInputAction.Player.Attack.triggered || PlayerController.Instance.defaultInputAction.UI.Submit.triggered) && isStart == true)
            MoveToNextTeachBox();
    }

    //開始寶石教學
    public void StartGemTeach()
    {
        isStart = true;
        teachBoxNum = 0;
        mainTeachBox.obj.SetActive(true);
        AllFadeIn();
    }

    //全部淡入
    private void AllFadeIn()
    {
        float time = 0.2f;
        mainTeachBox.blackPanel.DOFade(0.65f, time).From(0);
        mainTeachBox.arrow.DOFade(1, time).From(0);
        mainTeachBox.teachText.DOFade(1, time).From(0);
        foreach (RectTransform r in mainTeachBox.moveTarget)
            r.GetComponent<Image>().DOFade(1, time).From(0);
        mainTeachBox.arrow.DOFade(1, time).From(0);
    }

    //全部淡出
    public virtual void AllFadeOut()
    {
        float time = 0.2f;
        mainTeachBox.blackPanel.DOFade(0, time);
        mainTeachBox.arrow.DOFade(0, time);
        mainTeachBox.teachText.DOFade(0, time);
        foreach (RectTransform r in mainTeachBox.moveTarget)
            r.GetComponent<Image>().DOFade(0, time);
        mainTeachBox.arrow.DOFade(0, time).OnComplete(() => { FadeOutEnd(); });
    }

    //淡出結束
    public virtual void FadeOutEnd()
    {
        mainTeachBox.obj.SetActive(false);
    }

    //移動到下一個說明
    public virtual void MoveToNextTeachBox()
    {
        for (int i = 0; i < mainTeachBox.moveTarget.Count; i++)
        {
            mainTeachBox.moveTarget[i].DOMove(teachBoxList[teachBoxNum].GetChild(i).position, 0.3f);
            mainTeachBox.moveTarget[i].DOSizeDelta(teachBoxList[teachBoxNum].GetChild(i).GetComponent<RectTransform>().sizeDelta, 0.3f);
        }
        mainTeachBox.teachText.DOFade(0, 0.1f).OnComplete(() =>
        {
            mainTeachBox.teachText.transform.position = teachBoxList[teachBoxNum].GetChild(3).position;
            mainTeachBox.teachText.text = teachBoxList[teachBoxNum].GetChild(3).GetComponent<TMP_Text>().text;
            mainTeachBox.teachText.DOFade(1, 0.1f);
        });
        mainTeachBox.arrow.DOFade(0, 0.1f).OnComplete(() =>
        {
            mainTeachBox.arrow.transform.position = teachBoxList[teachBoxNum].GetChild(2).position;
            mainTeachBox.arrow.DOFade(1, 0.1f);
        });
    }
}