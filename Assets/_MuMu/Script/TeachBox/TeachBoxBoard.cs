using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeachBoxBoard : MonoBehaviour
{
    private int teachNum = 0;
    public List<NormalTeachBox> normalTeachBoxes;

    public void StartTeach()
    {
        teachNum = 0;
        normalTeachBoxes[teachNum].SlideLeftIn();
    }

    public void NextTeach()
    {
        normalTeachBoxes[teachNum].SlideRightOut();
        teachNum++;
        if(teachNum < normalTeachBoxes.Count)
            normalTeachBoxes[teachNum].SlideLeftIn();
    }

    public void HideAllTeach()
    {
        foreach (NormalTeachBox n in normalTeachBoxes)
            n.SlideRightOut();
    }

    public void ShowAllTeach()
    {
        foreach (NormalTeachBox n in normalTeachBoxes)
            n.SlideLeftIn();
    }
}
