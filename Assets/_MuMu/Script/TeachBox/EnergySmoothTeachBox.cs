using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using TMPro;

public class EnergySmoothTeachBox : SmoothTeachBox
{
    public override void FadeOutEnd()
    {
        base.FadeOutEnd();
        //TeachManager.Instance.UnLockAllBaseOperation();
        PlayerController.Instance.canCtrl = true;
    }

    public override void MoveToNextTeachBox()
    {
        teachBoxNum++;
        if (teachBoxNum >= teachBoxList.Count)
        {
            isStart = false;
            AllFadeOut();
            return;
        }
        base.MoveToNextTeachBox();
    }
}
