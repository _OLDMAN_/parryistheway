using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class GemSmoothTeachBox : SmoothTeachBox
{
    public List<GameObject> panelMask;

    public override void MoveToNextTeachBox()
    {
        teachBoxNum++;
        base.MoveToNextTeachBox();
        foreach (GameObject o in panelMask)
            o.SetActive(true);
        if (teachBoxNum >= teachBoxList.Count - 1)
        {
            isStart = false;
            //EventSystem.current.SetSelectedGameObject(GemManager.Instance.EnterShopSelectedCard);
        }
    }
    public override void AllFadeOut()
    {
        base.AllFadeOut();
        foreach (GameObject o in panelMask)
            o.SetActive(false);
    }
}