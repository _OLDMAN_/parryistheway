using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class DeviceHintSwitch : MonoBehaviour
{
    public List<HintDecoration> hintDecorations = new List<HintDecoration>();

    private void OnEnable()
    {
        RefreshHintObj();
        InputSystem.onDeviceChange += RefreshHintObj;
    }

    private void OnDisable()
    {
        InputSystem.onDeviceChange -= RefreshHintObj;
    }


    public void RefreshHintObj()
    {
        string deviceName;
        if (Gamepad.current != null)
        {
            deviceName = Gamepad.current.displayName;
        }
        else
        {
            deviceName = "Mouse";
        }

        foreach (var i in hintDecorations)
        {
            if (i.name == deviceName)
            {
                i.hintObj.SetActive(true);
            }
            else
            {
                i.hintObj.SetActive(false);
            }
        }
    }
    public void RefreshHintObj(InputDevice d, InputDeviceChange c)
    {
        string deviceName;
        if(Gamepad.current != null)
        {
            deviceName = Gamepad.current.displayName;
            Debug.Log(deviceName);
        }
        else
        {
            deviceName = "Mouse";
        }

        foreach (var i in hintDecorations)
        {
            if (i.name == deviceName)
            {
                i.hintObj.SetActive(true);
            }
            else
            {
                i.hintObj.SetActive(false);
            }
        }
    }
}

[System.Serializable]
public class HintDecoration
{
    public string name;
    public GameObject hintObj;
}

