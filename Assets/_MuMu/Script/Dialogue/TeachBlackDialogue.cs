using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public class TeachBlackDialogue : BlackDialogue
{
    private void Start()
    {
        StartDialogue();
    }

    public override void EndDialogue()
    {
        base.EndDialogue();
        RoomManager.Instance.currentRoom.OnRoomEnter.Invoke();
    }
}
