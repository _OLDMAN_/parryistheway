using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;

public enum DialogueSentenceMode
{
    None, Type, OneSentence
}

public class NpcDialogue : MonoBehaviour
{
    public GameObject dialogueObj;
    public GameObject npcImage;
    public GameObject continueObj;
    public TMP_Text nameText;
    public TMP_Text dialogueText;

    public string npcName;
    public Sprite npcSprite;

    public DialogueSentenceMode sentenceMode;
    public float sentenceWaitTime = 0.3f;
    public float typeSec = 0.05f;
    public bool fadeInOnStart;
    public float fadeInOnStartSec = 0.1f;

    [TextArea(3, 10)]
    public string[] sentences;

    private bool isStart;
    private bool isEnd;
    private Queue<string> sentenceQueue;
    private string currentSentence;
    private bool isTypeing;
    private Coroutine typeCoroutine;
    private Tween fadeInTween;

    private void Start()
    {
        sentenceQueue = new Queue<string>();
    }

    private void Update()
    {
        if ((PlayerController.Instance.defaultInputAction.Player.Attack.triggered || PlayerController.Instance.defaultInputAction.UI.Submit.triggered) && isStart == true && isEnd == false)
            DisplayNextSentence();
    }

    public void StartDialogue()
    {
        PlayerController.Instance.canCtrl = false;
        isStart = true;
        nameText.text = npcName;
        continueObj.SetActive(false);
        sentenceQueue.Clear();
        foreach (string sentence in sentences)
        {
            sentenceQueue.Enqueue(sentence);
        }

        dialogueObj.SetActive(true);
        DisplayNextSentence();
    }

    public void DisplayNextSentence()
    {
        if (isEnd == true) return;
        SoundManager.Instance.PlaySound("UI_Interact");

        if (sentenceQueue.Count == 0)
        {
            EndDialogue();
            return;
        }

        if (isTypeing == true)
        {
            StopCoroutine(typeCoroutine);
            fadeInTween.Kill();
            dialogueText.text = currentSentence;
            dialogueText.color = new Color(dialogueText.color.r, dialogueText.color.g, dialogueText.color.b, 1);
            continueObj.SetActive(true);
            sentenceQueue.Dequeue();
            isTypeing = false;
            return;
        }

        continueObj.SetActive(false);
        currentSentence = sentenceQueue.Peek();
        if (fadeInOnStart == true)
            fadeInTween = dialogueText.DOFade(1, fadeInOnStartSec).From(0);

        if (sentenceMode == DialogueSentenceMode.Type)
            typeCoroutine = StartCoroutine(TypeSentence(currentSentence));
        else if (sentenceMode == DialogueSentenceMode.OneSentence)
            typeCoroutine = StartCoroutine(OneSentence(currentSentence));
        else
            dialogueText.text = currentSentence;
    }

    private IEnumerator OneSentence(string sentence)
    {
        isTypeing = true;
        dialogueText.text = "";
        foreach (string letter in sentence.Split('\n'))
        {
            dialogueText.text += letter;
            yield return new WaitForSeconds(sentenceWaitTime);
        }
        sentenceQueue.Dequeue();
        isTypeing = false;
    }

    private IEnumerator TypeSentence(string sentence)
    {
        isTypeing = true;
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return new WaitForSeconds(typeSec);
        }
        continueObj.SetActive(true);
        sentenceQueue.Dequeue();
        isTypeing = false;
    }

    public virtual void EndDialogue()
    {
        PlayerController.Instance.canCtrl = true;
        SlideDownOut();
        isEnd = true;
    }

    private void SlideDownOut()
    {
        dialogueObj.transform.DOMoveY(-Camera.main.pixelHeight * 0.5f , 0.7f).SetEase(Ease.InBack);
    }
}