using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(NpcDialogue), true)]
public class NpcDialogueEditor : Editor
{
    public override void OnInspectorGUI()
    {
        NpcDialogue m_Target = (NpcDialogue)target;

        EditorGUILayout.PropertyField(serializedObject.FindProperty("dialogueObj"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("npcImage"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("continueObj"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("nameText"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("dialogueText"));

        GUILayout.Space(5);
        GUILayout.Label("NPC設定");
        EditorGUILayout.PropertyField(serializedObject.FindProperty("npcName"));
        EditorGUILayout.PropertyField(serializedObject.FindProperty("npcSprite"));

        GUILayout.Space(5);
        GUILayout.Label("對話設定");
        EditorGUILayout.PropertyField(serializedObject.FindProperty("sentenceMode"));
        switch (m_Target.sentenceMode)
        {
            case DialogueSentenceMode.None:

                break;

            case DialogueSentenceMode.Type:
                m_Target.typeSec = EditorGUILayout.FloatField("TypeSec", m_Target.typeSec);
                break;

            case DialogueSentenceMode.OneSentence:
                m_Target.sentenceWaitTime = EditorGUILayout.FloatField("SentenceWaitTime", m_Target.sentenceWaitTime);
                break;
        }

        m_Target.fadeInOnStart = EditorGUILayout.Toggle("FadeInOnStart", m_Target.fadeInOnStart);
        if (m_Target.fadeInOnStart == true)
            m_Target.fadeInOnStartSec = EditorGUILayout.FloatField("FadeInSec", m_Target.fadeInOnStartSec);

        EditorGUILayout.PropertyField(serializedObject.FindProperty("sentences"));
        serializedObject.ApplyModifiedProperties();
    }
}
