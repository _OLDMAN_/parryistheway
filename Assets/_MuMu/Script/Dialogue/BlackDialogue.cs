using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using DG.Tweening;
using UnityEngine.UI;

public class BlackDialogue : MonoBehaviour
{
    public GameObject blackDialogueObj;
    public Image blackPanel;
    public TMP_Text[] dialogueText;

    [TextArea(3, 10)]
    public string[] sentences;
    public float sentenceFadeInSec = 1.5f;
    public float sentenceWaitTime = 0.5f;
    public float endFadeOutSec = 0.5f;
    protected bool isEnd = false;

    private bool isStart;
    private Queue<string> sentenceQueue = new Queue<string>();
    private string currentSentence;
    private bool isTypeing;
    private int sentenceNum;
    private int splitCount;
    private Coroutine fadeInSentence;
    private Tween fadeInTween;

    public void Update()
    {
        if (PlayerController.Instance.defaultInputAction.UI.Submit.triggered && isStart == true)
        {
            DisplayNextSentence();
        }
    }

    public void StartDialogue()
    {
        isStart = true;
        PlayerController.Instance.canCtrl = false;
        TextFade();
        blackDialogueObj.SetActive(true);
        sentenceQueue.Clear();
        foreach (string sentence in sentences)
        {
            sentenceQueue.Enqueue(sentence);
        }

        DisplayNextSentence();
    }

    private void TextFade()
    {
        foreach (TMP_Text child in dialogueText)
        {
            Color light = child.color;
            light.a = 0;
            child.color = light;
        }
    }

    public void DisplayNextSentence()
    {
        if (isEnd == true) return;

        if (sentenceQueue.Count == 0)
        {
            EndDialogue();
            return;
        }

        if (isTypeing == true)
        {
            Color light = dialogueText[sentenceNum].color;
            light.a = 1;
            fadeInTween.Kill();
            if (fadeInSentence != null)
            {
                StopCoroutine(fadeInSentence);
            }
            dialogueText[sentenceNum].color = light;
            sentenceNum++;
            fadeInSentence = StartCoroutine(StartFadeIn());
            return;
        }

        currentSentence = sentenceQueue.Dequeue();
        TextFade();
        string[] splitSentence = currentSentence.Split('\n');
        splitCount = splitSentence.Length;
        for (int i = 0; i < splitCount; i++)
            dialogueText[i].text = splitSentence[i];
        sentenceNum = 0;
        isTypeing = true;
        fadeInSentence = StartCoroutine(StartFadeIn());
    }

    private IEnumerator StartFadeIn()
    {
        while (sentenceNum < splitCount)
        {
            fadeInTween = dialogueText[sentenceNum].DOFade(1, sentenceFadeInSec).From(0).OnComplete(() => sentenceNum++);
            yield return new WaitForSeconds(sentenceWaitTime + sentenceFadeInSec);
        }

        //sentenceQueue.Dequeue();
        isTypeing = false;
        DisplayNextSentence();
    }

    public virtual void EndDialogue()
    {
        fadeInTween.Kill();
        blackPanel.DOFade(0, endFadeOutSec).From(1).OnComplete(() =>
        {
            blackDialogueObj.SetActive(false);
            FadeOutComplete();
        });
        for (int i = 0; i < splitCount; i++)
        {
            dialogueText[i].DOFade(0, endFadeOutSec).From(1);
        }
        isStart = false;
        isEnd = true;
    }

    public virtual void FadeOutComplete()
    {

    }
}