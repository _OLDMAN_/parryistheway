using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Room_Tutorial1Npc : NpcDialogue
{
    public override void EndDialogue()
    {
        base.EndDialogue();
        RoomManager.Instance.currentRoomSetting.teachBoxBoard.StartTeach();
    }
}
