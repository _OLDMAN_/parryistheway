using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossBlackDialogue : BlackDialogue
{
    public override void EndDialogue()
    {
        ResetAllValue();
        BackToMenu();
    }

    private void ResetAllValue()
    {
        PlayerHP.Instance.ResetValue();
    }

    private void BackToMenu()
    {
        SoundManager.Instance.BackToMenu();
    }
}