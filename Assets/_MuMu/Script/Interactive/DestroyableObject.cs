using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyableObject : MonoBehaviour, IDestroyable
{
    public int destroyCount = 1;
    public int destroyCounter { get; set; }
    public bool isDestroy { get; set; }

    public virtual void Awake()
    {
        destroyCounter = destroyCount;
    }

    public virtual void Damage(int count = 1)
    {
        destroyCounter -= count;
        if (destroyCounter <= 0)
            Destroy();
    }

    public virtual void Destroy()
    {
        isDestroy = true;
        Destroy(gameObject);
    }
}
