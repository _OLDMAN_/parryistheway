using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilityObj : InteractableObject
{
    [SerializeField] private Quest_PickUpItem quest_pickupitem;

    public enum PlayerCtrl { DASH, ATTACK, BLOCK, BLOCKATTACK };
    public PlayerCtrl unLockType;

    protected override void Interact()
    {
        base.Interact();
        CompleteDashQuest();
        UnLockPlayerCtrl();
        SoundManager.Instance.PlaySound("GainDash");
        ParticleManager.Instance.PlayParticle("FireCollect", PlayerController.Instance.transform.position, Quaternion.identity, PlayerController.Instance.transform);
        ParticleManager.Instance.PlayParticle("FireDissipate", transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    private void UnLockPlayerCtrl()
    {
        if(unLockType == PlayerCtrl.DASH)
            TeachManager.Instance.unLockDash = true;
        else if (unLockType == PlayerCtrl.ATTACK)
            TeachManager.Instance.unLockAttackAndDashAttack = true;
        else if (unLockType == PlayerCtrl.BLOCK)
            TeachManager.Instance.unLockBlockAndBlockAttack = true;
        else if (unLockType == PlayerCtrl.BLOCKATTACK)
            TeachManager.Instance.unLockBlockBurst = true;

        if (unLockType == PlayerCtrl.DASH)
            RoomManager.Instance.currentRoomSetting.teachBoxBoard.NextTeach();
        else
            RoomManager.Instance.currentRoomSetting.teachBoxBoard.ShowAllTeach();
    }

    private void CompleteDashQuest()
    {
        quest_pickupitem.QuestComplete();
    }
}
