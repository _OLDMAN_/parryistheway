using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveFruit : DestroyableObject
{
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public override void Damage(int count = 1)
    {
        if (isDestroy == true) return;

        destroyCounter -= count;
        if (destroyCounter <= 0)
            Destroy();
        else
            animator.SetTrigger("Hit");

        ParticleManager.Instance.PlayParticle("BerryFruitHit", transform.position, transform.rotation);
        SoundManager.Instance.PlaySound("FruitHurt");
    }

    public override void Destroy()
    {
        animator.SetTrigger("Dead");
        gameObject.layer = default;
        isDestroy = true;
    }
}
