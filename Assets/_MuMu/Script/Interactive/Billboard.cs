using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : InteractableObject
{
    public NpcDialogue npcDialogue;
    protected override void Interact()
    {
        base.Interact();
        StartDialogue();
    }

    void StartDialogue()
    {
        npcDialogue.StartDialogue();
    }
}
