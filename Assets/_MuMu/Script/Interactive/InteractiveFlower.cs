using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveFlower : DestroyableObject
{
    public GameObject stemObj;
    public ParticleSystem hitParticle;
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public override void Damage(int count = 1)
    {
        if (isDestroy == true) return;

        destroyCounter -= count;
        if (destroyCounter <= 0)
            Destroy();
        else
            animator.SetTrigger("Hit");

        SoundManager.Instance.PlaySound("FruitHurt");
        hitParticle.Play();
    }
    public override void Destroy()
    {
        stemObj.SetActive(false);
        animator.SetTrigger("Dead");
        gameObject.layer = default;
        isDestroy = true;
    }
}
