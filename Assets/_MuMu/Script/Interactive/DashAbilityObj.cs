using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DashAbilityObj : InteractableObject
{
    [SerializeField]private Quest_PickUpItem quest_pickupitem;

    protected override void Interact()
    {
        base.Interact();
        TeachManager.Instance.unLockDash = true;
        CompleteDashQuest();
        RoomManager.Instance.currentRoomSetting.teachBoxBoard.NextTeach();
        ParticleManager.Instance.PlayParticle("FireDissipate", transform.position, Quaternion.identity);
        Destroy(gameObject);
    }

    private void CompleteDashQuest()
    {
        quest_pickupitem.QuestComplete();
    }
}
