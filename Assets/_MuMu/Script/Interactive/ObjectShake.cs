using UnityEngine;
using System.Collections;

/// http://www.mikedoesweb.com/2012/camera-shake-in-unity/

public class ObjectShake : MonoBehaviour {

	private Vector3 originPosition;
	private Quaternion originRotation;
	public bool shakePos = true;
	public bool shakeRot = true;
	public float shake_decay = 0.002f;
	public float shake_intensity = .3f;

	private float temp_shake_intensity = 0;

    private void Start()
    {
		temp_shake_intensity = shake_intensity;
	}

    void Update ()
	{
		if (temp_shake_intensity < shake_intensity)
		{
			if (shakePos == true)
				transform.position = originPosition + Random.insideUnitSphere * temp_shake_intensity;
			if (shakeRot == true)
			{
                transform.rotation = new Quaternion(
                    originRotation.x + Random.Range(-temp_shake_intensity, temp_shake_intensity) * .2f,
                    originRotation.y + Random.Range(-temp_shake_intensity, temp_shake_intensity) * .2f,
                    originRotation.z + Random.Range(-temp_shake_intensity, temp_shake_intensity) * .2f,
                    originRotation.w + Random.Range(-temp_shake_intensity, temp_shake_intensity) * .2f);
            }

			temp_shake_intensity += shake_decay * Time.deltaTime;
		}
	}
	
	public void Shake()
	{
		originPosition = transform.position;
		originRotation = transform.rotation;
		temp_shake_intensity = 0;
	}
}