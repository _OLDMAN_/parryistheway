using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableObject : MonoBehaviour
{
    public Action OnInteracted;

    protected bool canUse = true;
    protected bool playerEneter = false;
    [SerializeField] protected Transform hint;
    [SerializeField] protected Transform center;


    protected virtual void Update()
    {
        if (canUse == false || playerEneter == false) return;

        if(PlayerController.Instance.defaultInputAction.Player.Dash.triggered)
        {
            canUse = false;
            Interact();
            OnInteracted?.Invoke();
        }
    }
   
    protected virtual void Interact()
    {
        SoundManager.Instance.PlaySound("UI_Interact");
        PlayerController.Instance.inItemInteractRange = false;
        CloseHint();
    }

    protected virtual void OnTriggerStay(Collider other)
    {
        if (canUse == false || playerEneter == true) return;

        if (other.CompareTag("Player"))
        {
            playerEneter = true;
            ShowHint();
            PlayerController.Instance.inItemInteractRange = true;
        }
    }
    protected virtual void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            playerEneter = false;
            CloseHint();
            PlayerController.Instance.inItemInteractRange = false;
        }
    }
    protected void ShowHint()
    {
        hint.transform.forward = Camera.main.transform.forward;
        hint.gameObject.SetActive(true);
    }
    protected void CloseHint()
    {
        hint.gameObject.SetActive(false);
    }
}
