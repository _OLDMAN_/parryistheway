using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FeelEffect : MonoBehaviourSingleton<FeelEffect>
{
    public Parameter_GameFeel feelParameter;

    [Header("卡偵")]
    public InputField stopTimeInput;
    public InputField stopIntensityInput;
    [Header("卡偵過度")]
    public Toggle stopLerpToggle;
    public InputField stopLerpTimeInput;
    public InputField stopLerpIntensityInput;
    [Header("慢動作")]
    public InputField slowTimeInput;
    public InputField slowTimeIntensityInput;
    [Header("慢動作過度")]
    public Toggle slowTimeLerpToggle;
    public InputField slowTimeLerpInput;
    public InputField slowTimeLerpIntensityInput;
    [Header("鏡頭晃動")]
    public InputField shakeCameraInput;
    public InputField shakeCameraIntensityInput;

    public void Save()
    {
        feelParameter.heavyAttackStop.time = float.Parse(stopTimeInput.text);
        feelParameter.heavyAttackStop.intensity = float.Parse(stopIntensityInput.text);
        feelParameter.heavyAttackStop.lerpToggle = stopLerpToggle.isOn;
        feelParameter.heavyAttackStop.lerpTime = float.Parse(stopLerpTimeInput.text);
        feelParameter.lastAttackSlow.time = float.Parse(slowTimeInput.text);
        feelParameter.lastAttackSlow.intensity = float.Parse(slowTimeIntensityInput.text);
        feelParameter.lastAttackSlow.lerpToggle = slowTimeLerpToggle.isOn;
        feelParameter.lastAttackSlow.lerpTime = float.Parse(slowTimeLerpInput.text);
        feelParameter.lastAttackShake.time = float.Parse(shakeCameraInput.text);
        feelParameter.lastAttackShake.intensity = float.Parse(shakeCameraIntensityInput.text);
    }

    public void Read()
    {
        stopTimeInput.text = feelParameter.heavyAttackStop.time.ToString();
        stopIntensityInput.text = feelParameter.heavyAttackStop.intensity.ToString();
 
        stopLerpToggle.isOn = feelParameter.heavyAttackStop.lerpToggle;
        stopLerpTimeInput.text = feelParameter.heavyAttackStop.lerpTime.ToString();
 
        slowTimeInput.text = feelParameter.lastAttackSlow.time.ToString();
        slowTimeIntensityInput.text = feelParameter.lastAttackSlow.intensity.ToString();
 
        slowTimeLerpToggle.isOn = feelParameter.lastAttackSlow.lerpToggle;
        slowTimeLerpInput.text = feelParameter.lastAttackSlow.lerpTime.ToString();
 
        shakeCameraInput.text = feelParameter.lastAttackShake.time.ToString();
        shakeCameraIntensityInput.text = feelParameter.lastAttackShake.intensity.ToString();
    }
}
