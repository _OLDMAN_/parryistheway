using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class SpawnInfo{
    public string name;
    public List<MonsterInfo> monsterList;
}

[System.Serializable]
public class MonsterInfo{
    public string name;
    public GameObject prefab;
    public InputField input;
}

public class SpawnEnemy : MonoBehaviourSingleton<SpawnEnemy>
{
    public List<SpawnInfo> spawnInfoList;
    public List<GameObject> enemys;
    public Transform area;
    public void Spawn()
    {
        foreach(SpawnInfo info in spawnInfoList)
        {
            foreach(MonsterInfo monster in info.monsterList)
            {
                GameObject prefab = monster.prefab;
                int count;
                int.TryParse(monster.input.text, out int result);
                count = result;
                Debug.Log(info.name + monster.name + " : " + count);
                for(int c = 0; c < count; c++)
                {
                    float randx = Random.Range(-area.localScale.x / 2, area.localScale.x / 2);
                    float randz = Random.Range(-area.localScale.z / 2, area.localScale.z / 2);
                    GameObject enemy = Instantiate(prefab, area.position + new Vector3(randx, 0, randz), Quaternion.identity);
                    enemys.Add(enemy);
                }
            }
        }
        if(enemys.Count != 0)
        {
            DelPanel.Instance.SwitchDelPanel(true);
            DelPanel.Instance.RefreshEnemyCountText();
        }
    }

    public void ResetInputField()
    {
        foreach(SpawnInfo info in spawnInfoList)
        {
            foreach(MonsterInfo monster in info.monsterList)
            {
                monster.input.text = "0";
            }
        }
    }

    public void RemoveEnemy(GameObject obj)
    {
        enemys.Remove(obj);
        //DelPanel.Instance.RefreshEnemyCountText();
        if(enemys.Count == 0)
        {
            //DelPanel.Instance.SwitchDelPanel(false);
            GamefeelManager.Instance.LastKill();
        }
    }
}
