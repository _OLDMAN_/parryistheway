using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPanel : MonoBehaviour
{
    [Header("F1開/關")]
    public GameObject spawnPanel;
    private bool spawnSwitch;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.F1))
            SwitchSpawnPanel();
    }

    private void SwitchSpawnPanel()
    {
        spawnSwitch = !spawnSwitch;
        spawnPanel.SetActive(spawnSwitch);
        SwitchPlayerCtrl(!spawnSwitch);
        if(spawnSwitch == true)
            SpawnEnemy.Instance.ResetInputField();
    }

    public void CloseSpawnPanel()
    {
        spawnSwitch = false;
        spawnPanel.SetActive(spawnSwitch);
        SwitchPlayerCtrl(!spawnSwitch);
    }

    private void SwitchPlayerCtrl(bool value)
    {
        PlayerController.Instance.canCtrl = value;
    }
}
