using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FeelPanel : MonoBehaviour
{
    [Header("F2開/關")]
    public GameObject feelPanel;
    private bool feelSwitch;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.F2))
            SwitchSpawnPanel();
    }
    private void SwitchSpawnPanel()
    {
        feelSwitch = !feelSwitch;
        feelPanel.SetActive(feelSwitch);
        SwitchPlayerCtrl(!feelSwitch);
        if(feelSwitch == true)
        {
            FeelEffect.Instance.Read();
            Time.timeScale = 0;
        }
        else
            Time.timeScale = 1;
    }

    public void CloseSpawnPanel()
    {
        feelSwitch = false;
        feelPanel.SetActive(feelSwitch);
        SwitchPlayerCtrl(!feelSwitch);
        Time.timeScale = 1;
    }

    private void SwitchPlayerCtrl(bool value)
    {
        PlayerController.Instance.canCtrl = value;
    }
}
