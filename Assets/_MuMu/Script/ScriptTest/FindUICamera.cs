using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FindUICamera : MonoBehaviourSingleton<FindUICamera>
{
    public Canvas playerUI_Canvas;
    public Canvas gem_Canvas;

    public void Find()
    {
        Camera uiCamera = GameObject.FindWithTag("UICamera").GetComponent<Camera>();
        playerUI_Canvas.worldCamera = uiCamera;
        gem_Canvas.worldCamera = uiCamera;
    }
}
