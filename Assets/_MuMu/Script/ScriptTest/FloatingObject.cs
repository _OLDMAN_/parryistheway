using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloatingObject : MonoBehaviour
{
    private float radian = 0;
    private float perRadian = 0.2f;
    private float radius = 0.05f;
    private Vector3 oldPos;
    public bool disableRot;

    void Start()
    {
        oldPos = transform.position;
    }

    void Update()
    {
        radian += perRadian;
        float dy = Mathf.Cos(radian) * radius;
        transform.position = oldPos + new Vector3(0, dy, 0);
        if (disableRot == false)
            transform.localEulerAngles += new Vector3(0, dy * 10, dy * 10);

    }
}
