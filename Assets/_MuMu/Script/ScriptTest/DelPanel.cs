using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DelPanel : MonoBehaviourSingleton<DelPanel>
{
    public GameObject delPanel;
    public Text enemyCountUI;

    public void SwitchDelPanel(bool value)
    {
        delPanel.SetActive(value);
    }

    public void RefreshEnemyCountText()
    {
        enemyCountUI.text = "怪物數量 : " + SpawnEnemy.Instance.enemys.Count.ToString();
    }
    
    public void DelAllEnemy()
    {
        int count = SpawnEnemy.Instance.enemys.Count;
        for(int i = 0; i < count; i++)
        {
            Melee01ActionManager melee1 = SpawnEnemy.Instance.enemys[0].GetComponent<Melee01ActionManager>();
            if(melee1 != null){
                melee1.Dead();
                continue;
            }
            Melee02ActionManager melee2 = SpawnEnemy.Instance.enemys[0].GetComponent<Melee02ActionManager>();
            if(melee2 != null){
                melee2.Dead();
                continue;
            }
            Range01ActionManager range1 = SpawnEnemy.Instance.enemys[0].GetComponent<Range01ActionManager>();
            if(range1 != null){
                range1.Dead();
                continue;
            }
            Range02ActionManager range2 = SpawnEnemy.Instance.enemys[0].GetComponent<Range02ActionManager>();
            if(range2 != null){
                range2.Dead();
                continue;
            }
        }
        SwitchDelPanel(false);
    }
}
