﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemCardInformation : MonoBehaviour
{
    public NewGemType gemType;
    public CardSetting cardSetting;
    public List<QualitySprite> qualitySprite;

    public void RefreshCard()
    {
        gameObject.SetActive(true);
        NewGem playerGem = null;
        if (gemType == NewGemType.Dash)
            playerGem = PlayerGem.Instance.dashAttack;
        else if (gemType == NewGemType.Q)
            playerGem = PlayerGem.Instance.autoBlockSkill;
        else if (gemType == NewGemType.Block)
            playerGem = PlayerGem.Instance.blockSkill;

        if (playerGem.skill != null)
        {
            cardSetting.mainText.text = playerGem.skill.Detail();
            cardSetting.secondText01.text = playerGem.secondSkills[0].Detail();
            if (playerGem.secondSkills.Count > 1)
            {
                cardSetting.secondText02.text = playerGem.secondSkills[1].Detail();
                cardSetting.secondText02.gameObject.SetActive(true);
            }
            else
                cardSetting.secondText02.gameObject.SetActive(false);
            if (playerGem.skill.quality == SkillQuality.Purple)
                cardSetting.qualityBG.sprite = qualitySprite[1].sprite;
            else if (playerGem.skill.quality == SkillQuality.Gold)
                cardSetting.qualityBG.sprite = qualitySprite[2].sprite;
        }
        else
        {
            gameObject.SetActive(false);
        }
    }
}
