using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemManager : MonoBehaviourSingleton<GemManager>
{
    public List<EnhanceData> enhances;
    public List<PlayerSkill> skills;
    public List<SecondSkill> secondSkills;

    public int enhanceWeight;
    public int gemWeight;
    private Dictionary<string, int> weightDict = new Dictionary<string, int>();

    private List<Enhance> randEnhances = new List<Enhance>();
    private List<NewGem> randNewGem = new List<NewGem>();

    public GameObject goldGem;
    public GameObject purpleGem;
    public GameObject blueGem;
    public GameObject teachDashGem;
    public GameObject teachBlockGem;
    public GameObject teachQGem;

    public GameObject shop;

    private void Start()
    {
        weightDict.Add("能力", enhanceWeight);
        weightDict.Add("寶石", gemWeight);
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Keypad5))
            SpawnGem(PlayerController.Instance.transform.position);
    }
    public void DisplayShop()
    {
        shop.SetActive(true);
        GemShop.Instance.OpenCardEventTrigger();
        PlayerController.Instance.canCtrl = false;
        PlayerController.Instance.animation.PlayAnimation(PlayerAni.Idle);
        QuestManager.Instance.QuestBoardHide();
        if(RoomManager.Instance.currentRoomSetting.teachBoxBoard != null)
        RoomManager.Instance.currentRoomSetting.teachBoxBoard.HideAllTeach();
    }

    public GameObject SpawnGem(Vector3 pos)
    {
        pos.y = 1;

        randEnhances.Clear();
        randNewGem.Clear();
        int a = 3;
        while (a > 0)
        {
            SpawnNewGemOrEnhance();
            a--;
        }
        int gold = 0;
        int purple = 0;
        foreach(Enhance enhance in randEnhances)
        {
            if (enhance.quality == EnhanceQuality.Gold)
                gold++;
            else if (enhance.quality == EnhanceQuality.Purple)
                purple++;
        }
        foreach(NewGem newGem in randNewGem)
        {
            if (newGem.skill.quality == SkillQuality.Gold)
                gold++;
            else if (newGem.skill.quality == SkillQuality.Purple)
                purple++;
        }
        GameObject o;
        if (gold > 0)
            o = Instantiate(goldGem, pos, Quaternion.identity);
        else if(purple > 0)
            o = Instantiate(purpleGem, pos, Quaternion.identity);
        else
            o = Instantiate(blueGem, pos, Quaternion.identity);
        o.GetComponent<GemObject>().SetGem(randNewGem, randEnhances);
        return o;
    }

    private int GetTotalWeight()
    {
        int totalweight = 0;
        foreach (var weight in weightDict.Values)
            totalweight += weight;
        return totalweight;
    }

    private string GetRanId()
    {
        int ranNum = Random.Range(0, GetTotalWeight());
        int counter = 0;
        foreach(var temp in weightDict)
        {
            counter += temp.Value;
            if(ranNum < counter)
                return temp.Key;
        }

        return "";
    }

    private void SpawnNewGemOrEnhance()
    {
        string type = GetRanId();
        if (type == "寶石")
            SpawnNewGem();
        else if (type == "能力")
            SpawnNewEnhance();
    }

    private void SpawnNewGem()
    {
        PlayerSkill skill = GetRanPlayerSkill();
        randNewGem.Add(new NewGem(skill, secondSkills.GetRandom(skill.secondSkillCount)));
    }
    public PlayerSkill GetRanPlayerSkill()
    {
        Dictionary<PlayerSkill, int> dict = new Dictionary<PlayerSkill, int>();
        foreach (PlayerSkill p in skills)
            dict.Add(p, p.weight);

        int totalweight = 0;
        foreach (var weight in dict.Values)
            totalweight += weight;

        int ranNum = Random.Range(0, totalweight);
        int counter = 0;
        foreach (var temp in dict)
        {
            counter += temp.Value;
            if (ranNum < counter)
                return temp.Key;
        }

        return null;
    }
    private void SpawnNewEnhance()
    {
        randEnhances.Add(GetRanEnhance().GetEnhance());
    }
    public EnhanceData GetRanEnhance()
    {
        Dictionary<EnhanceData, int> dict = new Dictionary<EnhanceData, int>();
        foreach (EnhanceData p in enhances)
            dict.Add(p, p.weight);

        int totalweight = 0;
        foreach (var weight in dict.Values)
            totalweight += weight;

        int ranNum = Random.Range(0, totalweight);
        int counter = 0;
        foreach (var temp in dict)
        {
            counter += temp.Value;
            if (ranNum < counter)
                return temp.Key;
        }

        return null;
    }
}
