using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemObject : InteractableObject
{
    public GameObject focusCamera;
    public Transform qualityParticle;
    public ParticleSystem highLightParticle;

    public List<NewGem> newGems = new List<NewGem>();
    public List<Enhance> enhances = new List<Enhance>();
    public bool randGem = true;
    private Rigidbody rb;

    public void SetGem(List<NewGem> gems, List<Enhance> enhances)
    {
        newGems = gems;
        this.enhances = enhances;
    }
    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        StartCoroutine(StartFocus());
        RandFall();
        Physics.IgnoreCollision(GetComponent<Collider>(), PlayerController.Instance.GetComponent<Collider>(), true);
        hint.forward = Camera.main.transform.forward;
    }
    private void KeepParticleFollow()
    {
        qualityParticle.rotation = Quaternion.identity;
        highLightParticle.transform.rotation = Quaternion.identity;
        hint.forward = Camera.main.transform.forward;
    }
    private void LateUpdate()
    {
        KeepParticleFollow();
    }

    private void RandFall()
    {
        Vector2 dir = Random.insideUnitCircle;
        rb.AddForce(new Vector3(dir.x, 0.5f, dir.y) * 2, ForceMode.Impulse);
    }

    private IEnumerator StartFocus()
    {
        SoundManager.Instance.PlaySound("GemDrop");

        PlayerController.Instance.canCtrl = false;
        PlayerController.Instance.animation.PlayAnimation(PlayerAni.Idle);
        focusCamera.SetActive(true);
        yield return new WaitForSeconds(2.5f);
        focusCamera.SetActive(false);
        highLightParticle.Play();
        yield return new WaitForSeconds(1);
        PlayerController.Instance.canCtrl = true;
    }

    protected override void Interact()
    {
        SoundManager.Instance.PlaySound("UI_Interact");
        GemManager.Instance.DisplayShop();
        if (randGem)
            RandGive();
        else
        {
            int randNum = 0;
            for (int i = 0; i < newGems.Count; i++)
            {
                GemShop.Instance.buyCards[randNum].SetInformation(newGems[i]);
                randNum++;
            }
            for (int i = 0; i < enhances.Count; i++)
            {
                GemShop.Instance.buyCards[randNum].SetInformation(enhances[i]);
                randNum++;
            }
        }
        Destroy(gameObject);
    }
    private void RandGive()
    {
        int[] numArray = new int[3] { 0, 1, 2 };
        int[] randArray = numArray.GetRandomNum();
        int randNum = 0;
        for (int i = 0; i < newGems.Count; i++)
        {
            GemShop.Instance.buyCards[randArray[randNum]].SetInformation(newGems[i]);
            randNum++;
        }
        for (int i = 0; i < enhances.Count; i++)
        {
            GemShop.Instance.buyCards[randArray[randNum]].SetInformation(enhances[i]);
            randNum++;
        }
    }
}
