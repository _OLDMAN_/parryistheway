using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GemShop : MonoBehaviourSingleton<GemShop>
{
    public List<BuyCardInformation> buyCards;
    public List<GemCardInformation> gemCards;

    public void RefreshGemCards()
    {
        foreach (GemCardInformation card in gemCards)
            card.RefreshCard();
    }

    public void OpenCardEventTrigger()
    {
        foreach (BuyCardInformation card in buyCards)
        {
            card.cardSetting.qualityBG.GetComponent<Button>().interactable = true;
            //card.setting.bg.GetComponent<EventTrigger>().enabled = true;
        }
    }

    public void CloseCardEventTrigger()
    {
        foreach (BuyCardInformation card in buyCards)
        {
            card.cardSetting.qualityBG.GetComponent<Button>().interactable = false;
            //card.setting.bg.GetComponent<EventTrigger>().enabled = false;
        }
    }
    public void LeaveShop()
    {
        transform.parent.gameObject.SetActive(false);
        if (QuestManager.Instance.currentQuest != null)
            QuestManager.Instance.currentQuest.QuestComplete();
        PlayerController.Instance.canCtrl = true;
        PlayerController.Instance.inItemInteractRange = false;
        QuestManager.Instance.QuestBoardDisplay();
        if (RoomManager.Instance.currentRoomSetting.teachBoxBoard != null)
            RoomManager.Instance.currentRoomSetting.teachBoxBoard.ShowAllTeach();
    }
    public void PlayGemSlideIn()
    {
        SoundManager.Instance.PlaySound("UI_Gem");
    }

    public void EnterEndFrame()
    {
        if (TeachManager.Instance.isTeach == true)
        {
            if (RoomManager.Instance.roomNum == 4)
                RoomManager.Instance.currentRoomSetting.gemTeachBox.StartGemTeach();
        }
    }
}
