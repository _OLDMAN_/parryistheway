using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BuyCardInformation : MonoBehaviour
{
    public NewGemType gemType;
    public CardSetting cardSetting;
    public List<QualitySprite> qualitySprites;
    public GameObject gemSelectObj;
    public GameObject enhanceSelectObj;
    public Animator animator;

    private Enhance enhance;
    private NewGem gem;

    public void SetInformation(NewGem gem)
    {
        enhance = null;
        this.gem = gem;
        SetQualityBG(gem.skill.quality.ToString());
        cardSetting.skillIcon.gameObject.SetActive(true);
        cardSetting.skillIcon.sprite = gem.skill.skillIcon;
        cardSetting.mainText.text = gem.skill.Detail();
        if(gem.secondSkills.Count > 1)
        {
            cardSetting.secondText01.gameObject.SetActive(true);
            cardSetting.secondText02.gameObject.SetActive(true);
            cardSetting.secondText01.text = gem.secondSkills[0].Detail();
            cardSetting.secondText02.text = gem.secondSkills[1].Detail();
        }
        else
        {
            cardSetting.secondText01.gameObject.SetActive(true);
            cardSetting.secondText02.gameObject.SetActive(false);
            cardSetting.secondText01.text = gem.secondSkills[0].Detail();
        }
    }

    public void SetInformation(Enhance enhance)
    {
        gem = null;
        this.enhance = enhance;
        SetQualityBG(enhance.quality.ToString());
        cardSetting.skillIcon.gameObject.SetActive(false);
        cardSetting.secondText01.gameObject.SetActive(false);
        cardSetting.secondText02.gameObject.SetActive(false);
        cardSetting.mainText.text = enhance.detail;
    }

    private void SetQualityBG(string quality)
    {
        if (quality == "Blue")
            cardSetting.qualityBG.sprite = qualitySprites[0].sprite;
        else if (quality == "Purple")
            cardSetting.qualityBG.sprite = qualitySprites[1].sprite;
        else if (quality == "Gold")
            cardSetting.qualityBG.sprite = qualitySprites[2].sprite;
    }

    public void OnPointerEnter()
    {
        SoundManager.Instance.PlaySound("UI_Hover");

        if (gem != null)
            gemSelectObj.SetActive(true);
        else
            enhanceSelectObj.SetActive(true);
    }

    public void OnPointerExit()
    {
        if (gem != null)
            gemSelectObj.SetActive(false);
        else
            enhanceSelectObj.SetActive(false);
    }
    public void OnClick()
    {
        SoundManager.Instance.PlaySound("UI_BtnPressed");
        GemShop.Instance.CloseCardEventTrigger();
        if (gem != null)
        {
            animator.SetTrigger("Gem");
            if (gemType == NewGemType.Dash)
            {
                animator.SetTrigger("GemA");
                PlayerGem.Instance.dashAttack = gem;
            }
            else if (gemType == NewGemType.Block)
            {
                animator.SetTrigger("GemB");
                PlayerGem.Instance.blockSkill = gem;
            }
            else
            {
                animator.SetTrigger("GemC");
                PlayerGem.Instance.autoBlockSkill = gem;
            }
        }
        else
        {
            animator.SetTrigger("Ability");
            enhance.Cast();
        }
        if (TeachManager.Instance.isTeach == true)
        {
            if (RoomManager.Instance.roomNum == 4)
                RoomManager.Instance.currentRoomSetting.gemTeachBox.AllFadeOut();
        }
    }

}

[System.Serializable]
public class QualitySprite
{
    public string name;
    public Sprite sprite;
}

[System.Serializable]
public class CardSetting
{
    public Image qualityBG;
    public Image skillIcon;
    public Text mainText;
    public Text secondText01;
    public Text secondText02;
}
