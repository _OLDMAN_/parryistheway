// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "IntensifyCrystal"
{
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255

		_ColorMask ("Color Mask", Float) = 15

		[Toggle(UNITY_UI_ALPHACLIP)] _UseUIAlphaClip ("Use Alpha Clip", Float) = 0
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		[HDR]_Color0("Color 0", Color) = (1,1,1,1)
		_Saturate("Saturate", Float) = 0
		[HDR][Header(Point Light Setting)]_LightColor("LightColor", Color) = (0,0,0,0)
		_VitualPointLightTexture("Vitual Point Light Texture", 2D) = "white" {}
		_LightZoom("Light Zoom", Float) = 0
		_LightXOffset("Light X Offset", Float) = 0
		_LightYOffset("Light Y Offset", Float) = 0
		[Header(Flow Setting)]_ShadowXOffset("Shadow X Offset", Float) = 0
		_ShadowYOffset("Shadow Y Offset", Float) = 0
		_ShadowTexture("Shadow Texture", 2D) = "white" {}
		_ShadowSpeed("Shadow Speed", Float) = 0
		[HDR]_FlowColor("Flow Color", Color) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}

	}

	SubShader
	{
		LOD 0

		Tags { "Queue"="Transparent" "IgnoreProjector"="True" "RenderType"="Transparent" "PreviewType"="Plane" "CanUseSpriteAtlas"="True" }
		
		Stencil
		{
			Ref [_Stencil]
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
			CompFront [_StencilComp]
			PassFront [_StencilOp]
			FailFront Keep
			ZFailFront Keep
			CompBack Always
			PassBack Keep
			FailBack Keep
			ZFailBack Keep
		}


		Cull Off
		Lighting Off
		ZWrite Off
		ZTest [unity_GUIZTestMode]
		Blend SrcAlpha OneMinusSrcAlpha
		ColorMask [_ColorMask]

		
		Pass
		{
			Name "Default"
		CGPROGRAM
			
			#ifndef UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX
			#define UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input)
			#endif
			#pragma vertex vert
			#pragma fragment frag
			#pragma target 3.0

			#include "UnityCG.cginc"
			#include "UnityUI.cginc"

			#pragma multi_compile __ UNITY_UI_CLIP_RECT
			#pragma multi_compile __ UNITY_UI_ALPHACLIP
			
			#include "UnityShaderVariables.cginc"
			#define ASE_NEEDS_FRAG_COLOR

			
			struct appdata_t
			{
				float4 vertex   : POSITION;
				float4 color    : COLOR;
				float2 texcoord : TEXCOORD0;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				
			};

			struct v2f
			{
				float4 vertex   : SV_POSITION;
				fixed4 color    : COLOR;
				half2 texcoord  : TEXCOORD0;
				float4 worldPosition : TEXCOORD1;
				UNITY_VERTEX_INPUT_INSTANCE_ID
				UNITY_VERTEX_OUTPUT_STEREO
				
			};
			
			uniform fixed4 _Color;
			uniform fixed4 _TextureSampleAdd;
			uniform float4 _ClipRect;
			uniform sampler2D _MainTex;
			uniform sampler2D _ShadowTexture;
			uniform float _ShadowSpeed;
			uniform float _ShadowXOffset;
			uniform float _ShadowYOffset;
			uniform float4 _FlowColor;
			uniform float4 _Color0;
			uniform sampler2D _TextureSample0;
			uniform float4 _TextureSample0_ST;
			uniform float _Saturate;
			uniform sampler2D _VitualPointLightTexture;
			uniform float _LightXOffset;
			uniform float _LightYOffset;
			uniform float _LightZoom;
			uniform float4 _LightColor;

			
			v2f vert( appdata_t IN  )
			{
				v2f OUT;
				UNITY_SETUP_INSTANCE_ID( IN );
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(OUT);
				UNITY_TRANSFER_INSTANCE_ID(IN, OUT);
				OUT.worldPosition = IN.vertex;
				
				
				OUT.worldPosition.xyz +=  float3( 0, 0, 0 ) ;
				OUT.vertex = UnityObjectToClipPos(OUT.worldPosition);

				OUT.texcoord = IN.texcoord;
				
				OUT.color = IN.color * _Color;
				return OUT;
			}

			fixed4 frag(v2f IN  ) : SV_Target
			{
				UNITY_SETUP_INSTANCE_ID( IN );
				UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX( IN );

				float2 appendResult34 = (float2(0.0 , _ShadowSpeed));
				float2 appendResult30 = (float2(_ShadowXOffset , _ShadowYOffset));
				float2 panner32 = ( 1.0 * _Time.y * appendResult34 + appendResult30);
				float2 texCoord31 = IN.texcoord.xy * float2( 1,1 ) + panner32;
				float2 uv_TextureSample0 = IN.texcoord.xy * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
				float4 tex2DNode2 = tex2D( _TextureSample0, uv_TextureSample0 );
				float3 desaturateInitialColor1 = ( IN.color * tex2DNode2 ).rgb;
				float desaturateDot1 = dot( desaturateInitialColor1, float3( 0.299, 0.587, 0.114 ));
				float3 desaturateVar1 = lerp( desaturateInitialColor1, desaturateDot1.xxx, _Saturate );
				float2 appendResult21 = (float2(_LightXOffset , _LightYOffset));
				float2 texCoord13 = IN.texcoord.xy * float2( 1,1 ) + appendResult21;
				float2 temp_cast_3 = (0.5).xx;
				float4 tex2DNode12 = tex2D( _VitualPointLightTexture, ( ( ( texCoord13 - temp_cast_3 ) * _LightZoom ) + texCoord13 ) );
				float4 appendResult8 = (float4(( ( tex2D( _ShadowTexture, texCoord31 ).r * _FlowColor ) + float4( (( _Color0 * float4( desaturateVar1 , 0.0 ) )).rgb , 0.0 ) + ( tex2DNode12.r * _LightColor * tex2DNode12.a ) ).rgb , ( IN.color.a * tex2DNode2.a )));
				
				half4 color = appendResult8;
				
				#ifdef UNITY_UI_CLIP_RECT
                color.a *= UnityGet2DClipping(IN.worldPosition.xy, _ClipRect);
                #endif
				
				#ifdef UNITY_UI_ALPHACLIP
				clip (color.a - 0.001);
				#endif

				return color;
			}
		ENDCG
		}
	}
	CustomEditor "ASEMaterialInspector"
	
	
}
/*ASEBEGIN
Version=18900
0;0;1536;812.6;1796.483;-38.1188;1.367496;True;False
Node;AmplifyShaderEditor.RangedFloatNode;20;-1660.88,662.1154;Inherit;False;Property;_LightYOffset;Light Y Offset;7;0;Create;True;0;0;0;False;0;False;0;0.22;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;19;-1662.88,582.1154;Inherit;False;Property;_LightXOffset;Light X Offset;6;0;Create;True;0;0;0;False;0;False;0;0.19;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;21;-1479.88,601.1154;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;29;-1653.926,1189.907;Inherit;False;Property;_ShadowYOffset;Shadow Y Offset;9;0;Create;True;0;0;0;False;0;False;0;-1.64;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;33;-1639.047,1402.601;Inherit;False;Property;_ShadowSpeed;Shadow Speed;11;0;Create;True;0;0;0;False;0;False;0;-0.62;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.RangedFloatNode;18;-1276.88,759.1154;Inherit;False;Constant;_Float1;Float 1;4;0;Create;True;0;0;0;False;0;False;0.5;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;13;-1338.009,511.4396;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;28;-1652.926,1109.907;Inherit;False;Property;_ShadowXOffset;Shadow X Offset;8;1;[Header];Create;True;1;Flow Setting;0;0;False;0;False;0;-0.47;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;34;-1403.047,1346.601;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.RangedFloatNode;17;-1099.88,886.1154;Inherit;False;Property;_LightZoom;Light Zoom;5;0;Create;True;0;0;0;False;0;False;0;1.63;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.DynamicAppendNode;30;-1469.926,1128.907;Inherit;False;FLOAT2;4;0;FLOAT;0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleSubtractOpNode;14;-1122.88,693.1154;Inherit;False;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.VertexColorNode;6;-919.5842,-195.8139;Inherit;False;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;2;-1049.913,16.85752;Inherit;True;Property;_TextureSample0;Texture Sample 0;0;0;Create;True;0;0;0;False;0;False;-1;None;d92fab069bfbad247928b21ef71f784b;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.RangedFloatNode;5;-774.0136,267.4576;Inherit;False;Property;_Saturate;Saturate;2;0;Create;True;0;0;0;False;0;False;0;0;0;0;0;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;7;-677.5846,-3.913831;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;16;-932.8799,760.1154;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT;0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.PannerNode;32;-1289.047,1129.601;Inherit;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1;False;1;FLOAT2;0
Node;AmplifyShaderEditor.SimpleAddOpNode;15;-836.8799,494.1154;Inherit;False;2;2;0;FLOAT2;0,0;False;1;FLOAT2;0,0;False;1;FLOAT2;0
Node;AmplifyShaderEditor.TextureCoordinatesNode;31;-1070.055,1081.231;Inherit;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.DesaturateOpNode;1;-493.8575,19.31693;Inherit;False;2;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.ColorNode;3;-594.1138,-174.9425;Inherit;False;Property;_Color0;Color 0;1;1;[HDR];Create;True;0;0;0;False;0;False;1,1,1,1;0.8816556,0.8816556,0.8816556,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;4;-283.6599,9.289662;Inherit;False;2;2;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SamplerNode;12;-688.98,465.7901;Inherit;True;Property;_VitualPointLightTexture;Vitual Point Light Texture;4;0;Create;True;0;0;0;False;0;False;-1;None;f3507182359cc22469de038b7f7a5d56;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SamplerNode;22;-687.6414,863.8414;Inherit;True;Property;_ShadowTexture;Shadow Texture;10;0;Create;True;0;0;0;False;0;False;-1;None;36ada77200269134f905e49618155867;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;8;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1;False;6;FLOAT;0;False;7;SAMPLERSTATE;;False;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;27;-373.1518,275.498;Inherit;False;Property;_FlowColor;Flow Color;12;1;[HDR];Create;True;0;0;0;False;0;False;0,0,0,0;0.3396226,0.02691347,0.02691347,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.ColorNode;36;-617.889,671.9921;Inherit;False;Property;_LightColor;LightColor;3;2;[HDR];[Header];Create;True;1;Point Light Setting;0;0;False;0;False;0,0,0,0;0.2641509,0.004602743,0,1;True;0;5;COLOR;0;FLOAT;1;FLOAT;2;FLOAT;3;FLOAT;4
Node;AmplifyShaderEditor.SwizzleNode;10;-115.8286,5.207228;Inherit;False;FLOAT3;0;1;2;3;1;0;COLOR;0,0,0,0;False;1;FLOAT3;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;24;-119.0835,256.8102;Inherit;True;2;2;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;23;-325.5812,494.4807;Inherit;False;3;3;0;FLOAT;0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0;False;1;COLOR;0
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;11;-413.4727,176.3707;Inherit;False;2;2;0;FLOAT;0;False;1;FLOAT;0;False;1;FLOAT;0
Node;AmplifyShaderEditor.SimpleAddOpNode;26;138.2039,-13.64874;Inherit;False;3;3;0;COLOR;0,0,0,0;False;1;FLOAT3;0,0,0;False;2;COLOR;0,0,0,0;False;1;COLOR;0
Node;AmplifyShaderEditor.DynamicAppendNode;8;285.2688,-13.6532;Inherit;False;FLOAT4;4;0;FLOAT3;0,0,0;False;1;FLOAT;0;False;2;FLOAT;0;False;3;FLOAT;0;False;1;FLOAT4;0
Node;AmplifyShaderEditor.TemplateMultiPassMasterNode;0;607.0215,-13.76133;Float;False;True;-1;2;ASEMaterialInspector;0;6;IntensifyCrystal;5056123faa0c79b47ab6ad7e8bf059a4;True;Default;0;0;Default;2;False;True;2;5;False;-1;10;False;-1;0;1;False;-1;0;False;-1;False;False;False;False;False;False;False;False;False;False;False;False;True;2;False;-1;False;True;True;True;True;True;0;True;-9;False;False;False;False;False;False;False;True;True;0;True;-5;255;True;-8;255;True;-7;0;True;-4;0;True;-6;1;False;-1;1;False;-1;7;False;-1;1;False;-1;1;False;-1;1;False;-1;False;True;2;False;-1;True;0;True;-11;False;True;5;Queue=Transparent=Queue=0;IgnoreProjector=True;RenderType=Transparent=RenderType;PreviewType=Plane;CanUseSpriteAtlas=True;False;0;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;2;0;;0;0;Standard;0;0;1;True;False;;False;0
WireConnection;21;0;19;0
WireConnection;21;1;20;0
WireConnection;13;1;21;0
WireConnection;34;1;33;0
WireConnection;30;0;28;0
WireConnection;30;1;29;0
WireConnection;14;0;13;0
WireConnection;14;1;18;0
WireConnection;7;0;6;0
WireConnection;7;1;2;0
WireConnection;16;0;14;0
WireConnection;16;1;17;0
WireConnection;32;0;30;0
WireConnection;32;2;34;0
WireConnection;15;0;16;0
WireConnection;15;1;13;0
WireConnection;31;1;32;0
WireConnection;1;0;7;0
WireConnection;1;1;5;0
WireConnection;4;0;3;0
WireConnection;4;1;1;0
WireConnection;12;1;15;0
WireConnection;22;1;31;0
WireConnection;10;0;4;0
WireConnection;24;0;22;1
WireConnection;24;1;27;0
WireConnection;23;0;12;1
WireConnection;23;1;36;0
WireConnection;23;2;12;4
WireConnection;11;0;6;4
WireConnection;11;1;2;4
WireConnection;26;0;24;0
WireConnection;26;1;10;0
WireConnection;26;2;23;0
WireConnection;8;0;26;0
WireConnection;8;3;11;0
WireConnection;0;0;8;0
ASEEND*/
//CHKSM=C88B7666FCBFA4E445738473EFBEFDD1B5990C7E