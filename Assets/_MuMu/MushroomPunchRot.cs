using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MushroomPunchRot : MonoBehaviour
{
    public float punchForce = 30;
    public float punchSec = 0.5f;
    public int punchVibrato = 10;

    private bool isTweening;

    private void OnTriggerEnter(Collider other)
    {
        if (isTweening == true) return;

        if (other.CompareTag("Player"))
        {
            isTweening = true;
            Vector3 dir = transform.position - other.transform.position;
            Vector3 punchDir = Vector3.Cross(Vector3.up, dir.GetZeroY());
            transform.DOPunchRotation(punchDir.normalized * punchForce, punchSec, punchVibrato).OnComplete(()=> isTweening = false);
            SoundManager.Instance.PlaySound("MushroomTouch");
        }
    }
}
