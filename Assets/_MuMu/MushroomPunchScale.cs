using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class MushroomPunchScale : MonoBehaviour
{
    public float punchForce = 1;
    public float punchSec = 0.5f;

    private bool isTweening;

    private void OnTriggerEnter(Collider other)
    {
        if (isTweening == true) return;

        if (other.CompareTag("Player"))
        {
            isTweening = true;
            //Vector3 dir = transform.position - other.transform.position;
            transform.DOPunchScale(-Vector3.one * punchForce, punchSec).OnComplete(()=> isTweening = false);
        }
    }
}
