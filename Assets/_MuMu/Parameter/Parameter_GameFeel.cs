using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newGameFeel", menuName = "Parameter/GameFeel", order = 0)]
public class Parameter_GameFeel : ScriptableObject 
{
    [Header("重擊卡偵")]
    public StopFeel heavyAttackStop;
    public SlowTime heavyAttackSlow;

    [Header("最後一擊")]
    public SlowTime lastAttackSlow;
    public ShakeCamera lastAttackShake;

    [Header("格黨卡偵")]
    public StopFeel blockAttackStop;
    public SlowTime blockAttackSlow;

    [Header("最後一擊_Boss")]
    public SlowTime bossLastAttackSlow;
}

/// <summary>
/// 卡肉
/// </summary>
[System.Serializable]
public class StopFeel
{
    public float time;
    public float intensity = 0;

    public bool lerpToggle;
    public float lerpTime;
}

/// <summary>
/// 慢動作
/// </summary>
[System.Serializable]
public class SlowTime
{
    public float time;
    public float intensity = 0.2f;

    public bool lerpToggle;
    public float lerpTime;
}

/// <summary>
/// 攝影機晃動
/// </summary>
[System.Serializable]
public class ShakeCamera
{
    public float time;
    public float intensity;
}
