using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum BossShootType { ShortShoot, LongShoot, ContinuousShootShort, ContinuousShootLong, Strafing }
public class BossShootF : MonoBehaviour
{
    public BossManagerF bossManager;
    private Animator animator;
    private Transform player;

    public float rotateSpeed;
    public Transform muzzle;
    public Transform warningSpot;

    [Header("shortBullet")]
    public GameObject shortBulletPrefab;
    public float shortBulletSpeed;
    public float shortShootPrepareSec;
    public int shortShootBulletDamage;

    [Header("longBullet")]
    public GameObject longBulletPrefab;
    public float longBulletSpeed;
    public float longShootPrepareSec;
    public int longShootBulletDamage;

    public float ContinuousShootSpace;

    public Vector2 strafingAngle = new Vector2(10, 170);
    public int perAngle;
    private BossShootType shootType;
    private bool isShooting;

    private void Start()
    {
        animator = GetComponent<Animator>();
        player = GameObject.FindWithTag("Player").transform;
    }
    private void Update()
    {
        if (bossManager.bossBreak.isBreak == true || bossManager.bossStatus.movieing == true) return;
        if (isShooting == true)
        {
            Quaternion dir = Quaternion.LookRotation(player.position - transform.position);
            transform.rotation = Quaternion.Lerp(transform.rotation, dir, Time.deltaTime * rotateSpeed);
        }
    }
    public void Shoot(BossShootType shootType)
    {
        bossManager.bossHealth.superArmor = true;
        this.shootType = shootType;
        if (shootType == BossShootType.ShortShoot || shootType == BossShootType.ContinuousShootShort)
        {
            animator.SetTrigger("ShortShoot");
            ParticleManager.Instance.PlayParticle("BossShootWarningShort", warningSpot.position, warningSpot.rotation, warningSpot);
            SoundManager.Instance.PlaySound("BossShootWarningShort");
        }
        else if (shootType == BossShootType.LongShoot || shootType == BossShootType.ContinuousShootLong)
        {
            animator.SetTrigger("LongShoot");
            ParticleManager.Instance.PlayParticle("BossShootWarningLong", warningSpot.position, warningSpot.rotation, warningSpot);
            SoundManager.Instance.PlaySound("BossShootWarningLong");
        }
        else if (shootType == BossShootType.Strafing) 
        {
            animator.SetTrigger("ShortShoot");
            Strafing();
            ParticleManager.Instance.PlayParticle("BossShootWarningShort", warningSpot.position, warningSpot.rotation, warningSpot);
            SoundManager.Instance.PlaySound("BossShootWarningLong");
        }
        isShooting = true;
    }
    public void ShootFrame()
    {
        if (shootType == BossShootType.ShortShoot)
            ShortShoot();
        else if(shootType == BossShootType.LongShoot)
            LongShoot();
        else if (shootType == BossShootType.ContinuousShootLong)
            ContinuousShootLong();
        else if (shootType == BossShootType.ContinuousShootShort)
            ContinuousShootShort();
    }

    private void ShortShoot()
    {
        StartCoroutine(Shoot(shortShootPrepareSec, 1, shortShootBulletDamage, true));
    }

    private void LongShoot()
    {
        StartCoroutine(Shoot(longShootPrepareSec, 1, longShootBulletDamage,false));
    }

    private void ContinuousShootShort()
    {
        StartCoroutine(Shoot(shortShootPrepareSec, 3, shortShootBulletDamage, true));
    }
    private void ContinuousShootLong()
    {
        StartCoroutine(Shoot(longShootPrepareSec, 3, longShootBulletDamage, false));
    }

    private void Strafing()
    {
        StartCoroutine(DoStrafing());
    }

    private IEnumerator Shoot(float prePareSec, int bulletCount, int bulletDamage, bool isShortshoot)
    {
        yield return new WaitForSeconds(prePareSec);
        int counter = bulletCount;
        while(counter > 0)
        {
            if (isShortshoot)
            {
                GameObject bullet = Instantiate(shortBulletPrefab, muzzle.position, muzzle.rotation);
                bullet.GetComponent<EnemyBullet>().SetBullet(shortBulletSpeed, bulletDamage, GetPlayerDir(), transform);
            }
            else
            {
                GameObject bullet = Instantiate(longBulletPrefab, muzzle.position, muzzle.rotation);
                bullet.GetComponent<EnemyBullet>().SetBullet(longBulletSpeed, bulletDamage, GetPlayerDir(), transform);
            }
            SoundManager.Instance.PlaySound("Shoot");
            counter--;
            yield return new WaitForSeconds(ContinuousShootSpace);
        }
        bossManager.bossHealth.superArmor = false;
    }

    private IEnumerator DoStrafing()
    {
        float angle = strafingAngle.x;
        while (angle <= strafingAngle.y)
        {
            GameObject bullet = Instantiate(shortBulletPrefab, muzzle.position, muzzle.rotation);
            bullet.GetComponent<EnemyBullet>().SetBullet(shortBulletSpeed, shortShootBulletDamage, Quaternion.Euler(0, -90 + angle, 0) * transform.forward, transform);
            angle += perAngle;
            yield return new WaitForSeconds(ContinuousShootSpace);
        }
        isShooting = false;
    }
    private Vector3 GetPlayerDir()
    {
        Vector3 dir = player.position.GetZeroY() - muzzle.transform.position.GetZeroY();
        return dir.normalized;
    }

    public void Break()
    {
        StopAllCoroutines();
        bossManager.bossHealth.superArmor = false;
    }

    public void StopAction()
    {
        StopAllCoroutines();
    }
}
