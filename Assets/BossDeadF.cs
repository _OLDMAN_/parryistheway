using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossDeadF : MonoBehaviour
{
    public BossManagerF bossManager;
    private Animator animator;
    public bool die;
    public ParticleSystem dieParticle;
    public BossBlackDialogue blackDialogue;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void Die()
    {
        die = true;
        animator.enabled = false;
        bossManager.comboEvent.ResetEmpty();
        bossManager.shoot.Break();
        bossManager.hand_L.Break();
        bossManager.hand_R.Break();
        bossManager.bossBreak.SwitchKinematic(false);
        GamefeelManager.Instance.BossLastKill();
        PlayerController.Instance.canCtrl = false;
        StartCoroutine(DieAnimation());
    }

    private IEnumerator DieAnimation()
    {
        yield return new WaitForSecondsRealtime(1);
        dieParticle.Play();
        yield return new WaitForSeconds(2);
        GamefeelManager.Instance.ShakeCamera(4, 0.5f);
        BlackManager.Instance.BlackFadeOut(1f);
        yield return new WaitForSeconds(1);
        blackDialogue.StartDialogue();
    }
}
