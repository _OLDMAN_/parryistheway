﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BossManagerF : MonoBehaviour
{
    public BossCtrlF ctrl;
    public BossShootF shoot;
    public BossBreakF bossBreak;
    public BossHandF hand_L;
    public BossHandF hand_R;
    public NewBossHealth bossHealth;
    public BossComboEvent comboEvent;
    public NewBossStatus bossStatus;

    public void RestartBossRoom()
    {
        BlackManager.Instance.BlackFadeOut(1.5f, () => {
            PlayerHP.Instance.ResetValue();
            Room_Boss.startPlay = true;
            SceneManager.LoadScene("Boss_試擺");
        });
    }
}
