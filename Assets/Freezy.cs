using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Freezy : MonoBehaviour
{
    public KeyCode freeze;
    public KeyCode unfreeze;
    public float delay;

    private void Update()
    {
        if (Input.GetKeyDown(freeze))
            Freeze();
        else if(Input.GetKeyDown(unfreeze))
            UnFreeze();
    }

    private void Freeze()
    {
        StartCoroutine(DelayFeeze());
    }

    private IEnumerator DelayFeeze()
    {
        yield return new WaitForSecondsRealtime(delay);
        Time.timeScale = 0f;
    }

    private void UnFreeze()
    {
        Time.timeScale = 1f;
    }
}
