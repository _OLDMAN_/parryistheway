using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class testLoad : MonoBehaviourSingleton<testLoad>
{
    public GameObject loader;
    public Text progressText;
    public Image progressImage;

    public void LoadScene(string sceneName)
    {
        StartCoroutine(StartLoadScene(sceneName));
    }
    private IEnumerator StartLoadScene(string sceneName)
    {
        loader.SetActive(true);
        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(sceneName);
        while (!asyncLoad.isDone)
        {
            progressText.text = (asyncLoad.progress * 100).ToString("0.0") + "%";
            progressImage.fillAmount = asyncLoad.progress;
            yield return null;
        }
        loader.SetActive(false);
    }
}
