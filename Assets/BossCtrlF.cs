using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BossCtrlF : MonoBehaviour
{
    public BossComboEvent comboEvent;
    public Vector3 oriPos;
    private Vector3 oriRot = new Vector3(0, 180, 0);

    public Transform awlParent;
    public GameObject awlPrefab;
    public Transform awlArea;
    public GameObject wavePrefab;
    public Transform wave_L;
    public Transform wave_R;

    private void Start()
    {
        oriPos = transform.position;
    }

    public void StartAction()
    {
         comboEvent.PlayRandCombo();
    }

    public void Relax(float time)
    {
        transform.DOMove(oriPos, time);
        transform.DORotate(oriRot, time);
    }

    public void SpawnAwl()
    {
        float xSize = awlArea.localScale.x / 2f;
        float randX = Random.Range(-xSize, xSize);
        float zSize = awlArea.localScale.z / 2f;
        float randZ = Random.Range(-zSize, zSize);
        Vector3 randSpwanPoint = awlArea.position + new Vector3(randX, 5, randZ);
        Instantiate(awlPrefab, randSpwanPoint, Quaternion.identity, awlParent);
    }

    public void SpawnWave_L()
    {
        Instantiate(wavePrefab, wave_L.position, wave_L.rotation);
    }
    public void SpawnWave_R()
    {
        Instantiate(wavePrefab, wave_R.position, wave_R.rotation);
    }
}
