using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class shaderController : MonoBehaviour
{
    public Material material;
    void Start()
    {
        // typeof(Material)是为了防止不同类型文件重名
        //material = GetComponent<MeshRenderer>().material(0);
        material = Resources.Load("RenderFeatureTest", typeof(Material)) as Material;
        material.SetFloat("_StyleSwitch", 0);
    }
    void ShowMsg(int Size)
    {
        //设置浮点数值
        material.SetFloat("_StyleSwitch", Size);
    }
    void ShowMsg2(int Size2)
    {
        //设置浮点数值
        material.SetFloat("_StyleSwitch", Size2);
    }
}
