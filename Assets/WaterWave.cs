using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterWave : MonoBehaviour
{

    private void OnEnable()
    {
        InvokeRepeating("PlayWaveSound", 0, 3);
    }
    private void OnDisable()
    {
        CancelInvoke("PlayWaveSound");
    }

    private void PlayWaveSound()
    {
        SoundManager.Instance.PlaySound("BossWaves");
    }
}
