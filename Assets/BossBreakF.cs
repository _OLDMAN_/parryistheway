using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class BossBreakF : MonoBehaviour
{
    public BossManagerF bossManager;
    private Animator animator;
    private Rigidbody rb;
    public Rigidbody[] breakObj;
    public Transform[] allTransform;
    private Vector3[] allOriLocalPos;
    private Quaternion[] allOriLocalRot;

    public int maxBreakCount = 5;
    [HideInInspector] public int breakCounter;
    public float dizzyTime = 3;
    public float recoverTime;
    public bool isBreak;
    private GameObject dizzyObj;

    private void Start()
    {
        animator = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        breakCounter = maxBreakCount;
        allOriLocalPos = new Vector3[allTransform.Length];
        allOriLocalRot = new Quaternion[allTransform.Length];
        for (int i = 0; i < allTransform.Length; i++)
        {
            allOriLocalPos[i] = allTransform[i].localPosition;
            allOriLocalRot[i] = allTransform[i].localRotation;
        }
    }
    public void Break()
    {
        breakCounter = maxBreakCount;
        animator.enabled = false;
        bossManager.comboEvent.ResetEmpty();
        bossManager.shoot.Break();
        bossManager.hand_L.Break();
        bossManager.hand_R.Break();
        SwitchKinematic(false);
        dizzyObj = ParticleManager.Instance.GetParticle("EnemyDizzy", transform.position + Vector3.up * 4, Quaternion.identity);
        dizzyObj.transform.localScale = Vector3.one * 3;
        Invoke("BreakRecover", dizzyTime);
        isBreak = true;
    }

    public void BreakRecover()
    {
        bossManager.hand_L.BreakRecover(recoverTime);
        bossManager.hand_R.BreakRecover(recoverTime);
        SwitchKinematic(true);
        StartCoroutine(DoBreakObjReset());
    }

    public void SwitchKinematic(bool value)
    {
        foreach (Rigidbody rb in breakObj)
            rb.isKinematic = value;
        rb.isKinematic = value;
    }

    private IEnumerator DoBreakObjReset()
    {
        for (int i = 0; i < allTransform.Length; i++)
        {
            allTransform[i].transform.DOLocalMove(allOriLocalPos[i], recoverTime);
            allTransform[i].transform.DOLocalRotateQuaternion(allOriLocalRot[i], recoverTime);
        }
        bossManager.ctrl.Relax(recoverTime);
        yield return new WaitForSeconds(recoverTime);
        //�w�t��_����
        animator.enabled = true;
        animator.Play("Idle");
        dizzyObj.GetComponent<PoolRecycle>().Recycle();

        yield return new WaitForSeconds(1);
        bossManager.comboEvent.PlayRandCombo();
        isBreak = false;
    }

    public void Relax()
    {
        StopAllCoroutines();
        CancelInvoke();
        StartCoroutine(ResetStonePosition());
    }

    private IEnumerator ResetStonePosition()
    {
        SwitchKinematic(true);
        for (int i = 0; i < allTransform.Length; i++)
        {
            allTransform[i].transform.DOLocalMove(allOriLocalPos[i], recoverTime);
            allTransform[i].transform.DOLocalRotateQuaternion(allOriLocalRot[i], recoverTime);
        }
        yield return new WaitForSeconds(recoverTime);
        //�w�t��_����
        animator.enabled = true;
        animator.Play("Idle");
        if (dizzyObj != null)
            dizzyObj.GetComponent<PoolRecycle>().Recycle();
        isBreak = false;
    }
}
